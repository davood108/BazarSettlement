﻿namespace Test_Project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column1 = new RPNCalendar.UI.Controls.DataGridViewPersianDateTimePickerColumn();
            this.faDatePicker1 = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.faMonthView1 = new RPNCalendar.UI.Controls.PersianMonthView();
            this.persianDatePickerConverter1 = new RPNCalendar.UI.Controls.PersianDatePickerConverter();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(12, 215);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 21);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(121, 186);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "نمایش تاریخ انتخاب شده";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(184, 12);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.Size = new System.Drawing.Size(357, 150);
            this.dataGridViewX1.TabIndex = 6;
            // 
            // Column1
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Column1.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column1.HeaderText = "Column1";
            this.Column1.HorizontalAlignment = System.Drawing.StringAlignment.Near;
            this.Column1.Name = "Column1";
            this.Column1.SelectedDateTime = new System.DateTime(((long)(0)));
            this.Column1.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            this.Column1.VerticalAlignment = System.Drawing.StringAlignment.Near;
            // 
            // faDatePicker1
            // 
            this.faDatePicker1.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.faDatePicker1.Location = new System.Drawing.Point(12, 186);
            this.faDatePicker1.Name = "faDatePicker1";
            this.faDatePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.faDatePicker1.ScrollOption = RPNCalendar.UI.ScrollOptionTypes.Day;
            this.faDatePicker1.Size = new System.Drawing.Size(103, 20);
            this.faDatePicker1.TabIndex = 4;
            this.faDatePicker1.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            this.faDatePicker1.SelectedDateTimeChanged += new System.EventHandler(this.faDatePicker1_SelectedDateTimeChanged);
            // 
            // faMonthView1
            // 
            this.faMonthView1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.faMonthView1.IsNull = false;
            this.faMonthView1.Location = new System.Drawing.Point(12, 14);
            this.faMonthView1.Name = "faMonthView1";
            this.faMonthView1.SelectedDateTime = new System.DateTime(2009, 5, 6, 10, 38, 13, 867);
            this.faMonthView1.TabIndex = 3;
            this.faMonthView1.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // persianDatePickerConverter1
            // 
            this.persianDatePickerConverter1.Location = new System.Drawing.Point(218, 215);
            this.persianDatePickerConverter1.Name = "persianDatePickerConverter1";
            this.persianDatePickerConverter1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.persianDatePickerConverter1.Size = new System.Drawing.Size(141, 20);
            this.persianDatePickerConverter1.TabIndex = 7;
            this.persianDatePickerConverter1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.persianDatePickerConverter1.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(432, 217);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "نمایش جعبه پیام";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 252);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.persianDatePickerConverter1);
            this.Controls.Add(this.dataGridViewX1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.faDatePicker1);
            this.Controls.Add(this.faMonthView1);
            this.Controls.Add(this.dateTimePicker1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private RPNCalendar.UI.Controls.PersianMonthView faMonthView1;
        private RPNCalendar.UI.Controls.PersianDatePicker faDatePicker1;
        private System.Windows.Forms.Button button1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private RPNCalendar.UI.Controls.DataGridViewPersianDateTimePickerColumn Column1;
        private RPNCalendar.UI.Controls.PersianDatePickerConverter persianDatePickerConverter1;
        private System.Windows.Forms.Button button2;
    }
}

