﻿namespace RPNCalendar.Resource
{
    /// <summary>
    /// كلاس پایه تغییر زبان
    /// </summary>
    public abstract class BaseLocalizer
    {

        #region Abstract Methods
        /// <summary>
        /// متد ابستركت برای دریافت كلمه ی محلی شده
        /// </summary>
        /// <param name="id">كد كلمه</param>
        /// <returns>مقدار كلمه</returns>
        public abstract System.String GetLocalizedString(StringIDEnum id);
        #endregion

    }
}