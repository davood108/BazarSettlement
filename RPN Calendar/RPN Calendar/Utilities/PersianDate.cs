﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using RPNCalendar.Resource;
using RPNCalendar.Utilities;
using RPNCalendar.Utilities.Exceptions;
#endregion

namespace RPNCalendar.Utilities
{
    /// <summary>
    /// كلاس ارائه تقویم جلالی
    /// <example>مثالی از نحوه فراخوانی كد
    /// <code>
    ///		class MyClass 
    ///     {
    ///		   public static void Main() 
    ///        {
    ///		      Console.WriteLine("Current Persian Date Is : " + PersianDate.Now.ToString());
    ///		   }
    ///		}
    /// </code>
    /// </example>
    /// <seealso cref="PersianDateConverter"/>
    /// </summary>
    [TypeConverter("RPNCalendar.UI.Design.PersianDateTypeConverter")]
    [Serializable]
    public sealed class PersianDate :
        IFormattable,
        ICloneable,
        IComparable,
        IComparable<PersianDate>,
        IComparer,
        IComparer<PersianDate>,
        IEquatable<PersianDate>
    {

        #region Fields

        #region Private Methods
        private int _Year;
        private int _Month;
        private int _Day;
        private int _Hour;
        private int _Minute;
        private int _Second;
        private int _Millisecond;
        private readonly TimeSpan _Time;
        private static readonly PersianDate _CurrentPersianDate;
        private const string _AmDesignator = "ق.ظ";
        private const string _PmDesignator = "ب.ظ";
        private readonly PersianCalendar _PersianCalendar = new PersianCalendar();
        #endregion

        #region Public Methods
        [NonSerialized]
        public static PersianDate MinValue;

        [NonSerialized]
        public static PersianDate MaxValue;
        #endregion

        #endregion

        #region Constructors

        #region static PersianDate()
        /// <summary>
        /// سازنده استاتیك كلاس ، تنظیم كننده مقدار جاری تقویم فارسی و كران تقویم
        /// </summary>
        static PersianDate()
        {
            _CurrentPersianDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            MinValue = new PersianDate(1, 1, 1, 12, 0, 0, 0); // 12:00:00.000 AM, 22/03/0622
            MaxValue = new PersianDate(DateTime.MaxValue);
        }
        #endregion

        #region public PersianDate()
        /// <summary>
        /// سازنده پیش فرض كلاس
        /// </summary>
        public PersianDate()
        {
            _Year = Now._Year;
            _Month = Now.Month;
            _Day = Now.Day;
            _Hour = Now.Hour;
            _Minute = Now.Minute;
            _Second = Now.Second;
            _Millisecond = Now.Millisecond;
            _Time = Now.Time;
        }
        #endregion

        #region public PersianDate(DateTime EnglishDateTime)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnglishDateTime"></param>
        public PersianDate(DateTime EnglishDateTime)
        {
            Assign(PersianDateConverter.ToPersianDate(EnglishDateTime));
        }
        #endregion

        #region public PersianDate(string datetime, string format)
        /// <summary>
        /// Constructs a PersianDate instance with values provided in datetime string. 
        /// use the format you want to parse the string with. Currently it can be either g, G, or d value.
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="format"></param>
        public PersianDate(string datetime, string format)
        {
            Assign(Parse(datetime, format));
        }
        #endregion

        #region public PersianDate(string Date, TimeSpan time)
        /// <summary>
        /// Constructs a PersianDate instance with values provided in datetime string. You should
        /// include Date part only in <c>Date</c> and set the Time of the instance as a <c>TimeSpan</c>.
        /// </summary>
        /// <exception cref="InvalidPersianDateException"></exception>
        /// <param name="Date"></param>
        /// <param name="time"></param>
        public PersianDate(string Date, TimeSpan time)
        {
            PersianDate pd = Parse(Date);

            pd.Hour = time.Hours;
            pd.Minute = time.Minutes;
            pd.Second = time.Seconds;
            pd.Millisecond = time.Milliseconds;

            Assign(pd);
        }
        #endregion

        #region public PersianDate(string Date)
        /// <summary>
        /// Constructs a PersianDate instance with values provided as a string. The provided string should be in format 'yyyy/mm/dd'.
        /// </summary>
        /// <exception cref="InvalidPersianDateException"></exception>
        /// <param name="Date"></param>
        public PersianDate(string Date)
        {
            Assign(Parse(Date));
        }
        #endregion

        #region public PersianDate(int year, int month, int day, int hour, int minute)
        /// <summary>
        /// Constructs a PersianDate instance with values specified as <c>Integer</c> 
        /// and default second and millisecond set to zero.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        public PersianDate(int year, int month, int day, int hour, int minute)
            : this(year, month, day, hour, minute, 0)
        {

        }
        #endregion

        #region public PersianDate(int year, int month, int day, int hour, int minute, int second)
        /// <summary>
        /// Constructs a PersianDate instance with values specified as <c>Integer</c> and default millisecond set to zero.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <param name="second"></param>
        public PersianDate(int year, int month, int day, int hour, int minute, int second)
            : this(year, month, day, hour, minute, second, 0)
        {

        }
        #endregion

        #region public PersianDate(int year, int month, int day, int hour, int minute, int second, int millisecond)
        /// <summary>
        /// Constructs a PersianDate instance with values specified as <c>Integer</c>.
        /// </summary>
        /// <exception cref="InvalidPersianDateException"></exception>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <param name="second"></param>
        /// <param name="millisecond"></param>
        public PersianDate(int year, int month, int day, int hour, int minute, int second, int millisecond)
        {
            CheckYear(year);
            CheckMonth(month);
            CheckDay(year, month, day);
            CheckHour(hour);
            CheckMinute(minute);
            CheckSecond(second);
            CheckMillisecond(millisecond);

            _Year = year;
            _Month = month;
            _Day = day;
            _Hour = hour;
            _Minute = minute;
            _Second = second;
            _Millisecond = millisecond;

            _Time = new TimeSpan(0, hour, minute, second, millisecond);
        }
        #endregion

        #region public PersianDate(int year, int month, int day)
        /// <summary>
        /// Constructs a PersianDate instance with values specified as <c>Integer</c>. 
        /// Time value of this instance is set to <c>DateTime.Now</c>.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        public PersianDate(int year, int month, int day)
        {
            CheckYear(year);
            CheckMonth(month);
            CheckDay(year, month, day);

            _Year = year;
            _Month = month;
            _Day = day;
            _Hour = DateTime.Now.Hour;
            _Minute = DateTime.Now.Minute;
            _Second = DateTime.Now.Second;
            _Millisecond = DateTime.Now.Millisecond;

            _Time = new TimeSpan(0, _Hour, _Minute, _Second, _Millisecond);
        }
        #endregion

        #endregion

        #region Properties

        #region public static string AMDesignator
        /// <summary>
        /// AMDesignator.
        /// </summary>
        public static string AMDesignator
        {
            get { return _AmDesignator; }
        }
        #endregion

        #region public static string PMDesignator
        /// <summary>
        /// PMDesignator.
        /// </summary>
        public static string PMDesignator
        {
            get { return _PmDesignator; }
        }
        #endregion

        #region public static PersianDate Now
        /// <summary>
        /// Current date/time in PersianDate format.
        /// </summary>
        [Browsable(false)]
        [Description("Current date/time in PersianDate format")]
        public static PersianDate Now
        {
            get { return _CurrentPersianDate; }
        }
        #endregion

        #region public int Year
        /// <summary>
        /// Year value of PersianDate.
        /// </summary>
        [Description("Year value of PersianDate")]
        [NotifyParentProperty(true)]
        public int Year
        {
            get { return _Year; }
            set
            {
                CheckYear(value);
                _Year = value;
            }
        }
        #endregion

        #region public int Month
        /// <summary>
        /// Month value of PersianDate.
        /// </summary>
        [Description("Month value of PersianDate")]
        [NotifyParentProperty(true)]
        public int Month
        {
            get { return _Month; }
            set
            {
                CheckMonth(value);
                _Month = value;
            }
        }
        #endregion

        #region public int Day
        /// <summary>
        /// Day value of PersianDate.
        /// </summary>
        [Description("Day value of PersianDate")]
        [NotifyParentProperty(true)]
        public int Day
        {
            get { return _Day; }
            set
            {
                CheckDay(Year, Month, value);
                _Day = value;
            }
        }
        #endregion

        #region public int Hour
        /// <summary>
        /// Hour value of PersianDate.
        /// </summary>
        [Description("Hour value of PersianDate")]
        [NotifyParentProperty(true)]
        public int Hour
        {
            get { return _Hour; }
            set
            {
                CheckHour(value);
                _Hour = value;
            }
        }
        #endregion

        #region public int Minute
        /// <summary>
        /// Minute value of PersianDate.
        /// </summary>
        [Description("Minute value of PersianDate")]
        [NotifyParentProperty(true)]
        public int Minute
        {
            get { return _Minute; }
            set
            {
                CheckMinute(value);
                _Minute = value;
            }
        }
        #endregion

        #region public int Second
        /// <summary>
        /// Second value of PersianDate.
        /// </summary>
        [Description("Second value of PersianDate")]
        [NotifyParentProperty(true)]
        public int Second
        {
            get { return _Second; }
            set
            {
                CheckSecond(value);
                _Second = value;
            }
        }
        #endregion

        #region public int Millisecond
        /// <summary>
        /// Millisecond value of PersianDate.
        /// </summary>
        [Description("Millisecond value of PersianDate")]
        [NotifyParentProperty(true)]
        public int Millisecond
        {
            get { return _Millisecond; }
            set
            {
                CheckMillisecond(value);
                _Millisecond = value;
            }
        }
        #endregion

        #region public TimeSpan Time
        /// <summary>
        /// Time value of PersianDate in TimeSpan format.
        /// </summary>
        [Browsable(false)]
        [Description("Time value of PersianDate in TimeSpan format.")]
        public TimeSpan Time
        {
            get { return _Time; }
        }
        #endregion

        #region public PersianDayOfWeek DayOfWeek
        /// <summary>
        /// Returns the DayOfWeek of the date instance
        /// </summary>
        public PersianDayOfWeek DayOfWeek
        {
            get
            {
                DateTime dt = this;
                switch (dt.DayOfWeek)
                {
                    case System.DayOfWeek.Saturday: return PersianDayOfWeek.Saturday;
                    case System.DayOfWeek.Sunday: return PersianDayOfWeek.Sunday;
                    case System.DayOfWeek.Monday: return PersianDayOfWeek.Monday;
                    case System.DayOfWeek.Tuesday: return PersianDayOfWeek.Tuesday;
                    case System.DayOfWeek.Wednesday: return PersianDayOfWeek.Wednesday;
                    case System.DayOfWeek.Thursday: return PersianDayOfWeek.Thursday;
                    default: return PersianDayOfWeek.Friday;
                }
            }
        }
        #endregion

        #region public string LocalizedMonthName
        /// <summary>
        /// Localized name of PersianDate months.
        /// </summary>
        [Browsable(false)]
        [Description("Localized name of PersianDate months")]
        public string LocalizedMonthName
        {
            get { return PersianMonthNames.Default[_Month]; }
        }
        #endregion

        #region public string LocalizedWeekDayName
        /// <summary>
        /// Weekday names of this instance in localized format.
        /// </summary>
        [Browsable(false)]
        [Description("Weekday names of this instance in localized format.")]
        public string LocalizedWeekDayName
        {
            get
            {
                return PersianDateConverter.DayOfWeek(this);
            }
        }
        #endregion

        #region public int MonthDays
        /// <summary>
        /// Number of days in this _Month.
        /// </summary>
        [Browsable(false)]
        [Description("Number of days in this _Month")]
        public int MonthDays
        {
            get
            {
                return PersianDateConverter.MonthDays(_Month);
            }
        }
        #endregion

        #region public bool IsNull
        [Browsable(false)]
        public bool IsNull
        {
            get
            {
                return Year == MinValue.Year && Month == MinValue.Month && Day == MinValue.Day;
            }
        }
        #endregion
        
        #endregion

        #region Overrided Methods

        #region public override string ToString()
        /// <summary>
        /// Returns Date in 'yyyy/mm/dd' string format.
        /// </summary>
        /// <returns>string representation of evaluated Date.</returns>
        /// <example>An example on how to get the written form of a Date.
        /// <code>
        ///		class MyClass {
        ///		   public static void Main()
        ///		   {	
        ///				Console.WriteLine(PersianDate.Now.ToString());
        ///		   }
        ///		}
        /// </code>
        /// </example>
        /// <seealso cref="ToWritten"/>
        public override string ToString()
        {
            return ToString("g", null);
        }
        #endregion

        #endregion

        #region Operators Overloading

        #region public static bool operator == (PersianDate date1, PersianDate date2)
        /// <summary>
        /// Compares two instance of the PersianDate for the specified operator.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static bool operator ==(PersianDate date1, PersianDate date2)
        {
            if ((date1 as object) == null && (date2 as object) == null) return true;
            if ((date1 as object) == null || (date2 as object) == null) return false;

            return date1.Year == date2.Year &&
                   date1.Month == date2.Month &&
                   date1.Day == date2.Day &&
                   date1.Hour == date2.Hour &&
                   date1.Minute == date2.Minute &&
                   date1.Second == date2.Second &&
                   date1.Millisecond == date2.Millisecond;
        }
        #endregion

        #region public static bool operator != (PersianDate date1, PersianDate date2)
        /// <summary>
        /// Compares two instance of the PersianDate for the specified operator.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static bool operator !=(PersianDate date1, PersianDate date2)
        {
            if ((date1 as object) == null && (date2 as object) == null) return false;
            if ((date1 as object) == null || (date2 as object) == null) return true;

            return date1.Year != date2.Year ||
                   date1.Month != date2.Month ||
                   date1.Day != date2.Day ||
                   date1.Hour != date2.Hour ||
                   date1.Minute != date2.Minute ||
                   date1.Second != date2.Second ||
                   date1.Millisecond != date2.Millisecond;
        }
        #endregion

        #region public static bool operator > (PersianDate date1, PersianDate date2)
        /// <summary>
        /// Compares two instance of the PersianDate for the specified operator.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static bool operator >(PersianDate date1, PersianDate date2)
        {
            if ((date1 as object) == null && (date2 as object) == null) return false;
            if ((date1 as object) == null || (date2 as object) == null)
                throw new NullReferenceException();
            // =======================================================
            if (date1.Year > date2.Year) return true;
            if (date1.Year == date2.Year && date1.Month > date2.Month) return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day > date2.Day)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour > date2.Hour)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour == date2.Hour &&
                date1.Minute > date2.Minute)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour == date2.Hour &&
                date1.Minute == date2.Minute &&
                date1.Second > date2.Second)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour == date2.Hour &&
                date1.Minute == date2.Minute &&
                date1.Second == date2.Second &&
                date1.Millisecond > date2.Millisecond)
                return true;
            // =======================================================
            return false;
        }
        #endregion

        #region public static bool operator < (PersianDate date1, PersianDate date2)
        /// <summary>
        /// Compares two instance of the PersianDate for the specified operator.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static bool operator <(PersianDate date1, PersianDate date2)
        {
            if ((date1 as object) == null && (date2 as object) == null)
                return false;
            // =======================================================
            if ((date1 as object) == null || (date2 as object) == null)
                throw new NullReferenceException();
            // =======================================================
            if (date1.Year < date2.Year) return true;
            // =======================================================
            if (date1.Year == date2.Year && date1.Month < date2.Month) return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day < date2.Day)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour < date2.Hour)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour == date2.Hour &&
                date1.Minute < date2.Minute)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour == date2.Hour &&
                date1.Minute == date2.Minute &&
                date1.Second < date2.Second)
                return true;
            // =======================================================
            if (date1.Year == date2.Year &&
                date1.Month == date2.Month &&
                date1.Day == date2.Day &&
                date1.Hour == date2.Hour &&
                date1.Minute == date2.Minute &&
                date1.Second == date2.Second &&
                date1.Millisecond < date2.Millisecond)
                return true;
            // =======================================================
            return false;
        }
        #endregion

        #region public static bool operator <= (PersianDate date1, PersianDate date2)
        /// <summary>
        /// Compares two instance of the PersianDate for the specified operator.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static bool operator <=(PersianDate date1, PersianDate date2)
        {
            return (date1 < date2) || (date1 == date2);
        }
        #endregion

        #region public static bool operator >= (PersianDate date1, PersianDate date2)
        /// <summary>
        /// Compares two instance of the PersianDate for the specified operator.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static bool operator >=(PersianDate date1, PersianDate date2)
        {
            return (date1 > date2) || (date1 == date2);
        }
        #endregion

        #region public override int GetHashCode()
        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// System.Object.GetHashCode() is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return ToString("s").GetHashCode();
        }
        #endregion

        #region public override bool Equals(object obj)
        /// <summary>
        /// Determines whether the specified System.Object instances are considered equal.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is PersianDate) return this == (PersianDate)obj;
            return false;
        }
        #endregion

        #endregion

        #region Implicit Casting

        #region public static implicit operator DateTime(PersianDate ThePersianDate)
        public static implicit operator DateTime(PersianDate ThePersianDate)
        {
            return PersianDateConverter.ToGregorianDateTime(ThePersianDate);
        }
        #endregion

        #region public static implicit operator PersianDate(DateTime EnglishDate)
        public static implicit operator PersianDate(DateTime EnglishDate)
        {
            if (EnglishDate.Equals(DateTime.MinValue)) return MinValue;
            return PersianDateConverter.ToPersianDate(EnglishDate);
        }
        #endregion

        #endregion

        #region Interfaces Implementation

        #region ICloneable Members
        /// <summary>
        /// ارائه یك شیء شبیه سازی شده از كلاس تقویم فارسی
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new PersianDate(Year, Month, Day, Hour, Minute, Second, Millisecond);
        }
        #endregion

        #region ICompareable Interface

        #region public int CompareTo(object obj)
        ///<summary>
        /// Compares the current instance with another object of the same type.
        ///</summary>
        ///<returns>
        ///A 32-bit signed integer that indicates the relative order of the objects being compared. 
        /// The return value has these meanings: 
        /// Less than zero: This instance is less than obj. Zero: This instance is equal to obj. 
        /// Greater than zero: This instance is greater than obj.
        ///</returns>
        ///<param name="obj">An object to compare with this instance. </param>
        ///<exception cref="T:System.ArgumentException">
        /// obj is not the same type as this instance. 
        /// </exception>
        /// <filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            if (!(obj is PersianDate))
                throw new ArgumentException("Comparing value is not of type PersianDate.");
            // =====================================================
            PersianDate ComparedPersianDate = (PersianDate)obj;
            if (ComparedPersianDate < this) return 1;
            if (ComparedPersianDate > this) return -1;
            return 0;
        }
        #endregion

        #endregion

        #region IComparer Interface

        #region public int Compare(object x, object y)
        ///<summary>
        ///Compares two objects and returns a value indicating whether 
        /// one is less than, equal to, or greater than the other.
        ///</summary>
        ///<returns>
        ///Value Condition Less than zero x is less than y. Zero x equals y. Greater than zero x is greater than y. 
        ///</returns>
        ///<param name="y">The second object to compare. </param>
        ///<param name="x">The first object to compare. </param>
        ///<exception cref="T:System.ArgumentException">
        /// either x nor y implements the 
        /// <see cref="T:System.IComparable"></see> 
        /// interface.-or- x and y are of different types and neither one can handle comparisons with the other. 
        /// </exception><filterpriority>2</filterpriority>
        /// <exception cref="T:System.ApplicationException">Either x or y is a null reference</exception>
        public int Compare(object x, object y)
        {
            if (x == null || y == null) throw new ApplicationException("Invalid PersianDate comparer.");

            if (!(x is PersianDate)) throw new ArgumentException("x value is not of type PersianDate.");

            if (!(y is PersianDate)) throw new ArgumentException("y value is not of type PersianDate.");

            PersianDate pd1 = (PersianDate)x;
            PersianDate pd2 = (PersianDate)y;

            if (pd1 > pd2) return 1;
            if (pd1 < pd2) return -1;
            return 0;
        }
        #endregion

        #endregion

        #region IComparer<T> Interface

        #region public int CompareTo(PersianDate other)
        ///<summary>
        /// Compares the current object with another object of the same type.
        ///</summary>
        ///<returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. 
        /// The return value has the following meanings: 
        /// Less than zero: This object is less than the other parameter. 
        /// Zero: This object is equal to other. Greater than zero: This object is greater than other. 
        ///</returns>
        ///<param name="other">An object to compare with this object.</param>
        public int CompareTo(PersianDate other)
        {
            if (other < this) return 1;
            if (other > this) return -1;
            return 0;
        }
        #endregion

        #region public int Compare(PersianDate x, PersianDate y)
        ///<summary>
        /// Compares two objects and returns a value indicating 
        /// whether one is less than, equal to, or greater than the other.
        ///</summary>
        ///<returns>
        /// Value Condition Less than zerox is less than y.Zerox equals y.Greater than zero x is greater than y.
        ///</returns>
        ///<param name="y">The second object to compare.</param>
        ///<param name="x">The first object to compare.</param>
        public int Compare(PersianDate x, PersianDate y)
        {
            if (x > y) return 1;
            if (x < y) return -1;
            return 0;
        }
        #endregion

        #endregion

        #region IEquatable<T>

        #region public bool Equals(PersianDate other)
        ///<summary>
        /// Indicates whether the current object is equal to another object of the same type.
        ///</summary>
        ///<returns>
        /// true if the current object is equal to the other parameter; otherwise, false.
        ///</returns>
        ///<param name="other">An object to compare with this object.</param>
        public bool Equals(PersianDate other)
        {
            return this == other;
        }
        #endregion

        #endregion

        #region IFormattable

        #region public string ToString(string format)
        /// <summary>
        /// Returns string representation of this instance in default format.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public string ToString(string format)
        {
            return ToString(format, null);
        }
        #endregion

        #region public string ToString(IFormatProvider formatProvider)
        /// <summary>
        /// Returns string representation of this instance and format it using the <see cref="IFormatProvider"/> instance.
        /// </summary>
        /// <param name="formatProvider"></param>
        /// <returns></returns>
        public string ToString(IFormatProvider formatProvider)
        {
            return ToString(null, formatProvider);
        }
        #endregion

        #region public string ToString(string format, IFormatProvider formatProvider)
        /// <summary>
        /// Returns string representation of this instance in desired format, 
        /// or using provided <see cref="IFormatProvider"/> instance.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="formatProvider"></param>
        /// <returns></returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (format == null) format = "G";
            int smallhour = (Hour > 12) ? Hour - 12 : Hour;
            string designator = Hour > 12 ? PMDesignator : AMDesignator;
            // ===============================================
            if (formatProvider != null)
            {
                ICustomFormatter formatter = formatProvider.GetFormat(typeof(ICustomFormatter)) as ICustomFormatter;
                if (formatter != null) return formatter.Format(format, this, formatProvider);
            }
            // ===============================================
            switch (format)
            {
                case "D":
                    //'yyyy mm dd dddd' e.g. 'دوشنبه 20 شهریور 1384'
                    return string.Format("{0} {1} {2} {3}",
                        new object[] { LocalizedWeekDayName, PersianUtilities.ConvertToDuplex(Day), LocalizedMonthName, Year });
                // ===============================================
                case "f":
                    //'hh:mm yyyy mmmm dd dddd' e.g. 'دوشنبه 20 شهریور 1384 21:30'
                    return string.Format("{0} {1} {2} {3} {4}:{5}",
                        new object[] { LocalizedWeekDayName, PersianUtilities.ConvertToDuplex(Day), 
                            LocalizedMonthName, Year, PersianUtilities.ConvertToDuplex(Hour), 
                            PersianUtilities.ConvertToDuplex(Minute) });
                // ===============================================
                case "F":
                    //'tt hh:mm:ss yyyy mmmm dd dddd' e.g. 'دوشنبه 20 شهریور 1384 02:30:22 ب.ض'
                    return string.Format("{0} {1} {2} {3} {4}:{5}:{6} {7}", new object[] { 
                        LocalizedWeekDayName, PersianUtilities.ConvertToDuplex(Day), LocalizedMonthName, Year, 
                        PersianUtilities.ConvertToDuplex(smallhour), PersianUtilities.ConvertToDuplex(Minute), 
                        PersianUtilities.ConvertToDuplex(Second), designator });
                // ===============================================
                case "g":
                    // 'yyyy/mm/dd hh:mm tt'
                    return string.Format("{0}/{1}/{2} {3}:{4} {5}", new object[] { Year, PersianUtilities.ConvertToDuplex(Month), 
                        PersianUtilities.ConvertToDuplex(Day), PersianUtilities.ConvertToDuplex(smallhour), 
                        PersianUtilities.ConvertToDuplex(Minute), designator });
                // ===============================================
                case "G":
                    // 'yyyy/mm/dd hh:mm:ss tt'
                    return string.Format("{0}/{1}/{2} {3}:{4}:{5} {6}", new object[] { Year, PersianUtilities.ConvertToDuplex(Month), 
                        PersianUtilities.ConvertToDuplex(Day), PersianUtilities.ConvertToDuplex(smallhour), 
                        PersianUtilities.ConvertToDuplex(Minute), PersianUtilities.ConvertToDuplex(Second), designator });
                // ===============================================
                case "M":
                case "m":
                    // 'yyyy mmmm'
                    return string.Format("{0} {1}", Year, LocalizedMonthName);
                // ===============================================
                case "s":
                    //'yyyy-mm-ddThh:mm:ss'
                    return string.Format("{0}-{1}-{2}T{3}:{4}:{5}", Year, PersianUtilities.ConvertToDuplex(Month),
                        PersianUtilities.ConvertToDuplex(Day), PersianUtilities.ConvertToDuplex(Hour),
                        PersianUtilities.ConvertToDuplex(Minute), PersianUtilities.ConvertToDuplex(Second));
                // ===============================================
                case "t":
                    //'hh:mm tt'
                    return string.Format("{0}:{1} {2}", PersianUtilities.ConvertToDuplex(smallhour),
                        PersianUtilities.ConvertToDuplex(Minute), designator);
                // ===============================================
                case "T":
                    //'hh:mm:ss tt'
                    return string.Format("{0}:{1}:{2} {3}", new object[] { PersianUtilities.ConvertToDuplex(smallhour), 
                        PersianUtilities.ConvertToDuplex(Minute), PersianUtilities.ConvertToDuplex(Second), designator });
                // ===============================================
                default:
                    //ShortDatePattern yyyy/mm/dd e.g. '1384/9/1'
                    return string.Format("{0}/{1}/{2}", Year, PersianUtilities.ConvertToDuplex(Month),
                        PersianUtilities.ConvertToDuplex(Day));
            }
        }
        #endregion

        #endregion

        #endregion

        #region Private Check Methods

        #region static void CheckYear(int YearNo)
        private static void CheckYear(int YearNo)
        {
            if (YearNo < 1 || YearNo > 9999)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidYear));
        }
        #endregion

        #region static void CheckMonth(int MonthNo)
        private static void CheckMonth(int MonthNo)
        {
            if (MonthNo > 12 || MonthNo < 1)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidMonth));
        }
        #endregion

        #region void CheckDay(int YearNo, int MonthNo, int DayNo)
        private void CheckDay(int YearNo, int MonthNo, int DayNo)
        {
            if (MonthNo < 6 && DayNo > 31)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDay));
            if (MonthNo > 6 && DayNo > 30)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDay));
            if (MonthNo == 12 && DayNo > 29)
                if (!_PersianCalendar.IsLeapDay(YearNo, MonthNo, DayNo) || DayNo > 30)
                    throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                        Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDay));
        }
        #endregion

        #region static void CheckHour(int HourNo)
        private static void CheckHour(int HourNo)
        {
            if (HourNo > 24 || HourNo < 0)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidHour));
        }
        #endregion

        #region static void CheckMinute(int MinuteNo)
        private static void CheckMinute(int MinuteNo)
        {
            if (MinuteNo > 60 || MinuteNo < 0)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidMinute));
        }
        #endregion

        #region static void CheckSecond(int SecondNo)
        private static void CheckSecond(int SecondNo)
        {
            if (SecondNo > 60 || SecondNo < 0)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidSecond));
        }
        #endregion

        #region static void CheckMillisecond(int MillisecondNo)
        private static void CheckMillisecond(int MillisecondNo)
        {
            if (MillisecondNo < 0 || MillisecondNo > 1000)
                throw new InvalidPersianDateException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidMillisecond));
        }
        #endregion

        #endregion

        #region Public Methods

        #region void Assign(PersianDate pd)
        /// <summary>
        /// Assigns an instance of PersianDate's values to this instance.
        /// </summary>
        /// <param name="pd"></param>
        public void Assign(PersianDate pd)
        {
            Year = pd.Year;
            Month = pd.Month;
            Day = pd.Day;
            Hour = pd.Hour;
            Minute = pd.Minute;
            Second = pd.Second;
        }
        #endregion

        #region string ToWritten()
        /// <summary>
        /// Returns a string representation of current PersianDate value.
        /// </summary>
        /// <returns></returns>
        public string ToWritten()
        {
            return (LocalizedWeekDayName + " " + _Day + " " + LocalizedMonthName + " " + _Year);
        }
        #endregion

        #region Public Parse Methods

        #region static PersianDate Parse(string value, bool includesTime)
        /// <summary>
        /// Parse a string value into a PersianDate instance. Value could be either in 'yyyy/mm/dd hh:mm:ss' or 
        /// 'yyyy/mm/dd' formats. If you want to parse <c>Time</c> value too,
        /// you should set <c>includesTime</c> to <c>true</c>.
        /// </summary>
        /// <exception cref="InvalidPersianDateException"></exception>
        /// <param name="value"></param>
        /// <param name="includesTime"></param>
        /// <returns></returns>
        public static PersianDate Parse(string value, bool includesTime)
        {
            if (value == string.Empty)
                return MinValue;
            if (includesTime)
            {
                if (value.Length > 19)
                    throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                        Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateTimeLength));

                string[] dt = value.Split(" ".ToCharArray());

                if (dt.Length != 2)
                    throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                        Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateFormat));

                string _date = dt[0];
                string _time = dt[1];

                string[] dateParts = _date.Split("/".ToCharArray());
                string[] timeParts = _time.Split(":".ToCharArray());

                if (dateParts.Length != 3)
                    throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                        Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateFormat));

                if (timeParts.Length != 3)
                    throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                        Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidTimeFormat));

                int day = int.Parse(dateParts[2]);
                int month = int.Parse(dateParts[1]);
                int year = int.Parse(dateParts[0]);
                int hour = int.Parse(timeParts[0]);
                int minute = int.Parse(timeParts[1]);
                int second = int.Parse(timeParts[2]);

                return new PersianDate(year, month, day, hour, minute, second);
            }
            return Parse(value);
        }
        #endregion

        #region static PersianDate Parse(string value, string format)
        public static PersianDate Parse(string value, string format)
        {
            switch (format)
            {
                case "G": //yyyy/mm/dd hh:mm:ss tt
                    return ParseFullDateTime(value);

                case "g": //yyyy/mm/dd hh:mm tt
                    return ParseDateShortTime(value);

                case "d": //yyyy/mm/dd
                    return Parse(value);

                default:
                    throw new ArgumentException("Currently G,g,d formats are supported.");
            }
        }
        #endregion

        #region static PersianDate ParseFullDateTime(string value)
        /// <summary>
        /// Parse a string value into a PersianDate instance. Value should be in 'yyyy/mm/dd hh:mm:ss tt' formats.
        /// </summary>
        /// <exception cref="InvalidPersianDateException"></exception>
        /// <param name="value"></param>
        /// <returns></returns>
        private static PersianDate ParseFullDateTime(string value)
        {
            if (value == string.Empty)
                return PersianDateConverter.ToPersianDate(DateTime.Now);

            if (value.Length > 23)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateTimeLength));

            string[] dt = value.Split(" ".ToCharArray());

            if (dt.Length != 3)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateFormat));

            string _date = dt[0];
            string _time = dt[1];

            string[] dateParts = _date.Split("/".ToCharArray());
            string[] timeParts = _time.Split(":".ToCharArray());

            if (dateParts.Length != 3)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateFormat));

            if (timeParts.Length != 3)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidTimeFormat));

            int day = int.Parse(dateParts[2]);
            int month = int.Parse(dateParts[1]);
            int year = int.Parse(dateParts[0]);
            int hour = int.Parse(timeParts[0]);
            int minute = int.Parse(timeParts[1]);
            int second = int.Parse(timeParts[2]);

            return new PersianDate(year, month, day, hour, minute, second, 0);
        }
        #endregion

        #region static PersianDate ParseDateShortTime(string value)
        /// <summary>
        /// Parse a string value into a PersianDate instance. Value should be in 'yyyy/mm/dd hh:mm tt' formats.
        /// </summary>
        /// <exception cref="InvalidPersianDateException"></exception>
        /// <param name="value"></param>
        /// <returns></returns>
        private static PersianDate ParseDateShortTime(string value)
        {
            if (value == string.Empty)
                return PersianDateConverter.ToPersianDate(DateTime.Now);

            if (value.Length > 20)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateTimeLength));

            string[] dt = value.Split(" ".ToCharArray());

            if (dt.Length != 3)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateFormat));

            string _date = dt[0];
            string _time = dt[1];

            string[] dateParts = _date.Split("/".ToCharArray());
            string[] timeParts = _time.Split(":".ToCharArray());

            if (dateParts.Length != 3)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateFormat));

            if (timeParts.Length != 2)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidTimeFormat));

            int day = int.Parse(dateParts[2]);
            int month = int.Parse(dateParts[1]);
            int year = int.Parse(dateParts[0]);
            int hour = int.Parse(timeParts[0]);
            int minute = int.Parse(timeParts[1]);

            return new PersianDate(year, month, day, hour, minute, 0, 0);
        }
        #endregion

        #region static PersianDate Parse(string value)
        /// <summary>
        /// Parse a string value into a PersianDate instance. Value can only be in 'yyyy/mm/dd' format.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static PersianDate Parse(string value)
        {
            if (value.Length == 10) return ParseShortDate(value);
            if (value.Length == 20) return ParseDateShortTime(value);
            if (value.Length == 23) return ParseFullDateTime(value);

            throw new InvalidPersianDateFormatException("Can not parse the value. Format is incorrect.");
        }
        #endregion

        #region static PersianDate ParseShortDate(string value)
        private static PersianDate ParseShortDate(string value)
        {
            if (value == string.Empty) return PersianDateConverter.ToPersianDate(DateTime.Now);

            if (value.Length > 10)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateTimeLength));

            string[] dateParts = value.Split("/".ToCharArray());

            if (dateParts.Length != 3)
                throw new InvalidPersianDateFormatException(PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.PersianDate_InvalidDateFormat));

            int day = int.Parse(dateParts[2]);
            int month = int.Parse(dateParts[1]);
            int year = int.Parse(dateParts[0]);

            return new PersianDate(year, month, day);
        }
        #endregion

        #endregion

        #endregion

        #region Classes

        #region public class PersianMonthNames
        /// <summary>
        /// كلاس مدیریت نام ماه های فارسی
        /// </summary>
        public class PersianMonthNames
        {

            #region Fields

            public string Farvardin = "فروردین";
            public string Ordibehesht = "ارديبهشت";
            public string Khordad = "خرداد";
            public string Tir = "تير";
            public string Mordad = "مرداد";
            public string Shahrivar = "شهریور";
            public string Mehr = "مهر";
            public string Aban = "آبان";
            public string Azar = "آذر";
            public string Day = "دی";
            public string Bahman = "بهمن";
            public string Esfand = "اسفند";

            private static readonly PersianMonthNames _PersianMonthNames;

            #endregion

            #region Ctor
            /// <summary>
            /// سازنده پیش فرض
            /// </summary>
            static PersianMonthNames()
            {
                _PersianMonthNames = new PersianMonthNames();
            }
            #endregion

            #region Indexers

            #region public static PersianMonthNames Default
            /// <summary>
            /// ماه پیش فرض تقویم
            /// </summary>
            public static PersianMonthNames Default
            {
                get { return _PersianMonthNames; }
            }
            #endregion

            #region public string this[int month]
            /// <summary>
            /// شمارشگر ماه تقویم
            /// </summary>
            /// <param name="month"></param>
            /// <returns></returns>
            public string this[int month]
            {
                get
                {
                    return GetName(month);
                }
            }
            #endregion

            #endregion

            #region Methods

            #region private string GetName(int monthNo)
            /// <summary>
            /// دریافت نام ماه تقویم فارسی بر اساس شماره ماه
            /// </summary>
            /// <param name="monthNo">شماره ماه</param>
            /// <returns>نام ماه</returns>
            private string GetName(int monthNo)
            {
                switch (monthNo)
                {
                    case 1: return Farvardin;
                    case 2: return Ordibehesht;
                    case 3: return Khordad;
                    case 4: return Tir;
                    case 5: return Mordad;
                    case 6: return Shahrivar;
                    case 7: return Mehr;
                    case 8: return Aban;
                    case 9: return Azar;
                    case 10: return Day;
                    case 11: return Bahman;
                    case 12: return Esfand;
                    default:
                        throw new ArgumentOutOfRangeException("Month value " + monthNo + " is out of range");
                }
            }
            #endregion

            #endregion

        }

        #endregion

        #region public class PersianWeekDayNames
        /// <summary>
        /// كلاس مدیریت متن نام روزهای هفته
        /// </summary>
        public class PersianWeekDayNames
        {

            #region Fields

            public string Shanbeh = "شنبه";
            public string Yekshanbeh = "یکشنبه";
            public string Doshanbeh = "دوشنبه";
            public string Seshanbeh = "ﺳﻪشنبه";
            public string Chaharshanbeh = "چهارشنبه";
            public string Panjshanbeh = "پنجشنبه";
            public string Jomeh = "جمعه";
            private static readonly PersianWeekDayNames _PersianWeekDayNames;

            #endregion

            #region Ctor
            /// <summary>
            /// سازنده پیش فرض كلاس
            /// </summary>
            static PersianWeekDayNames()
            {
                _PersianWeekDayNames = new PersianWeekDayNames();
            }
            #endregion

            #region Indexer

            #region public static PersianWeekDayNames Default
            /// <summary>
            /// شیء پیش فرض كلاس نام های فارسی
            /// </summary>
            public static PersianWeekDayNames Default
            {
                get { return _PersianWeekDayNames; }
            }
            #endregion

            #region public string this[int day]
            /// <summary>
            /// ارائه نام روز هفته بر اساس شماره روز
            /// </summary>
            /// <param name="day"></param>
            /// <returns></returns>
            public string this[int day]
            {
                get { return GetName(day); }
            }
            #endregion
            
            #endregion

            #region Methods

            private string GetName(int WeekDayNo)
            {
                switch (WeekDayNo)
                {
                    case 0:
                        return Shanbeh;

                    case 1:
                        return Yekshanbeh;

                    case 2:
                        return Doshanbeh;

                    case 3:
                        return Seshanbeh;

                    case 4:
                        return Chaharshanbeh;

                    case 5:
                        return Panjshanbeh;

                    case 6:
                        return Jomeh;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            #endregion
        }

        #endregion

        #region public class PersianWeekDayAbbr
        /// <summary>
        /// كلاس مدیریت نام كوتاه ایام هفته تقویم
        /// </summary>
        public class PersianWeekDayAbbr
        {

            #region Fields
            public string Shanbeh = "ش";
            public string Yekshanbeh = "ی";
            public string Doshanbeh = "د";
            public string Seshanbeh = "س";
            public string Chaharshanbeh = "چ";
            public string Panjshanbeh = "پ";
            public string Jomeh = "ج";
            private static readonly PersianWeekDayAbbr _PersianWeekDayAbbr;
            #endregion

            #region Ctor
            /// <summary>
            /// سازنده پیش فرض كلاس
            /// </summary>
            static PersianWeekDayAbbr()
            {
                _PersianWeekDayAbbr = new PersianWeekDayAbbr();
            }
            #endregion

            #region Indexer

            #region public static PersianWeekDayAbbr Default
            /// <summary>
            /// 
            /// </summary>
            public static PersianWeekDayAbbr Default
            {
                get { return _PersianWeekDayAbbr; }
            }
            #endregion

            #region public string this[int day]
            /// <summary>
            /// 
            /// </summary>
            /// <param name="day"></param>
            /// <returns></returns>
            public string this[int day]
            {
                get { return GetName(day); }
            }
            #endregion
            
            #endregion

            #region Methods

            #region private string GetName(int WeekDayNo)
            private string GetName(int WeekDayNo)
            {
                switch (WeekDayNo)
                {
                    case 0: return Shanbeh;
                    case 1: return Yekshanbeh; 
                    case 2: return Doshanbeh;
                    case 3: return Seshanbeh;
                    case 4: return Chaharshanbeh;
                    case 5: return Panjshanbeh;
                    case 6: return Jomeh;
                    default: throw new ArgumentOutOfRangeException("WeekDay number " + "is out of range");
                }
            }
            #endregion
            
            #endregion

        }
        #endregion

        #endregion

    }
}