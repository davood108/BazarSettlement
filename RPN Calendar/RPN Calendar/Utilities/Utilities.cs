namespace RPNCalendar.Utilities
{
    /// <summary>
    /// مجموعه ابزارهای فارسی
    /// </summary>
    internal static class PersianUtilities
    {

        #region Methods

        #region internal static System.String ConvertToDuplex(int Num)
        /// <summary>
        /// تبدیل عدد یك رقمی به عدد دو رقمی با صفر
        /// </summary>
        /// <param name="Num">عدد ماه یا روز</param>
        /// <returns></returns>
        internal static System.String ConvertToDuplex(int Num)
        {
            if (Num > 9) return Num.ToString();
            return "0" + Num;
        }
        #endregion

        #endregion
        
    }
}