﻿namespace RPNCalendar.Utilities
{
    /// <summary>
    /// تعیین كننده ایام هفته فارسی
    /// </summary>
    public enum PersianDayOfWeek
    {
        Saturday = 0,
        Sunday = 1,
        Monday = 2,
        Tuesday = 3,
        Wednesday = 4,
        Thursday = 5,
        Friday = 6
    }
}