using System.ComponentModel;
using System.Windows.Forms;

namespace RPNCalendar.UI.FAPopup
{
    [ToolboxItem(false)]
    public class PersianPopupContainer : FATopFormBase
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style = unchecked((int) 0x80000000);
                cp.ClassStyle |= 0x0800;
                return cp;
            }
        }
    }
}