using System;
using System.Collections;
using System.Security.Permissions;

namespace RPNCalendar.UI.FAPopup
{
    [UIPermission(SecurityAction.Assert, Window = UIPermissionWindow.AllWindows,
        Clipboard = UIPermissionClipboard.OwnClipboard)]
    [ReflectionPermission(SecurityAction.Assert, Flags = ReflectionPermissionFlag.AllFlags)]
    [SecurityPermission(SecurityAction.Assert,
        Flags =
            SecurityPermissionFlag.UnmanagedCode | SecurityPermissionFlag.ControlAppDomain |
            SecurityPermissionFlag.ControlThread)]
    internal class HookInfo
    {
        #region Fields

        private readonly ArrayList hookControllers;
        private readonly int threadId;
        public IntPtr getMessageHookHandle;
        public Hook getMessageHookProc;
        public bool inHook, inMouseHook;
        public IntPtr mouseHookHandle;
        public Hook mouseHookProc;
        public IntPtr wndHookHandle;
        public Hook wndHookProc;
        //HookManager manager;

        #endregion

        #region Ctor

        public HookInfo() //(HookManager manager)
        {
            //this.manager = manager;
            inMouseHook = false;
            inHook = false;
            wndHookHandle = getMessageHookHandle = mouseHookHandle = IntPtr.Zero;
            wndHookProc = mouseHookProc = getMessageHookProc = null;
            threadId = HookManager.GetCurrentThreadId();
            hookControllers = new ArrayList();
        }

        #endregion

        #region Props

        public ArrayList HookControllers
        {
            get { return hookControllers; }
        }

        public int ThreadId
        {
            get { return threadId; }
        }

        #endregion
    }
}