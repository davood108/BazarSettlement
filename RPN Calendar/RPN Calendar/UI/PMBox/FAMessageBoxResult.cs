﻿using System;
using System.Threading;
using RPNCalendar.Resource;

namespace RPNCalendar.UI.PMBox
{
    /// <summary>
    /// Results that DrawTab FAMessageBox control returns.
    /// </summary>
    public sealed class FAMessageBoxResult
    {
        #region Fields

        /// <summary>
        /// string representing an Abort
        /// </summary>
        public const string Abort = "ABORT";

        /// <summary>
        /// string representing DrawTab Cancel
        /// </summary>
        public const string Cancel = "CANCEL";

        /// <summary>
        /// String representing DrawTab Ignore
        /// </summary>
        public const string Ignore = "IGNORE";

        /// <summary>
        /// string representing DrawTab No
        /// </summary>
        public const string No = "NO";

        /// <summary>
        /// string representing an OK
        /// </summary>
        public const string Ok = "OK";

        /// <summary>
        /// string representing DrawTab Retry
        /// </summary>
        public const string Retry = "RETRY";

        /// <summary>
        /// string representing DrawTab Yes
        /// </summary>
        public const string Yes = "YES";

        #endregion

        #region Methods

        /// <summary>
        /// Use this method to get equivalants of each available responses returned by control.
        /// </summary>
        /// <param name="EnglishButtonText"></param>
        /// <returns></returns>
        internal static string GetFAMessageBoxButton(string EnglishButtonText)
        {
            switch (EnglishButtonText.ToUpper())
            {
                case "OK":
                    return
                        PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.MessageBox_Ok);

                case "CANCEL":
                    return
                        PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.MessageBox_Cancel);

                case "YES":
                    return
                        PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.MessageBox_Yes);

                case "NO":
                    return
                        PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.MessageBox_No);

                case "ABORT":
                    return
                        PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.MessageBox_Abort);

                case "RETRY":
                    return
                        PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.MessageBox_Retry);

                case "IGNORE":
                    return
                        PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.MessageBox_Ignore);

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
    }
}