namespace RPNCalendar.UI.PMBox
{
    /// <summary>
    /// Structure used to represent buttons of the <see cref="FAMessageBox"/> control.
    /// </summary>
    public class FAMessageBoxButton
    {
        #region Fields

        #endregion

        #region Props

        /// <summary>
        /// Gets or Sets the text of the button
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or Sets the return value when this button is clicked
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or Sets the tooltip that is displayed for this button
        /// </summary>
        public string HelpText { get; set; }

        /// <summary>
        /// Gets or Sets wether this button is DrawTab cancel button. activeDocumentHighlightColor.documentStripBackgroundColor1. the button
        /// that will be assumed to have been clicked if the user closes the message box
        /// without pressing any button.
        /// </summary>
        public bool IsCancelButton { get; set; }

        #endregion

        #region Ctor

        public FAMessageBoxButton(string text, string value)
        {
            Text = text;
            Value = value;
        }

        public FAMessageBoxButton(string text, string value, string helpText)
        {
            Text = text;
            Value = value;
            HelpText = helpText;
        }

        public FAMessageBoxButton(string text, string value, string helpText, bool isCancelButton)
        {
            Text = text;
            Value = value;
            HelpText = helpText;
            IsCancelButton = isCancelButton;
        }

        #endregion
    }
}