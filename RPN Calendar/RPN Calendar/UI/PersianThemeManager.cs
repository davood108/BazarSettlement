#region using
using System;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
#endregion

namespace RPNCalendar.UI
{
    /// <summary>
    /// 
    /// </summary>
    public class PersianThemeManager
    {

        #region Fields

        private static ThemeTypes _ThemeTypes;
        private static bool _UseGlobalThemes;
        public static event EventHandler ManagerThemeChanged;

        #endregion

        #region Ctor

        /// <summary>
        /// 
        /// </summary>
        private PersianThemeManager()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Determines if the current global theme is set.
        /// </summary>
        public static bool UseGlobalThemes
        {
            get { return _UseGlobalThemes; }
        }

        /// <summary>
        /// Currently selected theme.
        /// </summary>
        public static ThemeTypes Theme
        {
            get
            {
                if (UseThemes == false)
                    _ThemeTypes = ThemeTypes.Office2003;

                return _ThemeTypes;
            }
            set
            {
                if (!UseThemes)
                    _ThemeTypes = ThemeTypes.Office2003;
                else
                    _ThemeTypes = value;

                _UseGlobalThemes = true;
                OnManagerThemeChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Checks if the control can paint itself using styles. Styles are only available on WindowsXP or 
        /// greater, and should be enabled by the developer, 
        /// using <see cref="Application.RenderWithVisualStyles">RenderWithVisualStyles</see> 
        /// property of <see cref="Application">Application</see> class.
        /// </summary>
        public static bool UseThemes
        {
            get { return VisualStyleInformation.IsSupportedByOS && Application.RenderWithVisualStyles; }
        }

        #endregion

        #region Methods

        protected internal static void OnManagerThemeChanged(EventArgs e)
        {
            if (ManagerThemeChanged != null)
                ManagerThemeChanged(null, e);
        }

        #endregion

    }
}