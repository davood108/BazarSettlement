#region using

using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using RPNCalendar.UI.Design;
using RPNCalendar.UI.Drawing;

#endregion

namespace RPNCalendar.UI.BaseClasses
{
    /// <summary>
    /// Base class for all controls, which provides painting functionality bases on selected theme.
    /// </summary>
    [ToolboxItem(false)]
    public class BaseStyledControl : Control
    {
        #region Fields

        private static readonly FAPainterOffice2000 _PainterOffice2000;
        private static readonly FAPainterOffice2003 _PainterOffice2003;
        private static readonly FAPainterWindowsXP _PainterWinXP;
        private static readonly ToolStripProfessionalRenderer _Renderer;
        private int _LockUpdate;
        private ThemeTypes _ThemeTypes;

        #endregion

        #region Ctors

        #region static BaseStyledControl()

        /// <summary>
        /// Creates static painter objects for each of available Themes.
        /// </summary>
        static BaseStyledControl()
        {
            _PainterOffice2000 = new FAPainterOffice2000();
            _PainterOffice2003 = new FAPainterOffice2003();
            _PainterWinXP = new FAPainterWindowsXP();
            _Renderer = new ToolStripProfessionalRenderer();
        }

        #endregion

        #region public BaseStyledControl()

        /// <summary>
        /// Creates a new instance of BaseStyledControl class.
        /// </summary>
        public BaseStyledControl()
        {
            // Set painting style for better performance.
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.UserPaint, true);

            PersianThemeManager.ManagerThemeChanged += (OnInternalManagerThemeChanged);

            if (UseThemes) Office2003Colors.Default.Init();

            if (!DesignMode && PersianThemeManager.UseGlobalThemes)
                Theme = PersianThemeManager.Theme;
        }

        #endregion

        #endregion

        #region Events

        /// <summary>
        /// Fired when current theme changes.
        /// </summary>
        public event EventHandler ThemeChanged;

        #endregion

        #region Methods

        #region public void BeginUpdate()

        /// <summary>
        /// Locks the control for update.
        /// </summary>
        public void BeginUpdate()
        {
            _LockUpdate++;
        }

        #endregion

        #region public void EndUpdate()

        /// <summary>
        /// Removes a update lock from control.
        /// </summary>
        public void EndUpdate()
        {
            _LockUpdate--;
        }

        #endregion

        #region public void CancelUpdate()

        /// <summary>
        /// Cancels all previous locks on the control. Does NOT repaint the control.
        /// </summary>
        public void CancelUpdate()
        {
            _LockUpdate = 0;
        }

        #endregion

        #region public bool CanUpdate

        /// <summary>
        /// Decides if the user is updatable or in lock mode.
        /// </summary>
        [Browsable(false)]
        public bool CanUpdate
        {
            get { return _LockUpdate == 0; }
        }

        #endregion

        #region public void Repaint()

        /// <summary>
        /// Invalidate and repaints the control if it is not in lock mode.
        /// </summary>
        public void Repaint()
        {
            if (CanUpdate)
                Invalidate();
        }

        #endregion

        #region public IFAPainter Painter

        /// <summary>
        /// Painter object which helps control paint itself on the screen, based on the current selected theme.
        /// </summary>
        [Browsable(false)]
        public IFAPainter Painter
        {
            get
            {
                if (!UseThemes || Theme == ThemeTypes.Office2000)
                    return _PainterOffice2000;
                if (UseThemes && Theme == ThemeTypes.Office2003)
                    return _PainterOffice2003;
                if (UseThemes && Theme == ThemeTypes.WindowsXP)
                    return _PainterWinXP;
                return _PainterOffice2000;
            }
        }

        #endregion

        #region protected override void OnSystemColorsChanged(EventArgs e)

        protected override void OnSystemColorsChanged(EventArgs e)
        {
            base.OnSystemColorsChanged(e);
            UpdateRenderer();
            Invalidate();
        }

        #endregion

        #region protected virtual void OnThemeChanged(EventArgs e)

        protected virtual void OnThemeChanged(EventArgs e)
        {
            if (ThemeChanged != null) ThemeChanged(this, e);
            Repaint();
        }

        #endregion

        #region protected virtual void OnThemeChanged()

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnThemeChanged()
        {
            if (ThemeChanged != null)
                ThemeChanged(this, EventArgs.Empty);
            Repaint();
        }

        #endregion

        #region private void UpdateRenderer()

        /// <summary>
        /// 
        /// </summary>
        private void UpdateRenderer()
        {
            if (!UseThemes || Theme == ThemeTypes.Office2000)
                _Renderer.ColorTable.UseSystemColors = true;
            else
                _Renderer.ColorTable.UseSystemColors = false;
        }

        #endregion

        #region private void OnInternalManagerThemeChanged(object sender, EventArgs e)

        private void OnInternalManagerThemeChanged(object sender, EventArgs e)
        {
            Theme = PersianThemeManager.Theme;
        }

        #endregion

        #endregion

        #region Properties

        #region public object About

        /// <summary>
        /// Displays the about form of the control when in Design-Mode.
        /// </summary>
        [DesignOnly(true)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [ParenthesizePropertyName(true)]
        [Editor(typeof (AboutDialogEditor), typeof (UITypeEditor))]
        public object About
        {
            get { return null; }
        }

        #endregion

        #region internal static ProfessionalColorTable ColorTable

        [Browsable(false)]
        internal static ProfessionalColorTable ColorTable
        {
            get { return _Renderer.ColorTable; }
        }

        #endregion

        #region internal bool IsRightToLeft

        [Browsable(false)]
        internal bool IsRightToLeft
        {
            get { return RightToLeft == RightToLeft.Yes; }
        }

        #endregion

        #region internal static ToolStripProfessionalRenderer ToolStripRenderer

        [Browsable(false)]
        internal static ToolStripProfessionalRenderer ToolStripRenderer
        {
            get { return _Renderer; }
        }

        #endregion

        #region public bool UseThemes

        /// <summary>
        /// Checks if the control can paint itself using styles. 
        /// Styles are only available on WindowsXP or 
        /// greater, and should be enabled by the developer, 
        /// using <see cref="Application.RenderWithVisualStyles">RenderWithVisualStyles</see> property of 
        /// <see cref="Application">Application</see> class.
        /// </summary>
        [Browsable(false)]
        public bool UseThemes
        {
            get { return PersianThemeManager.UseThemes; }
        }

        #endregion

        #region public ThemeTypes Theme

        /// <summary>
        /// Currently selected theme.
        /// </summary>
        [DefaultValue(typeof (ThemeTypes), "Office2000")]
        public ThemeTypes Theme
        {
            get
            {
                if (UseThemes == false) _ThemeTypes = ThemeTypes.Office2000;
                return _ThemeTypes;
            }
            set
            {
                if (_ThemeTypes == value) return;
                if (!UseThemes) _ThemeTypes = ThemeTypes.Office2000;
                else _ThemeTypes = value;
                UpdateRenderer();
                OnThemeChanged(EventArgs.Empty);
            }
        }

        #endregion

        #endregion

        #region Overrided Methods

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (!DesignMode && Theme != PersianThemeManager.Theme && PersianThemeManager.UseGlobalThemes)
                Theme = PersianThemeManager.Theme;
        }

        #endregion
    }
}