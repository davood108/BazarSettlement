#region using

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using RPNCalendar.UI.Drawing;

#endregion

namespace RPNCalendar.UI.BaseClasses
{
    /// <summary>
    /// TextEdit control which emulates a textbox control. 
    /// </summary>
    [ToolboxItem(false)]
    public class TextEditBase : BaseControl
    {

        #region Fields

        #region internal RightToLeft RTL
        /// <summary>
        /// فیلد تعیین راست به چپ كلاس
        /// </summary>
        internal RightToLeft RTL;
        #endregion

        #region internal TextBox TextBox
        /// <summary>
        /// كنترل جعبه متن موجود در كلاس
        /// </summary>
        internal MaskedTextBox TextBox;
        #endregion
        
        #endregion

        #region Ctor
        /// <summary>
        /// Creates a new instance of TextEditBase class.
        /// </summary>
        public TextEditBase()
        {
            TextBox = new MaskedTextBox();
            TextBox.PromptChar = ' ';
            TextBox.HidePromptOnLeave = true;
            TextBox.InsertKeyMode = InsertKeyMode.Overwrite;

            // محل تعیین سایز پیش فرض كنترل
            Width = 100;
            Height = 20;

            // ToDo: محل تغییر راست به چپ كنترل جعبه متن
            RTL = RightToLeft.No;
            base.BackColor = SystemColors.Window;
            TextBox.RightToLeft = RTL;
            TextBox.BorderStyle = BorderStyle.None;
            TextBox.AutoSize = false;
            #region Add Event Handlers
            TextBox.MouseEnter += (TextBox_MouseEnter);
            TextBox.MouseLeave += (TextBox_MouseLeave);
            TextBox.GotFocus += (TextBox_GotFocus);
            TextBox.LostFocus += (TextBox_LostFocus);
            TextBox.SizeChanged += (TextBox_SizeChanged);
            TextBox.TextChanged += (TextBox_TextChanged);
            TextBox.MouseUp += (InvokeMouseUp);
            TextBox.MouseDown += (InvokeMouseDown);
            TextBox.MouseEnter += (InvokeMouseEnter);
            TextBox.MouseHover += (InvokeMouseHover);
            TextBox.MouseLeave += (InvokeMouseLeave);
            TextBox.MouseMove += (InvokeMouseMove);
            TextBox.KeyDown += (InvokeKeyDown);
            TextBox.KeyPress += (InvokeKeyPress);
            TextBox.KeyUp += (InvokeKeyUp);
            TextBox.Click += (InvokeClick);
            TextBox.DoubleClick += (InvokeDoubleClick);
            ThemeChanged += (OnThemeChanged);
            #endregion
            Controls.Add(TextBox);
        }
        #endregion

        #region Properties

        #region public bool Multiline

        /// <summary>
        /// Checks if the textbox control should be in multiline mode.
        /// </summary>
        [DefaultValue(false)]
        [RefreshProperties(RefreshProperties.All)]
        public bool Multiline
        {
            get { return TextBox.Multiline; }
            set
            {
                if (TextBox.Multiline == value) return;
                TextBox.Multiline = value;
                OnPropertyChanged("Multiline");
                if (value == false)
                {
                    Height = 20;
                    SetPosTextBox();
                }
            }
        }

        #endregion

        #region public override Color BackColor

        /// <summary>
        /// BackColor of the control.
        /// </summary>
        [DefaultValue(typeof (Color), "Window")]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                if (base.BackColor == value) return;
                base.BackColor = value;
                OnPropertyChanged("BackColor");
            }
        }

        #endregion

        #region public override RightToLeft RightToLeft

        /// <summary>
        /// RightToLeft state of the control.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [DefaultValue(RightToLeft.Yes)]
        [RefreshProperties(RefreshProperties.All)]
        public override RightToLeft RightToLeft
        {
            get { return RTL; }
            set
            {
                if (RTL == value) return;
                RTL = value;
                TextBox.RightToLeft = value;
                OnPropertyChanged("RightToLeft");
                OnRightToLeftChanged(EventArgs.Empty);
            }
        }

        #endregion

        #region public virtual string SelectionText

        /// <summary>
        /// Selection Text of the textbox control.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual string SelectionText
        {
            get { return TextBox.SelectedText; }
            set
            {
                if (TextBox.SelectedText == value) return;
                TextBox.SelectedText = value;
                OnPropertyChanged("SelectionText");
            }
        }

        #endregion

        #region public virtual int SelectionStart

        /// <summary>
        /// SelectionStart of the control.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual int SelectionStart
        {
            get { return TextBox.SelectionStart; }
            set { TextBox.SelectionStart = value; }
        }

        #endregion

        #region public override string Text

        /// <summary>
        /// Text of the control.
        /// </summary>
        [DefaultValue("")]
        public override string Text
        {
            get { return TextBox.Text; }
            set
            {
                if (Text == value) return;
                base.Text = value;
                TextBox.Text = value;
            }
        }

        #endregion

        #region public override Font Font

        /// <summary>
        /// Font of the control.
        /// </summary>
        public override Font Font
        {
            get { return base.Font; }
            set
            {
                if (base.Font == value) return;
                base.Font = value;
                TextBox.Font = value;
                OnPropertyChanged("Font");
            }
        }

        #endregion

        #region public virtual int SelectionLength

        /// <summary>
        /// Selection length of the control.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual int SelectionLength
        {
            get { return TextBox.SelectionLength; }
            set { TextBox.SelectionLength = value; }
        }

        #endregion

        #region public override bool IsDisabled

        /// <summary>
        /// Sets the control in disabled mode.
        /// </summary>
        [Category("Behavior"), DefaultValue(false)]
        [RefreshProperties(RefreshProperties.All)]
        public override bool IsDisabled
        {
            get { return base.IsDisabled; }
            set
            {
                if (base.IsDisabled == value) return;

                base.IsDisabled = value;
                if (value)
                {
                    TextBox.BackColor = SystemColors.Control;
                    TextBox.Enabled = false;
                }
                else
                {
                    if (!IsReadonly) TextBox.BackColor = BackColor;
                    TextBox.Enabled = true;
                }
                Repaint();
            }
        }

        #endregion

        #region public override bool IsReadonly

        /// <summary>
        /// Sets or Gets the control in Readonly mode.
        /// </summary>
        [Category("Behavior"), DefaultValue(false)]
        [RefreshProperties(RefreshProperties.All)]
        public override bool IsReadonly
        {
            get { return base.IsReadonly; }
            set
            {
                if (base.IsReadonly == value) return;

                base.IsReadonly = value;
                if (value)
                {
                    TextBox.BackColor = SystemColors.Control;
                    TextBox.ReadOnly = true;
                }
                else
                {
                    if (!IsDisabled) TextBox.BackColor = BackColor;
                    TextBox.ReadOnly = false;
                }
                Repaint();
            }
        }

        #endregion

        #region public virtual int MaxLength

        /// <summary>
        /// MaxLength of the TextBox control.
        /// </summary>
        [Category("Behavior"), DefaultValue(32767)]
        public virtual int MaxLength
        {
            get { return TextBox.MaxLength; }
            set { TextBox.MaxLength = value; }
        }

        #endregion

        #region public virtual bool HideSelection

        /// <summary>
        /// HideSelection of the TextBox control.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool HideSelection
        {
            get { return TextBox.HideSelection; }
            set { TextBox.HideSelection = value; }
        }

        #endregion

        #region public override Image BackgroundImage

        /// <summary>
        /// Background Image of the control.
        /// </summary>
        [Browsable(false)]
        public override Image BackgroundImage
        {
            get { return base.BackgroundImage; }
        }

        #endregion

        #region public override bool IsFocused

        /// <summary>
        /// Gets or Sets the control in Focused state.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public override bool IsFocused
        {
            get
            {
                if (!Focused) return TextBox.Focused;
                return true;
            }
            set
            {
                if (base.IsFocused == value) return;

                base.IsFocused = value;
                if (value) TextBox.Focus();
            }
        }

        #endregion

        #endregion

        #region Methods

        #region protected virtual Rectangle GetContentRect()

        protected virtual Rectangle GetContentRect()
        {
            return new Rectangle(2, 2, Width - 2, Height - 4);
        }

        #endregion

        #endregion

        #region Invoke Event Methods

        #region private void InvokeClick(object sender, EventArgs e)

        private void InvokeClick(object sender, EventArgs e)
        {
            OnClick(e);
        }

        #endregion

        #region private void InvokeDoubleClick(object sender, EventArgs e)

        private void InvokeDoubleClick(object sender, EventArgs e)
        {
            OnDoubleClick(e);
        }

        #endregion

        #region private void InvokeKeyDown(object sender, KeyEventArgs e)

        private void InvokeKeyDown(object sender, KeyEventArgs e)
        {
            OnKeyDown(e);
        }

        #endregion

        #region private void InvokeKeyPress(object sender, KeyPressEventArgs e)

        private void InvokeKeyPress(object sender, KeyPressEventArgs e)
        {
            OnKeyPress(e);
        }

        #endregion

        #region private void InvokeKeyUp(object sender, KeyEventArgs e)

        private void InvokeKeyUp(object sender, KeyEventArgs e)
        {
            OnKeyUp(e);
        }

        #endregion

        #region private void InvokeMouseDown(object sender, MouseEventArgs e)

        private void InvokeMouseDown(object sender, MouseEventArgs e)
        {
            OnMouseDown(e);
        }

        #endregion

        #region private void InvokeMouseEnter(object sender, EventArgs e)

        private void InvokeMouseEnter(object sender, EventArgs e)
        {
            OnMouseEnter(e);
        }

        #endregion

        #region private void InvokeMouseHover(object sender, EventArgs e)

        private void InvokeMouseHover(object sender, EventArgs e)
        {
            IsHot = true;
            OnMouseHover(e);
        }

        #endregion

        #region private void InvokeMouseLeave(object sender, EventArgs e)

        private void InvokeMouseLeave(object sender, EventArgs e)
        {
            IsHot = false;
            OnMouseLeave(e);
        }

        #endregion

        #region private void InvokeMouseMove(object sender, MouseEventArgs e)

        private void InvokeMouseMove(object sender, MouseEventArgs e)
        {
            OnMouseMove(e);
        }

        #endregion

        #region private void InvokeMouseUp(object sender, MouseEventArgs e)

        private void InvokeMouseUp(object sender, MouseEventArgs e)
        {
            OnMouseUp(e);
        }

        #endregion

        #endregion

        #region Paint Methods

        #region protected override void OnPaint(PaintEventArgs e)

        protected override void OnPaint(PaintEventArgs e)
        {
            var clipRect = new Rectangle(0, 0, Width, Height);
            var args = new PaintEventArgs(e.Graphics, clipRect);

            if (IsDisabled || IsReadonly)
                OnDrawDisabledBackground(args);
            if (UseThemes && Theme == ThemeTypes.WindowsXP)
                OnDrawXPTextBoxBorder(args);
            else if (UseThemes && Theme == ThemeTypes.Office2003)
                OnDrawOffice2003Border(args);
            else
                OnDrawNormalTextBoxBorder(args);
            OnDrawButtons(args);
            base.OnPaint(e);
        }

        #endregion

        #region protected virtual void OnDrawDisabledBackground(PaintEventArgs e)

        protected virtual void OnDrawDisabledBackground(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(SystemBrushes.Control, e.ClipRectangle);
        }

        #endregion

        #region protected virtual void OnDrawButtons(PaintEventArgs e)

        protected virtual void OnDrawButtons(PaintEventArgs e)
        {
        }

        #endregion

        #region private void OnDrawOffice2003Border(PaintEventArgs e)

        private void OnDrawOffice2003Border(PaintEventArgs e)
        {
            if (IsDisabled || IsReadonly)
            {
                e.Graphics.FillRectangle(SystemBrushes.Control, e.ClipRectangle);
                e.Graphics.DrawRectangle(SystemPens.ControlDark,
                                         new Rectangle(e.ClipRectangle.X, e.ClipRectangle.Y,
                                                       e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1));
            }
            else if ((IsHot || IsFocused) && !IsReadonly && !IsDisabled)
            {
                Color color = Office2003Colors.Default[Office2003Color.NavBarBackColor2];
                using (var pen = new Pen(color))
                {
                    e.Graphics.DrawRectangle(pen,
                                             new Rectangle(e.ClipRectangle.X, e.ClipRectangle.Y,
                                                           e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1));
                }
            }
        }

        #endregion

        #region private void OnDrawXPTextBoxBorder(PaintEventArgs e)

        private void OnDrawXPTextBoxBorder(PaintEventArgs e)
        {
            Rectangle clipRect = e.ClipRectangle;
            VisualStyleRenderer renderer = null;

            if (IsDisabled || IsReadonly)
                e.Graphics.FillRectangle(SystemBrushes.Control, clipRect);

            if (IsReadonly || IsDisabled)
                renderer = new VisualStyleRenderer(VisualStyleElement.TextBox.TextEdit.ReadOnly);
            else if (IsHot && !IsReadonly && !IsDisabled)
                renderer = new VisualStyleRenderer(VisualStyleElement.TextBox.TextEdit.Hot);
            else if (IsFocused && !IsReadonly && !IsDisabled)
                renderer = new VisualStyleRenderer(VisualStyleElement.TextBox.TextEdit.Focused);
            if (renderer == null)
                renderer = new VisualStyleRenderer(VisualStyleElement.TextBox.TextEdit.Normal);
            renderer.DrawBackground(e.Graphics, clipRect);
        }

        #endregion

        #region private void OnDrawNormalTextBoxBorder(PaintEventArgs e)

        private void OnDrawNormalTextBoxBorder(PaintEventArgs e)
        {
            Rectangle clipRect = e.ClipRectangle;
            Rectangle contentRect = GetContentRect();

            if (IsReadonly) IsHot = false;
            if (IsDisabled || IsReadonly)
                e.Graphics.FillRectangle(SystemBrushes.Control, contentRect);
            else
                using (var br = new SolidBrush(BackColor))
                {
                    e.Graphics.FillRectangle(br, contentRect);
                }
            ControlPaint.DrawBorder3D(e.Graphics, clipRect, Border3DStyle.Sunken);
        }

        #endregion

        #endregion

        #region Overrides

        #region protected override void OnRightToLeftChanged(EventArgs e)

        protected override void OnRightToLeftChanged(EventArgs e)
        {
            base.OnRightToLeftChanged(e);
            TextBox.RightToLeft = RTL;
            SetPosTextBox();
            Repaint();
        }

        #endregion

        #region protected override void OnBackColorChanged(EventArgs e)

        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            TextBox.BackColor = BackColor;
        }

        #endregion

        #region protected override void OnGotFocus(EventArgs e)

        protected override void OnGotFocus(EventArgs e)
        {
            TextBox.Focus();
            base.OnGotFocus(e);
        }

        #endregion

        #region protected override void OnSizeChanged(EventArgs e)

        protected override void OnSizeChanged(EventArgs e)
        {
            SetPosTextBox();
            base.OnSizeChanged(e);
        }

        #endregion

        #region protected override void OnTextChanged(EventArgs e)

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            TextBox.Text = Text;
        }

        #endregion

        #region protected virtual void SetPosTextBox(Rectangle content)

        protected virtual void SetPosTextBox(Rectangle content)
        {
            try
            {
                if (UseThemes && (Theme == ThemeTypes.WindowsXP || Theme == ThemeTypes.Office2003))
                {
                    TextBox.Top = (Height - TextBox.Height + 2)/2;
                    TextBox.Size = new Size(content.Width - 4, content.Height);
                    TextBox.Left = content.Left;
                }
                else
                {
                    TextBox.Top = (Height + 2 - TextBox.Height)/2;
                    TextBox.Size = new Size(content.Width - 3, content.Height);
                    TextBox.Left = content.Left;
                }
            }
            catch
            {
            }
            Repaint();
        }

        #endregion

        #region protected virtual void SetPosTextBox()

        protected virtual void SetPosTextBox()
        {
            SetPosTextBox(GetContentRect());
        }

        #endregion

        #region private void TextBox_GotFocus(object sender, EventArgs e)

        private void TextBox_GotFocus(object sender, EventArgs e)
        {
            Repaint();
            InvokeGotFocus(this, e);
        }

        #endregion

        #region private void TextBox_LostFocus(object sender, EventArgs e)

        private void TextBox_LostFocus(object sender, EventArgs e)
        {
            Repaint();
            InvokeLostFocus(this, e);
        }

        #endregion

        #region private void TextBox_MouseEnter(object sender, EventArgs e)

        private void TextBox_MouseEnter(object sender, EventArgs e)
        {
            IsHot = true;
            Repaint();
        }

        #endregion

        #region private void TextBox_MouseLeave(object sender, EventArgs e)

        private void TextBox_MouseLeave(object sender, EventArgs e)
        {
            IsHot = ClientRectangle.Contains(PointToClient(Cursor.Position));
            Repaint();
        }

        #endregion

        #region private void TextBox_SizeChanged(object sender, EventArgs e)

        private void TextBox_SizeChanged(object sender, EventArgs e)
        {
            SetPosTextBox();
            Repaint();
        }

        #endregion

        #region private void TextBox_TextChanged(object sender, EventArgs e)

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            Text = TextBox.Text;
        }

        #endregion

        #region private void OnThemeChanged(object sender, EventArgs e)

        private void OnThemeChanged(object sender, EventArgs e)
        {
            SetPosTextBox();
        }

        #endregion

        #endregion
        
    }
}