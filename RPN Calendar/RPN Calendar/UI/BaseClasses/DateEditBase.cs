using System.ComponentModel;

namespace RPNCalendar.UI.BaseClasses
{
    /// <summary>
    /// DateEditBase is the base class for all date picker controls, which provider formatting information.
    /// </summary>
    [ToolboxItem(false)]
    public class DateEditBase : TextEditBase
    {

        #region Fields

        private FormatInfoTypes _Format;

        #endregion

        #region Properties

        #region public FormatInfoTypes FormatInfo
        /// <summary>
        /// FormatInfoTypes instance, used to format date to string representation.
        /// </summary>
        [Description("FormatInfoTypes instance, used to format date to string representation.")]
        [DefaultValue(typeof (FormatInfoTypes), "ShortDate")]
        public FormatInfoTypes FormatInfo
        {
            get { return _Format; }
            set
            {
                if (_Format == value)
                    return;
                _Format = value;
                UpdateTextValue();
            }
        }
        #endregion

        #endregion

        #region Methods

        #region public virtual void UpdateTextValue()
        /// <summary>
        /// Updates text representation of the selected value.
        /// </summary>
        public virtual void UpdateTextValue()
        {

        }
        #endregion

        #region protected internal static string GetFormatByFormatInfo(FormatInfoTypes fi)
        /// <summary>
        /// Returns a string representation of the FormatInfoTypes.
        /// </summary>
        /// <param name="fi"></param>
        /// <returns></returns>
        protected internal static string GetFormatByFormatInfo(FormatInfoTypes fi)
        {
            switch (fi)
            {
                case FormatInfoTypes.DateShortTime:
                    return "g";
                case FormatInfoTypes.FullDateTime:
                    return "G";
                default:
                    return "d";
            }
        }
        #endregion

        #endregion

    }
}