#region using
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
#endregion

namespace RPNCalendar.UI.Controls
{

    /// <summary>
    /// PersianDatePickerConverterStrip is a wrapper for 
    /// <see cref="PersianDatePickerConverter"/> class, which
    /// makes it usable on <see cref="ToolStrip"/> Controls.
    /// </summary>
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
    public class PersianDatePickerConverterStrip : ToolStripControlHost
    {

        #region Ctor
        /// <summary>
        /// Creates a new instance of <see cref="PersianDatePickerConverterStrip"/>.
        /// </summary>
        public PersianDatePickerConverterStrip() : base(CreateControlInstance())
        {

        }
        #endregion

        #region Properties

        #region PersianDatePickerConverter PersianDatePickerConverter
        /// <summary>
        /// Represents the PersianDatePickerConverter control the will 
        /// be displayed by this tool strip.
        /// </summary>
        [Description("Represents the PersianDatePickerConverter control the will be displayed by this tool strip.")]
        public PersianDatePickerConverter PersianDatePickerConverter
        {
            get { return Control as PersianDatePickerConverter; }
        }
        #endregion

        #region override Color BackColor
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set { }
        }
        #endregion

        #region override string Text
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Text
        {
            get { return string.Empty; }
            set
            {
                value = string.Empty;
                base.Text = value;
            }
        }
        #endregion

        #endregion

        #region Methods

        #region private static Control CreateControlInstance()
        /// <summary>
        /// Create the actual control, this is static so it can be called from the constructor.
        /// </summary>
        /// <returns></returns>
        private static Control CreateControlInstance()
        {
            var dp = new PersianDatePickerConverter();

            if (PersianThemeManager.UseThemes)
            {
                dp.Theme = ThemeTypes.Office2003;
            }
            else
            {
                dp.Theme = ThemeTypes.Office2000;
            }

            return dp;
        }
        #endregion

        #region public bool ShouldSerializeText()
        /// <summary>
        /// Determines when to serialize Text value of the control.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeText()
        {
            return false;
        }
        #endregion
        
        #endregion

    }
}