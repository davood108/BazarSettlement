#region using
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using RPNCalendar.Resource;
using RPNCalendar.UI.Design;
using RPNCalendar.Utilities;
#endregion


namespace RPNCalendar.UI.Controls
{

    /// <summary>
    /// كلاس كمبو تقویم فارسی با قابلیت به تبدیل تقویم شمسی
    /// </summary>
    [ToolboxItem(true)]
    [DefaultEvent("SelectedDateTimeChanged")]
    [DefaultProperty("SelectedDateTime")]
    [Designer(typeof(PersianDatePickerDesigner))]
    public class PersianDatePickerConverter : PersianContainerComboBox
    {

        #region Fields
        private readonly Button _btnConvert;
        private readonly PersianMonthViewContainer _PMView;
        private Image _ButtonImage;
        private CalendarTypes _CalendarType;
        private int _DefaultConvertWidth = 20;
        private DateTime _SelectedDateTime;
        #endregion

        #region Events

        /// <summary>
        /// Fires when SelectedDateTime property is changed.
        /// </summary>
        public event EventHandler SelectedDateTimeChanged;

        /// <summary>
        /// Fires when SelectedDateTime property is changing. 
        /// Use properties of EventHandler class to specify validation of the SelectedDateTime.
        /// </summary>
        public event SelectedDateTimeChangingEventHandler SelectedDateTimeChanging;

        /// <summary>
        /// Fires when user clicks on _btnConvert button of the control.
        /// </summary>
        public event EventHandler ConvertButtonClicked;

        #endregion

        #region Ctor
        /// <summary>
        /// سازنده پیش فرض كلاس
        /// </summary>
        public PersianDatePickerConverter()
        {

            #region Set Event Handlers
            RightToLeftChanged += OnInternalRightToLeftChanged;
            ThemeChanged += OnInternalThemeChanged;
            PopupShowing += OnInternalPopupShowing;
            _PMView = new PersianMonthViewContainer(this);
            _PMView.MonthViewControl.SelectedDateTimeChanged +=
                OnMVSelectedDateTimeChanged;
            _PMView.MonthViewControl.ButtonClicked += OnMVButtonClicked;
            PersianLocalizeManager.LocalizerChanged += OnInternalLocalizerChanged;
            PersianThemeManager.ManagerThemeChanged +=
                OnInternalManagerThemeChanged;
            #endregion

            #region Make Convert Button
            _btnConvert = new Button();
            _btnConvert.Size = new Size(_DefaultConvertWidth, Height - 4);
            _btnConvert.Image = ButtonImage;
            _btnConvert.TabStop = false;
            _btnConvert.Click += OnConvertButtonClick;
            _btnConvert.GotFocus += OnConvertButtonFocus;
            Controls.Add(_btnConvert);
            #endregion

            FormatInfo = FormatInfoTypes.ShortDate;

            if (_PMView.MonthViewControl.DefaultCulture.Equals(
                _PMView.MonthViewControl.PersianCulture))
                _CalendarType = CalendarTypes.Persian;
            else
                _CalendarType = CalendarTypes.English;

            Text = PersianLocalizeManager.GetLocalizerByCulture(
                Thread.CurrentThread.CurrentUICulture).GetLocalizedString(
                    StringIDEnum.Validation_NullText);
            UpdateButtons();
        }
        #endregion

        #region Properties

        #region Boolean IsNull
        /// <summary>
        /// تعیین امكان ثبت حالت بدون مقدار برای كنترل
        /// </summary>
        [DefaultValue(true)]
        [Description("تعیین امكان ثبت حالت بدون مقدار برای كنترل")]
        public Boolean IsNull
        {
            get { return _PMView.MonthViewControl.IsNull; }
            set
            {
                _PMView.MonthViewControl.IsNull = value;
                UpdateTextValue();
            }
        }
        #endregion

        #region ScrollOptionTypes ScrollOption
        /// <summary>
        /// تعیین نوع عملكرد دكمه ی اسكرول موس
        /// </summary>
        [DefaultValue(typeof(ScrollOptionTypes), "Month")]
        [Description("تعیین نوع عملكرد دكمه ی اسكرول موس")]
        public ScrollOptionTypes ScrollOption
        {
            get { return _PMView.MonthViewControl.ScrollOption; }
            set { _PMView.MonthViewControl.ScrollOption = value; }
        }
        #endregion

        #region override Boolean IsHot
        /// <summary>
        /// تعیین وضعیت نمایش كنترل در هنگام حركت موس
        /// </summary>
        [DefaultValue(false)]
        [Browsable(false)]
        [Description("تعیین وضعیت نمایش كنترل در هنگام حركت موس")]
        public override Boolean IsHot
        {
            get
            {
                return base.IsHot ||
                    _btnConvert.Bounds.Contains(PointToClient(MousePosition));
            }
            set { base.IsHot = value; }
        }
        #endregion

        #region TextImageRelation TextImageRelation
        /// <summary>
        /// تعیین حالت قرارگیری متن با عكس دكمه تبدیل
        /// </summary>
        [Description("تعیین حالت قرارگیری متن با عكس دكمه تبدیل")]
        [DefaultValue(TextImageRelation.ImageBeforeText)]
        public TextImageRelation TextImageRelation
        {
            get { return _btnConvert.TextImageRelation; }
            set { _btnConvert.TextImageRelation = value; }
        }
        #endregion

        #region String ButtonText
        /// <summary>
        /// متن دكمه ی تبدیل
        /// </summary>
        [DefaultValue("")]
        [Description("متن دكمه ی تبدیل")]
        public String ButtonText
        {
            get
            {
                if (_btnConvert == null) return string.Empty;
                return _btnConvert.Text;
            }
            set
            {
                if (value == null) _btnConvert.Text = string.Empty;
                _btnConvert.Text = value;
            }
        }
        #endregion

        #region internal int ButtonWidth
        /// <summary>
        /// حرض دكمه ی تبدیل
        /// </summary>
        [Browsable(false)]
        [DefaultValue(20)]
        internal int ButtonWidth
        {
            get { return _DefaultConvertWidth; }
            set
            {
                _DefaultConvertWidth = value;
                UpdateButtons();
            }
        }
        #endregion

        #region Image ButtonImage
        /// <summary>
        /// تصویر دكمه ی تبدیل
        /// </summary>
        [Description("تصویر دكمه ی تبدیل")]
        [DefaultValue(null)]
        public Image ButtonImage
        {
            get { return _ButtonImage; }
            set
            {
                if (_ButtonImage == value) return;
                if (_btnConvert == null) return;
                if (value is Bitmap) ((Bitmap)value).MakeTransparent();
                _ButtonImage = value;
                _btnConvert.Image = value;
            }
        }
        #endregion

        #region CalendarTypes CalendarType
        /// <summary>
        /// نوع تقویم كنترل
        /// </summary>
        [DefaultValue(CalendarTypes.Persian)]
        [Browsable(false)]
        public CalendarTypes CalendarType
        {
            get { return _CalendarType; }
            set
            {
                if (_CalendarType == value)
                    return;

                _CalendarType = value;
            }
        }
        #endregion

        #region DateTime SelectedDateTime
        /// <summary>
        /// مقدار انتخاب شده تاریخ كنترل
        /// </summary>
        [Bindable(true)]
        [Localizable(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Description("مقدار انتخاب شده تاریخ كنترل")]
        public DateTime SelectedDateTime
        {
            get { return _SelectedDateTime; }
            set
            {
                if (_SelectedDateTime == value)
                    return;

                #region Validating
                var validateArgs = new ValueValidatingEventArgs(Text);
                OnValueValidating(validateArgs);
                if (validateArgs.HasError) return;
                #endregion

                DateTime oldValue = _SelectedDateTime;
                DateTime newValue = value;

                var changeArgs =
                    new SelectedDateTimeChangingEventArgs(oldValue, newValue);
                OnSelectedDateTimeChanging(changeArgs);

                #region Cancel Pressed
                if (changeArgs.Cancel)
                {
                    if (string.IsNullOrEmpty(changeArgs.Message))
                        Error.SetError(this, PersianLocalizeManager.GetLocalizerByCulture(
                            Thread.CurrentThread.CurrentUICulture).
                            GetLocalizedString(StringIDEnum.Validation_Cancel));
                    else
                        Error.SetError(this, changeArgs.Message);
                    return;
                }
                #endregion

                #region Handle Null Value
                if (!string.IsNullOrEmpty(changeArgs.Message))
                    Error.SetError(this, changeArgs.Message);
                else
                    Error.SetError(this, string.Empty);
                #endregion

                // No Errors , Proceed:
                _PMView.MonthViewControl.SelectedDateTime = changeArgs.NewValue;
            }
        }
        #endregion

        #endregion

        #region Event Handlers

        #region OnInternalManagerThemeChanged
        private void OnInternalManagerThemeChanged(object sender, EventArgs e)
        {
            Theme = PersianThemeManager.Theme;
        }
        #endregion

        #region OnInternalRightToLeftChanged
        private void OnInternalRightToLeftChanged(object sender, EventArgs e)
        {
            UpdateButtons();
        }
        #endregion

        #region OnMVSelectedDateTimeChanged
        private void OnMVSelectedDateTimeChanged(object sender, EventArgs e)
        {
            SetSelectedDateTime(_PMView.MonthViewControl.SelectedDateTime);
        }

        #endregion

        #region OnMVButtonClicked
        private void OnMVButtonClicked(object sender, CalendarButtonClickedEventArgs e)
        {
            HideDropDown();
        }
        #endregion

        #region OnConvertButtonFocus
        private void OnConvertButtonFocus(object sender, EventArgs e)
        {
            FocusNextControl();
        }
        #endregion

        #region OnInternalLocalizerChanged
        private void OnInternalLocalizerChanged(object sender, EventArgs e)
        {
            UpdateTextValue();
        }
        #endregion

        #region OnInternalPopupShowing
        private void OnInternalPopupShowing(object sender, EventArgs e)
        {
            _PMView.MonthViewControl.Theme = Theme;
            var args = new ValueValidatingEventArgs(Text);
            OnValueValidating(args);
        }
        #endregion

        #region OnInternalThemeChanged
        private void OnInternalThemeChanged(object sender, EventArgs e)
        {
            if (Theme == ThemeTypes.Office2000)
            {
                _btnConvert.FlatStyle = FlatStyle.Flat;
                _btnConvert.BackColor = SystemColors.Control;
            }
            else
            {
                _btnConvert.FlatStyle = FlatStyle.Standard;
                _btnConvert.UseVisualStyleBackColor = true;
            }
        }
        #endregion

        #region OnConvertButtonClick
        private void OnConvertButtonClick(object sender, EventArgs e)
        {
            OnConvertButtonClicked(EventArgs.Empty);

            #region Return If Null Passed
            if (Text == null || Text ==
                PersianLocalizeManager.GetLocalizerByCulture(
                Thread.CurrentThread.CurrentUICulture).GetLocalizedString(
                    StringIDEnum.Validation_NullText) || Text.Length == 0)
                return;
            #endregion

            #region Change Calendar Type
            if (CalendarType == CalendarTypes.Persian)
                CalendarType = CalendarTypes.English;
            else
                CalendarType = CalendarTypes.Persian;
            #endregion

            try
            {

                #region For Persian Calendar
                if (CalendarType == CalendarTypes.Persian)
                {
                    _PMView.MonthViewControl.DefaultCalendar =
                        _PMView.MonthViewControl.PersianCalendar;
                    _PMView.MonthViewControl.DefaultCulture =
                        _PMView.MonthViewControl.PersianCulture;
                    DateTime dt = SelectedDateTime;
                    Text = ((PersianDate)dt).ToString(GetFormatByFormatInfo(FormatInfo));
                }
                #endregion

                #region Other Cultures
                else
                {
                    _PMView.MonthViewControl.DefaultCalendar = 
                        _PMView.MonthViewControl.InvariantCalendar;
                    _PMView.MonthViewControl.DefaultCulture = 
                        _PMView.MonthViewControl.InvariantCulture;
                    DateTime dt = SelectedDateTime;
                    Text = dt.ToString(GetFormatByFormatInfo(FormatInfo), 
                        _PMView.MonthViewControl.DefaultCulture);
                }
                #endregion

            }
            catch (FormatException)
            {
                Text =
                    PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(
                        StringIDEnum.Validation_NullText);
                _PMView.MonthViewControl.SetNoneDay();
            }
        }
        #endregion

        #region OnSelectedDateTimeChanging
        protected virtual void OnSelectedDateTimeChanging
            (SelectedDateTimeChangingEventArgs e)
        {
            e.Cancel = false;
            if (SelectedDateTimeChanging != null)
                SelectedDateTimeChanging(this, e);
        }
        #endregion

        #region OnConvertButtonClicked
        protected virtual void OnConvertButtonClicked(EventArgs e)
        {
            if (ConvertButtonClicked != null)
                ConvertButtonClicked(this, e);
        }
        #endregion

        #region OnBindingPopupControl
        protected override void OnBindingPopupControl(BindPopupControlEventArgs e)
        {
            e.BindedControl = _PMView;
            base.OnBindingPopupControl(e);
        }
        #endregion

        #region OnSelectedDateTimeChanged
        protected virtual void OnSelectedDateTimeChanged(EventArgs e)
        {
            if (SelectedDateTimeChanged != null)
                SelectedDateTimeChanged(this, e);
        }
        #endregion

        #region OnResize
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            UpdateButtons();
        }
        #endregion

        #region OnValidating
        protected override void OnValidating(CancelEventArgs e)
        {
            var args = new ValueValidatingEventArgs(Text);
            OnValueValidating(args);
            e.Cancel = args.HasError;

            base.OnValidating(e);
        }
        #endregion

        #region OnValueValidating
        protected override void OnValueValidating(ValueValidatingEventArgs e)
        {
            try
            {
                string txt = e.Value.ToString();
                if (string.IsNullOrEmpty(txt) || txt ==
                    PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(
                        StringIDEnum.Validation_NullText))
                {
                    e.HasError = false;
                }
                else if (_PMView.MonthViewControl.DefaultCulture.Equals(
                    _PMView.MonthViewControl.PersianCulture))
                {
                    PersianDate pd = 
                        PersianDate.Parse(txt, GetFormatByFormatInfo(FormatInfo));
                    e.HasError = false;
                    _PMView.MonthViewControl.IsNull = false;
                    _PMView.MonthViewControl.SelectedDateTime = pd;
                }
                else if (_PMView.MonthViewControl.DefaultCulture.Equals(
                    _PMView.MonthViewControl.InvariantCulture))
                {
                    DateTime dt = DateTime.Parse(txt);
                    e.HasError = false;
                    _PMView.MonthViewControl.IsNull = false;
                    _PMView.MonthViewControl.SelectedDateTime = dt;
                }
            }
            catch (Exception)
            {
                e.HasError = true;
                _PMView.MonthViewControl.IsNull = true;
            }
        }
        #endregion

        #endregion

        #region Methods

        #region void UpdateButtons()
        private void UpdateButtons()
        {
            SetPosTextBox();

            Rectangle r2 = GetContentRect();

            if (_btnConvert == null) return;

            _btnConvert.Width = _DefaultConvertWidth;
            _btnConvert.Height = Height - 4;
            _btnConvert.BringToFront();

            if (!IsRightToLeft)
                _btnConvert.Location = new Point(r2.Left - 2, (Height - _btnConvert.Height) / 2);
            else
                _btnConvert.Location =
                    new Point(r2.Width + 2, (Height - _btnConvert.Height) / 2);
        }
        #endregion

        #region void SetSelectedDateTime(DateTime dt)
        private void SetSelectedDateTime(DateTime dt)
        {
            DateTime oldValue = _SelectedDateTime;
            DateTime newValue = dt;

            var changeArgs = new SelectedDateTimeChangingEventArgs(oldValue, newValue);
            OnSelectedDateTimeChanging(changeArgs);

            if (changeArgs.Cancel)
            {
                if (string.IsNullOrEmpty(changeArgs.Message))
                    Error.SetError(this,
                        PersianLocalizeManager.GetLocalizerByCulture(
                        Thread.CurrentThread.CurrentUICulture).
                        GetLocalizedString(StringIDEnum.Validation_Cancel));
                else
                    Error.SetError(this, changeArgs.Message);
                return;
            }

            if (!string.IsNullOrEmpty(changeArgs.Message))
                Error.SetError(this, changeArgs.Message);
            else
                Error.SetError(this, string.Empty);

            _SelectedDateTime = dt;
            OnSelectedDateTimeChanged(EventArgs.Empty);

            UpdateTextValue();
        }
        #endregion

        #region protected override void SetPosTextBox(Rectangle content)
        protected override void SetPosTextBox(Rectangle content)
        {
            try
            {
                if (UseThemes && (Theme == ThemeTypes.WindowsXP ||
                    Theme == ThemeTypes.Office2003))
                {
                    TextBox.Top = (Height - TextBox.Height + 2) / 2;
                    TextBox.Size =
                        new Size(content.Width - 4 - _DefaultConvertWidth, content.Height);
                    TextBox.Left = content.Left;
                }
                else
                {
                    TextBox.Top = (Height + 2 - TextBox.Height) / 2;
                    TextBox.Size =
                        new Size(content.Width - 2 - _DefaultConvertWidth, content.Height);
                    TextBox.Left = content.Left;
                }
            }
            catch { }

            if (!IsRightToLeft) TextBox.Left += _DefaultConvertWidth;
            Repaint();
        }
        #endregion

        #region public override void UpdateTextValue()
        /// <summary>
        /// Updates text representation of the selected value
        /// </summary>
        public override void UpdateTextValue()
        {
            if (_PMView.MonthViewControl.IsNull)
            {
                Text =
                    PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(
                        StringIDEnum.Validation_NullText);
            }
            else
            {
                if (_PMView.MonthViewControl.DefaultCulture.Equals(
                    _PMView.MonthViewControl.PersianCulture))
                    Text = ((PersianDate)SelectedDateTime).ToString(
                        GetFormatByFormatInfo(FormatInfo));
                else
                    Text = SelectedDateTime.ToString(GetFormatByFormatInfo(FormatInfo),
                        _PMView.MonthViewControl.DefaultCulture);
            }
        }
        #endregion

        #region ShouldSerialize and Reset

        #region bool ShouldSerializeSelectedDateTime()
        /// <summary>
        /// Decides to serialize SelectedDateTime or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeSelectedDateTime()
        {
            return SelectedDateTime != DateTime.MinValue;
        }
        #endregion

        #region bool ShouldSerializeCalendarType()
        /// <summary>
        /// Decides to serialize CalendarType or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeCalendarType()
        {
            return false;
        }
        #endregion

        #region bool ShouldSerializeButtonImage()
        /// <summary>
        /// Decides to serialize ButtonImage or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeButtonImage()
        {
            return ButtonImage != null;
        }
        #endregion

        #region void ResetButtonImage()
        /// <summary>
        /// Resets ButtonImage to default value.
        /// </summary>
        public void ResetButtonImage()
        {
            ButtonImage = null;
        }
        #endregion

        #region void ResetSelectedDateTime()
        /// <summary>
        /// Rests SelectedDateTime to default value.
        /// </summary>
        public void ResetSelectedDateTime()
        {
            _SelectedDateTime = DateTime.MinValue;
            IsNull = true;
        }
        #endregion

        #endregion

        #endregion
        
    }
}