#region using
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
#endregion

namespace RPNCalendar.UI.Controls
{
    /// <summary>
    /// PersianMonthViewStrip is a wrapper for 
    /// <see cref="Controls.PersianMonthView"/> class, 
    /// which makes it usable on <see cref="ToolStrip"/> Controls.
    /// </summary>
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
    public class PersianMonthViewStrip : ToolStripControlHost
    {

        #region Ctor
        /// <summary>
        /// Creates a new instance of <see cref="PersianMonthViewStrip"/>.
        /// </summary>
        public PersianMonthViewStrip()
            : base(CreateControlInstance())
        {
        }
        #endregion

        #region Properties

        #region PersianMonthView PersianMonthView
        /// <summary>
        /// Returns the <see cref="PersianMonthView"/> instance the control is hosting.
        /// </summary>
        [Description("Represents a PersianMonthView control that displayed by this tool strip.")]
        public PersianMonthView PersianMonthView
        {
            get { return Control as PersianMonthView; }
        }
        #endregion

        #region override Color BackColor
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set { }
        }
        #endregion

        #region override string Text
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Text
        {
            get { return string.Empty; }
            set
            {
                value = string.Empty;
                base.Text = value;
            }
        }
        #endregion

        #endregion

        #region Methods

        #region private static Control CreateControlInstance()
        /// <summary>
        /// Create the actual control, this is static so it can be called from the constructor.
        /// </summary>
        /// <returns></returns>
        private static Control CreateControlInstance()
        {
            var mv = new PersianMonthView(false);
            if (PersianThemeManager.UseThemes)
                mv.Theme = ThemeTypes.Office2003;
            else
                mv.Theme = ThemeTypes.Office2000;
            return mv;
        }
        #endregion

        #region public bool ShouldSerializeText()
        /// <summary>
        /// Determines when to serialize Text value of the control.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeText()
        {
            return false;
        }
        #endregion
        
        #endregion

    }
}