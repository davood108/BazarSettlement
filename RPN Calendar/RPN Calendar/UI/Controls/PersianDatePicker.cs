#region using
using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using RPNCalendar.Resource;
using RPNCalendar.UI.Design;
using RPNCalendar.Utilities;
#endregion

namespace RPNCalendar.UI.Controls
{

    /// <summary>
    /// كنترل تقویم پارسی رایان پرتونگار
    /// </summary>
    [ToolboxItem(true)]
    [DefaultEvent("SelectedDateTimeChanged")]
    [DefaultProperty("SelectedDateTime")]
    [Designer(typeof(PersianDatePickerDesigner))]
    [DefaultBindingProperty("SelectedDateTime")]
    [Description("كنترل تقویم پارسی رایان پرتونگار")]
    public class PersianDatePicker : PersianContainerComboBox
    {

        #region Fields

        #region internal PersianMonthViewContainer _PersianMonthViewContainer
        /// <summary>
        /// نمونه كلاس نمایش تقویم ماهیانه فارسی
        /// </summary>
        internal PersianMonthViewContainer _PersianMonthViewContainer;
        #endregion

        #region DateTime _SelectedDateTime
        /// <summary>
        /// فیلد تاریخ انتخاب شده توسط كاربر
        /// </summary>
        private DateTime _SelectedDateTime;
        #endregion

        #region CultureName _ControlCulture
        /// <summary>
        /// نوع فرهنگ كنترل
        /// </summary>
        private CultureName _ControlCulture;
        #endregion

        #endregion

        #region Events

        /// <summary>
        /// Fires when SelectedDateTime property of the control changes.
        /// </summary>
        public event EventHandler SelectedDateTimeChanged;

        /// <summary>
        /// Fires when SelectedDateTime property of the control is changing.
        /// </summary>
        public event SelectedDateTimeChangingEventHandler SelectedDateTimeChanging;

        #endregion

        #region Ctor
        /// <summary>
        /// Creates a new instance of PersianDatePicker Class.
        /// </summary>
        public PersianDatePicker()
        {
            ControlCulture = CultureName.Persian;
            Theme = ThemeTypes.Office2003;
            _PersianMonthViewContainer = new PersianMonthViewContainer(this);

            #region Add Event Handlers
            RightToLeftChanged += OnInternalRightToLeftChanged;
            _PersianMonthViewContainer.MonthViewControl.SelectedDateTimeChanged += OnMVSelectedDateTimeChanged;
            _PersianMonthViewContainer.MonthViewControl.ButtonClicked += OnMVButtonClicked;
            PersianLocalizeManager.LocalizerChanged += OnInternalLocalizerChanged;
            PopupShowing += OnInternalPopupShowing;
            TextBox.PreviewKeyDown += (TextBox_PreviewKeyDown);
            TextBox.KeyPress += (TextBox_KeyPress);
            #endregion
            
            // تنظیم مقدار پیش فرض جعبه متن به مقدار تهی
            TextBox.Text = PersianLocalizeManager.GetLocalizerByCulture(
                Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.Validation_NullText);
            FormatInfo = FormatInfoTypes.ShortDate;
        }

        #endregion

        #region Properties

        #region bool IsNull
        /// <summary>
        /// تعیین تهی بودن یا نبودن مقدار تاریخ انتخاب شده
        /// </summary>
        [DefaultValue(true)]
        [Description("تعیین تهی بودن یا نبودن مقدار تاریخ انتخاب شده")]
        public bool IsNull
        {
            get { return _PersianMonthViewContainer.MonthViewControl.IsNull; }
            set
            {
                _PersianMonthViewContainer.MonthViewControl.IsNull = value;
                UpdateTextValue();
            }
        }
        #endregion

        #region ScrollOptionTypes ScrollOption
        /// <summary>
        /// تعیین نحوه جابجایی و رفتار كنترل با اسكرول موس
        /// </summary>
        [DefaultValue(typeof(ScrollOptionTypes), "Month")]
        [Description("تعیین نحوه جابجایی و رفتار كنترل با اسكرول موس")]
        public ScrollOptionTypes ScrollOption
        {
            get { return _PersianMonthViewContainer.MonthViewControl.ScrollOption; }
            set { _PersianMonthViewContainer.MonthViewControl.ScrollOption = value; }
        }
        #endregion

        #region DateTime SelectedDateTime
        /// <summary>
        /// تاریخ انتخاب شده توسط كاربر
        /// </summary>
        [Bindable(true)]
        [Localizable(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Description("تاریخ انتخاب شده توسط كاربر")]
        public DateTime SelectedDateTime
        {
            get { return _SelectedDateTime; }
            set
            {
                if (_SelectedDateTime == value) return;

                #region Validating
                ValueValidatingEventArgs validateArgs = 
                    new ValueValidatingEventArgs(TextBox.Text);
                OnValueValidating(validateArgs);
                if (validateArgs.HasError) return;
                #endregion
                
                DateTime oldValue = _SelectedDateTime;
                DateTime newValue = value;
                SelectedDateTimeChangingEventArgs changeArgs = 
                    new SelectedDateTimeChangingEventArgs(oldValue, newValue);
                OnSelectedDateTimeChanging(changeArgs);

                #region For Canceling
                if (changeArgs.Cancel)
                {
                    if (String.IsNullOrEmpty(changeArgs.Message))
                        Error.SetError(this, PersianLocalizeManager.GetLocalizerByCulture(
                            Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.Validation_Cancel));
                    else
                        Error.SetError(this, changeArgs.Message);
                    return;
                }
                #endregion

                #region Set Error
                if (!String.IsNullOrEmpty(changeArgs.Message))
                    Error.SetError(this, changeArgs.Message);
                else
                    Error.SetError(this, string.Empty);
                #endregion
                
                _PersianMonthViewContainer.MonthViewControl.SelectedDateTime = 
                    changeArgs.NewValue;
            }
        }
        #endregion

        #region CultureName ControlCulture
        /// <summary>
        /// نوع فرهنگ كنترل
        /// </summary>
        [Bindable(true)]
        [Localizable(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Description("نوع فرهنگ كنترل")]
        public CultureName ControlCulture
        {
            get { return _ControlCulture; }
            set
            {
                if (value == CultureName.Persian)
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("fa-IR");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("fa-IR");
                    TextBox.Mask = "####/##/##";
                }
                else
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                    TextBox.Mask = "####/##/##";
                }
                _ControlCulture = value;
            }
        }
        #endregion

        #endregion

        #region Event Handlers

        #region OnInternalLocalizerChanged
        private void OnInternalLocalizerChanged(object sender, EventArgs e)
        {
            UpdateTextValue();
        }
        #endregion

        #region OnInternalRightToLeftChanged
        private void OnInternalRightToLeftChanged(object sender, EventArgs e)
        {
            SetPosTextBox();
        }
        #endregion

        #region OnInternalPopupShowing
        private void OnInternalPopupShowing(object sender, EventArgs e)
        {
            _PersianMonthViewContainer.MonthViewControl.Theme = Theme;
            if (!String.IsNullOrEmpty(TextBox.Text) || TextBox.Text == "    /  /")
            {
                var args = new ValueValidatingEventArgs(TextBox.Text);
                OnValueValidating(args);    
            }
        }
        #endregion

        #region OnBindingPopupControl
        protected override void OnBindingPopupControl(BindPopupControlEventArgs e)
        {
            e.BindedControl = _PersianMonthViewContainer;
            base.OnBindingPopupControl(e);
        }
        #endregion

        #region OnSelectedDateTimeChanging
        protected virtual void OnSelectedDateTimeChanging(SelectedDateTimeChangingEventArgs e)
        {
            e.Cancel = false;
            if (SelectedDateTimeChanging != null)
                SelectedDateTimeChanging(this, e);
        }
        #endregion

        #region OnSelectedDateTimeChanged
        protected virtual void OnSelectedDateTimeChanged(EventArgs e)
        {
            if (SelectedDateTimeChanged != null)
                SelectedDateTimeChanged(this, e);
        }
        #endregion

        #region OnMVSelectedDateTimeChanged
        private void OnMVSelectedDateTimeChanged(object sender, EventArgs e)
        {
            SetSelectedDateTime(_PersianMonthViewContainer.MonthViewControl.SelectedDateTime);
        }
        #endregion

        #region OnMVButtonClicked
        private void OnMVButtonClicked(object sender, CalendarButtonClickedEventArgs e)
        {
            HideDropDown();
        }
        #endregion

        #region TextBox_PreviewKeyDown
        void TextBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            Int32 Position = TextBox.SelectionStart;
           
            #region Up Key Pressed
            if (e.KeyData.ToString() == "Up" && TextBox.Text != "    /  /")
            {
                if (Position < 4) SelectedDateTime = SelectedDateTime.AddYears(1);
                else if (Position < 8) SelectedDateTime = SelectedDateTime.AddMonths(1);
                else SelectedDateTime = SelectedDateTime.AddDays(1);
                UpdateTextValue();
            }
            #endregion

            #region Down Key Pressed
            else if (e.KeyData.ToString() == "Down" && TextBox.Text != "    /  /")
            {
                if (Position < 4) SelectedDateTime = SelectedDateTime.AddYears(-1);
                else if (Position < 8) SelectedDateTime = SelectedDateTime.AddMonths(-1);
                else SelectedDateTime = SelectedDateTime.AddDays(-1);
                UpdateTextValue();
            }
            #endregion

            #region Up Or Down Key With Null Data
            else if ((e.KeyData.ToString() == "Up" || e.KeyData.ToString() == "Down") && TextBox.Text == "    /  /")
                SelectedDateTime = DateTime.Now;
            #endregion

            TextBox.SelectionStart = Position;
        }
        #endregion

        #region TextBox_KeyPress
        void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Int32 Position = TextBox.SelectionStart;
            if (Position > 9) return;
            String NewDateText = TextBox.Text;
            try
            {
            NewDateText = NewDateText.Remove(Position, 1);
            NewDateText = NewDateText.Insert(Position, e.KeyChar.ToString());
            }
            catch(Exception){}
            try
            {
                SelectedDateTime = PersianDateConverter.ToGregorianDateTime(PersianDate.Parse(NewDateText));
            }
            catch (Exception)
            {
                e.Handled = true;
            }
            finally
            {
                TextBox.SelectionStart = Position;
            }
        }
        #endregion

        #endregion

        #region Methods

        #region private void SetSelectedDateTime(DateTime dt)
        /// <summary>
        /// تنظیم مقدار تاریخ بر اساس انتخاب كاربر از جدول ماه ها
        /// </summary>
        /// <param name="RecievedDateTime"></param>
        private void SetSelectedDateTime(DateTime RecievedDateTime)
        {
            DateTime oldValue = _SelectedDateTime;
            DateTime newValue = RecievedDateTime;

            SelectedDateTimeChangingEventArgs changeArgs =
                new SelectedDateTimeChangingEventArgs(oldValue, newValue);
            OnSelectedDateTimeChanging(changeArgs);

            #region Cancel Select New Date
            if (changeArgs.Cancel)
            {
                if (String.IsNullOrEmpty(changeArgs.Message))
                    Error.SetError(this, PersianLocalizeManager.GetLocalizerByCulture(
                        Thread.CurrentThread.CurrentUICulture).GetLocalizedString(StringIDEnum.Validation_Cancel));
                else Error.SetError(this, changeArgs.Message);
                return;
            }
            #endregion

            #region Invalid Date Selected
            if (!String.IsNullOrEmpty(changeArgs.Message))
                Error.SetError(this, changeArgs.Message);
            else
                Error.SetError(this, String.Empty);
            #endregion

            changeArgs = new SelectedDateTimeChangingEventArgs(oldValue, newValue);
            OnSelectedDateTimeChanging(changeArgs);
            _SelectedDateTime = changeArgs.NewValue;
            OnSelectedDateTimeChanged(EventArgs.Empty);

            UpdateTextValue();
        }
        #endregion

        #region public bool ShouldSerializeSelectedDateTime()
        /// <summary>
        /// Decides to serialize the SelectedDateTime property or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeSelectedDateTime()
        {
            return SelectedDateTime != DateTime.MinValue;
        }
        #endregion

        #region public void ResetSelectedDateTime()
        /// <summary>
        /// Rests SelectedDateTime to default value.
        /// </summary>
        public void ResetSelectedDateTime()
        {
            _SelectedDateTime = DateTime.MinValue;
            IsNull = true;
        }
        #endregion

        #endregion

        #region Overrided Methods

        #region protected override void OnValidating(CancelEventArgs e)
        protected override void OnValidating(CancelEventArgs e)
        {
            ValueValidatingEventArgs args = new ValueValidatingEventArgs(TextBox.Text);
            OnValueValidating(args);
            e.Cancel = args.HasError;

            base.OnValidating(e);
        }
        #endregion

        #region protected override void OnValueValidating(ValueValidatingEventArgs e)
        protected override void OnValueValidating(ValueValidatingEventArgs e)
        {
            try
            {
                String ValidatedText = e.Value.ToString();

                #region Selected Date Is Null Or Empty
                if (String.IsNullOrEmpty(ValidatedText) ||
                    ValidatedText ==
                    PersianLocalizeManager.GetLocalizerByCulture(Thread.CurrentThread.CurrentUICulture).GetLocalizedString(
                        StringIDEnum.Validation_NullText))
                    e.HasError = false;
                #endregion

                #region For Persian Culture
                else if (_PersianMonthViewContainer.MonthViewControl.DefaultCulture.Equals(
                    _PersianMonthViewContainer.MonthViewControl.PersianCulture))
                {
                    PersianDate pd = PersianDate.Parse(ValidatedText, GetFormatByFormatInfo(FormatInfo));
                    e.HasError = false;
                    _PersianMonthViewContainer.MonthViewControl.IsNull = false;
                    _PersianMonthViewContainer.MonthViewControl.SelectedDateTime = pd;
                }
                #endregion

                #region For English Culture
                else if (_PersianMonthViewContainer.MonthViewControl.DefaultCulture.Equals(
                    _PersianMonthViewContainer.MonthViewControl.InvariantCulture))
                {
                    DateTime dt = DateTime.Parse(ValidatedText);
                    e.HasError = false;
                    _PersianMonthViewContainer.MonthViewControl.IsNull = false;
                    _PersianMonthViewContainer.MonthViewControl.SelectedDateTime = dt;
                }
                #endregion

            }
            catch (Exception)
            {
                _PersianMonthViewContainer.MonthViewControl.SelectedDateTime = DateTime.Now;
            }
        }
        #endregion

        #region public override void UpdateTextValue()
        /// <summary>
        /// تابع به روز رسانی جعبه متن بر اساس تاریخ انتخاب شده
        /// </summary>
        public override void UpdateTextValue()
        {

            #region For Null Selection
            if (_PersianMonthViewContainer.MonthViewControl.IsNull)
                TextBox.Text = PersianLocalizeManager.GetLocalizerByCulture(
                    Thread.CurrentThread.CurrentUICulture).GetLocalizedString(
                    StringIDEnum.Validation_NullText);
            #endregion

            else
            {

                #region For Persian Date
                if (_PersianMonthViewContainer.MonthViewControl.DefaultCulture.Equals(
                    _PersianMonthViewContainer.MonthViewControl.PersianCulture))
                    TextBox.Text = ((PersianDate)SelectedDateTime).ToString(GetFormatByFormatInfo(FormatInfo));
                #endregion

                #region For Other Cultures Date
                else
                    TextBox.Text = SelectedDateTime.ToString(GetFormatByFormatInfo(FormatInfo),
                        _PersianMonthViewContainer.MonthViewControl.DefaultCulture);
                #endregion

            }
        }
        #endregion

        #endregion

    }
}