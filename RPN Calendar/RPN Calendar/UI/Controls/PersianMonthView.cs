﻿#region using
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Text;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using RPNCalendar.Resource;
using RPNCalendar.UI.BaseClasses;
using RPNCalendar.UI.Design;
using RPNCalendar.Utilities;
using PersianCalendar = RPNCalendar.Utilities.PersianCalendar;
#endregion

namespace RPNCalendar.UI.Controls
{
    /// <summary>
    /// MonthView control is a calendar control that displays days of a month in 
    /// a view, and user can select dates in various formats.
    /// This control currently supports three cultures and calendars, both RTL and 
    /// LTR rendering and displaying numeric values in correct localized format.
    /// </summary>
    [ToolboxItem(true)]
    [Designer(typeof(PersianMonthViewDesigner))]
    [DefaultEvent("SelectedDateTimeChanged")]
    [DefaultProperty("SelectedDateTime")]
    [ToolboxBitmap(typeof(PersianMonthView))]
    public class PersianMonthView : BaseStyledControl
    {

        #region Constants Fields

        private const int DEF_ARROW_SIZE = 3;
        private const int DEF_BUTTON_HEIGHT = 23;
        private const int DEF_BUTTON_WIDTH = 60;
        private const int DEF_COLUMNS_COUNT = 7;
        private const int DEF_FOOTER_SIZE = 27;
        private const int DEF_HEADER_SIZE = 21;
        private const int DEF_NONE_TAB_INDEX = 101;
        private const int DEF_ROWS_COUNT = 7;
        private const int DEF_ROWS_MARGIN = 3;
        private const int DEF_TODAY_TAB_INDEX = 100;
        private const int DEF_WEEK_DAY_HEIGHT = 20;

        #endregion

        #region Fields

        private readonly StringFormat _StringFormat;
        private readonly GregorianCalendar _GCal;
        private readonly HijriCalendar _HijCal;
        private readonly PersianCalendar _PerCal;
        private readonly ArrayList _RectsList = new ArrayList(100);
        private readonly DateTimeCollection _SelectedDateRange;
        private readonly ArrayList _SelectedRects = new ArrayList();
        private bool _btnNoneActive;
        private bool _btnTodayActive;
        private Calendar _CustomCalendar;
        private DateTime _SelectedDateTime;
        private int _iDay;

        private int _iLastFocused = 1;
        private int _iMonth;
        private bool _IsMultiSelect;
        private bool _IsNull;
        private int _iYear;
        private Rectangle _rcBody;
        private Rectangle _rcFooter;
        private Rectangle rcHeader;
        private bool _RectsCreated;
        private ScrollOptionTypes _ScrollOption;
        private DateTime _SelectedGregorianDate;
        private bool _ShowBorder = true;
        private bool _ShowFocusRect;

        #endregion

        #region Events

        /// <summary>
        /// Fires when SelectedDateTime value changes.
        /// </summary>
        public event EventHandler SelectedDateTimeChanged;

        /// <summary>
        /// Fires when a date is added/removes to SelectedDateRange collection, if the control is in <see cref="IsMultiSelect"/> mode.
        /// </summary>
        public event EventHandler SelectedDateRangeChanged;

        /// <summary>
        /// Fires when Day value changes.
        /// </summary>
        public event EventHandler DayChanged;

        /// <summary>
        /// Fires when MonthValue changes.
        /// </summary>
        public event EventHandler MonthChanged;

        /// <summary>
        /// Fires when Year value changes.
        /// </summary>
        public event EventHandler YearChanged;

        /// <summary>
        /// Fires when current day is being printed.
        /// </summary>
        public event CustomDrawDayEventHandler DrawCurrentDay;

        /// <summary>
        /// Fires when user clicks on a day, None button or Today button.
        /// </summary>
        public event CalendarButtonClickedEventHandler ButtonClicked;

        #endregion

        #region Properties

        /// <summary>
        /// Determinces scrolling option of the PersianMonthView control.
        /// </summary>
        [DefaultValue(typeof(ScrollOptionTypes), "Month")]
        [Description("Determinces scrolling option of the PersianMonthView control.")]
        public ScrollOptionTypes ScrollOption
        {
            get { return _ScrollOption; }
            set
            {
                if (_ScrollOption == value)
                    return;

                _ScrollOption = value;
            }
        }

        [Browsable(false)]
        internal StringFormat OneLineNoTrimming
        {
            get
            {
                _StringFormat.Alignment = StringAlignment.Center;
                _StringFormat.LineAlignment = StringAlignment.Center;
                _StringFormat.Trimming = StringTrimming.None;
                _StringFormat.FormatFlags = StringFormatFlags.LineLimit;
                _StringFormat.HotkeyPrefix = HotkeyPrefix.Show;

                return _StringFormat;
            }
        }

        /// <summary>
        /// Determines if the control has not made any selection yet.
        /// </summary>
        [DefaultValue(true)]
        [RefreshProperties(RefreshProperties.All)]
        public bool IsNull
        {
            get { return _IsNull; }
            set
            {
                if (_IsNull == value)
                    return;

                _IsNull = value;
                if (_IsNull)
                {
                    // Also clear the selection
                    _SelectedDateRange.Clear();
                }

                Invalidate();
            }
        }

        /// <summary>
        /// Gets the Day value.
        /// </summary>
        [Browsable(false)]
        public int Day
        {
            get { return _iDay; }
        }

        /// <summary>
        /// Gets the Month value.
        /// </summary>
        [Browsable(false)]
        public int Month
        {
            get { return _iMonth; }
        }

        /// <summary>
        /// Gets the Year value.
        /// </summary>
        [Browsable(false)]
        public int Year
        {
            get { return _iYear; }
        }

        /// <summary>
        /// Arabic culture supported by the control : ("AR-SA")
        /// </summary>
        [Browsable(false)]
        public CultureInfo ArabicCulture
        {
            get { return PersianLocalizeManager.ArabicCulture; }
        }

        /// <summary>
        /// Invariant culture supported by the control.
        /// </summary>
        [Browsable(false)]
        public CultureInfo InvariantCulture
        {
            get { return PersianLocalizeManager.InvariantCulture; }
        }

        /// <summary>
        /// Persian culture supported by the control. ("FA-IR")
        /// </summary>
        [Browsable(false)]
        public CultureInfo PersianCulture
        {
            get { return PersianLocalizeManager.FarsiCulture; }
        }

        /// <summary>
        /// PersianCalendar instance with which controls calculates values of <see cref="PersianCulture"/>.
        /// </summary>
        [Browsable(false)]
        public PersianCalendar PersianCalendar
        {
            get { return _PerCal; }
        }

        /// <summary>
        /// GregorianCalendar instance with which controls calculates values of <see cref="InvariantCulture"/>.
        /// </summary>
        [Browsable(false)]
        public Calendar InvariantCalendar
        {
            get { return _GCal; }
        }

        /// <summary>
        /// HijriCalendar instance with which controls calculates values of <see cref="ArabicCulture"/>.
        /// </summary>
        [Browsable(false)]
        public Calendar HijriCalendar
        {
            get { return _HijCal; }
        }

        /// <summary>
        /// Default calendar of the control, based on <see cref="Thread.CurrentCulture"/> and <see cref="Thread.CurrentUICulture"/> properties.
        /// </summary>
        [Browsable(false)]
        public Calendar DefaultCalendar
        {
            get { return GetDefaultCalendar(); }
            set
            {
                _CustomCalendar = value;
                _iYear = DefaultCalendar.GetYear(_SelectedGregorianDate);
                Invalidate();
            }
        }

        /// <summary>
        /// Gets or Sets to show a border around the control.
        /// </summary>
        [DefaultValue(true)]
        [Description("Gets or Sets to show a border around the control.")]
        public bool ShowBorder
        {
            get { return _ShowBorder; }
            set
            {
                if (_ShowBorder == value)
                    return;

                _ShowBorder = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Gets or Sets to show the focus rectangle around the selected day.
        /// </summary>
        [DefaultValue(false)]
        [Description("Gets or Sets to show the focus rectangle around the selected day.")]
        public bool ShowFocusRect
        {
            get { return _ShowFocusRect; }
            set
            {
                if (_ShowFocusRect == value)
                    return;

                _ShowFocusRect = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Selected values collection, if the control is in MultiSelect mode.
        /// </summary>
        [Editor(typeof(DateTimeCollectionEditor), typeof(UITypeEditor))]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [TypeConverter(typeof(DateTimeConverter))]
        [Description("Selected values collection, if the control is in MultiSelect mode.")]
        public DateTimeCollection SelectedDateRange
        {
            get
            {
                if (IsMultiSelect)
                {
                    return _SelectedDateRange;
                }
                var singleSelection = new DateTimeCollection();
                if (!IsNull)
                    singleSelection.Add(SelectedDateTime);

                return singleSelection;
            }
        }

        /// <summary>
        /// Gets or Sets the control in MultiSelect mode.
        /// </summary>
        [DefaultValue(false)]
        [Description("Gets or Sets the control in MultiSelect mode.")]
        public bool IsMultiSelect
        {
            get { return _IsMultiSelect; }
            set
            {
                if (_IsMultiSelect == value)
                    return;

                _IsMultiSelect = value;
                Repaint();
            }
        }

        /// <summary>
        /// Currently selected DateTime in calendar control.
        /// </summary>
        [Description("Currently selected DateTime instance from calendar.")]
        [RefreshProperties(RefreshProperties.All)]
        [Bindable(true)]
        [Localizable(true)]
        public DateTime SelectedDateTime
        {
            get { return _SelectedGregorianDate; }
            set
            {
                if (value.Equals(DateTime.MinValue))
                    IsNull = true;
                else
                    IsNull = false;

                if (_SelectedGregorianDate == value)
                    return;

                _SelectedGregorianDate = value;

                _iDay = DefaultCalendar.GetDayOfMonth(_SelectedGregorianDate);
                OnDayChanged(EventArgs.Empty);

                _iMonth = DefaultCalendar.GetMonth(_SelectedGregorianDate);
                OnMonthChanged(EventArgs.Empty);

                _iYear = DefaultCalendar.GetYear(_SelectedGregorianDate);
                OnYearChanged(EventArgs.Empty);

                _RectsCreated = false;
                Invalidate();

                OnSelectedDateTimeChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Size of the control that can not be changes. Control's size is fixed to 166 x 166 pixels.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new Size Size
        {
            get { return base.Size; }
            set { base.Size = new Size(166, 166); }
        }

        /// <summary>
        /// Is control in popup mode?
        /// </summary>
        [Browsable(false)]
        [DefaultValue(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool IsPopupMode { get; set; }

        #endregion

        #region Hidden Props

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Bindable(false)]
        public override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override DockStyle Dock
        {
            get { return DockStyle.None; }
            set { }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override AnchorStyles Anchor
        {
            get { return AnchorStyles.None; }
            set { }
        }

        #endregion

        #region Ctor && Dispose

        /// <summary>
        /// Creates a new instance of PersianMonthView class. Initiated control could be uses in PopupMode or Normal mode, depending on the use.
        /// </summary>
        /// <param name="popupMode"></param>
        public PersianMonthView(bool popupMode)
        {
            IsPopupMode = popupMode;
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.ResizeRedraw, true);

            _PerCal = new PersianCalendar();
            _GCal = new GregorianCalendar();
            _HijCal = new HijriCalendar();

            _SelectedDateRange = new DateTimeCollection();
            _SelectedDateRange.CollectionChanged += OnSelectionCollectionChanged;

            base.Size = new Size(166, 166);
            base.Font = new Font("Tahoma", 8.25F);
            SelectedDateTime = DateTime.Now;
            _SelectedDateTime = SelectedDateTime;
            _StringFormat = new StringFormat();
            _ScrollOption = ScrollOptionTypes.Month;
            _IsNull = true;

            PersianLocalizeManager.LocalizerChanged += OnInternalLocalizeChanged;
        }

        /// <summary>
        /// Creates a new instance of PersianMonthView for normal mode usage.
        /// </summary>
        public PersianMonthView()
            : this(false)
        {
        }

        /// <summary>
        /// Disposes the control.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_StringFormat != null) _StringFormat.Dispose();
            }

            base.Dispose(disposing);
        }

        #endregion

        #region Paint Methods

        protected override void OnPaint(PaintEventArgs pe)
        {
            if (!CanUpdate)
                return;

            try
            {
                BeginUpdate();

                var rc = new Rectangle(0, 0, Width, Height);

                //Active rectangles must be rebuild
                if (_RectsCreated == false)
                    _RectsList.Clear();

                OnDrawHeader(new PaintEventArgs(pe.Graphics, rcHeader));
                OnDrawFooter(new PaintEventArgs(pe.Graphics, _rcFooter));

                if (_RectsCreated == false)
                {
                    _rcBody = new Rectangle(rc.X, rcHeader.Bottom, rc.Width, _rcFooter.Top - rcHeader.Bottom);
                    _rcBody = Rectangle.Inflate(_rcBody, -4, -1);
                }

                OnDrawBody(new PaintEventArgs(pe.Graphics, _rcBody));
                OnDrawBorder(new PaintEventArgs(pe.Graphics, rc));

                _RectsCreated = true;
            }
            finally
            {
                EndUpdate();
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            if (!CanUpdate)
                return;

            try
            {
                BeginUpdate();

                Graphics g = pevent.Graphics;
                var rc = new Rectangle(0, 0, Width, Height);

                Painter.DrawFilledBackground(g, rc, false, 90f);

                // Draw header background
                rcHeader = new Rectangle(rc.X + 1, rc.Y + 1, rc.Width - 2, DEF_HEADER_SIZE);
                Painter.DrawButton(g, rcHeader, string.Empty, Font, null, ItemState.Normal, false, true);

                // Construct footer rect
                int yBott = rc.Bottom - DEF_FOOTER_SIZE - 1;
                _rcFooter = new Rectangle(rc.X + 6, yBott, rc.Width - 12, DEF_FOOTER_SIZE);
            }
            finally
            {
                EndUpdate();
            }
        }

        private void OnDrawBorder(PaintEventArgs e)
        {
            if (ShowBorder)
            {
                var border = new Rectangle(e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width - 1,
                                           e.ClipRectangle.Height - 1);
                Painter.DrawBorder(e.Graphics, border, false);
            }
        }

        private void OnDrawHeader(PaintEventArgs pevent)
        {
            Rectangle rc = pevent.ClipRectangle;
            Rectangle rcOut = Rectangle.Inflate(rc, -6, -1);

            var ev = new PaintEventArgs(pevent.Graphics, rcOut);

            OnDrawMonthHeader(ev);
            OnDrawYearHeader(ev);
        }

        private void OnDrawMonthHeader(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            Rectangle rc = pevent.ClipRectangle;

            string strMonth = CurrentMonthName;
            string strLongestMonth = PersianDate.PersianMonthNames.Default.Ordibehesht;

            // Draw left arrow and add it as Active Rectangle
            Rectangle rect = Painter.DrawArrow(g, rc, true, false, DEF_ARROW_SIZE);
            AddActiveRect(rect, TRectangleAction.MonthDown);

            SizeF sz = g.MeasureString(strLongestMonth, Font);
            var rcText = new Rectangle(rect.Right + 4, rc.Y, (int)sz.Width + 20, rc.Height);
            g.DrawString(strMonth, Font, SystemBrushes.WindowText, rcText, OneLineNoTrimming);

            // draw  right arrow and add it like Active Rectangle
            rect = Painter.DrawArrow(g, new Rectangle(rcText.Right + 4, rc.Y, 100, rc.Height), false, false,
                                     DEF_ARROW_SIZE);
            AddActiveRect(rect, TRectangleAction.MonthUp);
        }

        private void OnDrawYearHeader(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            Rectangle rc = pevent.ClipRectangle;

            string strYear = toFarsi.Convert(_iYear.ToString(), DefaultCulture);

            Rectangle rect = Painter.DrawArrow(g,
                                               new Rectangle(rc.Right - 4 - DEF_ARROW_SIZE - 2, rc.Y, DEF_ARROW_SIZE * 2,
                                                             rc.Height), false, false, DEF_ARROW_SIZE);
            AddActiveRect(rect, TRectangleAction.YearUp);

            SizeF sz = g.MeasureString(strYear, Font);
            var rcText = new Rectangle(rect.Left - 4 - (int)sz.Width - 8, rc.Y, (int)sz.Width + 8, rc.Height);
            g.DrawString(strYear, Font, SystemBrushes.WindowText, rcText, OneLineNoTrimming);

            rect = Painter.DrawArrow(g,
                                     new Rectangle(rcText.Left - 4 - DEF_ARROW_SIZE - 2, rc.Y, DEF_ARROW_SIZE * 2,
                                                   rc.Height), true, false, DEF_ARROW_SIZE);
            AddActiveRect(rect, TRectangleAction.YearDown);
        }

        private void OnDrawFooter(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            OnDrawFooterButtons(new PaintEventArgs(g, _rcFooter));
        }

        private void OnDrawFooterButtons(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            Rectangle rc = pevent.ClipRectangle;

            const int buttonSpace = 10;
            int margin = ((rc.Width - (DEF_BUTTON_WIDTH * 2) - buttonSpace) / 2);

            var fmt = new StringFormat();
            fmt.Alignment = StringAlignment.Center;
            fmt.LineAlignment = StringAlignment.Center;
            fmt.Trimming = StringTrimming.None;
            fmt.FormatFlags |= StringFormatFlags.DirectionRightToLeft | StringFormatFlags.NoWrap;

            var rcToday = new Rectangle(rc.X + margin, rc.Y + rc.Height / 2 - DEF_BUTTON_HEIGHT / 2, DEF_BUTTON_WIDTH,
                                        DEF_BUTTON_HEIGHT);
            var rcNone = new Rectangle(rcToday.Right + buttonSpace, rcToday.Y, rcToday.Width, rcToday.Height);
            AddActiveRect(rcToday, TRectangleAction.TodayBtn, DEF_TODAY_TAB_INDEX);
            AddActiveRect(rcNone, TRectangleAction.NoneBtn, DEF_NONE_TAB_INDEX);

            ItemState todayState = ItemState.Normal;
            ItemState noneState = ItemState.Normal;

            if (_btnTodayActive)
                todayState = ItemState.HotTrack;

            if (_btnNoneActive)
                noneState = ItemState.HotTrack;

            Painter.DrawButton(g, rcToday,
                               PersianLocalizeManager.GetLocalizerByCulture(DefaultCulture).GetLocalizedString(
                                   StringIDEnum.FAMonthView_Today), Font, fmt, todayState, true, true);
            Painter.DrawButton(g, rcNone,
                               PersianLocalizeManager.GetLocalizerByCulture(DefaultCulture).GetLocalizedString(
                                   StringIDEnum.FAMonthView_None), Font, fmt, noneState, true, true);

            fmt.Dispose();
        }

        private void OnDrawBody(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            Rectangle rc = pevent.ClipRectangle;

            int iColWidth = rc.Width / DEF_COLUMNS_COUNT;
            int iRowHeight = (rc.Height - DEF_WEEK_DAY_HEIGHT) / DEF_ROWS_COUNT;

            #region Top Separator

            Painter.DrawSeparator(g, new Point(rc.X + 2, rc.Y + DEF_WEEK_DAY_HEIGHT - 3),
                                  new Point(rc.Right - 2, rc.Y + DEF_WEEK_DAY_HEIGHT - 3));

            #endregion

            #region Weekday Name

            var rcHead = new Rectangle(rc.X, rc.Y, iColWidth, DEF_WEEK_DAY_HEIGHT - 3);

            if (IsRightToLeftCulture)
            {
                for (int i = DEF_COLUMNS_COUNT; i > 0; i--)
                {
                    rcHead.X = rc.Width - (i * iColWidth);
                    string strDayWeek = GetAbbrDayName(i - 1);
                    g.DrawString(strDayWeek, Font, SystemBrushes.WindowText, rcHead, OneLineNoTrimming);
                }
            }
            else
            {
                for (int i = 0; i < DEF_COLUMNS_COUNT; i++)
                {
                    rcHead.X = rc.X + (i * iColWidth);
                    string strDayWeek = GetAbbrDayName(i);
                    g.DrawString(strDayWeek, Font, SystemBrushes.WindowText, rcHead, OneLineNoTrimming);
                }
            }

            #endregion

            #region Calculate Month Values

            //How many days are in DrawTab month and first day of month
            int numDays = DefaultCalendar.GetDaysInMonth(_iYear, _iMonth);
            var dtStartOfMonth = new DateTime(_iYear, _iMonth, 1, 0, 0, 0, DefaultCalendar);
            int firstDay = GetFirstDayOfWeek(dtStartOfMonth);

            int rowNo = 1;

            int iLastMonth = _iMonth;
            int iLastYear = _iYear;

            if (_iMonth - 1 < 1 && iLastYear > 1)
            {
                iLastMonth = 12;
                iLastYear--;
            }
            else if (iLastMonth - 1 > 0)
            {
                iLastMonth--;
            }

            int prevMonthDays = DefaultCalendar.GetDaysInMonth(iLastYear, iLastMonth);
            int lastingDays = prevMonthDays - firstDay;
            Brush brush = Brushes.Gray;
            Rectangle rcDay;

            if (IsRightToLeftCulture)
                rcDay = new Rectangle(rcHead.X, rc.Y + DEF_WEEK_DAY_HEIGHT, rcHead.Width - 2, iRowHeight + 1);
            else
                rcDay = new Rectangle(rc.X, rc.Y + DEF_WEEK_DAY_HEIGHT, rcHead.Width - 2, iRowHeight + 1);

            #endregion

            #region Pre-Day Padding

            for (int y = lastingDays; y < prevMonthDays; y++)
            {
                //Disabled Days
                string disabledDay = toFarsi.Convert((y + 1).ToString(), DefaultCulture);
                g.DrawString(disabledDay, Font, brush, rcDay, OneLineNoTrimming);

                if (IsRightToLeftCulture)
                    rcDay.X = rcDay.X - iColWidth;
                else
                    rcDay.X = rcDay.X + iColWidth;
            }

            #endregion

            #region Current Day

            for (int x = 1; x <= numDays; x++)
            {
                var dtInPaint = new DateTime(_iYear, _iMonth, x, DefaultCalendar);
                brush = SystemBrushes.WindowText;

                //draw weekday header names
                string DayNo = toFarsi.Convert(x.ToString(), DefaultCulture);
                int index = x;

                //Selected Days
                if (IsMultiSelect && (_SelectedRects.Contains(index) || SelectedDateRange.Contains(dtInPaint)))
                {
                    if (!ShowFocusRect)
                        Painter.DrawSelectedPanel(g, rcDay);
                    else
                        Painter.DrawFocusRect(g, rcDay);

                    g.DrawString(DayNo, Font, SystemBrushes.ControlText, rcDay, OneLineNoTrimming);
                    AddActiveRect(rcDay, TRectangleAction.MonthDay, index);
                }
                else if (_iDay == x) //Current Day
                {
                    AddActiveRect(rcDay, TRectangleAction.MonthDay, index);
                    var args = new CustomDrawDayEventArgs(rcDay, g, _iYear, _iMonth, x, true);
                    OnDrawCurrentDay(args);

                    if (!args.Handled)
                    {
                        if (!IsNull && !ShowFocusRect)
                            Painter.DrawSelectedPanel(g, rcDay);
                        else if (!IsNull && ShowFocusRect)
                            Painter.DrawFocusRect(g, rcDay);
                        else if (IsNull)
                            Painter.DrawSelectionBorder(g, rcDay);

                        g.DrawString(DayNo, Font, SystemBrushes.ControlText, rcDay, OneLineNoTrimming);
                    }
                }
                else //Other Days
                {
                    AddActiveRect(rcDay, TRectangleAction.MonthDay, index);
                    var args = new CustomDrawDayEventArgs(rcDay, g, _iYear, _iMonth, x, false);
                    OnDrawCurrentDay(args);

                    if (!args.Handled)
                    {
                        g.DrawString(DayNo, Font, brush, rcDay, OneLineNoTrimming);
                    }
                }

                if (IsRightToLeftCulture)
                {
                    rcDay.X = rcDay.X - iColWidth;

                    if (rcDay.X < 0)
                    {
                        rowNo++;
                        rcDay.X = rcHead.X;
                        rcDay.Y = rcDay.Y + iRowHeight + DEF_ROWS_MARGIN;
                    }
                }
                else
                {
                    rcDay.X = rcDay.X + iColWidth;

                    if (rcDay.X > rc.Width - rcDay.Width)
                    {
                        rowNo++;
                        rcDay.X = rc.X;
                        rcDay.Y = rcDay.Y + iRowHeight + DEF_ROWS_MARGIN;
                    }
                }
            }

            #endregion

            #region Post-Day Padding

            //Draw next month starting days as disabled
            int endDay;
            brush = Brushes.Gray;

            if (firstDay != 0)
                endDay = numDays + 1;
            else
                endDay = numDays;

            for (int i = endDay; i < 42; i++)
            {
                if (rowNo > 6)
                    break;

                string disabledDay = toFarsi.Convert((i - endDay + 1).ToString(), DefaultCulture);
                g.DrawString(disabledDay, Font, brush, rcDay, OneLineNoTrimming);

                if (IsRightToLeftCulture)
                {
                    rcDay.X = rcDay.X - iColWidth;

                    if (rcDay.X < 0)
                    {
                        rowNo++;
                        rcDay.X = rcHead.X;
                        rcDay.Y = rcDay.Y + iRowHeight + DEF_ROWS_MARGIN;
                    }
                }
                else
                {
                    rcDay.X = rcDay.X + iColWidth;

                    if (rcDay.X > rc.Width - rcDay.Width)
                    {
                        rowNo++;
                        rcDay.X = rc.X;
                        rcDay.Y = rcDay.Y + iRowHeight + DEF_ROWS_MARGIN;
                    }
                }
            }

            #endregion
        }

        protected override void OnResize(EventArgs e)
        {
            if (Width < 166)
                Width = 166;

            if (Height < 166)
                Height = 166;

            _RectsCreated = false;
            Invalidate();
        }

        private int GetFirstDayOfWeek(DateTime date)
        {
            if (DefaultCulture.Equals(PersianLocalizeManager.FarsiCulture))
            {
                PersianDate pd = date;
                return (int)pd.DayOfWeek;
            }
            if (DefaultCulture.Equals(PersianLocalizeManager.ArabicCulture))
            {
                return (int)date.DayOfWeek;
            }
            return (int)date.DayOfWeek;
        }

        private void AddActiveRect(Rectangle rc, TRectangleAction action, object tag)
        {
            if (_RectsCreated == false)
            {
                _RectsList.Add(new ActRect(rc, action, tag));
            }
        }

        private void AddActiveRect(Rectangle rc, TRectangleAction action)
        {
            if (_RectsCreated == false)
            {
                _RectsList.Add(new ActRect(rc, action));
            }
        }

        #endregion

        #region Overrides

        internal void OnRecalculateRequired()
        {
            ResetAllRectangleStates();

            if (_RectsCreated)
                _RectsCreated = false;

            ActRect rect = FindActiveRectByTag(_iDay);
            if (_iLastFocused < DEF_TODAY_TAB_INDEX)
                _iLastFocused = _iDay;

            if (rect != null)
                rect.State |= TRectangleStatus.FocusSelect;
        }

        /// <summary>
        /// Scrolls days in the view to the Left.
        /// </summary>
        public void ScrollDaysLeft()
        {
            if (_iLastFocused < DEF_TODAY_TAB_INDEX)
                SelectedDateTime = DefaultCalendar.AddDays(SelectedDateTime, -1);
        }

        /// <summary>
        /// Scrolls days in the view to the Right.
        /// </summary>
        public void ScrollDaysRight()
        {
            if (_iLastFocused < DEF_TODAY_TAB_INDEX)
                SelectedDateTime = DefaultCalendar.AddDays(SelectedDateTime, 1);
        }

        /// <summary>
        /// Scrolls days in the view to the Up.
        /// </summary>
        public void ScrollDaysUp()
        {
            if (_iLastFocused < DEF_TODAY_TAB_INDEX)
                SelectedDateTime = DefaultCalendar.AddDays(SelectedDateTime, -7);
        }

        /// <summary>
        /// Scrolls days in the view to the Down.
        /// </summary>
        public void ScrollDaysDown()
        {
            if (_iLastFocused < DEF_TODAY_TAB_INDEX)
                SelectedDateTime = DefaultCalendar.AddDays(SelectedDateTime, 7);
        }

        internal void SetFocusOnNextControl()
        {
            ResetFocusedRectangleState();

            if (_iLastFocused < DEF_TODAY_TAB_INDEX)
            {
                _iLastFocused = DEF_TODAY_TAB_INDEX;
            }
            else if (_iLastFocused == DEF_TODAY_TAB_INDEX)
            {
                _iLastFocused = DEF_NONE_TAB_INDEX;
            }
            else
            {
                // ReSharper disable PossibleNullReferenceException
                Control ctrl = FindForm().GetNextControl(this, true);
                // ReSharper restore PossibleNullReferenceException
                if (ctrl != null) ctrl.Focus();
            }
        }

        internal void SetFocusOnPrevControl()
        {
            ResetFocusedRectangleState();

            if (_iLastFocused < DEF_TODAY_TAB_INDEX)
            {
                _iLastFocused = DEF_NONE_TAB_INDEX;
            }
            else if (_iLastFocused == DEF_TODAY_TAB_INDEX && _iDay != 0)
            {
                _iLastFocused = _iDay;

                ActRect rc = FindActiveRectByTag(_iDay);
                if (rc != null)
                {
                    rc.State |= TRectangleStatus.Focused | TRectangleStatus.Selected;
                }
            }
            else if (_iLastFocused == DEF_NONE_TAB_INDEX)
            {
                _iLastFocused = DEF_TODAY_TAB_INDEX;
            }
            else
            {
                // ReSharper disable PossibleNullReferenceException
                Control ctrl = FindForm().GetNextControl(this, false);
                // ReSharper restore PossibleNullReferenceException
                if (ctrl != null) ctrl.Focus();
            }
        }

        /// <summary>
        /// Changes the Year value to the Next Year.
        /// </summary>
        public void ToNextYear()
        {
            try
            {
                SelectedDateTime = DefaultCalendar.AddYears(SelectedDateTime, 1);
                OnRecalculateRequired();
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        /// <summary>
        /// Changes the Year value to the Previous Year.
        /// </summary>
        public void ToPrevYear()
        {
            try
            {
                SelectedDateTime = DefaultCalendar.AddYears(SelectedDateTime, -1);
                OnRecalculateRequired();
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        /// <summary>
        /// Changes the Month value to the Next Month.
        /// </summary>
        public void ToNextMonth()
        {
            try
            {
                SelectedDateTime = DefaultCalendar.AddMonths(SelectedDateTime, 1);
                OnRecalculateRequired();
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        /// <summary>
        /// Changes the Month value to the Previous Month.
        /// </summary>
        public void ToPrevMonth()
        {
            try
            {
                SelectedDateTime = DefaultCalendar.AddMonths(SelectedDateTime, -1);
                OnRecalculateRequired();
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void RecalculateSelectionUp()
        {
            SelectedDateTime = _SelectedDateTime.AddDays(-7);

            if (SelectedDateTime.Month != _SelectedDateTime.Month)
            {
                ScrollDaysUp();
            }
            else
            {
                ResetFocusedRectangleState();
            }
        }

        private void RecalculateSelectionDown()
        {
            SelectedDateTime = _SelectedDateTime.AddDays(7);

            if (SelectedDateTime.Month != _SelectedDateTime.Month) // switch to another month
            {
                ScrollDaysDown();
            }
            else
            {
                ResetFocusedRectangleState();
            }
        }

        private void RecalculateSelectionLeft()
        {
            SelectedDateTime = _SelectedDateTime.AddDays(-1);

            if (SelectedDateTime.Month != _SelectedDateTime.Month) // switch to another month
            {
                ScrollDaysLeft();
            }
            else
            {
                ResetFocusedRectangleState();
            }
        }

        private void RecalculateSelectionRight()
        {
            SelectedDateTime = _SelectedDateTime.AddDays(1);

            if (SelectedDateTime.Month != _SelectedDateTime.Month) // switch to another month
            {
                ScrollDaysRight();
            }
            else
            {
                ResetFocusedRectangleState();
            }
        }

        private void OnRectangleClick(ActRect rc)
        {
            switch (rc.Action)
            {
                case TRectangleAction.MonthDown:
                    ToPrevMonth();
                    break;
                case TRectangleAction.MonthUp:
                    ToNextMonth();
                    break;
                case TRectangleAction.YearDown:
                    ToPrevYear();
                    break;
                case TRectangleAction.YearUp:
                    ToNextYear();
                    break;
                case TRectangleAction.TodayBtn:
                    _iLastFocused = DEF_TODAY_TAB_INDEX;
                    SetTodayDay();
                    OnButtonClicked(new CalendarButtonClickedEventArgs(FAMonthViewButtons.Today));
                    break;
                case TRectangleAction.NoneBtn:
                    _iLastFocused = DEF_NONE_TAB_INDEX;
                    SetNoneDay();
                    OnButtonClicked(new CalendarButtonClickedEventArgs(FAMonthViewButtons.None));
                    break;
                case TRectangleAction.MonthDay:
                    if (_iDay == 0) return;

                    var index = (int)rc.Tag;
                    _iLastFocused = index;

                    SelectedDateTime = new DateTime(_iYear, _iMonth, index, 0, 0, 0, DefaultCalendar);
                    IsNull = false;

                    if (IsMultiSelect)
                    {
                        if (!_IsNull && !SelectedDateRange.Contains(SelectedDateTime))
                        {
                            SelectedDateRange.Add(SelectedDateTime);
                            if (!_SelectedRects.Contains(rc.Tag))
                                _SelectedRects.Add(rc.Tag);
                        }
                    }

                    OnButtonClicked(new CalendarButtonClickedEventArgs(FAMonthViewButtons.MonthDay));
                    break;
            }

            Invalidate();
        }

        private void OnSelectionClick(ActRect rc)
        {
            if (rc.Action == TRectangleAction.MonthDay)
            {
                if (rc.IsSelected == false)
                {
                    rc.State |= TRectangleStatus.Selected;

                    SelectedDateTime = new DateTime(_iYear, _iMonth, (int)rc.Tag, 0, 0, 0, DefaultCalendar);
                    if (!_IsNull && !SelectedDateRange.Contains(SelectedDateTime))
                    {
                        SelectedDateRange.Add(SelectedDateTime);

                        if (!_SelectedRects.Contains(rc.Tag))
                            _SelectedRects.Add(rc.Tag);
                    }
                }
                else
                {
                    rc.State = (TRectangleStatus)((int)rc.State & ~(int)TRectangleStatus.Selected);
                    _SelectedRects.Remove(rc.Tag);
                }

                _iLastFocused = (int)rc.Tag;
                _IsNull = false;
            }

            Invalidate();
        }

        private void OnSelectionCollectionChanged(object sender, CollectionChangedEventArgs e)
        {
            ResetSelectedRectangleState();
            _SelectedRects.Clear();

            if (SelectedDateRange.Count > 0)
            {
                SelectedDateTime = SelectedDateRange[0];
            }
            else
            {
                _IsNull = true;
            }

            OnRecalculateRequired();
            Invalidate();
            OnSelectedDateRangeChanged(EventArgs.Empty);
        }

        private void OnEnterPressed()
        {
            ResetSelectedRectangleState();

            ActRect rect = FindActiveRectByTag(_iLastFocused);

            if (rect != null)
            {
                switch (rect.Action)
                {
                    case TRectangleAction.TodayBtn:
                        SetTodayDay();
                        break;

                    case TRectangleAction.NoneBtn:
                        SetNoneDay();
                        break;
                }
            }
        }

        #endregion

        #region Public Methods

        [Browsable(false)]
        public CultureInfo DefaultCulture
        {
            get
            {
                if (PersianLocalizeManager.CustomCulture != null)
                    return PersianLocalizeManager.CustomCulture;

                if (Thread.CurrentThread.CurrentUICulture.Equals(PersianLocalizeManager.FarsiCulture))
                    return PersianLocalizeManager.FarsiCulture;
                if (Thread.CurrentThread.CurrentUICulture.Equals(
                    PersianLocalizeManager.ArabicCulture))
                    return PersianLocalizeManager.ArabicCulture;
                return PersianLocalizeManager.InvariantCulture;
            }
            set
            {
                PersianLocalizeManager.CustomCulture = value;
                _RectsCreated = false;
                _iYear = DefaultCalendar.GetYear(_SelectedGregorianDate);
            }
        }

        private bool IsRightToLeftCulture
        {
            get { return DefaultCulture.TextInfo.IsRightToLeft; }
        }

        /// <summary>
        /// Current Month name shown in the view.
        /// </summary>
        [Browsable(false)]
        public string CurrentMonthName
        {
            get
            {
                if (DefaultCulture.Equals(PersianLocalizeManager.FarsiCulture))
                    return PersianDate.PersianMonthNames.Default[_iMonth];
                return DefaultCulture.DateTimeFormat.
                    MonthGenitiveNames[_iMonth - 1].ToUpper();
            }
        }

        private Calendar GetDefaultCalendar()
        {
            if (_CustomCalendar != null)
                return _CustomCalendar;

            if (DefaultCulture.Equals(PersianLocalizeManager.FarsiCulture))
                return _PerCal;
            if (DefaultCulture.Equals(PersianLocalizeManager.ArabicCulture))
                return _HijCal;
            return _GCal;
        }

        /// <summary>
        /// Gets the abbreviated name of the specified day.
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public string GetAbbrDayName(int day)
        {
            if (PersianLocalizeManager.CustomCulture != null)
            {
                if (PersianLocalizeManager.CustomCulture.Equals(PersianLocalizeManager.FarsiCulture))
                {
                    return PersianDate.PersianWeekDayAbbr.Default[day];
                }
                return PersianLocalizeManager.CustomCulture.DateTimeFormat.ShortestDayNames[day].ToUpper();
            }

            if (DefaultCulture.Equals(PersianLocalizeManager.FarsiCulture))
                return PersianDate.PersianWeekDayAbbr.Default[day];
            if (DefaultCulture.Equals(PersianLocalizeManager.ArabicCulture))
                return PersianLocalizeManager.ArabicCulture.DateTimeFormat.
                    AbbreviatedDayNames[day].Substring(2, 1);
            return PersianLocalizeManager.InvariantCulture.DateTimeFormat.
                AbbreviatedDayNames[day].Substring(0, 1);
        }

        /// <summary>
        /// Clears the selection of the control. Also clears any selected date in MultiSelect mode. 
        /// </summary>
        public void SetNoneDay()
        {
            IsNull = true;
            SelectedDateRange.Clear();
            OnSelectedDateTimeChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Sets the selection value to Today.
        /// </summary>
        public void SetTodayDay()
        {
            SelectedDateRange.Clear();
            SelectedDateTime = DateTime.Now;
            IsNull = false;
            SelectedDateRange.Add(SelectedDateTime);
        }

        #endregion

        #region Custom Events

        protected internal virtual void OnButtonClicked(CalendarButtonClickedEventArgs e)
        {
            if (ButtonClicked != null)
                ButtonClicked(this, e);
        }

        protected internal virtual void OnMonthChanged(EventArgs e)
        {
            if (MonthChanged != null)
                MonthChanged(this, e);
        }

        protected internal virtual void OnYearChanged(EventArgs e)
        {
            if (YearChanged != null)
                YearChanged(this, e);
        }

        protected internal virtual void OnDayChanged(EventArgs e)
        {
            if (DayChanged != null)
                DayChanged(this, e);
        }

        protected internal virtual void OnSelectedDateTimeChanged(EventArgs e)
        {
            if (SelectedDateTimeChanged != null)
                SelectedDateTimeChanged(this, e);
        }

        protected internal virtual void OnSelectedDateRangeChanged(EventArgs e)
        {
            if (SelectedDateRangeChanged != null)
                SelectedDateRangeChanged(this, e);
        }

        protected internal virtual void OnDrawCurrentDay(CustomDrawDayEventArgs e)
        {
            if (DrawCurrentDay != null)
                DrawCurrentDay(this, e);
        }

        #endregion

        #region Helper Methods

        private ActRect FindActiveRectByPoint(Point pnt)
        {
            foreach (ActRect rc in _RectsList)
            {
                if (rc.Rect.Contains(pnt))
                    return rc;
            }

            return null;
        }

        private ActRect FindActiveRectByTag(object tag)
        {
            foreach (ActRect rect in _RectsList)
            {
                if (rect.Tag != null && rect.Tag.Equals(tag))
                    return rect;
            }

            return null;
        }

        private void ResetActiveRectanglesState()
        {
            foreach (ActRect rc in _RectsList)
            {
                if ((rc.State & TRectangleStatus.Active) > 0)
                {
                    rc.State = (TRectangleStatus)((int)rc.State & ~(int)TRectangleStatus.Active);
                }
            }
        }

        private void ResetSelectedRectangleState()
        {
            foreach (ActRect rc in _RectsList)
            {
                if ((rc.State & TRectangleStatus.Selected) > 0)
                {
                    rc.State = (TRectangleStatus)((int)rc.State & ~(int)TRectangleStatus.Selected);
                }
            }
        }

        private void ResetFocusedRectangleState()
        {
            foreach (ActRect rc in _RectsList)
            {
                if ((rc.State & TRectangleStatus.Focused) > 0)
                {
                    rc.State = (TRectangleStatus)((int)rc.State & ~(int)TRectangleStatus.Focused);
                }
            }
        }

        private void ResetAllRectangleStates()
        {
            foreach (ActRect rc in _RectsList)
            {
                rc.State = TRectangleStatus.Normal;
            }
        }

        #endregion

        #region Mouse Events

        protected override void OnMouseEnter(EventArgs e)
        {
            Point pnt = MousePosition;
            pnt = PointToClient(pnt);
            _btnTodayActive = false;
            _btnNoneActive = false;

            ResetActiveRectanglesState();

            ActRect rect = FindActiveRectByPoint(pnt);

            if (rect != null && rect.Action != TRectangleAction.WeekDay)
            {
                rect.State |= TRectangleStatus.Active;
            }

            if (rect != null && rect.Action == TRectangleAction.TodayBtn)
            {
                _btnTodayActive = true;
            }

            if (rect != null && rect.Action == TRectangleAction.NoneBtn)
            {
                _btnNoneActive = true;
            }

            Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            _btnNoneActive = false;
            _btnTodayActive = false;

            ResetActiveRectanglesState();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta < 0)
            {
                switch (ScrollOption)
                {
                    case ScrollOptionTypes.Day:
                        ScrollDaysLeft();
                        break;

                    case ScrollOptionTypes.Month:
                        ToNextMonth();
                        break;

                    case ScrollOptionTypes.Year:
                        ToNextYear();
                        break;
                }
            }
            else
            {
                switch (ScrollOption)
                {
                    case ScrollOptionTypes.Day:
                        ScrollDaysRight();
                        break;

                    case ScrollOptionTypes.Month:
                        ToPrevMonth();
                        break;

                    case ScrollOptionTypes.Year:
                        ToPrevYear();
                        break;
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Point pnt = MousePosition;
            pnt = PointToClient(pnt);
            _btnTodayActive = false;
            _btnNoneActive = false;

            ResetActiveRectanglesState();

            ActRect rect = FindActiveRectByPoint(pnt);

            if (rect != null && rect.Action != TRectangleAction.WeekDay)
            {
                rect.State |= TRectangleStatus.Active;
            }

            if (rect != null && rect.Action == TRectangleAction.TodayBtn)
            {
                _btnTodayActive = true;
            }

            if (rect != null && rect.Action == TRectangleAction.NoneBtn)
            {
                _btnNoneActive = true;
            }

            Invalidate();
        }

        private void OnInternalLocalizeChanged(object sender, EventArgs e)
        {
            OnRecalculateRequired();
            Repaint();
        }

        internal void OnInternalMouseDown()
        {
            OnRecalculateRequired();

            Point pnt = MousePosition;
            pnt = PointToClient(pnt);
            ActRect rect = FindActiveRectByPoint(pnt);

            if (rect != null && rect.Action != TRectangleAction.WeekDay)
            {
                if (_iLastFocused == DEF_NONE_TAB_INDEX)
                {
                    rect.State |= TRectangleStatus.Pressed;
                }
                if (_iLastFocused == DEF_TODAY_TAB_INDEX)
                {
                    rect.State |= TRectangleStatus.Pressed;
                }
            }
        }

        internal void OnInternalMouseClick(Point location)
        {
            if (!IsPopupMode)
                Focus();

            ActRect rect = FindActiveRectByPoint(location);

            if (rect != null && rect.Action != TRectangleAction.WeekDay)
            {
                ResetActiveRectanglesState();
                ResetFocusedRectangleState();

                // if selection begin
                if ((ModifierKeys & (Keys.Control | Keys.Shift)) == 0)
                {
                    _SelectedRects.Clear();
                    _SelectedDateRange.Clear();
                    ResetSelectedRectangleState();
                    OnRectangleClick(rect);
                }
                else
                {
                    if (!_SelectedRects.Contains(rect))
                        _SelectedRects.Add(rect);

                    OnSelectionClick(rect);
                }
            }
        }

        /// <summary>
        /// Returns index of the ActRect control on click, if the HitTest is true. Returns -1 if hitting was not successfull.
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public int HitTest(Point location)
        {
            ActRect rect = FindActiveRectByPoint(location);
            if (rect != null)
                return (int)rect.Tag;
            return -1;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                OnInternalMouseDown();

            base.OnMouseDown(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            OnInternalMouseClick(e.Location);

            base.OnMouseClick(e);
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            Point pnt = MousePosition;
            pnt = PointToClient(pnt);

            ActRect rect = FindActiveRectByPoint(pnt);

            if (rect != null && rect.Action != TRectangleAction.WeekDay)
            {
                ResetActiveRectanglesState();
                ResetSelectedRectangleState();
                ResetFocusedRectangleState();

                OnRectangleClick(rect);
            }

            base.OnDoubleClick(e);
        }

        #endregion

        #region Keyboard And Focus Event Handlers

        protected override void OnGotFocus(EventArgs e)
        {
            Invalidate();
        }

        protected override void OnLostFocus(EventArgs e)
        {
            ResetFocusedRectangleState();
            Invalidate();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.Modifiers & (Keys.Alt | Keys.Control | Keys.Shift))
            {
                //Only Shift key is pressed
                case Keys.Shift:
                    switch (e.KeyCode)
                    {
                        case Keys.Tab:
                            SetFocusOnPrevControl();
                            break;
                        case Keys.Down:
                            RecalculateSelectionDown();
                            break;
                        case Keys.Up:
                            RecalculateSelectionUp();
                            break;
                        case Keys.Left:
                            RecalculateSelectionLeft();
                            break;
                        case Keys.Right:
                            RecalculateSelectionRight();
                            break;
                    }
                    break;

                //Only Alt key is pressed
                case Keys.Alt:
                    switch (e.KeyCode)
                    {
                        case Keys.Left:
                            ToPrevMonth();
                            break;
                        case Keys.Right:
                            ToNextMonth();
                            break;
                        case Keys.N:
                            SetNoneDay();
                            break;
                        case Keys.T:
                            SetTodayDay();
                            break;
                    }
                    break;

                //Only Control key is pressed
                case Keys.Control:
                    switch (e.KeyCode)
                    {
                        case Keys.Up:
                            ToNextYear();
                            break;
                        case Keys.Down:
                            ToPrevYear();
                            break;
                    }
                    break;

                default:
                    switch (e.KeyCode)
                    {
                        case Keys.Down:
                            if (_iLastFocused == DEF_TODAY_TAB_INDEX || _iLastFocused == DEF_NONE_TAB_INDEX)
                                SetFocusOnNextControl();
                            else
                                ScrollDaysDown();
                            break;
                        case Keys.Up:
                            if (_iLastFocused == DEF_TODAY_TAB_INDEX || _iLastFocused == DEF_NONE_TAB_INDEX)
                                SetFocusOnPrevControl();
                            else
                                ScrollDaysUp();
                            break;
                        case Keys.Left:
                            if (_iLastFocused == DEF_TODAY_TAB_INDEX || _iLastFocused == DEF_NONE_TAB_INDEX)
                                SetFocusOnPrevControl();
                            else
                                ScrollDaysLeft();
                            break;
                        case Keys.Right:
                            if (_iLastFocused == DEF_TODAY_TAB_INDEX || _iLastFocused == DEF_NONE_TAB_INDEX)
                                SetFocusOnNextControl();
                            else
                                ScrollDaysRight();
                            break;
                        case Keys.Tab:
                            SetFocusOnNextControl();
                            break;

                        case Keys.Space:
                        case Keys.Enter:
                            OnEnterPressed();
                            break;
                    }
                    break;
            }

            base.OnKeyDown(e);
            Invalidate();
        }

        #endregion

        #region Designer Methods

        /// <summary>
        /// Determines to serialize Size property or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeSize()
        {
            return false;
        }

        /// <summary>
        /// Determines to serialize SelectedDateTime property or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeSelectedDateTime()
        {
            return !SelectedDateTime.Equals(DateTime.MinValue);
        }

        /// <summary>
        /// Determines to serialize SelectedDateRange property or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeSelectedDateRange()
        {
            return SelectedDateRange != null && SelectedDateRange.Count > 0;
        }

        /// <summary>
        /// Determines to serialize DefaultCalendar property or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeDefaultCalendar()
        {
            return false;
        }

        /// <summary>
        /// Determines to serialize DefaultCulture property or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeDefaultCulture()
        {
            return false;
        }

        #endregion

        #region internal class ActRect
        internal class ActRect
        {

            #region Fields

            private bool m_bInvalidate = true;
            private TRectangleStatus m_state;

            #endregion

            #region Ctor

            public ActRect(Rectangle rc, TRectangleStatus state,
                TRectangleAction act, bool invalidate)
            {
                Rect = rc;
                m_state = state;
                m_bInvalidate = invalidate;
                Action = act;
            }

            public ActRect(Rectangle rc, TRectangleStatus state, TRectangleAction act)
                : this(rc, state, act, true)
            {

            }

            public ActRect(Rectangle rc, TRectangleAction act, object tag)
            {
                Rect = rc;
                m_state = TRectangleStatus.Normal;
                Action = act;
                Tag = tag;
            }

            public ActRect(Rectangle rc, TRectangleAction act)
                : this(rc, TRectangleStatus.Normal, act, true)
            {
            }

            public ActRect(Rectangle rc, TRectangleStatus state)
                : this(rc, state, TRectangleAction.None, true)
            {

            }

            public ActRect(Rectangle rc)
                : this(rc, TRectangleStatus.Normal, TRectangleAction.None, true)
            {

            }

            public ActRect()
                : this(Rectangle.Empty, TRectangleStatus.Normal, TRectangleAction.None, true)
            {

            }

            #endregion

            #region Properties

            public Rectangle Rect { get; set; }

            public TRectangleStatus State
            {
                get { return m_state; }
                set { m_state = TRectangleStatus.Normal; }
            }

            public bool InvalidateOnChange
            {
                get { return m_bInvalidate; }
                set { m_bInvalidate = value; }
            }

            public TRectangleAction Action { get; set; }

            public object Tag { get; set; }

            public bool IsFocused
            {
                get
                {
                    return (m_state & TRectangleStatus.Focused) ==
                        TRectangleStatus.Focused;
                }
            }

            public bool IsSelected
            {
                get
                {
                    return (m_state & TRectangleStatus.Selected) ==
                        TRectangleStatus.Selected;
                }
            }

            public bool IsActive
            {
                get { return (m_state & TRectangleStatus.Active) == TRectangleStatus.Active; }
            }

            #endregion

        }
        #endregion

    }
}