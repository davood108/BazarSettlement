#region using
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using RPNCalendar.Utilities;
#endregion

namespace RPNCalendar.UI.Controls
{

    #region public class DataGridViewPersianDateTimePickerColumn : DataGridViewColumn
    /// <summary>
    /// كلاس تقویم پارسی كنترل جدول رایان پرتونگار
    /// </summary>
    [ToolboxBitmap(typeof(DataGridViewPersianDateTimePickerColumn),
        "Images\\DataGridViewPersianDateTimePickerColumn.bmp")]
    [Description("كلاس تقویم پارسی كنترل جدول رایان پرتونگار")]
    public class DataGridViewPersianDateTimePickerColumn : DataGridViewColumn
    {

        #region Constructor
        /// <summary>
        /// سازنده پیش فرض كلاس
        /// </summary>
        public DataGridViewPersianDateTimePickerColumn()
            : base(new DataGridViewFADateTimePickerCell())
        {

        }
        #endregion

        #region Properties

        #region ThemeTypes Theme
        [DefaultValue("Office2003")]
        /// <summary>
        /// قالب ظاهر كنترل
        /// </summary>
        public ThemeTypes Theme
        {
            get { return PersianDatePickerCellTemplate.Theme; }
            set { PersianDatePickerCellTemplate.Theme = value; }
        }
        #endregion

        #region StringAlignment VerticalAlignment
        [DefaultValue("Center")]
        /// <summary>
        /// محل قرارگیری عمودی متن
        /// </summary>
        public StringAlignment VerticalAlignment
        {
            get { return PersianDatePickerCellTemplate.VerticalAlignment; }
            set { PersianDatePickerCellTemplate.VerticalAlignment = value; }
        }
        #endregion

        #region StringAlignment HorizontalAlignment
        [DefaultValue("Center")]
        /// <summary>
        /// محل قرارگیری افقی متن در جعبه متن
        /// </summary>
        public StringAlignment HorizontalAlignment
        {
            get { return PersianDatePickerCellTemplate.HorizontalAlignment; }
            set { PersianDatePickerCellTemplate.HorizontalAlignment = value; }
        }
        #endregion

        #region DateTime SelectedDateTime
        /// <summary>
        /// خصوصیت بدست آوردن مقدار تاریخ پیش فرض
        /// </summary>
        [Category("Behavior")]
        [Description("خصوصیت بدست آوردن مقدار تاریخ پیش فرض")]
        public DateTime SelectedDateTime
        {
            // The maximum number of radio buttons to display in the cells of the column.
            get
            {
                if (PersianDatePickerCellTemplate == null)
                    throw new InvalidOperationException(
                        "Operation cannot be completed because this " +
                        "DataGridViewColumn does not have a CellTemplate.");
                return PersianDatePickerCellTemplate.SelectedDateTime;
            }
            set
            {
                if (SelectedDateTime != value)
                {
                    PersianDatePickerCellTemplate.SelectedDateTime = value;
                    if (DataGridView != null)
                    {
                        DataGridViewRowCollection dataGridViewRows = DataGridView.Rows;
                        int rowCount = dataGridViewRows.Count;

                        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
                        {
                            DataGridViewRow dataGridViewRow =
                                dataGridViewRows.SharedRow(rowIndex);
                            var dataGridViewCell =
                                dataGridViewRow.Cells[Index] as DataGridViewFADateTimePickerCell;
                            if (dataGridViewCell != null)
                                dataGridViewCell.SelectedDateTime = value;
                        }
                        DataGridView.InvalidateColumn(Index);
                        // ToDo: Add code to autosize the column and rows, 
                        // the column headers, 
                        // the row headers, depending on the autosize settings of the grid.
                    }
                }
            }
        }
        #endregion

        #region public override DataGridViewCell CellTemplate
        /// <summary>
        /// تعیین تنظیمات قالب سلول ها
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override DataGridViewCell CellTemplate
        {
            get { return base.CellTemplate; }
            set
            {
                var dataGridViewFADateTimePickerCell =
                    value as DataGridViewFADateTimePickerCell;
                if (value != null && dataGridViewFADateTimePickerCell == null)
                    throw new InvalidCastException(
                        "Value provided for CellTemplate must be of type " +
                        "DataGridViewRadioButtonElements.DataGridViewRadioButtonCell " +
                        "or derive from it.");
                base.CellTemplate = value;
            }
        }
        #endregion

        #region private DataGridViewFADateTimePickerCell PersianDatePickerCellTemplate
        /// <summary>
        /// Small utility function that returns the template cell 
        /// as a DataGridViewRadioButtonCell.
        /// </summary>
        private DataGridViewFADateTimePickerCell PersianDatePickerCellTemplate
        {
            get { return (DataGridViewFADateTimePickerCell)CellTemplate; }
        }
        #endregion

        #endregion

        #region Overrided Methods

        #region public override string ToString()
        /// <summary>
        /// مقدار رشته ای ردیف مورد نظر را باز میگرداند
        /// </summary>
        public override string ToString()
        {
            StringBuilder StringCollection = new StringBuilder(64);
            StringCollection.Append("DataGridViewPersianDateTimePickerCell { Name=");
            StringCollection.Append(Name);
            StringCollection.Append(", Index=");
            StringCollection.Append(Index.ToString(CultureInfo.CurrentCulture));
            StringCollection.Append(" }");
            return StringCollection.ToString();
        }
        #endregion

        #endregion

    }
    #endregion

    #region public class DataGridViewFADateTimePickerCell : DataGridViewTextBoxCell
    public class DataGridViewFADateTimePickerCell : DataGridViewTextBoxCell
    {

        #region Fields
        private static readonly Type _EditType =
            typeof(DataGridViewPersianDateTimePickerEditor);
        private static readonly Type _ValueType = typeof(DateTime);
        private FormatInfoTypes _FormatInfoTypes;
        #endregion

        #region Properties

        #region FormatInfoTypes FormatInfo
        /// <summary>
        /// خصوصیت تعیین فرمت رشته ای تاریخ
        /// </summary>
        [Description("خصوصیت تعیین فرمت رشته ای تاریخ")]
        [DefaultValue(typeof(FormatInfoTypes), "ShortDate")]
        public FormatInfoTypes FormatInfo
        {
            get { return _FormatInfoTypes; }
            set
            {
                if (_FormatInfoTypes == value)
                    return;
                _FormatInfoTypes = value;
            }
        }
        #endregion

        #region ThemeTypes Theme
        /// <summary>
        /// نوع قالب سلول
        /// </summary>
        [Description("نوع قالب سلول")]
        public ThemeTypes Theme
        {
            get
            {
                if (DataGridView == null || EditingPersianDatePicker == null)
                    return ThemeTypes.Office2003;
                return EditingPersianDatePicker.Theme;
            }
            set
            {
                if (DataGridView != null && EditingPersianDatePicker != null)
                    EditingPersianDatePicker.Theme = value;
            }
        }
        #endregion

        #region DateTime SelectedDateTime
        public DateTime SelectedDateTime { get; set; }
        #endregion

        #region StringAlignment VerticalAlignment
        [DefaultValue("Center")]
        public StringAlignment VerticalAlignment { get; set; }
        #endregion

        #region StringAlignment HorizontalAlignment
        [DefaultValue("Center")]
        public StringAlignment HorizontalAlignment { get; set; }
        #endregion

        #region public override Type EditType
        public override Type EditType
        {
            get { return _EditType; }
        }
        #endregion

        #region public override Type ValueType
        public override Type ValueType
        {
            get { return _ValueType; }
        }
        #endregion

        #region private DataGridViewPersianDateTimePickerEditor EditingPersianDatePicker
        /// <summary>
        /// Returns the current DataGridView EditingControl as a 
        /// DataGridViewNumericUpDownEditingControl control
        /// </summary>
        private DataGridViewPersianDateTimePickerEditor EditingPersianDatePicker
        {
            get
            {
                return DataGridView.EditingControl as DataGridViewPersianDateTimePickerEditor;
            }
        }
        #endregion
        
        #endregion

        #region Methods

        #region public override void InitializeEditingControl(...)
        public override void InitializeEditingControl(int rowIndex,
            object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex,
                initialFormattedValue, dataGridViewCellStyle);
            var editor =
                DataGridView.EditingControl as DataGridViewPersianDateTimePickerEditor;

            if (editor != null)
            {
                editor.RightToLeft = DataGridView.RightToLeft;
                editor.Theme = Theme;
                string formattedValue = initialFormattedValue.ToString();

                if (string.IsNullOrEmpty(formattedValue))
                {
                    editor.SelectedDateTime = DateTime.Now;
                    editor._PersianMonthViewContainer.MonthViewControl.SetNoneDay();
                }
                else
                {
                    editor.SelectedDateTime = DateTime.Parse(formattedValue);
                }
            }
        }
        #endregion

        #region protected override void Paint(...)
        protected override void Paint(Graphics graphics, Rectangle clipBounds,
            Rectangle cellBounds, int rowIndex,
            DataGridViewElementStates cellState, object value, object formattedValue,
            string errorText, DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {

            if (DataGridView == null)
                return;

            // First paint the borders and background of the cell.
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value,
                formattedValue, errorText, cellStyle, advancedBorderStyle,
                paintParts & ~(DataGridViewPaintParts.ErrorIcon |
                DataGridViewPaintParts.ContentForeground));

            Point ptCurrentCell = DataGridView.CurrentCellAddress;
            bool cellCurrent = ptCurrentCell.X == ColumnIndex &&
                ptCurrentCell.Y == rowIndex;
            bool cellEdited = cellCurrent && DataGridView.EditingControl != null;

            // If the cell is in editing mode, there is nothing else to paint
            if (!cellEdited && value != null && !string.IsNullOrEmpty(value.ToString()))
            {
                PersianDate pd = null;
                if (value is DateTime)
                    pd = (DateTime)value;
                else if (value is string)
                    pd = PersianDate.Parse(value.ToString());

                if (pd != null)
                {
                    using (var brFG = new SolidBrush(cellStyle.ForeColor))
                    using (var brSelected = new SolidBrush(cellStyle.SelectionForeColor))
                    using (var fmt = new StringFormat())
                    {
                        fmt.LineAlignment = HorizontalAlignment;
                        fmt.Alignment = VerticalAlignment;
                        fmt.Trimming = StringTrimming.None;
                        fmt.FormatFlags = StringFormatFlags.LineLimit;

                        graphics.DrawString(pd.ToString(), cellStyle.Font,
                            IsInState(cellState, DataGridViewElementStates.Selected) ?
                            brSelected : brFG, cellBounds, fmt);
                    }
                }
            }

            if (PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
                base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, 
                    formattedValue, errorText, 
                    cellStyle, advancedBorderStyle, DataGridViewPaintParts.ErrorIcon);
        }
        #endregion

        #region private static bool IsInState(...)
        private static bool IsInState(DataGridViewElementStates currentState, 
            DataGridViewElementStates checkState)
        {
            return (currentState & checkState) != 0;
        }
        #endregion

        #region private static bool PartPainted(...)
        /// <summary>
        /// Little utility function called by the Paint function to see if a 
        /// particular part needs to be painted. 
        /// </summary>
        private static bool PartPainted(DataGridViewPaintParts paintParts, 
            DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }
        #endregion

        #region private bool OwnsEditor(int rowIndex)
        /// <summary>
        /// Determines whether this cell, at the given row index, 
        /// shows the grid's editing control or not.
        /// The row index needs to be provided as a parameter because this 
        /// cell may be shared among multiple rows.
        /// </summary>
        private bool OwnsEditor(int rowIndex)
        {
            if (rowIndex == -1 || DataGridView == null)
                return false;

            var editor = DataGridView.EditingControl as DataGridViewPersianDateTimePickerEditor;
            return editor != null && rowIndex == editor.EditingControlRowIndex;
        }
        #endregion

        #region internal void SetValue(int rowIndex, DateTime value)
        internal void SetValue(int rowIndex, DateTime value)
        {
            SelectedDateTime = value;
            if (OwnsEditor(rowIndex))
                EditingPersianDatePicker.SelectedDateTime = value;
        }
        #endregion
        
        #endregion

    }
    #endregion

    #region public class DataGridViewPersianDateTimePickerEditor
    public class DataGridViewPersianDateTimePickerEditor : 
        PersianDatePicker, IDataGridViewEditingControl
    {

        #region Ctor
        public DataGridViewPersianDateTimePickerEditor()
        {
            SelectedDateTimeChanged += OnInternalSelectedDateTimeChanged;
        }
        #endregion

        #region Event Handlers

        #region OnInternalSelectedDateTimeChanged
        private void OnInternalSelectedDateTimeChanged(object sender, EventArgs e)
        {
            EditingControlValueChanged = true;
            NotifyDataGridViewOfValueChange();
        }
        #endregion

        #endregion

        #region Methods

        #region private void NotifyDataGridViewOfValueChange()
        /// <summary>
        /// Small utility function that updates the local dirty state and 
        /// notifies the grid of the value change.
        /// </summary>
        private void NotifyDataGridViewOfValueChange()
        {
            if (EditingControlValueChanged)
                EditingControlDataGridView.NotifyCurrentCellDirty(true);
        }
        #endregion

        #endregion

        #region IDataGridViewEditingControl Members Implementation

        #region Properties

        #region DataGridView EditingControlDataGridView
        ///<summary>
        ///Gets or sets the <see cref="T:System.Windows.Forms.DataGridView"></see> 
        /// that contains the cell.
        ///</summary>
        ///<returns>
        /// The <see cref="T:System.Windows.Forms.DataGridView"></see> that contains 
        /// the <see cref="T:System.Windows.Forms.DataGridViewCell"></see> that is 
        /// being edited; null if there is no associated 
        /// <see cref="T:System.Windows.Forms.DataGridView"></see>.
        ///</returns>
        public DataGridView EditingControlDataGridView { get; set; }
        #endregion

        #region object EditingControlFormattedValue
        ///<summary>
        ///Gets or sets the formatted value of the cell being modified by the editor.
        ///</summary>
        ///<returns>
        /// An <see cref="T:System.Object"></see> that 
        /// represents the formatted value of the cell.
        ///</returns>
        public object EditingControlFormattedValue
        {
            get { return SelectedDateTime; }
            set { SelectedDateTime = (DateTime)value; }
        }
        #endregion

        #region int EditingControlRowIndex
        ///<summary>
        /// Gets or sets the index of the hosting cell's parent row.
        ///</summary>
        ///<returns>
        /// The index of the row that contains the cell, or –1 if there is no parent row.
        ///</returns>
        public int EditingControlRowIndex { get; set; }
        #endregion

        #region Cursor EditingPanelCursor
        ///<summary>
        /// Gets the cursor used when the mouse pointer is over the 
        /// <see cref="P:System.Windows.Forms.DataGridView.EditingPanel"></see> 
        /// but not over the editing control.
        ///</summary>
        ///<returns>
        /// A <see cref="T:System.Windows.Forms.Cursor"></see> that represents the 
        /// mouse pointer used for the editing panel. 
        ///</returns>
        public Cursor EditingPanelCursor
        {
            get { return Cursors.Default; }
        }
        #endregion

        #region bool RepositionEditingControlOnValueChange
        ///<summary>
        /// Gets or sets a value indicating whether the cell contents need to 
        /// be repositioned whenever the value changes.
        ///</summary>
        ///<returns>
        /// true if the contents need to be repositioned; otherwise, false.
        ///</returns>
        public bool RepositionEditingControlOnValueChange
        {
            get { return true; }
        }
        #endregion

        #region virtual bool EditingControlValueChanged
        /// <summary>
        /// Property which indicates whether the value of the 
        /// editing control has changed or not
        /// </summary>
        public virtual bool EditingControlValueChanged { get; set; }
        #endregion

        #endregion

        #region Methods

        #region public void ApplyCellStyleToEditingControl(...)
        ///<summary>
        ///Changes the control's user interface (UI) to be 
        /// consistent with the specified cell style.
        ///</summary>
        ///<param name="dataGridViewCellStyle">The 
        /// <see cref="T:System.Windows.Forms.DataGridViewCellStyle"></see> 
        /// to use as the model for the UI.</param>
        public void ApplyCellStyleToEditingControl(
            DataGridViewCellStyle dataGridViewCellStyle)
        {

        }
        #endregion

        #region public bool EditingControlWantsInputKey(...)
        ///<summary>
        ///Determines whether the specified key is a regular input key that the editing 
        /// control should process or a special key that 
        /// the <see cref="T:System.Windows.Forms.DataGridView"></see> should process.
        ///</summary>
        ///<returns>
        /// true if the specified key is a regular input key that should be 
        /// handled by the editing control; otherwise, false.
        ///</returns>
        /// <param name="keyData">A 
        /// <see cref="T:System.Windows.Forms.Keys"></see> that represents 
        /// the key that was pressed.</param>
        /// <param name="dataGridViewWantsInputKey">true when the 
        /// <see cref="T:System.Windows.Forms.DataGridView"></see> 
        /// wants to process the <see cref="T:System.Windows.Forms.Keys"></see> in 
        /// keyData; otherwise, false.</param>
        public bool EditingControlWantsInputKey(Keys keyData,
            bool dataGridViewWantsInputKey)
        {
            return true;
        }
        #endregion

        #region public object GetEditingControlFormattedValue(...)
        ///<summary>
        ///Retrieves the formatted value of the cell.
        ///</summary>
        ///<returns>
        /// An <see cref="T:System.Object"></see> that represents the 
        /// formatted version of the cell contents.
        ///</returns>
        ///<param name="context">A bitwise combination of 
        /// <see cref="T:System.Windows.Forms.DataGridViewDataErrorContexts"></see> 
        /// values that specifies the context in which the data is needed.</param>
        public object GetEditingControlFormattedValue(
            DataGridViewDataErrorContexts context)
        {
            if (_PersianMonthViewContainer.MonthViewControl.IsNull)
                return string.Empty;
            return SelectedDateTime.ToString("G");
        }
        #endregion

        #region public void PrepareEditingControlForEdit(bool selectAll)
        ///<summary>
        /// Prepares the currently selected cell for editing.
        ///</summary>
        ///<param name="selectAll">true to select all of the cell's content; otherwise, 
        /// false.</param>
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            if (selectAll)
                TextBox.SelectAll();
            else
                TextBox.SelectionStart = TextBox.Text.Length;
        }
        #endregion

        #endregion
        
        #endregion
        
    }
    #endregion
    
}