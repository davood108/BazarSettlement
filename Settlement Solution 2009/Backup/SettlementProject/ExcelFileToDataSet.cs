﻿#region using
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.DbLayer;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.InputBaseData
{
    /// <summary>
    /// كلاس تبدیل فایل اكسل به داده دات نت
    /// </summary>
    class ExcelFileToDataSet
    {
        #region Fields
        private String _SheetName;
        private String _SourceFile;
        private OleDbConnection _OleDbConnection;
        private OleDbDataAdapter _OleDbDataAdapter;
/*
        private String Code;
*/
        readonly DbSMDataContext _DbSM = new DbSMDataContext
            (Classes.DbBizClass.dbConnStr);




        #endregion

        #region Properties

        #region String SheetName
        public String SheetName
        {
            get { return _SheetName; }
            set { _SheetName = value; }
        }
        #endregion

        #region String SourceFile
        public String SourceFile
        {
            get { return _SourceFile; }
            set { _SourceFile = value; }
        }
        #endregion

        #endregion

        #region Methods

        #region Base
    
        #region public Boolean ConnectionOpen()
        /// <summary>
        /// تابع باز كردن ارتباط با فایل اكسل
        /// </summary>
        /// <returns>صحت انجام كار</returns>
        public Boolean ConnectionOpen()
        {
            try
            {
                //_OleDbConnection = new OleDbConnection("provider= Microsoft.ACE.OLEDB.12.0 " +
                //                                       ";data Source =" + _SourceFile +
                //                                       "; Extended Properties = \"Excel 12.0 Xml;HDR=YES\";");

                _OleDbConnection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0" +
                                                      ";data Source=" + _SourceFile +
                                                      ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"");
    
                _OleDbConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show("error" + e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region public Boolean ConnectionOpen(String ConnectionString)
        public Boolean ConnectionOpen(String ConnectionString)
        {
            try
            {
                _OleDbConnection = new OleDbConnection("provider= Microsoft.ACE.OLEDB.12.0 " +
                                                       ";data Source =" +
                                                       ConnectionString + "; Extended Properties = \"Excel 12.0 Xml;HDR=YES\";");
                _OleDbConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show("error" + e.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region public DataSet LoadFile()
        /// <summary>
        /// خواندن اطلاعات از اكسل
        /// </summary>
        /// <returns>صحت انجام كار</returns>
        public DataSet LoadFile()
        {
            OleDbCommand ExcelOleDbCommand = new OleDbCommand("select * from ["
                                                              + _SheetName + "$]");
            ExcelOleDbCommand.Connection = _OleDbConnection;
            _OleDbDataAdapter = new OleDbDataAdapter();
            _OleDbDataAdapter.SelectCommand = ExcelOleDbCommand;

            DataSet TempDataSet = new DataSet();
            try
            {
                _OleDbDataAdapter.Fill(TempDataSet);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا");
                return null;
            }
            return TempDataSet;
        }
        #endregion

        #region public DataSet LoadFile(String TheSheetName)
         //<summary>
         //خواندن اطلاعات از اكسل
         //</summary>
         //<param name="TheSheetName"></param>
         //<returns>صحت انجام كار</returns>
        public DataSet LoadFile(String TheSheetName)
        {
            OleDbCommand ExcelOleDbCommand = new OleDbCommand("select * from ["
                                                              + TheSheetName + "$]");
            ExcelOleDbCommand.Connection = _OleDbConnection;
            _OleDbDataAdapter = new OleDbDataAdapter();
            _OleDbDataAdapter.SelectCommand = ExcelOleDbCommand;

            DataSet TempDataSet = new DataSet();
            try
            {
                _OleDbDataAdapter.Fill(TempDataSet);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا");
                return null;
            }
            return TempDataSet;
        }
        #endregion

        #endregion

        #region MazandaranExcel  
 


        #region Boolean FillExcelDataToBillMD
        public Boolean  FillExcelDataToBillMD(DataSet NewDataSet, string PartName, string dateT)
        {
            {
                string _PartName = PartName;

               // int i;
                switch (_PartName)
                {
                    case "Sellers":
                        //#region Sellers
                        //for (i = 0; i < NewDataSet.Tables[0].Rows.Count; i++)
                        //{
                        //    #region UnitType
                        //    String UnitType = NewDataSet.Tables[0].Rows[i][2].ToString();
                        //    if (UnitType == "CC")
                        //        UnitType = "C";
                        //    #endregion
                        //    try
                        //    {
                        //        _DbSM.InsertBillMD(NewDataSet.Tables[0].Rows[i][1].ToString()
                        //                           , UnitType
                        //                           , NewDataSet.Tables[0].Rows[i][3].ToString()
                        //                           , Convert.ToByte(NewDataSet.Tables[0].Rows[i][4].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][5].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][6].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][7].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][8].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][9].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][10].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][11].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][12].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][13].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][14].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][15].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][16].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][17].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][18].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][19].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][20].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][21].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][22].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][23].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][24].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][25].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][26].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][27].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][28].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][29].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][30].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][31].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][32].ToString())
                        //                           , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][33].ToString())
                        //            );
                        //    }
                        //        #region Catch
                        //    catch (Exception)
                        //    {
                        //        const String ErrorMessage =
                        //            "خطادر واردكردن اطلاعات جدول " + "BillM" + " در  بانك اطلاعات .\n" +
                        //            "موارد زیر را بررسی نمایید:\n" +
                        //            "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                        //        PersianMessageBox.Show(ErrorMessage, "خطا!",
                        //                               MessageBoxButtons.OK, MessageBoxIcon.Error,
                        //                               MessageBoxDefaultButton.Button1);

                        //        return false;
                        //    }
                        //    #endregion
                        //}
                        //#endregion
                        break;
                    case "Buyer":
                        //#region Buyers
                        //for (i = 0; i < NewDataSet.Tables[0].Rows.Count; i++)
                        //{
                        //    try
                        //    {
                        //        _DbSM.InsertBillMDB("برق منطقه ای مازندران"
                        //                                  , Convert.ToString(NewDataSet.Tables[0].Rows[i][0])
                        //                                  , (byte?)Convert.ToInt32(NewDataSet.Tables[0].Rows[i][1])
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][2].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][3].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][4].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][5].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][6].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][7].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][8].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][9].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][10].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][11].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][12].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][13].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][14].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][15].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][16].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][17].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][18].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][19].ToString())
                        //                                  , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i][20].ToString())
                        //                    );

                        //    }
                        //        #region Catch
                        //    catch (Exception)
                        //    {
                        //        const String ErrorMessage =
                        //            "خطادر واردكردن اطلاعات جدول " + "BillM" + " در  بانك اطلاعات .\n" +
                        //            "موارد زیر را بررسی نمایید:\n" +
                        //            "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                        //        PersianMessageBox.Show(ErrorMessage, "خطا!",
                        //                               MessageBoxButtons.OK, MessageBoxIcon.Error,
                        //                               MessageBoxDefaultButton.Button1);

                        //        return false;
                        //    }
                        //    #endregion
                        //}
                        //#endregion
                        break;
                    case "Transfer":
                        //#region Transfer
                        //i = NewDataSet.Tables[0].Rows.Count;
                        //_DbSM.InsertBillMT("برق منطقه ای مازندران", dateT
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 28][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 27][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 26][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 25][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 24][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 23][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 22][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 21][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 20][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 19][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 18][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 17][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 16][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 15][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 14][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 13][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 12][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 11][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 10][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 9][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 8][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 7][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 6][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 5][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 4][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 3][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 2][2].ToString())
                        //                     , Convert.ToDecimal(NewDataSet.Tables[0].Rows[i - 1][2].ToString())
                        //    );

                        //#endregion
                        break;
                }

                return true;
            }


        }
        #endregion
        



        #endregion
        #endregion

        
    }
}