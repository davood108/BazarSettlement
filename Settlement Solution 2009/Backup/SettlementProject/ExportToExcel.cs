using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using FlexCel.Report;
using RPNCalendar.Utilities;

namespace MohanirPouya
{
    internal class ExportToExcel
    {

        private  string today;


  
        private FlexCelReport Report;

        private static void createsubdir(string dir)
        {
            var di = new DirectoryInfo(dir);
            if (!di.Exists)
            {
                di.Create();
            }
        }

        private void Export(DataSet dataset, out string DataPath)
        {
            PersianDate OccuredDate = PersianDateConverter.ToPersianDate
            (DateTime.Now.Date);
            
            today = OccuredDate.ToString("d");
            Report = new FlexCelReport {DeleteEmptyRanges = false, ErrorActions = TErrorActions.None};
            Report.ClearTables();
            Report.AddTable(dataset);
            DataPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).Replace(@"file:\", "") + @"\";
            if (!File.Exists(DataPath + "Generic Reports.template.xls"))
            {
                DataPath = DataPath + @"..\..\";
            }
        }

        public void toexcel(string reportcaption, string date, DataSet dataset,string tempfile, string savedir,
                            string savefile, bool answer)
        {
            string DataPath;
            string savefilename = savedir + savefile;
            createsubdir(savedir);
            Export(dataset, out DataPath);
            File.Delete(savefilename);
            string date_m = date;
            Report.SetValue("Date-m", date_m);
            Report.SetValue("Date", date);
            Report.SetValue("today", today);
            Report.SetValue("ReportCaption", reportcaption);
            try
            {
                Report.Run(tempfile, savefilename);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
 
            }
     
            if (answer &&
                (MessageBox.Show("آیا مایل هستید فایل ساخته شده را مشاهده نمائید؟", "تائید", MessageBoxButtons.YesNo) ==
                 DialogResult.Yes))
            {
                Process.Start(savefilename);
            }
        }
        
        public void toexcel(string reportcaption, string date1, string date2, DataSet dataset, string tempfile,
                            string savedir, string savefile, bool answer)
        {
            string DataPath;
            string savefilename = savedir + savefile;
            createsubdir(savedir);
            Export(dataset, out DataPath);
            File.Delete(savefilename);
           string date_m = date2;
            Report.SetValue("Date-m", date_m);
            Report.SetValue("Date", date1);
            Report.SetValue("today", today);
            Report.SetValue("ReportCaption", reportcaption);
            Report.Run(DataPath + tempfile, savefilename);
            if (answer &&
                (MessageBox.Show("آیا مایل هستید فایل ساخته شده را مشاهده نمائید؟", "تائید", MessageBoxButtons.YesNo) ==
                 DialogResult.Yes))
            {
                Process.Start(savefilename);
            }
        }
    }
}