﻿namespace MohanirPouya.Forms.Start
{
    partial class DbConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MyWizard = new DevComponents.DotNetBar.Wizard();
            this.PSelectConnection = new DevComponents.DotNetBar.WizardPage();
            this.btnConnectionDetails = new DevComponents.DotNetBar.ButtonX();
            this.chkUseLastConnection = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkMakeNewConnection = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.lblText3 = new System.Windows.Forms.Label();
            this.lblText2 = new System.Windows.Forms.Label();
            this.lblText1 = new System.Windows.Forms.Label();
            this.PSetConnectionString = new DevComponents.DotNetBar.WizardPage();
            this.textBoxX2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label3 = new System.Windows.Forms.Label();
            this.lblServerName = new System.Windows.Forms.Label();
            this.txtServerName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblLocalDatabase = new System.Windows.Forms.Label();
            this.lblLocalServerName = new System.Windows.Forms.Label();
            this.txtLocalServerName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblIPAddress = new System.Windows.Forms.Label();
            this.txtIPAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboLocalDatabases = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.chkNetworkDatabase = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkLocalDatabase = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.lblSelectConnectionType = new System.Windows.Forms.Label();
            this.lblText4 = new System.Windows.Forms.Label();
            this.PCheckPassword = new DevComponents.DotNetBar.WizardPage();
            this.txtPassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtUsername = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblText5 = new System.Windows.Forms.Label();
            this.chkSaveUserName = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEnterUNameAndPass = new System.Windows.Forms.Label();
            this.PictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.FormTimer = new System.Windows.Forms.Timer(this.components);
            this.MyWizard.SuspendLayout();
            this.PSelectConnection.SuspendLayout();
            this.PSetConnectionString.SuspendLayout();
            this.PCheckPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // MyWizard
            // 
            this.MyWizard.BackButtonText = "< قبلی";
            this.MyWizard.BackButtonWidth = 70;
            this.MyWizard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(229)))), ((int)(((byte)(253)))));
            this.MyWizard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MyWizard.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.MyWizard.CancelButtonText = "خروج";
            this.MyWizard.CancelButtonWidth = 70;
            this.MyWizard.Cursor = System.Windows.Forms.Cursors.Default;
            this.MyWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MyWizard.FinishButtonTabIndex = 3;
            this.MyWizard.FinishButtonText = "اتصال";
            this.MyWizard.FinishButtonWidth = 70;
            this.MyWizard.FooterHeight = 35;
            // 
            // 
            // 
            this.MyWizard.FooterStyle.BackColor = System.Drawing.Color.Transparent;
            this.MyWizard.ForeColor = System.Drawing.Color.Navy;
            this.MyWizard.HeaderCaptionFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyWizard.HeaderDescriptionIndent = 45;
            this.MyWizard.HeaderHeight = 45;
            this.MyWizard.HeaderImageAlignment = DevComponents.DotNetBar.eWizardTitleImageAlignment.Left;
            this.MyWizard.HeaderImageSize = new System.Drawing.Size(37, 37);
            // 
            // 
            // 
            this.MyWizard.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
            this.MyWizard.HeaderStyle.BackColorGradientAngle = 90;
            this.MyWizard.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.MyWizard.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.MyWizard.HeaderStyle.BorderBottomWidth = 1;
            this.MyWizard.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.MyWizard.HeaderStyle.BorderLeftWidth = 1;
            this.MyWizard.HeaderStyle.BorderRightWidth = 1;
            this.MyWizard.HeaderStyle.BorderTopWidth = 1;
            this.MyWizard.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.MyWizard.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyWizard.HeaderTitleIndent = 45;
            this.MyWizard.HelpButtonText = "كمك";
            this.MyWizard.HelpButtonVisible = false;
            this.MyWizard.HelpButtonWidth = 70;
            this.MyWizard.Location = new System.Drawing.Point(0, 0);
            this.MyWizard.Name = "MyWizard";
            this.MyWizard.NextButtonText = "بعدی >";
            this.MyWizard.NextButtonWidth = 70;
            this.MyWizard.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MyWizard.Size = new System.Drawing.Size(404, 272);
            this.MyWizard.TabIndex = 0;
            this.MyWizard.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.PSelectConnection,
            this.PSetConnectionString,
            this.PCheckPassword});
            this.MyWizard.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.btnExit_Click);
            this.MyWizard.BackButtonClick += new System.ComponentModel.CancelEventHandler(this.btnBack_Click);
            this.MyWizard.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.btnNext_Click);
            this.MyWizard.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.btnAcceptPassword);
            // 
            // PSelectConnection
            // 
            this.PSelectConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PSelectConnection.BackColor = System.Drawing.Color.Transparent;
            this.PSelectConnection.CanvasColor = System.Drawing.Color.Transparent;
            this.PSelectConnection.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PSelectConnection.Controls.Add(this.btnConnectionDetails);
            this.PSelectConnection.Controls.Add(this.chkUseLastConnection);
            this.PSelectConnection.Controls.Add(this.chkMakeNewConnection);
            this.PSelectConnection.Controls.Add(this.lblText3);
            this.PSelectConnection.Controls.Add(this.lblText2);
            this.PSelectConnection.Controls.Add(this.lblText1);
            this.PSelectConnection.Location = new System.Drawing.Point(7, 57);
            this.PSelectConnection.Name = "PSelectConnection";
            this.PSelectConnection.PageDescription = "انتخاب نوع دسترسی به تنظیمات";
            this.PSelectConnection.PageTitle = "ارتباط با بانك اطلاعاتی";
            this.PSelectConnection.Size = new System.Drawing.Size(390, 168);
            this.PSelectConnection.TabIndex = 8;
            // 
            // btnConnectionDetails
            // 
            this.btnConnectionDetails.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnConnectionDetails.Enabled = false;
            this.btnConnectionDetails.Location = new System.Drawing.Point(104, 102);
            this.btnConnectionDetails.Name = "btnConnectionDetails";
            this.btnConnectionDetails.Size = new System.Drawing.Size(143, 23);
            this.btnConnectionDetails.TabIndex = 1;
            this.btnConnectionDetails.Text = "مشاهده جزئیات اتصال ...";
            this.btnConnectionDetails.Click += new System.EventHandler(this.btnConnectionDetails_Click);
            // 
            // chkUseLastConnection
            // 
            this.chkUseLastConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkUseLastConnection.BackColor = System.Drawing.Color.Transparent;
            this.chkUseLastConnection.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkUseLastConnection.Location = new System.Drawing.Point(253, 102);
            this.chkUseLastConnection.Name = "chkUseLastConnection";
            this.chkUseLastConnection.Size = new System.Drawing.Size(126, 20);
            this.chkUseLastConnection.TabIndex = 10;
            this.chkUseLastConnection.Text = "استفاده از آخرین اتصال";
            this.chkUseLastConnection.CheckedChanged += new System.EventHandler(this.chkUseLastConnection_CheckedChanged);
            // 
            // chkMakeNewConnection
            // 
            this.chkMakeNewConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMakeNewConnection.BackColor = System.Drawing.Color.Transparent;
            this.chkMakeNewConnection.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkMakeNewConnection.Location = new System.Drawing.Point(274, 79);
            this.chkMakeNewConnection.Name = "chkMakeNewConnection";
            this.chkMakeNewConnection.Size = new System.Drawing.Size(105, 20);
            this.chkMakeNewConnection.TabIndex = 10;
            this.chkMakeNewConnection.Text = "ایجاد اتصال جدید";
            // 
            // lblText3
            // 
            this.lblText3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText3.AutoSize = true;
            this.lblText3.BackColor = System.Drawing.Color.Transparent;
            this.lblText3.Location = new System.Drawing.Point(153, 152);
            this.lblText3.Name = "lblText3";
            this.lblText3.Size = new System.Drawing.Size(226, 13);
            this.lblText3.TabIndex = 9;
            this.lblText3.Text = "پس از انتخاب ، بر روی گزینه \"بعدی\" كلیك نمایید.";
            // 
            // lblText2
            // 
            this.lblText2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText2.BackColor = System.Drawing.Color.Transparent;
            this.lblText2.Location = new System.Drawing.Point(10, 30);
            this.lblText2.Name = "lblText2";
            this.lblText2.Size = new System.Drawing.Size(369, 46);
            this.lblText2.TabIndex = 7;
            this.lblText2.Text = "در صورتی كه قبلا اتصالی به بانك اطلاعاتی برقرار كرده باشید ، می توانید با استفاده" +
                " از آن به بانك اطلاعاتی متصل شوید. در غیر این صورت می باید یك اتصال جدید ایجاد ن" +
                "مایید.";
            // 
            // lblText1
            // 
            this.lblText1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText1.AutoSize = true;
            this.lblText1.BackColor = System.Drawing.Color.Transparent;
            this.lblText1.Location = new System.Drawing.Point(76, 7);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(303, 13);
            this.lblText1.TabIndex = 7;
            this.lblText1.Text = "این بخش شما را برای ارتباط با بانك اطلاعاتی راهنمایی می نماید.";
            // 
            // PSetConnectionString
            // 
            this.PSetConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PSetConnectionString.BackColor = System.Drawing.Color.Transparent;
            this.PSetConnectionString.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PSetConnectionString.Controls.Add(this.textBoxX2);
            this.PSetConnectionString.Controls.Add(this.label4);
            this.PSetConnectionString.Controls.Add(this.textBoxX1);
            this.PSetConnectionString.Controls.Add(this.label3);
            this.PSetConnectionString.Controls.Add(this.lblServerName);
            this.PSetConnectionString.Controls.Add(this.txtServerName);
            this.PSetConnectionString.Controls.Add(this.lblLocalDatabase);
            this.PSetConnectionString.Controls.Add(this.lblLocalServerName);
            this.PSetConnectionString.Controls.Add(this.txtLocalServerName);
            this.PSetConnectionString.Controls.Add(this.lblIPAddress);
            this.PSetConnectionString.Controls.Add(this.txtIPAddress);
            this.PSetConnectionString.Controls.Add(this.cboLocalDatabases);
            this.PSetConnectionString.Controls.Add(this.chkNetworkDatabase);
            this.PSetConnectionString.Controls.Add(this.chkLocalDatabase);
            this.PSetConnectionString.Controls.Add(this.lblSelectConnectionType);
            this.PSetConnectionString.Controls.Add(this.lblText4);
            this.PSetConnectionString.Location = new System.Drawing.Point(7, 57);
            this.PSetConnectionString.Name = "PSetConnectionString";
            this.PSetConnectionString.PageDescription = "انتخاب نوع ارتباط جدید";
            this.PSetConnectionString.PageTitle = "ارتباط با بانك اطلاعاتی";
            this.PSetConnectionString.Size = new System.Drawing.Size(390, 168);
            this.PSetConnectionString.TabIndex = 0;
            // 
            // textBoxX2
            // 
            this.textBoxX2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.textBoxX2.Border.Class = "TextBoxBorder";
            this.textBoxX2.Enabled = false;
            this.textBoxX2.Location = new System.Drawing.Point(35, 128);
            this.textBoxX2.MaxLength = 100;
            this.textBoxX2.Name = "textBoxX2";
            this.textBoxX2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxX2.Size = new System.Drawing.Size(112, 21);
            this.textBoxX2.TabIndex = 17;
            this.textBoxX2.Tag = "2";
            this.textBoxX2.Text = "Password";
            this.textBoxX2.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(156, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 16;
            this.label4.Tag = "2";
            this.label4.Text = "رمز عبور:";
            // 
            // textBoxX1
            // 
            this.textBoxX1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Enabled = false;
            this.textBoxX1.Location = new System.Drawing.Point(35, 96);
            this.textBoxX1.MaxLength = 100;
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxX1.Size = new System.Drawing.Size(112, 21);
            this.textBoxX1.TabIndex = 15;
            this.textBoxX1.Tag = "2";
            this.textBoxX1.Text = "Admin";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(153, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 14;
            this.label3.Tag = "2";
            this.label3.Text = "نام کاربری:";
            // 
            // lblServerName
            // 
            this.lblServerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServerName.AutoSize = true;
            this.lblServerName.BackColor = System.Drawing.Color.Transparent;
            this.lblServerName.Enabled = false;
            this.lblServerName.Location = new System.Drawing.Point(330, 98);
            this.lblServerName.Name = "lblServerName";
            this.lblServerName.Size = new System.Drawing.Size(52, 13);
            this.lblServerName.TabIndex = 7;
            this.lblServerName.Tag = "2";
            this.lblServerName.Text = "نام سرور:";
            // 
            // txtServerName
            // 
            this.txtServerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtServerName.Border.Class = "TextBoxBorder";
            this.txtServerName.Enabled = false;
            this.txtServerName.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerName.Location = new System.Drawing.Point(225, 93);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtServerName.Size = new System.Drawing.Size(102, 22);
            this.txtServerName.TabIndex = 8;
            this.txtServerName.Tag = "2";
            this.txtServerName.Text = "EmsServer";
            // 
            // lblLocalDatabase
            // 
            this.lblLocalDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocalDatabase.AutoSize = true;
            this.lblLocalDatabase.BackColor = System.Drawing.Color.Transparent;
            this.lblLocalDatabase.Enabled = false;
            this.lblLocalDatabase.Location = new System.Drawing.Point(153, 47);
            this.lblLocalDatabase.Name = "lblLocalDatabase";
            this.lblLocalDatabase.Size = new System.Drawing.Size(69, 13);
            this.lblLocalDatabase.TabIndex = 4;
            this.lblLocalDatabase.Tag = "1";
            this.lblLocalDatabase.Text = "بانك اطلاعات:";
            // 
            // lblLocalServerName
            // 
            this.lblLocalServerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocalServerName.AutoSize = true;
            this.lblLocalServerName.BackColor = System.Drawing.Color.Transparent;
            this.lblLocalServerName.Enabled = false;
            this.lblLocalServerName.Location = new System.Drawing.Point(331, 47);
            this.lblLocalServerName.Name = "lblLocalServerName";
            this.lblLocalServerName.Size = new System.Drawing.Size(51, 13);
            this.lblLocalServerName.TabIndex = 2;
            this.lblLocalServerName.Tag = "1";
            this.lblLocalServerName.Text = "نام نمونه:";
            // 
            // txtLocalServerName
            // 
            this.txtLocalServerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtLocalServerName.Border.Class = "TextBoxBorder";
            this.txtLocalServerName.Enabled = false;
            this.txtLocalServerName.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalServerName.Location = new System.Drawing.Point(225, 42);
            this.txtLocalServerName.Name = "txtLocalServerName";
            this.txtLocalServerName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtLocalServerName.Size = new System.Drawing.Size(102, 22);
            this.txtLocalServerName.TabIndex = 3;
            this.txtLocalServerName.Tag = "1";
            this.txtLocalServerName.Text = "SqlDeveloper";
            this.txtLocalServerName.Leave += new System.EventHandler(this.txtLocalServerName_Leave);
            // 
            // lblIPAddress
            // 
            this.lblIPAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIPAddress.AutoSize = true;
            this.lblIPAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblIPAddress.Enabled = false;
            this.lblIPAddress.Location = new System.Drawing.Point(333, 127);
            this.lblIPAddress.Name = "lblIPAddress";
            this.lblIPAddress.Size = new System.Drawing.Size(49, 13);
            this.lblIPAddress.TabIndex = 9;
            this.lblIPAddress.Tag = "2";
            this.lblIPAddress.Text = "آدرس IP:";
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtIPAddress.Border.Class = "TextBoxBorder";
            this.txtIPAddress.Enabled = false;
            this.txtIPAddress.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIPAddress.Location = new System.Drawing.Point(225, 122);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtIPAddress.Size = new System.Drawing.Size(102, 22);
            this.txtIPAddress.TabIndex = 10;
            this.txtIPAddress.Tag = "2";
            this.txtIPAddress.Text = "192.168.1.1";
            // 
            // cboLocalDatabases
            // 
            this.cboLocalDatabases.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboLocalDatabases.DisplayMember = "Text";
            this.cboLocalDatabases.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboLocalDatabases.Enabled = false;
            this.cboLocalDatabases.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLocalDatabases.FormattingEnabled = true;
            this.cboLocalDatabases.ItemHeight = 15;
            this.cboLocalDatabases.Location = new System.Drawing.Point(11, 43);
            this.cboLocalDatabases.Name = "cboLocalDatabases";
            this.cboLocalDatabases.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboLocalDatabases.Size = new System.Drawing.Size(139, 21);
            this.cboLocalDatabases.TabIndex = 5;
            this.cboLocalDatabases.Tag = "1";
            this.cboLocalDatabases.Text = "[Select Local Database]";
            // 
            // chkNetworkDatabase
            // 
            this.chkNetworkDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkNetworkDatabase.BackColor = System.Drawing.Color.Transparent;
            this.chkNetworkDatabase.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkNetworkDatabase.Location = new System.Drawing.Point(156, 67);
            this.chkNetworkDatabase.Name = "chkNetworkDatabase";
            this.chkNetworkDatabase.Size = new System.Drawing.Size(226, 20);
            this.chkNetworkDatabase.TabIndex = 6;
            this.chkNetworkDatabase.Text = "وارد كردن آدرس و نام بانك اطلاعاتی در شبكه";
            this.chkNetworkDatabase.CheckedChanged += new System.EventHandler(this.chkNetworkDatabase_CheckedChanged);
            // 
            // chkLocalDatabase
            // 
            this.chkLocalDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkLocalDatabase.BackColor = System.Drawing.Color.Transparent;
            this.chkLocalDatabase.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkLocalDatabase.Location = new System.Drawing.Point(189, 19);
            this.chkLocalDatabase.Name = "chkLocalDatabase";
            this.chkLocalDatabase.Size = new System.Drawing.Size(193, 20);
            this.chkLocalDatabase.TabIndex = 1;
            this.chkLocalDatabase.Text = "استفاده از بانك های اطلاعاتی محلی";
            this.chkLocalDatabase.CheckedChanged += new System.EventHandler(this.chkLocalDatabase_CheckedChanged);
            // 
            // lblSelectConnectionType
            // 
            this.lblSelectConnectionType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSelectConnectionType.AutoSize = true;
            this.lblSelectConnectionType.BackColor = System.Drawing.Color.Transparent;
            this.lblSelectConnectionType.Location = new System.Drawing.Point(257, 4);
            this.lblSelectConnectionType.Name = "lblSelectConnectionType";
            this.lblSelectConnectionType.Size = new System.Drawing.Size(125, 13);
            this.lblSelectConnectionType.TabIndex = 0;
            this.lblSelectConnectionType.Text = "نوع اتصال را انتخاب نمایید:";
            // 
            // lblText4
            // 
            this.lblText4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText4.AutoSize = true;
            this.lblText4.BackColor = System.Drawing.Color.Transparent;
            this.lblText4.Location = new System.Drawing.Point(153, 152);
            this.lblText4.Name = "lblText4";
            this.lblText4.Size = new System.Drawing.Size(226, 13);
            this.lblText4.TabIndex = 13;
            this.lblText4.Text = "پس از انتخاب ، بر روی گزینه \"بعدی\" كلیك نمایید.";
            // 
            // PCheckPassword
            // 
            this.PCheckPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PCheckPassword.BackColor = System.Drawing.Color.Transparent;
            this.PCheckPassword.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PCheckPassword.Controls.Add(this.txtPassword);
            this.PCheckPassword.Controls.Add(this.txtUsername);
            this.PCheckPassword.Controls.Add(this.lblText5);
            this.PCheckPassword.Controls.Add(this.chkSaveUserName);
            this.PCheckPassword.Controls.Add(this.label2);
            this.PCheckPassword.Controls.Add(this.label1);
            this.PCheckPassword.Controls.Add(this.lblEnterUNameAndPass);
            this.PCheckPassword.Controls.Add(this.PictureBoxLogo);
            this.PCheckPassword.Location = new System.Drawing.Point(7, 57);
            this.PCheckPassword.Name = "PCheckPassword";
            this.PCheckPassword.PageDescription = "ورود نام كاربری و رمز عبور";
            this.PCheckPassword.PageTitle = "ارتباط با بانك اطلاعاتی";
            this.PCheckPassword.Size = new System.Drawing.Size(390, 168);
            this.PCheckPassword.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtPassword.Border.Class = "TextBoxBorder";
            this.txtPassword.Location = new System.Drawing.Point(220, 81);
            this.txtPassword.MaxLength = 100;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPassword.Size = new System.Drawing.Size(161, 21);
            this.txtPassword.TabIndex = 12;
            this.txtPassword.Text = "Password";
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtUsername.Border.Class = "TextBoxBorder";
            this.txtUsername.Location = new System.Drawing.Point(220, 41);
            this.txtUsername.MaxLength = 100;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUsername.Size = new System.Drawing.Size(161, 21);
            this.txtUsername.TabIndex = 11;
            this.txtUsername.Text = "Admin";
            // 
            // lblText5
            // 
            this.lblText5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText5.AutoSize = true;
            this.lblText5.BackColor = System.Drawing.Color.Transparent;
            this.lblText5.Location = new System.Drawing.Point(153, 152);
            this.lblText5.Name = "lblText5";
            this.lblText5.Size = new System.Drawing.Size(226, 13);
            this.lblText5.TabIndex = 10;
            this.lblText5.Text = "پس از انتخاب ، بر روی گزینه \"اتصال\" كلیك نمایید.";
            // 
            // chkSaveUserName
            // 
            this.chkSaveUserName.AutoSize = true;
            this.chkSaveUserName.BackColor = System.Drawing.Color.Transparent;
            this.chkSaveUserName.Checked = true;
            this.chkSaveUserName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSaveUserName.CheckValue = "Y";
            this.chkSaveUserName.Location = new System.Drawing.Point(284, 108);
            this.chkSaveUserName.Name = "chkSaveUserName";
            this.chkSaveUserName.Size = new System.Drawing.Size(97, 16);
            this.chkSaveUserName.TabIndex = 2;
            this.chkSaveUserName.Text = "ذخیره نام كاربری";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(332, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "رمز عبور:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(324, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "نام کاربری:";
            // 
            // lblEnterUNameAndPass
            // 
            this.lblEnterUNameAndPass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnterUNameAndPass.AutoSize = true;
            this.lblEnterUNameAndPass.BackColor = System.Drawing.Color.Transparent;
            this.lblEnterUNameAndPass.Location = new System.Drawing.Point(220, 3);
            this.lblEnterUNameAndPass.Name = "lblEnterUNameAndPass";
            this.lblEnterUNameAndPass.Size = new System.Drawing.Size(165, 13);
            this.lblEnterUNameAndPass.TabIndex = 3;
            this.lblEnterUNameAndPass.Text = "نام كاربری و رمز عبور را وارد نمایید.";
            // 
            // PictureBoxLogo
            // 
            this.PictureBoxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PictureBoxLogo.Image = global::MohanirPouya.Properties.Resources.Button___Logoff;
            this.PictureBoxLogo.Location = new System.Drawing.Point(5, -10);
            this.PictureBoxLogo.Name = "PictureBoxLogo";
            this.PictureBoxLogo.Size = new System.Drawing.Size(162, 159);
            this.PictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxLogo.TabIndex = 12;
            this.PictureBoxLogo.TabStop = false;
            // 
            // FormTimer
            // 
            this.FormTimer.Interval = 500;
            this.FormTimer.Tick += new System.EventHandler(this.FormTimer_Tick);
            // 
            // DbConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(404, 272);
            this.Controls.Add(this.MyWizard);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "DbConnection";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ارتباط با بانك اطلاعاتی";
            this.Load += new System.EventHandler(this.DbConnection_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DbConnectionSet_FormClosing);
            this.MyWizard.ResumeLayout(false);
            this.PSelectConnection.ResumeLayout(false);
            this.PSelectConnection.PerformLayout();
            this.PSetConnectionString.ResumeLayout(false);
            this.PSetConnectionString.PerformLayout();
            this.PCheckPassword.ResumeLayout(false);
            this.PCheckPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.WizardPage PSelectConnection;
        private System.Windows.Forms.Label lblText1;
        private DevComponents.DotNetBar.WizardPage PSetConnectionString;
        private DevComponents.DotNetBar.WizardPage PCheckPassword;
        private System.Windows.Forms.Label lblText2;
        private System.Windows.Forms.Label lblText3;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkUseLastConnection;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkMakeNewConnection;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkSaveUserName;
        private System.Windows.Forms.Label lblEnterUNameAndPass;
        internal System.Windows.Forms.PictureBox PictureBoxLogo;
        private System.Windows.Forms.Label lblText4;
        private System.Windows.Forms.Label lblText5;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkLocalDatabase;
        private System.Windows.Forms.Label lblSelectConnectionType;
        private System.Windows.Forms.Label lblServerName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtServerName;
        private System.Windows.Forms.Label lblIPAddress;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIPAddress;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboLocalDatabases;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkNetworkDatabase;
        private DevComponents.DotNetBar.ButtonX btnConnectionDetails;
        private System.Windows.Forms.Label lblLocalServerName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtLocalServerName;
        private System.Windows.Forms.Label lblLocalDatabase;
        private System.Windows.Forms.Timer FormTimer;
        internal DevComponents.DotNetBar.Wizard MyWizard;
        private DevComponents.DotNetBar.Controls.TextBoxX txtUsername;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX2;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private System.Windows.Forms.Label label3;
    }
}