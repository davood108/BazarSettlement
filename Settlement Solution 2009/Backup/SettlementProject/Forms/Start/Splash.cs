﻿#region using
using System;
using System.Windows.Forms;
#endregion

namespace MohanirPouya.Forms.Start
{
    public partial class frmSplash : Form
    {

        #region Ctor

        public frmSplash()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void SplashTimer_Tick(object sender, EventArgs e)
        {
            Hide();
            GC.Collect();
            SplashTimer.Enabled = false;
            SplashTimer.Tick -= SplashTimer_Tick;
            Form FL = new DbConnection();
            FL.Show();
        }

        #endregion

    }
}