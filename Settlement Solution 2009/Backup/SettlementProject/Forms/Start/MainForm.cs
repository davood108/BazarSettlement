﻿#region using

using System;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Rendering;
using MohanirPouya.Forms.Help;
using MohanirPouya.Forms.Management.BillItemsSettings;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirPouya.Forms.Management.ManageParameters.Parameters;
using MohanirPouya.Forms.Management.ManageParameters.ParametersC;
using MohanirPouya.Forms.Management.UsersAndGroups;
using MohanirPouya.Forms.ManagementBill;
using MohanirPouya.Forms.Support;
using MohanirVBClasses;
using MohanirPouya.Forms.InputBaseData;

#endregion

namespace MohanirPouya.Forms.Start
{
    public partial class frmMainForm : Form
    {


        #region Constructors

        public frmMainForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers


        #region Bill

        #region btnDailyBill_Click
        private void btnDailyBill_Click(object sender, EventArgs e)
        {

          new SelectedTime(1);
        }

        #endregion

        #region btnMonthlyBill_Click
        private void btnMonthlyBill_Click(object sender, EventArgs e)
        {
        new SelectedTime(2);
        }
        #endregion

        #region btnFinalBill_Click
        private void btnFinalBill_Click(object sender, EventArgs e)
        {
        new SelectedTime(3);
        }
        #endregion

        #endregion

        #region Managment

        #region User
        private void btnUsers_Click(object sender, EventArgs e)
        {
            new UsersGroups();
        }
        #endregion

        #region Sellers

        #region btnSellers_Click

        private void btnSellers_Click(object sender, EventArgs e)
        {
            btnSellers.Expanded = true;
        }

        #endregion

        #region btnSellersParams_Click

        private void btnSellersParams_Click(object sender, EventArgs e)
        {
            new frmParameters("Sellers", "Pa");
        }

        #endregion

        #region btnSellersParamsC_Click

        private void btnSellersParamsC_Click(object sender, EventArgs e)
        {
            new frmParametersC("Sellers", "Pc");
        }

        #endregion

        #region btnSellersSarfasl_Click

        private void btnSellersSarfasl_Click(object sender, EventArgs e)
        {
            new frmParametersC("Sellers", "Sa");
        }

        #endregion

        #region btnSellersShakhes_Click

        private void btnSellersShakhes_Click(object sender, EventArgs e)
        {
            new frmParametersC("Sellers", "Sh");
        }

        #endregion

        #region Param_Structure_Click
        private void Param_Structure_S_Click(object sender, EventArgs e)
        {
            new ParamStructure("Sellers");
        }
        #endregion

        #region btnSellersBillItems_Click


        #region BtnSellerPowerBillItemSet
        private void BtnSellerPowerBillItemSet_Click(object sender, EventArgs e)
        {
            new BillItemSet("Sellers", "P");
        }
        #endregion

        #region btnSellersUnitBillItemSet
        private void btnSellersUnitBillItemSet_Click(object sender, EventArgs e)
        {
            new BillItemSet("Sellers", "U");
        }
        #endregion

        #region btnSellersTazmini
        private void btn_Tazmini_Click(object sender, EventArgs e)
        {
            new BillItemSet("Sellers", "Z");
        }

        #endregion
        #endregion

        #region btnSellersManageBill_Click
        private void btnSellersManageBill_Click(object sender, EventArgs e)
        {
            //  new SellersManageBills().ShowDialog();
        }

        #endregion

        #endregion

        #region Buyers

        #region btnBuyers_Click

        private void btnBuyers_Click(object sender, EventArgs e)
        {
            btnBuyers.Expanded = true;
        }

        #endregion

        #region btnBuyersParams_Click

        private void btnBuyersParams_Click(object sender, EventArgs e)
        {
            new frmParameters("Buyers", "Pa");
        }

        #endregion

        #region btnBuyersParamsC_Click

        private void btnBuyersParamsC_Click(object sender, EventArgs e)
        {
            new frmParametersC("Buyers", "Pc");
        }

        #endregion

        #region btnBuyersSarfasl_Click

        private void btnBuyersSarfasl_Click(object sender, EventArgs e)
        {
            new frmParametersC("Buyers", "Sa");
        }

        #endregion

        #region btnBuyersShakhes_Click

        private void btnBuyersShakhes_Click(object sender, EventArgs e)
        {
            new frmParametersC("Buyers", "Sh");
        }

        #endregion

        #region Param_Structure_B_Click
        private void Param_Structure_B_Click(object sender, EventArgs e)
        {
            new ParamStructure("Buyers");
        }
        #endregion

        #region btnBuyersBillItems_Click

        private void btnBuyersBillItem_Click(object sender, EventArgs e)
        {
            new BillItemSet("Buyers", "P");
        }

        #endregion

        #region btnBuyersManageBill_Click

        private void btnBuyersManageItem_Click(object sender, EventArgs e)
        {
            //   new frmBuyersManagebills().ShowDialog();
        }
        #endregion


        #endregion

        #region Transfer

     

        #region btnTransferParams_Click

         private void btnTransferParams_Click(object sender, EventArgs e)
        {
           new frmParameters("Transfer", "Pa");
        }

        #endregion

        #region btnTransfeParamsC_Click
        private void btnTransferParamsC_Click(object sender, EventArgs e)
        {
             new frmParametersC("Transfer", "Pc");
        }
         
        #endregion

        #region btnTransferBillItem_click
              private void btnTransferBillItem_Click(object sender, EventArgs e)
              {
                  new BillItemSet("Transfer", "P");
              }
    
        #endregion

        #region btnTransferBillItemD

              private void btnTransferBillItemD_Click(object sender, EventArgs e)
              {
                  new BillItemSet("Transfer", "D");

              }
              #endregion

        #region Param_Structure_T_Click
        private void Param_Structure_T_Click(object sender, EventArgs e)
              {
                  new ParamStructure("Transfer");

              }
        #endregion

        #region btnTransferInputData
        private void btnTransferInfoIn_Click(object sender, EventArgs e)
              {
                  new TransferInsertData("I");
              }
              
       

        #endregion

        #region btnTransferDeleteData
        
        private void btnTransferInfoDel_Click(object sender, EventArgs e)
        {

            new TransferInsertData("D");
        }
        #endregion


        #endregion
        
        #region Style

              #region buttonStyleCustom_SelectedColorChanged

              private void buttonStyleCustom_SelectedColorChanged(object sender, EventArgs e)
        {
            eOffice2007ColorScheme colorScheme = MainRibbonControl.Office2007ColorTable;
            RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, colorScheme,
                                                                    btnStyleCustom.SelectedColor);
            btnStyleCustom.Checked = true;
            btnStyleOffice2007Blue.Checked = false;
            btnStyleOffice2007Black.Checked = false;
            btnStyleOffice2007Silver.Checked = false;
        }

        #endregion

        #region btnStyleCustom_ColorPreview

        private void btnStyleCustom_ColorPreview(object sender, ColorPreviewEventArgs e)
        {
            eOffice2007ColorScheme colorScheme = MainRibbonControl.Office2007ColorTable;
            RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, colorScheme,
                                                                    e.Color);
            btnStyleCustom.Checked = true;
            btnStyleOffice2007Blue.Checked = false;
            btnStyleOffice2007Black.Checked = false;
            btnStyleOffice2007Silver.Checked = false;
        }

        #endregion

        #region buttonStyleOffice2007Blue_Click

        private void buttonStyleOffice2007Blue_Click(object sender, EventArgs e)
        {
            MainRibbonControl.Office2007ColorTable = eOffice2007ColorScheme.Blue;
            btnStyleOffice2007Blue.Checked = true;
            btnStyleCustom.Checked = false;
        }

        #endregion

        #region buttonStyleOffice2007Black_Click

        private void buttonStyleOffice2007Black_Click(object sender, EventArgs e)
        {
            MainRibbonControl.Office2007ColorTable = eOffice2007ColorScheme.Black;
            btnStyleOffice2007Black.Checked = true;
            btnStyleCustom.Checked = false;
        }

        #endregion

        #region buttonStyleOffice2007Silver_Click

        private void buttonStyleOffice2007Silver_Click(object sender, EventArgs e)
        {
            MainRibbonControl.Office2007ColorTable = eOffice2007ColorScheme.Silver;
            btnStyleOffice2007Silver.Checked = true;
            btnStyleCustom.Checked = false;
        }

        #endregion

        #endregion

        #endregion

        #region Support

        #region btnQueryEditor_Click

        private void btnQueryEditor_Click(object sender, EventArgs e)
        {
            new frmQueryDesigner().ShowDialog();
        }

        #endregion

        #region btnBackup_Click

        private void btnBackup_Click(object sender, EventArgs e)
        {
            new ParamStructure("Transfer", "M");
        }

        #endregion

        #region btnRestore_Click

        private void btnRestore_Click(object sender, EventArgs e)
        {
        }

        #endregion

        #endregion

        #region Help

        #region btnContactSupport_Click

        private void btnContactSupport_Click(object sender, EventArgs e)
        {
        }

        #endregion

        #region btnActivateSupport_Click

        private void btnActivateSupport_Click(object sender, EventArgs e)
        {
        }

        #endregion

        #region btnAbout_Click

        private void btnAbout_Click(object sender, EventArgs e)
        {
            new frmAbout().ShowDialog();
        }

        #endregion

        #endregion

        #region btnExit_Click

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region Form Closing

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("مایل به خروج از سیستم هستید؟",
                                       "تایید خروج", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                       MessageBoxDefaultButton.Button1) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            FormClosing -= frmMain_FormClosing;
            Application.Exit();
        }

        #endregion

    

        
  #endregion








    }
}