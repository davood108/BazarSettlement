﻿#region using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Windows.Forms;
using MohanirPouya.DbLayer;
using MohanirVBClasses;
using User=MohanirPouya.DbLayer.User;

#endregion

namespace MohanirPouya.Forms.Start
{
    /// <summary>
    /// كلاس فرم مدیریت ارتباط با بانك اطلاعات
    /// </summary>
    public partial class DbConnection : Form
    {

        #region Ctors
        public DbConnection()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handlers

        #region Form Load
        private void DbConnection_Load(object sender, EventArgs e)
        {
            if (CheckDataFileExist())
            {
                chkUseLastConnection.Checked = true;
                FormTimer.Enabled = true;
                return;
            }
            chkUseLastConnection.Enabled = false;
            chkMakeNewConnection.Checked = true;
        }
        #endregion

        #region Exit Button Clicked
        private void btnExit_Click(object sender, CancelEventArgs e)
        {
            Application.Exit();
        }
        #endregion

        #region Form Closing

        private void DbConnectionSet_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("مایل به خروج از سیستم هستید؟",
                    "تایید خروج"  , MessageBoxButtons.YesNo ,MessageBoxIcon.Question , 
                        MessageBoxDefaultButton.Button1) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            FormClosing -= DbConnectionSet_FormClosing;
            Application.Exit();
        }

        #endregion

        #region Accept Password Button Clicked
        private void btnAcceptPassword(object sender, CancelEventArgs e)
        {
            Classes.DbBizClass.dbConnStr = LoadConnectionString();
            if (!CheckUserAndPass())
            {
                e.Cancel = true;
                return;
            }
            Dispose();
            GC.Collect();
            new frmMainForm().ShowDialog();
        }

        private bool CheckUserAndPass()
        {
            var DBClass = 
                new DbSMDataContext(Classes.DbBizClass.dbConnStr);
            List<User> MyData = DBClass.Users.Where(
                Data => Data.UserName == txtUsername.Text.Trim() &&
                Data.Password == txtPassword.Text
                && Data.IsActive && Data.Type<3).ToList();
            if (MyData.Count == 0)
            {
             PersianMessageBox.Show("نام کاربری یا کلمه عبور صحیح نمی باشد!", "خطا!",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return false;
            }
         
            Classes.DbBizClass.CurrentUserID = MyData.First().ID;
            Classes.DbBizClass.CurrentUserName = MyData.First().LastName;
            Classes.DbBizClass.CurrentUserLastName = MyData.First().LastName;
            Classes.DbBizClass.CurrentUserType = MyData.First().Type;
            return true; 
        }
        #endregion

        #region Use Last Connection CheckBox CheckedChanged

        private void chkUseLastConnection_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUseLastConnection.Checked)
            {
                btnConnectionDetails.Enabled = true;
                return;
            }
            btnConnectionDetails.Enabled = false;
        }

        #endregion

        #region Enter Connection String From Text Button Clicked

        #endregion

        #region Connection Details Button Clicked

        private void btnConnectionDetails_Click(object sender, EventArgs e)
        {
            try
            {
                var NewSqlConnection = new SqlConnection(LoadConnectionString());
                MessageBox.Show(
                    "Data Source = " + NewSqlConnection.DataSource + "\n" +
                    "Database = " + NewSqlConnection.Database,
                    "متن رشته ارتباط با بانك اطلاعاتی", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception Ex)
            {
                PersianMessageBox.Show("امكان خواندن فایل داده ها وجود ندارد ، متن خطا: \n" +
                    Ex.Message, "خطا!", MessageBoxButtons.OK, MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1);
            }
        }

        #endregion

        #region Next Button Clicked

        private void btnNext_Click(object sender, CancelEventArgs e)
        {
            // وضعیت صفحه اول
            if (MyWizard.SelectedPage == PSelectConnection)
            {
                if (chkMakeNewConnection.Checked)
                {
                    return;
                }
                if (chkUseLastConnection.Checked)
                {
                    Classes.DbBizClass.dbConnStr = 
                        LoadConnectionString();
                    MyWizard.SelectedPage = PCheckPassword;
                    txtUsername.Focus();
                    return;
                }
            }
            // وضعیت صفحه دوم
            if (MyWizard.SelectedPage == PSetConnectionString)
            {
                if (chkLocalDatabase.Checked)
                {
                    //UId=USERNAME ; PWD=Password
                    // ToDo: محل تغییر اتصال
                    SaveConnectionString(@"Server = .\" + txtLocalServerName.Text +
                        "; Database = " + cboLocalDatabases.Text + ";Integrated Security = True ;");
                    txtUsername.Focus();
                    return;
                }
                SaveConnectionString("Server = " + txtServerName.Text + @"\" + txtIPAddress.Text +
                    ","  + "; Database = Settlement"+
                    "; UId=Bazar ; PWD=238590;");
                txtUsername.Focus();
                return;
            }
            // وضعیت صفحه سوم
            if (MyWizard.SelectedPage == PCheckPassword)
            {
                return;
            }
            return;
        }

        #endregion

        #region Back Button Clicked

        private void btnBack_Click(object sender, CancelEventArgs e)
        {
            // وضعیت صفحه دوم
            if (MyWizard.SelectedPage == PSetConnectionString)
            {
                return;
            }
            // وضعیت صفحه سوم
            if (MyWizard.SelectedPage == PCheckPassword)
            {
                if (chkMakeNewConnection.Checked)
                {
                    return;
                }
                if (chkMakeNewConnection.Checked == false)
                {
                    MyWizard.SelectedPage = PSelectConnection;
                    return;
                }
            }
            return;
        }

        #endregion

        #region Connection String Page Check Box Checked Change

        #region CheckBox LocalDatabase Checked Changed

        private void chkLocalDatabase_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLocalDatabase.Checked)
            {
                foreach (Control C in PSetConnectionString.Controls)
                {
                    if (Convert.ToInt32(C.Tag) == 1)
                        C.Enabled = true;
                    if (Convert.ToInt32(C.Tag) == 2)
                        C.Enabled = false;
                }
                txtLocalServerName.Focus();
            }
        }

        #endregion

        #region CheckBox NetworkDatabase Checked Changed

        private void chkNetworkDatabase_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNetworkDatabase.Checked)
            {
                foreach (Control C in PSetConnectionString.Controls)
                {
                    if (Convert.ToInt32(C.Tag) == 1)
                        C.Enabled = false;
                    if (Convert.ToInt32(C.Tag) == 2)
                        C.Enabled = true;
                }
            }
        }

        #endregion

        #endregion

        #region txtLocalServerName_Leave
        private void txtLocalServerName_Leave(object sender, EventArgs e)
        { 
            ///Initial Catalog=Master
            cboLocalDatabases.Items.Clear();
            String ConnectionString = @"Data Source = .\ " + txtLocalServerName.Text +
                ";  Integrated Security = True;";
            const String CommandString = "SELECT [name] FROM sys.databases";
            try
            {
                var TheSqlCommand =
                    new SqlCommand(CommandString, new SqlConnection(ConnectionString));

                TheSqlCommand.Connection.Open();
                SqlDataReader MyDataReader = TheSqlCommand.ExecuteReader();
                if (MyDataReader != null)
                    while (MyDataReader.Read())
                        cboLocalDatabases.Items.Add(MyDataReader[0].ToString());
                if (MyDataReader != null) MyDataReader.Close();
                TheSqlCommand.Connection.Close();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا در ارتباط با سرور!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        #endregion

        #region Form TimeTicker Tick
        private void FormTimer_Tick(object sender, EventArgs e)
        {
            FormTimer.Enabled = false;
            FormTimer.Tick -= FormTimer_Tick;
            MyWizard.SelectedPage = PCheckPassword;
        }
        #endregion

        #endregion

        #region Methods

        #region Save Connection String Method

        /// <summary>
        /// ذخیره رشته ارتباط با بانك اطلاعاتی
        /// </summary>
        /// <param name="ConnectionString">مقدار رشته ی ارتباط با بانك اطلاعاتی</param>
        public void SaveConnectionString(String ConnectionString)
        {
            var MyDataTable = new DataTable();
            MyDataTable.Columns.Add("Field");
            MyDataTable.Columns.Add("Value");
            MyDataTable.Rows.Add("Connection String", ConnectionString);
            var MyDataSet = new DataSet("MyDataSet");
            MyDataSet.Tables.Add(MyDataTable);
            try
            {
                MyDataSet.WriteXml("DbConnection.DAT");
            }
            catch (Exception Ex)
            {
                PersianMessageBox.Show(Ex.Message, "خطا", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1);
            }
        }

        #endregion

        #region Load Connection String Method

        /// <summary>
        /// خواندن رشته ارتباط با بانك اطلاعاتی
        /// </summary>
        public String LoadConnectionString()
        {
            var MyDataSet = new DataSet("MyDataSet");
            try
            {
                MyDataSet.ReadXml("DbConnection.DAT");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            String ReturnValue = String.Empty;
            // For each row, print the values of each column.
            foreach (DataRow MyDataRow in MyDataSet.Tables[0].Rows)
            {
                foreach (DataColumn MyDataColumn in MyDataSet.Tables[0].Columns)
                {
                    if (MyDataColumn.Caption == "Value")
                        ReturnValue = (MyDataRow[MyDataColumn].ToString());
                }
            }
            return ReturnValue;
        }

        #endregion

        #region Check Data File Exist

        /// <summary>
        /// تابعی برای بررسی آنكه فایل داده ها موجود است یا خیر
        /// </summary>
        /// <returns>
        /// در صورت وجود مقدار درست و در غیر این صورت مقدار نادرست بر میگرداند
        /// </returns>
        private static Boolean CheckDataFileExist()
        {
            if (File.Exists("DbConnection.DAT"))
                return true;
            return false;
        }

        #endregion


        #endregion

    }
}