﻿namespace MohanirPouya.Forms.Start
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainForm));
            this.MainRibbonControl = new DevComponents.DotNetBar.RibbonControl();
            this.RPanelSupport = new DevComponents.DotNetBar.RibbonPanel();
            this.RBarBackup = new DevComponents.DotNetBar.RibbonBar();
            this.btnBackup = new DevComponents.DotNetBar.ButtonItem();
            this.btnRestore = new DevComponents.DotNetBar.ButtonItem();
            this.RBarManageDb = new DevComponents.DotNetBar.RibbonBar();
            this.btnQueryEditor = new DevComponents.DotNetBar.ButtonItem();
            this.btnCheckTables = new DevComponents.DotNetBar.ButtonItem();
            this.RPanelManagement = new DevComponents.DotNetBar.RibbonPanel();
            this.RBarCalculationManagement = new DevComponents.DotNetBar.RibbonBar();
            this.btnUpdateCalculations = new DevComponents.DotNetBar.ButtonItem();
            this.btnSetSarfasl = new DevComponents.DotNetBar.ButtonItem();
            this.RBarParametersManagement = new DevComponents.DotNetBar.RibbonBar();
            this.btnSellers = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersSarfasl = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersShakhes = new DevComponents.DotNetBar.ButtonItem();
            this.Param_Structure_S = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersBillItems = new DevComponents.DotNetBar.ButtonItem();
            this.BtnSellerPowerBillItemSet = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersUnitBillItemSet = new DevComponents.DotNetBar.ButtonItem();
            this.btn_Tazmini = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersManageBill = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyers = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersSarfasl = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersShakhes = new DevComponents.DotNetBar.ButtonItem();
            this.Param_Structure_B = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersBillItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersManageItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransfer = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferInfoIn = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferInfoDel = new DevComponents.DotNetBar.ButtonItem();
            this.Param_Structure_T = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferBillItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferBillItemD = new DevComponents.DotNetBar.ButtonItem();
            this.RBarSecurity = new DevComponents.DotNetBar.RibbonBar();
            this.btnGroups = new DevComponents.DotNetBar.ButtonItem();
            this.btnUsers = new DevComponents.DotNetBar.ButtonItem();
            this.RPanelReports = new DevComponents.DotNetBar.RibbonPanel();
            this.RBarReports = new DevComponents.DotNetBar.RibbonBar();
            this.btnSellersReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferReport = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.btnDailyBill = new DevComponents.DotNetBar.ButtonItem();
            this.btnMonthlyBill = new DevComponents.DotNetBar.ButtonItem();
            this.btnFinalBill = new DevComponents.DotNetBar.ButtonItem();
            this.RPanelHelp = new DevComponents.DotNetBar.RibbonPanel();
            this.RBarHelp = new DevComponents.DotNetBar.RibbonBar();
            this.btnContactSupport = new DevComponents.DotNetBar.ButtonItem();
            this.btnActivateSupport = new DevComponents.DotNetBar.ButtonItem();
            this.btnAbout = new DevComponents.DotNetBar.ButtonItem();
            this.RTabBill = new DevComponents.DotNetBar.RibbonTabItem();
            this.RTabReports = new DevComponents.DotNetBar.RibbonTabItem();
            this.RTabManagement = new DevComponents.DotNetBar.RibbonTabItem();
            this.RTabSupport = new DevComponents.DotNetBar.RibbonTabItem();
            this.RTabHelp = new DevComponents.DotNetBar.RibbonTabItem();
            this.btnChangeStyle = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleOffice2007Blue = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleOffice2007Black = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleOffice2007Silver = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleCustom = new DevComponents.DotNetBar.ColorPickerDropDown();
            this.StartButton = new DevComponents.DotNetBar.Office2007StartButton();
            this.ItemContainerStart = new DevComponents.DotNetBar.ItemContainer();
            this.btnStartSellers = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartSellersParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartSellersParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartSellersSarfasl = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartSellersShakhes = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartBuyers = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartBuyersParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartBuyersParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartBuyersSarfasl = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartBuyersShakhes = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransfer = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransferLine = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransferTransfer = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransferParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTranferParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransferDistribution = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartDistributionParam = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartDistributionParamC = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransferTrans = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransferTransParam = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartTransferTransParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartOtherService = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartFreq = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartFreqParam = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartFreqParamC = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartReActive = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartReActiveParam = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartReActiveParamC = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.ItemContainerStartSmall = new DevComponents.DotNetBar.ItemContainer();
            this.btnStartAbout = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartExit = new DevComponents.DotNetBar.ButtonItem();
            this.MainQatCustomizeItem = new DevComponents.DotNetBar.QatCustomizeItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.MainStatusBar = new DevComponents.DotNetBar.Bar();
            this.lblCurentUser = new DevComponents.DotNetBar.LabelItem();
            this.lblUserGroup = new DevComponents.DotNetBar.LabelItem();
            this.labelPosition = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer13 = new DevComponents.DotNetBar.ItemContainer();
            this.elementStyle1 = new DevComponents.DotNetBar.ElementStyle();
            this.MainRibbonControl.SuspendLayout();
            this.RPanelSupport.SuspendLayout();
            this.RPanelManagement.SuspendLayout();
            this.RPanelReports.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.RPanelHelp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainStatusBar)).BeginInit();
            this.SuspendLayout();
            // 
            // MainRibbonControl
            // 
            this.MainRibbonControl.CaptionFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainRibbonControl.CaptionVisible = true;
            this.MainRibbonControl.Controls.Add(this.RPanelManagement);
            this.MainRibbonControl.Controls.Add(this.RPanelSupport);
            this.MainRibbonControl.Controls.Add(this.RPanelReports);
            this.MainRibbonControl.Controls.Add(this.ribbonPanel1);
            this.MainRibbonControl.Controls.Add(this.RPanelHelp);
            this.MainRibbonControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainRibbonControl.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RTabBill,
            this.RTabReports,
            this.RTabManagement,
            this.RTabSupport,
            this.RTabHelp,
            this.btnChangeStyle});
            this.MainRibbonControl.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.MainRibbonControl.Location = new System.Drawing.Point(0, 0);
            this.MainRibbonControl.Name = "MainRibbonControl";
            this.MainRibbonControl.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.MainRibbonControl.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.StartButton,
            this.MainQatCustomizeItem});
            this.MainRibbonControl.Size = new System.Drawing.Size(785, 150);
            this.MainRibbonControl.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainRibbonControl.SystemText.MaximizeRibbonText = "بزرگ نمایی";
            this.MainRibbonControl.SystemText.MinimizeRibbonText = "كوچك نمایی";
            this.MainRibbonControl.SystemText.QatAddItemText = "افزودن به نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatCustomizeMenuLabel = "شخصی سازی نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatCustomizeText = "شخصی سازی نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatDialogAddButton = "افزودن";
            this.MainRibbonControl.SystemText.QatDialogCancelButton = "انصراف";
            this.MainRibbonControl.SystemText.QatDialogCaption = "شخصیسازی نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatDialogCategoriesLabel = "انتخاب فرمان از";
            this.MainRibbonControl.SystemText.QatDialogOkButton = "تایید";
            this.MainRibbonControl.SystemText.QatDialogPlacementCheckbox = "قرار دادن نوار دسترسی فوری زیر منو";
            this.MainRibbonControl.SystemText.QatDialogRemoveButton = "حذف";
            this.MainRibbonControl.SystemText.QatPlaceAboveRibbonText = "قرار دادن نوار دسترسی فوری بالای منو";
            this.MainRibbonControl.SystemText.QatPlaceBelowRibbonText = "قرار دادن نوار دسترسی فوری زیر منو";
            this.MainRibbonControl.SystemText.QatRemoveItemText = "حذف از نوار دسترسی فوری";
            this.MainRibbonControl.TabGroupHeight = 14;
            this.MainRibbonControl.TabGroupsVisible = true;
            this.MainRibbonControl.TabIndex = 0;
            this.MainRibbonControl.UseExternalCustomization = true;
            // 
            // RPanelSupport
            // 
            this.RPanelSupport.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RPanelSupport.Controls.Add(this.RBarBackup);
            this.RPanelSupport.Controls.Add(this.RBarManageDb);
            this.RPanelSupport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RPanelSupport.Location = new System.Drawing.Point(0, 56);
            this.RPanelSupport.Name = "RPanelSupport";
            this.RPanelSupport.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.RPanelSupport.Size = new System.Drawing.Size(785, 92);
            this.RPanelSupport.TabIndex = 3;
            this.RPanelSupport.Visible = false;
            // 
            // RBarBackup
            // 
            this.RBarBackup.AutoOverflowEnabled = true;
            this.RBarBackup.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarBackup.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnBackup,
            this.btnRestore});
            this.RBarBackup.Location = new System.Drawing.Point(249, 0);
            this.RBarBackup.Name = "RBarBackup";
            this.RBarBackup.Size = new System.Drawing.Size(255, 89);
            this.RBarBackup.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarBackup.TabIndex = 1;
            this.RBarBackup.Text = "پشتیبانی";
            // 
            // btnBackup
            // 
            this.btnBackup.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnBackup.Image = global::MohanirPouya.Properties.Resources.Aero_Soft;
            this.btnBackup.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBackup.ImagePaddingHorizontal = 8;
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Text = "ایجاد فایل پشتیبان";
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // btnRestore
            // 
            this.btnRestore.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRestore.Image = global::MohanirPouya.Properties.Resources.Aqua_Soft;
            this.btnRestore.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnRestore.ImagePaddingHorizontal = 8;
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Text = "بازیابی پشتیبانی";
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // RBarManageDb
            // 
            this.RBarManageDb.AutoOverflowEnabled = true;
            this.RBarManageDb.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarManageDb.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnQueryEditor,
            this.btnCheckTables});
            this.RBarManageDb.Location = new System.Drawing.Point(3, 0);
            this.RBarManageDb.Name = "RBarManageDb";
            this.RBarManageDb.Size = new System.Drawing.Size(246, 89);
            this.RBarManageDb.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarManageDb.TabIndex = 0;
            this.RBarManageDb.Text = "مدیریت بانك اطلاعات";
            // 
            // btnQueryEditor
            // 
            this.btnQueryEditor.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnQueryEditor.Image = global::MohanirPouya.Properties.Resources.Blank;
            this.btnQueryEditor.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnQueryEditor.ImagePaddingHorizontal = 8;
            this.btnQueryEditor.Name = "btnQueryEditor";
            this.btnQueryEditor.Text = "ویرایشگر پرس و جو";
            this.btnQueryEditor.Click += new System.EventHandler(this.btnQueryEditor_Click);
            // 
            // btnCheckTables
            // 
            this.btnCheckTables.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCheckTables.Image = global::MohanirPouya.Properties.Resources.Default_Application;
            this.btnCheckTables.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnCheckTables.ImagePaddingHorizontal = 8;
            this.btnCheckTables.Name = "btnCheckTables";
            this.btnCheckTables.Text = "بررسی جداول";
            // 
            // RPanelManagement
            // 
            this.RPanelManagement.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RPanelManagement.Controls.Add(this.RBarCalculationManagement);
            this.RPanelManagement.Controls.Add(this.RBarParametersManagement);
            this.RPanelManagement.Controls.Add(this.RBarSecurity);
            this.RPanelManagement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RPanelManagement.Location = new System.Drawing.Point(0, 56);
            this.RPanelManagement.Name = "RPanelManagement";
            this.RPanelManagement.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.RPanelManagement.Size = new System.Drawing.Size(785, 92);
            this.RPanelManagement.TabIndex = 2;
            // 
            // RBarCalculationManagement
            // 
            this.RBarCalculationManagement.AutoOverflowEnabled = true;
            this.RBarCalculationManagement.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarCalculationManagement.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnUpdateCalculations,
            this.btnSetSarfasl});
            this.RBarCalculationManagement.Location = new System.Drawing.Point(443, 0);
            this.RBarCalculationManagement.Name = "RBarCalculationManagement";
            this.RBarCalculationManagement.Size = new System.Drawing.Size(138, 89);
            this.RBarCalculationManagement.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarCalculationManagement.TabIndex = 2;
            this.RBarCalculationManagement.Text = "مدیریت محاسبات";
            // 
            // btnUpdateCalculations
            // 
            this.btnUpdateCalculations.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUpdateCalculations.Image = global::MohanirPouya.Properties.Resources.Time___Date;
            this.btnUpdateCalculations.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnUpdateCalculations.ImagePaddingHorizontal = 8;
            this.btnUpdateCalculations.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUpdateCalculations.Name = "btnUpdateCalculations";
            this.btnUpdateCalculations.Text = "به روز رسانی محاسبات";
            // 
            // btnSetSarfasl
            // 
            this.btnSetSarfasl.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSetSarfasl.Image = global::MohanirPouya.Properties.Resources.System;
            this.btnSetSarfasl.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSetSarfasl.ImagePaddingHorizontal = 8;
            this.btnSetSarfasl.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSetSarfasl.Name = "btnSetSarfasl";
            this.btnSetSarfasl.Text = "تخصیص سرفصل ها";
            // 
            // RBarParametersManagement
            // 
            this.RBarParametersManagement.AutoOverflowEnabled = true;
            this.RBarParametersManagement.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarParametersManagement.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSellers,
            this.btnBuyers,
            this.btnTransfer});
            this.RBarParametersManagement.Location = new System.Drawing.Point(105, 0);
            this.RBarParametersManagement.Name = "RBarParametersManagement";
            this.RBarParametersManagement.Size = new System.Drawing.Size(338, 89);
            this.RBarParametersManagement.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarParametersManagement.TabIndex = 1;
            this.RBarParametersManagement.Text = "مدیریت پارامتر ها";
            // 
            // btnSellers
            // 
            this.btnSellers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSellers.Image = global::MohanirPouya.Properties.Resources.payment;
            this.btnSellers.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellers.ImagePaddingHorizontal = 8;
            this.btnSellers.Name = "btnSellers";
            this.btnSellers.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSellersParams,
            this.btnSellersParamsC,
            this.btnSellersSarfasl,
            this.btnSellersShakhes,
            this.Param_Structure_S,
            this.btnSellersBillItems,
            this.btnSellersManageBill});
            this.btnSellers.Text = "فروشندگان";
            this.btnSellers.Click += new System.EventHandler(this.btnSellers_Click);
            // 
            // btnSellersParams
            // 
            this.btnSellersParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersParams.ImagePaddingHorizontal = 8;
            this.btnSellersParams.Name = "btnSellersParams";
            this.btnSellersParams.Text = "پارامتر ها";
            this.btnSellersParams.Click += new System.EventHandler(this.btnSellersParams_Click);
            // 
            // btnSellersParamsC
            // 
            this.btnSellersParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersParamsC.ImagePaddingHorizontal = 8;
            this.btnSellersParamsC.Name = "btnSellersParamsC";
            this.btnSellersParamsC.Text = "پارامتر های شرطی";
            this.btnSellersParamsC.Click += new System.EventHandler(this.btnSellersParamsC_Click);
            // 
            // btnSellersSarfasl
            // 
            this.btnSellersSarfasl.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersSarfasl.ImagePaddingHorizontal = 8;
            this.btnSellersSarfasl.Name = "btnSellersSarfasl";
            this.btnSellersSarfasl.Text = "سرفصل ها";
            this.btnSellersSarfasl.Click += new System.EventHandler(this.btnSellersSarfasl_Click);
            // 
            // btnSellersShakhes
            // 
            this.btnSellersShakhes.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersShakhes.ImagePaddingHorizontal = 8;
            this.btnSellersShakhes.Name = "btnSellersShakhes";
            this.btnSellersShakhes.Text = "شاخص ها";
            this.btnSellersShakhes.Click += new System.EventHandler(this.btnSellersShakhes_Click);
            // 
            // Param_Structure_S
            // 
            this.Param_Structure_S.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.Param_Structure_S.ImagePaddingHorizontal = 8;
            this.Param_Structure_S.Name = "Param_Structure_S";
            this.Param_Structure_S.Text = "ساختار پارامترها";
            this.Param_Structure_S.Click += new System.EventHandler(this.Param_Structure_S_Click);
            // 
            // btnSellersBillItems
            // 
            this.btnSellersBillItems.BeginGroup = true;
            this.btnSellersBillItems.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnSellersBillItems.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersBillItems.ImagePaddingHorizontal = 8;
            this.btnSellersBillItems.Name = "btnSellersBillItems";
            this.btnSellersBillItems.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BtnSellerPowerBillItemSet,
            this.btnSellersUnitBillItemSet,
            this.btn_Tazmini});
            this.btnSellersBillItems.Text = "آیتم های صورتحساب";
            // 
            // BtnSellerPowerBillItemSet
            // 
            this.BtnSellerPowerBillItemSet.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.BtnSellerPowerBillItemSet.ImagePaddingHorizontal = 8;
            this.BtnSellerPowerBillItemSet.Name = "BtnSellerPowerBillItemSet";
            this.BtnSellerPowerBillItemSet.Text = "به صورت نیروگاهی";
            this.BtnSellerPowerBillItemSet.Click += new System.EventHandler(this.BtnSellerPowerBillItemSet_Click);
            // 
            // btnSellersUnitBillItemSet
            // 
            this.btnSellersUnitBillItemSet.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersUnitBillItemSet.ImagePaddingHorizontal = 8;
            this.btnSellersUnitBillItemSet.Name = "btnSellersUnitBillItemSet";
            this.btnSellersUnitBillItemSet.Text = "به تفکیک واحد";
            this.btnSellersUnitBillItemSet.Click += new System.EventHandler(this.btnSellersUnitBillItemSet_Click);
            // 
            // btn_Tazmini
            // 
            this.btn_Tazmini.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btn_Tazmini.ImagePaddingHorizontal = 8;
            this.btn_Tazmini.Name = "btn_Tazmini";
            this.btn_Tazmini.Text = "نیروگاههای تضمینی";
            this.btn_Tazmini.Click += new System.EventHandler(this.btn_Tazmini_Click);
            // 
            // btnSellersManageBill
            // 
            this.btnSellersManageBill.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnSellersManageBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersManageBill.ImagePaddingHorizontal = 8;
            this.btnSellersManageBill.Name = "btnSellersManageBill";
            this.btnSellersManageBill.Text = "مدیریت صورتحساب";
            this.btnSellersManageBill.Click += new System.EventHandler(this.btnSellersManageBill_Click);
            // 
            // btnBuyers
            // 
            this.btnBuyers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnBuyers.Image = global::MohanirPouya.Properties.Resources.green_dollar;
            this.btnBuyers.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyers.ImagePaddingHorizontal = 8;
            this.btnBuyers.Name = "btnBuyers";
            this.btnBuyers.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnBuyersParams,
            this.btnBuyersParamsC,
            this.btnBuyersSarfasl,
            this.btnBuyersShakhes,
            this.Param_Structure_B,
            this.btnBuyersBillItem,
            this.btnBuyersManageItem});
            this.btnBuyers.Text = "خریداران";
            this.btnBuyers.Click += new System.EventHandler(this.btnBuyers_Click);
            // 
            // btnBuyersParams
            // 
            this.btnBuyersParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersParams.ImagePaddingHorizontal = 8;
            this.btnBuyersParams.Name = "btnBuyersParams";
            this.btnBuyersParams.Text = "پارامتر ها";
            this.btnBuyersParams.Click += new System.EventHandler(this.btnBuyersParams_Click);
            // 
            // btnBuyersParamsC
            // 
            this.btnBuyersParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersParamsC.ImagePaddingHorizontal = 8;
            this.btnBuyersParamsC.Name = "btnBuyersParamsC";
            this.btnBuyersParamsC.Text = "پارامتر های شرطی";
            this.btnBuyersParamsC.Click += new System.EventHandler(this.btnBuyersParamsC_Click);
            // 
            // btnBuyersSarfasl
            // 
            this.btnBuyersSarfasl.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersSarfasl.ImagePaddingHorizontal = 8;
            this.btnBuyersSarfasl.Name = "btnBuyersSarfasl";
            this.btnBuyersSarfasl.Text = "سرفصل ها";
            this.btnBuyersSarfasl.Click += new System.EventHandler(this.btnBuyersSarfasl_Click);
            // 
            // btnBuyersShakhes
            // 
            this.btnBuyersShakhes.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersShakhes.ImagePaddingHorizontal = 8;
            this.btnBuyersShakhes.Name = "btnBuyersShakhes";
            this.btnBuyersShakhes.Text = "شاخص ها";
            this.btnBuyersShakhes.Click += new System.EventHandler(this.btnBuyersShakhes_Click);
            // 
            // Param_Structure_B
            // 
            this.Param_Structure_B.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.Param_Structure_B.ImagePaddingHorizontal = 8;
            this.Param_Structure_B.Name = "Param_Structure_B";
            this.Param_Structure_B.Text = "ساختار پارامترها";
            this.Param_Structure_B.Click += new System.EventHandler(this.Param_Structure_B_Click);
            // 
            // btnBuyersBillItem
            // 
            this.btnBuyersBillItem.BeginGroup = true;
            this.btnBuyersBillItem.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnBuyersBillItem.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersBillItem.ImagePaddingHorizontal = 8;
            this.btnBuyersBillItem.Name = "btnBuyersBillItem";
            this.btnBuyersBillItem.Text = "آیتم های صورتحساب";
            this.btnBuyersBillItem.Click += new System.EventHandler(this.btnBuyersBillItem_Click);
            // 
            // btnBuyersManageItem
            // 
            this.btnBuyersManageItem.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnBuyersManageItem.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersManageItem.ImagePaddingHorizontal = 8;
            this.btnBuyersManageItem.Name = "btnBuyersManageItem";
            this.btnBuyersManageItem.Text = "مدیریت صورتحساب";
            this.btnBuyersManageItem.Click += new System.EventHandler(this.btnBuyersManageItem_Click);
            // 
            // btnTransfer
            // 
            this.btnTransfer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnTransfer.Image = global::MohanirPouya.Properties.Resources.web;
            this.btnTransfer.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransfer.ImagePaddingHorizontal = 8;
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnTransferParams,
            this.btnTransferParamsC,
            this.btnTransferInfoIn,
            this.btnTransferInfoDel,
            this.Param_Structure_T,
            this.btnTransferBillItem,
            this.btnTransferBillItemD});
            this.btnTransfer.Text = "خدمات انتقال";
            // 
            // btnTransferParams
            // 
            this.btnTransferParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferParams.ImagePaddingHorizontal = 8;
            this.btnTransferParams.Name = "btnTransferParams";
            this.btnTransferParams.Text = "پارامتر ها";
            this.btnTransferParams.Click += new System.EventHandler(this.btnTransferParams_Click);
            // 
            // btnTransferParamsC
            // 
            this.btnTransferParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferParamsC.ImagePaddingHorizontal = 8;
            this.btnTransferParamsC.Name = "btnTransferParamsC";
            this.btnTransferParamsC.Text = "پارامتر های شرطی";
            this.btnTransferParamsC.Click += new System.EventHandler(this.btnTransferParamsC_Click);
            // 
            // btnTransferInfoIn
            // 
            this.btnTransferInfoIn.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferInfoIn.ImagePaddingHorizontal = 8;
            this.btnTransferInfoIn.Name = "btnTransferInfoIn";
            this.btnTransferInfoIn.Text = "ورود اطلاعات";
            this.btnTransferInfoIn.Click += new System.EventHandler(this.btnTransferInfoIn_Click);
            // 
            // btnTransferInfoDel
            // 
            this.btnTransferInfoDel.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferInfoDel.ImagePaddingHorizontal = 8;
            this.btnTransferInfoDel.Name = "btnTransferInfoDel";
            this.btnTransferInfoDel.Text = "پاک کردن اطلاعات";
            this.btnTransferInfoDel.Click += new System.EventHandler(this.btnTransferInfoDel_Click);
            // 
            // Param_Structure_T
            // 
            this.Param_Structure_T.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.Param_Structure_T.ImagePaddingHorizontal = 8;
            this.Param_Structure_T.Name = "Param_Structure_T";
            this.Param_Structure_T.Text = "ساختار پارامترها";
            this.Param_Structure_T.Click += new System.EventHandler(this.Param_Structure_T_Click);
            // 
            // btnTransferBillItem
            // 
            this.btnTransferBillItem.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnTransferBillItem.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferBillItem.ImagePaddingHorizontal = 8;
            this.btnTransferBillItem.Name = "btnTransferBillItem";
            this.btnTransferBillItem.Text = "آیتم های صورتحساب (کلی)";
            this.btnTransferBillItem.Click += new System.EventHandler(this.btnTransferBillItem_Click);
            // 
            // btnTransferBillItemD
            // 
            this.btnTransferBillItemD.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btnTransferBillItemD.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferBillItemD.ImagePaddingHorizontal = 8;
            this.btnTransferBillItemD.Name = "btnTransferBillItemD";
            this.btnTransferBillItemD.Text = "آیتم های صورتحساب (جزئی)";
            this.btnTransferBillItemD.Click += new System.EventHandler(this.btnTransferBillItemD_Click);
            // 
            // RBarSecurity
            // 
            this.RBarSecurity.AutoOverflowEnabled = true;
            this.RBarSecurity.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarSecurity.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnGroups,
            this.btnUsers});
            this.RBarSecurity.Location = new System.Drawing.Point(3, 0);
            this.RBarSecurity.Name = "RBarSecurity";
            this.RBarSecurity.Size = new System.Drawing.Size(102, 89);
            this.RBarSecurity.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarSecurity.TabIndex = 0;
            this.RBarSecurity.Text = "امنیت";
            // 
            // btnGroups
            // 
            this.btnGroups.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnGroups.Image = global::MohanirPouya.Properties.Resources.Public;
            this.btnGroups.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnGroups.ImagePaddingHorizontal = 8;
            this.btnGroups.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Text = "گروه های كاربری";
            // 
            // btnUsers
            // 
            this.btnUsers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUsers.Image = global::MohanirPouya.Properties.Resources.Administrative;
            this.btnUsers.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnUsers.ImagePaddingHorizontal = 8;
            this.btnUsers.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Text = "كاربران";
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // RPanelReports
            // 
            this.RPanelReports.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RPanelReports.Controls.Add(this.RBarReports);
            this.RPanelReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RPanelReports.Location = new System.Drawing.Point(0, 56);
            this.RPanelReports.Name = "RPanelReports";
            this.RPanelReports.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.RPanelReports.Size = new System.Drawing.Size(785, 92);
            this.RPanelReports.TabIndex = 1;
            this.RPanelReports.Visible = false;
            // 
            // RBarReports
            // 
            this.RBarReports.AutoOverflowEnabled = true;
            this.RBarReports.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarReports.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSellersReport,
            this.btnBuyersReport,
            this.btnTransferReport});
            this.RBarReports.Location = new System.Drawing.Point(3, 0);
            this.RBarReports.Name = "RBarReports";
            this.RBarReports.Size = new System.Drawing.Size(299, 89);
            this.RBarReports.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarReports.TabIndex = 0;
            this.RBarReports.Text = "گزارش های مدیریت";
            // 
            // btnSellersReport
            // 
            this.btnSellersReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSellersReport.Image = global::MohanirPouya.Properties.Resources.payment;
            this.btnSellersReport.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersReport.ImagePaddingHorizontal = 8;
            this.btnSellersReport.Name = "btnSellersReport";
            this.btnSellersReport.PopupSide = DevComponents.DotNetBar.ePopupSide.Right;
            this.btnSellersReport.Text = "فروشندگان";
            // 
            // btnBuyersReport
            // 
            this.btnBuyersReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnBuyersReport.Image = global::MohanirPouya.Properties.Resources.green_dollar;
            this.btnBuyersReport.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersReport.ImagePaddingHorizontal = 8;
            this.btnBuyersReport.Name = "btnBuyersReport";
            this.btnBuyersReport.Text = "خریداران";
            // 
            // btnTransferReport
            // 
            this.btnTransferReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnTransferReport.Image = global::MohanirPouya.Properties.Resources.web;
            this.btnTransferReport.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferReport.ImagePaddingHorizontal = 8;
            this.btnTransferReport.Name = "btnTransferReport";
            this.btnTransferReport.Stretch = true;
            this.btnTransferReport.Text = "خدمات انتقال";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel1.Controls.Add(this.ribbonBar1);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(785, 92);
            this.ribbonPanel1.TabIndex = 5;
            this.ribbonPanel1.Visible = false;
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnDailyBill,
            this.btnMonthlyBill,
            this.btnFinalBill});
            this.ribbonBar1.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(370, 89);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar1.TabIndex = 0;
            // 
            // btnDailyBill
            // 
            this.btnDailyBill.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnDailyBill.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDailyBill.Image = global::MohanirPouya.Properties.Resources.Time___Date;
            this.btnDailyBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnDailyBill.ImagePaddingHorizontal = 8;
            this.btnDailyBill.Name = "btnDailyBill";
            this.btnDailyBill.SubItemsExpandWidth = 14;
            this.btnDailyBill.Text = "روزانه";
            this.btnDailyBill.Click += new System.EventHandler(this.btnDailyBill_Click);
            // 
            // btnMonthlyBill
            // 
            this.btnMonthlyBill.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnMonthlyBill.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMonthlyBill.Image = global::MohanirPouya.Properties.Resources._4585;
            this.btnMonthlyBill.ImageFixedSize = new System.Drawing.Size(32, 32);
            this.btnMonthlyBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnMonthlyBill.ImagePaddingHorizontal = 8;
            this.btnMonthlyBill.Name = "btnMonthlyBill";
            this.btnMonthlyBill.SubItemsExpandWidth = 14;
            this.btnMonthlyBill.Text = "ماهیانه";
            this.btnMonthlyBill.Click += new System.EventHandler(this.btnMonthlyBill_Click);
            // 
            // btnFinalBill
            // 
            this.btnFinalBill.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFinalBill.Image = global::MohanirPouya.Properties.Resources._4165;
            this.btnFinalBill.ImageFixedSize = new System.Drawing.Size(32, 32);
            this.btnFinalBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnFinalBill.ImagePaddingHorizontal = 8;
            this.btnFinalBill.Name = "btnFinalBill";
            this.btnFinalBill.SplitButton = true;
            this.btnFinalBill.Stretch = true;
            this.btnFinalBill.SubItemsExpandWidth = 14;
            this.btnFinalBill.Text = "قطعی";
            this.btnFinalBill.Click += new System.EventHandler(this.btnFinalBill_Click);
            // 
            // RPanelHelp
            // 
            this.RPanelHelp.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RPanelHelp.Controls.Add(this.RBarHelp);
            this.RPanelHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RPanelHelp.Location = new System.Drawing.Point(0, 56);
            this.RPanelHelp.Name = "RPanelHelp";
            this.RPanelHelp.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.RPanelHelp.Size = new System.Drawing.Size(785, 92);
            this.RPanelHelp.TabIndex = 4;
            this.RPanelHelp.Visible = false;
            // 
            // RBarHelp
            // 
            this.RBarHelp.AutoOverflowEnabled = true;
            this.RBarHelp.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarHelp.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnContactSupport,
            this.btnActivateSupport,
            this.btnAbout});
            this.RBarHelp.Location = new System.Drawing.Point(3, 0);
            this.RBarHelp.Name = "RBarHelp";
            this.RBarHelp.Size = new System.Drawing.Size(354, 89);
            this.RBarHelp.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarHelp.TabIndex = 0;
            this.RBarHelp.Text = "كمك";
            // 
            // btnContactSupport
            // 
            this.btnContactSupport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnContactSupport.Image = global::MohanirPouya.Properties.Resources.Connect;
            this.btnContactSupport.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnContactSupport.ImagePaddingHorizontal = 8;
            this.btnContactSupport.Name = "btnContactSupport";
            this.btnContactSupport.Text = "ارتباط با پشتیبانی";
            this.btnContactSupport.Click += new System.EventHandler(this.btnContactSupport_Click);
            // 
            // btnActivateSupport
            // 
            this.btnActivateSupport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActivateSupport.Image = global::MohanirPouya.Properties.Resources.Security_Orange;
            this.btnActivateSupport.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnActivateSupport.ImagePaddingHorizontal = 8;
            this.btnActivateSupport.Name = "btnActivateSupport";
            this.btnActivateSupport.Text = "فعال سازی پشتیبانی";
            this.btnActivateSupport.Click += new System.EventHandler(this.btnActivateSupport_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAbout.Image = global::MohanirPouya.Properties.Resources.Fabvorites;
            this.btnAbout.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnAbout.ImagePaddingHorizontal = 8;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Text = "درباره ...";
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // RTabBill
            // 
            this.RTabBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabBill.ImagePaddingHorizontal = 8;
            this.RTabBill.Name = "RTabBill";
            this.RTabBill.Panel = this.ribbonPanel1;
            this.RTabBill.Text = "صورتحساب";
            // 
            // RTabReports
            // 
            this.RTabReports.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabReports.ImagePaddingHorizontal = 8;
            this.RTabReports.Name = "RTabReports";
            this.RTabReports.Panel = this.RPanelReports;
            this.RTabReports.Text = "گزارش ها";
            // 
            // RTabManagement
            // 
            this.RTabManagement.Checked = true;
            this.RTabManagement.ColorTable = DevComponents.DotNetBar.eRibbonTabColor.Green;
            this.RTabManagement.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabManagement.ImagePaddingHorizontal = 8;
            this.RTabManagement.Name = "RTabManagement";
            this.RTabManagement.Panel = this.RPanelManagement;
            this.RTabManagement.Text = "مدیریت";
            // 
            // RTabSupport
            // 
            this.RTabSupport.ColorTable = DevComponents.DotNetBar.eRibbonTabColor.Magenta;
            this.RTabSupport.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabSupport.ImagePaddingHorizontal = 8;
            this.RTabSupport.Name = "RTabSupport";
            this.RTabSupport.Panel = this.RPanelSupport;
            this.RTabSupport.Text = "پشتیبانی";
            // 
            // RTabHelp
            // 
            this.RTabHelp.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabHelp.ImagePaddingHorizontal = 8;
            this.RTabHelp.Name = "RTabHelp";
            this.RTabHelp.Panel = this.RPanelHelp;
            this.RTabHelp.Text = "راهنمایی";
            // 
            // btnChangeStyle
            // 
            this.btnChangeStyle.AutoExpandOnClick = true;
            this.btnChangeStyle.FontBold = true;
            this.btnChangeStyle.FontItalic = true;
            this.btnChangeStyle.ForeColor = System.Drawing.Color.Red;
            this.btnChangeStyle.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnChangeStyle.ImagePaddingHorizontal = 8;
            this.btnChangeStyle.Name = "btnChangeStyle";
            this.btnChangeStyle.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStyleOffice2007Blue,
            this.btnStyleOffice2007Black,
            this.btnStyleOffice2007Silver,
            this.btnStyleCustom});
            this.btnChangeStyle.Text = "استیل";
            this.btnChangeStyle.Tooltip = "ظاهر فرم جاری برنامه";
            // 
            // btnStyleOffice2007Blue
            // 
            this.btnStyleOffice2007Blue.Checked = true;
            this.btnStyleOffice2007Blue.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleOffice2007Blue.ImagePaddingHorizontal = 8;
            this.btnStyleOffice2007Blue.Name = "btnStyleOffice2007Blue";
            this.btnStyleOffice2007Blue.OptionGroup = "style";
            this.btnStyleOffice2007Blue.Text = "<font color=\"Blue\"><b>آبی</b></font>";
            this.btnStyleOffice2007Blue.Click += new System.EventHandler(this.buttonStyleOffice2007Blue_Click);
            // 
            // btnStyleOffice2007Black
            // 
            this.btnStyleOffice2007Black.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleOffice2007Black.ImagePaddingHorizontal = 8;
            this.btnStyleOffice2007Black.Name = "btnStyleOffice2007Black";
            this.btnStyleOffice2007Black.OptionGroup = "style";
            this.btnStyleOffice2007Black.Text = "<font color=\"black\"><b>مشكی</b></font>";
            this.btnStyleOffice2007Black.Click += new System.EventHandler(this.buttonStyleOffice2007Black_Click);
            // 
            // btnStyleOffice2007Silver
            // 
            this.btnStyleOffice2007Silver.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleOffice2007Silver.ImagePaddingHorizontal = 8;
            this.btnStyleOffice2007Silver.Name = "btnStyleOffice2007Silver";
            this.btnStyleOffice2007Silver.OptionGroup = "style";
            this.btnStyleOffice2007Silver.Text = "<font color=\"Silver\"><b>نقره ای</b></font>";
            this.btnStyleOffice2007Silver.Click += new System.EventHandler(this.buttonStyleOffice2007Silver_Click);
            // 
            // btnStyleCustom
            // 
            this.btnStyleCustom.BeginGroup = true;
            this.btnStyleCustom.ColorTable = DevComponents.DotNetBar.eButtonColor.Magenta;
            this.btnStyleCustom.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleCustom.ImagePaddingHorizontal = 8;
            this.btnStyleCustom.Name = "btnStyleCustom";
            this.btnStyleCustom.Text = "رنگ اختصاصی";
            this.btnStyleCustom.Tooltip = "انتخاب رنگ اختصاصی دلخواه شما";
            this.btnStyleCustom.ColorPreview += new DevComponents.DotNetBar.ColorPreviewEventHandler(this.btnStyleCustom_ColorPreview);
            this.btnStyleCustom.SelectedColorChanged += new System.EventHandler(this.buttonStyleCustom_SelectedColorChanged);
            // 
            // StartButton
            // 
            this.StartButton.AutoExpandOnClick = true;
            this.StartButton.CanCustomize = false;
            this.StartButton.Category = "پرونده";
            this.StartButton.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.StartButton.Image = ((System.Drawing.Image)(resources.GetObject("StartButton.Image")));
            this.StartButton.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.StartButton.ImagePaddingHorizontal = 2;
            this.StartButton.ImagePaddingVertical = 2;
            this.StartButton.Name = "StartButton";
            this.StartButton.ShowSubItems = false;
            this.StartButton.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ItemContainerStart,
            this.ItemContainerStartSmall});
            this.StartButton.Text = "پرونده";
            this.StartButton.Tooltip = "مدیریت پارامتر ها";
            // 
            // ItemContainerStart
            // 
            // 
            // 
            // 
            this.ItemContainerStart.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.ItemContainerStart.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.ItemContainerStart.Name = "ItemContainerStart";
            this.ItemContainerStart.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartSellers,
            this.btnStartBuyers,
            this.btnStartTransfer,
            this.btnStartOtherService});
            // 
            // btnStartSellers
            // 
            this.btnStartSellers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartSellers.Image = global::MohanirPouya.Properties.Resources.payment;
            this.btnStartSellers.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartSellers.ImagePaddingHorizontal = 8;
            this.btnStartSellers.Name = "btnStartSellers";
            this.btnStartSellers.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartSellersParams,
            this.btnStartSellersParamsC,
            this.btnStartSellersSarfasl,
            this.btnStartSellersShakhes,
            this.buttonItem4});
            this.btnStartSellers.Text = "فروشندگان";
            // 
            // btnStartSellersParams
            // 
            this.btnStartSellersParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartSellersParams.ImagePaddingHorizontal = 8;
            this.btnStartSellersParams.Name = "btnStartSellersParams";
            this.btnStartSellersParams.Text = "پارامتر ها";
            this.btnStartSellersParams.Click += new System.EventHandler(this.btnSellersParams_Click);
            // 
            // btnStartSellersParamsC
            // 
            this.btnStartSellersParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartSellersParamsC.ImagePaddingHorizontal = 8;
            this.btnStartSellersParamsC.Name = "btnStartSellersParamsC";
            this.btnStartSellersParamsC.Text = "پارامتر های شرطی";
            this.btnStartSellersParamsC.Click += new System.EventHandler(this.btnSellersParamsC_Click);
            // 
            // btnStartSellersSarfasl
            // 
            this.btnStartSellersSarfasl.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartSellersSarfasl.ImagePaddingHorizontal = 8;
            this.btnStartSellersSarfasl.Name = "btnStartSellersSarfasl";
            this.btnStartSellersSarfasl.Text = "سرفصل ها";
            this.btnStartSellersSarfasl.Click += new System.EventHandler(this.btnSellersSarfasl_Click);
            // 
            // btnStartSellersShakhes
            // 
            this.btnStartSellersShakhes.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartSellersShakhes.ImagePaddingHorizontal = 8;
            this.btnStartSellersShakhes.Name = "btnStartSellersShakhes";
            this.btnStartSellersShakhes.Text = "شاخص ها";
            this.btnStartSellersShakhes.Click += new System.EventHandler(this.btnSellersShakhes_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.BeginGroup = true;
            this.buttonItem4.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonItem4.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.buttonItem4.ImagePaddingHorizontal = 8;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Text = "آیتم های صورتحساب";
            // 
            // btnStartBuyers
            // 
            this.btnStartBuyers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartBuyers.Image = global::MohanirPouya.Properties.Resources.green_dollar;
            this.btnStartBuyers.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartBuyers.ImagePaddingHorizontal = 8;
            this.btnStartBuyers.Name = "btnStartBuyers";
            this.btnStartBuyers.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartBuyersParams,
            this.btnStartBuyersParamsC,
            this.btnStartBuyersSarfasl,
            this.btnStartBuyersShakhes,
            this.buttonItem5});
            this.btnStartBuyers.Text = "خریداران";
            // 
            // btnStartBuyersParams
            // 
            this.btnStartBuyersParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartBuyersParams.ImagePaddingHorizontal = 8;
            this.btnStartBuyersParams.Name = "btnStartBuyersParams";
            this.btnStartBuyersParams.Text = "پارامتر ها";
            this.btnStartBuyersParams.Click += new System.EventHandler(this.btnBuyersParams_Click);
            // 
            // btnStartBuyersParamsC
            // 
            this.btnStartBuyersParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartBuyersParamsC.ImagePaddingHorizontal = 8;
            this.btnStartBuyersParamsC.Name = "btnStartBuyersParamsC";
            this.btnStartBuyersParamsC.Text = "پارامتر های شرطی";
            this.btnStartBuyersParamsC.Click += new System.EventHandler(this.btnBuyersParamsC_Click);
            // 
            // btnStartBuyersSarfasl
            // 
            this.btnStartBuyersSarfasl.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartBuyersSarfasl.ImagePaddingHorizontal = 8;
            this.btnStartBuyersSarfasl.Name = "btnStartBuyersSarfasl";
            this.btnStartBuyersSarfasl.Text = "سرفصل ها";
            this.btnStartBuyersSarfasl.Click += new System.EventHandler(this.btnBuyersSarfasl_Click);
            // 
            // btnStartBuyersShakhes
            // 
            this.btnStartBuyersShakhes.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartBuyersShakhes.ImagePaddingHorizontal = 8;
            this.btnStartBuyersShakhes.Name = "btnStartBuyersShakhes";
            this.btnStartBuyersShakhes.Text = "شاخص ها";
            this.btnStartBuyersShakhes.Click += new System.EventHandler(this.btnBuyersShakhes_Click);
            // 
            // buttonItem5
            // 
            this.buttonItem5.BeginGroup = true;
            this.buttonItem5.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonItem5.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.buttonItem5.ImagePaddingHorizontal = 8;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Text = "آیتم های صورتحساب";
            // 
            // btnStartTransfer
            // 
            this.btnStartTransfer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartTransfer.Image = global::MohanirPouya.Properties.Resources.web;
            this.btnStartTransfer.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransfer.ImagePaddingHorizontal = 8;
            this.btnStartTransfer.Name = "btnStartTransfer";
            this.btnStartTransfer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartTransferLine,
            this.btnStartTransferTrans,
            this.buttonItem6});
            this.btnStartTransfer.Text = "خدمات انتقال";
            // 
            // btnStartTransferLine
            // 
            this.btnStartTransferLine.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransferLine.ImagePaddingHorizontal = 8;
            this.btnStartTransferLine.Name = "btnStartTransferLine";
            this.btnStartTransferLine.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartTransferTransfer,
            this.btnStartTransferDistribution});
            this.btnStartTransferLine.Text = "خط";
            // 
            // btnStartTransferTransfer
            // 
            this.btnStartTransferTransfer.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransferTransfer.ImagePaddingHorizontal = 8;
            this.btnStartTransferTransfer.Name = "btnStartTransferTransfer";
            this.btnStartTransferTransfer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartTransferParams,
            this.btnStartTranferParamsC});
            this.btnStartTransferTransfer.Text = "انتقال";
            // 
            // btnStartTransferParams
            // 
            this.btnStartTransferParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransferParams.ImagePaddingHorizontal = 8;
            this.btnStartTransferParams.Name = "btnStartTransferParams";
            this.btnStartTransferParams.Text = "پارامتر ها";
            // 
            // btnStartTranferParamsC
            // 
            this.btnStartTranferParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTranferParamsC.ImagePaddingHorizontal = 8;
            this.btnStartTranferParamsC.Name = "btnStartTranferParamsC";
            this.btnStartTranferParamsC.Text = "پارامتر های شرطی";
            // 
            // btnStartTransferDistribution
            // 
            this.btnStartTransferDistribution.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransferDistribution.ImagePaddingHorizontal = 8;
            this.btnStartTransferDistribution.Name = "btnStartTransferDistribution";
            this.btnStartTransferDistribution.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartDistributionParam,
            this.btnStartDistributionParamC});
            this.btnStartTransferDistribution.Text = "توزیع";
            // 
            // btnStartDistributionParam
            // 
            this.btnStartDistributionParam.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartDistributionParam.ImagePaddingHorizontal = 8;
            this.btnStartDistributionParam.Name = "btnStartDistributionParam";
            this.btnStartDistributionParam.Text = "پارامتر ها";
            // 
            // btnStartDistributionParamC
            // 
            this.btnStartDistributionParamC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartDistributionParamC.ImagePaddingHorizontal = 8;
            this.btnStartDistributionParamC.Name = "btnStartDistributionParamC";
            this.btnStartDistributionParamC.Text = "پارامتر های شرطی";
            // 
            // btnStartTransferTrans
            // 
            this.btnStartTransferTrans.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransferTrans.ImagePaddingHorizontal = 8;
            this.btnStartTransferTrans.Name = "btnStartTransferTrans";
            this.btnStartTransferTrans.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartTransferTransParam,
            this.btnStartTransferTransParamsC});
            this.btnStartTransferTrans.Text = "ترانس";
            // 
            // btnStartTransferTransParam
            // 
            this.btnStartTransferTransParam.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransferTransParam.ImagePaddingHorizontal = 8;
            this.btnStartTransferTransParam.Name = "btnStartTransferTransParam";
            this.btnStartTransferTransParam.Text = "پارامتر ها";
            // 
            // btnStartTransferTransParamsC
            // 
            this.btnStartTransferTransParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartTransferTransParamsC.ImagePaddingHorizontal = 8;
            this.btnStartTransferTransParamsC.Name = "btnStartTransferTransParamsC";
            this.btnStartTransferTransParamsC.Text = "پارامتر های شرطی";
            // 
            // buttonItem6
            // 
            this.buttonItem6.BeginGroup = true;
            this.buttonItem6.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonItem6.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.buttonItem6.ImagePaddingHorizontal = 8;
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.Text = "آیتم های صورتحساب";
            // 
            // btnStartOtherService
            // 
            this.btnStartOtherService.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartOtherService.Image = global::MohanirPouya.Properties.Resources.battery;
            this.btnStartOtherService.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartOtherService.ImagePaddingHorizontal = 8;
            this.btnStartOtherService.Name = "btnStartOtherService";
            this.btnStartOtherService.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartFreq,
            this.btnStartReActive,
            this.buttonItem7});
            this.btnStartOtherService.Text = "خدمات جانبی";
            // 
            // btnStartFreq
            // 
            this.btnStartFreq.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartFreq.ImagePaddingHorizontal = 8;
            this.btnStartFreq.Name = "btnStartFreq";
            this.btnStartFreq.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartFreqParam,
            this.btnStartFreqParamC});
            this.btnStartFreq.Text = "كنترل فركانس";
            // 
            // btnStartFreqParam
            // 
            this.btnStartFreqParam.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartFreqParam.ImagePaddingHorizontal = 8;
            this.btnStartFreqParam.Name = "btnStartFreqParam";
            this.btnStartFreqParam.Text = "پارامتر ها";
            // 
            // btnStartFreqParamC
            // 
            this.btnStartFreqParamC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartFreqParamC.ImagePaddingHorizontal = 8;
            this.btnStartFreqParamC.Name = "btnStartFreqParamC";
            this.btnStartFreqParamC.Text = "پارامتر های شرطی";
            // 
            // btnStartReActive
            // 
            this.btnStartReActive.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartReActive.ImagePaddingHorizontal = 8;
            this.btnStartReActive.Name = "btnStartReActive";
            this.btnStartReActive.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartReActiveParam,
            this.btnStartReActiveParamC});
            this.btnStartReActive.Text = "توان راكتیو";
            // 
            // btnStartReActiveParam
            // 
            this.btnStartReActiveParam.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartReActiveParam.ImagePaddingHorizontal = 8;
            this.btnStartReActiveParam.Name = "btnStartReActiveParam";
            this.btnStartReActiveParam.Text = "پارامتر ها";
            // 
            // btnStartReActiveParamC
            // 
            this.btnStartReActiveParamC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartReActiveParamC.ImagePaddingHorizontal = 8;
            this.btnStartReActiveParamC.Name = "btnStartReActiveParamC";
            this.btnStartReActiveParamC.Text = "پارامتر های شرطی";
            // 
            // buttonItem7
            // 
            this.buttonItem7.BeginGroup = true;
            this.buttonItem7.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonItem7.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.buttonItem7.ImagePaddingHorizontal = 8;
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.Text = "آیتم های صورتحساب";
            // 
            // ItemContainerStartSmall
            // 
            // 
            // 
            // 
            this.ItemContainerStartSmall.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.ItemContainerStartSmall.Name = "ItemContainerStartSmall";
            this.ItemContainerStartSmall.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartAbout,
            this.btnStartExit});
            // 
            // btnStartAbout
            // 
            this.btnStartAbout.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartAbout.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnStartAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnStartAbout.Image")));
            this.btnStartAbout.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartAbout.ImagePaddingHorizontal = 8;
            this.btnStartAbout.Name = "btnStartAbout";
            this.btnStartAbout.SubItemsExpandWidth = 24;
            this.btnStartAbout.Text = "درباره";
            this.btnStartAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnStartExit
            // 
            this.btnStartExit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnStartExit.Image = ((System.Drawing.Image)(resources.GetObject("btnStartExit.Image")));
            this.btnStartExit.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartExit.ImagePaddingHorizontal = 8;
            this.btnStartExit.Name = "btnStartExit";
            this.btnStartExit.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F10);
            this.btnStartExit.SubItemsExpandWidth = 24;
            this.btnStartExit.Text = "خروج از سیستم";
            this.btnStartExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // MainQatCustomizeItem
            // 
            this.MainQatCustomizeItem.Name = "MainQatCustomizeItem";
            this.MainQatCustomizeItem.Tooltip = "مدیریت دسترسی فوری به فرامین";
            // 
            // buttonItem11
            // 
            this.buttonItem11.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.buttonItem11.ImagePaddingHorizontal = 8;
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.SubItemsExpandWidth = 14;
            this.buttonItem11.Text = "buttonItem11";
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 150);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(785, 359);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 1;
            // 
            // MainStatusBar
            // 
            this.MainStatusBar.AccessibleDescription = "DotNetBar Bar (MainStatusBar)";
            this.MainStatusBar.AccessibleName = "DotNetBar Bar";
            this.MainStatusBar.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.MainStatusBar.AntiAlias = true;
            this.MainStatusBar.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.MainStatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MainStatusBar.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainStatusBar.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.MainStatusBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblCurentUser,
            this.lblUserGroup});
            this.MainStatusBar.ItemSpacing = 2;
            this.MainStatusBar.Location = new System.Drawing.Point(0, 509);
            this.MainStatusBar.Name = "MainStatusBar";
            this.MainStatusBar.Size = new System.Drawing.Size(785, 18);
            this.MainStatusBar.Stretch = true;
            this.MainStatusBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainStatusBar.TabIndex = 8;
            this.MainStatusBar.TabStop = false;
            this.MainStatusBar.Text = "barStatus";
            // 
            // lblCurentUser
            // 
            this.lblCurentUser.BeginGroup = true;
            this.lblCurentUser.Name = "lblCurentUser";
            this.lblCurentUser.Text = "كاربر جاری: محمد حسین شمشاد قد";
            // 
            // lblUserGroup
            // 
            this.lblUserGroup.Name = "lblUserGroup";
            this.lblUserGroup.Text = "گروه كاربری: مدیریت";
            // 
            // labelPosition
            // 
            this.labelPosition.Name = "labelPosition";
            this.labelPosition.PaddingLeft = 2;
            this.labelPosition.PaddingRight = 2;
            this.labelPosition.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.labelPosition.Width = 100;
            // 
            // itemContainer13
            // 
            // 
            // 
            // 
            this.itemContainer13.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer13.Name = "itemContainer13";
            // 
            // elementStyle1
            // 
            this.elementStyle1.Name = "elementStyle1";
            this.elementStyle1.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 527);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.MainRibbonControl);
            this.Controls.Add(this.MainStatusBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mohanir Pouya Settlement - نرم افزار تولید صورت حساب بازار برق ایران";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.MainRibbonControl.ResumeLayout(false);
            this.MainRibbonControl.PerformLayout();
            this.RPanelSupport.ResumeLayout(false);
            this.RPanelManagement.ResumeLayout(false);
            this.RPanelReports.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.RPanelHelp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainStatusBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl MainRibbonControl;
        private DevComponents.DotNetBar.RibbonPanel RPanelReports;
        private DevComponents.DotNetBar.RibbonPanel RPanelManagement;
        private DevComponents.DotNetBar.RibbonTabItem RTabReports;
        private DevComponents.DotNetBar.RibbonTabItem RTabManagement;
        private DevComponents.DotNetBar.Office2007StartButton StartButton;
        private DevComponents.DotNetBar.ItemContainer ItemContainerStartSmall;
        private DevComponents.DotNetBar.ButtonItem btnStartAbout;
        private DevComponents.DotNetBar.ButtonItem btnStartExit;
        private DevComponents.DotNetBar.QatCustomizeItem MainQatCustomizeItem;
        private DevComponents.DotNetBar.RibbonPanel RPanelHelp;
        private DevComponents.DotNetBar.RibbonPanel RPanelSupport;
        private DevComponents.DotNetBar.RibbonTabItem RTabSupport;
        private DevComponents.DotNetBar.RibbonTabItem RTabHelp;
        private DevComponents.DotNetBar.RibbonBar RBarSecurity;
        private DevComponents.DotNetBar.ButtonItem btnGroups;
        private DevComponents.DotNetBar.RibbonBar RBarReports;
        private DevComponents.DotNetBar.RibbonBar RBarHelp;
        private DevComponents.DotNetBar.RibbonBar RBarManageDb;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        private DevComponents.DotNetBar.ButtonItem btnSellersReport;
        private DevComponents.DotNetBar.ButtonItem btnBuyersReport;
        private DevComponents.DotNetBar.ButtonItem btnTransferReport;
        private DevComponents.DotNetBar.RibbonBar RBarParametersManagement;
        private DevComponents.DotNetBar.ButtonItem btnUsers;
        private DevComponents.DotNetBar.ButtonItem btnSellers;
        private DevComponents.DotNetBar.ButtonItem btnBuyers;
        private DevComponents.DotNetBar.ButtonItem btnTransfer;
        private DevComponents.DotNetBar.RibbonBar RBarCalculationManagement;
        private DevComponents.DotNetBar.ButtonItem btnUpdateCalculations;
        private DevComponents.DotNetBar.ButtonItem btnQueryEditor;
        private DevComponents.DotNetBar.RibbonBar RBarBackup;
        private DevComponents.DotNetBar.ButtonItem btnCheckTables;
        private DevComponents.DotNetBar.ButtonItem btnBackup;
        private DevComponents.DotNetBar.ButtonItem btnRestore;
        private DevComponents.DotNetBar.ButtonItem btnContactSupport;
        private DevComponents.DotNetBar.ButtonItem btnActivateSupport;
        private DevComponents.DotNetBar.ButtonItem btnAbout;
        private DevComponents.DotNetBar.ItemContainer ItemContainerStart;
        private DevComponents.DotNetBar.ButtonItem btnStartSellers;
        private DevComponents.DotNetBar.ButtonItem btnStartBuyers;
        private DevComponents.DotNetBar.ButtonItem btnStartTransfer;
        private DevComponents.DotNetBar.ButtonItem btnStartOtherService;
        private DevComponents.DotNetBar.ButtonItem btnSellersParams;
        private DevComponents.DotNetBar.ButtonItem btnSellersParamsC;
        private DevComponents.DotNetBar.ButtonItem btnSellersSarfasl;
        private DevComponents.DotNetBar.ButtonItem btnSellersShakhes;
        private DevComponents.DotNetBar.ButtonItem btnBuyersParams;
        private DevComponents.DotNetBar.ButtonItem btnBuyersParamsC;
        private DevComponents.DotNetBar.ButtonItem btnBuyersSarfasl;
        private DevComponents.DotNetBar.ButtonItem btnBuyersShakhes;
        private DevComponents.DotNetBar.ButtonItem btnStartSellersParams;
        private DevComponents.DotNetBar.ButtonItem btnStartSellersParamsC;
        private DevComponents.DotNetBar.ButtonItem btnStartSellersSarfasl;
        private DevComponents.DotNetBar.ButtonItem btnStartSellersShakhes;
        private DevComponents.DotNetBar.ButtonItem btnStartBuyersParams;
        private DevComponents.DotNetBar.ButtonItem btnStartBuyersParamsC;
        private DevComponents.DotNetBar.ButtonItem btnStartBuyersSarfasl;
        private DevComponents.DotNetBar.ButtonItem btnStartBuyersShakhes;
        private DevComponents.DotNetBar.ButtonItem btnStartTransferLine;
        private DevComponents.DotNetBar.ButtonItem btnStartTransferTrans;
        private DevComponents.DotNetBar.ButtonItem btnStartTransferTransParam;
        private DevComponents.DotNetBar.ButtonItem btnStartTransferTransParamsC;
        private DevComponents.DotNetBar.ButtonItem btnStartFreq;
        private DevComponents.DotNetBar.ButtonItem btnStartFreqParam;
        private DevComponents.DotNetBar.ButtonItem btnStartFreqParamC;
        private DevComponents.DotNetBar.ButtonItem btnStartReActive;
        private DevComponents.DotNetBar.ButtonItem btnStartReActiveParam;
        private DevComponents.DotNetBar.ButtonItem btnStartReActiveParamC;
        private DevComponents.DotNetBar.ButtonItem btnChangeStyle;
        private DevComponents.DotNetBar.ButtonItem btnStyleOffice2007Blue;
        private DevComponents.DotNetBar.ButtonItem btnStyleOffice2007Black;
        private DevComponents.DotNetBar.ButtonItem btnStyleOffice2007Silver;
        private DevComponents.DotNetBar.ColorPickerDropDown btnStyleCustom;
        private DevComponents.DotNetBar.Bar MainStatusBar;
        private DevComponents.DotNetBar.LabelItem lblCurentUser;
        private DevComponents.DotNetBar.LabelItem lblUserGroup;
        internal DevComponents.DotNetBar.LabelItem labelPosition;
        private DevComponents.DotNetBar.ItemContainer itemContainer13;
        private DevComponents.DotNetBar.ButtonItem btnStartTransferTransfer;
        private DevComponents.DotNetBar.ButtonItem btnStartTransferParams;
        private DevComponents.DotNetBar.ButtonItem btnStartTranferParamsC;
        private DevComponents.DotNetBar.ButtonItem btnStartTransferDistribution;
        private DevComponents.DotNetBar.ButtonItem btnStartDistributionParam;
        private DevComponents.DotNetBar.ButtonItem btnStartDistributionParamC;
        private DevComponents.DotNetBar.ElementStyle elementStyle1;
        private DevComponents.DotNetBar.ButtonItem btnSetSarfasl;
        private DevComponents.DotNetBar.ButtonItem btnSellersBillItems;
        private DevComponents.DotNetBar.ButtonItem btnBuyersBillItem;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem btnSellersManageBill;
        private DevComponents.DotNetBar.ButtonItem btnBuyersManageItem;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private DevComponents.DotNetBar.RibbonTabItem RTabBill;
        private DevComponents.DotNetBar.ButtonItem btnDailyBill;
        private DevComponents.DotNetBar.ButtonItem btnMonthlyBill;
        private DevComponents.DotNetBar.ButtonItem btnFinalBill;
        private DevComponents.DotNetBar.ButtonItem BtnSellerPowerBillItemSet;
        private DevComponents.DotNetBar.ButtonItem btnSellersUnitBillItemSet;
        private DevComponents.DotNetBar.ButtonItem btnTransferParams;
        private DevComponents.DotNetBar.ButtonItem btnTransferParamsC;
        private DevComponents.DotNetBar.ButtonItem btnTransferBillItem;
        private DevComponents.DotNetBar.ButtonItem btnTransferInfoIn;
        private DevComponents.DotNetBar.ButtonItem btnTransferInfoDel;
        private DevComponents.DotNetBar.ButtonItem btn_Tazmini;
        private DevComponents.DotNetBar.ButtonItem btnTransferBillItemD;
        private DevComponents.DotNetBar.ButtonItem Param_Structure_S;
        private DevComponents.DotNetBar.ButtonItem Param_Structure_B;
        private DevComponents.DotNetBar.ButtonItem Param_Structure_T;
    }
}