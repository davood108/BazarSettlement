﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MohanirPouya.Forms.Support
{
    public partial class frmQueryDesigner : Form
    {

        #region Fields

        DataTable _TempTable;

        #endregion

        #region Ctors

        public frmQueryDesigner()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (chkExecutingNonQuery.Checked)
                ExecutingNonQuery();
            if (chkExecutingReader.Checked)
                ExecutingReader();
            if (chkExecutingScalar.Checked)
                ExecutingScalar();
        }

        #endregion
        
        #region Methods

        #region ExecutingNonQuery

        void ExecutingNonQuery()
        {
            SqlCommand MyCommand =
                new SqlCommand(SqlStatementText.Text, 
                    new SqlConnection(Classes.DbBizClass.dbConnStr));
            try
            {
                MyCommand.Connection.Open();
                MessageBox.Show(MyCommand.ExecuteNonQuery().ToString() ,"Result");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message , "Error!");
            }
            finally
            {
                MyCommand.Connection.Close();
            }
        }

        #endregion

        #region ExecutingReader

        void ExecutingReader()
        {
            SqlCommand MyCommand =
                new SqlCommand(SqlStatementText.Text,
                    new SqlConnection(Classes.DbBizClass.dbConnStr));
            _TempTable = new DataTable();
            try
            {
                MyCommand.Connection.Open();
                _TempTable.Load(MyCommand.ExecuteReader());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error!");
            }
            finally
            {
                MyCommand.Connection.Close();
            }
            dgvResult.DataSource = _TempTable;
        }

        #endregion

        #region ExecutingScalar

        void ExecutingScalar()
        {
            SqlCommand MyCommand =
                new SqlCommand(SqlStatementText.Text,
                    new SqlConnection(Classes.DbBizClass.dbConnStr));
            try
            {
                MyCommand.Connection.Open();
                MessageBox.Show(MyCommand.ExecuteScalar().ToString());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error!");
            }
            finally
            {
                MyCommand.Connection.Close();
            }
        }

        #endregion

        #endregion
    }

}