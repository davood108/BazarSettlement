﻿namespace MohanirPouya.Forms.Support
{
    partial class frmQueryDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SqlStatementText = new System.Windows.Forms.RichTextBox();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.GroupPanelOptions = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnExecute = new DevComponents.DotNetBar.ButtonX();
            this.chkExecutingNonQuery = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkExecutingReader = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkExecutingScalar = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.panelEx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.GroupPanelOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // SqlStatementText
            // 
            this.SqlStatementText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SqlStatementText.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.SqlStatementText.Location = new System.Drawing.Point(12, 12);
            this.SqlStatementText.Name = "SqlStatementText";
            this.SqlStatementText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SqlStatementText.Size = new System.Drawing.Size(672, 71);
            this.SqlStatementText.TabIndex = 0;
            this.SqlStatementText.Text = "";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.dgvResult);
            this.panelEx1.Controls.Add(this.GroupPanelOptions);
            this.panelEx1.Controls.Add(this.SqlStatementText);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(696, 479);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 1;
            // 
            // dgvResult
            // 
            this.dgvResult.AllowUserToAddRows = false;
            this.dgvResult.AllowUserToDeleteRows = false;
            this.dgvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvResult.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.dgvResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResult.Location = new System.Drawing.Point(12, 145);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.ReadOnly = true;
            this.dgvResult.RowTemplate.Height = 24;
            this.dgvResult.Size = new System.Drawing.Size(672, 322);
            this.dgvResult.TabIndex = 2;
            // 
            // GroupPanelOptions
            // 
            this.GroupPanelOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupPanelOptions.CanvasColor = System.Drawing.SystemColors.Control;
            this.GroupPanelOptions.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.GroupPanelOptions.Controls.Add(this.btnExecute);
            this.GroupPanelOptions.Controls.Add(this.chkExecutingNonQuery);
            this.GroupPanelOptions.Controls.Add(this.chkExecutingReader);
            this.GroupPanelOptions.Controls.Add(this.chkExecutingScalar);
            this.GroupPanelOptions.DrawTitleBox = false;
            this.GroupPanelOptions.Location = new System.Drawing.Point(12, 89);
            this.GroupPanelOptions.Name = "GroupPanelOptions";
            this.GroupPanelOptions.Size = new System.Drawing.Size(672, 50);
            // 
            // 
            // 
            this.GroupPanelOptions.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GroupPanelOptions.Style.BackColorGradientAngle = 90;
            this.GroupPanelOptions.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.GroupPanelOptions.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanelOptions.Style.BorderBottomWidth = 1;
            this.GroupPanelOptions.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GroupPanelOptions.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanelOptions.Style.BorderLeftWidth = 1;
            this.GroupPanelOptions.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanelOptions.Style.BorderRightWidth = 1;
            this.GroupPanelOptions.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanelOptions.Style.BorderTopWidth = 1;
            this.GroupPanelOptions.Style.CornerDiameter = 4;
            this.GroupPanelOptions.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GroupPanelOptions.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GroupPanelOptions.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.GroupPanelOptions.TabIndex = 1;
            this.GroupPanelOptions.Text = "انتخاب ها";
            // 
            // btnExecute
            // 
            this.btnExecute.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.Location = new System.Drawing.Point(523, 2);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(144, 23);
            this.btnExecute.TabIndex = 3;
            this.btnExecute.Text = "اجرای فرامین";
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // chkExecutingNonQuery
            // 
            this.chkExecutingNonQuery.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkExecutingNonQuery.Checked = true;
            this.chkExecutingNonQuery.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkExecutingNonQuery.CheckValue = "Y";
            this.chkExecutingNonQuery.Location = new System.Drawing.Point(4, 0);
            this.chkExecutingNonQuery.Name = "chkExecutingNonQuery";
            this.chkExecutingNonQuery.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkExecutingNonQuery.Size = new System.Drawing.Size(128, 23);
            this.chkExecutingNonQuery.TabIndex = 0;
            this.chkExecutingNonQuery.Text = "Executing NonQuery";
            // 
            // chkExecutingReader
            // 
            this.chkExecutingReader.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkExecutingReader.Location = new System.Drawing.Point(138, 0);
            this.chkExecutingReader.Name = "chkExecutingReader";
            this.chkExecutingReader.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkExecutingReader.Size = new System.Drawing.Size(116, 23);
            this.chkExecutingReader.TabIndex = 0;
            this.chkExecutingReader.Text = "Executing Reader";
            // 
            // chkExecutingScalar
            // 
            this.chkExecutingScalar.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkExecutingScalar.Location = new System.Drawing.Point(260, 0);
            this.chkExecutingScalar.Name = "chkExecutingScalar";
            this.chkExecutingScalar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkExecutingScalar.Size = new System.Drawing.Size(114, 23);
            this.chkExecutingScalar.TabIndex = 0;
            this.chkExecutingScalar.Text = "Executing Scalar";
            // 
            // frmQueryDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 479);
            this.Controls.Add(this.panelEx1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Name = "frmQueryDesigner";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "اجرای فرامین Sql";
            this.panelEx1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.GroupPanelOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox SqlStatementText;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private System.Windows.Forms.DataGridView dgvResult;
        private DevComponents.DotNetBar.Controls.GroupPanel GroupPanelOptions;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkExecutingNonQuery;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkExecutingReader;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkExecutingScalar;
        private DevComponents.DotNetBar.ButtonX btnExecute;
    }
}