﻿using System;
using System.Data.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirVBClasses;

namespace MohanirPouya.Forms.Management.UsersAndGroups
{
     public partial class UsersGroups :MohanirFormDialog 

    {
         private DbSMDataContext _DbSM;
  
         private Table<User> _DataSource;


         public UsersGroups()
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                 (DbBizClass.dbConnStr);
         dgvUser.AutoGenerateColumns = false;


            lblSep.Visible = false;
            lblSubtitle.Visible = false;
            lblTitle.Visible = false;
            label2.Text = "لیست کاربران";
            FilldgvDataSource();
            ShowDialog();
            
        }

         private void UsersGroups_Load(object sender, EventArgs e)
         {

         }


        #region Method

         #region FilldgvDataSource

         private void FilldgvDataSource()
         {
             _DataSource = _DbSM.Users;
             dgvUser.DataSource = _DataSource;
       }
        #endregion

        #endregion


     

         private void Apply_Click(object sender, EventArgs e)
         {
             try
             {
                 _DbSM.SubmitChanges();
             }
             catch(Exception)
             {
                 const String ErrorMessage =
                                   "خطادر واردكردن اطلاعات در بانك اطلاعات .\n" +
                                   "موارد زیر را بررسی نمایید:\n" +
                                   "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟\n"+
                                   "2. آیا اطلاعات تمام گزینه ها را وارد نموده اید؟";
                 PersianMessageBox.Show(ErrorMessage, "خطا!",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error,
                                        MessageBoxDefaultButton.Button1);
             }
         }

         private void AddItem_Click(object sender, EventArgs e)
         {
             dgvUser.AllowUserToAddRows = true;
         }

         private void dgvUser_DataError(object sender, DataGridViewDataErrorEventArgs e)
         {
             const String ErrorMessage =
                                  "گزینه مورد نظر باید دارای مقدار باشد ! " ;
             PersianMessageBox.Show(ErrorMessage, "خطا!",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1);
         }

         
    }
}