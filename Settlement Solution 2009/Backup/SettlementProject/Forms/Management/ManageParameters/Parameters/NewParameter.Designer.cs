﻿using MohanirCompilerRichTextBox = MohanirPouya.Forms.Options.ManageParameters.Classes.MohanirCompilerRichTextBox;

namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    partial class frmNewParameter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewParameter));
            this.lstParameters = new System.Windows.Forms.ListBox();
            this.lblParamName = new System.Windows.Forms.Label();
            this.txtParamName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblParamDescribe = new System.Windows.Forms.Label();
            this.txtParamDescribe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ParameterPanel = new DevComponents.DotNetBar.PanelEx();
            this.PanelDescribe = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblDescription = new System.Windows.Forms.Label();
            this.PanelCodeCompiler = new System.Windows.Forms.Panel();
            this.ToolStripFunctions = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnOpenFml = new System.Windows.Forms.ToolStripButton();
            this.btnSaveFml = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCutTextFromFml = new System.Windows.Forms.ToolStripButton();
            this.btnCopyTextFromFml = new System.Windows.Forms.ToolStripButton();
            this.btnPasteTextToFml = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.btnRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.lblOperators = new System.Windows.Forms.ToolStripLabel();
            this.btnOpSum = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpSubstract = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpMultiplation = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpDevide = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnFunctions = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnFkMax = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFkMin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPower = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSqrt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbs = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStep = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStepEPG = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAva = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSum1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum3 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum4 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum5 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum6 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum7 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblParamsList = new System.Windows.Forms.Label();
            this.btnSaveAndClose = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnValidation = new DevComponents.DotNetBar.ButtonX();
            this.MyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.PCreationDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.lblBeginDate = new System.Windows.Forms.Label();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.lblSubtitle = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnMax = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMin = new System.Windows.Forms.ToolStripMenuItem();
            this.rtbCode = new MohanirPouya.Forms.Options.ManageParameters.Classes.MohanirCompilerRichTextBox();
            this.ParameterPanel.SuspendLayout();
            this.PanelDescribe.SuspendLayout();
            this.PanelCodeCompiler.SuspendLayout();
            this.ToolStripFunctions.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstParameters
            // 
            this.lstParameters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lstParameters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstParameters.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstParameters.FormattingEnabled = true;
            this.lstParameters.IntegralHeight = false;
            this.lstParameters.ItemHeight = 14;
            this.lstParameters.Location = new System.Drawing.Point(7, 28);
            this.lstParameters.Name = "lstParameters";
            this.lstParameters.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lstParameters.Size = new System.Drawing.Size(206, 220);
            this.lstParameters.Sorted = true;
            this.lstParameters.TabIndex = 0;
            this.lstParameters.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstParameters_DoubleClick);
            this.lstParameters.SelectedIndexChanged += new System.EventHandler(this.lstParameters_SelectedIndexChanged);
            // 
            // lblParamName
            // 
            this.lblParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamName.AutoSize = true;
            this.lblParamName.Location = new System.Drawing.Point(708, 75);
            this.lblParamName.Name = "lblParamName";
            this.lblParamName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamName.Size = new System.Drawing.Size(109, 13);
            this.lblParamName.TabIndex = 1;
            this.lblParamName.Text = "نام پارامتر (انگلیسی):";
            // 
            // txtParamName
            // 
            this.txtParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParamName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtParamName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtParamName.Border.Class = "TextBoxBorder";
            this.txtParamName.Location = new System.Drawing.Point(604, 91);
            this.txtParamName.MaxLength = 50;
            this.txtParamName.Name = "txtParamName";
            this.txtParamName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtParamName.Size = new System.Drawing.Size(213, 21);
            this.txtParamName.TabIndex = 0;
            this.MyToolTip.SetToolTip(this.txtParamName, "ورود نام پارامتر اجباری می باشد. بدون ذكر فاصله نامی به انگلیسی وارد نمایید");
            this.txtParamName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtParamName_KeyPress);
            // 
            // lblParamDescribe
            // 
            this.lblParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamDescribe.AutoSize = true;
            this.lblParamDescribe.Location = new System.Drawing.Point(444, 76);
            this.lblParamDescribe.Name = "lblParamDescribe";
            this.lblParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamDescribe.Size = new System.Drawing.Size(51, 13);
            this.lblParamDescribe.TabIndex = 7;
            this.lblParamDescribe.Text = "توضیحات:";
            // 
            // txtParamDescribe
            // 
            this.txtParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtParamDescribe.Border.Class = "TextBoxBorder";
            this.txtParamDescribe.Location = new System.Drawing.Point(12, 92);
            this.txtParamDescribe.MaxLength = 100;
            this.txtParamDescribe.Name = "txtParamDescribe";
            this.txtParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtParamDescribe.Size = new System.Drawing.Size(480, 21);
            this.txtParamDescribe.TabIndex = 6;
            this.txtParamDescribe.Text = "توضیح پیرامون پارامتر";
            this.MyToolTip.SetToolTip(this.txtParamDescribe, "متنی برای ثبت توضیحات پیرامون پارامتر");
            // 
            // ParameterPanel
            // 
            this.ParameterPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ParameterPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.ParameterPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ParameterPanel.Controls.Add(this.PanelDescribe);
            this.ParameterPanel.Controls.Add(this.PanelCodeCompiler);
            this.ParameterPanel.Controls.Add(this.lblParamsList);
            this.ParameterPanel.Controls.Add(this.lstParameters);
            this.ParameterPanel.Location = new System.Drawing.Point(13, 119);
            this.ParameterPanel.Name = "ParameterPanel";
            this.ParameterPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ParameterPanel.RightToLeftLayout = true;
            this.ParameterPanel.Size = new System.Drawing.Size(804, 370);
            this.ParameterPanel.Style.Alignment = System.Drawing.StringAlignment.Far;
            this.ParameterPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.ParameterPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.ParameterPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ParameterPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ParameterPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ParameterPanel.Style.GradientAngle = 90;
            this.ParameterPanel.Style.LineAlignment = System.Drawing.StringAlignment.Near;
            this.ParameterPanel.TabIndex = 8;
            // 
            // PanelDescribe
            // 
            this.PanelDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PanelDescribe.BackColor = System.Drawing.Color.Transparent;
            this.PanelDescribe.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelDescribe.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelDescribe.Controls.Add(this.lblDescription);
            this.PanelDescribe.DrawTitleBox = false;
            this.PanelDescribe.Location = new System.Drawing.Point(7, 254);
            this.PanelDescribe.Name = "PanelDescribe";
            this.PanelDescribe.Size = new System.Drawing.Size(206, 104);
            // 
            // 
            // 
            this.PanelDescribe.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelDescribe.Style.BackColorGradientAngle = 90;
            this.PanelDescribe.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelDescribe.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderBottomWidth = 1;
            this.PanelDescribe.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelDescribe.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderLeftWidth = 1;
            this.PanelDescribe.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderRightWidth = 1;
            this.PanelDescribe.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderTopWidth = 1;
            this.PanelDescribe.Style.CornerDiameter = 4;
            this.PanelDescribe.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelDescribe.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelDescribe.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelDescribe.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.PanelDescribe.TabIndex = 2;
            this.PanelDescribe.Text = "توضیحات پارامتر ها";
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescription.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblDescription.Location = new System.Drawing.Point(0, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDescription.Size = new System.Drawing.Size(200, 82);
            this.lblDescription.TabIndex = 0;
            // 
            // PanelCodeCompiler
            // 
            this.PanelCodeCompiler.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelCodeCompiler.Controls.Add(this.rtbCode);
            this.PanelCodeCompiler.Controls.Add(this.ToolStripFunctions);
            this.PanelCodeCompiler.Location = new System.Drawing.Point(219, 8);
            this.PanelCodeCompiler.Name = "PanelCodeCompiler";
            this.PanelCodeCompiler.Size = new System.Drawing.Size(572, 350);
            this.PanelCodeCompiler.TabIndex = 11;
            // 
            // ToolStripFunctions
            // 
            this.ToolStripFunctions.AutoSize = false;
            this.ToolStripFunctions.BackColor = System.Drawing.Color.White;
            this.ToolStripFunctions.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToolStripFunctions.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStripFunctions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnOpenFml,
            this.btnSaveFml,
            this.toolStripSeparator3,
            this.btnCutTextFromFml,
            this.btnCopyTextFromFml,
            this.btnPasteTextToFml,
            this.toolStripSeparator1,
            this.btnUndo,
            this.btnRedo,
            this.toolStripSeparator,
            this.lblOperators,
            this.btnOpSum,
            this.btnOpSubstract,
            this.btnOpMultiplation,
            this.btnOpDevide,
            this.toolStripSeparator5,
            this.btnFunctions});
            this.ToolStripFunctions.Location = new System.Drawing.Point(0, 0);
            this.ToolStripFunctions.Name = "ToolStripFunctions";
            this.ToolStripFunctions.Size = new System.Drawing.Size(572, 25);
            this.ToolStripFunctions.TabIndex = 1;
            this.ToolStripFunctions.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(23, 22);
            this.btnNew.Text = "فرمول نویسی مجدد";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnOpenFml
            // 
            this.btnOpenFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOpenFml.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFml.Image")));
            this.btnOpenFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpenFml.Name = "btnOpenFml";
            this.btnOpenFml.Size = new System.Drawing.Size(23, 22);
            this.btnOpenFml.Text = "خواندن فایل فرمول";
            this.btnOpenFml.Click += new System.EventHandler(this.btnOpenFml_Click);
            // 
            // btnSaveFml
            // 
            this.btnSaveFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveFml.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveFml.Image")));
            this.btnSaveFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveFml.Name = "btnSaveFml";
            this.btnSaveFml.Size = new System.Drawing.Size(23, 22);
            this.btnSaveFml.Text = "ذخیره فرمول در فایل";
            this.btnSaveFml.Click += new System.EventHandler(this.btnSaveFml_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCutTextFromFml
            // 
            this.btnCutTextFromFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCutTextFromFml.Image = ((System.Drawing.Image)(resources.GetObject("btnCutTextFromFml.Image")));
            this.btnCutTextFromFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCutTextFromFml.Name = "btnCutTextFromFml";
            this.btnCutTextFromFml.Size = new System.Drawing.Size(23, 22);
            this.btnCutTextFromFml.Text = "برش";
            this.btnCutTextFromFml.Click += new System.EventHandler(this.btnCutTextFromFml_Click);
            // 
            // btnCopyTextFromFml
            // 
            this.btnCopyTextFromFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopyTextFromFml.Image = ((System.Drawing.Image)(resources.GetObject("btnCopyTextFromFml.Image")));
            this.btnCopyTextFromFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopyTextFromFml.Name = "btnCopyTextFromFml";
            this.btnCopyTextFromFml.Size = new System.Drawing.Size(23, 22);
            this.btnCopyTextFromFml.Text = "رونوشت";
            this.btnCopyTextFromFml.Click += new System.EventHandler(this.btnCopyTextFromFml_Click);
            // 
            // btnPasteTextToFml
            // 
            this.btnPasteTextToFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPasteTextToFml.Image = ((System.Drawing.Image)(resources.GetObject("btnPasteTextToFml.Image")));
            this.btnPasteTextToFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPasteTextToFml.Name = "btnPasteTextToFml";
            this.btnPasteTextToFml.Size = new System.Drawing.Size(23, 22);
            this.btnPasteTextToFml.Text = "چسباندن";
            this.btnPasteTextToFml.Click += new System.EventHandler(this.btnPasteTextToFml_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::MohanirPouya.Properties.Resources.Redo;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "بازگشت";
            this.btnUndo.ToolTipText = "باز گرداندن تغییرات به حالت قبلی";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnRedo
            // 
            this.btnRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRedo.Image = global::MohanirPouya.Properties.Resources.Undo;
            this.btnRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(23, 22);
            this.btnRedo.Text = "پیش روی";
            this.btnRedo.ToolTipText = "باز گرداندن تغییرات به حالت بعدی";
            this.btnRedo.Click += new System.EventHandler(this.btnRedo_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // lblOperators
            // 
            this.lblOperators.ForeColor = System.Drawing.Color.Red;
            this.lblOperators.Name = "lblOperators";
            this.lblOperators.Size = new System.Drawing.Size(56, 22);
            this.lblOperators.Text = "عملگر ها:";
            // 
            // btnOpSum
            // 
            this.btnOpSum.Name = "btnOpSum";
            this.btnOpSum.Size = new System.Drawing.Size(27, 25);
            this.btnOpSum.Text = "+";
            this.btnOpSum.ToolTipText = "جمع";
            this.btnOpSum.Click += new System.EventHandler(this.btnOpSum_Click);
            // 
            // btnOpSubstract
            // 
            this.btnOpSubstract.Name = "btnOpSubstract";
            this.btnOpSubstract.Size = new System.Drawing.Size(23, 25);
            this.btnOpSubstract.Text = "-";
            this.btnOpSubstract.ToolTipText = "تفریق";
            this.btnOpSubstract.Click += new System.EventHandler(this.btnOpSubstract_Click);
            // 
            // btnOpMultiplation
            // 
            this.btnOpMultiplation.Name = "btnOpMultiplation";
            this.btnOpMultiplation.Size = new System.Drawing.Size(26, 25);
            this.btnOpMultiplation.Text = "*";
            this.btnOpMultiplation.ToolTipText = "ضرب";
            this.btnOpMultiplation.Click += new System.EventHandler(this.btnOpMultiplation_Click);
            // 
            // btnOpDevide
            // 
            this.btnOpDevide.Name = "btnOpDevide";
            this.btnOpDevide.Size = new System.Drawing.Size(24, 25);
            this.btnOpDevide.Text = "/";
            this.btnOpDevide.ToolTipText = "تقسیم";
            this.btnOpDevide.Click += new System.EventHandler(this.btnOpDevide_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnFunctions
            // 
            this.btnFunctions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMin,
            this.btnMax,
            this.btnFkMax,
            this.btnFkMin,
            this.toolStripSeparator4,
            this.btnPower,
            this.btnSqrt,
            this.btnAbs,
            this.btnStep,
            this.btnStepEPG,
            this.btnAva,
            this.toolStripSeparator6,
            this.btnSum1,
            this.btnSum2,
            this.btnSum3,
            this.btnSum4,
            this.btnSum5,
            this.btnSum6,
            this.btnSum7});
            this.btnFunctions.ForeColor = System.Drawing.Color.Purple;
            this.btnFunctions.Image = ((System.Drawing.Image)(resources.GetObject("btnFunctions.Image")));
            this.btnFunctions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFunctions.Name = "btnFunctions";
            this.btnFunctions.Size = new System.Drawing.Size(41, 22);
            this.btnFunctions.Text = "توابع";
            // 
            // btnFkMax
            // 
            this.btnFkMax.Name = "btnFkMax";
            this.btnFkMax.Size = new System.Drawing.Size(212, 22);
            this.btnFkMax.Text = "ماكسیمم دو ضابطه ای";
            this.btnFkMax.Click += new System.EventHandler(this.btnFkMax_Click);
            // 
            // btnFkMin
            // 
            this.btnFkMin.Name = "btnFkMin";
            this.btnFkMin.Size = new System.Drawing.Size(212, 22);
            this.btnFkMin.Text = "مینیمم دو ضابطه ای";
            this.btnFkMin.Click += new System.EventHandler(this.btnFkMin_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(209, 6);
            // 
            // btnPower
            // 
            this.btnPower.Name = "btnPower";
            this.btnPower.Size = new System.Drawing.Size(212, 22);
            this.btnPower.Text = "توان";
            this.btnPower.Click += new System.EventHandler(this.btnPower_Click);
            // 
            // btnSqrt
            // 
            this.btnSqrt.Name = "btnSqrt";
            this.btnSqrt.Size = new System.Drawing.Size(212, 22);
            this.btnSqrt.Text = "جذر";
            this.btnSqrt.Click += new System.EventHandler(this.btnSqrt_Click);
            // 
            // btnAbs
            // 
            this.btnAbs.Name = "btnAbs";
            this.btnAbs.Size = new System.Drawing.Size(212, 22);
            this.btnAbs.Text = "قدر مطلق";
            this.btnAbs.ToolTipText = "تابع قدر مطلق ، این تابع یك مقدار را دریافت كرده و قدر مطلق آن را بر می گرداند";
            this.btnAbs.Click += new System.EventHandler(this.btnAbs_Click);
            // 
            // btnStep
            // 
            this.btnStep.Name = "btnStep";
            this.btnStep.Size = new System.Drawing.Size(212, 22);
            this.btnStep.Text = "محاسبه با پله قیمت نیروگاه";
            this.btnStep.Click += new System.EventHandler(this.btnStep_Click);
            // 
            // btnStepEPG
            // 
            this.btnStepEPG.Name = "btnStepEPG";
            this.btnStepEPG.Size = new System.Drawing.Size(212, 22);
            this.btnStepEPG.Text = "محاسبه با پله قیمت القایی";
            this.btnStepEPG.Click += new System.EventHandler(this.btnStepEPG_Click);
            // 
            // btnAva
            // 
            this.btnAva.Name = "btnAva";
            this.btnAva.Size = new System.Drawing.Size(212, 22);
            this.btnAva.Text = "تابع آمادگی انرژی محدود";
            this.btnAva.Click += new System.EventHandler(this.btnAva_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(209, 6);
            // 
            // btnSum1
            // 
            this.btnSum1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum1.Name = "btnSum1";
            this.btnSum1.Size = new System.Drawing.Size(212, 22);
            this.btnSum1.Text = "مجموع نوع واحد ها";
            this.btnSum1.Click += new System.EventHandler(this.btnSumByUnitType_Click);
            // 
            // btnSum2
            // 
            this.btnSum2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum2.Name = "btnSum2";
            this.btnSum2.Size = new System.Drawing.Size(212, 22);
            this.btnSum2.Text = "مجموع نیروگاه ها";
            this.btnSum2.Click += new System.EventHandler(this.btnSumByPowerStation_Click);
            // 
            // btnSum3
            // 
            this.btnSum3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum3.Name = "btnSum3";
            this.btnSum3.Size = new System.Drawing.Size(212, 22);
            this.btnSum3.Text = "مجموع شركت ها";
            this.btnSum3.Click += new System.EventHandler(this.btnSumByCompany_Click);
            // 
            // btnSum4
            // 
            this.btnSum4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum4.Name = "btnSum4";
            this.btnSum4.Size = new System.Drawing.Size(212, 22);
            this.btnSum4.Text = "مجموع نوع شركت";
            this.btnSum4.Click += new System.EventHandler(this.btnSumByCompanyType_Click);
            // 
            // btnSum5
            // 
            this.btnSum5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum5.Name = "btnSum5";
            this.btnSum5.Size = new System.Drawing.Size(212, 22);
            this.btnSum5.Text = "مجموع در منطقه";
            this.btnSum5.Click += new System.EventHandler(this.btnSumByRegion_Click);
            // 
            // btnSum6
            // 
            this.btnSum6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum6.Name = "btnSum6";
            this.btnSum6.Size = new System.Drawing.Size(212, 22);
            this.btnSum6.Text = "مجموع روزانه";
            this.btnSum6.Click += new System.EventHandler(this.btnSumByDay_Click);
            // 
            // btnSum7
            // 
            this.btnSum7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum7.Name = "btnSum7";
            this.btnSum7.Size = new System.Drawing.Size(212, 22);
            this.btnSum7.Text = "مجموع ماهیانه";
            this.btnSum7.Click += new System.EventHandler(this.btnSumByMonth_Click);
            // 
            // lblParamsList
            // 
            this.lblParamsList.AutoSize = true;
            this.lblParamsList.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblParamsList.Location = new System.Drawing.Point(7, 8);
            this.lblParamsList.Name = "lblParamsList";
            this.lblParamsList.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamsList.Size = new System.Drawing.Size(56, 14);
            this.lblParamsList.TabIndex = 1;
            this.lblParamsList.Text = "پارامتر ها:";
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveAndClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveAndClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSaveAndClose.Location = new System.Drawing.Point(12, 495);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSaveAndClose.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F8);
            this.btnSaveAndClose.Size = new System.Drawing.Size(120, 28);
            this.btnSaveAndClose.TabIndex = 9;
            this.btnSaveAndClose.Text = "ذخیره و تایید <b><font color=\"#ED1C24\">(F8)</font></b>";
            this.btnSaveAndClose.Tooltip = "ذخیره نمودن پارامتر و بست فرم ایجاد پارامتر";
            this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(697, 495);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(120, 28);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Tooltip = "انصرف از طراحی پارامتر و خروج از فرم";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnValidation
            // 
            this.btnValidation.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnValidation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnValidation.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnValidation.Location = new System.Drawing.Point(138, 495);
            this.btnValidation.Name = "btnValidation";
            this.btnValidation.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnValidation.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5);
            this.btnValidation.Size = new System.Drawing.Size(120, 28);
            this.btnValidation.TabIndex = 10;
            this.btnValidation.Text = "بررسی دستورات <b><font color=\"#ED1C24\">(F5)</font></b>";
            this.btnValidation.Tooltip = "بررسی دستورات وارد شده";
            this.btnValidation.Click += new System.EventHandler(this.btnValidation_Click);
            // 
            // MyToolTip
            // 
            this.MyToolTip.AutoPopDelay = 10000;
            this.MyToolTip.InitialDelay = 500;
            this.MyToolTip.IsBalloon = true;
            this.MyToolTip.ReshowDelay = 100;
            this.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MyToolTip.ToolTipTitle = "راهنمایی";
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.PCreationDate);
            this.MainPanel.Controls.Add(this.lblBeginDate);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Controls.Add(this.ParameterPanel);
            this.MainPanel.Controls.Add(this.btnSaveAndClose);
            this.MainPanel.Controls.Add(this.txtParamDescribe);
            this.MainPanel.Controls.Add(this.lblParamDescribe);
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.lblParamName);
            this.MainPanel.Controls.Add(this.btnValidation);
            this.MainPanel.Controls.Add(this.txtParamName);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(827, 535);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 0;
            // 
            // PCreationDate
            // 
            this.PCreationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PCreationDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.PCreationDate.IsNull = false;
            this.PCreationDate.Location = new System.Drawing.Point(498, 92);
            this.PCreationDate.Name = "PCreationDate";
            this.PCreationDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PCreationDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.PCreationDate.Size = new System.Drawing.Size(100, 20);
            this.PCreationDate.TabIndex = 63;
            this.PCreationDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBeginDate.AutoSize = true;
            this.lblBeginDate.Location = new System.Drawing.Point(521, 75);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBeginDate.Size = new System.Drawing.Size(80, 13);
            this.lblBeginDate.TabIndex = 44;
            this.lblBeginDate.Text = "تاریخ آغاز اعتبار:";
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.lblSubtitle);
            this.TopPanel.Controls.Add(this.lblTitle);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(827, 69);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.TopPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 43;
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubtitle.Font = new System.Drawing.Font("B Yekan", 14.25F);
            this.lblSubtitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblSubtitle.Location = new System.Drawing.Point(3, 33);
            this.lblSubtitle.Name = "lblSubtitle";
            this.lblSubtitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSubtitle.Size = new System.Drawing.Size(818, 29);
            this.lblSubtitle.TabIndex = 29;
            this.lblSubtitle.Text = "ایجاد پارامتر جدید";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(823, 37);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "خدمات انتقال - خط";
            // 
            // btnMax
            // 
            this.btnMax.Name = "btnMax";
            this.btnMax.Size = new System.Drawing.Size(212, 22);
            this.btnMax.Text = "ماکسیمم تک ضابطه ای";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnMin
            // 
            this.btnMin.Name = "btnMin";
            this.btnMin.Size = new System.Drawing.Size(212, 22);
            this.btnMin.Text = "مینیمم تک ضابطه ای";
            this.btnMin.Click += new System.EventHandler(this.btnMin_Click);
            // 
            // rtbCode
            // 
            this.rtbCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCode.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rtbCode.Location = new System.Drawing.Point(0, 25);
            this.rtbCode.Name = "rtbCode";
            this.rtbCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtbCode.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbCode.Size = new System.Drawing.Size(572, 325);
            this.rtbCode.TabIndex = 0;
            this.rtbCode.Text = "";
            // 
            // frmNewParameter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(827, 535);
            this.Controls.Add(this.MainPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MinimizeBox = false;
            this.Name = "frmNewParameter";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم ایجاد پارامتر جدید";
            this.Load += new System.EventHandler(this.Form_Load);
            this.ParameterPanel.ResumeLayout(false);
            this.ParameterPanel.PerformLayout();
            this.PanelDescribe.ResumeLayout(false);
            this.PanelCodeCompiler.ResumeLayout(false);
            this.ToolStripFunctions.ResumeLayout(false);
            this.ToolStripFunctions.PerformLayout();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstParameters;
        private System.Windows.Forms.Label lblParamName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtParamName;
        private System.Windows.Forms.Label lblParamDescribe;
        private DevComponents.DotNetBar.Controls.TextBoxX txtParamDescribe;
        private DevComponents.DotNetBar.PanelEx ParameterPanel;
        private System.Windows.Forms.Label lblParamsList;
        private System.Windows.Forms.ToolStrip ToolStripFunctions;
        private System.Windows.Forms.ToolStripDropDownButton btnFunctions;
        private System.Windows.Forms.ToolStripMenuItem btnFkMax;
        private System.Windows.Forms.ToolStripMenuItem btnFkMin;
        private DevComponents.DotNetBar.ButtonX btnSaveAndClose;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private System.Windows.Forms.ToolStripButton btnOpenFml;
        private System.Windows.Forms.ToolStripButton btnSaveFml;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnCutTextFromFml;
        private System.Windows.Forms.ToolStripButton btnCopyTextFromFml;
        private System.Windows.Forms.ToolStripButton btnPasteTextToFml;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.Panel PanelCodeCompiler;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripButton btnRedo;
        private DevComponents.DotNetBar.ButtonX btnValidation;
        private System.Windows.Forms.ToolStripMenuItem btnSqrt;
        private System.Windows.Forms.ToolStripMenuItem btnAbs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem btnPower;
        private DevComponents.DotNetBar.Controls.GroupPanel PanelDescribe;
        private System.Windows.Forms.Label lblDescription;
        private MohanirCompilerRichTextBox rtbCode;
        private System.Windows.Forms.ToolStripMenuItem btnOpSum;
        private System.Windows.Forms.ToolStripMenuItem btnOpSubstract;
        private System.Windows.Forms.ToolStripMenuItem btnOpMultiplation;
        private System.Windows.Forms.ToolStripMenuItem btnOpDevide;
        private System.Windows.Forms.ToolStripLabel lblOperators;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolTip MyToolTip;
        private System.Windows.Forms.ToolStripMenuItem btnSum1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem btnSum2;
        private System.Windows.Forms.ToolStripMenuItem btnSum3;
        private System.Windows.Forms.ToolStripMenuItem btnSum4;
        private System.Windows.Forms.ToolStripMenuItem btnSum5;
        private System.Windows.Forms.ToolStripMenuItem btnSum7;
        private System.Windows.Forms.ToolStripMenuItem btnSum6;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private System.Windows.Forms.Label lblSubtitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripMenuItem btnStep;
        private System.Windows.Forms.ToolStripMenuItem btnAva;
        private System.Windows.Forms.Label lblBeginDate;
        public RPNCalendar.UI.Controls.PersianDatePicker PCreationDate;
        private System.Windows.Forms.ToolStripMenuItem btnStepEPG;
        private System.Windows.Forms.ToolStripMenuItem btnMax;
        private System.Windows.Forms.ToolStripMenuItem btnMin;
    }
}