﻿using System.Windows.Forms;
using MohanirCompilerRichTextBox=MohanirPouya.Forms.Options.ManageParameters.Classes.MohanirCompilerRichTextBox;

namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    partial class frmModifyParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmModifyParameters));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ParameterPanel = new DevComponents.DotNetBar.PanelEx();
            this.PanelDescribe = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblDescription = new System.Windows.Forms.Label();
            this.PanelCodeCompiler = new System.Windows.Forms.Panel();
            this.rtbCode = new MohanirPouya.Forms.Options.ManageParameters.Classes.MohanirCompilerRichTextBox();
            this.ToolStripFunctions = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnOpenFml = new System.Windows.Forms.ToolStripButton();
            this.btnSaveFml = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCutTextFromFml = new System.Windows.Forms.ToolStripButton();
            this.btnCopyTextFromFml = new System.Windows.Forms.ToolStripButton();
            this.btnPasteTextToFml = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.btnRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lblOperators = new System.Windows.Forms.ToolStripLabel();
            this.btnOpSum = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpSubstract = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpMultiplation = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpDevide = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnFunctions = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnFkMax = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFkMin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPower = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSqrt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSum1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum3 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum4 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum5 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum6 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSum7 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblParamsList = new System.Windows.Forms.Label();
            this.lstParameters = new System.Windows.Forms.ListBox();
            this.lblParamName = new System.Windows.Forms.Label();
            this.lblParamDescribe = new System.Windows.Forms.Label();
            this.txtParamDescribe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.MyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.lblCurrentParamName = new System.Windows.Forms.Label();
            this.ParametersLog = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.CCreationDate = new System.Data.DataColumn();
            this.CStartDate = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.btnValidation = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSaveAndClose = new DevComponents.DotNetBar.ButtonX();
            this.dgvLog = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colmun8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colmun5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.revisionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creationDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewColumn();
            this.startDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewColumn();
            this.expirationDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.btnDeActive = new DevComponents.DotNetBar.ButtonX();
            this.PCreationDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.lblBeginDate = new System.Windows.Forms.Label();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.lblSubtitle = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnMin = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMax = new System.Windows.Forms.ToolStripMenuItem();
            this.ParameterPanel.SuspendLayout();
            this.PanelDescribe.SuspendLayout();
            this.PanelCodeCompiler.SuspendLayout();
            this.ToolStripFunctions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLog)).BeginInit();
            this.MainPanel.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ParameterPanel
            // 
            this.ParameterPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ParameterPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.ParameterPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ParameterPanel.Controls.Add(this.PanelDescribe);
            this.ParameterPanel.Controls.Add(this.PanelCodeCompiler);
            this.ParameterPanel.Controls.Add(this.lblParamsList);
            this.ParameterPanel.Controls.Add(this.lstParameters);
            this.ParameterPanel.Location = new System.Drawing.Point(12, 115);
            this.ParameterPanel.Name = "ParameterPanel";
            this.ParameterPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ParameterPanel.RightToLeftLayout = true;
            this.ParameterPanel.Size = new System.Drawing.Size(803, 349);
            this.ParameterPanel.Style.Alignment = System.Drawing.StringAlignment.Far;
            this.ParameterPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.ParameterPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.ParameterPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ParameterPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ParameterPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ParameterPanel.Style.GradientAngle = 90;
            this.ParameterPanel.Style.LineAlignment = System.Drawing.StringAlignment.Near;
            this.ParameterPanel.TabIndex = 18;
            // 
            // PanelDescribe
            // 
            this.PanelDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PanelDescribe.BackColor = System.Drawing.Color.Transparent;
            this.PanelDescribe.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelDescribe.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelDescribe.Controls.Add(this.lblDescription);
            this.PanelDescribe.DrawTitleBox = false;
            this.PanelDescribe.Location = new System.Drawing.Point(7, 266);
            this.PanelDescribe.Name = "PanelDescribe";
            this.PanelDescribe.Size = new System.Drawing.Size(204, 80);
            // 
            // 
            // 
            this.PanelDescribe.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelDescribe.Style.BackColorGradientAngle = 90;
            this.PanelDescribe.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelDescribe.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderBottomWidth = 1;
            this.PanelDescribe.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelDescribe.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderLeftWidth = 1;
            this.PanelDescribe.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderRightWidth = 1;
            this.PanelDescribe.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderTopWidth = 1;
            this.PanelDescribe.Style.CornerDiameter = 4;
            this.PanelDescribe.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelDescribe.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelDescribe.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelDescribe.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.PanelDescribe.TabIndex = 1;
            this.PanelDescribe.Text = "توضیحات پارامتر ها";
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescription.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblDescription.Location = new System.Drawing.Point(0, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDescription.Size = new System.Drawing.Size(198, 58);
            this.lblDescription.TabIndex = 0;
            // 
            // PanelCodeCompiler
            // 
            this.PanelCodeCompiler.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelCodeCompiler.Controls.Add(this.rtbCode);
            this.PanelCodeCompiler.Controls.Add(this.ToolStripFunctions);
            this.PanelCodeCompiler.Location = new System.Drawing.Point(217, 3);
            this.PanelCodeCompiler.Name = "PanelCodeCompiler";
            this.PanelCodeCompiler.Size = new System.Drawing.Size(581, 343);
            this.PanelCodeCompiler.TabIndex = 11;
            // 
            // rtbCode
            // 
            this.rtbCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCode.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rtbCode.Location = new System.Drawing.Point(0, 25);
            this.rtbCode.Name = "rtbCode";
            this.rtbCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtbCode.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbCode.Size = new System.Drawing.Size(581, 318);
            this.rtbCode.TabIndex = 0;
            this.rtbCode.Text = "";
            // 
            // ToolStripFunctions
            // 
            this.ToolStripFunctions.AutoSize = false;
            this.ToolStripFunctions.BackColor = System.Drawing.Color.White;
            this.ToolStripFunctions.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToolStripFunctions.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStripFunctions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnOpenFml,
            this.btnSaveFml,
            this.toolStripSeparator3,
            this.btnCutTextFromFml,
            this.btnCopyTextFromFml,
            this.btnPasteTextToFml,
            this.toolStripSeparator1,
            this.btnUndo,
            this.btnRedo,
            this.toolStripSeparator2,
            this.lblOperators,
            this.btnOpSum,
            this.btnOpSubstract,
            this.btnOpMultiplation,
            this.btnOpDevide,
            this.toolStripSeparator5,
            this.btnFunctions});
            this.ToolStripFunctions.Location = new System.Drawing.Point(0, 0);
            this.ToolStripFunctions.Name = "ToolStripFunctions";
            this.ToolStripFunctions.Size = new System.Drawing.Size(581, 25);
            this.ToolStripFunctions.TabIndex = 1;
            this.ToolStripFunctions.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(23, 22);
            this.btnNew.Text = "فرمول نویسی مجدد";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnOpenFml
            // 
            this.btnOpenFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOpenFml.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFml.Image")));
            this.btnOpenFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpenFml.Name = "btnOpenFml";
            this.btnOpenFml.Size = new System.Drawing.Size(23, 22);
            this.btnOpenFml.Text = "خواندن فایل فرمول";
            this.btnOpenFml.Click += new System.EventHandler(this.btnOpenFml_Click);
            // 
            // btnSaveFml
            // 
            this.btnSaveFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveFml.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveFml.Image")));
            this.btnSaveFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveFml.Name = "btnSaveFml";
            this.btnSaveFml.Size = new System.Drawing.Size(23, 22);
            this.btnSaveFml.Text = "ذخیره فرمول در فایل";
            this.btnSaveFml.Click += new System.EventHandler(this.btnSaveFml_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCutTextFromFml
            // 
            this.btnCutTextFromFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCutTextFromFml.Image = ((System.Drawing.Image)(resources.GetObject("btnCutTextFromFml.Image")));
            this.btnCutTextFromFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCutTextFromFml.Name = "btnCutTextFromFml";
            this.btnCutTextFromFml.Size = new System.Drawing.Size(23, 22);
            this.btnCutTextFromFml.Text = "برش";
            this.btnCutTextFromFml.Click += new System.EventHandler(this.btnCutTextFromFml_Click);
            // 
            // btnCopyTextFromFml
            // 
            this.btnCopyTextFromFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopyTextFromFml.Image = ((System.Drawing.Image)(resources.GetObject("btnCopyTextFromFml.Image")));
            this.btnCopyTextFromFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopyTextFromFml.Name = "btnCopyTextFromFml";
            this.btnCopyTextFromFml.Size = new System.Drawing.Size(23, 22);
            this.btnCopyTextFromFml.Text = "رونوشت";
            this.btnCopyTextFromFml.Click += new System.EventHandler(this.btnCopyTextFromFml_Click);
            // 
            // btnPasteTextToFml
            // 
            this.btnPasteTextToFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPasteTextToFml.Image = ((System.Drawing.Image)(resources.GetObject("btnPasteTextToFml.Image")));
            this.btnPasteTextToFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPasteTextToFml.Name = "btnPasteTextToFml";
            this.btnPasteTextToFml.Size = new System.Drawing.Size(23, 22);
            this.btnPasteTextToFml.Text = "چسباندن";
            this.btnPasteTextToFml.Click += new System.EventHandler(this.btnPasteTextToFml_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::MohanirPouya.Properties.Resources.Redo;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "بازگشت";
            this.btnUndo.ToolTipText = "باز گرداندن تغییرات به حالت قبلی";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnRedo
            // 
            this.btnRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRedo.Image = global::MohanirPouya.Properties.Resources.Undo;
            this.btnRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(23, 22);
            this.btnRedo.Text = "پیش روی";
            this.btnRedo.ToolTipText = "باز گرداندن تغییرات به حالت بعدی";
            this.btnRedo.Click += new System.EventHandler(this.btnRedo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // lblOperators
            // 
            this.lblOperators.ForeColor = System.Drawing.Color.Red;
            this.lblOperators.Name = "lblOperators";
            this.lblOperators.Size = new System.Drawing.Size(56, 22);
            this.lblOperators.Text = "عملگر ها:";
            // 
            // btnOpSum
            // 
            this.btnOpSum.Name = "btnOpSum";
            this.btnOpSum.Size = new System.Drawing.Size(27, 25);
            this.btnOpSum.Text = "+";
            this.btnOpSum.ToolTipText = "جمع";
            this.btnOpSum.Click += new System.EventHandler(this.btnOpSum_Click);
            // 
            // btnOpSubstract
            // 
            this.btnOpSubstract.Name = "btnOpSubstract";
            this.btnOpSubstract.Size = new System.Drawing.Size(23, 25);
            this.btnOpSubstract.Text = "-";
            this.btnOpSubstract.ToolTipText = "تفریق";
            this.btnOpSubstract.Click += new System.EventHandler(this.btnOpSubstract_Click);
            // 
            // btnOpMultiplation
            // 
            this.btnOpMultiplation.Name = "btnOpMultiplation";
            this.btnOpMultiplation.Size = new System.Drawing.Size(26, 25);
            this.btnOpMultiplation.Text = "*";
            this.btnOpMultiplation.ToolTipText = "ضرب";
            this.btnOpMultiplation.Click += new System.EventHandler(this.btnOpMultiplation_Click);
            // 
            // btnOpDevide
            // 
            this.btnOpDevide.Name = "btnOpDevide";
            this.btnOpDevide.Size = new System.Drawing.Size(24, 25);
            this.btnOpDevide.Text = "/";
            this.btnOpDevide.ToolTipText = "تقسیم";
            this.btnOpDevide.Click += new System.EventHandler(this.btnOpDevide_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnFunctions
            // 
            this.btnFunctions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMin,
            this.btnMax,
            this.btnFkMax,
            this.btnFkMin,
            this.toolStripSeparator4,
            this.btnPower,
            this.btnSqrt,
            this.btnAbs,
            this.toolStripSeparator6,
            this.btnSum1,
            this.btnSum2,
            this.btnSum3,
            this.btnSum4,
            this.btnSum5,
            this.btnSum6,
            this.btnSum7});
            this.btnFunctions.ForeColor = System.Drawing.Color.Purple;
            this.btnFunctions.Image = ((System.Drawing.Image)(resources.GetObject("btnFunctions.Image")));
            this.btnFunctions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFunctions.Name = "btnFunctions";
            this.btnFunctions.Size = new System.Drawing.Size(41, 22);
            this.btnFunctions.Text = "توابع";
            // 
            // btnFkMax
            // 
            this.btnFkMax.Name = "btnFkMax";
            this.btnFkMax.Size = new System.Drawing.Size(193, 22);
            this.btnFkMax.Text = "ماكسیمم دو ضابطه ای";
            this.btnFkMax.Click += new System.EventHandler(this.btnFkMax_Click);
            // 
            // btnFkMin
            // 
            this.btnFkMin.Name = "btnFkMin";
            this.btnFkMin.ShowShortcutKeys = false;
            this.btnFkMin.Size = new System.Drawing.Size(193, 22);
            this.btnFkMin.Text = "مینیمم دو ضابطه ای";
            this.btnFkMin.Click += new System.EventHandler(this.btnFkMin_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(175, 6);
            // 
            // btnPower
            // 
            this.btnPower.Name = "btnPower";
            this.btnPower.Size = new System.Drawing.Size(178, 22);
            this.btnPower.Text = "توان";
            this.btnPower.Click += new System.EventHandler(this.btnPower_Click);
            // 
            // btnSqrt
            // 
            this.btnSqrt.Name = "btnSqrt";
            this.btnSqrt.Size = new System.Drawing.Size(178, 22);
            this.btnSqrt.Text = "جذر";
            this.btnSqrt.Click += new System.EventHandler(this.btnSqrt_Click);
            // 
            // btnAbs
            // 
            this.btnAbs.Name = "btnAbs";
            this.btnAbs.Size = new System.Drawing.Size(178, 22);
            this.btnAbs.Text = "قدر مطلق";
            this.btnAbs.ToolTipText = "تابع قدر مطلق ، این تابع یك مقدار را دریافت كرده و قدر مطلق آن را بر می گرداند";
            this.btnAbs.Click += new System.EventHandler(this.btnAbs_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(175, 6);
            // 
            // btnSum1
            // 
            this.btnSum1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum1.Name = "btnSum1";
            this.btnSum1.Size = new System.Drawing.Size(178, 22);
            this.btnSum1.Text = "مجموع نوع واحد ها";
            this.btnSum1.Click += new System.EventHandler(this.btnSumByUnitType_Click);
            // 
            // btnSum2
            // 
            this.btnSum2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum2.Name = "btnSum2";
            this.btnSum2.Size = new System.Drawing.Size(178, 22);
            this.btnSum2.Text = "مجموع نیروگاه ها";
            this.btnSum2.Click += new System.EventHandler(this.btnSumByPowerStation_Click);
            // 
            // btnSum3
            // 
            this.btnSum3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum3.Name = "btnSum3";
            this.btnSum3.Size = new System.Drawing.Size(178, 22);
            this.btnSum3.Text = "مجموع شركت ها";
            this.btnSum3.Click += new System.EventHandler(this.btnSumByCompany_Click);
            // 
            // btnSum4
            // 
            this.btnSum4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum4.Name = "btnSum4";
            this.btnSum4.Size = new System.Drawing.Size(178, 22);
            this.btnSum4.Text = "مجموع نوع شركت";
            this.btnSum4.Click += new System.EventHandler(this.btnSumByCompanyType_Click);
            // 
            // btnSum5
            // 
            this.btnSum5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum5.Name = "btnSum5";
            this.btnSum5.Size = new System.Drawing.Size(178, 22);
            this.btnSum5.Text = "مجموع در منطقه";
            this.btnSum5.Click += new System.EventHandler(this.btnSumByRegion_Click);
            // 
            // btnSum6
            // 
            this.btnSum6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum6.Name = "btnSum6";
            this.btnSum6.Size = new System.Drawing.Size(178, 22);
            this.btnSum6.Text = "مجموع روزانه";
            this.btnSum6.Click += new System.EventHandler(this.btnSumByDay_Click);
            // 
            // btnSum7
            // 
            this.btnSum7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSum7.Name = "btnSum7";
            this.btnSum7.Size = new System.Drawing.Size(178, 22);
            this.btnSum7.Text = "مجموع ماهیانه";
            this.btnSum7.Click += new System.EventHandler(this.btnSumByMonth_Click);
            // 
            // lblParamsList
            // 
            this.lblParamsList.AutoSize = true;
            this.lblParamsList.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblParamsList.Location = new System.Drawing.Point(7, 8);
            this.lblParamsList.Name = "lblParamsList";
            this.lblParamsList.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamsList.Size = new System.Drawing.Size(56, 14);
            this.lblParamsList.TabIndex = 2;
            this.lblParamsList.Text = "پارامتر ها:";
            // 
            // lstParameters
            // 
            this.lstParameters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lstParameters.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstParameters.FormattingEnabled = true;
            this.lstParameters.IntegralHeight = false;
            this.lstParameters.ItemHeight = 14;
            this.lstParameters.Location = new System.Drawing.Point(7, 28);
            this.lstParameters.Name = "lstParameters";
            this.lstParameters.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lstParameters.Size = new System.Drawing.Size(204, 232);
            this.lstParameters.Sorted = true;
            this.lstParameters.TabIndex = 0;
            this.lstParameters.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstParameters_DoubleClick);
            this.lstParameters.SelectedIndexChanged += new System.EventHandler(this.lstParameters_SelectedIndexChanged);
            // 
            // lblParamName
            // 
            this.lblParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamName.AutoSize = true;
            this.lblParamName.Location = new System.Drawing.Point(762, 74);
            this.lblParamName.Name = "lblParamName";
            this.lblParamName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamName.Size = new System.Drawing.Size(55, 13);
            this.lblParamName.TabIndex = 11;
            this.lblParamName.Text = "نام پارامتر:";
            // 
            // lblParamDescribe
            // 
            this.lblParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamDescribe.AutoSize = true;
            this.lblParamDescribe.Location = new System.Drawing.Point(444, 76);
            this.lblParamDescribe.Name = "lblParamDescribe";
            this.lblParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamDescribe.Size = new System.Drawing.Size(51, 13);
            this.lblParamDescribe.TabIndex = 17;
            this.lblParamDescribe.Text = "توضیحات:";
            // 
            // txtParamDescribe
            // 
            this.txtParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtParamDescribe.Border.Class = "TextBoxBorder";
            this.txtParamDescribe.Location = new System.Drawing.Point(12, 91);
            this.txtParamDescribe.MaxLength = 100;
            this.txtParamDescribe.Name = "txtParamDescribe";
            this.txtParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtParamDescribe.Size = new System.Drawing.Size(480, 21);
            this.txtParamDescribe.TabIndex = 16;
            // 
            // MyToolTip
            // 
            this.MyToolTip.AutoPopDelay = 10000;
            this.MyToolTip.InitialDelay = 500;
            this.MyToolTip.IsBalloon = true;
            this.MyToolTip.ReshowDelay = 100;
            this.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MyToolTip.ToolTipTitle = "راهنمایی";
            // 
            // lblCurrentParamName
            // 
            this.lblCurrentParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentParamName.BackColor = System.Drawing.Color.White;
            this.lblCurrentParamName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCurrentParamName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblCurrentParamName.Location = new System.Drawing.Point(604, 91);
            this.lblCurrentParamName.Name = "lblCurrentParamName";
            this.lblCurrentParamName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCurrentParamName.Size = new System.Drawing.Size(213, 21);
            this.lblCurrentParamName.TabIndex = 11;
            this.lblCurrentParamName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ParametersLog
            // 
            this.ParametersLog.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.CCreationDate,
            this.CStartDate,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.ParametersLog.TableName = "ParametersLog";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "ردیف";
            this.dataColumn1.ColumnName = "Revision";
            this.dataColumn1.DataType = typeof(int);
            // 
            // CCreationDate
            // 
            this.CCreationDate.Caption = "تاریخ ایجاد";
            this.CCreationDate.ColumnName = "CreationDate";
            this.CCreationDate.DataType = typeof(System.DateTime);
            this.CCreationDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // CStartDate
            // 
            this.CStartDate.Caption = "تاریخ آغاز اعتبار";
            this.CStartDate.ColumnName = "StartDate";
            this.CStartDate.DataType = typeof(System.DateTime);
            this.CStartDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "تاریخ اتمام اعتبار";
            this.dataColumn4.ColumnName = "ExpirationDate";
            this.dataColumn4.DataType = typeof(System.DateTime);
            this.dataColumn4.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "متن كد پارامتر";
            this.dataColumn5.ColumnName = "SelectString";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "نام كاربر";
            this.dataColumn6.ColumnName = "UserID";
            // 
            // btnValidation
            // 
            this.btnValidation.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnValidation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnValidation.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnValidation.Location = new System.Drawing.Point(135, 617);
            this.btnValidation.Name = "btnValidation";
            this.btnValidation.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnValidation.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5);
            this.btnValidation.Size = new System.Drawing.Size(123, 28);
            this.btnValidation.TabIndex = 24;
            this.btnValidation.Text = "بررسی دستورات <b><font color=\"#ED1C24\">(F5)</font></b>";
            this.btnValidation.Tooltip = "بررسی دستورات وارد شده";
            this.btnValidation.Click += new System.EventHandler(this.btnValidation_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(697, 617);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(117, 28);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Tooltip = "انصرف از طراحی پارامتر و خروج از فرم";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveAndClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveAndClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSaveAndClose.Location = new System.Drawing.Point(12, 617);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSaveAndClose.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F8);
            this.btnSaveAndClose.Size = new System.Drawing.Size(117, 28);
            this.btnSaveAndClose.TabIndex = 26;
            this.btnSaveAndClose.Text = "ذخیره و تایید <b><font color=\"#ED1C24\">(F8)</font></b>";
            this.btnSaveAndClose.Tooltip = "ذخیره نمودن پارامتر و بست فرم ایجاد پارامتر";
            this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
            // 
            // dgvLog
            // 
            this.dgvLog.AllowUserToAddRows = false;
            this.dgvLog.AllowUserToDeleteRows = false;
            this.dgvLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLog.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.dgvLog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLog.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column3,
            this.Column7,
            this.colmun8,
            this.colmun5,
            this.Column4});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLog.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvLog.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLog.Location = new System.Drawing.Point(12, 470);
            this.dgvLog.Name = "dgvLog";
            this.dgvLog.ReadOnly = true;
            this.dgvLog.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvLog.RowHeadersVisible = false;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.NullValue = "-";
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvLog.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvLog.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Transparent;
            this.dgvLog.RowTemplate.Height = 24;
            this.dgvLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLog.Size = new System.Drawing.Size(803, 141);
            this.dgvLog.TabIndex = 27;
            this.dgvLog.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvLog_CellMouseClick);
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Revision";
            this.Column2.HeaderText = "ردیف";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CreationDate";
            this.Column3.HeaderText = "تاریخ تولید پارامتر";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "UserID";
            this.Column7.HeaderText = "کاربر تعریف کننده";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // colmun8
            // 
            this.colmun8.DataPropertyName = "StartDate";
            this.colmun8.HeaderText = "تاریخ آغاز اعتبار";
            this.colmun8.Name = "colmun8";
            this.colmun8.ReadOnly = true;
            // 
            // colmun5
            // 
            this.colmun5.DataPropertyName = "EndDate";
            this.colmun5.HeaderText = "تاریخ پایان آعتبار";
            this.colmun5.Name = "colmun5";
            this.colmun5.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Active";
            this.Column4.HeaderText = "غیرفعال";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // revisionDataGridViewTextBoxColumn1
            // 
            this.revisionDataGridViewTextBoxColumn1.DataPropertyName = "Revision";
            this.revisionDataGridViewTextBoxColumn1.HeaderText = "ردیف";
            this.revisionDataGridViewTextBoxColumn1.Name = "revisionDataGridViewTextBoxColumn1";
            this.revisionDataGridViewTextBoxColumn1.ReadOnly = true;
            this.revisionDataGridViewTextBoxColumn1.Width = 50;
            // 
            // userIDDataGridViewTextBoxColumn1
            // 
            this.userIDDataGridViewTextBoxColumn1.DataPropertyName = "UserID";
            this.userIDDataGridViewTextBoxColumn1.HeaderText = "كاربر ثبت كننده";
            this.userIDDataGridViewTextBoxColumn1.Name = "userIDDataGridViewTextBoxColumn1";
            this.userIDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.userIDDataGridViewTextBoxColumn1.Width = 120;
            // 
            // creationDateDataGridViewTextBoxColumn1
            // 
            this.creationDateDataGridViewTextBoxColumn1.DataPropertyName = "CreationDate";
            this.creationDateDataGridViewTextBoxColumn1.HeaderText = "تاریخ ثبت";
            this.creationDateDataGridViewTextBoxColumn1.Name = "creationDateDataGridViewTextBoxColumn1";
            this.creationDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.creationDateDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.creationDateDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.creationDateDataGridViewTextBoxColumn1.Width = 140;
            // 
            // startDateDataGridViewTextBoxColumn1
            // 
            this.startDateDataGridViewTextBoxColumn1.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn1.HeaderText = "آغاز اعتبار";
            this.startDateDataGridViewTextBoxColumn1.Name = "startDateDataGridViewTextBoxColumn1";
            this.startDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.startDateDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.startDateDataGridViewTextBoxColumn1.Width = 140;
            // 
            // expirationDateDataGridViewTextBoxColumn1
            // 
            this.expirationDateDataGridViewTextBoxColumn1.DataPropertyName = "ExpirationDate";
            this.expirationDateDataGridViewTextBoxColumn1.HeaderText = "پایان اعتبار";
            this.expirationDateDataGridViewTextBoxColumn1.Name = "expirationDateDataGridViewTextBoxColumn1";
            this.expirationDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.expirationDateDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.expirationDateDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.expirationDateDataGridViewTextBoxColumn1.Width = 140;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ساختار";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Text = "مشاهده";
            this.Column1.ToolTipText = "مشاهده ساختار تعریف شده برای تاریخ ذكر شده";
            this.Column1.UseColumnTextForButtonValue = true;
            this.Column1.Width = 90;
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.btnDeActive);
            this.MainPanel.Controls.Add(this.PCreationDate);
            this.MainPanel.Controls.Add(this.lblBeginDate);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Controls.Add(this.lblParamName);
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.dgvLog);
            this.MainPanel.Controls.Add(this.btnValidation);
            this.MainPanel.Controls.Add(this.ParameterPanel);
            this.MainPanel.Controls.Add(this.btnSaveAndClose);
            this.MainPanel.Controls.Add(this.lblCurrentParamName);
            this.MainPanel.Controls.Add(this.txtParamDescribe);
            this.MainPanel.Controls.Add(this.lblParamDescribe);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(827, 657);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 28;
            // 
            // btnDeActive
            // 
            this.btnDeActive.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeActive.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnDeActive.Location = new System.Drawing.Point(264, 617);
            this.btnDeActive.Name = "btnDeActive";
            this.btnDeActive.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDeActive.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnDeActive.Size = new System.Drawing.Size(122, 28);
            this.btnDeActive.TabIndex = 66;
            this.btnDeActive.Text = "غیرفعال<b><font color=\"#ED1C24\">(F3)</font></b>";
            this.btnDeActive.Tooltip = "غیرفعال نمودن ویرایش انتخاب شده";
            this.btnDeActive.Click += new System.EventHandler(this.btnDeActive_Click);
            // 
            // PCreationDate
            // 
            this.PCreationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PCreationDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.PCreationDate.IsNull = false;
            this.PCreationDate.Location = new System.Drawing.Point(498, 91);
            this.PCreationDate.Name = "PCreationDate";
            this.PCreationDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PCreationDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.PCreationDate.Size = new System.Drawing.Size(100, 20);
            this.PCreationDate.TabIndex = 65;
            this.PCreationDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBeginDate.AutoSize = true;
            this.lblBeginDate.Location = new System.Drawing.Point(521, 74);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBeginDate.Size = new System.Drawing.Size(80, 13);
            this.lblBeginDate.TabIndex = 64;
            this.lblBeginDate.Text = "تاریخ آغاز اعتبار:";
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.lblSubtitle);
            this.TopPanel.Controls.Add(this.lblTitle);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(827, 69);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.TopPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 43;
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubtitle.Font = new System.Drawing.Font("B Yekan", 14.25F);
            this.lblSubtitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblSubtitle.Location = new System.Drawing.Point(3, 36);
            this.lblSubtitle.Name = "lblSubtitle";
            this.lblSubtitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSubtitle.Size = new System.Drawing.Size(822, 29);
            this.lblSubtitle.TabIndex = 29;
            this.lblSubtitle.Text = "ویرایش پارامتر";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(821, 37);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "خدمات انتقال - خط";
            // 
            // btnMin
            // 
            this.btnMin.Name = "btnMin";
            this.btnMin.Size = new System.Drawing.Size(193, 22);
            this.btnMin.Text = "مینیمم تک ضابطه ای";
            this.btnMin.Click += new System.EventHandler(this.btnMin_Click);
            // 
            // btnMax
            // 
            this.btnMax.Name = "btnMax";
            this.btnMax.Size = new System.Drawing.Size(193, 22);
            this.btnMax.Text = "ماکسیمم تک ضابطه ای";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // frmModifyParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(827, 657);
            this.Controls.Add(this.MainPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MinimizeBox = false;
            this.Name = "frmModifyParameters";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم ویرایش پارامتر";
            this.Load += new System.EventHandler(this.Form_Load);
            this.ParameterPanel.ResumeLayout(false);
            this.ParameterPanel.PerformLayout();
            this.PanelDescribe.ResumeLayout(false);
            this.PanelCodeCompiler.ResumeLayout(false);
            this.ToolStripFunctions.ResumeLayout(false);
            this.ToolStripFunctions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLog)).EndInit();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx ParameterPanel;
        private DevComponents.DotNetBar.Controls.GroupPanel PanelDescribe;
        private Label lblDescription;
        private Panel PanelCodeCompiler;
        private MohanirCompilerRichTextBox rtbCode;
        private ToolStrip ToolStripFunctions;
        private ToolStripButton btnNew;
        private ToolStripButton btnOpenFml;
        private System.Windows.Forms.ToolStripButton btnSaveFml;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnCutTextFromFml;
        private System.Windows.Forms.ToolStripButton btnCopyTextFromFml;
        private System.Windows.Forms.ToolStripButton btnPasteTextToFml;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripButton btnRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel lblOperators;
        private System.Windows.Forms.ToolStripMenuItem btnOpSum;
        private System.Windows.Forms.ToolStripMenuItem btnOpSubstract;
        private System.Windows.Forms.ToolStripMenuItem btnOpMultiplation;
        private System.Windows.Forms.ToolStripMenuItem btnOpDevide;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripDropDownButton btnFunctions;
        private System.Windows.Forms.ToolStripMenuItem btnFkMax;
        private System.Windows.Forms.ToolStripMenuItem btnFkMin;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem btnSqrt;
        private System.Windows.Forms.ToolStripMenuItem btnAbs;
        private System.Windows.Forms.ToolStripMenuItem btnPower;
        private Label lblParamsList;
        private System.Windows.Forms.ListBox lstParameters;
        private System.Windows.Forms.Label lblParamName;
        private System.Windows.Forms.Label lblParamDescribe;
        private DevComponents.DotNetBar.Controls.TextBoxX txtParamDescribe;
        private System.Windows.Forms.ToolTip MyToolTip;
        private System.Windows.Forms.Label lblCurrentParamName;
        private System.Data.DataTable ParametersLog;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn CCreationDate;
        private System.Data.DataColumn CStartDate;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private DevComponents.DotNetBar.ButtonX btnValidation;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSaveAndClose;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn revisionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn userIDDataGridViewTextBoxColumn1;
        private DataGridViewColumn creationDateDataGridViewTextBoxColumn1;
        private DataGridViewColumn startDateDataGridViewTextBoxColumn1;
        private DataGridViewColumn expirationDateDataGridViewTextBoxColumn1;
        private DataGridViewButtonColumn Column1;
        private ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem btnSum1;
        private System.Windows.Forms.ToolStripMenuItem btnSum2;
        private System.Windows.Forms.ToolStripMenuItem btnSum3;
        private System.Windows.Forms.ToolStripMenuItem btnSum4;
        private System.Windows.Forms.ToolStripMenuItem btnSum5;
        private System.Windows.Forms.ToolStripMenuItem btnSum6;
        private ToolStripMenuItem btnSum7;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private Label lblSubtitle;
        private Label lblTitle;
        public RPNCalendar.UI.Controls.PersianDatePicker PCreationDate;
        private Label lblBeginDate;
        private DevComponents.DotNetBar.ButtonX btnDeActive;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column7;
        private DataGridViewTextBoxColumn colmun8;
        private DataGridViewTextBoxColumn colmun5;
        private DataGridViewCheckBoxColumn Column4;
        private ToolStripMenuItem btnMin;
        private ToolStripMenuItem btnMax;
    }
}