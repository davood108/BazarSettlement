﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    /// <summary>
    /// فرم ویرایش پارامتر های ثبت شده
    /// </summary>
    public partial class frmModifyParameters : Form
    {

        #region Fields

        #region readonly String _CurrentParamName
        /// <summary>
        /// نام پارامتری كه در حال ویرایش است
        /// </summary>
        private readonly String _CurrentParamName;
        #endregion

        #region Int32 _CurrentParamRevision
        /// <summary>
        /// شماره آخرین پارامتر ثبت شده
        /// </summary>
        private Int32 _CurrentParamRevision;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش تعیین شده
        /// </summary>
        private readonly String _PartName;
        #endregion

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر ها
        /// </summary>
        private DataTable _DataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _ArrayListParams;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر
        /// </summary>
        private readonly String _ParamType;

        private int step;
        private string TestingQuery;

#pragma warning disable 649
        private readonly string _UserID;
        private string Cdate;
#pragma warning restore 649

        #endregion

        private DataTable dataLogtable;
        private readonly DbSMDataContext _DbSM;
        private string CommandText;
        private SqlConnection MySqlConnection;
        private SqlCommand MySqlCommand;
        private bool TestState;
        private int _StartDateCount;

        #endregion

        #region Constructor
        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="ParamName">نام پارامتری كه در حال ویرایش است</param>
        /// <param name="PartName">نام بخش در حال ویرایش</param>
        /// <param name="ParamType">نوع پارامتر</param>
        public frmModifyParameters(String ParamName, String PartName , String ParamType)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                            (DbBizClass.dbConnStr);

            _UserID = DbBizClass.CurrentUserLastName;

            #region Set Form Title
            if (PartName == "Sellers")
                lblTitle.Text = "فروشندگان";
            else if (PartName == "Buyers")
                lblTitle.Text = "خریداران";
            else if (PartName == "LineTransfer")
                lblTitle.Text = "خدمات انتقال";
            #endregion

            _CurrentParamName = ParamName;
            _PartName = PartName;
            _ParamType = ParamType;

            #region SetTime
            PersianDate OccuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            PCreationDate.Text = OccuredDate.ToString();
            #endregion

            ShowDialog();
        }
        #endregion

        #region Events Handlers

        #region Form Load

        private void Form_Load(object sender, EventArgs e)
        {
            #region Set RiboonFunction
            switch (_PartName)
            {
                case "Sellers":

                    break;
                case "Buyers":

                    btnSum1.Text = "مجموع بر اساس ساعت";
                    btnSum2.Text = "مجموع بر اساس تاریخ";
                    btnSum3.Text = "مجموع بر اساس نوع شرکت";
                    btnSum4.Visible = false;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    break;
                case "Transfer":
                    btnSum1.Text = "مجموع بر اساس سطح ولتاژ";
                    btnSum2.Text = "مجموع بر اساس فوق توزیع و انتقال";
                    btnSum3.Text = "مجموع بر اساس ماه";
                    btnSum4.Visible = false;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    break;
            }
            #endregion 

            FillParametersTable();
            SetParamLastDataByName("F",10000);

            #region Prepare lstParameters
            lstParameters.DataSource = _ArrayListParams;
            lstParameters.SelectedIndex = 0;
            #endregion

            SetRtbCodeMembers();
            FillLogDataGrid();
        }

        #endregion

        #region lstParameters Event Handlers

        #region DoubleClick

        private void lstParameters_DoubleClick(object sender, MouseEventArgs e)
        {
            rtbCode.SelectedText = lstParameters.SelectedItem.ToString();
            rtbCode.Focus();
        }

        #endregion

        #region SelectedIndexChanged

        /// <summary>
        /// اين رويه مقادير توضيحات هر پارامتر را نمايش مي دهد
        /// </summary>
        private void lstParameters_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDescription.Text = Classes.Parameters.GetParamDescription
                (lstParameters.SelectedItem.ToString(), _DataTableParams);
        }

        #endregion

        #endregion
        
        #region Toolbar Buttons

        #region btnNew_Click

        private void btnNew_Click(object sender, EventArgs e)
        {
            rtbCode.Clear();
            rtbCode.Focus();
        }

        #endregion

        #region btnOpenFml_Click

        private void btnOpenFml_Click(object sender, EventArgs e)
        {
            var MyOpenFileDialog = new OpenFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = "txt Files|*.txt|All Files|*.*",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           Multiselect = false,
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           ShowReadOnly = false,
                                           ReadOnlyChecked = false,
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = "خواندن فرمول از فایل"
                                       };

            // Determine whether the user selected a file from the OpenFileDialog.
            if (MyOpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                MyOpenFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.LoadFile(MyOpenFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnSaveFml_Click

        private void btnSaveFml_Click(object sender, EventArgs e)
        {
            var MySaveFileDialog = new SaveFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = "txt Files|*.txt",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = "ذخیره فرمول در فایل"
                                       };
            MySaveFileDialog.ShowDialog();

            // Determine whether the user selected a file from the SaveFileDialog.
            if (MySaveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                MySaveFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.SaveFile(MySaveFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnCutTextFromFml_Click

        private void btnCutTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Cut();
            rtbCode.Focus();
        }

        #endregion

        #region btnCopyTextFromFml_Click

        private void btnCopyTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Copy();
            rtbCode.Focus();
        }

        #endregion

        #region btnPasteTextToFml_Click

        private void btnPasteTextToFml_Click(object sender, EventArgs e)
        {
            rtbCode.Paste();
            rtbCode.Focus();
        }

        #endregion

        #region btnUndo_Click

        private void btnUndo_Click(object sender, EventArgs e)
        {
            rtbCode.Undo();
            rtbCode.Focus();
        }

        #endregion

        #region btnRedo_Click

        private void btnRedo_Click(object sender, EventArgs e)
        {
            rtbCode.Redo();
            rtbCode.Focus();
        }

        #endregion

        #region btnFkMax_Click

        private void btnFkMax_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Max( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnFkMin_Click

        private void btnFkMin_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Min( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnMin_Click

        private void btnMin_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Min() ";
            rtbCode.Focus();
        }


        #endregion

        #region btnMax_Click


        private void btnMax_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Max() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSum_Click

        private void btnOpSum_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "+ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSubstract_Click

        private void btnOpSubstract_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "- ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpMultiplation_Click

        private void btnOpMultiplation_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "* ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpDevide_Click

        private void btnOpDevide_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "/ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSqrt_Click

        private void btnSqrt_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sqrt() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnAbs_Click

        private void btnAbs_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Abs() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnPower_Click

        private void btnPower_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Power( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region Sum Buttons

        #region btnSumByUnitType_Click (Sum1)

        private void btnSumByUnitType_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum1( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByPowerStation_Click (Sum2)

        private void btnSumByPowerStation_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum2( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompany_Click (Sum3)

        private void btnSumByCompany_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum3( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompanyType_Click (Sum4)

        private void btnSumByCompanyType_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum4( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByRegion_Click (Sum5)

        private void btnSumByRegion_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum5( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByDay_Click (Sum6)

        private void btnSumByDay_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum6( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByMonth_Click (Sum7)

        private void btnSumByMonth_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum7( ) ";
            rtbCode.Focus();
        }

        #endregion

        #endregion

        #endregion

        #region dgvLog_CellMouseClick

        /// <summary>
        /// روال مدیریت كلیك بر روی جدول جزئیات
        /// </summary>
        private void dgvLog_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {


            SetParamLastDataByName("E", Convert.ToInt16(dgvLog.Rows[e.RowIndex].Cells[0].Value));
          
        //    return true;
        }

        #endregion

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void btnValidation_Click(object sender, EventArgs e)
        {
          

            #region Step Type

            string fg = rtbCode.Text;
            if (fg.Length > 14)
            {
                if (fg.Substring(0, 14) == "Functions.Step")
                {

                    step = 1;
                }
                else step = fg.Substring(0, 13) == "Functions.Ava" ? 2 : 0;

            }
            #endregion
      
            #region Power-Step
            if (step == 1)
            {
               string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                string h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw1),0)* [Sellers].[PwPrP].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    h = h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
        

               TestingQuery = Classes.ManageParameters.
                              GetParamString(_PartName, _CurrentParamName, h, _DataTableParams,_ArrayListParams, true, 0, step, rtbCode.Text);
           if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
            }
            #endregion

            #region Ava - step 2
            if (step == 2)
            {

                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));

                string h11 = h1[0].Substring(14);
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                string h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + lblCurrentParamName.Text + "'";

                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, lblCurrentParamName.Text,
                                                                                                                 h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);
                if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
                {
                    return;
                }
            }
            #endregion

            #region Non-Step
            if (step != 1 && step != 2)
            {
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, lblCurrentParamName.Text, rtbCode.Text, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);
                if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
            }
            #endregion
        }
        #endregion

        #region btnSaveAndClose_Click

        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {

            #region Check First Day of Month choose
            //if ((PCreationDate.Text.Substring(8, 2)) != "01")
            //{
            //    PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
            //               MessageBoxButtons.OK, MessageBoxIcon.Error,
            //               MessageBoxDefaultButton.Button1);
            //    return;
         //   }
            #endregion

            #region Check Same Date in DataBase
            #region Prepare SqlCommand

            CommandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _PartName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName and StartDate=@StartDate and Active=0";
            MySqlConnection =
               new SqlConnection(DbBizClass.dbConnStr);
            MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;
            MySqlCommand.Parameters.Add("@StartDate", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@StartDate"].Value = PCreationDate.Text;
            #endregion

            #region Execute SqlCommand

            try
            {
                MySqlCommand.Connection.Open();
                SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                if (MySqlDataReader != null)
                {
                    MySqlDataReader.Read();
                    _StartDateCount = Convert.ToInt32(MySqlDataReader[0].ToString());



                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion

            if(_StartDateCount>0)
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار تکراری می باشد.باید ابتدا ویرایش موجود غیر فعال شود !", "خطا",
           MessageBoxButtons.OK, MessageBoxIcon.Error,
           MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion

            #region Get Current Param Revision

            #region Prepare SqlCommand

            CommandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _PartName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName";
            MySqlConnection =
               new SqlConnection(DbBizClass.dbConnStr);
            MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

            #endregion

            #region Execute SqlCommand

            try
            {
                MySqlCommand.Connection.Open();
                SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                if (MySqlDataReader != null)
                {
                    MySqlDataReader.Read();
                    _CurrentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());



                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion

            #endregion

            #region Set Current Date
            PersianDate OccuredDate1 = PersianDateConverter.ToPersianDate
(DateTime.Now);
            Cdate = OccuredDate1.ToString();
            #endregion
            
            #region Step Type

            string fg = rtbCode.Text;
            if (fg.Length > 14)
            {
                if (fg.Substring(0, 14) == "Functions.Step")
                {

                    step = 1;
                }
                else step = fg.Substring(0, 13) == "Functions.Ava" ? 2 : 0;

            }
            #endregion

            #region Power-Step

            #region Setrtb Text
            if (step == 1)
            {
                string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                string h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw1),0)* [Sellers].[PwPrP].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    h = h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
            #endregion

            #region Testing
                TestingQuery = Classes.ManageParameters.
                             GetParamString(_PartName, _CurrentParamName, h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);

                if (!String.IsNullOrEmpty(TestingQuery))
                    TestState = CheckTestingQuery(TestingQuery);
                if (TestState == false)
                {
                    PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                           "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                           MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }
                #endregion

            #region Create Table
                if (CheckDescriptionTextBox()==false
               || Classes.ManageParameters.CreateParameterTable(_DataTableParams, _ArrayListParams, lblCurrentParamName.Text, txtParamDescribe.Text, _CurrentParamRevision , h, _UserID, _PartName, _ParamType, step, rtbCode.Text, PCreationDate.Text, Cdate) == false) return;
                #endregion
            
            }
            #endregion

            #region Ava-Step2

            if (step == 2)
            {
                #region rtb Text
                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));

                string h11 = h1[0].Substring(14);
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                string h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + lblCurrentParamName.Text + "'";
                #endregion

                #region Testing
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, lblCurrentParamName.Text,
                                                                                                               h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);

                if (!String.IsNullOrEmpty(TestingQuery))
                    TestState = CheckTestingQuery(TestingQuery);
                if (TestState == false)
                {
                    PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                           "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                           MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }
                #endregion

                #region Create Table
                if (CheckDescriptionTextBox()==false
               || Classes.ManageParameters.CreateParameterTable(_DataTableParams,
                                                              _ArrayListParams, lblCurrentParamName.Text, txtParamDescribe.Text, _CurrentParamRevision , h, _UserID, _PartName, _ParamType, step, rtbCode.Text, PCreationDate.Text, Cdate) == false) return;
                #endregion
            }
            #endregion

            #region Non-Step
            if (step != 1 && step != 2)
            {
                #region Testing
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, lblCurrentParamName.Text, rtbCode.Text, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);
                if (!String.IsNullOrEmpty(TestingQuery))
                    TestState = CheckTestingQuery(TestingQuery);
                if (TestState == false)
                {
                    PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                           "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                           MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }
                #endregion

                #region Create Table
                if (CheckDescriptionTextBox()==false
               ||  Classes.ManageParameters.CreateParameterTable(
                    _DataTableParams, _ArrayListParams, lblCurrentParamName.Text, txtParamDescribe.Text, _CurrentParamRevision, rtbCode.Text, _UserID, _PartName, _ParamType, step, rtbCode.Text, PCreationDate.Text, Cdate) == false) return;
                #endregion
            }
            #endregion


    
            FillLogDataGrid();
        }

        #endregion

        #region btn Deactive
        private void btnDeActive_Click(object sender, EventArgs e)
        {
            if((bool) dgvLog.SelectedRows[0].Cells[5].Value)
            {
               return; 
            }


            DialogResult QResult =
       PersianMessageBox.Show("آيا از اين كار اطمينان داريد؟\n " +
                              "در صورت تایید ، این ویرایش از متغیر دیگر قابل دسترس نخواهد بود!", "هشدار!",
                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                              MessageBoxDefaultButton.Button1);
            if (QResult == DialogResult.No)
            {
                return;
            }


            _DbSM.Param_Deactivation(Convert.ToInt16(dgvLog.SelectedRows[0].Cells[0].Value), _CurrentParamName, _PartName);
            FillLogDataGrid();
        }
        #endregion

        #region btnCancel Click

        /// <summary>
        /// دستگیره مدیریت انصراف از تغییرات
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()

        /// <summary>
        /// بدست آوردن جدول و ليست پارامتر ها از بانك اطلاعات
        /// </summary>
        private void FillParametersTable()
        {
            _DataTableParams = Classes.Parameters.GetAllParamsByPart(_PartName);
            _ArrayListParams =
                Classes.Parameters.GetParamsNameByTable(_DataTableParams);
            _ArrayListParams.Remove(_CurrentParamName);
        }

        #endregion

        #region void SetParamLastDataByName()

        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void SetParamLastDataByName(string param, int Rev)
        {
            lblCurrentParamName.Text = _CurrentParamName;
            if (param == "F")
            {

                #region Select String

                #region Prepare SqlCommand

                 CommandText = "SELECT " +
                                     " SelectString " +
                                     "FROM " + _PartName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName ORDER BY StartDate  DESC";
                MySqlConnection =
                    new SqlConnection(DbBizClass.dbConnStr);
                 MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
                MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    MySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();
                        //_CurrentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());


                        rtbCode.Text = MySqlDataReader[0].ToString();
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    MySqlCommand.Connection.Close();
                }

                #endregion

                #endregion

                #region Get Current Param Revision 

                #region Prepare SqlCommand

               CommandText = "SELECT " +
                                     " Count(ID) " +
                                     "FROM " + _PartName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName";
                 MySqlConnection =
                    new SqlConnection(DbBizClass.dbConnStr);
               MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
                MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    MySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();
                       _CurrentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());


                 
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    MySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            else
            {
                #region Get Current Param Revision , BeginDate , EndDate , Select String

                #region Prepare SqlCommand

              CommandText = "SELECT  " +
                                     " SelectString " +
                                     "FROM " + _PartName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName and Revision=@Rev ORDER BY StartDate  DESC";
             MySqlConnection =
                    new SqlConnection(DbBizClass.dbConnStr);
                MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
                MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;
                MySqlCommand.Parameters.Add("@Rev", SqlDbType.Int);
                MySqlCommand.Parameters["@Rev"].Value = Rev;

                #endregion

                #region Execute SqlCommand

                try
                {
                    MySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();



                        rtbCode.Text = MySqlDataReader[0].ToString();
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    MySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            txtParamDescribe.Text =
                Classes.Parameters.GetParamDescription(_CurrentParamName, _DataTableParams);
        }

        #endregion

        #region void SetRtbCodeMembers()

        /// <summary>
        /// تنظيم عناصر قابل كامپايل در جعبه متن
        /// </summary>
        private void SetRtbCodeMembers()
        {

            #region Set Keywords
            rtbCode.CompilerSettings.Keywords.Add("Max");
            rtbCode.CompilerSettings.Keywords.Add("Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Max");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Step");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Ava");
            rtbCode.CompilerSettings.Keywords.Add("Sqrt");
            rtbCode.CompilerSettings.Keywords.Add("Abs");
            rtbCode.CompilerSettings.Keywords.Add("Power");
            rtbCode.CompilerSettings.Keywords.Add("Sum1");
            rtbCode.CompilerSettings.Keywords.Add("Sum2");
            rtbCode.CompilerSettings.Keywords.Add("Sum3");
            rtbCode.CompilerSettings.Keywords.Add("Sum4");
            rtbCode.CompilerSettings.Keywords.Add("Sum5");
            rtbCode.CompilerSettings.Keywords.Add("Sum6");
            rtbCode.CompilerSettings.Keywords.Add("Sum7");
            #endregion

            #region Set Parameters Keyword
            foreach (String Str in _ArrayListParams)
                rtbCode.CompilerSettings.Keywords.Add(Str);
            #endregion

            #region Set Compiler Settings
            // تنظيم رنگ پارامتر ها:
            rtbCode.CompilerSettings.KeywordColor = Color.Blue ;
            rtbCode.CompilerSettings.IntegerColor = Color.Red;
            // تنظيم خواص كلاس:
            rtbCode.CompilerSettings.EnableStrings = true;
            rtbCode.CompilerSettings.EnableIntegers = true;
            // كامپايل كلمات كليدي:
            rtbCode.CompileKeywords();
            // بررسي خطوط برنامه
            rtbCode.ProcessAllLines();
            #endregion

        }

        #endregion

        #region void FillLogDataGrid()

        /// <summary>
        /// تمكیل آیتم های جدول سابقه تغییرات پارامتر
        /// </summary>
        private void FillLogDataGrid()
        {
            dgvLog.AutoGenerateColumns = false;
            dataLogtable=new DataTable();

            #region Prepare SqlCommand

          CommandText =
                "SELECT Revision  , " +
                "CreationDate,UserID,Startdate,Enddate,'متن پارامتر' as Selectstring,Active   " +
                "FROM " + _PartName + ".ParametersLog " +
                "WHERE EnglishName = @ParamName ORDER BY StartDate  DESC";
            MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
             MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 50);
            MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

            #endregion

            #region Execute SqlCommand

            try
            {
                MySqlCommand.Connection.Open();
// ReSharper disable AssignNullToNotNullAttribute
                dataLogtable.Load(MySqlCommand.ExecuteReader());
                dgvLog.DataSource = dataLogtable;
// ReSharper restore AssignNullToNotNullAttribute
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion
            
            #region Change Deactive revision backcolor
            for (int i = 0; i < dgvLog.Rows.Count; i++)
            {

                if ((bool) dgvLog.Rows[i].Cells[5].Value)
                {
                    dgvLog.Rows[i].DefaultCellStyle.BackColor =Color.Gray;
                 }
            }
            #endregion
        }

        #endregion

        #region Boolean CheckDescriptionTextBox()

        /// <summary>
        /// بررسي جعبه متن توضيحات پارامتر
        /// </summary>
        private Boolean CheckDescriptionTextBox()
        {
            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var ParameterResult = new DataTable();

            var MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
            var MySqlCommand = new SqlCommand(query, MySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                MySqlCommand.Connection.Open();

                // ReSharper disable AssignNullToNotNullAttribute
                ParameterResult.Load(MySqlCommand.ExecuteReader());
                // ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion
            return true;

        }
        #endregion

        #endregion

    }
}