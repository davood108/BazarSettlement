﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    /// <summary>
    /// فرم تعریف پارامتر جدید
    /// </summary>
    public partial class frmNewParameter : Form
    {

        #region Fields

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر ها
        /// </summary>
        private DataTable _DataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _ArrayListParams;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش مورد جستجو
        /// </summary>
        private readonly String _PartName;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر مورد نظر
        /// </summary>
        private readonly String _ParamType;

        private int step;
        private string TestingQuery;

        private readonly string _UserID;
        private string Cdate;
        private bool TestState;

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="PartName">نام بخش از قبیل فروشندگان ، خریداران و غیره</param>
        /// <param name="ParamType">نوع پارامتر از قبیا عادی و غیره</param>
        public frmNewParameter(String PartName, String ParamType)
        {
             InitializeComponent();
            _UserID = DbBizClass.CurrentUserLastName;

            #region Set Form Title
            switch (PartName)
            {
                case "Sellers":
                    lblTitle.Text = "فروشندگان";
                    break;
                case "Buyers":
                    lblTitle.Text = "خریداران";
                    break;
                case "Transfer":
                    lblTitle.Text = "خدمات انتقال";
                    break;
            }
            #endregion

            #region SetTime
            PersianDate OccuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);


            PCreationDate.Text = OccuredDate.ToString();


            #endregion

            _PartName = PartName;
            _ParamType = ParamType;
        }
        #endregion

        #region Events Handlers

        #region Form Load
        private void Form_Load(object sender, EventArgs e)
        {
            #region Set RiboonFunction
            switch (_PartName)
            {
                case "Sellers":
                   
                    break;
                case "Buyers":

                    btnSum1.Text = "مجموع بر اساس ساعت";
                    btnSum2.Text = "مجموع بر اساس تاریخ";
                    btnSum3.Text = "مجموع بر اساس نوع شرکت";
                    btnSum4.Visible = false ;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    break;
                case "Transfer":
                    btnSum1.Text = "مجموع بر اساس سطح ولتاژ";
                    btnSum2.Text = "مجموع بر اساس فوق توزیع و انتقال";
                    btnSum3.Text = "مجموع بر اساس ماه";
                    btnSum4.Visible = false;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    
                    break;
            }
            #endregion 
   
            FillParametersTable();

            #region Prepare lstParams
            lstParameters.DataSource = _ArrayListParams;
            lstParameters.SelectedIndex = 0;
            #endregion
            
            SetRtbCodeMembers();
            SetParamsNameAutoComplete();
        }
        #endregion

        #region lstParameters Event Handlers

        #region lstParameters_DoubleClick

        private void lstParameters_DoubleClick(object sender, MouseEventArgs e)
        {
            rtbCode.SelectedText = lstParameters.SelectedItem.ToString();
            rtbCode.Focus();
        }

        #endregion

        #region lstParameters_SelectedIndexChanged

        /// <summary>
        /// اين رويه مقادير توضيحات هر پارامتر را نمايش مي دهد
        /// </summary>
        private void lstParameters_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDescription.Text = Classes.Parameters.GetParamDescription
                (lstParameters.SelectedItem.ToString(), _DataTableParams);
        }

        #endregion

        #endregion

        #region Toolbar Buttons

        #region btnNew_Click

        private void btnNew_Click(object sender, EventArgs e)
        {
            rtbCode.Clear();
            rtbCode.Focus();
        }

        #endregion

        #region btnOpenFml_Click

        private void btnOpenFml_Click(object sender, EventArgs e)
        {
            var MyOpenFileDialog = new OpenFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = "txt Files|*.txt|All Files|*.*",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           Multiselect = false,
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           ShowReadOnly = false,
                                           ReadOnlyChecked = false,
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = "خواندن فرمول از فایل"
                                       };

            // Determine whether the user selected a file from the OpenFileDialog.
            if (MyOpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                MyOpenFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.LoadFile(MyOpenFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnSaveFml_Click

        private void btnSaveFml_Click(object sender, EventArgs e)
        {
            var MySaveFileDialog = new SaveFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = "txt Files|*.txt",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = "ذخیره فرمول در فایل"
                                       };
            MySaveFileDialog.ShowDialog();

            // Determine whether the user selected a file from the SaveFileDialog.
            if (MySaveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                MySaveFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.SaveFile(MySaveFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnCutTextFromFml_Click

        private void btnCutTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Cut();
            rtbCode.Focus();
        }

        #endregion

        #region btnCopyTextFromFml_Click

        private void btnCopyTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Copy();
            rtbCode.Focus();
        }

        #endregion

        #region btnPasteTextToFml_Click

        private void btnPasteTextToFml_Click(object sender, EventArgs e)
        {
            rtbCode.Paste();
            rtbCode.Focus();
        }

        #endregion

        #region btnUndo_Click

        private void btnUndo_Click(object sender, EventArgs e)
        {
            rtbCode.Undo();
            rtbCode.Focus();
        }

        #endregion

        #region btnRedo_Click

        private void btnRedo_Click(object sender, EventArgs e)
        {
            rtbCode.Redo();
            rtbCode.Focus();
        }

        #endregion

        #region btnFkMax_Click

        private void btnFkMax_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Max( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnMin_Click

        private void btnMin_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Min() ";
            rtbCode.Focus();
        }
        

        #endregion

        #region btnMax_Click


        private void btnMax_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Max() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnFkMin_Click

        private void btnFkMin_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Min( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSum_Click

        private void btnOpSum_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "+ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSubstract_Click

        private void btnOpSubstract_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "- ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpMultiplation_Click

        private void btnOpMultiplation_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "* ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpDevide_Click

        private void btnOpDevide_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "/ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSqrt_Click

        private void btnSqrt_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sqrt() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnAbs_Click

        private void btnAbs_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Abs()";
            rtbCode.Focus();
        }

        #endregion

        #region btnPower_Click

        private void btnPower_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Power( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnStep Click
        private void btnStep_Click(object sender, EventArgs e)
        {
            step = 1;
            rtbCode.SelectedText = "Functions.Step()";
            rtbCode.Focus();
        }
        #endregion 

        #region btnStepEPG_Click
        private void btnStepEPG_Click(object sender, EventArgs e)
        {
            step = 3;
            rtbCode.SelectedText = "Functions.EPG()";
            rtbCode.Focus();
        }
        #endregion

        #region btnAva Click
        private void btnAva_Click(object sender, EventArgs e)
        {
            step = 2;
            rtbCode.SelectedText = "Functions.Ava(,)";
            rtbCode.Focus();
        }
        #endregion 

        #region Sum Buttons

        #region btnSumByUnitType_Click (Sum1)

        private void btnSumByUnitType_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum1()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByPowerStation_Click (Sum2)

        private void btnSumByPowerStation_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum2()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompany_Click (Sum3)

        private void btnSumByCompany_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum3()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompanyType_Click (Sum4)

        private void btnSumByCompanyType_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum4()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByRegion_Click (Sum5)

        private void btnSumByRegion_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum5()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByDay_Click (Sum6)

        private void btnSumByDay_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum6()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByMonth_Click (Sum7)

        private void btnSumByMonth_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum7()";
            rtbCode.Focus();
        }

        #endregion

        #endregion

        #endregion

        #region txtParamName KeyPress

        /// <summary>
        /// روالي براي بررسي عدم ورود كليد فاصله در نام پارامتر
        /// </summary>
        private void txtParamName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(32))
            {
                e.Handled = true;
                return;
            }
        }

        #endregion

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void btnValidation_Click(object sender, EventArgs e)
        {
            if (CheckParamNameTextBox() == false) return;

            #region Step Type

            string fg = rtbCode.Text;
            if (fg.Length > 14)
            {
                if (fg.Substring(0, 14) == "Functions.Step")
                {

                    step = 1;
                }
                else if (fg.Substring(0, 13) == "Functions.Ava")
                {

                    step = 2;
                }
                else if (fg.Substring(0, 13) == "Functions.EPG")
                {

                    step = 3;
                }
                else
                {

                    step = 0;
                }
            }
            #endregion

            #region Power-Step
            if (step == 1)
            {
               string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                string h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw1),0)* [Sellers].[PwPrP].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    h = h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
              

                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text,h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);
                if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
            }
            #endregion
            
            #region Ava - step 2
            if (step== 2)
            {

                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));

                string h11 = h1[0].Substring(14);
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                string h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + txtParamName.Text + "'";

                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text,h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);
                if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
                {
                    return;
                }
            }
            #endregion

            #region Power-StepEPG
            if (step == 3)
            {
            string h1 = rtbCode.Text.Substring(14, rtbCode.TextLength - 15);
                string h = "Functions.Max(Functions.Min(" + h1 + "-RequiredValue,([Sellers].[FL_EPG].Pw1)-RequiredValue),0)* [Sellers].[FL_EPG].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    h = h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[FL_EPG].Pw" + (y - 1) + ",[Sellers].[FL_EPG].Pw" + y + "-[Sellers].[FL_EPG].Pw" + (y - 1) + "),0)* [Sellers].[FL_EPG].Pr" + y + "";
                }


                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text, h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);
                if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
            }
            #endregion

            #region Non-Step
            if (step != 1 && step != 2 && step!=3)
            {
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text,rtbCode.Text, _DataTableParams, _ArrayListParams,true, 0, step, rtbCode.Text);
                if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
            }
            #endregion

        }
        #endregion

        #region btnSaveAndClose_Click
        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {

            #region Set Current Date
            PersianDate OccuredDate1 = PersianDateConverter.ToPersianDate
            (DateTime.Now);
            Cdate = OccuredDate1.ToString();
            #endregion

            #region Step Type

            string fg = rtbCode.Text;
            if (fg.Length > 14)
            {
                if (fg.Substring(0, 14) == "Functions.Step")
                {

                    step = 1;
                }
                else if (fg.Substring(0, 13) == "Functions.Ava")
                {

                    step = 2;
                }
                else if (fg.Substring(0, 13) == "Functions.EPG")
                {

                    step = 3;
                }
                else
                {

                    step = 0;
                }
            }
            #endregion

            #region Power-Step
            if (step == 1)
            {

                #region Set rtbText
                string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                string h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw1),0)* [Sellers].[PwPrP].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    h = h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
                #endregion

                #region testing
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text, h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);

                if (!String.IsNullOrEmpty(TestingQuery))
                   TestState= CheckTestingQuery(TestingQuery);
                if (TestState==false)
                {
                    PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                           "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                           MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }
                #endregion

                #region Create Table

                if (CheckParamNameTextBox() == false ||
                   CheckDescriptionTextBox() == false ||
                    Classes.ManageParameters.CreateParameterTable(_DataTableParams,_ArrayListParams,txtParamName.Text,txtParamDescribe.Text, 0,h, _UserID, _PartName, _ParamType, step,rtbCode.Text, PCreationDate.Text, Cdate) == 
                                                                                          false)
                {
                    return;
                }
                #endregion
            }
            #endregion

            #region Ava-Step2
            if (step==2)
            {
                #region Set rtbText
                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));
           
                string h11 = h1[0].Substring(14);  
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                string h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + txtParamName.Text + "'";

                #endregion

                #region testing
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text,h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);

                if (!String.IsNullOrEmpty(TestingQuery))
                    TestState = CheckTestingQuery(TestingQuery);
                if (TestState == false)
                {
                    PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                           "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                           MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }
                #endregion

                #region Create table

                if (CheckParamNameTextBox() == false ||
                   CheckDescriptionTextBox() == false ||
                    Classes.ManageParameters.CreateParameterTable(_DataTableParams,_ArrayListParams,txtParamName.Text,txtParamDescribe.Text, 0,h, _UserID, _PartName, _ParamType, step,rtbCode.Text, PCreationDate.Text, Cdate) == false)
                {
                    return;
                }
                #endregion
            }
            #endregion

            #region Power-Step EPG
            if (step == 3)
            {
                #region Testing

                string h1 = rtbCode.Text.Substring(14, rtbCode.TextLength - 15);
                string h = "Functions.Max(Functions.Min(" + h1 + "-RequiredValue,([Sellers].[FL_EPG].Pw1)-RequiredValue),0)* [Sellers].[FL_EPG].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    h = h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[FL_EPG].Pw" + (y - 1) + ",[Sellers].[FL_EPG].Pw" + y + "-[Sellers].[FL_EPG].Pw" + (y - 1) + "),0)* [Sellers].[FL_EPG].Pr" + y + "";
                }


                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text, h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text, h, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);

                if (!String.IsNullOrEmpty(TestingQuery))
                    TestState = CheckTestingQuery(TestingQuery);
                if (TestState == false)
                {
                    PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                           "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                           MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }
                #endregion

                #region Create Table

                if (CheckParamNameTextBox() == false ||
                   CheckDescriptionTextBox() == false ||
                    Classes.ManageParameters.CreateParameterTable(_DataTableParams, _ArrayListParams, txtParamName.Text, txtParamDescribe.Text, 0, h, _UserID, _PartName, _ParamType, step, rtbCode.Text, PCreationDate.Text, Cdate) ==
                                                                                          false)
                {
                    return;
                }
                #endregion
            }
            #endregion

            #region Non-Step
            if (step==0)
            {
                #region testing
                TestingQuery = Classes.ManageParameters.GetParamString(_PartName, txtParamName.Text, rtbCode.Text, _DataTableParams, _ArrayListParams, true, 0, step, rtbCode.Text);

                if (!String.IsNullOrEmpty(TestingQuery))
                    TestState = CheckTestingQuery(TestingQuery);
                if (TestState == false)
                {
                    PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                           "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                           MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }
                #endregion

                #region Create Table
                if (CheckParamNameTextBox() == false ||
                   
                    CheckDescriptionTextBox() == false ||
                    Classes.ManageParameters.CreateParameterTable(
                    _DataTableParams,_ArrayListParams,txtParamName.Text,txtParamDescribe.Text, 0,rtbCode.Text, _UserID,_PartName, _ParamType, step,rtbCode.Text,PCreationDate.Text,Cdate) 
                    == false) return;
                #endregion
            }
            #endregion

            Dispose();
        }

        

        #endregion

        #region btnCancel Click

        /// <summary>
        /// روال انصراف از تغییر پارامتر
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()
        /// <summary>
        /// بدست آوردن جدول و ليست پارامتر ها از بانك اطلاعات
        /// </summary>
        private void FillParametersTable()
        {
            _DataTableParams =
                Classes.Parameters.GetAllParamsByPart(_PartName);
            _ArrayListParams =
                Classes.Parameters.GetParamsNameByTable(_DataTableParams);
        }
        #endregion

        #region void SetRtbCodeMembers()
        /// <summary>
        /// تنظيم عناصر قابل كامپايل در جعبه متن
        /// </summary>
        private void SetRtbCodeMembers()
        {
            #region Set Keywords
            rtbCode.CompilerSettings.Keywords.Add("Max");
            rtbCode.CompilerSettings.Keywords.Add("Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Max");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Step");
            rtbCode.CompilerSettings.Keywords.Add("Functions.EPG");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Ava");
            rtbCode.CompilerSettings.Keywords.Add("Sqrt");
            rtbCode.CompilerSettings.Keywords.Add("Abs");
            rtbCode.CompilerSettings.Keywords.Add("Power");
            rtbCode.CompilerSettings.Keywords.Add("Sum1");
            rtbCode.CompilerSettings.Keywords.Add("Sum2");
            rtbCode.CompilerSettings.Keywords.Add("Sum3");
            rtbCode.CompilerSettings.Keywords.Add("Sum4");
            rtbCode.CompilerSettings.Keywords.Add("Sum5");
            rtbCode.CompilerSettings.Keywords.Add("Sum6");
            rtbCode.CompilerSettings.Keywords.Add("Sum7");
            #endregion

            #region Set Parameters Keyword
            foreach (String Str in _ArrayListParams)
                rtbCode.CompilerSettings.Keywords.Add(Str);
            #endregion

            #region Set Compiler Settings
            // تنظيم رنگ پارامتر ها:
            rtbCode.CompilerSettings.KeywordColor = Color.Blue;
            rtbCode.CompilerSettings.IntegerColor = Color.Red;
            // تنظيم خواص كلاس:
            rtbCode.CompilerSettings.EnableStrings = true;
            rtbCode.CompilerSettings.EnableIntegers = true;
            // كامپايل كلمات كليدي:
            rtbCode.CompileKeywords();
            // بررسي خطوط برنامه
            rtbCode.ProcessAllLines();
            #endregion
        }
        #endregion

        #region void SetParamsNameAutoComplete()
        /// <summary>
        /// تنظيم عناصر پارامتر ها براي نام پارامتر
        /// </summary>
        private void SetParamsNameAutoComplete()
        {
            var MyAutoCompleteCustomSource =
                new AutoCompleteStringCollection();
            for (int i = 0; i < _ArrayListParams.Count; i++)
                MyAutoCompleteCustomSource.Add(_ArrayListParams[i].ToString());
            txtParamName.AutoCompleteCustomSource = MyAutoCompleteCustomSource;
        }
        #endregion

        #region Parameter Saving Validations

        #region Boolean CheckParamNameTextBox()
        /// <summary>
        /// بررسي جعبه متن نام پارامتر
        /// </summary>
        private Boolean CheckParamNameTextBox()
        {
            #region Param Name Is Empty
            if (txtParamName.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر نامي به انگليسي وارد نماييد!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamName.Focus();
                return false;
            }
            #endregion

            #region Param Name Lenght Less Than 3 Charecter
            if (txtParamName.Text.Length < 3)
            {
                PersianMessageBox.Show("نام پارامتر بايد حداقل داراي 3 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamName.Focus();
                return false;
            }
            #endregion
            
            return true;
        }
        #endregion

        #region Boolean CheckDescriptionTextBox()
        /// <summary>
        /// بررسي جعبه متن توضيحات پارامتر
        /// </summary>
        private Boolean CheckDescriptionTextBox()
        {
            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            return true;
        }
        #endregion
        
        #endregion

        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var ParameterResult = new DataTable();

            var MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
            var MySqlCommand = new SqlCommand(query, MySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                MySqlCommand.Connection.Open();

// ReSharper disable AssignNullToNotNullAttribute
                ParameterResult.Load(MySqlCommand.ExecuteReader());
// ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion
            return true;

        }
        #endregion

   

        
        #endregion

     

    }
}