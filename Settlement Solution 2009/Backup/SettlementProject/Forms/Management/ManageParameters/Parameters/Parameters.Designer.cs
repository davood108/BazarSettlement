﻿namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    partial class frmParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelParameters = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lstParameters = new System.Windows.Forms.ListBox();
            this.btnAdd = new DevComponents.DotNetBar.ButtonX();
            this.btnEdit = new DevComponents.DotNetBar.ButtonX();
            this.btnClose = new DevComponents.DotNetBar.ButtonX();
            this.PanelDescribe = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnDelete = new DevComponents.DotNetBar.ButtonX();
            this.btnStructure = new DevComponents.DotNetBar.ButtonX();
            this.MyMainPanel.SuspendLayout();
            this.PanelParameters.SuspendLayout();
            this.PanelDescribe.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubtitle.AutoSize = false;
            this.lblSubtitle.Location = new System.Drawing.Point(3, 37);
            this.lblSubtitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSubtitle.Size = new System.Drawing.Size(222, 29);
            this.lblSubtitle.TabIndex = 6;
            this.lblSubtitle.Text = "مدیریت پارامتر ها";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.AutoSize = false;
            this.lblTitle.Location = new System.Drawing.Point(3, 0);
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(222, 37);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "خدمات انتقال - خط";
            // 
            // lblSep
            // 
            this.lblSep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSep.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSep.Size = new System.Drawing.Size(450, 2);
            this.lblSep.TabIndex = 5;
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.btnStructure);
            this.MyMainPanel.Controls.Add(this.btnEdit);
            this.MyMainPanel.Controls.Add(this.btnDelete);
            this.MyMainPanel.Controls.Add(this.PanelDescribe);
            this.MyMainPanel.Controls.Add(this.btnAdd);
            this.MyMainPanel.Controls.Add(this.PanelParameters);
            this.MyMainPanel.Controls.Add(this.btnClose);
            this.MyMainPanel.Size = new System.Drawing.Size(400, 432);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.btnClose, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.PanelParameters, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnAdd, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.PanelDescribe, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnDelete, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnEdit, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnStructure, 0);
            // 
            // PanelParameters
            // 
            this.PanelParameters.BackColor = System.Drawing.Color.Transparent;
            this.PanelParameters.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelParameters.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelParameters.Controls.Add(this.lstParameters);
            this.PanelParameters.DrawTitleBox = false;
            this.PanelParameters.Location = new System.Drawing.Point(199, 82);
            this.PanelParameters.Name = "PanelParameters";
            this.PanelParameters.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PanelParameters.Size = new System.Drawing.Size(188, 309);
            // 
            // 
            // 
            this.PanelParameters.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelParameters.Style.BackColorGradientAngle = 90;
            this.PanelParameters.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelParameters.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelParameters.Style.BorderBottomWidth = 1;
            this.PanelParameters.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelParameters.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelParameters.Style.BorderLeftWidth = 1;
            this.PanelParameters.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelParameters.Style.BorderRightWidth = 1;
            this.PanelParameters.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelParameters.Style.BorderTopWidth = 1;
            this.PanelParameters.Style.CornerDiameter = 4;
            this.PanelParameters.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelParameters.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelParameters.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelParameters.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.PanelParameters.TabIndex = 0;
            this.PanelParameters.Text = "انتخاب ها";
            // 
            // lstParameters
            // 
            this.lstParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstParameters.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstParameters.FormattingEnabled = true;
            this.lstParameters.IntegralHeight = false;
            this.lstParameters.ItemHeight = 14;
            this.lstParameters.Location = new System.Drawing.Point(0, 0);
            this.lstParameters.Name = "lstParameters";
            this.lstParameters.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lstParameters.Size = new System.Drawing.Size(182, 287);
            this.lstParameters.Sorted = true;
            this.lstParameters.TabIndex = 0;
            this.lstParameters.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstParameters_MouseDoubleClick);
            this.lstParameters.SelectedIndexChanged += new System.EventHandler(this.lstParameters_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAdd.Location = new System.Drawing.Point(88, 101);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAdd.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlN);
            this.btnAdd.Size = new System.Drawing.Size(105, 23);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.TabStop = false;
            this.btnAdd.Text = "جدید <b><font color=\"#C0504D\">(Ctrl+N)</font></b>";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnEdit.Location = new System.Drawing.Point(88, 131);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnEdit.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.btnEdit.Size = new System.Drawing.Size(105, 23);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.TabStop = false;
            this.btnEdit.Text = "ویرایش <b><font color=\"#758C48\">(Ctrl+E)</font></b>";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnClose
            // 
            this.btnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(297, 397);
            this.btnClose.Name = "btnClose";
            this.btnClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnClose.Size = new System.Drawing.Size(90, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "بستن <b><font color=\"#B77540\">(Esc)</font></b>";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // PanelDescribe
            // 
            this.PanelDescribe.BackColor = System.Drawing.Color.Transparent;
            this.PanelDescribe.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelDescribe.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelDescribe.Controls.Add(this.lblDescription);
            this.PanelDescribe.DrawTitleBox = false;
            this.PanelDescribe.Location = new System.Drawing.Point(12, 220);
            this.PanelDescribe.Name = "PanelDescribe";
            this.PanelDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PanelDescribe.Size = new System.Drawing.Size(181, 171);
            // 
            // 
            // 
            this.PanelDescribe.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelDescribe.Style.BackColorGradientAngle = 90;
            this.PanelDescribe.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelDescribe.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderBottomWidth = 1;
            this.PanelDescribe.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelDescribe.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderLeftWidth = 1;
            this.PanelDescribe.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderRightWidth = 1;
            this.PanelDescribe.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderTopWidth = 1;
            this.PanelDescribe.Style.CornerDiameter = 4;
            this.PanelDescribe.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelDescribe.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelDescribe.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelDescribe.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.PanelDescribe.TabIndex = 3;
            this.PanelDescribe.Text = "توضیحات";
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescription.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblDescription.Location = new System.Drawing.Point(0, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDescription.Size = new System.Drawing.Size(175, 149);
            this.lblDescription.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnDelete.Location = new System.Drawing.Point(88, 191);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDelete.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlD);
            this.btnDelete.Size = new System.Drawing.Size(105, 23);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.TabStop = false;
            this.btnDelete.Text = "حذف <b>(Ctrl+D)</b>";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnStructure
            // 
            this.btnStructure.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnStructure.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnStructure.Location = new System.Drawing.Point(88, 161);
            this.btnStructure.Name = "btnStructure";
            this.btnStructure.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnStructure.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btnStructure.Size = new System.Drawing.Size(105, 23);
            this.btnStructure.TabIndex = 8;
            this.btnStructure.TabStop = false;
            this.btnStructure.Text = "ساختار <b><font color=\"#758C48\">(Ctrl+S)</font></b>";
            this.btnStructure.Click += new System.EventHandler(this.btnStructure_Click);
            // 
            // frmParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(400, 432);
            this.FormSubtitle = "مدیریت پارامتر ها";
            this.FormTitle = "خدمات انتقال - خط";
            this.Name = "frmParameters";
            this.Text = "فرم مدیریت پارامتر ها";
            this.Load += new System.EventHandler(this.Form_Load);
            this.MyMainPanel.ResumeLayout(false);
            this.PanelParameters.ResumeLayout(false);
            this.PanelDescribe.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel PanelParameters;
        private System.Windows.Forms.ListBox lstParameters;
        private DevComponents.DotNetBar.ButtonX btnAdd;
        private DevComponents.DotNetBar.ButtonX btnEdit;
        private DevComponents.DotNetBar.ButtonX btnClose;
        private DevComponents.DotNetBar.Controls.GroupPanel PanelDescribe;
        private System.Windows.Forms.Label lblDescription;
        private DevComponents.DotNetBar.ButtonX btnDelete;
        private DevComponents.DotNetBar.ButtonX btnStructure;
    }
}