﻿#region using
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
#endregion

namespace MohanirPouya.Forms.Options.ManageParameters.Parameters
{
    /// <summary>
    /// فرم نمایش سابقه پارامتر
    /// </summary>
    public partial class frmShowParamRev : Form
    {
        
        #region Fields

        /// <summary>
        /// فیلد نام پارامتر
        /// </summary>
        private readonly String _ParamName;

        #endregion

        #region Constructor

        /// <summary>
        /// سازنده پیش فرض كلاس فرم نمایش سابقه پارامتر
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="PartName">نام بخش</param>
        public frmShowParamRev(String ParamName, Int32 Revision, String PartName)
        {
            InitializeComponent();
            _ParamName = ParamName;
            ShowQueries(ParamName, Revision, PartName);
        }

        #endregion

        #region Event Handlers

        #region Form Load

        private void Form_Load(object sender, EventArgs e)
        {
            Text = "نمایش پارامتر: " + _ParamName;
        }

        #endregion

        #region btnClose_Click

        private void btnClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        #endregion

        #endregion

        #region Methods

        #region void ShowQueries(String Param , In32 Revision , String PartName)

        /// <summary>
        /// نمایش اطلاعات مربوط به پارامتر در جدول
        /// </summary>
        /// <param name="Param">نام پارامتر</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="PartName">بخش مورد نظر</param>
        private void ShowQueries(String Param, Int32 Revision, String PartName)
        {

            #region Prepare SqlCommand

            String CommandText = "SELECT SelectString " +
                "FROM " + PartName + ".ParametersLog " +
                "WHERE EnglishName = @ParamName AND Revision = @Revision";
            SqlCommand MySqlCommand = new SqlCommand(CommandText,
                new SqlConnection(MohanirPouya.Classes.DbBizClass.dbConnStr));
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = Param;
            MySqlCommand.Parameters.Add("@Revision", SqlDbType.Int);
            MySqlCommand.Parameters["@Revision"].Value = Revision;

            #endregion

            #region Execute SqlCommand

            try
            {
                MySqlCommand.Connection.Open();
                rtbCode.Text = MySqlCommand.ExecuteScalar().ToString();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion

        }

        #endregion

        #endregion

    }
}