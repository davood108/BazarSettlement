﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirPouya.Forms.Management.ManageParameters.Parameters;
using MohanirPouya .Classes ;
using MohanirVBClasses;
#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    /// <summary>
    /// فرم مدیریت پارامتر ها
    /// </summary>
    public partial class frmParameters : MohanirFormDialog 
    {

        
        #region Fields

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر ها
        /// </summary>
        private DataTable _DataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// لیست پارامتر های ذخیره شده در بخش مورد نظر
        /// </summary>
        private ArrayList _ArrayListParams;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش مورد جستجو
        /// </summary>
        private readonly String _PartName;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر
        /// </summary>
        private readonly String _ParamType;
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// سازنده پیش فرض فرم مدیریت پارامتر ها
        /// </summary>
        /// <param name="PartName">نام بخش از قبیل فروشندگان ، خریداران و غیره</param>
        /// <param name="ParamType">نوع پارامتر از قبیل پارامتر عادی یا غیره</param>
        public frmParameters(String PartName, String ParamType)
        {
            InitializeComponent();
            #region Set Form Title
            switch (PartName)
            {
                case "Sellers":
                    lblTitle.Text = "فروشندگان";
                    break;
                case "Buyers":
                    lblTitle.Text = "خریداران";
                    break;
                case "Transfer":
                    lblTitle.Text = "خدمات انتقال";
                    break;
            }
            #endregion
            _PartName = PartName;
            _ParamType = ParamType;
            ShowDialog();
        }

        #endregion

        #region Events Handlers

        #region Form Load

        private void Form_Load(object sender, EventArgs e)
        {
            FillParametersListBox();
        }
        #endregion

        #region lstParameters_SelectedIndexChanged

        private void lstParameters_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDescription.Text = Classes.Parameters.GetParamDescription
                (lstParameters.SelectedItem.ToString(), _DataTableParams);
        }
        #endregion

        #region lstParameters_MouseDoubleClick
        private void lstParameters_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditParam();
        }
        #endregion

        #region btnAdd_Click
        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmNewParameter(_PartName, _ParamType).ShowDialog();
            FillParametersListBox();
        }
        #endregion

        #region btnEdit_Click
        /// <summary>
        /// دكمه ویرایش كد های پارامتر
        /// </summary>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditParam();
        }
        #endregion

        #region btnStructure_Click
        private void btnStructure_Click(object sender, EventArgs e)
        {
            new ParamStructure(_PartName, lstParameters.SelectedItem.ToString());
        }
        #endregion

        #region btnDelete_Click
        /// <summary>
        /// متد مدیریت كننده روال حذف
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {

            if (lstParameters.SelectedItem == null)
            {

            }
            else
            {
                      #region Check User Permission For Delete Parameter

            const String QuestionText = "آیا از انجام این كار اطمینان دارید؟\n" +
                                        " با اجرای این فرمان پارامتر ذكر شده " +
                                        "به همراه تمام سابقه ها آن حذف خواهد شد.\n" +
                                        "همچنین كلیه اجزائی كه از این عنصر استفاده كرده اند از كار خواهد افتاد!";
            DialogResult TheResult =
                PersianMessageBox.Show(QuestionText, "هشدار!",
                                       MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                       MessageBoxDefaultButton.Button1);
            if (TheResult == System.Windows.Forms.DialogResult.No)
            {
                lstParameters.Focus();
                return;
            }

            #endregion
            Classes.Parameters.DeleteParam(lstParameters.SelectedItem.ToString(), _PartName);
            FillParametersListBox();
            }
      
        }
        #endregion

        #region btnClose_Click
        private void btnClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        #endregion

        #endregion

        #region Methods

        #region void FillParametersListBox()

        /// <summary>
        /// بدست آوردن جدول و ليست پارامتر ها از بانك اطلاعات
        /// </summary>
        private void FillParametersListBox()
        {
            _DataTableParams = new DataTable();
            _DataTableParams =
                Classes.Parameters.GetSavedParamsByPart(_PartName, _ParamType);
            _ArrayListParams = new ArrayList();
            _ArrayListParams =
                Classes.Parameters.GetParamsNameByTable(_DataTableParams);
            lstParameters.DataSource = _ArrayListParams;
            #region Select First ListBox Item
            if (lstParameters.Items.Count != 0)
                lstParameters.SelectedIndex = 0;
            lstParameters.Focus();
            #endregion
        }
        #endregion

        #region void EditParam()
        /// <summary>
        /// تابع ویرایش پارامتر جاری
        /// </summary>
        private void EditParam()
        {
            #region Check ListBox Items Count

            const String ErrorMessage = "هیچ پارامتری برای ویرایش موجود نمی باشد!" +
                                        " برای ایجاد پارامتر بر روی دكمه ی \"پارامتر جدید\"كلیك نمایید";
            if (lstParameters.Items.Count == 0)
            {
                PersianMessageBox.Show(ErrorMessage, "خطا!",
                                       MessageBoxButtons.OK, MessageBoxIcon.Warning,
                                       MessageBoxDefaultButton.Button1);
                lstParameters.Focus();
                return;
            }

            #endregion

            new frmModifyParameters(lstParameters.SelectedItem.ToString(),
                                    _PartName, _ParamType);
            FillParametersListBox();
        }
        #endregion

   
        
        #endregion

    }
}