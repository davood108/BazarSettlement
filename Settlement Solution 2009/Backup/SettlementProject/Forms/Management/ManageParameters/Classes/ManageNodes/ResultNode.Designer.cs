﻿using MohanirCompilerRichTextBox=MohanirPouya.Forms.Options.ManageParameters.Classes.MohanirCompilerRichTextBox;

namespace MohanirPouya.Forms.Options.ManageParameters.Classes.ManageNodes
{
    partial class frmResultNode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmResultNode));
            this.ParameterPanel = new DevComponents.DotNetBar.PanelEx();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnAccept = new DevComponents.DotNetBar.ButtonX();
            this.PanelCodeCompiler = new System.Windows.Forms.Panel();
            this.rtbCode = new MohanirPouya.Forms.Options.ManageParameters.Classes.MohanirCompilerRichTextBox();
            this.ToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCutTextFromFml = new System.Windows.Forms.ToolStripButton();
            this.btnCopyTextFromFml = new System.Windows.Forms.ToolStripButton();
            this.btnPasteTextToFml = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.btnRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnOp = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnOpSum = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpSubstract = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpMultiplation = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpDevide = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnFunctions = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnFkMax = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFkMin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPower = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSqrt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbs = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelDescribe = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblParamsList = new System.Windows.Forms.Label();
            this.lstParamsList = new System.Windows.Forms.ListBox();
            this.ParameterPanel.SuspendLayout();
            this.PanelCodeCompiler.SuspendLayout();
            this.ToolStrip.SuspendLayout();
            this.PanelDescribe.SuspendLayout();
            this.SuspendLayout();
            // 
            // ParameterPanel
            // 
            this.ParameterPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.ParameterPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ParameterPanel.Controls.Add(this.btnCancel);
            this.ParameterPanel.Controls.Add(this.btnAccept);
            this.ParameterPanel.Controls.Add(this.PanelCodeCompiler);
            this.ParameterPanel.Controls.Add(this.PanelDescribe);
            this.ParameterPanel.Controls.Add(this.lblParamsList);
            this.ParameterPanel.Controls.Add(this.lstParamsList);
            this.ParameterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParameterPanel.Location = new System.Drawing.Point(0, 0);
            this.ParameterPanel.Name = "ParameterPanel";
            this.ParameterPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ParameterPanel.RightToLeftLayout = true;
            this.ParameterPanel.Size = new System.Drawing.Size(551, 308);
            this.ParameterPanel.Style.Alignment = System.Drawing.StringAlignment.Far;
            this.ParameterPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.ParameterPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.ParameterPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ParameterPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ParameterPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ParameterPanel.Style.GradientAngle = 90;
            this.ParameterPanel.Style.LineAlignment = System.Drawing.StringAlignment.Near;
            this.ParameterPanel.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(448, 229);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(91, 31);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "انصراف (Esc)";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAccept.Location = new System.Drawing.Point(448, 266);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(91, 31);
            this.btnAccept.TabIndex = 3;
            this.btnAccept.Text = "تایید (Enter)";
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // PanelCodeCompiler
            // 
            this.PanelCodeCompiler.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelCodeCompiler.Controls.Add(this.rtbCode);
            this.PanelCodeCompiler.Controls.Add(this.ToolStrip);
            this.PanelCodeCompiler.Location = new System.Drawing.Point(213, 12);
            this.PanelCodeCompiler.Name = "PanelCodeCompiler";
            this.PanelCodeCompiler.Size = new System.Drawing.Size(326, 202);
            this.PanelCodeCompiler.TabIndex = 0;
            // 
            // rtbCode
            // 
            this.rtbCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCode.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rtbCode.Location = new System.Drawing.Point(0, 25);
            this.rtbCode.Name = "rtbCode";
            this.rtbCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtbCode.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbCode.Size = new System.Drawing.Size(326, 177);
            this.rtbCode.TabIndex = 0;
            this.rtbCode.Text = "";
            // 
            // ToolStrip
            // 
            this.ToolStrip.AutoSize = false;
            this.ToolStrip.BackColor = System.Drawing.Color.Transparent;
            this.ToolStrip.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.toolStripSeparator3,
            this.btnCutTextFromFml,
            this.btnCopyTextFromFml,
            this.btnPasteTextToFml,
            this.toolStripSeparator1,
            this.btnUndo,
            this.btnRedo,
            this.toolStripSeparator2,
            this.btnOp,
            this.toolStripSeparator5,
            this.btnFunctions});
            this.ToolStrip.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip.Name = "ToolStrip";
            this.ToolStrip.Size = new System.Drawing.Size(326, 25);
            this.ToolStrip.TabIndex = 1;
            this.ToolStrip.Text = "راهنمایی";
            // 
            // btnNew
            // 
            this.btnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(23, 22);
            this.btnNew.Text = "فرمول نویسی مجدد";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCutTextFromFml
            // 
            this.btnCutTextFromFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCutTextFromFml.Image = ((System.Drawing.Image)(resources.GetObject("btnCutTextFromFml.Image")));
            this.btnCutTextFromFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCutTextFromFml.Name = "btnCutTextFromFml";
            this.btnCutTextFromFml.Size = new System.Drawing.Size(23, 22);
            this.btnCutTextFromFml.Text = "برش";
            this.btnCutTextFromFml.Click += new System.EventHandler(this.btnCutTextFromFml_Click);
            // 
            // btnCopyTextFromFml
            // 
            this.btnCopyTextFromFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopyTextFromFml.Image = ((System.Drawing.Image)(resources.GetObject("btnCopyTextFromFml.Image")));
            this.btnCopyTextFromFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopyTextFromFml.Name = "btnCopyTextFromFml";
            this.btnCopyTextFromFml.Size = new System.Drawing.Size(23, 22);
            this.btnCopyTextFromFml.Text = "رونوشت";
            this.btnCopyTextFromFml.Click += new System.EventHandler(this.btnCopyTextFromFml_Click);
            // 
            // btnPasteTextToFml
            // 
            this.btnPasteTextToFml.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPasteTextToFml.Image = ((System.Drawing.Image)(resources.GetObject("btnPasteTextToFml.Image")));
            this.btnPasteTextToFml.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPasteTextToFml.Name = "btnPasteTextToFml";
            this.btnPasteTextToFml.Size = new System.Drawing.Size(23, 22);
            this.btnPasteTextToFml.Text = "چسباندن";
            this.btnPasteTextToFml.Click += new System.EventHandler(this.btnPasteTextToFml_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::MohanirPouya.Properties.Resources.Redo;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "بازگشت";
            this.btnUndo.ToolTipText = "باز گرداندن تغییرات به حالت قبلی";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnRedo
            // 
            this.btnRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRedo.Image = global::MohanirPouya.Properties.Resources.Undo;
            this.btnRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(23, 22);
            this.btnRedo.Text = "پیش روی";
            this.btnRedo.ToolTipText = "باز گرداندن تغییرات به حالت بعدی";
            this.btnRedo.Click += new System.EventHandler(this.btnRedo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnOp
            // 
            this.btnOp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnOp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpSum,
            this.btnOpSubstract,
            this.btnOpMultiplation,
            this.btnOpDevide});
            this.btnOp.ForeColor = System.Drawing.Color.Red;
            this.btnOp.Image = ((System.Drawing.Image)(resources.GetObject("btnOp.Image")));
            this.btnOp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOp.Name = "btnOp";
            this.btnOp.Size = new System.Drawing.Size(65, 22);
            this.btnOp.Text = "عملگر ها";
            // 
            // btnOpSum
            // 
            this.btnOpSum.Name = "btnOpSum";
            this.btnOpSum.Size = new System.Drawing.Size(82, 22);
            this.btnOpSum.Text = "+";
            this.btnOpSum.ToolTipText = "جمع";
            this.btnOpSum.Click += new System.EventHandler(this.btnOpSum_Click);
            // 
            // btnOpSubstract
            // 
            this.btnOpSubstract.Name = "btnOpSubstract";
            this.btnOpSubstract.Size = new System.Drawing.Size(82, 22);
            this.btnOpSubstract.Text = "-";
            this.btnOpSubstract.ToolTipText = "تفریق";
            this.btnOpSubstract.Click += new System.EventHandler(this.btnOpSubstract_Click);
            // 
            // btnOpMultiplation
            // 
            this.btnOpMultiplation.Name = "btnOpMultiplation";
            this.btnOpMultiplation.Size = new System.Drawing.Size(82, 22);
            this.btnOpMultiplation.Text = "*";
            this.btnOpMultiplation.ToolTipText = "ضرب";
            this.btnOpMultiplation.Click += new System.EventHandler(this.btnOpMultiplation_Click);
            // 
            // btnOpDevide
            // 
            this.btnOpDevide.Name = "btnOpDevide";
            this.btnOpDevide.Size = new System.Drawing.Size(82, 22);
            this.btnOpDevide.Text = "/";
            this.btnOpDevide.ToolTipText = "تقسیم";
            this.btnOpDevide.Click += new System.EventHandler(this.btnOpDevide_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnFunctions
            // 
            this.btnFunctions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFkMax,
            this.btnFkMin,
            this.toolStripSeparator4,
            this.btnPower,
            this.btnSqrt,
            this.btnAbs});
            this.btnFunctions.ForeColor = System.Drawing.Color.Purple;
            this.btnFunctions.Image = ((System.Drawing.Image)(resources.GetObject("btnFunctions.Image")));
            this.btnFunctions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFunctions.Name = "btnFunctions";
            this.btnFunctions.Size = new System.Drawing.Size(41, 22);
            this.btnFunctions.Text = "توابع";
            // 
            // btnFkMax
            // 
            this.btnFkMax.Name = "btnFkMax";
            this.btnFkMax.Size = new System.Drawing.Size(123, 22);
            this.btnFkMax.Text = "ماكسیمم";
            this.btnFkMax.Click += new System.EventHandler(this.btnFkMax_Click);
            // 
            // btnFkMin
            // 
            this.btnFkMin.Name = "btnFkMin";
            this.btnFkMin.Size = new System.Drawing.Size(123, 22);
            this.btnFkMin.Text = "مینیمم";
            this.btnFkMin.Click += new System.EventHandler(this.btnFkMin_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(120, 6);
            // 
            // btnPower
            // 
            this.btnPower.Name = "btnPower";
            this.btnPower.Size = new System.Drawing.Size(123, 22);
            this.btnPower.Text = "توان";
            this.btnPower.Click += new System.EventHandler(this.btnPower_Click);
            // 
            // btnSqrt
            // 
            this.btnSqrt.Name = "btnSqrt";
            this.btnSqrt.Size = new System.Drawing.Size(123, 22);
            this.btnSqrt.Text = "جذر";
            this.btnSqrt.Click += new System.EventHandler(this.btnSqrt_Click);
            // 
            // btnAbs
            // 
            this.btnAbs.Name = "btnAbs";
            this.btnAbs.Size = new System.Drawing.Size(123, 22);
            this.btnAbs.Text = "قدر مطلق";
            this.btnAbs.ToolTipText = "تابع قدر مطلق ، این تابع یك مقدار را دریافت كرده و قدر مطلق آن را بر می گرداند";
            this.btnAbs.Click += new System.EventHandler(this.btnAbs_Click);
            // 
            // PanelDescribe
            // 
            this.PanelDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelDescribe.BackColor = System.Drawing.Color.Transparent;
            this.PanelDescribe.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelDescribe.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelDescribe.Controls.Add(this.lblDescription);
            this.PanelDescribe.DrawTitleBox = false;
            this.PanelDescribe.Location = new System.Drawing.Point(12, 220);
            this.PanelDescribe.Name = "PanelDescribe";
            this.PanelDescribe.Size = new System.Drawing.Size(430, 77);
            // 
            // 
            // 
            this.PanelDescribe.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelDescribe.Style.BackColorGradientAngle = 90;
            this.PanelDescribe.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelDescribe.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderBottomWidth = 1;
            this.PanelDescribe.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelDescribe.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderLeftWidth = 1;
            this.PanelDescribe.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderRightWidth = 1;
            this.PanelDescribe.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelDescribe.Style.BorderTopWidth = 1;
            this.PanelDescribe.Style.CornerDiameter = 4;
            this.PanelDescribe.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelDescribe.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelDescribe.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelDescribe.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.PanelDescribe.TabIndex = 4;
            this.PanelDescribe.Text = "توضیحات";
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescription.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblDescription.Location = new System.Drawing.Point(0, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDescription.Size = new System.Drawing.Size(424, 55);
            this.lblDescription.TabIndex = 0;
            // 
            // lblParamsList
            // 
            this.lblParamsList.AutoSize = true;
            this.lblParamsList.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblParamsList.Location = new System.Drawing.Point(15, 17);
            this.lblParamsList.Name = "lblParamsList";
            this.lblParamsList.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamsList.Size = new System.Drawing.Size(121, 14);
            this.lblParamsList.TabIndex = 1;
            this.lblParamsList.Text = "پارامتر ها و سرفصل ها";
            // 
            // lstParamsList
            // 
            this.lstParamsList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lstParamsList.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstParamsList.FormattingEnabled = true;
            this.lstParamsList.IntegralHeight = false;
            this.lstParamsList.ItemHeight = 16;
            this.lstParamsList.Location = new System.Drawing.Point(12, 37);
            this.lstParamsList.Name = "lstParamsList";
            this.lstParamsList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lstParamsList.Size = new System.Drawing.Size(195, 177);
            this.lstParamsList.Sorted = true;
            this.lstParamsList.TabIndex = 0;
            this.lstParamsList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstParams_MouseDoubleClick);
            this.lstParamsList.SelectedIndexChanged += new System.EventHandler(this.lstParamsList_SelectedIndexChanged);
            // 
            // frmResultNode
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(551, 308);
            this.Controls.Add(this.ParameterPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmResultNode";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "مدیریت نتایج";
            this.Load += new System.EventHandler(this.Form_Load);
            this.ParameterPanel.ResumeLayout(false);
            this.ParameterPanel.PerformLayout();
            this.PanelCodeCompiler.ResumeLayout(false);
            this.ToolStrip.ResumeLayout(false);
            this.ToolStrip.PerformLayout();
            this.PanelDescribe.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx ParameterPanel;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnAccept;
        private System.Windows.Forms.Panel PanelCodeCompiler;
        private MohanirCompilerRichTextBox rtbCode;
        private System.Windows.Forms.ToolStrip ToolStrip;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnCutTextFromFml;
        private System.Windows.Forms.ToolStripButton btnCopyTextFromFml;
        private System.Windows.Forms.ToolStripButton btnPasteTextToFml;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripButton btnRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton btnOp;
        private System.Windows.Forms.ToolStripMenuItem btnOpSum;
        private System.Windows.Forms.ToolStripMenuItem btnOpSubstract;
        private System.Windows.Forms.ToolStripMenuItem btnOpMultiplation;
        private System.Windows.Forms.ToolStripMenuItem btnOpDevide;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripDropDownButton btnFunctions;
        private System.Windows.Forms.ToolStripMenuItem btnFkMax;
        private System.Windows.Forms.ToolStripMenuItem btnFkMin;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem btnPower;
        private System.Windows.Forms.ToolStripMenuItem btnSqrt;
        private System.Windows.Forms.ToolStripMenuItem btnAbs;
        private DevComponents.DotNetBar.Controls.GroupPanel PanelDescribe;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblParamsList;
        private System.Windows.Forms.ListBox lstParamsList;

    }
}