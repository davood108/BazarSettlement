﻿using System;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.Management.ManageParameters.Parameters;
using MohanirPouya.Forms.Management.ManageParameters.ParametersC;
using MohanirVBClasses;

namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{
    public partial class ParamStructure : Form
    {

        private readonly DbSMDataContext _DbSM;
 
        private readonly string _PartName;
        private IOrderedQueryable<ParametersLogS> queryS;
        private IOrderedQueryable<ParametersLogB> queryB;
        private IOrderedQueryable<ParametersLogT> queryT;
        private IQueryable<ParamInParamS> query2S;
        private IQueryable<ParamInParamB> query2B;
        private IQueryable<ParamInParamT> query2T;
        private IQueryable<ParameterS> querySC;
        private IQueryable<ParameterB> queryBC;
        private IQueryable<ParameterT> queryTC;
        private string _Categori;
        private readonly string _ParamName;
        private readonly int _Typ;
        private IQueryable<ParametersLogS> queryS1;
        private IQueryable<ParametersLogB> queryB1;
        private IQueryable<ParametersLogT> queryT1;
        private string[] Pname;
        private int Icount;
        private readonly int _Plevel;

        #region Ctor

        public ParamStructure(String PartName)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                (DbBizClass.dbConnStr);
  
            _PartName = PartName;
            _Typ = 0;

            #region Set Form Title
            if (_PartName == "Sellers")
                lblTitle.Text = "ساختار پارامترهای بخش فروش";
            else if (_PartName == "Buyers")
                lblTitle.Text = "ساختار پارامترهای بخش خرید";
            else if (_PartName == "Transfer")
                lblTitle.Text = "ساختار پارامترهای بخش خدمات انتقال";
            #endregion

            ShowDialog();
        }
        public ParamStructure(String PartName,String ParamName)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                (DbBizClass.dbConnStr);

            _PartName = PartName;
            _ParamName = ParamName;
            _Typ = 1;

            #region Set Form Title
            if (_PartName == "Sellers")
                lblTitle.Text = "ساختار پارامترهای بخش فروش";
            else if (_PartName == "Buyers")
                lblTitle.Text = "ساختار پارامترهای بخش خرید";
            else if (_PartName == "Transfer")
                lblTitle.Text = "ساختار پارامترهای بخش خدمات انتقال";
            #endregion

            ShowDialog();
        }
        public ParamStructure(String PartName, String ParamName,int Plevel)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                (DbBizClass.dbConnStr);

            _PartName = PartName;
            _ParamName = ParamName;
            _Typ = 2;
            _Plevel = Plevel;

            #region Set Form Title
            if (_PartName == "Sellers")
                lblTitle.Text = "ساختار پارامترهای بخش فروش";
            else if (_PartName == "Buyers")
                lblTitle.Text = "ساختار پارامترهای بخش خرید";
            else if (_PartName == "Transfer")
                lblTitle.Text = "ساختار پارامترهای بخش خدمات انتقال";
            #endregion
            FillTree();
         
        }


        #endregion

        #region Event

        #region Load
        private void ParamStructure_Load(object sender, EventArgs e)
        {
            FillTree();
        }
        #endregion

        #region PStructure_Click
        private void PStructure_Click(object sender, EventArgs e)
        {
            FillTree();
          }



        #endregion

        #region treeView1_NodeMouseDoubleClick
        private void treeView1_NodeMouseDoubleClick(object sender,                     TreeNodeMouseClickEventArgs e)
        {
            switch (_PartName)
            {
                case "Sellers":
                    {
                        querySC = _DbSM.ParameterS.
                            Where(data => data.EnglishName == e.Node.Text);
                        foreach (var t in querySC)
                        {
                            _Categori = t.Category;
                        }
                    }
                    break;
                case "Buyers":
                    {
                        queryBC = _DbSM.ParameterBs.
            Where(data => data.EnglishName == e.Node.Text);
                        foreach (var t in queryBC)
                        {
                            _Categori = t.Category;
                        }

                    }
                    break;
                case "Transfer":
                    {
                        queryTC = _DbSM.ParameterTs.
          Where(data => data.EnglishName == e.Node.Text);
                        foreach (var t in queryTC)
                        {
                            _Categori = t.Category;
                        }

                    }
                    break;
            }

            switch (_Categori)
            {
                case "Fx"  :
                    {
                        PersianMessageBox.Show("پارامتر انتخابی از نوع پارامترهای اصلی می باشد!", "خطا",
                                   MessageBoxButtons.OK, MessageBoxIcon.Error,
                                   MessageBoxDefaultButton.Button1);
                    
                    }
                    break;
                case "Pa":
                    {
                        new frmModifyParameters(e.Node.Text,
                                 _PartName, _Categori); 
                    }
                    break;
                case "Pc":
                    {
                        new frmModifyParametersC(e.Node.Text,
                                 _PartName, _Categori); 
                    }
                    break;

            }
        
        }
        #endregion

        #endregion

        #region Method

        #region Fill Tree

        private void FillTree()
        {
            treeView1.BeginUpdate();

            treeView1.Nodes.Clear();
            if (_Typ == 0)
            {
                #region Typ 0

                switch (_PartName)
                {
                    case "Sellers":
                        {
                            queryS = _DbSM.ParametersLogS.
                                Where(data => data.PLevel == 0).
                                OrderBy(data => data.EnglishName);
                            foreach (var v in queryS)
                            {
                                treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                            }

                        }
                        break;
                    case "Buyers":
                        {
                            queryB = _DbSM.ParametersLogBs.
                                Where(data => data.PLevel == 0).
                                OrderBy(data => data.EnglishName);
                            foreach (var v in queryB)
                            {
                                treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                            }

                        }
                        break;
                    case "Transfer":
                        {
                            queryT = _DbSM.ParametersLogTs.
                                Where(data => data.PLevel == 0).
                                OrderBy(data => data.EnglishName);
                            foreach (var v in queryT)
                            {
                                treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                            }

                        }
                        break;
                }

                #endregion
            }
            else
            {
                #region Typ 1,2
                switch (_PartName)
                {
                    case "Sellers":
                        {
                            queryS1 = _DbSM.ParametersLogS.
                                Where(data => data.EnglishName == _ParamName && data.Revision==0);

                            foreach (var v in queryS1.Distinct())
                            {
                                treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                            }

                        }
                        break;
                    case "Buyers":
                        {
                            queryB1 = _DbSM.ParametersLogBs.
                 Where(data => data.EnglishName == _ParamName);
                            foreach (var v in queryB1)
                            {
                                treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                            }

                        }
                        break;
                    case "Transfer":
                        {
                            queryT1 = _DbSM.ParametersLogTs.
                Where(data => data.EnglishName == _ParamName);
                            foreach (var v in queryT1)
                            {
                                treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                            }

                        }
                        break;
                }
                #endregion
            }


            Pname = new string[10000];
            Icount = 1;


            foreach (TreeNode t1 in treeView1.Nodes)
            {
                RecursiveSearchInTree(t1);
            }

            treeView1.EndUpdate();
            
            if(_Typ==2)
            {
                UpdatePlevel();
            }

        }


        #endregion

        #region String RecursiveSearchInTree(TreeNode TreeNode)


        private void RecursiveSearchInTree(TreeNode treeNode1)
        {
            
            switch (_PartName)
            {
                case "Sellers":
                    {

                        query2S = _DbSM.ParamInParamS.Where(d => d.Expr1 == treeNode1.Text);
                        foreach (var v1 in query2S)
                        {
                            TreeNode t2 = treeNode1.Nodes.Add(v1.EnglishName);

                            Pname[Icount] = v1.EnglishName;
                            Icount = Icount + 1;

                            RecursiveSearchInTree(t2);
                        }
                    }
                    break;
                case "Buyers":
                    {
                        query2B = _DbSM.ParamInParamBs.Where(d => d.Expr1 == treeNode1.Text);
                        foreach (var v1 in query2B)
                        {
                            TreeNode t2 = treeNode1.Nodes.Add(v1.EnglishName);

                            RecursiveSearchInTree(t2);
                        }

                    }
                    break;
                case "Transfer":
                    {

                        query2T = _DbSM.ParamInParamTs.Where(d => d.Expr1 == treeNode1.Text);
                        foreach (var v1 in query2T)
                        {
                            TreeNode t2 = treeNode1.Nodes.Add(v1.EnglishName);

                            RecursiveSearchInTree(t2);
                        }
                    }
                    break;
            }
      

        }

        #endregion

        #region UpdatePlevel
        private void UpdatePlevel()
        {
            foreach (var VARIABLE in Pname.Distinct())
            {
          _DbSM.SP_UpdatePlevel(VARIABLE, _Plevel);
            }
        }
        #endregion

       #endregion




    }
}