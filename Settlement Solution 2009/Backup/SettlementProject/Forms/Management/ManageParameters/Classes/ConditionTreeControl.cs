﻿#region using
using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using MohanirPouya.Forms.Options.ManageParameters.Classes.ManageNodes;
using MohanirVBClasses;
using PureComponents.TreeView;
#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{
    /// <summary>
    /// كلاس كنترل مدیریت درخت شروط و نتایج
    /// </summary>
    public partial class ConditionTreeControl : UserControl
    {
        #region Fields

        #region String _PartName

        #endregion

        #endregion

        #region Constructor & Destructor

        #region Constructor
        /// <summary>
        /// سازنده پیش فرض كلاس
        /// </summary>
        public ConditionTreeControl()
        {
            InitializeComponent();
        }
        #endregion

        #endregion

        #region Properties

        #region String PartName
        /// <summary>
        /// نام بخش مورد جستجو
        /// </summary>
        public String PartName { get; set; }
        #endregion

        #endregion

        #region Event Handlers

        #region btnAddRootCondition_MouseClick
        /// <summary>
        /// روال افزودن شرط ریشه به درخت
        /// </summary>
        private void btnAddRootCondition_MouseClick(object sender, MouseEventArgs e)
        {
            Node NewNode = AddRootCondition(CodeTreeView.Nodes);
            if (NewNode != null) CodeTreeView.Nodes.Add(NewNode);
            CodeTreeView.ExpandAll();
            CodeTreeView.Focus();
        }
        #endregion

        #region btnAddChildCondition_MouseClick
        /// <summary>
        /// روال افزودن شرط فرزند به درخت
        /// </summary>
        private void btnAddChildCondition_MouseClick(object sender, MouseEventArgs e)
        {
            Node NewConditionNode = AddChildCondition(CodeTreeView.SelectedNode);
            if (NewConditionNode != null)
                if (CodeTreeView.SelectedNode == null)
                    CodeTreeView.Nodes.Add(NewConditionNode);
                else
                {
                    CodeTreeView.SelectedNode.Nodes.Add(NewConditionNode);
                    CodeTreeView.SelectedNode.ExpandAll();
                }
            CodeTreeView.Focus();
        }
        #endregion

        #region btnAddResult_MouseClick
        /// <summary>
        /// روال افزودن نتیجه فرزند به درخت
        /// </summary>
        private void btnAddResult_MouseClick(object sender, MouseEventArgs e)
        {
            Node NewResultNode = AddChildResult(CodeTreeView.SelectedNode);
            if (CodeTreeView.SelectedNode != null)
                if (NewResultNode != null)
                {
                    CodeTreeView.SelectedNode.Nodes.Add(NewResultNode);
                    CodeTreeView.SelectedNode.ExpandAll();
                }
            CodeTreeView.Focus();
        }
        #endregion

        #region btnEditNode_MouseClick
        private void btnEditNode_MouseClick(object sender, MouseEventArgs e)
        {
            CodeTreeView.SelectedNode = EditTreeNode(CodeTreeView.SelectedNode);
            CodeTreeView.Focus();
        }
        #endregion

        #region btnClearTreeView_MouseClick
        /// <summary>
        /// روال خالی كردن درخت از گره
        /// </summary>
        private void btnClearTreeView_MouseClick(object sender, MouseEventArgs e)
        {
            #region Check User Permission
            DialogResult MyResult =
                PersianMessageBox.Show("آیا از این كار اطمینان دارید؟" +
                                       " با این كار كلیه شروط و نتایج حذف خواهند گردید", "هشدار",
                                       MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                                       MessageBoxDefaultButton.Button1);
            #endregion
            #region Clear TreeView
            if (MyResult == DialogResult.Yes) CodeTreeView.Nodes.Clear();
            #endregion
        }
        #endregion

        #endregion

        #region Methods

        #region TreeNode AddRootCondition(TreeNodeCollection Nodes)
        /// <summary>
        /// تابعی برای افزودن شرط ریشه به درخت
        /// </summary>
        /// <param name="Nodes">گره های مورد نظر</param>
        /// <returns>گره شرط تولید شده</returns>
        private Node AddRootCondition(NodeCollection Nodes)
        {
            Node MadeNode = null;

            #region Check Adding (Condition) Node In (Result) Node * Denied *
            foreach (Node node in Nodes)
            {
                if (node.Tag.ToString() == "R")
                {
                    PersianMessageBox.Show("امكان افزودن شرط با وجود نتیجه وجود ندارد!",
                                           "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                           MessageBoxDefaultButton.Button1);
                    return MadeNode;
                }
            }
            #endregion
            var MyForm = new frmConditionNode(PartName);
            MyForm.ShowDialog();
            if (MyForm.IsOk)
                MadeNode = SetConditionNodeProperties(MyForm.ConditionExpression);
            return MadeNode;
        }
        #endregion

        #region TreeNode AddChildCondition(TreeNode SelectedNode)
        /// <summary>
        /// تابعی برای افزودن شرط فرزند به یك گره
        /// </summary>
        /// <param name="SelectedNode">گره انتخاب شده یا گره پدر</param>
        /// <returns>گره شرط تولید شده</returns>
        private Node AddChildCondition(Node SelectedNode)
        {
            Node MadeNode = null;
            #region If No Node Is Selected
            if (SelectedNode == null)
            {
                var MyForm = new frmConditionNode(PartName);
                MyForm.ShowDialog();
                if (MyForm.IsOk)
                    MadeNode = SetConditionNodeProperties(MyForm.ConditionExpression);
            }
                #endregion

                #region Else If Selected Node Is (Result) * Denied *
            else if (SelectedNode.Tag.ToString() == "R")
            {
                PersianMessageBox.Show("خطا! در زیر نتیجه نمی توان شرط قرار داد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
            }
                #endregion

                #region Else If Selected Node Is (Condition)
            else if (SelectedNode.Tag.ToString() == "C")
            {
                #region If Node Have (Result) Child Node * Denied *
                foreach (Node Node in SelectedNode.Nodes)
                {
                    if (Node.Tag.ToString() == "R")
                    {
                        PersianMessageBox.Show("خطا! این گره شامل نتیجه می باشد!", "خطا",
                                               MessageBoxButtons.OK, MessageBoxIcon.Error,
                                               MessageBoxDefaultButton.Button1);
                        return MadeNode;
                    }
                }
                #endregion
                var MyForm = new frmConditionNode(PartName);
                MyForm.ShowDialog();
                if (MyForm.IsOk)
                    MadeNode = SetConditionNodeProperties(MyForm.ConditionExpression);
            }
            #endregion
            return MadeNode;
        }
        #endregion

        #region TreeNode AddChildResult(TreeNode SelectedNode)
        /// <summary>
        /// تابعی برای افزودن نتیجه فرزند به یك گره
        /// </summary>
        /// <param name="SelectedNode">گره انتخاب شده یا گره پدر</param>
        /// <returns>گره شرط تولید شده</returns>
        private Node AddChildResult(Node SelectedNode)
        {
            Node MadeNode = null;
            #region Check Adding (Result) Without (Condition) * Denied *
            if (SelectedNode == null)
            {
                PersianMessageBox.Show("امكان افزودن نتیجه بدون وجود شرط نمیباشد",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
            }
                #endregion

                #region If Selected Node Is (Condition)
            else if (SelectedNode.Tag.ToString() == "C")
            {
                #region If Selected Node Have Child(s) * Denied *
                if (SelectedNode.Nodes.Count > 0)
                {
                    PersianMessageBox.Show(
                        "امكان افزودن نتیجه به گره ای شامل شرط یا نتیجه ممكن نمی باشد",
                        "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
                }
                    #endregion
                    #region Else: Add New Child (Result) Node To Selected (Condition) Node
                else
                {
                    var MyForm = new frmResultNode(PartName);
                    MyForm.ShowDialog();
                    if (MyForm.IsOk)
                        MadeNode = SetResultNodeProperties(MyForm.ResultExpression);
                }
                #endregion
            }
                #endregion

                #region Else If Selected Node Is (Result) * Denied *
            else if (SelectedNode.Tag.ToString() == "R")
            {
                PersianMessageBox.Show("خطا! در زیر نتیجه نمی توان نتیجه قرار داد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
            }
            #endregion
            return MadeNode;
        }
        #endregion

        #region TreeNode EditTreeNode(TreeNode SelectedNode)
        /// <summary>
        /// تابعی برای ویرایش گره انتخاب شده
        /// </summary>
        /// <param name="SelectedNode">گره انتخاب شده</param>
        /// <returns>گره ویرایش شده</returns>
        private Node EditTreeNode(Node SelectedNode)
        {
            Node MadeNode = SelectedNode;

            #region If There Is No Selected Node * Denied *
            if (SelectedNode == null)
            {
                PersianMessageBox.Show("گزینه ای برای ویرایش انتخاب نشده!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
            }
                #endregion

                #region Else If: (Condition) Node Is Selected
            else if (SelectedNode.Tag.ToString() == "C")
            {
                var MyForm = new frmConditionNode(PartName);
                String NodeCondition =
                    SelectedNode.Text.Substring(3, SelectedNode.Text.Length - 8);

                MyForm.ConditionExpression = NodeCondition;
                MyForm.ShowDialog();
                #region Set Selected Node Properties
                if (MyForm.IsOk)
                {
                    String Condition = MyForm.ConditionExpression;
                    String PrintCondition = "IF " + Condition + " THEN";
                    MadeNode.Text = PrintCondition;
                }
                #endregion
            }
                #endregion

                #region Else If: (Result) Node Is Selected
            else if (SelectedNode.Tag.ToString() == "R")
            {
                var MyForm = new frmResultNode(PartName) {ResultExpression = SelectedNode.Text};
                MyForm.ShowDialog();
                #region Set Selected Node Properties
                if (MyForm.IsOk)
                {
                    String Result = MyForm.ResultExpression;
                    String PrintCondition = Result;
                    MadeNode.Text = PrintCondition;
                }
                #endregion
            }
            #endregion

            return MadeNode;
        }
        #endregion

        #region + Filling TreeView Items Methods +

        #region public void FillTreeView(String CurrentParamXml)
        /// <summary>
        /// متدی برای تكمیل ساختار درخت از اطلاعات پارامتر جاری
        /// </summary>
        /// <param name="CurrentParamXml">رشته ایكس ام ال درخت</param>
        public void FillTreeView(String CurrentParamXml)
        {
            #region DeSerialize TreeView Data From XML String
            CodeTreeView.Clear();
            try
            {            
                File.WriteAllText("TempXml.Xml", CurrentParamXml, Encoding.Unicode);
                CodeTreeView.LoadXml("TempXml.Xml");
                File.Delete("TempXml.Xml");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            #endregion
            SetTreeNodesProperties();
            CodeTreeView.ExpandAll();
        }
        #endregion

        #region private void SetTreeNodesProperties()
        /// <summary>
        /// متدی برای تنظیم رنگ عناصر درخت
        /// </summary>
        private void SetTreeNodesProperties()
        {
            foreach (Node CurrentNode in CodeTreeView.Nodes)
            {
                CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                if (CurrentNode.Tag.ToString() == "C") // For Conditions (C):
                {
                    CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                    CurrentNode.NodeStyle.Font = new Font(new FontFamily("Arial"), 14);
                    CurrentNode.NodeStyle.HoverBackColor = Color.PaleGreen;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Navy;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.GreenYellow;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.MediumBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.Blue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading; 
                    if (CurrentNode.Nodes.Count > 0)
                        RecursiveSetTreeNodeColor(CurrentNode);
                }
                else // For Results (R):
                {
                    CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                    CurrentNode.NodeStyle.Font =
                        new Font(new FontFamily("Arial Narrow"), 14, FontStyle.Bold);
                    CurrentNode.NodeStyle.HoverBackColor = Color.LightCoral;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Red;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.LightSalmon;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.RoyalBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.CornflowerBlue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
                }
            }
        }
        #endregion

        #region private void RecursiveSetTreeNodeColor(Node TheNode)
        /// <summary>
        /// تابعی برای جستجوی بازگشتی عناصر درخت و تنظیم رنگ آنها
        /// </summary>
        /// <param name="TheNode">گره جاری برای جستجو</param>
        private static void RecursiveSetTreeNodeColor(Node TheNode)
        {
            foreach (Node CurrentNode in TheNode.Nodes)
            {
                CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                if (CurrentNode.Tag.ToString() == "C") // For Conditions (C):
                {
                    CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                    CurrentNode.NodeStyle.Font = new Font(new FontFamily("Arial"), 14);
                    CurrentNode.NodeStyle.HoverBackColor = Color.PaleGreen;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Navy;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.GreenYellow;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.MediumBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.Blue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
                    if (CurrentNode.Nodes.Count > 0)
                        RecursiveSetTreeNodeColor(CurrentNode);
                }
                else // For Results (R):
                {
                    CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                    CurrentNode.NodeStyle.Font =
                        new Font(new FontFamily("Arial Narrow"), 14, FontStyle.Bold);
                    CurrentNode.NodeStyle.HoverBackColor = Color.LightCoral;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Red;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.LightSalmon;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.RoyalBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.CornflowerBlue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
                }
            }
        }
        #endregion

        #region private Node SetConditionNodeProperties(String Condition)
        private static Node SetConditionNodeProperties(String Condition)
        {
            var CurrentNode = new Node {NodeStyleSource = NodeStyleSource.Local};
            String PrintCondition = "IF " + Condition + " THEN";
            CurrentNode.Text = PrintCondition;            
            CurrentNode.Tag = "C";
            CurrentNode.NodeStyle.Font = new Font(new FontFamily("Arial") , 14);
            CurrentNode.NodeStyle.HoverBackColor = Color.PaleGreen;
            CurrentNode.NodeStyle.HoverForeColor = Color.Black;
            CurrentNode.NodeStyle.ForeColor = Color.Navy;
            CurrentNode.NodeStyle.SelectedBackColor = Color.GreenYellow;
            CurrentNode.NodeStyle.SelectedForeColor = Color.MediumBlue;
            CurrentNode.NodeStyle.SelectedBorderColor = Color.Blue;
            CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
            return CurrentNode;
        }
        #endregion

        #region private Node SetResultNodeProperties(String Result)
        private static Node SetResultNodeProperties(String Result)
        {
            var CurrentNode = new Node();
            CurrentNode.NodeStyleSource = NodeStyleSource.Local;
            CurrentNode.Text = Result;
            CurrentNode.Tag = "R";
            CurrentNode.NodeStyle.Font = 
                new Font(new FontFamily("Arial Narrow"), 14 , FontStyle.Bold);
            CurrentNode.NodeStyle.HoverBackColor = Color.LightCoral;
            CurrentNode.NodeStyle.HoverForeColor = Color.Black;
            CurrentNode.NodeStyle.ForeColor = Color.Red;
            CurrentNode.NodeStyle.SelectedBackColor = Color.LightSalmon;
            CurrentNode.NodeStyle.SelectedForeColor = Color.RoyalBlue;
            CurrentNode.NodeStyle.SelectedBorderColor = Color.CornflowerBlue;
            CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
            return CurrentNode;
        }
        #endregion

        #endregion

        #endregion
    }
}