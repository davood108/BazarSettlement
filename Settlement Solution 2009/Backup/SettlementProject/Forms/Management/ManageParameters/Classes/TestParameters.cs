﻿#region using
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirVBClasses;
#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{
    /// <summary>
    /// فرم بررسی نتیجه پارامتر
    /// </summary>
    public partial class frmTestParameters : Form
    {
        private DbSMDataContext _DbSM;
        private object[] Param;
        private SqlDataReader reader1;

        #region Constructor
        /// <summary>
        /// سازنده پیش فرض فرم نمايش مقادير آزمايشي پارامتر
        /// </summary>
        /// <param name="CommandText">رشته فرمان اجرايي اس كيو ال</param>
        public frmTestParameters(String CommandText)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
     (DbBizClass.dbConnStr);
            try
            {
                FillDataGridView(CommandText);
                ShowDialog();
            }
            catch (Exception)
            {
                Dispose();
            }
        }
        #endregion

        #region Methods

        #region void FillDataGridView(String CommandText)
        /// <summary>
        /// تابعي براي خواندن اطلاعات از بانك اطلاعاتي و نمايش آنها در جدول
        /// </summary>
        /// <param name="CommandText">رشته ي فرمان اس كيو ال</param>
        private void FillDataGridView(String CommandText)
        {

            #region Prepare SqlCommand
            var ParameterResult = new DataTable();
            //ParameterResult.Columns.Add("xxx", typeof (SqlDecimal));
            //_DbSM.ExecuteCommand(CommandText);


            var MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
            var MySqlCommand = new SqlCommand(CommandText, MySqlConnection) {CommandTimeout = 0};

            #endregion

            #region Execute SqlCommand
            try
            {
                MySqlCommand.Connection.Open();

                ParameterResult.Load(MySqlCommand.ExecuteReader());
             //   reader1 = MySqlCommand.ExecuteReader();
              //  _DbSM.ExecuteQuery<Reader2>(CommandText);
                dgvResult.DataSource = ParameterResult;
              //  dgvResult.DataSource = reader1;
            }
            catch (Exception)
            {
                PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                                       "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                                       MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
           //     MessageBox.Show(e.Message);
                Dispose();
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion

            #region Set Columns Size
            foreach (DataGridViewColumn column in dgvResult.Columns)
            {
                column.Width = 130;
            }
            #endregion
            
        }
        #endregion

        #endregion

        #region Event Handlers

        #region Form_Closing
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            Dispose();
        }
        #endregion

        #endregion

    }

    internal class Reader2
    {
    }
}