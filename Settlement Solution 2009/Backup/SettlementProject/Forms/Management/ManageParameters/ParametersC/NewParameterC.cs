﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.ParametersC
{
    /// <summary>
    /// فرم افزودن سرفصل جدید به بانك اطلاعات
    /// </summary>
    public partial class frmNewParameterC : Form
    {

        #region Fields

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر و سرفصل ها
        /// </summary>
        private DataTable _DataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _ArrayListParams;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش مورد جستجو
        /// </summary>
        private readonly String _PartName;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر مورد نظر
        /// </summary>
        private readonly String _ParamType;

        private readonly string _Reuse;
        private readonly string _CurrentParamName;
        private string _CurrentParamXml;

        #endregion

        private readonly string _UserID;
        private  string TestingQuery;
        private string Cdate;
        private bool TestState;

        #endregion

        #region Constructor
        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="PartName">نام بخش مورد نظر</param>
        /// <param name="ParamType">نوع پارامتر</param>
        /// <param name="Reuse"></param>
        /// <param name="ListItem"></param>
        /// <param name="testingQuery"></param>
        public frmNewParameterC(String PartName, String ParamType, string Reuse, string ListItem, string testingQuery)
        {
            InitializeComponent();
            _UserID =DbBizClass.CurrentUserLastName;
            _PartName = PartName;
            TestingQuery = testingQuery;
            TreeViewCode.PartName = PartName;
            _ParamType = ParamType;
            _Reuse = Reuse;
            _CurrentParamName = ListItem;
            #region SetTime
            PersianDate OccuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            PCreationDate.Text = OccuredDate.ToString();
            #endregion
        }

   

        #endregion

        #region Events Handlers

        #region Form Load

        /// <summary>
        /// روال مدیریت آغاز فراخوانی فرم
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            #region Set Form Title
            if (_PartName == "Sellers") lblTitle.Text = "فروشندگان";
            else if (_PartName == "Buyers") lblTitle.Text = "خریداران";
            else if (_PartName == "Transfer") lblTitle.Text = "خدمات انتقال";
            if (_ParamType == "Pc")
            {
                Text = "فرم ایجاد پارامتر شرطی جدید";
                lblSubtitle.Text = "ایجاد پارامتر شرطی جدید";
            }
            else if (_ParamType == "Sa")
            {
                Text = "فرم ایجاد سرفصل جدید";
                lblSubtitle.Text = "ایجاد سرفصل جدید";
            }
            else if (_ParamType == "Sh")
            {
                Text = "فرم ایجاد شاخص جدید";
                lblSubtitle.Text = "ایجاد شاخص جدید";
            }
            #endregion
            
            FillParametersTable();
            SetParamsNameAutoComplete();
            if (_Reuse == "R")
            {
                FillParametersTable();
                SetParamLastDataByName();
                TreeViewCode.FillTreeView(_CurrentParamXml);
            }
        }

        #endregion

        // 000000000000000000000000

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void btnValidation_Click(object sender, EventArgs e)
        {
            #region Get Line Condition String From TreeView Nodes
            String LineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCode.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(LineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion

            #region Check User Data Entry
            if (CheckParamNameTextBox() == false)  return;
            #endregion

 

            String ParamName = txtParamName.Text;
            TestingQuery = ManageParametersC.GetParamString(_PartName, ParamName,LineConditionString, _DataTableParams, _ArrayListParams, true, 0);
            
            if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
        }
        #endregion

        #region btnSaveAndClose_Click
        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            #region Check First Day of Month choose
            //if ((PCreationDate.Text.Substring(8, 2)) != "01")
            //{
            //    PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
            //               MessageBoxButtons.OK, MessageBoxIcon.Error,
            //               MessageBoxDefaultButton.Button1);
            //    return;
            //}
            #endregion

            #region Set Current Date
            PersianDate OccuredDate1 = PersianDateConverter.ToPersianDate
(DateTime.Now);
            Cdate = OccuredDate1.ToString();
            #endregion

            #region Check User Data Entry
            if (CheckParamNameTextBox() == false ||
             CheckDescriptionTextBox() == false) return;
            #endregion

            #region Get Line Condition String From TreeView Nodes
            String LineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCode.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(LineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion
          
            #region Declare Variables
            String ParamName = txtParamName.Text;
            const Int32 NewParamRevision = 0;

            #endregion

            #region Testing
            TestingQuery = ManageParametersC.GetParamString(_PartName, ParamName, LineConditionString, _DataTableParams, _ArrayListParams, true, 0);

            if (!String.IsNullOrEmpty(TestingQuery))
                TestState = CheckTestingQuery(TestingQuery);
            if (TestState == false)
            {
                PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                       "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                       MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion

            #region *** Creating Parameter Table By SQL Query ***
            if (ManageParametersC.CreateParameterTable(_DataTableParams,
                                                      _ArrayListParams, ParamName, txtParamDescribe.Text, NewParamRevision, LineConditionString, _UserID,_PartName, _ParamType, TreeViewCode.CodeTreeView, TestingQuery,PCreationDate.Text,Cdate) == false) return;
            #endregion

            Dispose();
        }
        #endregion

        // 000000000000000000000000

        #region btnCancel Click

        /// <summary>
        /// روال انصراف از تعریف پارامتر شرطی
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()
        /// <summary>
        /// بدست آوردن ليست كلیه پارامترها ، پارامترهای شرطی ، سرفصلها و شاخص ها
        /// </summary>
        private void FillParametersTable()
        {
            _DataTableParams = Classes.Parameters.GetAllParamsByPart(_PartName);
            _ArrayListParams =
                Classes.Parameters.GetParamsNameByTable(_DataTableParams);
        }

        #endregion

        #region void SetParamsNameAutoComplete()

        /// <summary>
        /// تنظيم عناصر پارامتر ها براي نام پارامتر
        /// </summary>
        private void SetParamsNameAutoComplete()
        {
            var MyAutoCompleteCustomSource =
                new AutoCompleteStringCollection();
            for (int i = 0; i < _ArrayListParams.Count; i++)
                MyAutoCompleteCustomSource.Add(_ArrayListParams[i].ToString());
            txtParamName.AutoCompleteCustomSource = MyAutoCompleteCustomSource;
        }

        #endregion

        #region void SetParamLastDataByName()
        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void SetParamLastDataByName()
        {


            #region Get Current Param Revision  , SelectString (XML)

            #region Prepare SqlCommand

            String CommandText = "SELECT TOP 1 Revision , " +
                                 "SelectString " +
                                 "FROM " + _PartName + ".ParametersLog " +
                                 "WHERE EnglishName = @ParamName ORDER BY Revision DESC";
            var MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
            var MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

            #endregion

            #region Execute SqlCommand
            try
            {
                MySqlCommand.Connection.Open();
                SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                if (MySqlDataReader != null)
                {
                    MySqlDataReader.Read();
                    _CurrentParamXml = MySqlDataReader[1].ToString();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion

            #endregion


        }

        #endregion


        #region Boolean CheckParamNameTextBox()

        /// <summary>
        /// بررسي جعبه متن نام پارامتر
        /// </summary>
        private Boolean CheckParamNameTextBox()
        {
            if (txtParamName.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر نامي به انگليسي وارد نماييد!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamName.Focus();
                return false;
            }
            if (txtParamName.Text.Length < 3)
            {
                PersianMessageBox.Show("نام پارامتر بايد حداقل داراي 3 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamName.Focus();
                return false;
            }
            return true;
        }

        #endregion
        
        #region Boolean CheckDescriptionTextBox()

        /// <summary>
        /// بررسي جعبه متن توضيحات پارامتر
        /// </summary>
        private Boolean CheckDescriptionTextBox()
        {
            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            return true;
        }

        #endregion


        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var ParameterResult = new DataTable();

            var MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
            var MySqlCommand = new SqlCommand(query, MySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                MySqlCommand.Connection.Open();

                // ReSharper disable AssignNullToNotNullAttribute
                ParameterResult.Load(MySqlCommand.ExecuteReader());
                // ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion
            return true;

        }
        #endregion


        #endregion

    }
}