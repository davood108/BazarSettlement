﻿namespace MohanirPouya.Forms.Options.ManageParameters.ParametersC
{
    partial class frmShowParamCRev
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PureComponents.TreeView.TreeViewPathSelectorStyle treeViewPathSelectorStyle1 = new PureComponents.TreeView.TreeViewPathSelectorStyle();
            PureComponents.TreeView.ContextMenuStrings contextMenuStrings1 = new PureComponents.TreeView.ContextMenuStrings();
            PureComponents.TreeView.TreeViewStyle treeViewStyle1 = new PureComponents.TreeView.TreeViewStyle();
            PureComponents.TreeView.NodeStyle nodeStyle1 = new PureComponents.TreeView.NodeStyle();
            PureComponents.TreeView.CheckBoxStyle checkBoxStyle1 = new PureComponents.TreeView.CheckBoxStyle();
            PureComponents.TreeView.ExpandBoxStyle expandBoxStyle1 = new PureComponents.TreeView.ExpandBoxStyle();
            PureComponents.TreeView.NodeTooltipStyle nodeTooltipStyle1 = new PureComponents.TreeView.NodeTooltipStyle();
            this.MyPanel = new DevComponents.DotNetBar.PanelEx();
            this.btnClose = new DevComponents.DotNetBar.ButtonX();
            this.TreeViewNavigator = new PureComponents.TreeView.TreeViewPathSelector();
            this.CodeTreeView = new PureComponents.TreeView.TreeView();
            this.MyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTreeView)).BeginInit();
            this.SuspendLayout();
            // 
            // MyPanel
            // 
            this.MyPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MyPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MyPanel.Controls.Add(this.TreeViewNavigator);
            this.MyPanel.Controls.Add(this.CodeTreeView);
            this.MyPanel.Controls.Add(this.btnClose);
            this.MyPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MyPanel.Location = new System.Drawing.Point(0, 0);
            this.MyPanel.Name = "MyPanel";
            this.MyPanel.RightToLeftLayout = true;
            this.MyPanel.Size = new System.Drawing.Size(731, 513);
            this.MyPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyPanel.Style.GradientAngle = 90;
            this.MyPanel.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(12, 478);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "بستن <b><font color=\"#FFC20E\">(Esc)</font></b>";
            // 
            // TreeViewNavigator
            // 
            this.TreeViewNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeViewNavigator.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeViewNavigator.Location = new System.Drawing.Point(12, 12);
            this.TreeViewNavigator.Name = "TreeViewNavigator";
            this.TreeViewNavigator.Size = new System.Drawing.Size(707, 32);
            treeViewPathSelectorStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
            treeViewPathSelectorStyle1.FadeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(193)))), ((int)(((byte)(255)))));
            this.TreeViewNavigator.Style = treeViewPathSelectorStyle1;
            this.TreeViewNavigator.TabIndex = 26;
            this.TreeViewNavigator.TreeView = this.CodeTreeView;
            // 
            // CodeTreeView
            // 
            this.CodeTreeView.AllowAdding = false;
            this.CodeTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CodeTreeView.ContextMenuArranging = true;
            this.CodeTreeView.ContextMenuEditing = true;
            contextMenuStrings1.AddNode = "افزودن";
            contextMenuStrings1.Collapse = "بستن";
            contextMenuStrings1.Copy = "كپی";
            contextMenuStrings1.DeleteNode = "حذف";
            contextMenuStrings1.EditNode = "ویرایش";
            contextMenuStrings1.Expand = "باز كردن";
            contextMenuStrings1.MoveBottom = "ارسال به پایین";
            contextMenuStrings1.MoveDown = "انتقال به پایین";
            contextMenuStrings1.MoveLeft = "جابجایی به چپ";
            contextMenuStrings1.MoveRight = "جابجایی به راست";
            contextMenuStrings1.MoveTop = "ارسال به بالا";
            contextMenuStrings1.MoveUp = "انتقال به بالا";
            contextMenuStrings1.Paste = "چسباندن";
            this.CodeTreeView.ContextMenuStrings = contextMenuStrings1;
            this.CodeTreeView.LabelEdit = false;
            this.CodeTreeView.Location = new System.Drawing.Point(12, 50);
            this.CodeTreeView.Multiline = false;
            this.CodeTreeView.Name = "CodeTreeView";
            this.CodeTreeView.Size = new System.Drawing.Size(707, 422);
            treeViewStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
            treeViewStyle1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(51)))), ((int)(((byte)(161)))));
            treeViewStyle1.FadeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(193)))), ((int)(((byte)(255)))));
            treeViewStyle1.FillStyle = PureComponents.TreeView.FillStyle.VerticalCentreFading;
            treeViewStyle1.FlashColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(108)))));
            treeViewStyle1.FullRowSelect = false;
            treeViewStyle1.HighlightSelectedPath = false;
            treeViewStyle1.LineStyle = PureComponents.TreeView.LineStyle.Dot;
            checkBoxStyle1.BorderColor = System.Drawing.Color.CornflowerBlue;
            checkBoxStyle1.BorderStyle = PureComponents.TreeView.CheckBoxBorderStyle.Solid;
            checkBoxStyle1.CheckColor = System.Drawing.Color.RoyalBlue;
            checkBoxStyle1.HoverBackColor = System.Drawing.Color.LightSteelBlue;
            checkBoxStyle1.HoverBorderColor = System.Drawing.Color.RoyalBlue;
            checkBoxStyle1.HoverCheckColor = System.Drawing.Color.Black;
            nodeStyle1.CheckBoxStyle = checkBoxStyle1;
            expandBoxStyle1.BackColor = System.Drawing.Color.LightBlue;
            expandBoxStyle1.ForeColor = System.Drawing.Color.Crimson;
            nodeStyle1.ExpandBoxStyle = expandBoxStyle1;
            nodeStyle1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nodeStyle1.HoverBackColor = System.Drawing.Color.LightCoral;
            nodeStyle1.SelectedBackColor = System.Drawing.Color.LightSalmon;
            nodeStyle1.SelectedBorderColor = System.Drawing.Color.CornflowerBlue;
            nodeStyle1.SelectedFillStyle = PureComponents.TreeView.FillStyle.VistaFading;
            nodeStyle1.SelectedForeColor = System.Drawing.Color.RoyalBlue;
            nodeTooltipStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
            nodeTooltipStyle1.BorderColor = System.Drawing.Color.RoyalBlue;
            nodeTooltipStyle1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nodeStyle1.TooltipStyle = nodeTooltipStyle1;
            nodeStyle1.UnderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeViewStyle1.NodeStyle = nodeStyle1;
            treeViewStyle1.ShowSubitemsIndicator = true;
            this.CodeTreeView.Style = treeViewStyle1;
            this.CodeTreeView.TabIndex = 25;
            // 
            // frmShowParamCRev
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(731, 513);
            this.Controls.Add(this.MyPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MinimizeBox = false;
            this.Name = "frmShowParamCRev";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "درخت شروط و نتایج ثبت شده";
            this.MyPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CodeTreeView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx MyPanel;
        private DevComponents.DotNetBar.ButtonX btnClose;
        internal PureComponents.TreeView.TreeViewPathSelector TreeViewNavigator;
        public PureComponents.TreeView.TreeView CodeTreeView;
    }
}