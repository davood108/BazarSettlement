﻿using ConditionTreeControl=MohanirPouya.Forms.Management.ManageParameters.Classes.ConditionTreeControl;

namespace MohanirPouya.Forms.Management.ManageParameters.ParametersC
{
    partial class frmNewParameterC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.txtParamName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtParamDescribe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnValidation = new DevComponents.DotNetBar.ButtonX();
            this.btnSaveAndClose = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.lblParamName = new System.Windows.Forms.Label();
            this.lblParamDescribe = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.PCreationDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.lblBeginDate = new System.Windows.Forms.Label();
            this.TreeViewCode = new ConditionTreeControl();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.lblSubtitle = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.MainPanel.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MyToolTip
            // 
            this.MyToolTip.AutoPopDelay = 10000;
            this.MyToolTip.InitialDelay = 500;
            this.MyToolTip.IsBalloon = true;
            this.MyToolTip.ReshowDelay = 100;
            this.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MyToolTip.ToolTipTitle = "راهنمایی";
            // 
            // txtParamName
            // 
            this.txtParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParamName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtParamName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtParamName.Border.Class = "TextBoxBorder";
            this.txtParamName.Location = new System.Drawing.Point(672, 91);
            this.txtParamName.MaxLength = 50;
            this.txtParamName.Name = "txtParamName";
            this.txtParamName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtParamName.Size = new System.Drawing.Size(147, 21);
            this.txtParamName.TabIndex = 15;
            this.MyToolTip.SetToolTip(this.txtParamName, "ورود نام سرفصل اجباری می باشد. بدون ذكر فاصله نامی به انگلیسی وارد نمایید");
            // 
            // txtParamDescribe
            // 
            this.txtParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtParamDescribe.Border.Class = "TextBoxBorder";
            this.txtParamDescribe.Location = new System.Drawing.Point(12, 91);
            this.txtParamDescribe.MaxLength = 100;
            this.txtParamDescribe.Name = "txtParamDescribe";
            this.txtParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtParamDescribe.Size = new System.Drawing.Size(551, 21);
            this.txtParamDescribe.TabIndex = 18;
            this.txtParamDescribe.Text = "توضیح پیرامون پارامتر";
            this.MyToolTip.SetToolTip(this.txtParamDescribe, "متنی برای ثبت توضیحات پیرامون پارامتر");
            // 
            // btnValidation
            // 
            this.btnValidation.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnValidation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnValidation.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnValidation.Location = new System.Drawing.Point(137, 474);
            this.btnValidation.Name = "btnValidation";
            this.btnValidation.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnValidation.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5);
            this.btnValidation.Size = new System.Drawing.Size(119, 28);
            this.btnValidation.TabIndex = 20;
            this.btnValidation.Text = "بررسی دستورات <b><font color=\"#ED1C24\">(F5)</font></b>";
            this.btnValidation.Tooltip = "بررسی دستورات وارد شده";
            this.btnValidation.Click += new System.EventHandler(this.btnValidation_Click);
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveAndClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveAndClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSaveAndClose.Location = new System.Drawing.Point(12, 474);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSaveAndClose.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F8);
            this.btnSaveAndClose.Size = new System.Drawing.Size(119, 28);
            this.btnSaveAndClose.TabIndex = 22;
            this.btnSaveAndClose.Text = "ذخیره و تایید <b><font color=\"#ED1C24\">(F8)</font></b>";
            this.btnSaveAndClose.Tooltip = "ذخیره نمودن پارامتر و بست فرم ایجاد پارامتر";
            this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(700, 474);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(119, 28);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Tooltip = "انصرف از طراحی پارامتر و خروج از فرم";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblParamName
            // 
            this.lblParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamName.AutoSize = true;
            this.lblParamName.Location = new System.Drawing.Point(737, 75);
            this.lblParamName.Name = "lblParamName";
            this.lblParamName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamName.Size = new System.Drawing.Size(78, 13);
            this.lblParamName.TabIndex = 26;
            this.lblParamName.Text = "نام (انگلیسی):";
            // 
            // lblParamDescribe
            // 
            this.lblParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamDescribe.AutoSize = true;
            this.lblParamDescribe.Location = new System.Drawing.Point(515, 75);
            this.lblParamDescribe.Name = "lblParamDescribe";
            this.lblParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamDescribe.Size = new System.Drawing.Size(51, 13);
            this.lblParamDescribe.TabIndex = 23;
            this.lblParamDescribe.Text = "توضیحات:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(707, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.PCreationDate);
            this.MainPanel.Controls.Add(this.lblBeginDate);
            this.MainPanel.Controls.Add(this.TreeViewCode);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Controls.Add(this.lblParamName);
            this.MainPanel.Controls.Add(this.btnValidation);
            this.MainPanel.Controls.Add(this.btnSaveAndClose);
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.txtParamDescribe);
            this.MainPanel.Controls.Add(this.txtParamName);
            this.MainPanel.Controls.Add(this.lblParamDescribe);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(829, 514);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 27;
            // 
            // PCreationDate
            // 
            this.PCreationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PCreationDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.PCreationDate.IsNull = false;
            this.PCreationDate.Location = new System.Drawing.Point(569, 92);
            this.PCreationDate.Name = "PCreationDate";
            this.PCreationDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PCreationDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.PCreationDate.Size = new System.Drawing.Size(100, 20);
            this.PCreationDate.TabIndex = 67;
            this.PCreationDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBeginDate.AutoSize = true;
            this.lblBeginDate.Location = new System.Drawing.Point(592, 75);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBeginDate.Size = new System.Drawing.Size(80, 13);
            this.lblBeginDate.TabIndex = 66;
            this.lblBeginDate.Text = "تاریخ آغاز اعتبار:";
            // 
            // TreeViewCode
            // 
            this.TreeViewCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeViewCode.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.TreeViewCode.Location = new System.Drawing.Point(12, 118);
            this.TreeViewCode.Name = "TreeViewCode";
            this.TreeViewCode.PartName = null;
            this.TreeViewCode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TreeViewCode.Size = new System.Drawing.Size(807, 350);
            this.TreeViewCode.TabIndex = 43;
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.lblSubtitle);
            this.TopPanel.Controls.Add(this.lblTitle);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(829, 69);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.TopPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 42;
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubtitle.Font = new System.Drawing.Font("B Yekan", 14.25F);
            this.lblSubtitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblSubtitle.Location = new System.Drawing.Point(3, 36);
            this.lblSubtitle.Name = "lblSubtitle";
            this.lblSubtitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSubtitle.Size = new System.Drawing.Size(823, 29);
            this.lblSubtitle.TabIndex = 29;
            this.lblSubtitle.Text = "ایجاد پارامتر شرطی جدید";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(823, 37);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "خدمات انتقال - خط";
            // 
            // frmNewParameterC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(829, 514);
            this.Controls.Add(this.MainPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MinimizeBox = false;
            this.Name = "frmNewParameterC";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم ایجاد پارامتر شرطی جدید";
            this.Load += new System.EventHandler(this.Form_Load);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip MyToolTip;
        private DevComponents.DotNetBar.Controls.TextBoxX txtParamName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtParamDescribe;
        private DevComponents.DotNetBar.ButtonX btnValidation;
        private DevComponents.DotNetBar.ButtonX btnSaveAndClose;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private System.Windows.Forms.Label lblParamName;
        private System.Windows.Forms.Label lblParamDescribe;
        private System.Windows.Forms.Button button1;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private System.Windows.Forms.Label lblSubtitle;
        private System.Windows.Forms.Label lblTitle;
        private ConditionTreeControl TreeViewCode;
        public RPNCalendar.UI.Controls.PersianDatePicker PCreationDate;
        private System.Windows.Forms.Label lblBeginDate;
    }
}