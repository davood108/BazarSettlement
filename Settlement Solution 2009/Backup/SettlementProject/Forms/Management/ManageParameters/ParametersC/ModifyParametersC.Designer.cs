﻿using System.Windows.Forms;
using RPNCalendar.UI.Controls;
using ConditionTreeControl=MohanirPouya.Forms.Management.ManageParameters.Classes.ConditionTreeControl;

namespace MohanirPouya.Forms.Management.ManageParameters.ParametersC
{
    partial class frmModifyParametersC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.MyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.txtParamDescribe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnValidation = new DevComponents.DotNetBar.ButtonX();
            this.btnSaveAndClose = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.lblCurrentParamName = new System.Windows.Forms.Label();
            this.lblParamDescribe = new System.Windows.Forms.Label();
            this.DataSetLog = new System.Data.DataSet();
            this.ParametersLog = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.CCreationDate = new System.Data.DataColumn();
            this.CStartDate = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.txtCurrentParamName = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.btnDeActive = new DevComponents.DotNetBar.ButtonX();
            this.dgvLog = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colmun8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colmun5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PCreationDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.lblBeginDate = new System.Windows.Forms.Label();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.lblSubtitle = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.TreeViewCodeM = new MohanirPouya.Forms.Management.ManageParameters.Classes.ConditionTreeControl();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).BeginInit();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLog)).BeginInit();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MyToolTip
            // 
            this.MyToolTip.AutoPopDelay = 10000;
            this.MyToolTip.InitialDelay = 500;
            this.MyToolTip.IsBalloon = true;
            this.MyToolTip.ReshowDelay = 100;
            this.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MyToolTip.ToolTipTitle = "راهنمایی";
            // 
            // txtParamDescribe
            // 
            this.txtParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtParamDescribe.Border.Class = "TextBoxBorder";
            this.txtParamDescribe.Location = new System.Drawing.Point(12, 91);
            this.txtParamDescribe.MaxLength = 100;
            this.txtParamDescribe.Name = "txtParamDescribe";
            this.txtParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtParamDescribe.Size = new System.Drawing.Size(551, 21);
            this.txtParamDescribe.TabIndex = 30;
            this.MyToolTip.SetToolTip(this.txtParamDescribe, "متنی برای ثبت توضیحات پیرامون پارامتر");
            // 
            // btnValidation
            // 
            this.btnValidation.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnValidation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnValidation.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnValidation.Location = new System.Drawing.Point(137, 639);
            this.btnValidation.Name = "btnValidation";
            this.btnValidation.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnValidation.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5);
            this.btnValidation.Size = new System.Drawing.Size(119, 28);
            this.btnValidation.TabIndex = 32;
            this.btnValidation.Text = "بررسی دستورات <b><font color=\"#ED1C24\">(F5)</font></b>";
            this.btnValidation.Tooltip = "بررسی دستورات وارد شده";
            this.btnValidation.Click += new System.EventHandler(this.btnValidation_Click);
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveAndClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveAndClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSaveAndClose.Location = new System.Drawing.Point(12, 639);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSaveAndClose.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F8);
            this.btnSaveAndClose.Size = new System.Drawing.Size(119, 28);
            this.btnSaveAndClose.TabIndex = 34;
            this.btnSaveAndClose.Text = "ذخیره و تایید <b><font color=\"#ED1C24\">(F8)</font></b>";
            this.btnSaveAndClose.Tooltip = "ذخیره نمودن پارامتر و بست فرم ایجاد پارامتر";
            this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(701, 639);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(119, 28);
            this.btnCancel.TabIndex = 33;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Tooltip = "انصرف از طراحی پارامتر و خروج از فرم";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblCurrentParamName
            // 
            this.lblCurrentParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentParamName.AutoSize = true;
            this.lblCurrentParamName.Location = new System.Drawing.Point(744, 75);
            this.lblCurrentParamName.Name = "lblCurrentParamName";
            this.lblCurrentParamName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCurrentParamName.Size = new System.Drawing.Size(78, 13);
            this.lblCurrentParamName.TabIndex = 38;
            this.lblCurrentParamName.Text = "نام (انگلیسی):";
            // 
            // lblParamDescribe
            // 
            this.lblParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamDescribe.AutoSize = true;
            this.lblParamDescribe.Location = new System.Drawing.Point(515, 75);
            this.lblParamDescribe.Name = "lblParamDescribe";
            this.lblParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamDescribe.Size = new System.Drawing.Size(51, 13);
            this.lblParamDescribe.TabIndex = 35;
            this.lblParamDescribe.Text = "توضیحات:";
            // 
            // DataSetLog
            // 
            this.DataSetLog.DataSetName = "ParametersLog";
            this.DataSetLog.Tables.AddRange(new System.Data.DataTable[] {
            this.ParametersLog});
            // 
            // ParametersLog
            // 
            this.ParametersLog.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.CCreationDate,
            this.CStartDate,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.ParametersLog.TableName = "ParametersLog";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "ردیف";
            this.dataColumn1.ColumnName = "Revision";
            this.dataColumn1.DataType = typeof(int);
            // 
            // CCreationDate
            // 
            this.CCreationDate.Caption = "تاریخ ایجاد";
            this.CCreationDate.ColumnName = "CreationDate";
            this.CCreationDate.DataType = typeof(System.DateTime);
            this.CCreationDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // CStartDate
            // 
            this.CStartDate.Caption = "تاریخ آغاز";
            this.CStartDate.ColumnName = "StartDate";
            this.CStartDate.DataType = typeof(System.DateTime);
            this.CStartDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "تاریخ اتمام اعتبار";
            this.dataColumn4.ColumnName = "ExpirationDate";
            this.dataColumn4.DataType = typeof(System.DateTime);
            this.dataColumn4.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "متن كد پارامتر";
            this.dataColumn5.ColumnName = "SelectString";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "نام كاربر";
            this.dataColumn6.ColumnName = "UserID";
            // 
            // txtCurrentParamName
            // 
            this.txtCurrentParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurrentParamName.BackColor = System.Drawing.Color.White;
            this.txtCurrentParamName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtCurrentParamName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtCurrentParamName.Location = new System.Drawing.Point(671, 91);
            this.txtCurrentParamName.Name = "txtCurrentParamName";
            this.txtCurrentParamName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCurrentParamName.Size = new System.Drawing.Size(149, 20);
            this.txtCurrentParamName.TabIndex = 40;
            this.txtCurrentParamName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ساختار";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Text = "مشاهده";
            this.Column1.ToolTipText = "مشاهده ساختار تعریف شده برای تاریخ ذكر شده";
            this.Column1.UseColumnTextForButtonValue = true;
            this.Column1.Width = 90;
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.TreeViewCodeM);
            this.MainPanel.Controls.Add(this.btnDeActive);
            this.MainPanel.Controls.Add(this.dgvLog);
            this.MainPanel.Controls.Add(this.PCreationDate);
            this.MainPanel.Controls.Add(this.lblBeginDate);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Controls.Add(this.lblCurrentParamName);
            this.MainPanel.Controls.Add(this.btnValidation);
            this.MainPanel.Controls.Add(this.txtCurrentParamName);
            this.MainPanel.Controls.Add(this.btnSaveAndClose);
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.txtParamDescribe);
            this.MainPanel.Controls.Add(this.lblParamDescribe);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(829, 679);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 41;
            // 
            // btnDeActive
            // 
            this.btnDeActive.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeActive.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnDeActive.Location = new System.Drawing.Point(262, 639);
            this.btnDeActive.Name = "btnDeActive";
            this.btnDeActive.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDeActive.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnDeActive.Size = new System.Drawing.Size(117, 28);
            this.btnDeActive.TabIndex = 71;
            this.btnDeActive.Text = "غیرفعال<b><font color=\"#ED1C24\">(F3)</font></b>";
            this.btnDeActive.Tooltip = "غیرفعال نمودن ویرایش انتخاب شده";
            this.btnDeActive.Click += new System.EventHandler(this.btnDeActive_Click);
            // 
            // dgvLog
            // 
            this.dgvLog.AllowUserToAddRows = false;
            this.dgvLog.AllowUserToDeleteRows = false;
            this.dgvLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLog.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.dgvLog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLog.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column3,
            this.Column7,
            this.colmun8,
            this.colmun5,
            this.Column4});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLog.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLog.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLog.Location = new System.Drawing.Point(12, 490);
            this.dgvLog.Name = "dgvLog";
            this.dgvLog.ReadOnly = true;
            this.dgvLog.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvLog.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.NullValue = "-";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvLog.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLog.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Transparent;
            this.dgvLog.RowTemplate.Height = 24;
            this.dgvLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLog.Size = new System.Drawing.Size(808, 141);
            this.dgvLog.TabIndex = 70;
            this.dgvLog.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvLog_CellMouseClick);
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Revision";
            this.Column2.HeaderText = "ردیف";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CreationDate";
            this.Column3.HeaderText = "تاریخ تولید پارامتر";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "UserID";
            this.Column7.HeaderText = "کاربر تعریف کننده";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // colmun8
            // 
            this.colmun8.DataPropertyName = "StartDate";
            this.colmun8.HeaderText = "تاریخ آغاز اعتبار";
            this.colmun8.Name = "colmun8";
            this.colmun8.ReadOnly = true;
            // 
            // colmun5
            // 
            this.colmun5.DataPropertyName = "EndDate";
            this.colmun5.HeaderText = "تاریخ پایان آعتبار";
            this.colmun5.Name = "colmun5";
            this.colmun5.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Active";
            this.Column4.HeaderText = "غیرفعال";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // PCreationDate
            // 
            this.PCreationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PCreationDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.PCreationDate.IsNull = false;
            this.PCreationDate.Location = new System.Drawing.Point(569, 92);
            this.PCreationDate.Name = "PCreationDate";
            this.PCreationDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PCreationDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.PCreationDate.Size = new System.Drawing.Size(100, 20);
            this.PCreationDate.TabIndex = 69;
            this.PCreationDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBeginDate.AutoSize = true;
            this.lblBeginDate.Location = new System.Drawing.Point(589, 75);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBeginDate.Size = new System.Drawing.Size(80, 13);
            this.lblBeginDate.TabIndex = 68;
            this.lblBeginDate.Text = "تاریخ آغاز اعتبار:";
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.lblSubtitle);
            this.TopPanel.Controls.Add(this.lblTitle);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(829, 69);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.TopPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 43;
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubtitle.Font = new System.Drawing.Font("B Yekan", 14.25F);
            this.lblSubtitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblSubtitle.Location = new System.Drawing.Point(3, 36);
            this.lblSubtitle.Name = "lblSubtitle";
            this.lblSubtitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSubtitle.Size = new System.Drawing.Size(820, 29);
            this.lblSubtitle.TabIndex = 29;
            this.lblSubtitle.Text = "ویرایش پارامتر شرطی";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblTitle.Location = new System.Drawing.Point(3, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(823, 37);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "خدمات انتقال - خط";
            // 
            // TreeViewCodeM
            // 
            this.TreeViewCodeM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeViewCodeM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.TreeViewCodeM.Location = new System.Drawing.Point(13, 118);
            this.TreeViewCodeM.Name = "TreeViewCodeM";
            this.TreeViewCodeM.PartName = null;
            this.TreeViewCodeM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TreeViewCodeM.Size = new System.Drawing.Size(807, 366);
            this.TreeViewCodeM.TabIndex = 72;
            // 
            // frmModifyParametersC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(829, 679);
            this.Controls.Add(this.MainPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(837, 548);
            this.Name = "frmModifyParametersC";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم ویرایش پارامتر شرطی";
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).EndInit();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLog)).EndInit();
            this.TopPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ToolTip MyToolTip;
        private DevComponents.DotNetBar.ButtonX btnValidation;
        private DevComponents.DotNetBar.ButtonX btnSaveAndClose;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private Label lblCurrentParamName;
        private Label lblParamDescribe;
        private DevComponents.DotNetBar.Controls.TextBoxX txtParamDescribe;
        private System.Data.DataSet DataSetLog;
        private System.Data.DataTable ParametersLog;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn CCreationDate;
        private System.Data.DataColumn CStartDate;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private Label txtCurrentParamName;

        private DataGridViewButtonColumn Column1;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private Label lblSubtitle;
        private Label lblTitle;
        private Label lblBeginDate;
        public PersianDatePicker PCreationDate;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvLog;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column7;
        private DataGridViewTextBoxColumn colmun8;
        private DataGridViewTextBoxColumn colmun5;
        private DataGridViewCheckBoxColumn Column4;
        private DevComponents.DotNetBar.ButtonX btnDeActive;
        private ConditionTreeControl TreeViewCodeM;
    }
}