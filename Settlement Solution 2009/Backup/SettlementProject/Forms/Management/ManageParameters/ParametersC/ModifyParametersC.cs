﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.ParametersC
{
    /// <summary>
    /// كلاس مدیریت فرم ویرایش پارامتر ها
    /// </summary>
    public partial class frmModifyParametersC : Form
    {

        #region Fields

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر ها
        /// </summary>
        private DataTable _DataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _ArrayListParams;
        #endregion

        #region readonly String _CurrentParamName
        /// <summary>
        /// نام پارامتری كه در حال ویرایش است
        /// </summary>
        private readonly String _CurrentParamName;
        #endregion

        #region Int32 _CurrentParamRevision
        /// <summary>
        /// شماره آخرین پارامتر ثبت شده
        /// </summary>
        private Int32 _CurrentParamRevision;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر
        /// </summary>
        private readonly String _ParamType;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش تعیین شده
        /// </summary>
        private readonly String _PartName;
        #endregion

        #region String _CurrentParamXml

        /// <summary>
        /// ایكس ام ال پارامتر جاری
        /// </summary>
        private String _CurrentParamXml;

        //    private PersianDatePicker startDateDataGridViewTextBoxColumn1;

        #endregion

        private readonly string _UserID;
        private string Cdate;
        private readonly DbSMDataContext _DbSM;
        private DataTable dataLogtable;
        private string CommandText;
        private SqlConnection MySqlConnection;
        private SqlCommand MySqlCommand;
        private int _StartDateCount;
        private string TestingQuery;
        private bool TestState;

        #endregion

        #region Constructor
        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="ParamName">نام پارامتر برای ویرایش</param>
        /// <param name="PartName">نام بخش شامل فروشندگان ، خریداران و غیره</param>
        /// <param name="ParamType">نوع پارامتر از قبیل پارامتر شرطی ، سرفصل و غیره</param>
        public frmModifyParametersC(String ParamName, String PartName, String ParamType)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
(DbBizClass.dbConnStr);

            _UserID = DbBizClass.CurrentUserLastName;

            

            _CurrentParamName = ParamName;
            _PartName = PartName;
            TreeViewCodeM.PartName = PartName;
            _ParamType = ParamType;

            #region Set Form Title
            if (_PartName == "Sellers") lblTitle.Text = "فروشندگان";
            else if (_PartName == "Buyers") lblTitle.Text = "خریداران";
            else if (_PartName == "Transfer") lblTitle.Text = "خدمات انتقال";
            if (_ParamType == "Pc")
            {
                // ReSharper disable DoNotCallOverridableMethodsInConstructor
                Text = "فرم ویرایش پارامتر شرطی";
                // ReSharper restore DoNotCallOverridableMethodsInConstructor
                lblSubtitle.Text = "ویرایش پارامتر شرطی";
            }
            else if (_ParamType == "Sa")
            {
                // ReSharper disable DoNotCallOverridableMethodsInConstructor
                Text = "فرم ویرایش سرفصل";
                // ReSharper restore DoNotCallOverridableMethodsInConstructor
                lblSubtitle.Text = "ویرایش سرفصل";
            }
            else if (_ParamType == "Sh")
            {
                // ReSharper disable DoNotCallOverridableMethodsInConstructor
                Text = "فرم ویرایش شاخص";
                // ReSharper restore DoNotCallOverridableMethodsInConstructor
                lblSubtitle.Text = "ویرایش شاخص";
            }
            #endregion

            #region SetTime
            PersianDate OccuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            PCreationDate.Text = OccuredDate.ToString();
            #endregion

            ShowDialog();
        }
        #endregion

        #region Events Handlers

        #region Form Load
        /// <summary>
        /// روال مدیریت آغاز فراخوانی فرم
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            
            FillParametersTable();
            SetParamLastDataByName("F",10000);
            TreeViewCodeM.FillTreeView(_CurrentParamXml);
            FillLogDataGrid();
        }
        #endregion

        #region dgvLog_CellMouseClick

        /// <summary>
        /// روال مدیریت كلیك بر روی جدول جزئیات
        /// </summary>
        private void dgvLog_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SetParamLastDataByName("E", Convert.ToInt16(dgvLog.Rows[e.RowIndex].Cells[0].Value));
            TreeViewCodeM.FillTreeView(_CurrentParamXml);
        }
        #endregion

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void btnValidation_Click(object sender, EventArgs e)
        {
            #region Get Line Condition String From TreeView Nodes
            String LineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCodeM.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(LineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion

     
          TestingQuery = ManageParametersC.GetParamString(_PartName,
                                                                   _CurrentParamName,LineConditionString, _DataTableParams, _ArrayListParams, true,_CurrentParamRevision);

     

            if (!String.IsNullOrEmpty(TestingQuery)) new frmTestParameters(TestingQuery);
        }
        #endregion

        #region btnSaveAndClose_Click
        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {

            #region Check First Day of Month choose
            if ((PCreationDate.Text.Substring(8, 2)) != "01")
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
                           MessageBoxButtons.OK, MessageBoxIcon.Error,
                           MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion

            #region Check Same Date in DataBase
            #region Prepare SqlCommand

            CommandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _PartName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName and StartDate=@StartDate and Active=0";
            MySqlConnection =
               new SqlConnection(DbBizClass.dbConnStr);
            MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;
            MySqlCommand.Parameters.Add("@StartDate", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@StartDate"].Value = PCreationDate.Text;
            #endregion

            #region Execute SqlCommand

            try
            {
                MySqlCommand.Connection.Open();
                SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                if (MySqlDataReader != null)
                {
                    MySqlDataReader.Read();
                    _StartDateCount = Convert.ToInt32(MySqlDataReader[0].ToString());



                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion

            if (_StartDateCount > 0)
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار تکراری می باشد.باید ابتدا ویرایش موجود غیر فعال شود !", "خطا",
           MessageBoxButtons.OK, MessageBoxIcon.Error,
           MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion

            #region Get Current Param Revision

            #region Prepare SqlCommand

            CommandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _PartName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName";
            MySqlConnection =
               new SqlConnection(DbBizClass.dbConnStr);
            MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

            #endregion

            #region Execute SqlCommand

            try
            {
                MySqlCommand.Connection.Open();
                SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                if (MySqlDataReader != null)
                {
                    MySqlDataReader.Read();
                    _CurrentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());



                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion

            #endregion

            #region Set Current Date
            PersianDate OccuredDate1 = PersianDateConverter.ToPersianDate
(DateTime.Now);
            Cdate = OccuredDate1.ToString();
            #endregion

            #region Check User Data Entry
            if (CheckDescriptionTextBox() == false) return;
            #endregion

            #region Get Line Condition String From TreeView Nodes
            String LineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCodeM.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(LineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return;
            }
            #endregion
            
            #region Testing
         TestingQuery = ManageParametersC.GetParamString(_PartName, _CurrentParamName, LineConditionString, _DataTableParams, _ArrayListParams, true, 0);

         if (!String.IsNullOrEmpty(TestingQuery))
             TestState = CheckTestingQuery(TestingQuery);
         if (TestState == false)
         {
             PersianMessageBox.Show("در دستور وارد شده خطایی وجود دارد!" +
                    "كدهای وارد شده را مجددا بررسی نمایید", "خطا!", MessageBoxButtons.OK,
                    MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
             return;
         }
         #endregion

            #region Creating Parameter View By SQL Query
            if (ManageParametersC.CreateParameterTable(_DataTableParams,
                                                      _ArrayListParams, _CurrentParamName, txtParamDescribe.Text,_CurrentParamRevision, LineConditionString, _UserID,_PartName, _ParamType, TreeViewCodeM.CodeTreeView, TestingQuery,PCreationDate.Text,Cdate) == false) return;
            #endregion

            FillLogDataGrid();
        }
        #endregion

        #region btn Deactive
        private void btnDeActive_Click(object sender, EventArgs e)
        {
            if ((bool)dgvLog.SelectedRows[0].Cells[5].Value)
            {
                return;
            }


            DialogResult QResult =
       PersianMessageBox.Show("آيا از اين كار اطمينان داريد؟\n " +
                              "در صورت تایید ، این ویرایش از متغیر دیگر قابل دسترس نخواهد بود!", "هشدار!",
                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                              MessageBoxDefaultButton.Button1);
            if (QResult == DialogResult.No)
            {
                return;
            }


            _DbSM.Param_Deactivation(Convert.ToInt16(dgvLog.SelectedRows[0].Cells[0].Value), _CurrentParamName, _PartName);
            FillLogDataGrid();
        }
        #endregion

        #region btnCancel Click

        /// <summary>
        /// روال انصراف از تغییر پارامتر
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()
        /// <summary>
        /// بدست آوردن ليست كلیه پارامترها ، پارامترهای شرطی ، سرفصلها و شاخص ها
        /// </summary>
        private void FillParametersTable()
        {
            _DataTableParams = Classes.Parameters.GetAllParamsByPart(_PartName);
            _ArrayListParams =
                Classes.Parameters.GetParamsNameByTable(_DataTableParams);
        }
        #endregion

        #region void SetParamLastDataByName()
        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void SetParamLastDataByName(string param, int Rev)
        {
            txtCurrentParamName.Text = _CurrentParamName;

            if (param == "F")
            {
                #region Get SelectString (XML)

                #region Prepare SqlCommand

                 CommandText = "SELECT " +
                                     "SelectString " +
                                     "FROM " + _PartName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName ORDER BY StartDate  DESC";
               MySqlConnection =
                    new SqlConnection(DbBizClass.dbConnStr);
             MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
                MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    MySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();
                   

                        _CurrentParamXml = MySqlDataReader[0].ToString();
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    MySqlCommand.Connection.Close();
                }

                #endregion

                #endregion

                #region Get Current Param Revision

                #region Prepare SqlCommand

                CommandText = "SELECT " +
                                      " Count(ID) " +
                                      "FROM " + _PartName + ".ParametersLog " +
                                      "WHERE EnglishName = @ParamName";
                MySqlConnection =
                   new SqlConnection(DbBizClass.dbConnStr);
                MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
                MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    MySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();
                        _CurrentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());



                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    MySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            else
            {
                #region Get Current Param Revision , BeginDate , EndDate , SelectString (XML)

                #region Prepare SqlCommand

            CommandText = "SELECT  " +
                                         " SelectString " +
                                         "FROM " + _PartName + ".ParametersLog " +
                                         "WHERE EnglishName = @ParamName and Revision=@Rev ORDER BY StartDate  DESC";
                 MySqlConnection =
                    new SqlConnection(DbBizClass.dbConnStr);
                MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
                MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;
                MySqlCommand.Parameters.Add("@Rev", SqlDbType.Int);
                MySqlCommand.Parameters["@Rev"].Value = Rev;

                #endregion

                #region Execute SqlCommand

                try
                {
                    MySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();
 
                        _CurrentParamXml = MySqlDataReader[0].ToString();
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    MySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            txtParamDescribe.Text = Classes.Parameters.GetParamDescription
                (_CurrentParamName, _DataTableParams);
        }

        #endregion

        #region void FillLogDataGrid()

        /// <summary>
        /// تمكیل آیتم های جدول سابقه تغییرات پارامتر
        /// </summary>
        private void FillLogDataGrid()
        {
            dgvLog.AutoGenerateColumns = false;
            dataLogtable = new DataTable();

            #region Prepare SqlCommand

            CommandText =
                  "SELECT Revision  , " +
                  "CreationDate,UserID,Startdate,Enddate,'متن پارامتر' as Selectstring,Active   " +
                  "FROM " + _PartName + ".ParametersLog " +
                  "WHERE EnglishName = @ParamName ORDER BY StartDate  DESC";
            MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
            MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 50);
            MySqlCommand.Parameters["@ParamName"].Value = _CurrentParamName;

            #endregion

            #region Execute SqlCommand

            try
            {
                MySqlCommand.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                dataLogtable.Load(MySqlCommand.ExecuteReader());
                dgvLog.DataSource = dataLogtable;
                // ReSharper restore AssignNullToNotNullAttribute
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }

            #endregion

            #region Change Deactive revision backcolor
            for (int i = 0; i < dgvLog.Rows.Count; i++)
            {

                if ((bool)dgvLog.Rows[i].Cells[5].Value)
                {
                    dgvLog.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                }
            }
            #endregion
        }

        #endregion

        #region Boolean CheckDescriptionTextBox()

        /// <summary>
        /// بررسي جعبه متن توضيحات پارامتر
        /// </summary>
        private Boolean CheckDescriptionTextBox()
        {
            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                txtParamDescribe.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var ParameterResult = new DataTable();

            var MySqlConnection =
                new SqlConnection(DbBizClass.dbConnStr);
            var MySqlCommand = new SqlCommand(query, MySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                MySqlCommand.Connection.Open();

                // ReSharper disable AssignNullToNotNullAttribute
                ParameterResult.Load(MySqlCommand.ExecuteReader());
                // ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion
            return true;

        }
        #endregion

  #endregion

    }
}