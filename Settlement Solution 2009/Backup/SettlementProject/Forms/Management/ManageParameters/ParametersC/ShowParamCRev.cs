﻿#region using
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using PureComponents.TreeView;
#endregion

namespace MohanirPouya.Forms.Options.ManageParameters.ParametersC
{
    /// <summary>
    /// فرمی برای نمایش درخت سرفصل ها
    /// </summary>
    public partial class frmShowParamCRev : Form
    {

        #region Constructors
        /// <summary>
        /// سازنده پیش فرض كلاس
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="PartName">نام بخش از قبیل فروشندگان ، خریداران و غیره</param>
        public frmShowParamCRev(String ParamName, Int32 Revision , String PartName)
        {
            InitializeComponent();
            ShowQueries(ParamName, Revision, PartName);
        }
        #endregion

        #region Methods

        #region void ShowQueries(String ParamName, Int32 Revision , String PartName)
        /// <summary>
        /// نمایش اطلاعات مربوط به پارامتر در درخت
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="PartName">دوره پارامتر</param>
        private void ShowQueries(String ParamName, Int32 Revision , String PartName)
        {
            #region Prepare SqlCommand
            String CommandText =
                "SELECT SelectString " +
                "FROM " + PartName + ".ParametersLog " +
                "WHERE EnglishName = @PartName AND Revision = @Revision";
            SqlCommand MyCommand = new SqlCommand(CommandText,
                new SqlConnection(MohanirPouya.Classes.DbBizClass.dbConnStr));
            MyCommand.Parameters.Add("@PartName", SqlDbType.NVarChar, 30);
            MyCommand.Parameters["@PartName"].Value = ParamName;
            MyCommand.Parameters.Add("@Revision", SqlDbType.Int);
            MyCommand.Parameters["@Revision"].Value = Revision;
            #endregion

            #region Execute SqlCommand
            try
            {
                MyCommand.Connection.Open();
                String ParamXmlString = MyCommand.ExecuteScalar().ToString();
                FillTreeView(ParamXmlString);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                Dispose();
            }
            finally
            {
                MyCommand.Connection.Close();
            }
            #endregion
        }
        #endregion

        #region + Filling TreeView Items Methods +

        #region public void FillTreeView(String CurrentParamXml)
        /// <summary>
        /// متدی برای تكمیل ساختار درخت از اطلاعات پارامتر جاری
        /// </summary>
        /// <param name="CurrentParamXml">رشته ایكس ام ال درخت</param>
        public void FillTreeView(String CurrentParamXml)
        {
            #region DeSerialize TreeView Data From XML String
            File.WriteAllText("TempXml.Xml", CurrentParamXml, Encoding.Unicode);
            try
            {
                CodeTreeView.LoadXml("TempXml.Xml");
                File.Delete("TempXml.Xml");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            #endregion
            SetTreeNodesProperties();
            CodeTreeView.ExpandAll();
        }
        #endregion

        #region private void SetTreeNodesProperties()
        /// <summary>
        /// متدی برای تنظیم رنگ عناصر درخت
        /// </summary>
        private void SetTreeNodesProperties()
        {
            foreach (Node CurrentNode in CodeTreeView.Nodes)
            {
                CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                if (CurrentNode.Tag.ToString() == "C") // For Conditions (C):
                {
                    CurrentNode.NodeStyle.Font = new Font(new FontFamily("Arial"), 14);
                    CurrentNode.NodeStyle.HoverBackColor = Color.PaleGreen;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Navy;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.GreenYellow;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.MediumBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.Blue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
                    if (CurrentNode.Nodes.Count > 0)
                        RecursiveSetTreeNodeColor(CurrentNode);
                }
                else // For Results (R):
                {
                    CurrentNode.NodeStyle.Font =
                        new Font(new FontFamily("Arial Narrow"), 14, FontStyle.Bold);
                    CurrentNode.NodeStyle.HoverBackColor = Color.LightCoral;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Red;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.LightSalmon;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.RoyalBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.CornflowerBlue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
                }
            }
        }
        #endregion

        #region private void RecursiveSetTreeNodeColor(Node TheNode)
        /// <summary>
        /// تابعی برای جستجوی بازگشتی عناصر درخت و تنظیم رنگ آنها
        /// </summary>
        /// <param name="TheNode">گره جاری برای جستجو</param>
        private static void RecursiveSetTreeNodeColor(Node TheNode)
        {
            foreach (Node CurrentNode in TheNode.Nodes)
            {
                CurrentNode.NodeStyleSource = NodeStyleSource.Local;
                if (CurrentNode.Tag.ToString() == "C") // For Conditions (C):
                {
                    CurrentNode.NodeStyle.Font = new Font(new FontFamily("Arial"), 14);
                    CurrentNode.NodeStyle.HoverBackColor = Color.PaleGreen;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Navy;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.GreenYellow;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.MediumBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.Blue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
                    if (CurrentNode.Nodes.Count > 0)
                        RecursiveSetTreeNodeColor(CurrentNode);
                }
                else // For Results (R):
                {
                    CurrentNode.NodeStyle.Font =
                        new Font(new FontFamily("Arial Narrow"), 14, FontStyle.Bold);
                    CurrentNode.NodeStyle.HoverBackColor = Color.LightCoral;
                    CurrentNode.NodeStyle.HoverForeColor = Color.Black;
                    CurrentNode.NodeStyle.ForeColor = Color.Red;
                    CurrentNode.NodeStyle.SelectedBackColor = Color.LightSalmon;
                    CurrentNode.NodeStyle.SelectedForeColor = Color.RoyalBlue;
                    CurrentNode.NodeStyle.SelectedBorderColor = Color.CornflowerBlue;
                    CurrentNode.NodeStyle.SelectedFillStyle = FillStyle.VistaFading;
                }
            }
        }
        #endregion

        #endregion

        #endregion
    
    }
}