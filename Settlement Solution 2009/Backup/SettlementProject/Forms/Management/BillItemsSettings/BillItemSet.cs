﻿#region using

using System;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirVBClasses;

#endregion

namespace MohanirPouya.Forms.Management.BillItemsSettings
{
    /// <summary>
    /// کلاس نمایش فرم تنظیم آیتم های صورت حساب
    /// </summary>
    public partial class BillItemSet : MohanirFormDialog
    {


        #region Field
        private readonly DbSMDataContext _DbSM;
        private readonly String _PartName;
        private IQueryable<int> ParamID1;
        private IQueryable<string> ParamName1;
        private IQueryable<string> ParamDesc1;
        private readonly string _BillType;

        #endregion

        #region Ctor

        public 
            BillItemSet(string PartName, string BillType)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                (DbBizClass.dbConnStr);
            dgvItems.AutoGenerateColumns = false;
            _PartName = PartName; // seller   Buyers  Transfer PowerPlant
            _BillType = BillType; ///   نیروگاهی یا به تفکیک واحد jqldkd
            lblTitle.Visible = false;
            lblSep.Visible = false;

            #region Set Form Title
            switch (_PartName)
            {
                case "Sellers":

                    if(_BillType=="P")
                    {
                      
                        label1.Text = "آیتم های صورتحساب فروش به صورت نیروگاهی";
                    }
                    if (_BillType == "U")
                    {
                        label1.Text = "آیتم های صورتحساب فروش به تفکیک واحد";
                    }

                    if (_BillType == "Z")
                    {
                        label1.Text = "آیتم های صورتحساب نیروگاههای تضمینی";
                    }

                    Text = "تنظیم پارامتر های آیتم های صورتحساب فروش ";
                    break;
                case "Buyers":
                      label1.Visible = false;
                        label2.Visible = true;
                        label2.Text = "آیتم های صورتحساب خرید ";
                        Text = "تنظیم پارامتر های آیتم های صورتحساب خرید ";
                   
                        label1.Visible = false;
                        label2.Visible = true;
                        label2.Text = "آیتم های صورتحساب خرید ";
                        Text = "تنظیم پارامتر های آیتم های صورتحساب خرید ";
                  
                    break;
                case "Transfer":
                    if (_BillType == "P")
                    {
                        label1.Visible = false;
                        label2.Visible = true;
                        label2.Text = "آیتم های صورتحساب خدمات انتقال (کلی)";
                        Text = "تنظیم پارامتر های آیتم های خدمات انتقال ";
                    }
                    if (_BillType == "D")
                    {
                        label1.Visible = false;
                        label2.Visible = true;
                        label2.Text = "آیتم های صورتحساب خدمات انتقال (جزئی)";
                        Text = "تنظیم پارامتر های آیتم های خدمات انتقال ";
                    }

                    break;
                case "PowerPlant":
                    label1.Visible = false;
                    label2.Visible = true;
                    label2.Text = "آیتم های صورتحساب نیروگاه ";
                    Text = "تنظیم پارامتر های آیتم های صورتحساب نیروگاه ";
                    break;
            }
            #endregion

            FillDataSource();
            ShowDialog();
        }

        #endregion

        #region Event Handlers

        #region Form Load

        private void frmSetParameters_Load(object sender, EventArgs e)
        {
        
        }

        #endregion

        #region dgvItems_SelectionChanged
        private void dgvItem_selectionChanged(object sender, EventArgs e)
        {
            if (dgvItems.SelectedCells.Count!=0)
            {
      
                ShowItemParameter(Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value));
            }

        }

        #endregion

        #region btnSetParameter_Click

        private void btnSetParameter_Click(object sender, EventArgs e)
        {
            Int32 ParamID =
                GetItemParamID(Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value), _PartName, _BillType);
            ShowChooseBillItemParameter(ParamID, _PartName);
            ShowItemParameter(Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value));
        }

        #endregion

        #region btnAddItem_Click
        private void btnAddItem_Click(object sender, EventArgs e)
        {
            dgvItems.AllowUserToAddRows = true;
        }
        #endregion
        
        #region btnApply_Click
        private void btnApply_Click(object sender, EventArgs e)
        {
            #region try
            try
            {

                _DbSM.SubmitChanges();
                dgvItems.AllowUserToAddRows = false;
            }
                #endregion

                #region Catch
            catch (Exception er)
            {
                const String ErrorMessage =
                    "خطادر واردكردن اطلاعات در بانك اطلاعات .\n" +
                    "موارد زیر را بررسی نمایید:\n" +
                    "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟\n" +
                    "2. آیا اطلاعات تمام گزینه ها را وارد نموده اید؟";
                PersianMessageBox.Show(ErrorMessage, "خطا!",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                MessageBox.Show(er.Message);
            }
            #endregion

        }
        #endregion

        #region btnDeleteParameter_Click
        private void btnDeleteParameter_Click(object sender, EventArgs e)
        {
            switch (_PartName)
            {
                case "Sellers":
                    #region Sellers
                    if (_BillType == "P")
                    {
                        #region Billtype "P"
                        var query = (from varx in _DbSM.BillItemParameters
                                     where varx.OrderNo == Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value)
                                           && varx.subject == 'S'
                                           && varx.TYpe == 'P'
                                     select varx);
                        if (query.Any())
                        {
                            foreach (var varx in query)
                            {
                                _DbSM.BillItemParameters.DeleteOnSubmit(varx);
                            }

                        }
                        _DbSM.SubmitChanges();

                        #endregion
                    }
                    if (_BillType == "U")
                    {

                        #region Billtype "U"
                        var query = (from varx in _DbSM.BillItemParameters
                                     where varx.OrderNo == Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value)
                                           && varx.subject == 'S'
                                           && varx.TYpe == 'U'
                                     select varx);
                        if (query.Any())
                        {
                            foreach (var varx in query)
                            {
                                _DbSM.BillItemParameters.DeleteOnSubmit(varx);
                            }

                        }
                        _DbSM.SubmitChanges();
                        #endregion
                    }
                    if (_BillType == "Z")
                    {
                        #region Billtype "Z"
                        var query = (from varx in _DbSM.BillItemParameters
                                     where varx.OrderNo == Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value)
                                           && varx.subject == 'S'
                                           && varx.TYpe == 'Z'
                                     select varx);
                        if (query.Any())
                        {
                            foreach (var varx in query)
                            {
                                _DbSM.BillItemParameters.DeleteOnSubmit(varx);
                            }

                        }
                        _DbSM.SubmitChanges();
                        #endregion
                    }
                    break;
                    #endregion
                case "Buyers":

                    #region Buyers

                    var queryb = (from varx in _DbSM.BillItemParameters
                                  where varx.OrderNo == Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value)
                                        && varx.subject == 'B'
                                        && varx.TYpe == 'B'
                                  select varx);
                    if (queryb.Any())
                    {
                        foreach (var varx in queryb)
                        {
                            _DbSM.BillItemParameters.DeleteOnSubmit(varx);
                        }

                    }
                    _DbSM.SubmitChanges();
                    break;
                    #endregion
                case "Transfer":
                    #region Transfer

                    #region P
                    var queryt = (from varx in _DbSM.BillItemParameters
                                  where varx.OrderNo == Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value)
                                        && varx.subject == 'T'
                                        && varx.TYpe == 'T'
                                  select varx);
                    if (queryt.Any())
                    {
                        foreach (var varx in queryt)
                        {
                            _DbSM.BillItemParameters.DeleteOnSubmit(varx);
                        }

                    }
                    #endregion 

                    #region D
                     queryt = (from varx in _DbSM.BillItemParameters
                                  where varx.OrderNo == Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value)
                                        && varx.subject == 'T'
                                        && varx.TYpe == 'D'
                                  select varx);
                    if (queryt.Any())
                    {
                        foreach (var varx in queryt)
                        {
                            _DbSM.BillItemParameters.DeleteOnSubmit(varx);
                        }

                    }
                    #endregion 

                    _DbSM.SubmitChanges();

                    break;

                    #endregion
                case "PowerPlant":

                    #region PowerPlant
                    var queryP = (from varx in _DbSM.BillItemParameters
                                  where varx.OrderNo == Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value)
                                        && varx.subject == 'P'
                                        && varx.TYpe == 'P'
                                  select varx);
                    if (queryP.Any())
                    {
                        foreach (var varx in queryP)
                        {
                            _DbSM.BillItemParameters.DeleteOnSubmit(varx);
                        }

                    }
                    _DbSM.SubmitChanges();
                    break;
                    #endregion
            }
            FillDataSource();
        }
        #endregion

        #endregion

        #region Methods

        #region void ShowItemParameter(Int32 ItemID)

        /// <summary>
        /// تابعی برای نمایش پارامتر آیتم جاری
        /// </summary>
        /// <param name="ItemID">ردیف آیتم</param>
        private void ShowItemParameter(Int32 ItemID)
        {
            Int32 ParamID = GetItemParamID(ItemID,_PartName,_BillType);
            if (ParamID > 0)
                SetParamsTextBox(ParamID);
            else
            {
                txtSetParameter.Text = "Nothing";
                txtParamDesciption.Text = String.Empty;
            }
        }

        #endregion

        #region static Int32 GetItemParamID(int ItemID)

        private  int GetItemParamID(int ItemID, string _PName, string _BillTypeStatus)
        {
            Int32 ParamID=-1;
            switch (_PName)
            {
                case "Sellers":

                    #region Sellers

        
                    if (_BillTypeStatus=="P")
                    {
                        #region "P"
                        ParamID1 = (from Var in _DbSM.BillItemParameters
                                    where Var.OrderNo == ItemID && Var.subject == 'S' && Var.TYpe=='P'
                                    select Var.ParameterIX);
                        if (ParamID1.Any())
                        {
                            ParamID = Convert.ToInt32(ParamID1.First());
                        }
                    #endregion
                    }
                  

                    if (_BillTypeStatus == "U")
                    {
                        #region "U"
                        ParamID1 = (from Var in _DbSM.BillItemParameters
                                    where Var.OrderNo == ItemID && Var.subject == 'S' && Var.TYpe == 'U'
                                    select Var.ParameterIX);
                        if (ParamID1.Any())
                        {
                            ParamID = Convert.ToInt32(ParamID1.First());
                        }
                    #endregion
                    }

                    if (_BillTypeStatus == "Z")
                    {
                        #region "Z"
                        ParamID1 = (from Var in _DbSM.BillItemParameters
                                    where Var.OrderNo == ItemID && Var.subject == 'S' && Var.TYpe == 'Z'
                                    select Var.ParameterIX);
                        if (ParamID1.Any())
                        {
                            ParamID = Convert.ToInt32(ParamID1.First());
                        }
                        #endregion
                    }
                   
                    break;
                    #endregion

                case "Buyers":

                    #region Buyers
                    ParamID1 = (from Var in _DbSM.BillItemParameters
                                where Var.OrderNo == ItemID && Var.subject == 'B'
                                select Var.ParameterIX);
                    if (ParamID1.Any())
                    {
                        ParamID = Convert.ToInt32(ParamID1.First());
                    }
                    break;
                    #endregion

                case "Transfer":

                    #region Transfer
                    if (_BillTypeStatus == "P")
                    {
                        ParamID1 = (from Var in _DbSM.BillItemParameters
                                    where Var.OrderNo == ItemID && Var.subject == 'T'
                                    select Var.ParameterIX);
                        if (ParamID1.Any())
                        {
                            ParamID = Convert.ToInt32(ParamID1.First());
                        }
                    }
                    if(_BillTypeStatus =="D")
                    {
                        ParamID1 = (from Var in _DbSM.BillItemParameters
                                    where Var.OrderNo == ItemID && Var.subject == 'T'
                                    && Var.TYpe=='D'
                                    select Var.ParameterIX);
                        if (ParamID1.Any())
                        {
                            ParamID = Convert.ToInt32(ParamID1.First());
                        }
                    }
                    break;
                    #endregion

                case "PowerPlant":

                    #region PowerPlant
                    ParamID1 = (from Var in _DbSM.BillItemParameters
                                where Var.OrderNo == ItemID && Var.subject == 'P'
                                select Var.ParameterIX);
                    if (ParamID1.Any())
                    {
                        ParamID = Convert.ToInt32(ParamID1.First());
                    } break;
                    #endregion

            }
            
            return ParamID;
        }

        #endregion

        #region void SetParamsTextBox(int ParamID)

        /// <summary>
        /// تابع تنظیم عناصر پارامتر
        /// </summary>
        /// <param name="ParamID">كد پارامتر مورد نظر</param>
        private void SetParamsTextBox(int ParamID)
        {
            switch (_PartName)
            {
                case "Sellers":
                    #region Sellers
                    ParamName1 = (from Var in _DbSM.View_ParamAndLogs
                                  where Var.ParamID == ParamID
                                  select Var.EnglishName);
                    ParamDesc1 = (from Var in _DbSM.View_ParamAndLogs
                                  where Var.ParamID == ParamID
                                  select Var.Description);
                    txtSetParameter.Text = ParamName1.First();
                    txtParamDesciption.Text = ParamDesc1.First();
                    break;
                    #endregion
                case "Buyers":
                    #region buyers
                    ParamName1 = (from Var in _DbSM.View_ParamAndLogBs
                                  where Var.ParamID == ParamID
                                  select Var.EnglishName);
                    ParamDesc1 = (from Var in _DbSM.View_ParamAndLogBs
                                  where Var.ParamID == ParamID
                                  select Var.Description);
                    txtSetParameter.Text = ParamName1.First();
                    txtParamDesciption.Text = ParamDesc1.First();
                    break;
                    #endregion
                case "Transfer":
                    #region Transfer
                    ParamName1 = (from Var in _DbSM.View_ParamAndLogTs
                                  where Var.ParamID == ParamID
                                  select Var.EnglishName);
                    ParamDesc1 = (from Var in _DbSM.View_ParamAndLogTs
                                  where Var.ParamID == ParamID
                                  select Var.Description);
                    txtSetParameter.Text = ParamName1.First();
                    txtParamDesciption.Text = ParamDesc1.First();
                    break;
                    #endregion
                case "PowerPlant":
                    #region PowerPlant
                    ParamName1 = (from Var in _DbSM.View_ParamAndLogs
                                  where Var.ParamID == ParamID
                                  select Var.EnglishName);
                    ParamDesc1 = (from Var in _DbSM.View_ParamAndLogs
                                  where Var.ParamID == ParamID
                                  select Var.Description);
                    txtSetParameter.Text = ParamName1.First();
                    txtParamDesciption.Text = ParamDesc1.First();
                    #endregion
                    break;
            }
            
        }

        #endregion

        #region void ShowSetParamForm(int ParamID)

        /// <summary>
        /// نمایش فرم انتخاب پارامتر و بازگرداندن پارامتر انتخابی
        /// </summary>
        /// <param name="ParamID">كد پارامتر موجود برای آیتم</param>
        /// <param name="_PName"></param>
        private void ShowChooseBillItemParameter(int ParamID, string _PName)
        {
            var MyForm = new ChooseBillItemParameter(ParamID, _PName);
            if (MyForm.DialogResult == DialogResult.OK)
                SetNewParamForItem(MyForm.ParameterID,_PName,_BillType);
            MyForm.Dispose();
        }
        #endregion

        #region void SetNewParamForItem(int ParamID)

        /// <summary>
        /// تابعی برای تنظیم پارامتر جدید برای آیتم جاری
        /// </summary>
        /// <param name="ParamID">كد پارامتر جدید</param>
        /// <param name="_PName"></param>
        /// <param name="_BillTypeStatus"></param>
        private void SetNewParamForItem(int ParamID, string _PName, string _BillTypeStatus)
        {
            BillItemParameter bitemb;
            switch (_PName)
            {
                case "Sellers":
            
                    #region Sellers
                    if (_BillTypeStatus=="P")
                    {
                        #region Billtype "P"
                        var bitem = new BillItemParameter
                                                      {
                                                          OrderNo = Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value),
                                                          ParameterIX = ParamID,
                                                          subject = 'S',
                                                          TYpe = 'P'
                                                      };
                        var query = (from var in _DbSM.BillItemParameters
                                     where var.OrderNo == bitem.OrderNo && var.subject == bitem.subject
                                           && var.TYpe==bitem.TYpe 
                                     select var);
                        if (query.Any())
                        {
                            foreach (BillItemParameter var in query)
                            {
                                var.ParameterIX = bitem.ParameterIX;
                            }
                            _DbSM.SubmitChanges();
                        }
                        else
                        {
                            _DbSM.BillItemParameters.InsertOnSubmit(bitem);
                            _DbSM.SubmitChanges();
                        }  
                        #endregion
                    }
                    if (_BillTypeStatus == "U")
                    {
                        #region Billtype "U"
                        var bitem = new BillItemParameter
                                                      {
                                                          OrderNo = Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value),
                                                          ParameterIX = ParamID,
                                                          subject = 'S',
                                                          TYpe = 'U'
                                                      };
                        var query = (from var in _DbSM.BillItemParameters
                                     where var.OrderNo == bitem.OrderNo && var.subject == bitem.subject
                                           && var.TYpe==bitem.TYpe 
                                     select var);
                        if (query.Any())
                        {
                            foreach (BillItemParameter var in query)
                            {
                                var.ParameterIX = bitem.ParameterIX;
                            }
                            _DbSM.SubmitChanges();
                        }
                        else
                        {
                            _DbSM.BillItemParameters.InsertOnSubmit(bitem);
                            _DbSM.SubmitChanges();
                        }
                        #endregion
                    }
                    if (_BillTypeStatus == "Z")
                    {
                        #region Billtype "U"
                        var bitem = new BillItemParameter
                        {
                            OrderNo = Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value),
                            ParameterIX = ParamID,
                            subject = 'S',
                            TYpe = 'Z'
                        };
                        var query = (from var in _DbSM.BillItemParameters
                                     where var.OrderNo == bitem.OrderNo && var.subject == bitem.subject
                                           && var.TYpe == bitem.TYpe
                                     select var);
                        if (query.Any())
                        {
                            foreach (BillItemParameter var in query)
                            {
                                var.ParameterIX = bitem.ParameterIX;
                            }
                            _DbSM.SubmitChanges();
                        }
                        else
                        {
                            _DbSM.BillItemParameters.InsertOnSubmit(bitem);
                            _DbSM.SubmitChanges();
                        }
                        #endregion
                    }
                    break;
                    #endregion

                case "Buyers":

                    #region Buyers

                    bitemb = new BillItemParameter
                                 {
                                     OrderNo = Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value),
                                     ParameterIX = ParamID,
                                     subject = 'B',
                                     TYpe ='B'
                                 };

                    var queryb = (_DbSM.BillItemParameters.Where(
                        var => var.OrderNo == bitemb.OrderNo && var.subject == bitemb.subject
                               && var.TYpe == bitemb.TYpe));
                    if (queryb.Any())
                    {
                        foreach (BillItemParameter var in queryb)
                        {
                            var.ParameterIX = bitemb.ParameterIX;
                        }
                        _DbSM.SubmitChanges();
                    }
                    else
                    {
                        
                        _DbSM.BillItemParameters.InsertOnSubmit(bitemb);
                        _DbSM.SubmitChanges();
                    }
                    break;
                    #endregion

                case "Transfer":

                    #region Transfer
                    if (_BillTypeStatus == "P")
                    {
                        #region P

                        bitemb = new BillItemParameter
                                     {
                                         OrderNo = Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value),
                                         ParameterIX = ParamID,
                                         subject = 'T',
                                         TYpe = 'T'
                                     };
                        var queryT = (_DbSM.BillItemParameters.Where(
                            var => var.OrderNo == bitemb.OrderNo && var.subject == bitemb.subject
                                   && var.TYpe == bitemb.TYpe));
                        if (queryT.Any())
                        {
                            foreach (BillItemParameter var in queryT)
                            {
                                var.ParameterIX = bitemb.ParameterIX;
                            }
                            _DbSM.SubmitChanges();
                        }
                        else
                        {
                            _DbSM.BillItemParameters.InsertOnSubmit(bitemb);
                            _DbSM.SubmitChanges();
                        }

                        #endregion
                    }
                    if (_BillTypeStatus == "D")
                    {
                        #region D

                        bitemb = new BillItemParameter
                                     {
                                         OrderNo = Convert.ToInt32(dgvItems.SelectedRows[0].Cells[2].Value),
                                         ParameterIX = ParamID,
                                         subject = 'T',
                                         TYpe = 'D'
                                     };
                       var queryT = (_DbSM.BillItemParameters.Where(
                           var => var.OrderNo == bitemb.OrderNo && var.subject == bitemb.subject
                                  && var.TYpe == bitemb.TYpe));
                        if (queryT.Any())
                        {
                            foreach (BillItemParameter var in queryT)
                            {
                                var.ParameterIX = bitemb.ParameterIX;
                            }
                            _DbSM.SubmitChanges();
                        }
                        else
                        {
                            _DbSM.BillItemParameters.InsertOnSubmit(bitemb);
                            _DbSM.SubmitChanges();
                        }

                        #endregion
                    }
                    break;
                    #endregion

            }
        }

        #endregion

        #region dgvItems_DataError
        private void dgvItems_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            const String ErrorMessage =
                "گزینه مورد نظر باید دارای مقدار باشد ! ";
            PersianMessageBox.Show(ErrorMessage, "خطا!",
                                   MessageBoxButtons.OK, MessageBoxIcon.Error,
                                   MessageBoxDefaultButton.Button1);
        }
        #endregion

        #region FillDataSource()
        private void FillDataSource()
        {
            #region try
            try
            {

                switch (_PartName)
                {
                    case "Sellers":
                        if (_BillType == "P")
                        {
                            dgvItems.DataSource = _DbSM.billItems.Where(Data => Data.Subject == 'S').OrderBy(Data => Data.OrderNo);
                        }
                        if (_BillType == "U")
                        {
                            dgvItems.DataSource = _DbSM.billItems.Where(Data => Data.Subject == 'S').OrderBy(Data => Data.OrderNo); 
                        }

                        if (_BillType == "Z")
                        {
                            dgvItems.DataSource = _DbSM.billItems.Where(Data => Data.Subject == 'Z').OrderBy( Data=>Data.OrderNo);
                        }



                        break;
                    case "Buyers":
                        dgvItems.DataSource = _DbSM.billItems.Where(Data => Data.Subject == 'B');
                        break;
                    case "Transfer":
                        if (_BillType == "P")
                        {
                            dgvItems.DataSource = _DbSM.billItems.Where(Data => Data.Subject == 'T');
                        }
                        if(_BillType =="D")
                        {
                            dgvItems.DataSource = _DbSM.billItemsTDs;
                        }
                        break;
                    case "PowerPlant":
                        dgvItems.DataSource = _DbSM.billItems.Where(Data => Data.Subject == 'P');
                        break;
                }
            }
                #endregion

                #region Catch
            catch (Exception)
            {
                const String ErrorMessage =
                    "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                    "موارد زیر را بررسی نمایید:\n" +
                    "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                MessageBox.Show(ErrorMessage, "خطا!");

            }
            #endregion

        }
        #endregion
      
        #endregion

    }
}