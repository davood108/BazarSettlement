﻿#region using
using System;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.DbLayer;

#endregion

namespace MohanirPouya.Forms.Management.BillItemsSettings
{
    /// <summary>
    /// فرم انتخاب پارامتر جدید برای آیتم های صورتحساب
    /// </summary>
    public partial class ChooseBillItemParameter : Form
    {
        #region Field
        private readonly DbSMDataContext _DbSM;
        private readonly string _Partn;

        #endregion

        #region Ctor

        /// <summary>
        /// سازنده پیش فرض فرم
        /// </summary>
        /// <param name="ParamID">كد پارامتر مورد جستجو</param>
        /// <param name="_PartName"></param>
        public ChooseBillItemParameter(Int32 ParamID, string _PartName)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                (Classes.DbBizClass.dbConnStr);
            ParameterID = ParamID;
            _Partn = _PartName;
            dgvItems.AutoGenerateColumns = false;
            dgvItems .AutoResizeColumns();
            ShowDialog();
        }

        #endregion

        #region Event Handlers

        #region Form Load

        private void frmChooseParameter_Load(object sender, EventArgs e)
        {
            switch (_Partn)
            {
                case "Sellers":
                    dgvItems.DataSource = (from par in _DbSM.View_ParamAndLogs
                                     
                                           orderby par.EnglishName
                                           select par).ToList();
                    break;
                case "Buyers":
                    dgvItems.DataSource = (from par1 in _DbSM.View_ParamAndLogBs
                                           orderby par1.EnglishName

                                           select par1).ToList();
                    break;
                case "Transfer":
                    dgvItems.DataSource = (from par2 in _DbSM.View_ParamAndLogTs
                                           orderby par2.EnglishName

                                           select par2).ToList();
                    break;
           
            }
         

        }

        #endregion

        #region btnSave_Click

        private void btnSave_Click(object sender, EventArgs e)
        {
            ParameterID = Convert.ToInt32(dgvItems.SelectedRows[0].Cells[0].Value);
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        #endregion

        #region btnCancel_Click

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #endregion

        #region Properties

        #region int ParameterID

        /// <summary>
        /// پارامتر جاری
        /// </summary>
        public int ParameterID { get; set; }

        #endregion
        
        #endregion
        
    }
}