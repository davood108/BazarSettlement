﻿#region Using

using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.InputBaseData;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.InputBaseData
{
    public partial class TransferInsertData : MohanirFormDialog
    {
        #region Fields

        private readonly ExcelFileToDataSet _ExcelToGridObject;
        private String ExcelPath;
/*
        private String SheetName;
*/
        private readonly DbSMDataContext _DbSM;
        private DataSet TempDataSet0;

        #endregion

        #region Ctors

        public TransferInsertData(string sd)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                (DbBizClass.dbConnStr);
            if (sd == "I")
            {
                _ExcelToGridObject = new ExcelFileToDataSet();
                Cursor = Cursors.WaitCursor;

                LoadData();
                Cursor = DefaultCursor;
                //--------------
            }
            else
            {
                _DbSM.TruncateInfo();
                MessageBox.Show("اطلاعات با موفقییت پاک شد");
            }
            Process[] ExcelProcess = Process.GetProcessesByName("*.xls");
            foreach (Process process in ExcelProcess)
            {
                process.Kill();
                process.WaitForExit();
                process.Close();
                process.Dispose();
            }


         //   ShowDialog();
        }

        #endregion

        #region Method

        #region LoadData()

        public void LoadData()
        {
            #region Equipment Outage

            ExcelPath = @"C:\SettlementInputData\Transfer\Out";
            String[] FileName = Directory.GetFiles(ExcelPath);
            string dateT;

            foreach (String fileN in FileName)
            {
                _ExcelToGridObject.SourceFile = fileN;
                string Mounth = (fileN.Substring(fileN.Length - 6, 2));
                string Year = (fileN.Substring(fileN.Length - 10, 4));
                dateT = Year + "/" + Mounth;

                #region L400

                _ExcelToGridObject.SheetName = "گزارش خروج خطوط 400";
                _ExcelToGridObject.ConnectionOpen();
                TempDataSet0 = _ExcelToGridObject.LoadFile();
                if (TempDataSet0 == null) return;
                if (TempDataSet0.Tables[0].Rows.Count != 1)
                {
                    for (int i = 4; i < TempDataSet0.Tables[0].Rows.Count; i++)
                    {
                        _DbSM.InsertEquipeOutage("برق منطقه اي " +TempDataSet0.Tables[0].Rows[i][3].ToString(), dateT
                                                 , "L"
                                                 , TempDataSet0.Tables[0].Rows[i][1].ToString()
                                                 , 400
                                                 , TempDataSet0.Tables[0].Rows[i][4].ToString()
                                                 , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][6].ToString())
                            );
                    }
                }

                #endregion

                #region L230

                _ExcelToGridObject.SheetName = "گزارش خروج خطوط 230";
                _ExcelToGridObject.ConnectionOpen();
                TempDataSet0 = _ExcelToGridObject.LoadFile();
                if (TempDataSet0 == null) return;
                if (TempDataSet0.Tables[0].Rows.Count != 1)
                {
                    for (int i = 4; i < TempDataSet0.Tables[0].Rows.Count; i++)
                    {
                        if (TempDataSet0.Tables[0].Rows[i][0].ToString() != "")
                        {
                            _DbSM.InsertEquipeOutage("برق منطقه اي " +TempDataSet0.Tables[0].Rows[i][3].ToString(), dateT
                                                     , "L"
                                                 , TempDataSet0.Tables[0].Rows[i][1].ToString()
                                                 , 230
                                                 , TempDataSet0.Tables[0].Rows[i][4].ToString()
                                                 , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][6].ToString())
                                );
                        }
                    }
                }


                #endregion

                #region T400

                _ExcelToGridObject.SheetName = "گزارش خروج ترانسهاي 400";
                _ExcelToGridObject.ConnectionOpen();
                TempDataSet0 = _ExcelToGridObject.LoadFile();
                if (TempDataSet0 == null) return;
                if (TempDataSet0.Tables[0].Rows.Count != 1)
                {
                    for (int i = 4; i < TempDataSet0.Tables[0].Rows.Count; i++)
                    {

                        string code = TempDataSet0.Tables[0].Rows[i][1].ToString() + TempDataSet0.Tables[0].Rows[i][3].ToString();


                        _DbSM.InsertEquipeOutage("برق منطقه اي " +TempDataSet0.Tables[0].Rows[i][4].ToString(), dateT
                                                 , "T"
                                                 , code
                                                 , 400
                                                 , TempDataSet0.Tables[0].Rows[i][5].ToString()
                                                 , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][7].ToString())
                            );
                    }
                }


                #endregion

                #region T230

                _ExcelToGridObject.SheetName = "گزارش خروج ترانسهاي 230";
                _ExcelToGridObject.ConnectionOpen();
                TempDataSet0 = _ExcelToGridObject.LoadFile();
                if (TempDataSet0 == null) return;
                if (TempDataSet0.Tables[0].Rows.Count != 1)
                {
                    for (int i = 4; i < TempDataSet0.Tables[0].Rows.Count; i++)
                    {
                        string code = TempDataSet0.Tables[0].Rows[i][1].ToString() + TempDataSet0.Tables[0].Rows[i][3].ToString();
                        _DbSM.InsertEquipeOutage("برق منطقه اي " +TempDataSet0.Tables[0].Rows[i][4].ToString(), dateT
                                                 , "T"
                                                 , code
                                                 , 230
                                                 , TempDataSet0.Tables[0].Rows[i][5].ToString()
                                                 , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][7].ToString())
                            );
                    }
                }

                #endregion

            }
            #endregion

            #region Line
            ExcelPath = @"C:\SettlementInputData\Transfer\Line";
            String[] FileNameL = Directory.GetFiles(ExcelPath);
            foreach (String fileN in FileNameL)
            {
                _ExcelToGridObject.SourceFile = fileN;

                _ExcelToGridObject.SheetName = "LineBargh";
                _ExcelToGridObject.ConnectionOpen();
                TempDataSet0 = _ExcelToGridObject.LoadFile();
                if (TempDataSet0 == null) return;
                if (TempDataSet0.Tables[0].Rows.Count != 1)
                {
                    for (int i = 0; i < TempDataSet0.Tables[0].Rows.Count; i++)
                    {

                        if (TempDataSet0.Tables[0].Rows[i][6].ToString() != "")
                        {
                            _DbSM.InsertLine(("برق منطقه اي " + TempDataSet0.Tables[0].Rows[i][10].ToString())
                                             , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][9].ToString())
                                             , TempDataSet0.Tables[0].Rows[i][0].ToString()
                                             , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][6].ToString())
                                             , TempDataSet0.Tables[0].Rows[i][7].ToString()
                                             , TempDataSet0.Tables[0].Rows[i][8].ToString());
                        }

                    }
                }

            }

            #endregion

            #region Trans
            ExcelPath = @"C:\SettlementInputData\Transfer\Trans";
            String[] FileNameT = Directory.GetFiles(ExcelPath);
            foreach (String fileN in FileNameT)
            {
                _ExcelToGridObject.SourceFile = fileN;

                _ExcelToGridObject.SheetName = "TransBargh";
                _ExcelToGridObject.ConnectionOpen();
                TempDataSet0 = _ExcelToGridObject.LoadFile();
                if (TempDataSet0 == null) return;
                if (TempDataSet0.Tables[0].Rows.Count != 1)
                {
                    for (int i = 0; i < TempDataSet0.Tables[0].Rows.Count; i++)
                    {

                        if (TempDataSet0.Tables[0].Rows[i][5].ToString() != "")
                        {
                            //MessageBox.Show(TempDataSet0.Tables[0].Rows[i][1].ToString());
                            //MessageBox.Show(TempDataSet0.Tables[0].Rows[i][2].ToString() +
                            //    TempDataSet0.Tables[0].Rows[i][4].ToString());
                            //MessageBox.Show(TempDataSet0.Tables[0].Rows[i][5].ToString());
                            //MessageBox.Show(TempDataSet0.Tables[0].Rows[i][6].ToString());
                            //MessageBox.Show(TempDataSet0.Tables[0].Rows[i][9].ToString());
                            //MessageBox.Show(TempDataSet0.Tables[0].Rows[i][10].ToString());
                            _DbSM.InsertTrans(TempDataSet0.Tables[0].Rows[i][1].ToString()
                                             , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][6].ToString())
                                             , TempDataSet0.Tables[0].Rows[i][2].ToString()+
                                TempDataSet0.Tables[0].Rows[i][4].ToString()
                                             , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][5].ToString())
                                             , TempDataSet0.Tables[0].Rows[i][9].ToString()
                                             , TempDataSet0.Tables[0].Rows[i][10].ToString());
                        }

                    }
                }

            }

            #endregion

            //#region LT-62132
            //ExcelPath = @"C:\SettlementInputData\Transfer\LT-63132";
            //String[] FileNameL6 = Directory.GetFiles(ExcelPath);
            //foreach (String fileN in FileNameL6)
            //{
            //    _ExcelToGridObject.SourceFile = fileN;

            //    _ExcelToGridObject.SheetName = "LineBargh";
            //    _ExcelToGridObject.ConnectionOpen();
            //    TempDataSet0 = _ExcelToGridObject.LoadFile();
            //    if (TempDataSet0 == null) return;
            //    if (TempDataSet0.Tables[0].Rows.Count != 1)
            //    {
            //        for (int i = 0; i < TempDataSet0.Tables[0].Rows.Count; i++)
            //        {

            //            if (TempDataSet0.Tables[0].Rows[i][6].ToString() != "")
            //            {
            //                _DbSM.InsertLine(("برق منطقه اي " + TempDataSet0.Tables[0].Rows[i][10].ToString())
            //                                 , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][9].ToString())
            //                                 , TempDataSet0.Tables[0].Rows[i][0].ToString()
            //                                 , Convert.ToDecimal(TempDataSet0.Tables[0].Rows[i][6].ToString())
            //                                 , TempDataSet0.Tables[0].Rows[i][7].ToString()
            //                                 , TempDataSet0.Tables[0].Rows[i][8].ToString());
            //            }

            //        }
            //    }

            //}
            //#endregion

            #region Penalty
           #endregion




            MessageBox.Show("اطلاعات با موفقییت وارد شد");
        }

        #endregion

        


        

     

        #endregion

    }
}