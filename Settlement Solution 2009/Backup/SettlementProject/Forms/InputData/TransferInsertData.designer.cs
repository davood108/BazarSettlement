﻿namespace MohanirPouya.Forms.InputBaseData
{
    partial class TransferInsertData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAccept = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDisplay = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnDisplayT = new DevComponents.DotNetBar.ButtonX();
            this.btnAcceptT = new DevComponents.DotNetBar.ButtonX();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDel = new DevComponents.DotNetBar.ButtonX();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BillMD = new System.Windows.Forms.RadioButton();
            this.BillM = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.DateStart = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.DateEnd = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.MyMainPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(-4419, 38);
            this.lblSubtitle.Size = new System.Drawing.Size(0, 29);
            this.lblSubtitle.TabIndex = 8;
            this.lblSubtitle.Text = "";
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(-4368, 2);
            this.lblTitle.Size = new System.Drawing.Size(0, 37);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "";
            // 
            // lblSep
            // 
            this.lblSep.Location = new System.Drawing.Point(340, 27);
            this.lblSep.Size = new System.Drawing.Size(0, 10);
            this.lblSep.TabIndex = 6;
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.labelX1);
            this.MyMainPanel.Controls.Add(this.DateStart);
            this.MyMainPanel.Controls.Add(this.labelX2);
            this.MyMainPanel.Controls.Add(this.DateEnd);
            this.MyMainPanel.Controls.Add(this.groupBox2);
            this.MyMainPanel.Controls.Add(this.btnDel);
            this.MyMainPanel.Controls.Add(this.label2);
            this.MyMainPanel.Controls.Add(this.btnDisplayT);
            this.MyMainPanel.Controls.Add(this.btnAcceptT);
            this.MyMainPanel.Controls.Add(this.btnCancel);
            this.MyMainPanel.Controls.Add(this.btnDisplay);
            this.MyMainPanel.Controls.Add(this.btnAccept);
            this.MyMainPanel.Controls.Add(this.buttonX1);
            this.MyMainPanel.Controls.Add(this.label1);
            this.MyMainPanel.Size = new System.Drawing.Size(343, 217);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.label1, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.buttonX1, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnAccept, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnDisplay, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnCancel, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnAcceptT, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnDisplayT, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.label2, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnDel, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.groupBox2, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.DateEnd, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.labelX2, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.DateStart, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.labelX1, 0);
            // 
            // btnAccept
            // 
            this.btnAccept.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAccept.Location = new System.Drawing.Point(0, 0);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(0, 0);
            this.btnAccept.TabIndex = 59;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonX1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonX1.Location = new System.Drawing.Point(-466, 186);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Size = new System.Drawing.Size(117, 28);
            this.buttonX1.TabIndex = 26;
            this.buttonX1.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.buttonX1.Tooltip = "انصرف از طراحي پارامتر و خروج از فرم";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(44, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 37);
            this.label1.TabIndex = 27;
            this.label1.Text = "ورود اطلاعات پایه خدمات انتقال";
            // 
            // btnDisplay
            // 
            this.btnDisplay.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDisplay.Location = new System.Drawing.Point(251, 309);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(89, 28);
            this.btnDisplay.TabIndex = 47;
            this.btnDisplay.Text = "نمایش";
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(7, 179);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(97, 28);
            this.btnCancel.TabIndex = 56;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "انصراف <b><font color=\"#B77540\">(Esc)</font></b>";
            // 
            // btnDisplayT
            // 
            this.btnDisplayT.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDisplayT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDisplayT.Location = new System.Drawing.Point(239, 179);
            this.btnDisplayT.Name = "btnDisplayT";
            this.btnDisplayT.Size = new System.Drawing.Size(89, 28);
            this.btnDisplayT.TabIndex = 58;
            this.btnDisplayT.Text = "نمایش";
            // 
            // btnAcceptT
            // 
            this.btnAcceptT.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAcceptT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAcceptT.Location = new System.Drawing.Point(144, 179);
            this.btnAcceptT.Name = "btnAcceptT";
            this.btnAcceptT.Size = new System.Drawing.Size(89, 28);
            this.btnAcceptT.TabIndex = 57;
            this.btnAcceptT.Text = "اعمال";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(322, 37);
            this.label2.TabIndex = 69;
            this.label2.Text = "پاک کردن اطلاعات جداول  خدمات انتقال";
            this.label2.Visible = false;
            // 
            // btnDel
            // 
            this.btnDel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDel.Location = new System.Drawing.Point(110, 179);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(103, 28);
            this.btnDel.TabIndex = 76;
            this.btnDel.Text = "پاک کردن اطلاعات";
            this.btnDel.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.BillMD);
            this.groupBox2.Controls.Add(this.BillM);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox2.Location = new System.Drawing.Point(7, 93);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox2.Size = new System.Drawing.Size(321, 80);
            this.groupBox2.TabIndex = 78;
            this.groupBox2.TabStop = false;
            // 
            // BillMD
            // 
            this.BillMD.AutoSize = true;
            this.BillMD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.BillMD.ForeColor = System.Drawing.Color.Blue;
            this.BillMD.Location = new System.Drawing.Point(67, 39);
            this.BillMD.Name = "BillMD";
            this.BillMD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.BillMD.Size = new System.Drawing.Size(52, 17);
            this.BillMD.TabIndex = 90;
            this.BillMD.Tag = "Field";
            this.BillMD.Text = "BillM";
            this.BillMD.UseVisualStyleBackColor = true;
            // 
            // BillM
            // 
            this.BillM.AutoSize = true;
            this.BillM.Enabled = false;
            this.BillM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.BillM.ForeColor = System.Drawing.Color.Blue;
            this.BillM.Location = new System.Drawing.Point(234, 39);
            this.BillM.Name = "BillM";
            this.BillM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.BillM.Size = new System.Drawing.Size(52, 17);
            this.BillM.TabIndex = 88;
            this.BillM.Tag = "Field";
            this.BillM.Text = "BillM";
            this.BillM.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(80, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 87;
            this.label3.Text = "قطعی";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(28, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 41);
            this.pictureBox1.TabIndex = 86;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(243, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 85;
            this.label4.Text = "ماهیانه";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(196, 27);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 41);
            this.pictureBox2.TabIndex = 84;
            this.pictureBox2.TabStop = false;
            // 
            // labelX1
            // 
            this.labelX1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX1.ForeColor = System.Drawing.Color.DarkMagenta;
            this.labelX1.Location = new System.Drawing.Point(262, 64);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(66, 23);
            this.labelX1.TabIndex = 88;
            this.labelX1.Text = "تاریخ شروع:";
            this.labelX1.Visible = false;
            // 
            // DateStart
            // 
            this.DateStart.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.DateStart.IsNull = false;
            this.DateStart.Location = new System.Drawing.Point(174, 67);
            this.DateStart.Name = "DateStart";
            this.DateStart.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DateStart.SelectedDateTime = new System.DateTime(2009, 8, 4, 0, 0, 0, 0);
            this.DateStart.Size = new System.Drawing.Size(91, 20);
            this.DateStart.TabIndex = 86;
            this.DateStart.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            this.DateStart.Visible = false;
            // 
            // labelX2
            // 
            this.labelX2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX2.ForeColor = System.Drawing.Color.DarkMagenta;
            this.labelX2.Location = new System.Drawing.Point(101, 64);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(58, 23);
            this.labelX2.TabIndex = 89;
            this.labelX2.Text = "تاریخ پایان:";
            this.labelX2.Visible = false;
            // 
            // DateEnd
            // 
            this.DateEnd.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.DateEnd.IsNull = false;
            this.DateEnd.Location = new System.Drawing.Point(10, 67);
            this.DateEnd.Name = "DateEnd";
            this.DateEnd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DateEnd.SelectedDateTime = new System.DateTime(2009, 8, 4, 0, 0, 0, 0);
            this.DateEnd.Size = new System.Drawing.Size(88, 20);
            this.DateEnd.TabIndex = 87;
            this.DateEnd.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            this.DateEnd.Visible = false;
            // 
            // TransferInsertData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 217);
            this.FormSubtitle = "";
            this.FormTitle = "";
            this.Name = "TransferInsertData";
            this.Text = "فرم ورود اطلاعات";
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnAccept;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        protected System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ButtonX btnDisplay;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnDisplayT;
        private DevComponents.DotNetBar.ButtonX btnAcceptT;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.ButtonX btnDel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton BillMD;
        private System.Windows.Forms.RadioButton BillM;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevComponents.DotNetBar.LabelX labelX1;
        public RPNCalendar.UI.Controls.PersianDatePicker DateStart;
        private DevComponents.DotNetBar.LabelX labelX2;
        public RPNCalendar.UI.Controls.PersianDatePicker DateEnd;

    }
}