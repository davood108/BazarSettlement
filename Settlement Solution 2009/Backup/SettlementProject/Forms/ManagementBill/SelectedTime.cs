#region Using
using System;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using RPNCalendar.Utilities;
#endregion

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class SelectedTime : Office2007Form
    {
       
       
        #region Field

        private string PtypeE;
   //     private string UnitCN;
        private readonly int _TypeID;

        #endregion

        #region Ctor
        /// <summary>
        /// سازنده فرم
        /// </summary>
        public SelectedTime(Int32 TypeID)
        {
            InitializeComponent();

            #region SetTime
            PersianDate OccuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            StartDate.SelectedDateTime = DateTime.Now;
            EndDate.SelectedDateTime = DateTime.Now;
            StartDate.Text = OccuredDate.ToString();
            EndDate.Text = OccuredDate.ToString();

           

         
            #endregion
            _TypeID = TypeID;
            #region Type
            #region Daily
            if (TypeID == 1)
            {
                cBoxDaily.Checked = true;
                lblEDate.Visible = false;
                EndDate.Visible = false;
            }
                #endregion
            else if (TypeID == 2) cBoxMounthly.Checked = true;
            else if (TypeID == 3) cBoxConst.Checked = true;
            #endregion
            ShowDialog();
        }
        #endregion

        #region Event

        #region Form_Load
        private void Form_Load(object sender, EventArgs e)
        {
  
        

            #region FillCboTitle
            if(_TypeID ==1)
            {
                var Title = new String[1];
                Title[0] = "فروش";
                // Title[1] = "خريد";
                cboTitle.DataSource = Title;   
            }
            else
            {
                var Title = new String[3];
                Title[0] = "فروش";
                Title[1] = "خريد";
                Title[2] = "خدمات انتقال";
              
                cboTitle.DataSource = Title;
       
            }

            #endregion
        }
        #endregion

        #region btnSelect_Click
        private void btnSelect_Click(object sender, EventArgs e)
        {
            #region Type
            String Type = String.Empty;


            String Ptype = cboTitle.Text;

            switch (Ptype)
            {
                case "فروش":
                    PtypeE = "Sellers";
                    break;
                case "خريد":
                    PtypeE = "Buyers";
                    break;
                case "خدمات انتقال":

                    #region Transfer
                    PtypeE = "Transfer";
          
                    #endregion
                    break;
          
             
            }
            if (cBoxConst.Checked)   Type = "قطعي";
            else if (cBoxDaily.Checked)  {EndDate.Text = StartDate.Text;  Type = "روزانه";}
            else if (cBoxMounthly.Checked)  Type = "ماهيانه";
            #endregion


            #region Set  Initial

            Int16 Diff;
            if (cBoxDaily.Checked) Diff = 0;
            else 
                Diff = (Int16)(EndDate.SelectedDateTime).Subtract
                                  (StartDate.SelectedDateTime).Days;
            PersianDate WeekB = PersianDateConverter.ToPersianDate(StartDate.SelectedDateTime.AddDays
         (-7).Date);
            String WeekBText =  WeekB.ToString();
            string WeeKBDate = WeekBText.Substring(0, 10);                                   



            #endregion

            new BillSelectD(Type, Ptype, PtypeE,StartDate.Text, EndDate.Text,Diff,WeeKBDate);
         
        }
        #endregion

        

  
        #endregion

    }
}