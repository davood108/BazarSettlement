namespace MohanirPouya.Forms.ManagementBill
{
    partial class BillSelectD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.DataSetLog = new System.Data.DataSet();
            this.ParametersLog = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.CCreationDate = new System.Data.DataColumn();
            this.CStartDate = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.lblParamDescribe = new System.Windows.Forms.Label();
            this.lblBillType = new System.Windows.Forms.Label();
            this.lblBeginDate = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.btnSaveEMS = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.lblParamName = new System.Windows.Forms.Label();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.lblTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGridView = new DevComponents.DotNetBar.ButtonX();
            this.lblSDate = new System.Windows.Forms.Label();
            this.lblEDate = new System.Windows.Forms.Label();
            this.lblBillTitle = new System.Windows.Forms.Label();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.lstPowerStations = new SettlementControlLibraries.Controls.MohanirCheckList();
            this.lstSellerList = new SettlementControlLibraries.Controls.MohanirCheckList();
            this.lstSellerType = new SettlementControlLibraries.Controls.MohanirCheckList();
            this.btnExcelOut = new DevComponents.DotNetBar.ButtonX();
            this.btnSaveToEMS = new DevComponents.DotNetBar.ButtonX();
            this.ProgressBarSearch = new DevComponents.DotNetBar.Controls.ProgressBarX();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).BeginInit();
            this.TopPanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MyToolTip
            // 
            this.MyToolTip.AutoPopDelay = 10000;
            this.MyToolTip.InitialDelay = 500;
            this.MyToolTip.IsBalloon = true;
            this.MyToolTip.ReshowDelay = 100;
            this.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MyToolTip.ToolTipTitle = "راهنمايي";
            // 
            // DataSetLog
            // 
            this.DataSetLog.DataSetName = "ParametersLog";
            this.DataSetLog.Tables.AddRange(new System.Data.DataTable[] {
            this.ParametersLog});
            // 
            // ParametersLog
            // 
            this.ParametersLog.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.CCreationDate,
            this.CStartDate,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.ParametersLog.TableName = "ParametersLog";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "رديف";
            this.dataColumn1.ColumnName = "Revision";
            this.dataColumn1.DataType = typeof(int);
            // 
            // CCreationDate
            // 
            this.CCreationDate.Caption = "تاريخ ايجاد";
            this.CCreationDate.ColumnName = "CreationDate";
            this.CCreationDate.DataType = typeof(System.DateTime);
            this.CCreationDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // CStartDate
            // 
            this.CStartDate.Caption = "تاريخ آغاز اعتبار";
            this.CStartDate.ColumnName = "StartDate";
            this.CStartDate.DataType = typeof(System.DateTime);
            this.CStartDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "تاريخ اتمام اعتبار";
            this.dataColumn4.ColumnName = "ExpirationDate";
            this.dataColumn4.DataType = typeof(System.DateTime);
            this.dataColumn4.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "متن كد پارامتر";
            this.dataColumn5.ColumnName = "SelectString";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "نام كاربر";
            this.dataColumn6.ColumnName = "UserID";
            // 
            // lblParamDescribe
            // 
            this.lblParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamDescribe.AutoSize = true;
            this.lblParamDescribe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblParamDescribe.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblParamDescribe.Location = new System.Drawing.Point(253, 64);
            this.lblParamDescribe.Name = "lblParamDescribe";
            this.lblParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamDescribe.Size = new System.Drawing.Size(112, 13);
            this.lblParamDescribe.TabIndex = 17;
            this.lblParamDescribe.Text = "عنوان صورت حساب:";
            // 
            // lblBillType
            // 
            this.lblBillType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBillType.BackColor = System.Drawing.Color.White;
            this.lblBillType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBillType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblBillType.ForeColor = System.Drawing.Color.Green;
            this.lblBillType.Location = new System.Drawing.Point(369, 60);
            this.lblBillType.Name = "lblBillType";
            this.lblBillType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBillType.Size = new System.Drawing.Size(139, 21);
            this.lblBillType.TabIndex = 11;
            this.lblBillType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBeginDate.AutoSize = true;
            this.lblBeginDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblBeginDate.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblBeginDate.Location = new System.Drawing.Point(517, 98);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBeginDate.Size = new System.Drawing.Size(74, 13);
            this.lblBeginDate.TabIndex = 15;
            this.lblBeginDate.Text = "تاريخ شروع :";
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblEndDate.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblEndDate.Location = new System.Drawing.Point(253, 98);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblEndDate.Size = new System.Drawing.Size(64, 13);
            this.lblEndDate.TabIndex = 14;
            this.lblEndDate.Text = "تاريخ پايان:";
            // 
            // btnSaveEMS
            // 
            this.btnSaveEMS.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveEMS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveEMS.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnSaveEMS.Location = new System.Drawing.Point(420, 463);
            this.btnSaveEMS.Name = "btnSaveEMS";
            this.btnSaveEMS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSaveEMS.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.btnSaveEMS.Size = new System.Drawing.Size(140, 28);
            this.btnSaveEMS.TabIndex = 26;
            this.btnSaveEMS.Text = "مقایسه صورتحساب (F1)";
            this.btnSaveEMS.Tooltip = "ذخيره نمودن پارامتر و بست فرم ايجاد پارامتر";
            this.btnSaveEMS.Click += new System.EventHandler(this.btnCompare_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(20, 463);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(98, 28);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Tooltip = "انصرف از طراحي پارامتر و خروج از فرم";
            // 
            // lblParamName
            // 
            this.lblParamName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblParamName.AutoSize = true;
            this.lblParamName.Location = new System.Drawing.Point(315, 504);
            this.lblParamName.Name = "lblParamName";
            this.lblParamName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamName.Size = new System.Drawing.Size(0, 13);
            this.lblParamName.TabIndex = 11;
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.lblTitle);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(728, 40);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.TopPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 43;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.LightPink;
            this.lblTitle.Location = new System.Drawing.Point(245, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(238, 37);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "تنظيمات  نمايش صورتحساب";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label1.Location = new System.Drawing.Point(517, 64);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "نوع صورت حساب:";
            // 
            // btnGridView
            // 
            this.btnGridView.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGridView.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnGridView.Location = new System.Drawing.Point(568, 463);
            this.btnGridView.Name = "btnGridView";
            this.btnGridView.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnGridView.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5);
            this.btnGridView.Size = new System.Drawing.Size(140, 28);
            this.btnGridView.TabIndex = 47;
            this.btnGridView.Text = "نمايش (F5)";
            this.btnGridView.Tooltip = "بررسي دستورات وارد شده";
            this.btnGridView.Click += new System.EventHandler(this.btnGridView_Click);
            // 
            // lblSDate
            // 
            this.lblSDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSDate.BackColor = System.Drawing.Color.White;
            this.lblSDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblSDate.ForeColor = System.Drawing.Color.Green;
            this.lblSDate.Location = new System.Drawing.Point(369, 94);
            this.lblSDate.Name = "lblSDate";
            this.lblSDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSDate.Size = new System.Drawing.Size(139, 21);
            this.lblSDate.TabIndex = 51;
            this.lblSDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEDate
            // 
            this.lblEDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEDate.BackColor = System.Drawing.Color.White;
            this.lblEDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblEDate.ForeColor = System.Drawing.Color.Green;
            this.lblEDate.Location = new System.Drawing.Point(111, 94);
            this.lblEDate.Name = "lblEDate";
            this.lblEDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblEDate.Size = new System.Drawing.Size(139, 21);
            this.lblEDate.TabIndex = 52;
            this.lblEDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBillTitle
            // 
            this.lblBillTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBillTitle.BackColor = System.Drawing.Color.White;
            this.lblBillTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBillTitle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblBillTitle.ForeColor = System.Drawing.Color.Green;
            this.lblBillTitle.Location = new System.Drawing.Point(111, 60);
            this.lblBillTitle.Name = "lblBillTitle";
            this.lblBillTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBillTitle.Size = new System.Drawing.Size(139, 21);
            this.lblBillTitle.TabIndex = 53;
            this.lblBillTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.lstPowerStations);
            this.MainPanel.Controls.Add(this.lstSellerList);
            this.MainPanel.Controls.Add(this.lstSellerType);
            this.MainPanel.Controls.Add(this.btnExcelOut);
            this.MainPanel.Controls.Add(this.btnSaveToEMS);
            this.MainPanel.Controls.Add(this.ProgressBarSearch);
            this.MainPanel.Controls.Add(this.lblBillTitle);
            this.MainPanel.Controls.Add(this.lblEDate);
            this.MainPanel.Controls.Add(this.lblSDate);
            this.MainPanel.Controls.Add(this.btnGridView);
            this.MainPanel.Controls.Add(this.label1);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Controls.Add(this.lblParamName);
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.btnSaveEMS);
            this.MainPanel.Controls.Add(this.lblEndDate);
            this.MainPanel.Controls.Add(this.lblBeginDate);
            this.MainPanel.Controls.Add(this.lblBillType);
            this.MainPanel.Controls.Add(this.lblParamDescribe);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(728, 559);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 28;
            // 
            // lstPowerStations
            // 
            this.lstPowerStations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstPowerStations.BackColor = System.Drawing.Color.Transparent;
            this.lstPowerStations.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstPowerStations.Location = new System.Drawing.Point(21, 146);
            this.lstPowerStations.Name = "lstPowerStations";
            this.lstPowerStations.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lstPowerStations.Size = new System.Drawing.Size(214, 285);
            this.lstPowerStations.TabIndex = 63;
            this.lstPowerStations.Title = "لیست نیروگاه ها";
            // 
            // lstSellerList
            // 
            this.lstSellerList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstSellerList.BackColor = System.Drawing.Color.Transparent;
            this.lstSellerList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstSellerList.Location = new System.Drawing.Point(252, 146);
            this.lstSellerList.Name = "lstSellerList";
            this.lstSellerList.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lstSellerList.Size = new System.Drawing.Size(236, 285);
            this.lstSellerList.TabIndex = 62;
            this.lstSellerList.Title = "لیست فروشندگان";
            // 
            // lstSellerType
            // 
            this.lstSellerType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstSellerType.BackColor = System.Drawing.Color.Transparent;
            this.lstSellerType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstSellerType.Location = new System.Drawing.Point(504, 146);
            this.lstSellerType.Name = "lstSellerType";
            this.lstSellerType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lstSellerType.Size = new System.Drawing.Size(204, 285);
            this.lstSellerType.TabIndex = 61;
            this.lstSellerType.Title = "فروشندگان";
            // 
            // btnExcelOut
            // 
            this.btnExcelOut.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExcelOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcelOut.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnExcelOut.Location = new System.Drawing.Point(124, 463);
            this.btnExcelOut.Name = "btnExcelOut";
            this.btnExcelOut.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnExcelOut.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F6);
            this.btnExcelOut.Size = new System.Drawing.Size(140, 28);
            this.btnExcelOut.TabIndex = 60;
            this.btnExcelOut.Text = "تولید خروجی F6) Excel )";
            this.btnExcelOut.Tooltip = "بررسي دستورات وارد شده";
            this.btnExcelOut.Click += new System.EventHandler(this.btnExcelOut_Click);
            // 
            // btnSaveToEMS
            // 
            this.btnSaveToEMS.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveToEMS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveToEMS.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnSaveToEMS.Location = new System.Drawing.Point(272, 463);
            this.btnSaveToEMS.Name = "btnSaveToEMS";
            this.btnSaveToEMS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSaveToEMS.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnSaveToEMS.Size = new System.Drawing.Size(140, 28);
            this.btnSaveToEMS.TabIndex = 59;
            this.btnSaveToEMS.Text = "ذخیره در F3) EMS)";
            this.btnSaveToEMS.Tooltip = "بررسي دستورات وارد شده";
            this.btnSaveToEMS.Click += new System.EventHandler(this.btnSaveToEMS_Click);
            // 
            // ProgressBarSearch
            // 
            this.ProgressBarSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ProgressBarSearch.ForeColor = System.Drawing.Color.Red;
            this.ProgressBarSearch.Location = new System.Drawing.Point(20, 500);
            this.ProgressBarSearch.Maximum = 2;
            this.ProgressBarSearch.Name = "ProgressBarSearch";
            this.ProgressBarSearch.ProgressType = DevComponents.DotNetBar.eProgressItemType.Marquee;
            this.ProgressBarSearch.Size = new System.Drawing.Size(687, 23);
            this.ProgressBarSearch.TabIndex = 58;
            this.ProgressBarSearch.TabStop = false;
            this.ProgressBarSearch.Text = "در حال محاسبه";
            this.ProgressBarSearch.TextVisible = true;
            this.ProgressBarSearch.Visible = false;
            // 
            // BillSelectD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(728, 559);
            this.Controls.Add(this.MainPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BillSelectD";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم تنظيمات نمايش";
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).EndInit();
            this.TopPanel.ResumeLayout(false);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip MyToolTip;
        private System.Data.DataSet DataSetLog;
        private System.Data.DataTable ParametersLog;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn CCreationDate;
        private System.Data.DataColumn CStartDate;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Windows.Forms.Label lblParamDescribe;
        private System.Windows.Forms.Label lblBillType;
        private System.Windows.Forms.Label lblBeginDate;
        private System.Windows.Forms.Label lblEndDate;
        private DevComponents.DotNetBar.ButtonX btnSaveEMS;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private System.Windows.Forms.Label lblParamName;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ButtonX btnGridView;
        private System.Windows.Forms.Label lblBillTitle;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        public System.Windows.Forms.Label lblSDate;
        public System.Windows.Forms.Label lblEDate;
        internal DevComponents.DotNetBar.Controls.ProgressBarX ProgressBarSearch;
        private DevComponents.DotNetBar.ButtonX btnExcelOut;
        private DevComponents.DotNetBar.ButtonX btnSaveToEMS;
        private SettlementControlLibraries.Controls.MohanirCheckList lstPowerStations;
        private SettlementControlLibraries.Controls.MohanirCheckList lstSellerList;
        private SettlementControlLibraries.Controls.MohanirCheckList lstSellerType;
    }
}