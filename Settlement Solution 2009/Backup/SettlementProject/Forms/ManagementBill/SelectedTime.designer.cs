namespace MohanirPouya.Forms.ManagementBill
{
    partial class SelectedTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.DataSetLog = new System.Data.DataSet();
            this.ParametersLog = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.CCreationDate = new System.Data.DataColumn();
            this.CStartDate = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.lblTitle = new System.Windows.Forms.Label();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cBoxDaily = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cBoxConst = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cBoxMounthly = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.EndDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.StartDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.lblEDate = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.cboTitle = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnSelect = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).BeginInit();
            this.TopPanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // MyToolTip
            // 
            this.MyToolTip.AutoPopDelay = 10000;
            this.MyToolTip.InitialDelay = 500;
            this.MyToolTip.IsBalloon = true;
            this.MyToolTip.ReshowDelay = 100;
            this.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MyToolTip.ToolTipTitle = "راهنمایی";
            // 
            // DataSetLog
            // 
            this.DataSetLog.DataSetName = "ParametersLog";
            this.DataSetLog.Tables.AddRange(new System.Data.DataTable[] {
            this.ParametersLog});
            // 
            // ParametersLog
            // 
            this.ParametersLog.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.CCreationDate,
            this.CStartDate,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.ParametersLog.TableName = "ParametersLog";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "ردیف";
            this.dataColumn1.ColumnName = "Revision";
            this.dataColumn1.DataType = typeof(int);
            // 
            // CCreationDate
            // 
            this.CCreationDate.Caption = "تاریخ ایجاد";
            this.CCreationDate.ColumnName = "CreationDate";
            this.CCreationDate.DataType = typeof(System.DateTime);
            this.CCreationDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // CStartDate
            // 
            this.CStartDate.Caption = "تاریخ آغاز اعتبار";
            this.CStartDate.ColumnName = "StartDate";
            this.CStartDate.DataType = typeof(System.DateTime);
            this.CStartDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "تاریخ اتمام اعتبار";
            this.dataColumn4.ColumnName = "ExpirationDate";
            this.dataColumn4.DataType = typeof(System.DateTime);
            this.dataColumn4.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "متن كد پارامتر";
            this.dataColumn5.ColumnName = "SelectString";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "نام كاربر";
            this.dataColumn6.ColumnName = "UserID";
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.lblTitle);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(487, 40);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.TopPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 43;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.LightPink;
            this.lblTitle.Location = new System.Drawing.Point(3, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(481, 37);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "تنظیمات  صورتحساب";
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.pictureBox1);
            this.MainPanel.Controls.Add(this.cBoxDaily);
            this.MainPanel.Controls.Add(this.cBoxConst);
            this.MainPanel.Controls.Add(this.cBoxMounthly);
            this.MainPanel.Controls.Add(this.labelX1);
            this.MainPanel.Controls.Add(this.EndDate);
            this.MainPanel.Controls.Add(this.StartDate);
            this.MainPanel.Controls.Add(this.lblEDate);
            this.MainPanel.Controls.Add(this.labelX3);
            this.MainPanel.Controls.Add(this.labelX4);
            this.MainPanel.Controls.Add(this.cboTitle);
            this.MainPanel.Controls.Add(this.btnSelect);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(487, 256);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 28;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 216);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(84, 28);
            this.btnCancel.TabIndex = 72;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Tooltip = "انصرف از طراحي پارامتر و خروج از فرم";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MohanirPouya.Properties.Resources.CursorXP;
            this.pictureBox1.Location = new System.Drawing.Point(12, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(158, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 67;
            this.pictureBox1.TabStop = false;
            // 
            // cBoxDaily
            // 
            this.cBoxDaily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cBoxDaily.BackColor = System.Drawing.Color.Transparent;
            this.cBoxDaily.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cBoxDaily.Enabled = false;
            this.cBoxDaily.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cBoxDaily.Location = new System.Drawing.Point(310, 56);
            this.cBoxDaily.Name = "cBoxDaily";
            this.cBoxDaily.Size = new System.Drawing.Size(64, 19);
            this.cBoxDaily.TabIndex = 66;
            this.cBoxDaily.Text = "روزانه";
            // 
            // cBoxConst
            // 
            this.cBoxConst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cBoxConst.AutoSize = true;
            this.cBoxConst.BackColor = System.Drawing.Color.Transparent;
            this.cBoxConst.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cBoxConst.Enabled = false;
            this.cBoxConst.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cBoxConst.Location = new System.Drawing.Point(193, 59);
            this.cBoxConst.Name = "cBoxConst";
            this.cBoxConst.Size = new System.Drawing.Size(55, 16);
            this.cBoxConst.TabIndex = 64;
            this.cBoxConst.Text = "قطعی";
            // 
            // cBoxMounthly
            // 
            this.cBoxMounthly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cBoxMounthly.AutoSize = true;
            this.cBoxMounthly.BackColor = System.Drawing.Color.Transparent;
            this.cBoxMounthly.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cBoxMounthly.Enabled = false;
            this.cBoxMounthly.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cBoxMounthly.Location = new System.Drawing.Point(254, 59);
            this.cBoxMounthly.Name = "cBoxMounthly";
            this.cBoxMounthly.Size = new System.Drawing.Size(59, 16);
            this.cBoxMounthly.TabIndex = 65;
            this.cBoxMounthly.Text = "ماهیانه";
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            this.labelX1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.labelX1.Location = new System.Drawing.Point(380, 59);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(97, 16);
            this.labelX1.TabIndex = 63;
            this.labelX1.Text = "نوع صورت حساب:";
            // 
            // EndDate
            // 
            this.EndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EndDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.EndDate.IsNull = false;
            this.EndDate.Location = new System.Drawing.Point(310, 137);
            this.EndDate.Name = "EndDate";
            this.EndDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.EndDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.EndDate.Size = new System.Drawing.Size(99, 20);
            this.EndDate.TabIndex = 63;
            this.EndDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // StartDate
            // 
            this.StartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StartDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.StartDate.IsNull = false;
            this.StartDate.Location = new System.Drawing.Point(310, 111);
            this.StartDate.Name = "StartDate";
            this.StartDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.StartDate.Size = new System.Drawing.Size(99, 20);
            this.StartDate.TabIndex = 62;
            this.StartDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // lblEDate
            // 
            this.lblEDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEDate.AutoSize = true;
            this.lblEDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblEDate.Location = new System.Drawing.Point(415, 137);
            this.lblEDate.Name = "lblEDate";
            this.lblEDate.Size = new System.Drawing.Size(62, 16);
            this.lblEDate.TabIndex = 60;
            this.lblEDate.Text = "تاریخ پایان :";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX3.AutoSize = true;
            this.labelX3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX3.Location = new System.Drawing.Point(415, 111);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(69, 16);
            this.labelX3.TabIndex = 59;
            this.labelX3.Text = "تاریخ شروع :";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX4.AutoSize = true;
            this.labelX4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX4.Location = new System.Drawing.Point(331, 86);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(146, 16);
            this.labelX4.TabIndex = 58;
            this.labelX4.Text = "انتخاب عنوان صورت حساب:";
            // 
            // cboTitle
            // 
            this.cboTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTitle.DisplayMember = "Text";
            this.cboTitle.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboTitle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cboTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.cboTitle.FormattingEnabled = true;
            this.cboTitle.ItemHeight = 15;
            this.cboTitle.Location = new System.Drawing.Point(203, 84);
            this.cboTitle.Name = "cboTitle";
            this.cboTitle.Size = new System.Drawing.Size(122, 21);
            this.cboTitle.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.cboTitle.TabIndex = 57;
            this.cboTitle.WatermarkColor = System.Drawing.SystemColors.GradientInactiveCaption;
            // 
            // btnSelect
            // 
            this.btnSelect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelect.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelect.Location = new System.Drawing.Point(102, 216);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSelect.Size = new System.Drawing.Size(92, 28);
            this.btnSelect.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnSelect.TabIndex = 64;
            this.btnSelect.TabStop = false;
            this.btnSelect.Text = "انتخاب (Enter)";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // SelectedTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 256);
            this.Controls.Add(this.MainPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectedTime";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم تنظیمات صورت حساب";
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).EndInit();
            this.TopPanel.ResumeLayout(false);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip MyToolTip;
        private System.Data.DataSet DataSetLog;
        private System.Data.DataTable ParametersLog;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn CCreationDate;
        private System.Data.DataColumn CStartDate;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private System.Windows.Forms.Label lblTitle;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        public DevComponents.DotNetBar.Controls.CheckBoxX cBoxDaily;
        public DevComponents.DotNetBar.Controls.CheckBoxX cBoxConst;
        public DevComponents.DotNetBar.Controls.CheckBoxX cBoxMounthly;
        private DevComponents.DotNetBar.LabelX labelX1;
        public RPNCalendar.UI.Controls.PersianDatePicker EndDate;
        public RPNCalendar.UI.Controls.PersianDatePicker StartDate;
        private DevComponents.DotNetBar.LabelX lblEDate;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        public DevComponents.DotNetBar.Controls.ComboBoxEx cboTitle;
        private DevComponents.DotNetBar.ButtonX btnSelect;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.ButtonX btnCancel;
    }
}