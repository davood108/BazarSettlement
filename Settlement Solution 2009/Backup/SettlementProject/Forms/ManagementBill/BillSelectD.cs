﻿#region using
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class BillSelectD : Office2007Form
    {

        #region Field

        private readonly DbSMDataContext _DbSM;
        private readonly string _PtypeE;
        private readonly string _SDate;
        private readonly string _EDate;
        private readonly BackgroundWorker bw;
        private string SELECTString;
        private readonly string _Btype;
        private int ItemsCount;
        private string btnselect;
        private DataTable table1;
        private DataTable table1Rep;
        private DataTable table2;
        private DataTable table3;
        private DataTable table4;
        private readonly String _ConnectionString = DbBizClass.dbConnStr;
        private SqlConnection _SqlConnection;
        private SqlCommand _SqlCommand;
        private DataTable table5;
        private string CompanyCode;
        private DataTable table6;

        private string Date;
        private DataSet ds;
        private SqlConnection MySqlConnection;
        private SqlCommand MySqlCommand1;
        private ExportToExcel ExToexcel;
        private string CompanyName1;
        private string _mdate1;
        private string _mdate;
        private string _year;
        private DataSet ds1;
        private DataSet ds1Rep;
        private DataTable tablep;
        private SqlConnection MySqlConnectionR;
        private SqlCommand MySqlCommand1R;
        private ExportToExcel ExToexcelR;

        private DataTable tablep2;
        private int CreateXLS;
        private short _Diff;
        private DataTable table7;
        private string _WeekBDate;
        private IQueryable<billItem> query;
        private DataTable table8;
        private DataTable table9;
        private IQueryable<billItemsTD> query1;
        private SqlDataReader MySqlDataReaderCN;
        private DataTable tableCN;
        private String xcn;
        private string PowerStationCode;
        private string PowerStationName;
        private string MarketState;

        #endregion

        #region Properties

        #region Sellers String

        public String[] SellersString
        {
            get
            {
                var ReturnVal = new String[lstSellerType.List.CheckedItems.Count];
                for (int i = 0; i < lstSellerType.List.CheckedItems.Count; i++)
                {
                    ReturnVal[i] = lstSellerType.List.CheckedItems[i].ToString();
                }
                return ReturnVal;
            }
        }

        #endregion

        #region SellersList String

        public String[] SellersListString
        {
            get
            {
                var ReturnVal = new String[lstSellerList.List.CheckedItems.Count];
                for (int i = 0; i < lstSellerList.List.CheckedItems.Count; i++)
                {
                    ReturnVal[i] = lstSellerList.List.CheckedItems[i].ToString();
                }
                return ReturnVal;
            }
        }

        #endregion

        #region Power Stations String

        public String[] PowerStationsString
        {
            get
            {
                var ReturnVal = new String[lstPowerStations.List.CheckedItems.Count];
                for (int i = 0; i < lstPowerStations.List.CheckedItems.Count; i++)
                {
                    ReturnVal[i] = lstPowerStations.List.CheckedItems[i].ToString();
                }
                return ReturnVal;
            }
        }

        #endregion

        #endregion

        #region  Constructor
        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        public BillSelectD(string Type1, string Ptype, string PtypeE, string SDate, string EDate, short Diff, string WeeKBDate)
        {
            InitializeComponent();
            _DbSM = new DbSMDataContext
                (DbBizClass.dbConnStr);

            _Btype = Type1;
            _PtypeE = PtypeE;
            _SDate = SDate;
            _EDate = EDate;
            _Diff = Diff;
            _WeekBDate = WeeKBDate;

            #region initial BGWorker

            bw = new BackgroundWorker();
            bw.DoWork += bw_DoWork;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;

            #endregion

            #region Set Form Title
            lblBillTitle.Text = Ptype;
            lblSDate.Text = SDate;
            lblEDate.Text = EDate;
            lblBillType.Text = Type1;
            #endregion

            #region InitialList
            switch (_PtypeE)
            {
                case "Sellers":

                    #region seller

                    #endregion
                    break;

                case "Buyers":

                    #region Buyer

                    lstPowerStations.Visible = false;
                    lstSellerType.Title = "خریداران";
                    lstSellerList.Title = "لیست خریداران";

                    #endregion

                    break;
                case "Transfer":
                    #region Transfer
                    lstPowerStations.Visible = false;
                    lstSellerType.Title = "خدمات انتقال";
                    lstSellerList.Title = "لیست خدمات انتقال";
                    #endregion
                    break;
            }
            #endregion

            ShowDialog();

        }
        #endregion

        #region Events Handlers

        #region Form_Load

        private void Form_Load(object sender, EventArgs e)
        {

            _SqlConnection = new SqlConnection(_ConnectionString);
            _SqlCommand = new SqlCommand();
            btnselect = "";

            #region Fill list

            switch (_PtypeE)
            {
                case "Sellers":
                    #region  Sellers


                    if (_Btype == "روزانه")
                    {


                        _DbSM.SP_InitialUnitBill(_SDate, _SDate);

                    }
                    if (_Btype == "ماهيانه")
                    {
                        _DbSM.SP_InitialUnitBill(_SDate, _EDate);

                    }

                    FillSellers();
                    lstSellerType.List.SelectedIndexChanged += FillSellersList;
                    lstSellerList.List.SelectedIndexChanged += FillPowerStations;


                    #endregion
                    break;

                case "Buyers":
                    #region Buyers
                    FillSellers();
                    lstSellerType.List.SelectedIndexChanged += FillSellersList;

                    #endregion
                    break;

                case "Transfer":
                    #region  Transfer
                    FillSellers();
                    lstSellerType.List.SelectedIndexChanged += FillSellersList;
           
                    #endregion
                    break;
            }
            #endregion
        }

        #endregion

        #region btnCompare_Click
        private void btnCompare_Click(object sender, EventArgs e)
        {
            ProgressBarSearch.Visible = true;
            ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
            ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
            ProgressBarSearch.Text = "در حال انجام محاسبات";
            btnselect = "C";
            bw.RunWorkerAsync("C");

        }
        #endregion

        #region btnGridView_Click
        private void btnGridView_Click(object sender, EventArgs e)
        {
            switch (_PtypeE)
            {
                case "Sellers":
                    ProgressBarSearch.Visible = true;
                    ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                    ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                    ProgressBarSearch.Text = "در حال انجام محاسبات";
                    btnselect = "D";
                    bw.RunWorkerAsync("D");
                    break;
                case "Buyers":

                    break;
                case "Transfer":

                    break;
            }


        }

        #endregion

        #region btnExcelOut_Click

        private void btnExcelOut_Click(object sender, EventArgs e)
        {
            if (lstSellerList.List.CheckedItems.Count == 0)
            {
                PersianMessageBox.Show("هیچ شرکتی برای تولید صورتحساب انتخاب نشده است ",
                               "خطا", MessageBoxButtons.OK, MessageBoxIcon.Question,
                               MessageBoxDefaultButton.Button1);
                return;
            }

            ProgressBarSearch.Visible = true;
            ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
            ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
     //       ProgressBarSearch.Text = " Excelدر حال انجام محاسبات و تولید خروجی  ";
            btnselect = "E";
            bw.RunWorkerAsync("E");

        }

        #endregion

        #region btnSaveToEMS_Click

        private void btnSaveToEMS_Click(object sender, EventArgs e)
        {
            ProgressBarSearch.Visible = true;
            ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
            ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
            ProgressBarSearch.Text = "EMS در حال ذخیره سازی در ";
            btnselect = "M";
            bw.RunWorkerAsync("M");

        }
        #endregion

        #endregion

        #region Method

        #region BW_DoWork
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            if ((string)e.Argument == "D")
            {
                //InitialFixedParameter();
                //FillFixedParameter();
                MakeTable3();
                MakeTable3U();
            }
            if ((string)e.Argument == "C")
            {
                //InitialFixedParameter();
                //FillFixedParameter();
                MakeTable2();
                MakeTable3();
            }
            if ((string)e.Argument == "E")
            {

                InitialFixedParameter();

                FillFixedParameter();

                MakeTable2();

                MakeExcel();

            }
            if ((string)e.Argument == "M")
            {

                Fill_EmsSettlement();

            }

        }


        #endregion

        #region BW_RunWorkerCompleted

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                ProgressBarSearch.Value = 0;
                ProgressBarSearch.Text = "خطا در ";
            }
            else
            {
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                ProgressBarSearch.Value = 2;



                if (btnselect == "D")
                {
                    //          new BillsDisply(_PtypeE);
                    ProgressBarSearch.Text = "اتمام محاسبات";
                }
                else if (btnselect == "C")
                {

                    //          new BillCompare(lblSDate.Text, lblEDate.Text, _PtypeE, _Btype);
                    ProgressBarSearch.Text = "اتمام محاسبات";
                }
                else if (btnselect == "E")
                {
                    ProgressBarSearch.Text = "Excel اتمام تولید خروجی";
                }
                else if (btnselect == "M")
                {
                    ProgressBarSearch.Text = "EMS اتمام ذخیره سازی در";
                }
            }
        }

        #endregion
        
        #region   MakeTable2()

        private void MakeTable2()
        {

            switch (_PtypeE)
            {
                case "Sellers":

                    #region Sellers

                    #region Normal

                    #region Make Table Fixed Items


                    SELECTString = "IF OBJECT_ID('Sellers.BillData', 'U') IS NOT NULL " +
                                   "DROP TABLE Sellers.BillData; " +
                                   "CREATE TABLE Sellers.[BillData] ( " +
                                   "[RegionID] [int] ," +
                                   "[RegionName] [nvarchar](50) , " +
                                   "[CompanyTypeName] [nvarchar](50) , " +
                                   "[CompanyID] [int] , " +
                                   "[CompanyCode] [nvarchar](5) ," +
                                   "[CompanyName] [nvarchar](50) , " +
                                   "[PowerStationID] [int] ," +
                                   "[PowerStationCode] [varchar](5) , " +
                                   "[PowerStationName] [nvarchar](50) , " +
                                   "[PowerStationLatinName] [nvarchar](50) ," +
                                   "[UnitType] [nvarchar](50) , " +
                                   "[BillUnitT] [varchar](MAX) ," +
                                   "[BillUnitID] [int] ," +
                                   "[Date] [varchar](50) , " +
                                   "[Hour] [tinyint] ,";

                    #endregion

                    #region Make BillItems Columns

                    ItemsCount = GetBillItemsCount(_PtypeE, "S");

                    for (int i = 0; i < ItemsCount; i++)
                        if (i == ItemsCount - 1)
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0);";
                        else
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0, ";

                    #endregion

                    #region Insert Fixed Data Items



                    SELECTString += "\n INSERT INTO Sellers.[BillData]  " +
                    "([RegionID],[RegionName]," +
                    "[CompanyTypeName],[CompanyID],[CompanyCode],[CompanyName]," +
                    "[PowerStationID],[PowerStationCode],[PowerStationName],[PowerStationLatinName]," +
                    "[UnitType],[BillUnitT],[BillUnitID],[Date],[Hour]) " +
                    "(SELECT [RegionID],[RegionName]," +
                    "[CompanyTypeName],[CompanyID],[CompanyCode],[CompanyName], " +
                    "[PowerStationID],[PowerStationCode],[PowerStationName],[PowerStationLatinName]," +
                    "[UnitType],[BillUnitT],[BillUnitID],[Date],[Hour] " +
                    "FROM [Settlement].[Sellers].[FixedParameters] " +
                    " Where HasBill=1 and (MarketStateCode not LIKE 'EN%' and MarketStateCode NOT LIKE  'EP%' ))";


                    #endregion

                    #region Execute

                    SELECTString += " EXEC Sellers.SP_SaveBillData;";

                    #endregion

                    #region ExecuteSelectString

                    try
                    {
                     Parameters
                            .ExecuteSelectString(SELECTString);
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }

                    #endregion

                    #endregion

                    #region Tazmini

                    #region Make Table Fixed Items


                    SELECTString = "IF OBJECT_ID('Sellers.BillDataZ', 'U') IS NOT NULL " +
                                   "DROP TABLE Sellers.BillDataZ; " +
                                   "CREATE TABLE Sellers.[BillDataZ] ( " +
                                   "[RegionID] [int] ," +
                                   "[RegionName] [nvarchar](50) , " +
                                   "[CompanyTypeName] [nvarchar](50) , " + 
                                   "[CompanyID] [int] , " +
                                   "[CompanyCode] [nvarchar](5) ," +
                                   "[CompanyName] [nvarchar](50) , " +
                                   "[PowerStationID] [int] ," +
                                   "[PowerStationCode] [varchar](5) , " +
                                   "[PowerStationName] [nvarchar](50) , " +
                                   "[PowerStationLatinName] [nvarchar](50) ," +
                                   "[UnitType] [nvarchar](50) , " +
                                   "[UnitCode] [nvarchar](5) ," +
                                   "[MarketStateCode] [nvarchar](5),"+
                                   "[UnitID] [int],"+ 
                                   "[BillUnitT] [varchar](MAX) ," +
                                   "[BillUnitID] [int] ," +
                                   "[Date] [varchar](50) , " +
                                   "[Hour] [tinyint] ," +
                                   "LoadStateH [Nchar](2) ,";





                    #endregion

                    #region Make BillItems Columns

                    ItemsCount = GetBillItemsCount(_PtypeE, "Z");

                    for (int i = 0; i < ItemsCount; i++)
                        if (i == ItemsCount - 1)
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0);";
                        else
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0, ";

                    #endregion

                    #region Insert Fixed Data Items



                    SELECTString += "\n INSERT INTO Sellers.[BillDataZ]  " +
                    "([RegionID],[RegionName]," +
                    "[CompanyTypeName],[CompanyID],[CompanyCode],[CompanyName]," +
                    "[PowerStationID],[PowerStationCode],[PowerStationName],[PowerStationLatinName]," +
                    "[UnitType],[UnitCode],[MarketStateCode],[UnitID],[BillUnitT],[BillUnitID],[Date],[Hour],[LoadStateH] ) " +
                    "(SELECT [RegionID],[RegionName]," +
                    "[CompanyTypeName],[CompanyID],[CompanyCode],[CompanyName], " +
                    "[PowerStationID],[PowerStationCode],[PowerStationName],[PowerStationLatinName]," +
                    "[UnitType],[UnitCode],[MarketStateCode] ,[UnitID] ,[BillUnitT],[BillUnitID],[Date],[Hour],[LoadStateH] " +
                    "FROM [Settlement].[Sellers].[FixedParameters] " +
                    " Where (MarketStateCode like 'EN%' or MarketStateCode like 'EP%') )";


                    #endregion

                    #region Execute

        SELECTString += " EXEC Sellers.SP_SaveBillDataZ;";

                    #endregion

                    #region ExecuteSelectString

                    try
                    {
                        Parameters
                            .ExecuteSelectString(SELECTString);
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }

                    #endregion

                    #endregion

                    #endregion

                    break;

                case "Buyers":

                    #region Buyers

                    #region Make Table Fixed Items

                    SELECTString = "IF OBJECT_ID('Buyers.BillDisplay', 'U') IS NOT NULL " +
                                   "DROP TABLE Buyers.BillDisplay; " +
                                   "CREATE TABLE Buyers.[BillDisplay] ( " +
                                   "[CompanyName] [nvarchar](50) , " +
                                   "[Date] [varchar](50) NULL, " +
                                   "[Hour] [tinyint] NULL ,";

                    #endregion

                    #region Make BillItems Columns

                    ItemsCount = GetBillItemsCount(_PtypeE,"B");

                    for (int i = 0; i < ItemsCount; i++)
                        if (i == ItemsCount - 1)
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0);";
                        else
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0, ";

                    #endregion

                    #region Insert Fixed Data Items

                    SELECTString += "\n INSERT INTO Buyers.[BillDisplay] " +
                                    "([CompanyName] " +
                                    ",[Date] , [Hour]) " +
                                    "(SELECT CompanyName," +
                                    "Date, Hour " +
                                    "FROM [Settlement].[Buyers].[FixedParameters])";

                    #endregion

                    #region Execute

                    SELECTString += "EXEC Buyers.SP_SaveBillData;";

                    #endregion

                    #region ExecuteSelectString

                    try
                    {
                     Parameters
                            .ExecuteSelectString(SELECTString);
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }

                    #endregion

                    #endregion

                    break;

                case "Transfer":


                    break;

            }
        }

        #endregion

        #region MakeTable3()

        private void MakeTable3()
        {
            switch (_PtypeE)
            {
                case "Sellers":

                    #region Sellers

                    #region Update FixParameter

                    //       _DbSM.SP_FillFixedBillMk6();

                    #endregion

                    #region Make Table Fixed Items

                    SELECTString = "IF OBJECT_ID('Sellers.BillDisplayMK6', 'U') IS NOT NULL " +
                                   "DROP TABLE Sellers.BillDisplayMK6; " +
                                   "CREATE TABLE Sellers.[BillDisplayMK6] ( " +
                                   "[PowerStationName] [nvarchar](50) , " +
                                   "[UnitType] [varchar](2) NULL, " +
                                   "[Date] [varchar](50) NULL, " +
                                   "[Hour] [tinyint] NULL ,";

                    #endregion

                    #region Make BillItems Columns

                    ItemsCount = GetBillItemsCount(_PtypeE,"S");

                    for (int i = 0; i < ItemsCount; i++)
                        if (i == ItemsCount - 1)
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0);";
                        else
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0, ";

                    #endregion

                    #region Insert Fixed Data Items

                    SELECTString += "\n INSERT INTO Sellers.[BillDisplayMK6] " +
                                    "([PowerStationName] , [UnitType]" +
                                    ",[Date] , [Hour]) " +
                                    "(SELECT PowerStationName,UnitType," +
                                    "Date, Hour " +
                                    "FROM Sellers.BillDisplayPower)";

                    #endregion

                    #region Execute

                    SELECTString += "EXEC [Sellers].[SP_SaveBillDiplayMK6] ;";

                    #endregion

                    #region ExecuteSelectString

                    try
                    {
                       Parameters
                            .ExecuteSelectString(SELECTString);
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }

                    #endregion

                    #endregion

                    break;

                case "Buyers":

                    #region Buyers

                    #region Make Table Fixed Items

                    SELECTString = "IF OBJECT_ID('Buyers.BillDisplayMK6', 'U') IS NOT NULL " +
                                   "DROP TABLE Buyers.BillDisplayMK6; " +
                                   "CREATE TABLE Buyers.[BillDisplayMK6] ( " +
                                   "[CompanyName] [nvarchar](50) , " +
                                   "[Date] [varchar](50) NULL, " +
                                   "[Hour] [tinyint] NULL ,";

                    #endregion

                    #region Make BillItems Columns

                    ItemsCount = GetBillItemsCount(_PtypeE,"B");

                    for (int i = 0; i < ItemsCount; i++)
                        if (i == ItemsCount - 1)
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0);";
                        else
                            SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0, ";

                    #endregion

                    #region Insert Fixed Data Items

                    SELECTString += "\n INSERT INTO Buyers.[BillDisplayMK6] " +
                                    "([CompanyName] " +
                                    ",[Date] , [Hour]) " +
                                    "(SELECT CompanyName," +
                                    "Date, Hour " +
                                    "FROM [Settlement].[Buyers].[FixedParameters])";

                    #endregion

                    #region Execute

                    SELECTString += "EXEC Buyers.SP_SaveBillDisplayMK6B;";

                    #endregion

                    #region ExecuteSelectString

                    try
                    {
                   Parameters
                            .ExecuteSelectString(SELECTString);
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }

                    #endregion

                    #endregion

                    break;

                case "Transfer":



                    break;
            }
        }
        #endregion

        #region  MakeTable3U()
        private void MakeTable3U()
        {
            #region Make Table Fixed Items for Unit Mode

            SELECTString = "IF OBJECT_ID('Sellers.BillDisplayUnit', 'U') IS NOT NULL " +
                           "DROP TABLE Sellers.BillDisplayUnit; " +
                           "CREATE TABLE Sellers.[BillDisplayUnit] ( " +
                           "[PowerStationName] [nvarchar](50) ," +
                           "[UnitType] [varchar](2) NULL," +
                           "[UnitID] [int] NULL, " +
                           "[Date] [varchar](50) NULL, " +
                           "[Hour] [tinyint] NULL ,";


            #endregion

            #region Make BillItems Columns

            Int32 ItemsCount = GetBillItemsCount(_PtypeE,"U");

            for (int i = 0; i < ItemsCount; i++)
                if (i == ItemsCount - 1)
                    SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0);";
                else
                    SELECTString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0, ";

            #endregion

            #region Insert Fixed Data Items


            SELECTString += "\n INSERT INTO Sellers.[BillDisplayUnit] " +
                            "([PowerStationName] , [UnitType]" +
                            ", [UnitID] , [Date] , [Hour]) " +
                            "(SELECT PowerStationName,UnitCN AS UnitType," +
                            "UnitID,Date, Hour " +
                            "FROM Sellers.FixedParameters)";


            #endregion

            #region Execute

            SELECTString += "EXEC Sellers.SP_SaveBillDisplayUnit ;";

            #endregion

            #region ExecuteSelectString
            try
            {
           Parameters
                    .ExecuteSelectString(SELECTString);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);

            }
            #endregion
        }
        #endregion

        #region int GetBillItemsCount()

        private int GetBillItemsCount(string _PtypeE,string Taz)
        {
            Int32 ReturnValue = 0;
            switch (_PtypeE)
            {
                case "Sellers":
                    if (Taz == "S")
                    {
                       query = (from var in _DbSM.billItems
                                                      where var.Subject == 'S' 
                                                      select var);
                        ReturnValue = query.Count();
                    }
                    if (Taz == "Z")
                    {
                         query = (from var in _DbSM.billItems
                                                      where var.Subject == 'Z'
                                                      select var);
                        ReturnValue = query.Count();
                    }
                    break;
                case "Buyers":
                    query = (from var in _DbSM.billItems
                             where var.Subject == 'B'
                             select var);
                    ReturnValue = query.Count();
                    break;
                case "Transfer":
                    query1 = (from var in _DbSM.billItemsTDs
                             where var.Subject == 'D'
                             select var);
                    ReturnValue = query1.Count();
                    break;

                case "PowerPlant":
                    query = (from var in _DbSM.billItems
                             where var.Subject == 'P'
                             select var);
                    ReturnValue = query.Count();
                    break;
            }
            return ReturnValue;
        }

        #endregion

        #region InitialFixedParam

        private void InitialFixedParameter()
        {
        //    _SDate 
            int i = 0;
            SELECTString = "";
            _DbSM.TruncateDetails(_PtypeE);


            switch (_PtypeE)
            {
                case "Sellers":

                    #region Run Sellers SpFixParam

                    if (_Btype == "روزانه")
                    {

                        #region Company

                        if (lstPowerStations.List.CheckedItems.Count == 0)
                        {
                            // MessageBox.Show(lstSellerList.List.CheckedItems.Count.ToString());
                            foreach (var x in lstSellerList.List.CheckedItems)
                            {
                                if (lstSellerList.List.CheckedItems != null)
                                {

                               //     MessageBox.Show(lstSellerList.List.CheckedItems[i].ToString());
                                    try
                                    {


                                        _DbSM.SP_InitialFixedParamC(_SDate, _SDate,
                                                                    lstSellerList.List.CheckedItems[i].ToString());
                                    }
                                    #region Catch
                                    catch (Exception)
                                    {
                                        const String ErrorMessage =
                                            "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                                            "موارد زیر را بررسی نمایید:\n" +
                                            "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                                        MessageBox.Show(ErrorMessage, "خطا!");
                                        return;
                                    }
                                    #endregion

                                    i = i + 1;
                                }

                            }
                        }
                        #endregion

                        #region PowerPlant
                        else
                        {
                            foreach (var x in lstPowerStations.List.CheckedItems)
                            {
                                if (lstPowerStations.List.CheckedItems != null)
                                {
                                    _DbSM.SP_InitialFixedParamP(_SDate, _SDate, lstPowerStations.List.CheckedItems[i].ToString());
                                    i = i + 1;
                                }

                            }
                        }
                        #endregion
                   

                    }
                    if (_Btype == "ماهيانه")
                    {
                        #region Company

                        if (lstPowerStations.List.CheckedItems.Count == 0)
                        {
                            foreach (var x1 in lstSellerList.List.CheckedItems)
                            {
                                if (lstSellerList.List.CheckedItems != null)
                                {

                                    try
                                    {
                                        //string ghg = lstSellerList.List.CheckedItems[i].ToString().TrimStart().TrimEnd();

                                        _DbSM.SP_InitialFixedParamC(_SDate, _EDate,
                                                                   lstSellerList.List.CheckedItems[i].ToString().TrimStart().TrimEnd());
                                    }
                                    #region Catch
                                    catch (Exception e)
                                    {
                                        const String ErrorMessage =
                                            "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                                            "موارد زیر را بررسی نمایید:\n" +
                                            "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                                        MessageBox.Show(lstSellerList.List.CheckedItems[i].ToString());
                                        MessageBox.Show(ErrorMessage, "خطا!");
                                        MessageBox.Show(e.Message);
                                        return;
                                    }
                                    #endregion

                                    i = i + 1;
                                }

                            }
                        }
                        #endregion

                        #region PowerPlant
                        else
                        {
                            foreach (var x in lstPowerStations.List.CheckedItems)
                            {
                                if (lstPowerStations.List.CheckedItems != null)
                                {
                                    _DbSM.SP_InitialFixedParamP(_SDate, _EDate, lstPowerStations.List.CheckedItems[i].ToString());
                                    i = i + 1;
                                }

                            }
                        }
                        #endregion

                    }

                

                    #endregion

                    break;

                case "Buyers":

                    #region Run Buyers SpFixParam
                  
                     foreach (var x in lstSellerList.List.CheckedItems)
                        {
                            if (lstSellerList.List.CheckedItems != null) 

                            {
                               _DbSM .SP_InitialFixedParamBC(_SDate, _EDate, lstSellerList.List.CheckedItems[i].ToString());
                                i = i + 1;
                            }

                        }

                    #endregion

                    break;
                case "Transfer":
                    #region Run Transfer SpFixParam

              

                 _DbSM.SP_InitialParamTF(_SDate, _EDate);

           
                 
                    foreach (var x in lstSellerList.List.CheckedItems)
                    {
                        if (lstSellerList.List.CheckedItems != null)
                        {
                          _DbSM.SP_InitialParamTF2();

                       #region intial Company

                          try
                          {

                              _DbSM.CommandTimeout = 2000;
                              _DbSM.SP_InitialParamT(_SDate, _EDate,
                                                     lstSellerList.List.CheckedItems[i].ToString());

                          }
                          #region Catch

                          catch (Exception et)
                          {
                              const String ErrorMessage =
                                  "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                                  "موارد زیر را بررسی نمایید:\n" +
                                  "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                              MessageBox.Show(ErrorMessage, "خطا!");
                              MessageBox.Show(et.Message);
                              return;
                          }

                          #endregion

                            #endregion

                            #region Run Transfer SpFixParam

                            _DbSM.CommandTimeout = 2000;
                            _DbSM.SP_FillFixedParamT();
                            _DbSM.SP_CreateTableT();
                            //_DbSM.InsertLinelenght(_SDate, _EDate);
                            //_DbSM.InsertTransCapacity(_SDate, _EDate);
                            //_DbSM.InsertDetailT1(Date);

                            #endregion

                            #region Make table 2 Transfer

                            #region Make Table Fixed Items
                            SELECTString = "";


                            SELECTString += "IF OBJECT_ID('Transfer.BillData', 'U') IS NOT NULL " +
                                            "DROP TABLE Transfer.BillData; " +
                                            "CREATE TABLE Transfer.[BillData] ( " +
                                            "[CompanyCode] [nvarchar](5) ," +
                                            "[CompanyName] [nvarchar](50) , " +
                                            "[Date] [varchar](12) , " +
                                            "[Hour] [tinyint] ," +
                                            "[EquipmentCode] [nvarchar](50) ," +
                                            "[EquipmentType] [nchar](10) ," +
                                            "[EquipmentPosition] [nchar](10) ," +
                                            "[VoltageLevel] [numeric](4, 1) ,";





                            #endregion

                            #region Make BillItems Columns

                            ItemsCount = GetBillItemsCount(_PtypeE, "T");

                            for (int it = 0; it < ItemsCount; it++)
                            {
                                if (it == ItemsCount - 1)
                                {

                                    SELECTString += "Field" + (it + 1) + " numeric(18, 4) DEFAULT 0);";

                                }
                                else
                                {
                                    if (it == 3)
                                    {
                                        SELECTString += "Field" + (it + 1) + " nvarchar(50) ,";
                                    }
                                    else
                                    {

                                        SELECTString += "Field" + (it + 1) + " numeric(18, 4) DEFAULT 0, ";
                                    }

                                }
                            }

                            #endregion

                            #region Insert Fixed Data Items

                            SELECTString += "\n    INSERT INTO [Settlement].[Transfer].[BillData] " +
                                            " ([CompanyCode],[CompanyName],[Date]," +
                                            " [Hour],[EquipmentCode],[EquipmentType]," +
                                            " [EquipmentPosition],[VoltageLevel])" +
                                            " SELECT [CompanyCode],[CompanyName]," +
                                            " [Date],[Hour],[EquipmentCode],[EquipmentType]," +
                                            " [EquipmentPosition],[VoltageLevel] " +
                                            " FROM [Settlement].[Transfer].[FixedParameters] ";

                            #endregion

                            #region Execute

                            SELECTString += "EXEC Transfer.SP_SaveBillData ;";

                            #endregion

                            #region ExecuteSelectString

                            try
                            {
                            Parameters
                                    .ExecuteSelectString(SELECTString);
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }

                            #endregion

                            #endregion

                            #region Make Excel Transfer New

                            #region company




                            #region Intial

                            ds = new DataSet();
                            table1 = new DataTable();
                            table2 = new DataTable();
                            table3 = new DataTable();
                            table4 = new DataTable();
                            table5 = new DataTable();
                            table6 = new DataTable();
                            table7 = new DataTable();
                            table8 = new DataTable();

                            #endregion

                            #region bill-Table1

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText = ("SELECT * " +
                                                                   " FROM [Settlement].[Transfer].[BillData] " +
                                                                   " where CompanyName='" +
                                                                   lstSellerList.List.CheckedItems[i] + "'")

                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table1.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-table6-PS

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        ("SELECT distinct EquipmentType,VoltageLevel" +
                                                         " FROM [Settlement].[Transfer].[BillData] " +
                                                         " where  CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                         "'")
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table6.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-Date

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        "SELECT   DISTINCT (Date)  Date FROM [Settlement].[Transfer].[BillData] order by Date"
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table2.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-Sum

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        ("SELECT  *  FROM [Settlement].[Transfer].[BillIDataSum]" +
                                                         " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                         "'" +
                                                         " Order by EquipmentType,VoltageLevel,EquipmentCode,Date")
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table3.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-TotalLT

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        ("SELECT  *  FROM [Settlement].[Transfer].[BillIDataTotal_LT]" +
                                                         " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                         "'" +
                                                         " Order by EquipmentType,VoltageLevel,Date")
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table4.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-TotalM

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        ("SELECT  *  FROM [Settlement].[Transfer].[BillIDataTotal_M]" +
                                                         " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                         "'" +
                                                         " Order by EquipmentType,VoltageLevel")
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table5.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-Total

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        ("SELECT  *    FROM [Settlement].[Transfer].[BillIDataTotal]" +
                                                         " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                         "'")
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table7.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-TotalBC

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                            {
                                Connection = MySqlConnection,
                                CommandText =
                                    ("SELECT  *  FROM [Settlement].[Transfer].[BillIDataTotal_BC]" +
                                     " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                     "'" )
                            };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table8.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-CompanyCode

                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Transfer].[BillData] " +
                                                         " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                         "'")
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                CreateXLS = 1;
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                SqlDataReader MySqlDataReader =
                                    MySqlCommand1.ExecuteReader();
                                if (MySqlDataReader != null)
                                    if (MySqlDataReader.HasRows)
                                    {
                                        while (MySqlDataReader.Read())
                                            CompanyName1 = MySqlDataReader[1].ToString();
                                    }
                                    else
                                    {
                                        CreateXLS = 0;
                                    }
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region Bill-Excel

                            table1.TableName = "table1";
                            table2.TableName = "Date";
                            table3.TableName = "Sum";
                            table4.TableName = "TotalLT";
                            table5.TableName = "TotalM";
                            table6.TableName = "PS";
                            table7.TableName = "Total";
                            table8.TableName = "TotalBC";

                            ds.Tables.Add(table1);
                            ds.Tables.Add(table2);
                            ds.Tables.Add(table3);
                            ds.Tables.Add(table4);
                            ds.Tables.Add(table5);
                            ds.Tables.Add(table6);
                            ds.Tables.Add(table7);
                            ds.Tables.Add(table8);

                            #region Try

                            try
                            {
                                ExToexcel = new ExportToExcel();
                                ExToexcel.toexcel(
                                    CompanyName1 + "( " + " از تاریخ   " + _SDate + "  تا  " + _EDate+" )",
                                    "", ds,
                                    @"E:\Settlement\Temp\temp-Billtransfer.xls",
                                    @"E:\Settlement\Out\Monthly\Transfer\",
                                    @"BillT_" + CompanyName1 + ".xls", false);
                            }
                                #endregion

                                #region Catch

                            catch (Exception ee)
                            {
                                const String ErrorMessage =
                                    "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                                    "موارد زیر را بررسی نمایید:\n" +
                                    "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                                MessageBox.Show(ErrorMessage, "خطا!");
                                MessageBox.Show(ee.Message);
                                return;
                            }

                            #endregion

                            #endregion








                            #endregion

                            #endregion

                            i = i + 1;
                        }
                      
                        _DbSM.SP_BillDataTotal();

                        
                    }

                    #region Make Excel Transfer TotalReport

                    #region company




                    #region Intial

                    ds = new DataSet();
                    table1 = new DataTable();
                    table2 = new DataTable();


                    #endregion

                    #region bill-Table1

                    #region Prepare Connection & Command

                    MySqlConnection =
                        new SqlConnection(DbBizClass.dbConnStr);
                    MySqlCommand1 = new SqlCommand
                    {
                        Connection = MySqlConnection,
                        CommandText = ("SELECT * " +
                                       " FROM [Settlement].[Transfer].[BillData_Total] "
                               )

                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        MySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        table1.Load(MySqlCommand1.ExecuteReader());
                        // ReSharper restore AssignNullToNotNullAttribute
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }
                    finally
                    {
                        MySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Table2

                    #region Prepare Connection & Command

                    MySqlConnection =
                        new SqlConnection(DbBizClass.dbConnStr);
                    MySqlCommand1 = new SqlCommand
                    {
                        Connection = MySqlConnection,
                        CommandText =
                            "SELECT   * FROM [Settlement].[Transfer].[BillIData_Total_Sum] "
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        MySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        table2.Load(MySqlCommand1.ExecuteReader());
                        // ReSharper restore AssignNullToNotNullAttribute
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }
                    finally
                    {
                        MySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Bill-Excel

                    table1.TableName = "tablep";
                    table2.TableName = "tablep2";


                    ds.Tables.Add(table1);
                    ds.Tables.Add(table2);


                    #region Try

                    try
                    {
                        ExToexcel = new ExportToExcel();
                        ExToexcel.toexcel(
                         " " + " از تاریخ   " + _SDate + "  تا  " + _EDate + " ",
                            "", ds,
                            @"E:\Settlement\Temp\temp-Transfer-Report.xls",
                            @"E:\Settlement\Out\Monthly\Transfer\",
                            @"BillT_SumReport.xls", false);
                    }
                    #endregion

                    #region Catch

                    catch (Exception ee)
                    {
                        const String ErrorMessage =
                            "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                            "موارد زیر را بررسی نمایید:\n" +
                            "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                        MessageBox.Show(ErrorMessage, "خطا!");
                        MessageBox.Show(ee.Message);
                        return;
                    }

                    #endregion

                    #endregion








                    #endregion

                    #endregion
                    

                    

                    

                    #endregion
                    break;
            }
        }
        #endregion

        #region FillFixedParam

        private void FillFixedParameter()
        {
            switch (_PtypeE)
            {
                case "Sellers":

                    #region Run Sellers SpFixParam

                    try
                    {
                        if (_Btype == "روزانه")
                        {

                            _DbSM.CommandTimeout = 10000;
                            _DbSM.SP_FillFixedParamD(_SDate, _SDate);
                            _DbSM.SP_CreateBillText();
                            _DbSM.SP_PriceStep(_SDate);
                            _DbSM.SP_CreateTable(_SDate);
                        }
                        if (_Btype == "ماهيانه")
                        {

                            _DbSM.CommandTimeout = 10000;
                            _DbSM.SP_FillFixedParam(_SDate, _EDate);
                            _DbSM.SP_CreateBillText();
                            _DbSM.SP_PriceStep(_SDate);
                            _DbSM.SP_CreateTable(_SDate);
                
                        }
                   }

                 #region Catch
                    catch (Exception)
                    {
                        const String ErrorMessage =
                            "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                            "موارد زیر را بررسی نمایید:\n" +
                            "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                        MessageBox.Show(ErrorMessage, "خطا!");
                        return;
                    }
                    #endregion

                    #endregion

                    break;

                case "Buyers":

                    #region Run Buyers SpFixParam

                    #region try
                    try
                    {
                    _DbSM.CommandTimeout = 2000;
                    _DbSM.SP_FillFixedParamB(_SDate, _EDate,_WeekBDate);
              _DbSM.SP_CreateTableB();

                    }
                    #endregion

                    #region Catch
                    catch (Exception)
                    {
                        const String ErrorMessage =
                            "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                            "موارد زیر را بررسی نمایید:\n" +
                            "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                        MessageBox.Show(ErrorMessage, "خطا!");
      
                        return;
                    }
                    #endregion

                    #endregion

                    break;
                case "Transfer":

                    

                    break;
            }
        }
        #endregion

        #region MakeExcel
        private void MakeExcel()
        {
            int i = 0;
            switch (_PtypeE)
            {
                case "Sellers":



                    #region Reghabati

                    if (lstPowerStations.List.CheckedItems.Count == 0)
                    {
                        #region Company
                        tableCN = new DataTable();

                        #region bill-CompanyName

                        #region Prepare Connection & Command

                        MySqlConnection =
                            new SqlConnection(DbBizClass.dbConnStr);
                        MySqlCommand1 = new SqlCommand
                        {
                            Connection = MySqlConnection,
                            CommandText =
                                ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillData] ")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {

                            MySqlCommand1.Connection.Open();

                            tableCN.Load(MySqlCommand1.ExecuteReader());


                            if (tableCN.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                return;
                            }

                        }
                        catch (Exception Ex)
                        {
                            MessageBox.Show(Ex.Message);
                        }
                        finally
                        {
                            MySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        for (int j = 0; j < tableCN.Rows.Count; j++)
                        {
                            xcn = tableCN.Rows[j][1].ToString();


                            if (lstSellerList.List.CheckedItems != null)
                            {
                                #region company

                                #region Intial

                                ds = new DataSet();
                                table1 = new DataTable();
                                table2 = new DataTable();
                                table3 = new DataTable();
                                table4 = new DataTable();
                                table5 = new DataTable();
                                table6 = new DataTable();
                                #endregion

                                #region bill-Table1

                                #region Prepare Connection & Command

                                MySqlConnection =
                               new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText = ("SELECT * " +
                                                                       " FROM [Settlement].[Sellers].[BillData] " +
                                                                       " where CompanyName='" +
                                                                       xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table1.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-table6-PS

                                #region Prepare Connection & Command

                                MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT DISTINCT(PowerStationCode),PowerStationName,UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                                                             " FROM [Settlement].[Sellers].[BillData] " +
                                                             " where CompanyName='" + xcn +
                                                             "'" +
                                                             "Order by PowerStationCode,PowerStationName,UnitType,BillUnitT")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table6.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Date

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            "SELECT   DISTINCT (Date)  Date FROM [Settlement].[Sellers].[BillData] order by Date"
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table2.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Sum
                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  *  FROM [Settlement].[Sellers].[BillIDataSum]" +
                                                             "where CompanyName='" + xcn +
                                                             "'" +
                                                             "Order by PowerStationCode,PowerStationName,UnitType,BillUnitT,Date")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table3.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-TotalP
                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                                                       " FROM [Settlement].[Sellers].[BillIDataTotalP]" +
                                                                       " where CompanyName='" +
                                                                       xcn + "'" +
                                                                       "order by [PowerStationCode]")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table4.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Total
                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  *    FROM [Settlement].[Sellers].[BillIDataTotal]" +
                                                             " where CompanyName='" + xcn +
                                                             "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table5.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-CompanyCode

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillData] " +
                                                             " where CompanyName='" + xcn +
                                                             "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    CreateXLS = 1;
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    SqlDataReader MySqlDataReader =
                         MySqlCommand1.ExecuteReader();
                                    if (MySqlDataReader.HasRows)
                                    {
                                        while (MySqlDataReader.Read())
                                            CompanyCode = MySqlDataReader[0].ToString();
                                    }
                                    else
                                    {
                                        CreateXLS = 0;
                                    }
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                if (CreateXLS == 1)
                                {
                                    #region Bill-Excel

                                    table1.TableName = "table1";
                                    table2.TableName = "Date";
                                    table3.TableName = "Sum";
                                    table4.TableName = "TotalP";
                                    table5.TableName = "Total";
                                    table6.TableName = "PS";
                                    ds.Tables.Add(table1);
                                    ds.Tables.Add(table2);
                                    ds.Tables.Add(table3);
                                    ds.Tables.Add(table4);
                                    ds.Tables.Add(table5);
                                    ds.Tables.Add(table6);
                                    ExToexcel = new ExportToExcel();
                                    //ExToexcel.toexcel(
                                    //    lstSellerList.List.CheckedItems[i].ToString(),
                                    //    " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
                                    //    @"E:\Settlement\Temp\temp-BillSell.xls",
                                    //    @"E:\Settlement\Out\Daily\ComPany\",
                                    //    @"BillS_" + CompanyCode + ".xls", false);


                                    if (_Btype == "روزانه")
                                    {

                                        ExToexcel.toexcel(
                                                                         xcn,
                                                                          " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
                                                                          @"E:\Settlement\Temp\temp-BillSell.xls",
                                                                          @"E:\Settlement\Out\Daily\ComPany\Reghabati\",
                                                                          @"BillS_" + CompanyCode + ".xls", false);

                                    }
                                    if (_Btype == "ماهيانه")
                                    {
                                        ExToexcel.toexcel(
                                                xcn,
                                                " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
                                                @"E:\Settlement\Temp\temp-BillSell.xls",
                                                @"E:\Settlement\Out\Monthly\ComPany\Reghabati\",
                                                @"BillS_" + CompanyCode + ".xls", false);
                                    }

                                    #endregion
                                }




                                #endregion
                            }

                        }
                        #endregion
                    }
                    else
                    {
                        #region PowerPlant

                        #region  company

                        tableCN = new DataTable();

                        #region bill-CompanyName

                        #region Prepare Connection & Command

                        MySqlConnection =
                            new SqlConnection(DbBizClass.dbConnStr);
                        MySqlCommand1 = new SqlCommand
                        {
                            Connection = MySqlConnection,
                            CommandText =
                                ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillData] ")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {

                            MySqlCommand1.Connection.Open();

                            tableCN.Load(MySqlCommand1.ExecuteReader());


                            if (tableCN.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                return;
                            }

                        }
                        catch (Exception Ex)
                        {
                            MessageBox.Show(Ex.Message);
                        }
                        finally
                        {
                            MySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        for (int j = 0; j < tableCN.Rows.Count; j++)
                        {
                            xcn = tableCN.Rows[j][1].ToString();


                            if (lstSellerList.List.CheckedItems != null)
                            {
                                #region company

                                #region Intial

                                ds = new DataSet();
                                table1 = new DataTable();
                                table2 = new DataTable();
                                table3 = new DataTable();
                                table4 = new DataTable();
                                table5 = new DataTable();
                                table6 = new DataTable();
                                #endregion

                                #region bill-Table1

                                #region Prepare Connection & Command

                                MySqlConnection =
                               new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                {
                                    Connection = MySqlConnection,
                                    CommandText = ("SELECT * " +
                                                   " FROM [Settlement].[Sellers].[BillData] " +
                                                   " where CompanyName='" +
                                                   xcn + "'")
                                };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table1.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-table6-PS

                                #region Prepare Connection & Command

                                MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                {
                                    Connection = MySqlConnection,
                                    CommandText =
                                        ("SELECT DISTINCT(PowerStationCode),PowerStationName,UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                                         " FROM [Settlement].[Sellers].[BillData] " +
                                         " where CompanyName='" + xcn +
                                         "'" +
                                         "Order by PowerStationCode,PowerStationName,UnitType,BillUnitT")
                                };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table6.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Date

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                {
                                    Connection = MySqlConnection,
                                    CommandText =
                                        "SELECT   DISTINCT (Date)  Date FROM [Settlement].[Sellers].[BillData] order by Date"
                                };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table2.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Sum
                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                {
                                    Connection = MySqlConnection,
                                    CommandText =
                                        ("SELECT  *  FROM [Settlement].[Sellers].[BillIDataSum]" +
                                         "where CompanyName='" + xcn +
                                         "'" +
                                         "Order by PowerStationCode,PowerStationName,UnitType,BillUnitT,Date")
                                };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table3.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-TotalP
                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                {
                                    Connection = MySqlConnection,
                                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                                   " FROM [Settlement].[Sellers].[BillIDataTotalP]" +
                                                   " where CompanyName='" +
                                                   xcn + "'" +
                                                   "order by [PowerStationCode]")
                                };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table4.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Total
                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                {
                                    Connection = MySqlConnection,
                                    CommandText =
                                        ("SELECT  *    FROM [Settlement].[Sellers].[BillIDataTotal]" +
                                         " where CompanyName='" + xcn +
                                         "'")
                                };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table5.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-CompanyCode

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                {
                                    Connection = MySqlConnection,
                                    CommandText =
                                        ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillData] " +
                                         " where CompanyName='" + xcn +
                                         "'")
                                };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    CreateXLS = 1;
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    SqlDataReader MySqlDataReader =
                         MySqlCommand1.ExecuteReader();
                                    if (MySqlDataReader.HasRows)
                                    {
                                        while (MySqlDataReader.Read())
                                            CompanyCode = MySqlDataReader[0].ToString();
                                    }
                                    else
                                    {
                                        CreateXLS = 0;
                                    }
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                if (CreateXLS == 1)
                                {
                                    #region Bill-Excel

                                    table1.TableName = "table1";
                                    table2.TableName = "Date";
                                    table3.TableName = "Sum";
                                    table4.TableName = "TotalP";
                                    table5.TableName = "Total";
                                    table6.TableName = "PS";
                                    ds.Tables.Add(table1);
                                    ds.Tables.Add(table2);
                                    ds.Tables.Add(table3);
                                    ds.Tables.Add(table4);
                                    ds.Tables.Add(table5);
                                    ds.Tables.Add(table6);
                                    ExToexcel = new ExportToExcel();
                                    //ExToexcel.toexcel(
                                    //    lstSellerList.List.CheckedItems[i].ToString(),
                                    //    " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
                                    //    @"E:\Settlement\Temp\temp-BillSell.xls",
                                    //    @"E:\Settlement\Out\Daily\ComPany\",
                                    //    @"BillS_" + CompanyCode + ".xls", false);


                                    if (_Btype == "روزانه")
                                    {

                                        ExToexcel.toexcel(
                                                                         xcn,
                                                                          " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
                                                                          @"E:\Settlement\Temp\temp-BillSell.xls",
                                                                          @"E:\Settlement\Out\Daily\ComPany\Reghabati\",
                                                                          @"BillS_" + CompanyCode + ".xls", false);

                                    }
                                    if (_Btype == "ماهيانه")
                                    {
                                        ExToexcel.toexcel(
                                                xcn,
                                                " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
                                                @"E:\Settlement\Temp\temp-BillSell.xls",
                                                @"E:\Settlement\Out\Monthly\ComPany\Reghabati\",
                                                @"BillS_" + CompanyCode + ".xls", false);
                                    }

                                    #endregion
                                }




                                #endregion
                            }

                        }

                        #endregion

                        #region PowerPlant

                        tableCN = new DataTable();

                        #region bill-PowerStationName

                        #region Prepare Connection & Command

                        MySqlConnection =
                            new SqlConnection(DbBizClass.dbConnStr);
                        MySqlCommand1 = new SqlCommand
                        {
                            Connection = MySqlConnection,
                            CommandText =
                                ("SELECT  Distinct(PowerStationCode),PowerStationName  FROM [Settlement].[Sellers].[BillData] ")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {

                            MySqlCommand1.Connection.Open();

                            tableCN.Load(MySqlCommand1.ExecuteReader());


                            if (tableCN.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                return;
                            }

                        }
                        catch (Exception Ex)
                        {
                            MessageBox.Show(Ex.Message);
                        }
                        finally
                        {
                            MySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        for (int j = 0; j < tableCN.Rows.Count; j++)
                        {
                            xcn = tableCN.Rows[j][1].ToString();
                            if (lstPowerStations.List.CheckedItems != null)
                            {
                                #region Intial

                                ds = new DataSet();
                                table1 = new DataTable();
                                table2 = new DataTable();
                                table3 = new DataTable();
                                table4 = new DataTable();
                                table5 = new DataTable();
                                table6 = new DataTable();
                                #endregion

                                #region bill-Table1

                                #region Prepare Connection & Command

                                MySqlConnection =
                               new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText = ("SELECT * " +
                                                                       " FROM [Settlement].[Sellers].[BillData] " +
                                                                       " where PowerStationName='" +
                                                                      xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table1.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-table6-PS

                                #region Prepare Connection & Command

                                MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT DISTINCT(PowerStationCode),PowerStationName,UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                                                             " FROM [Settlement].[Sellers].[BillData] " +
                                                             " where PowerStationName='" +
                                                            xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table6.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Date

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            "SELECT   DISTINCT (Date)  Date FROM [Settlement].[Sellers].[BillData] order by Date"
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table2.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-Sum

                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  *     FROM [Settlement].[Sellers].[BillIDataSum]" +
                                                             " where PowerStationName='" +
                                                            xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table3.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-TotalP
                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                                                       " FROM [Settlement].[Sellers].[BillIDataTotalP]" +
                                                                       " where PowerStationName='" +
                                                                      xcn + "'" +
                                                                       "order by [PowerStationCode]")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table4.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-CompanyCodeName

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillData] " +
                                                             " where PowerStationName='" +
                                                            xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    SqlDataReader MySqlDataReader =
                         MySqlCommand1.ExecuteReader();
                                    if (MySqlDataReader.HasRows)
                                        while (MySqlDataReader.Read())
                                            CompanyCode = MySqlDataReader[0].ToString();

                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-PowerPlantName

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  Distinct(PowerStationLatinName),PowerStationCode FROM [Settlement].[Sellers].[BillData] " +
                                                             " where PowerStationName='" +
                                                            xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    SqlDataReader MySqlDataReader =
                         MySqlCommand1.ExecuteReader();
                                    if (MySqlDataReader != null)
                                        if (MySqlDataReader.HasRows)
                                            while (MySqlDataReader.Read())
                                                PowerStationName = MySqlDataReader[0].ToString();

                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-PowerPlantCode

                                #region Prepare Connection & Command

                                MySqlConnection =
                                    new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  Distinct(PowerStationCode) FROM [Settlement].[Sellers].[BillData] " +
                                                             " where PowerStationName='" +
                                                            xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    SqlDataReader MySqlDataReader =
                         MySqlCommand1.ExecuteReader();
                                    if (MySqlDataReader.HasRows)
                                        while (MySqlDataReader.Read())

                                            PowerStationCode = MySqlDataReader[0].ToString();
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region bill-TotalPP

                                #region Prepare Connection & Command

                                MySqlConnection =
                                   new SqlConnection(DbBizClass.dbConnStr);
                                MySqlCommand1 = new SqlCommand
                                                    {
                                                        Connection = MySqlConnection,
                                                        CommandText =
                                                            ("SELECT  *    FROM [Settlement].[Sellers].[BillIDataTotalPP]" +
                                                             " where PowerStationName='" +
                                                            xcn + "'")
                                                    };

                                #endregion

                                #region Execute Command

                                try
                                {
                                    MySqlCommand1.Connection.Open();
                                    // ReSharper disable AssignNullToNotNullAttribute
                                    table5.Load(MySqlCommand1.ExecuteReader());
                                    // ReSharper restore AssignNullToNotNullAttribute
                                }
                                catch (Exception Ex)
                                {
                                    MessageBox.Show(Ex.Message);
                                }
                                finally
                                {
                                    MySqlCommand1.Connection.Close();
                                }

                                #endregion


                                #endregion

                                #region Bill-Excel

                                table1.TableName = "table1";
                                table2.TableName = "Date";
                                table3.TableName = "Sum";
                                table4.TableName = "TotalP";
                                table5.TableName = "Total";
                                table6.TableName = "PS";
                                ds.Tables.Add(table1);
                                ds.Tables.Add(table2);
                                ds.Tables.Add(table3);
                                ds.Tables.Add(table4);
                                ds.Tables.Add(table5);
                                ds.Tables.Add(table6);
                                ExToexcel = new ExportToExcel();




                                if (_Btype == "روزانه")
                                {

                                    ExToexcel.toexcel(
                                        xcn, " از تاریخ   " + _SDate + "  تا  " + _EDate, ds, @"E:\Settlement\Temp\temp-BillSell.xls",
                                            @"E:\Settlement\Out\Daily\PowerPlant\Reghabati\",
                                            @"BillS_" + PowerStationName + " (" + PowerStationCode + ")" + ".xls", false);


                                }
                                if (_Btype == "ماهيانه")
                                {
                                    ExToexcel.toexcel(
                                                   xcn,
                                                   " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
@"E:\Settlement\Temp\temp-BillSell.xls",
@"E:\Settlement\Out\Monthly\PowerPlant\Reghabati\", @"BillS_" + PowerStationName + " (" + PowerStationCode + ")" + ".xls", false);
                                }

                                #endregion


                            }

                        }
                        #endregion

                        #endregion
                    }
                    #endregion

                    #region Rep


                    ds1Rep = new DataSet();
                    table1Rep = new DataTable();



                    #region bill-Table1

                    #region Prepare Connection & Command

                    MySqlConnection =
                   new SqlConnection(DbBizClass.dbConnStr);
                    MySqlCommand1 = new SqlCommand
                                        {
                                            Connection = MySqlConnection,
                                            CommandText = ("SELECT * " +
                                                           " FROM [Settlement].[Sellers].[BillDataRep]" +
                                                           " ORDER BY PSC,PSN,UnitType  ")
                                        };

                    #endregion

                    #region Execute Command

                    try
                    {
                        MySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        table1Rep.Load(MySqlCommand1.ExecuteReader());
                        // ReSharper restore AssignNullToNotNullAttribute
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }
                    finally
                    {
                        MySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Bill-Excel

                    table1Rep.TableName = "table1";
                    ds1Rep.Tables.Add(table1Rep);
                    ExToexcel = new ExportToExcel();




                    if (_Btype == "روزانه")
                    {

                        ExToexcel.toexcel("",
                  " از تاریخ   " + _SDate + "  تا  " + _SDate, ds1Rep,
                  @"E:\Settlement\Temp\temp-Rep_Buy.xls",
                  @"E:\Settlement\Out\Daily\",
                  @"Rep_Buy.xls", false);


                    }
                    if (_Btype == "ماهيانه")
                    {
                        ExToexcel.toexcel("",
                                     " از تاریخ   " + _SDate + "  تا  " + _EDate, ds1Rep,
                                     @"E:\Settlement\Temp\temp-Rep_Buy.xls",
                                     @"E:\Settlement\Out\Monthly\",
                                     @"Rep_Buy.xls", false);

                    }


                    #endregion

                    #endregion

                   #region companyTaz



//                    if (lstPowerStations.List.CheckedItems.Count == 0)
//                    {
//                        #region Company

//                        tableCN = new DataTable();

//                        #region bill-CompanyName

//                        #region Prepare Connection & Command

//                        MySqlConnection =
//                            new SqlConnection(DbBizClass.dbConnStr);
//                        MySqlCommand1 = new SqlCommand
//                        {
//                            Connection = MySqlConnection,
//                            CommandText =
//                                ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillDataZ] ")
//                        };

//                        #endregion

//                        #region Execute Command

//                        try
//                        {

//                            MySqlCommand1.Connection.Open();

//                            tableCN.Load(MySqlCommand1.ExecuteReader());


//                            if (tableCN.Rows.Count > 0)
//                            {

//                            }
//                            else
//                            {
//                                return;
//                            }

//                        }
//                        catch (Exception Ex)
//                        {
//                            MessageBox.Show(Ex.Message);
//                        }
//                        finally
//                        {
//                            MySqlCommand1.Connection.Close();
//                        }

//                        #endregion


//                        #endregion

//                        for (int j = 0; j < tableCN.Rows.Count; j++)
//                        {
//                            xcn = tableCN.Rows[j][1].ToString(); 

//                            if (lstSellerList.List.CheckedItems != null)
//                            {

//                                #region Intial

//                                ds = new DataSet();
//                                table1 = new DataTable();
//                                table2 = new DataTable();
//                                table3 = new DataTable();
//                                table4 = new DataTable();
//                                table5 = new DataTable();
//                                table6 = new DataTable();
//                                table7 = new DataTable();
//                                table8 = new DataTable();
//                                table9 = new DataTable();

//                                #endregion

//                                #region bill-Table1

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                               new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT * " +
//                                                   " FROM [Settlement].[Sellers].[BillDataZ] " +
//                                                   " where CompanyName='" +
//                                                  xcn+ "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table1.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-table6-PS

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT DISTINCT(PowerStationCode),PowerStationName,UnitType,UnitCode,[PowerStationCode] as PsCT" +
//                                         " FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where CompanyName='" + xcn +
//                                         "'" +
//                                         "Order by PowerStationCode,PowerStationName,UnitType,UnitCode")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table6.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Date

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        "SELECT   DISTINCT (Date)  Date FROM [Settlement].[Sellers].[BillDataZ] order by Date"
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table2.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Sum
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  *  FROM [Settlement].[Sellers].[BillIDataSumZ]" +
//                                         "where CompanyName='" + xcn +
//                                         "'" +
//                                         "Order by PowerStationCode,PowerStationName,UnitType,UnitCode,Date")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table3.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-TotalP
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
//                                                   " FROM [Settlement].[Sellers].[BillIDataTotalPZ]" +
//                                                   " where CompanyName='" +
//                                                   xcn + "'" +
//                                                   "order by [PowerStationCode]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table4.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Total
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  *    FROM [Settlement].[Sellers].[BillIDataTotalZ]" +
//                                         " where CompanyName='" + xcn +
//                                         "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table5.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-CompanyCode

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where CompanyName='" + xcn +
//                                         "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    CreateXLS = 1;
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    SqlDataReader MySqlDataReader =
//                         MySqlCommand1.ExecuteReader();
//                                    if (MySqlDataReader.HasRows)
//                                    {
//                                        while (MySqlDataReader.Read())
//                                            CompanyCode = MySqlDataReader[0].ToString();
//                                    }
//                                    else
//                                    {
//                                        CreateXLS = 0;
//                                    }
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Unit

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        "SELECT   DISTINCT (UnitCode)  UnitCode FROM [Settlement].[Sellers].[BillDataZ] "+
//                                          " where CompanyName='" + xcn +
//                                         "'"+
//                                       " order by UnitCode"
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table7.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-BillU
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
//                                                   " FROM [Settlement].[Sellers].[BillIDataBillUZ]" +
//                                                   " where CompanyName='" +
//                                                  xcn + "'" +
//                                                   "order by [PowerStationCode]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table8.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-BillRate
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT * " +
//                                                   " FROM [Settlement].[Sellers].[BillIDataRateZ]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table9.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                if (CreateXLS == 1)
//                                {
//                                    #region Bill-Excel

//                                    table1.TableName = "table1";
//                                    table2.TableName = "Date";
//                                    table3.TableName = "Sum";
//                                    table4.TableName = "TotalP";
//                                    table5.TableName = "Total";
//                                    table6.TableName = "PS";
//                                    table7.TableName = "Unit";
//                                    table8.TableName = "BillU";
//                                    table9.TableName = "BillRate";


//                                    ds.Tables.Add(table1);
//                                    ds.Tables.Add(table2);
//                                    ds.Tables.Add(table3);
//                                    ds.Tables.Add(table4);
//                                    ds.Tables.Add(table5);
//                                    ds.Tables.Add(table6);
//                                    ds.Tables.Add(table7);
//                                    ds.Tables.Add(table8);
//                                    ds.Tables.Add(table9);


//                                    ExToexcel = new ExportToExcel();
               

//                                    if (_Btype == "روزانه")
//                                    {

//                                        ExToexcel.toexcel(
//                                                                         xcn ,
//                                                                          " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//                                                                          @"E:\Settlement\Temp\temp-BillSellt.xls",
//                                                                          @"E:\Settlement\Out\Daily\ComPany\",
//                                                                          @"BillS_" + CompanyCode + ".xls", false);

//                                    }
//                                    if (_Btype == "ماهيانه")
//                                    {
//                                        ExToexcel.toexcel(
//                                                xcn ,
//                                                " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//                                                @"E:\Settlement\Temp\temp-BillSellt1.xls",
//                                                @"E:\Settlement\Out\Monthly\ComPany\Tazmini\",
//                                                @"BillS_" + CompanyCode + ".xls", false);
//                                    }

//                                    #endregion
//                                }


            
//                            }

//                        }
//                        #endregion
//                    }

//                    else
//                    {
//                        #region PowerPlant

//                        #region Company

//                        tableCN = new DataTable();

//                        #region bill-CompanyName

//                        #region Prepare Connection & Command

//                        MySqlConnection =
//                            new SqlConnection(DbBizClass.dbConnStr);
//                        MySqlCommand1 = new SqlCommand
//                        {
//                            Connection = MySqlConnection,
//                            CommandText =
//                                ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillDataZ] ")
//                        };

//                        #endregion

//                        #region Execute Command

//                        try
//                        {

//                            MySqlCommand1.Connection.Open();

//                            tableCN.Load(MySqlCommand1.ExecuteReader());


//                            if (tableCN.Rows.Count > 0)
//                            {

//                            }
//                            else
//                            {
//                                return;
//                            }

//                        }
//                        catch (Exception Ex)
//                        {
//                            MessageBox.Show(Ex.Message);
//                        }
//                        finally
//                        {
//                            MySqlCommand1.Connection.Close();
//                        }

//                        #endregion


//                        #endregion

//                        for (int j = 0; j < tableCN.Rows.Count; j++)
//                        {
//                            xcn = tableCN.Rows[j][1].ToString();

//                            if (lstSellerList.List.CheckedItems != null)
//                            {

//                                #region Intial

//                                ds = new DataSet();
//                                table1 = new DataTable();
//                                table2 = new DataTable();
//                                table3 = new DataTable();
//                                table4 = new DataTable();
//                                table5 = new DataTable();
//                                table6 = new DataTable();
//                                table7 = new DataTable();
//                                table8 = new DataTable();
//                                table9 = new DataTable();

//                                #endregion

//                                #region bill-Table1

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                               new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT * " +
//                                                   " FROM [Settlement].[Sellers].[BillDataZ] " +
//                                                   " where CompanyName='" +
//                                                  xcn + "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table1.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-table6-PS

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT DISTINCT(PowerStationCode),PowerStationName,UnitType,UnitCode,[PowerStationCode] as PsCT" +
//                                         " FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where CompanyName='" + xcn +
//                                         "'" +
//                                         "Order by PowerStationCode,PowerStationName,UnitType,UnitCode")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table6.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Date

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        "SELECT   DISTINCT (Date)  Date FROM [Settlement].[Sellers].[BillDataZ] order by Date"
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table2.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Sum

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  *  FROM [Settlement].[Sellers].[BillIDataSumZ]" +
//                                         "where CompanyName='" + xcn +
//                                         "'" +
//                                         "Order by PowerStationCode,PowerStationName,UnitType,UnitCode,Date")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table3.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-TotalP
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
//                                                   " FROM [Settlement].[Sellers].[BillIDataTotalPZ]" +
//                                                   " where CompanyName='" +
//                                                   xcn + "'" +
//                                                   "order by [PowerStationCode]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table4.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Total
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  *    FROM [Settlement].[Sellers].[BillIDataTotalZ]" +
//                                         " where CompanyName='" + xcn +
//                                         "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table5.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-CompanyCode

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  Distinct(CompanyCode),CompanyName FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where CompanyName='" + xcn +
//                                         "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    CreateXLS = 1;
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    SqlDataReader MySqlDataReader =
//                         MySqlCommand1.ExecuteReader();
//                                    if (MySqlDataReader.HasRows)
//                                    {
//                                        while (MySqlDataReader.Read())
//                                            CompanyCode = MySqlDataReader[0].ToString();
//                                    }
//                                    else
//                                    {
//                                        CreateXLS = 0;
//                                    }
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Unit

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        "SELECT   DISTINCT (UnitCode)  UnitCode FROM [Settlement].[Sellers].[BillDataZ] " +
//                                          " where CompanyName='" + xcn +
//                                         "'" +
//                                       " order by UnitCode"
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table7.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-BillU
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
//                                                   " FROM [Settlement].[Sellers].[BillIDataBillUZ]" +
//                                                   " where CompanyName='" +
//                                                  xcn + "'" +
//                                                   "order by [PowerStationCode]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table8.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-BillRate

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT * " +
//                                                   " FROM [Settlement].[Sellers].[BillIDataRateZ]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table9.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                if (CreateXLS == 1)
//                                {
//                                    #region Bill-Excel

//                                    table1.TableName = "table1";
//                                    table2.TableName = "Date";
//                                    table3.TableName = "Sum";
//                                    table4.TableName = "TotalP";
//                                    table5.TableName = "Total";
//                                    table6.TableName = "PS";
//                                    table7.TableName = "Unit";
//                                    table8.TableName = "BillU";
//                                    table9.TableName = "BillRate";


//                                    ds.Tables.Add(table1);
//                                    ds.Tables.Add(table2);
//                                    ds.Tables.Add(table3);
//                                    ds.Tables.Add(table4);
//                                    ds.Tables.Add(table5);
//                                    ds.Tables.Add(table6);
//                                    ds.Tables.Add(table7);
//                                    ds.Tables.Add(table8);
//                                    ds.Tables.Add(table9);


//                                    ExToexcel = new ExportToExcel();
//                                    //ExToexcel.toexcel(
//                                    //    lstSellerList.List.CheckedItems[i].ToString(),
//                                    //    " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//                                    //    @"E:\Settlement\Temp\temp-BillSell.xls",
//                                    //    @"E:\Settlement\Out\Daily\ComPany\",
//                                    //    @"BillS_" + CompanyCode + ".xls", false);


//                                    if (_Btype == "روزانه")
//                                    {

//                                        ExToexcel.toexcel(
//                                                                         xcn ,
//                                                                          " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//                                                                          @"E:\Settlement\Temp\temp-BillSellt.xls",
//                                                                          @"E:\Settlement\Out\Daily\ComPany\",
//                                                                          @"BillS_" + CompanyCode + ".xls", false);

//                                    }
//                                    if (_Btype == "ماهيانه")
//                                    {
//                                        ExToexcel.toexcel(
//                                               xcn ,
//                                                " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//                                                @"E:\Settlement\Temp\temp-BillSellt1.xls",
//                                                @"E:\Settlement\Out\Monthly\ComPany\Tazmini\",
//                                                @"BillS_" + CompanyCode + ".xls", false);
//                                    }

//                                    #endregion
//                                }



//                            }

//                        }
//                        #endregion

//                        #region PowerPlant

//                        tableCN = new DataTable();

//                        #region bill-PowerStationName

//                        #region Prepare Connection & Command

//                        MySqlConnection =
//                            new SqlConnection(DbBizClass.dbConnStr);
//                        MySqlCommand1 = new SqlCommand
//                        {
//                            Connection = MySqlConnection,
//                            CommandText =
//                                ("SELECT  Distinct(PowerStationCode),PowerStationName FROM [Settlement].[Sellers].[BillDataZ] ")
//                        };

//                        #endregion

//                        #region Execute Command

//                        try
//                        {

//                            MySqlCommand1.Connection.Open();

//                            tableCN.Load(MySqlCommand1.ExecuteReader());


//                            if (tableCN.Rows.Count > 0)
//                            {

//                            }
//                            else
//                            {
//                                return;
//                            }

//                        }
//                        catch (Exception Ex)
//                        {
//                            MessageBox.Show(Ex.Message);
//                        }
//                        finally
//                        {
//                            MySqlCommand1.Connection.Close();
//                        }

//                        #endregion


//                        #endregion

//                        for (int j = 0; j < tableCN.Rows.Count; j++)
//                        {
//                            xcn = tableCN.Rows[j][1].ToString();

//                            if (lstSellerList.List.CheckedItems != null)
//                            {

//                                #region Intial

//                                ds = new DataSet();
//                                table1 = new DataTable();
//                                table2 = new DataTable();
//                                table3 = new DataTable();
//                                table4 = new DataTable();
//                                table5 = new DataTable();
//                                table6 = new DataTable();
//                                table7 = new DataTable();
//                                table8 = new DataTable();
//                                table9 = new DataTable();

//                                #endregion

//                                #region bill-Table1

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                               new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT * " +
//                                                   " FROM [Settlement].[Sellers].[BillDataZ] " +
//                                                   " where PowerStationName='" +
//                                                  xcn + "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table1.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-table6-PS

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT DISTINCT(PowerStationCode),PowerStationName,UnitType,UnitCode,[PowerStationCode] as PsCT" +
//                                         " FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where PowerStationName='" + xcn +
//                                         "'" +
//                                         "Order by PowerStationCode,PowerStationName,UnitType,UnitCode")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table6.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Date

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        "SELECT   DISTINCT (Date)  Date FROM [Settlement].[Sellers].[BillDataZ] order by Date"
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table2.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Sum
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  *  FROM [Settlement].[Sellers].[BillIDataSumZ]" +
//                                         "where PowerStationName='" + xcn +
//                                         "'" +
//                                         "Order by PowerStationCode,PowerStationName,UnitType,UnitCode,Date")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table3.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-TotalP
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
//                                                   " FROM [Settlement].[Sellers].[BillIDataTotalPZ]" +
//                                                   " where PowerStationName='" +
//                                                   xcn + "'" +
//                                                   "order by [PowerStationCode]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table4.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Total
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  *    FROM [Settlement].[Sellers].[BillIDataTotalPPZ]" +
//                                         " where PowerStationName='" + xcn +
//                                         "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table5.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-PowerStationName

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  Distinct(PowerStationLatinName),PowerStationCode FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where PowerStationName='" +
//                                        xcn + "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    SqlDataReader MySqlDataReader =
//                         MySqlCommand1.ExecuteReader();
//                                    if (MySqlDataReader != null)
//                                        if (MySqlDataReader.HasRows)
//                                            while (MySqlDataReader.Read())
//                                                PowerStationName = MySqlDataReader[0].ToString();

//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-PowerStationCode

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  Distinct(PowerStationCode) FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where PowerStationName='" +
//                                        xcn + "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    SqlDataReader MySqlDataReader =
//                         MySqlCommand1.ExecuteReader();
//                                    if (MySqlDataReader.HasRows)
//                                        while (MySqlDataReader.Read())

//                                            PowerStationCode = MySqlDataReader[0].ToString();
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Unit

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        "SELECT   DISTINCT (UnitCode)  UnitCode FROM [Settlement].[Sellers].[BillDataZ] " +
//                                          " where PowerStationName='" + xcn +
//                                         "'" +
//                                       " order by UnitCode"
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table7.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-BillU
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
//                                                   " FROM [Settlement].[Sellers].[BillIDataBillUZ]" +
//                                                   " where PowerStationName='" +
//                                                  xcn + "'" +
//                                                   "order by [PowerStationCode]")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table8.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                #region bill-Unit-MarketState

//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                    new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                {
//                                    Connection = MySqlConnection,
//                                    CommandText =
//                                        ("SELECT  Distinct(MarketStateCode) FROM [Settlement].[Sellers].[BillDataZ] " +
//                                         " where PowerStationName='" +
//                                        xcn + "'")
//                                };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    SqlDataReader MySqlDataReader =
//                         MySqlCommand1.ExecuteReader();
//                                    if (MySqlDataReader.HasRows)
//                                        while (MySqlDataReader.Read())

//                                            MarketState = MySqlDataReader[0].ToString();
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion


//                                #region bill-BillRate
//                                #region Prepare Connection & Command

//                                MySqlConnection =
//                                   new SqlConnection(DbBizClass.dbConnStr);
//                                MySqlCommand1 = new SqlCommand
//                                                    {
//                                                        Connection = MySqlConnection,
//                                                        CommandText = ("SELECT * " +
//                                                                       " FROM [Settlement].[Sellers].[BillIDataRateZ]" +
//                                                                       "  where MarketStateCode='" +
//                                                                       MarketState + "'")
//                                                    };

//                                #endregion

//                                #region Execute Command

//                                try
//                                {
//                                    MySqlCommand1.Connection.Open();
//                                    // ReSharper disable AssignNullToNotNullAttribute
//                                    table9.Load(MySqlCommand1.ExecuteReader());
//                                    // ReSharper restore AssignNullToNotNullAttribute
//                                }
//                                catch (Exception Ex)
//                                {
//                                    MessageBox.Show(Ex.Message);
//                                }
//                                finally
//                                {
//                                    MySqlCommand1.Connection.Close();
//                                }

//                                #endregion


//                                #endregion

//                                if (CreateXLS == 1)
//                                {
//                                    #region Bill-Excel

//                                    table1.TableName = "table1";
//                                    table2.TableName = "Date";
//                                    table3.TableName = "Sum";
//                                    table4.TableName = "TotalP";
//                                    table5.TableName = "Total";
//                                    table6.TableName = "PS";
//                                    table7.TableName = "Unit";
//                                    table8.TableName = "BillU";
//                                    table9.TableName = "BillRate";


//                                    ds.Tables.Add(table1);
//                                    ds.Tables.Add(table2);
//                                    ds.Tables.Add(table3);
//                                    ds.Tables.Add(table4);
//                                    ds.Tables.Add(table5);
//                                    ds.Tables.Add(table6);
//                                    ds.Tables.Add(table7);
//                                    ds.Tables.Add(table8);
//                                    ds.Tables.Add(table9);


//                                    ExToexcel = new ExportToExcel();
//                                    //ExToexcel.toexcel(
//                                    //    lstSellerList.List.CheckedItems[i].ToString(),
//                                    //    " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//                                    //    @"E:\Settlement\Temp\temp-BillSell.xls",
//                                    //    @"E:\Settlement\Out\Daily\ComPany\",
//                                    //    @"BillS_" + CompanyCode + ".xls", false);


//                                    if (_Btype == "روزانه")
//                                    {

//                                        //ExToexcel.toexcel(
//                                        //                                 xcn,
//                                        //                                  " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//                                        //                                  @"E:\Settlement\Temp\temp-BillSellt.xls",
//                                        //                                  @"E:\Settlement\Out\Daily\ComPany\",
//                                        //                                  @"BillS_" + CompanyCode + ".xls", false);

//                                    }
//                                    if (_Btype == "ماهيانه")
//                                    {
//                                        ExToexcel.toexcel(xcn,
//                               " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
//@"E:\Settlement\Temp\temp-BillSellt1.xls",
//@"E:\Settlement\Out\Monthly\PowerPlant\Tazmini\", @"BillS_" + PowerStationName + " (" + PowerStationCode + ")" + ".xls", false);
//                                    }

//                                    #endregion
//                                }



//                            }

//                        }
//                        #endregion
              
                        

//                        #endregion
//                    }
                 #endregion

                    break;

                case "Buyers":

                    #region Make Excel Buyers

                    #region bill
                    foreach (var x in lstSellerList.List.CheckedItems)
                    {
                        if (lstSellerList.List.CheckedItems != null)
                        {
                            ds = new DataSet();
                            table1 = new DataTable();
                            table2 = new DataTable();
                            table3 = new DataTable();
                            table4 = new DataTable();

                            #region bill-Table1
                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                                                {
                                                    Connection = MySqlConnection,
                                                    CommandText =
                                                        "SELECT  *     FROM [Settlement].[Buyers].[BillDisplay]" +
                                                        " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                        "'"
                                                };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table1.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-Date
                            #region Prepare Connection & Command

                            MySqlConnection =
                                new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                            {
                                Connection = MySqlConnection,
                                CommandText =
                                    "SELECT   DISTINCT (Date)  Date   FROM [Settlement].[Buyers].[BillDisplay] order by Date"
                            };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table2.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-Sum
                            #region Prepare Connection & Command

                            MySqlConnection =
                               new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                            {
                                Connection = MySqlConnection,
                                CommandText = "SELECT  *   FROM [Settlement].[Buyers].[BillDataSum]" +
                                                        " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                        "'"
                            };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table3.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            #region bill-Total
                            #region Prepare Connection & Command

                            MySqlConnection =
                               new SqlConnection(DbBizClass.dbConnStr);
                            MySqlCommand1 = new SqlCommand
                            {
                                Connection = MySqlConnection,
                                CommandText = "SELECT  *   FROM [Settlement].[Buyers].[BillDataTotal]" +
                                                        " where CompanyName='" + lstSellerList.List.CheckedItems[i] +
                                                        "'"
                            };

                            #endregion

                            #region Execute Command

                            try
                            {
                                MySqlCommand1.Connection.Open();
                                // ReSharper disable AssignNullToNotNullAttribute
                                table4.Load(MySqlCommand1.ExecuteReader());
                                // ReSharper restore AssignNullToNotNullAttribute
                            }
                            catch (Exception Ex)
                            {
                                MessageBox.Show(Ex.Message);
                            }
                            finally
                            {
                                MySqlCommand1.Connection.Close();
                            }

                            #endregion


                            #endregion

                            table1.TableName = "table1";
                            table2.TableName = "Date";
                            table3.TableName = "Sum";
                            table4.TableName = "Total";
                            ds.Tables.Add(table1);
                            ds.Tables.Add(table2);
                            ds.Tables.Add(table3);
                            ds.Tables.Add(table4);
                            ExToexcel = new ExportToExcel();
                            ExToexcel.toexcel(
                                  lstSellerList.List.CheckedItems[i].ToString(), " از تاریخ   " + _SDate + "  تا  " + _EDate, ds, @"E:\Settlement\Bazar\Excel\Temp\temp-BillBuy1.xls",
                                  @"E:\Settlement\Bazar\Excel\Out\Sell\",
                                  @"BillB_" + lstSellerList.List.CheckedItems[i] + ".xls", false);

                            i = i + 1;
                        }
                    }
                    #endregion 

                    

                    #endregion

                    break;
                case "Transfer":

                    

                    break;
            }
        }
        #endregion

        #region Fill Sellers type
        /// <summary>
        /// تكمیل لیست باكس فروشندگان
        /// </summary>
        private void FillSellers()
        {
            // رشته فرمان شیء را خالی می كند

            _SqlCommand.CommandText = string.Empty;
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstSellerType.List.Items.Clear();
            lstSellerList.List.Items.Clear();
            _SqlCommand.Connection = _SqlConnection;

            //------------------
            #region Fill list

            switch (_PtypeE)
            {
                case "Sellers":

                    #region  Sellers
                    lstPowerStations.List.Items.Clear();
                    _SqlCommand.CommandText = "SELECT distinct ([TypeName]) [TypeName] FROM" +
                                       "[Settlement].[dbo].[View_Company_PowerPlant] where IsSeller=1";
                    #endregion

                    break;

                case "Buyers":

                    #region Buyers
                    _SqlCommand.CommandText = "SELECT distinct ([TypeName]) [TypeName] FROM" +
                                      "[Settlement].[Views].[EMS_S_Company] where IsBuyer=1";

                    #endregion

                    break;

                case "Transfer":

                    #region  Transfer

                    _SqlCommand.CommandText = "SELECT distinct ([TypeName]) [TypeName] FROM" +
                                       "[Settlement].[Views].[EMS_S_Company] where IsXmitter=1";
                    #endregion

                    break;
            }
            #endregion
            //------------------------

            lstSellerType.TheSqlConnection = _SqlConnection;
            lstSellerType.TheSqlCommand = _SqlCommand;
            lstSellerType.FillListBox();
        }
        #endregion

        #region Fill Sellers List
        /// <summary>
        /// تكمیل كننده ی لیست فروشندگان
        /// </summary>
        private void FillSellersList(object o, EventArgs e)
        {
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstSellerList.List.Items.Clear();
            lstPowerStations.List.Items.Clear();

            // متن فرمان اجرایی را پاك می كند
            _SqlCommand.CommandText = String.Empty;

            // حذف عناصر در صورتی كه گزینه ای انتخاب نشده باشد
            #region First List Box Checked Items = 0
            if (lstSellerType.List.CheckedItems.Count == 0)
            {
                lstSellerList.List.Items.Clear();
                // از تابع خارج می گردد
                return;
            }
            #endregion

            // بررسی موقعیتی كه تنها یك گزینه انتخاب شده
            #region First List Box Checked Items = 1
            if (lstSellerType.List.CheckedItems.Count == 1)
            {
                // افزودن پارامتر به دستور
                _SqlCommand.Parameters.Add("@Param1", SqlDbType.NVarChar, 50);
                // تخصیص مقدار رشته ای انتخاب شده به پارامتر
                _SqlCommand.Parameters[0].Value =
                    lstSellerType.List.CheckedItems[0].ToString();
                // تولید فرمان اجرایی اس كیو ال
                //************
                #region Fill list

                switch (_PtypeE)
                {
                    case "Sellers":
                        #region  Sellers
                        _SqlCommand.CommandText =
                    " SELECT  distinct ([CompanyName]) [CompanyName] " +
                    " FROM [Settlement].[dbo].[View_Company_PowerPlant] " +
                    " WHERE IsSeller = 1 AND TypeName = @Param1 ";
                        #endregion
                        break;

                    case "Buyers":
                        #region Buyers
                        _SqlCommand.CommandText =
            " SELECT  distinct ([Name]) [Name]  " +
            " FROM [Settlement].[Views].[EMS_S_Company] " +
            " WHERE IsBuyer = 1 AND TypeName = @Param1 ";

                        #endregion
                        break;

                    case "Transfer":
                        #region  Transfer
                        _SqlCommand.CommandText =
                   " SELECT  distinct ([Name]) [Name] " +
                   " FROM [Settlement].[Views].[EMS_S_Company] " +
                   " WHERE IsXmitter = 1 AND TypeName = @Param1 ";

                        #endregion
                        break;
                }
                #endregion
                //**************

            }
            #endregion

            // بررسی موقعیتی كه بیش از  یك گزینه انتخاب شده
            #region First List Box Checked Items > 1

            if (lstSellerType.List.CheckedItems.Count > 1)
            {
                // حلقه ای برای پیمایش عناصر به اندازه آیتم های چك شده
                for (int i = 0; i < lstSellerType.List.CheckedItems.Count; i++)
                {
                    // ایجاد شماره پارامتر - یكی بیشتر از شمارنده
                    string ParaName = "@Param" + i + 1;
                    // افزودن پارامتر به فرمان اجرایی بر اساس نام آن
                    _SqlCommand.Parameters.Add(ParaName, SqlDbType.NVarChar, 50);
                    // تخصیص مقدار آیتم انتخاب شده با اندیس شمارنده به پارامتر
                    _SqlCommand.Parameters[i].Value =
                        lstSellerType.List.CheckedItems[i].ToString();
                    // ------------------------------------------------------
                    // بررسی اینكه شمارنده اولین عنصر را بررسی می نماید یا خیر
                    if (i == 0)
                    {
                        #region Fill list

                        switch (_PtypeE)
                        {
                            case "Sellers":
                                #region  Sellers

                                _SqlCommand.CommandText =
                               "SELECT distinct ([CompanyName]) [CompanyName]" +
                               "FROM [Settlement].[dbo].[View_Company_PowerPlant] " +
                               " WHERE IsSeller = 1 AND TypeName = " + ParaName;
                                #endregion
                                break;

                            case "Buyers":
                                #region Buyers
                                _SqlCommand.CommandText =
                              "SELECT distinct ([Name]) [Name]" +
                               "FROM [Settlement].[Views].[EMS_S_Company] " +
                               " WHERE IsBuyer = 1 AND TypeName = " + ParaName;

                                #endregion
                                break;

                            case "Transfer":
                                #region  Transfer

                                _SqlCommand.CommandText =
                               "SELECT distinct ([Name]) [Name]" +
                               "FROM [Settlement].[Views].[EMS_S_Company] " +
                               " WHERE IsXmitter = 1 AND TypeName = " + ParaName;

                                #endregion
                                break;
                        }
                        #endregion

                    }
                    else
                    {
                        #region Fill list

                        switch (_PtypeE)
                        {
                            case "Sellers":
                                #region  Sellers

                                _SqlCommand.CommandText +=
                         " UNION SELECT distinct ([CompanyName]) [CompanyName]" +
                         " FROM [Settlement].[dbo].[View_Company_PowerPlant]" +
                         " WHERE IsSeller = 1 AND TypeName = " + ParaName;
                                #endregion
                                break;

                            case "Buyers":
                                #region Buyers
                                _SqlCommand.CommandText +=
                        " UNION SELECT distinct ([Name]) [Name]" +
                         " FROM [Settlement].[Views].[EMS_S_Company]" +
                          " WHERE  IsBuyer = 1 AND TypeName = " + ParaName;

                                #endregion
                                break;

                            case "Transfer":
                                #region  Transfer

                                _SqlCommand.CommandText +=
                         " UNION SELECT distinct ([Name]) [Name]" +
                         " FROM [Settlement].[Views].[EMS_S_Company]" +
                         " WHERE  IsXmitter = 1 AND TypeName = " + ParaName;

                                #endregion
                                break;
                        }
                        #endregion

                    }
                }
            }

            #endregion

            // هنگامی رخ می دهد كه حداقل یك عنصر انتخاب شده باشد
            #region First ListBox CheckItems Count > 0
            // رشته اتصال اصلی را به شیء اتصال نصبت می دهد
            _SqlConnection.ConnectionString = _ConnectionString;
            // شیء اتصال را به شیء اتصال لیست باكس نصبت می دهد
            lstSellerList.TheSqlConnection = _SqlConnection;
            // شیء فرمان اس كیو ال را به شیء فرمان لیست باكس نصب می دهد
            lstSellerList.TheSqlCommand = _SqlCommand;
            // لیست باكس را تكمیل می كند
            lstSellerList.FillListBox();
            // كلیه پارامتر های ایجاد شده را حذف می نماید
            lstSellerList.TheSqlCommand.Parameters.Clear();
            _SqlCommand.Parameters.Clear();
            #endregion

        }

        #endregion

        #region Fill PowerStations
        private void FillPowerStations(object o, EventArgs e)
        {
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstPowerStations.List.Items.Clear();

            // متن فرمان اجرایی را پاك می كند
            _SqlCommand.CommandText = String.Empty;

            // حذف عناصر در صورتی كه گزینه ای انتخاب نشده باشد
            #region First List Box Checked Items = 0
            if (lstSellerList.List.CheckedItems.Count == 0)
            {
                lstPowerStations.List.Items.Clear();
                // از تابع خارج می گردد
                return;
            }
            #endregion

            // بررسی موقعیتی كه تنها یك گزینه انتخاب شده
            #region First List Box Checked Items = 1
            if (lstSellerList.List.CheckedItems.Count == 1)
            {
                // افزودن پارامتر به دستور
                _SqlCommand.Parameters.Add("@Param1", SqlDbType.NVarChar, 50);
                // تخصیص مقدار رشته ای انتخاب شده به پارامتر
                _SqlCommand.Parameters[0].Value =
                    lstSellerList.List.CheckedItems[0].ToString();
                // تولید فرمان اجرایی اس كیو ال
                _SqlCommand.CommandText =
                    "SELECT Distinct([PName]) FROM [Settlement].[dbo].[View_Company_PowerPlant]" +
                    "Where IsSeller = 1 AND CompanyName = @Param1";

            }
            #endregion

            // بررسی موقعیتی كه بیش از  یك گزینه انتخاب شده
            #region First List Box Checked Items > 1

            if (lstSellerList.List.CheckedItems.Count > 1)
            {
                // حلقه ای برای پیمایش عناصر به اندازه آیتم های چك شده
                for (int i = 0; i < lstSellerList.List.CheckedItems.Count; i++)
                {
                    // ایجاد شماره پارامتر - یكی بیشتر از شمارنده
                    string ParaName = "@Param" + i + 1;
                    // افزودن پارامتر به فرمان اجرایی بر اساس نام آن
                    _SqlCommand.Parameters.Add(ParaName, SqlDbType.NVarChar, 50);
                    // تخصیص مقدار آیتم انتخاب شده با اندیس شمارنده به پارامتر
                    _SqlCommand.Parameters[i].Value =
                        lstSellerList.List.CheckedItems[i].ToString();
                    // ------------------------------------------------------
                    // بررسی اینكه شمارنده اولین عنصر را بررسی می نماید یا خیر
                    if (i == 0)
                    {
                        _SqlCommand.CommandText =
                            " SELECT Distinct([PName]) FROM [Settlement].[dbo].[View_Company_PowerPlant]" +
                            "Where IsSeller = 1 AND CompanyName =" + ParaName ;

                    }
                    else
                    {
                        _SqlCommand.CommandText +=
                            " UNION SELECT [PName] FROM [Settlement].[dbo].[View_Company_PowerPlant]" +
                                "Where IsSeller = 1 AND CompanyName =" + ParaName ;

                    }
                }
            }

            #endregion

            // هنگامی رخ می دهد كه حداقل یك عنصر انتخاب شده باشد
            #region First ListBox CheckItems Count > 0

            // رشته اتصال اصلی را به شیء اتصال نصبت می دهد
            _SqlConnection.ConnectionString = _ConnectionString;
            // شیء اتصال را به شیء اتصال لیست باكس نصبت می دهد
            lstPowerStations.TheSqlConnection = _SqlConnection;
            // شیء فرمان اس كیو ال را به شیء فرمان لیست باكس نصب می دهد
            lstPowerStations.TheSqlCommand = _SqlCommand;
            // لیست باكس را تكمیل می كند
            lstPowerStations.FillListBox();
            // كلیه پارامتر های ایجاد شده را حذف می نماید
            lstPowerStations.TheSqlCommand.Parameters.Clear();
            _SqlCommand.Parameters.Clear();

            #endregion

        }
        #endregion

        #region Fill_Ems_Settlement
        private void Fill_EmsSettlement()
        {
            PersianDate OD = PersianDateConverter.ToPersianDate
(DateTime.Now);
            string cdate = OD.ToString();
            var iii = cdate.Substring(0, 10) + "-" + PersianDateConverter.ToPersianDate
                                (DateTime.Now).Time + ".00";
            _DbSM.SP_Fill_EmsSettlement(Convert.ToChar("S"), iii, "D");
        }

        #endregion

        #endregion

    }
}