﻿using System;
using System.Windows.Forms;

namespace MohanirPouya.Forms.Help
{
    partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }

    }
}