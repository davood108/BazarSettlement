﻿#region using
using System;
using System.Globalization;
#endregion

namespace MohanirPouya.Classes
{
    public class DbBizClass
    {

        #region Fields

        #region static PersianCalendar _PersianCal
        /// <summary>
        /// یك نمونه از كلاس تقویم پارسی
        /// </summary>
        private static PersianCalendar _PersianCal;
        #endregion

        #region static String _DbSqlConnectionString
        private static String _DbSqlConnectionString;
        #endregion

        #region static Int32 _CurrentUserID
        private static Int32 _CurrentUserID;
        #endregion

        #region static string _CurrentUserName
        private static String _CurrentUserName;
        #endregion

        #region static string _CurrentUserLastName
        private static String _CurrentUserLastName;
        #endregion

        #region static Int32 _CurrentUserType
        private static Int32 _CurrentUserType;
        #endregion

        #endregion

        #region Properties

        #region Database SqlConnection Property
        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static String dbConnStr
        {
            get { return _DbSqlConnectionString; }
            set { _DbSqlConnectionString = value; }
        }
        #endregion

        #region static Int32 CurrentUserID
        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static Int32 CurrentUserID
        {
            get { return _CurrentUserID; }
            set { _CurrentUserID = value; }
        }
        #endregion

        #region static String CurrentUserName
        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static String  CurrentUserName
        {
            get { return _CurrentUserName; }
            set { _CurrentUserName = value; }
        }
        #endregion

        #region static String CurrentUserLastName
        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static String CurrentUserLastName
        {
            get { return _CurrentUserLastName; }
            set { _CurrentUserLastName = value; }
        }
        #endregion

        #region static String CurrentUserType
        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static Int32 CurrentUserType
        {
            get { return _CurrentUserType; }
            set { _CurrentUserType = value; }
        }
        #endregion

        #endregion

        #region Methods

        #region Get Current Persian Date Time Function

        /// <summary>
        /// دریافت مقدار تاریخ و زمان شمسی به صورت رشته
        /// </summary>
        /// <returns>تاریخ شمسی جاری</returns>
        public static String GetCurrentPersianDateTime()
        {
            _PersianCal = new PersianCalendar();
            string ReturnDate = 
                String.Format("{0:0000}/{1:00}/{2:00} - {3:00}:{4:00}", 
                _PersianCal.GetYear(DateTime.Now), _PersianCal.GetMonth(DateTime.Now), 
                _PersianCal.GetDayOfMonth(DateTime.Now), 
                DateTime.Now.Hour, DateTime.Now.Minute);
            return ReturnDate;
        }

        #endregion

        #endregion

    }
}