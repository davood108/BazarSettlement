﻿#region using
using System;
using System.Globalization;
#endregion

namespace MohanirPouya.Classes
{
    public class DbBizClass
    {

        #region Fields

        #region static PersianCalendar _PersianCal
        /// <summary>
        /// یك نمونه از كلاس تقویم پارسی
        /// </summary>
        private static PersianCalendar _persianCal;
        #endregion

        #region static String _DbSqlConnectionString

        #endregion

        #region static Int32 _CurrentUserID

        #endregion

        #region static string _CurrentUserName

        #endregion

        #region static string _CurrentUserLastName

        #endregion

        #region static Int32 _CurrentUserType

        #endregion

        #region static Int32 _CurrentUserDescription

        #endregion

        #endregion

        #region Properties

        #region Database SqlConnection Property

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>

        public static String DbConnStr { get; set; }
       
        #endregion

        #region Database SqlConnection Property

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static string DbConnStrPre { get; set; }

        #endregion

        #region static Int32 CurrentUserID

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static int CurrentUserID { get; set; }

        #endregion

        #region static String CurrentUserName

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static string CurrentUserName { get; set; }

        #endregion

        #region static String CurrentUserLastName

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static string CurrentUserLastName { get; set; }

        #endregion

        #region static String CurrentUserType

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static int CurrentUserType { get; set; }

        #endregion

        #region static String CurrentUserDescription

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static string CurrentUserDescription { get; set; }

        #endregion


        #endregion

        #region Methods

        #region Get Current Persian Date Time Function

        /// <summary>
        /// دریافت مقدار تاریخ و زمان شمسی به صورت رشته
        /// </summary>
        /// <returns>تاریخ شمسی جاری</returns>
        public static String GetCurrentPersianDateTime()
        {
            _persianCal = new PersianCalendar();
            string returnDate = 
                String.Format("{0:0000}/{1:00}/{2:00} - {3:00}:{4:00}", 
                _persianCal.GetYear(DateTime.Now), _persianCal.GetMonth(DateTime.Now), 
                _persianCal.GetDayOfMonth(DateTime.Now), 
                DateTime.Now.Hour, DateTime.Now.Minute);
            return returnDate;
        }

        #endregion

        #endregion

    }
}