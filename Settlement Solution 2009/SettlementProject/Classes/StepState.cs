﻿namespace MohanirPouya.Classes
{
    class StepState
    {
        #region ExecStateSB

        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>

        public static bool ExecStateSb { get; set; }

        #endregion

        #region  ExecStateT


        /// <summary>
        /// رشته اتصال به بانك اطلاعاتی
        /// </summary>
        public static bool ExecStateT{ get; set; }

        #endregion
    }
}
