﻿#region using
using System;
using System.ComponentModel;
using System.Windows.Forms;
#endregion

namespace MohanirPouya.Classes
{
    /// <summary>
    /// كلاس پایه برای فرم های دایلوگ
    /// </summary>
    [Description("كلاس پایه برای فرم های دایلوگ")]
    public partial class MohanirFormDialog : Form
    {

        #region Fields

        #region IsOkButtonPressed

        private Boolean _IsOkButtonPressed;

        #endregion

        #endregion

        #region Ctor

        /// <summary>
        /// سازنده پیش فرض كلاس
        /// </summary>
        public MohanirFormDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        #region Form Title
        /// <summary>
        /// تعیین كننده تیتر فرم
        /// </summary>
        [Category("مهانیر پویا")]
        [Description("تعیین كننده تیتر فرم")]
        [Browsable(true)]
        public String FormTitle
        {
            get
            { return lblTitle.Text; }
            set
            { lblTitle.Text  = value; }
        }
        #endregion

        #region Form Subtitle
        /// <summary>
        /// تعیین كننده تیتر توضیحات فرم
        /// </summary>
        [Category("مهانیر پویا")]
        [Description("تعیین كننده تیتر توضیحات فرم")]
        [Browsable(true)]
        public String FormSubtitle
        {
            get
            { return lblSubtitle.Text; }
            set
            { lblSubtitle.Text = value; }
        }

        #endregion

        #region Pressed Button

        /// <summary>
        /// تعیین كننده كلید انتخاب شده در فرم
        /// </summary>
        [Category("مهانیر پویا")]
        [Description("تعیین كننده كلید انتخاب شده در فرم")]
        public Boolean IsOkButtonPressed
        {
            get { return _IsOkButtonPressed; }
            set { _IsOkButtonPressed = value; }
        }

        #endregion

        private void MohanirFormDialog_Load(object sender, EventArgs e)
        {

        }

       
        #endregion

    }
}