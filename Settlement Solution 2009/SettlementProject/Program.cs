﻿using System.Windows.Forms;
using MohanirPouya.Forms.Start;
using System.Linq;
using System.IO;

namespace MohanirPouya
{
    internal static class Program
    {

        private static void Main()
        {
            var SettlementExist = Directory.Exists(@"D:\Settlement");
            var tempExist = Directory.Exists(@"D:\Settlement\Temp");
            var versionExist = File .Exists(@"D:\Settlement\Version.txt");

            if (SettlementExist == false)
            {
                Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(@"\\cb555\Settlement\Temp",
                           @"D:\Settlement\Temp");
                File.Copy(@"\\cb555\Settlement\Version.txt",
                           @"D:\Settlement\Version.txt");
            }
            else
            {
                if (tempExist == false || versionExist == false)
                {
                    #region Not Exist

                    Cursor.Current = Cursors.WaitCursor;
                    if (tempExist)
                    {
                        Directory.Delete(@"D:\\Settlement\Temp", true);
                    }
                    File.Delete(@"D:\Settlement\Version.txt");

                    var lastLine2 = File.ReadAllLines(@"\\cb555\Settlement\Version.txt").Last();
                    Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(@"\\cb555\Settlement\Temp",
                        @"D:\Settlement\Temp");
                    File.WriteAllText(@"D:\Settlement\Version.txt", lastLine2);

                    #endregion
                }
                else
                {
                    #region Exist

                    var lastLine = File.ReadAllLines(@"\\CB555\Settlement\Version.txt").Last();
                    var lastLine1 = File.ReadAllLines(@"D:\Settlement\Version.txt").First();
                    int fCount = Directory.GetFiles(@"\\cb555\Settlement\Temp", "*", SearchOption.AllDirectories).Length;
                    int fCountD = Directory.GetFiles(@"D:\Settlement\Temp", "*", SearchOption.AllDirectories).Length;

                    if ((lastLine != lastLine1) || (fCount != fCountD))
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        Directory.Delete(@"D:\Settlement\Temp", true);
                        File.Delete(@"D:\Settlement\Version.txt");

                        var lastLine2 = File.ReadAllLines(@"\\cb555\Settlement\Version.txt").Last();
                        Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(@"\\cb555\Settlement\Temp",
                            @"D:\Settlement\Temp");
                        File.WriteAllText(@"D:\Settlement\Version.txt", lastLine2);


                    }

                    #endregion
                }
            }
            Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);
         Application.Run(new FrmSplash());
         
            }
        }
    }

