﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Settlement Project")]
[assembly: AssemblyDescription("به سفارش بازار برق ایران")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mohanir Pouya")]
[assembly: AssemblyProduct("Settlement")]
[assembly: AssemblyCopyright("Copyright © Mohanir Pouya  2009")]
[assembly: AssemblyTrademark("Mohanir Pouya")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d98b7eb1-01e5-4aa9-9526-6586b8a6530b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
