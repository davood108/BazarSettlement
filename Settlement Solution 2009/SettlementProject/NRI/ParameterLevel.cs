﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MohanirPouya.Classes;
using   MohanirPouya.Forms.Management.ManageParameters.Classes;

namespace MohanirPouya.NRI
{
   public static class ParameterLevel
   {
       private static List<Parameter> _paramsList;

       public static void UplatePlevel(string subject)
       {

           DataTable dtParams = Parameters.GetAllParamsByPart(subject);
           _paramsList=new List<Parameter>();

           DataTableReader myDataTableReader = dtParams.CreateDataReader();
           while (myDataTableReader.Read())
           {
               if (myDataTableReader["Category"].ToString()=="Fx")
               {
                  _paramsList.Add(new Parameter(myDataTableReader["EnglishName"].ToString(), 0));
               }
               else
               {
                  _paramsList.Add(new Parameter(myDataTableReader["EnglishName"].ToString(), -1));
               }
               
           }
           myDataTableReader.Close();



           foreach (Parameter p in _paramsList)
           {
               if (p.Level == -1)
               {
                 ComputePLevel(subject, p.Name);
               }
           }

           foreach (Parameter p in _paramsList)
           {
               try
               {

                   #region Prepare SqlCommand

                   string commandText =
                       "Update " + subject + ".ParametersLog " +
                       "SET PLevel=" + p.Level +
                       " WHERE EnglishName='" + p.Name + "'";

                   SqlConnection mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                   SqlCommand mySqlCommand = new SqlCommand(commandText, mySqlConnection);

                   #endregion

                   #region Execute SqlCommand

                   try
                   {
                       mySqlCommand.Connection.Open();
                       mySqlCommand.ExecuteNonQuery();
                   }
                   catch (Exception ex)
                   {
                       MessageBox.Show(ex.Message, @"خطا!");
                   }
                   finally
                   {
                       mySqlCommand.Connection.Close();
                   }

                   #endregion

               }
               catch (Exception ex)
               {
                   MessageBox.Show(ex.Message);
               }
           }



       }



       private static int ComputePLevel(string subject, string paramName)
       {
           Parameter param = _paramsList.SingleOrDefault(p => p.Name == paramName);
           if (param.Level != -1)
           {
               return param.Level;
           }

           DataTable dt = new DataTable();
           try
           {

               #region Prepare SqlCommand

               string commandText =
                   "SELECT ID,EnglishName,EnglishNameIN " +
                   "FROM " + subject + ".ParamIn " +
                   "WHERE EnglishName='" + paramName + "'";

               SqlConnection mySqlConnection =
                   new SqlConnection(DbBizClass.DbConnStr);
               SqlCommand mySqlCommand = new SqlCommand(commandText, mySqlConnection);

               #endregion

               #region Execute SqlCommand

               try
               {
                   mySqlCommand.Connection.Open();
                   dt.Load(mySqlCommand.ExecuteReader());
               }
               catch (Exception ex)
               {
                   MessageBox.Show(ex.Message, @"خطا!");
               }
               finally
               {
                   mySqlCommand.Connection.Close();
               }

               #endregion

           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }


           int max = 0;
           for (int i = 0; i < dt.Rows.Count; i++)
           {
               int level = ComputePLevel(subject, dt.Rows[i]["EnglishNameIN"].ToString());
               if (level > max)
               {
                   max = level;
               }
           }
           max++;
           param.Level = max;

           return max;
       }

   }

    internal class Parameter
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public Parameter(string name,int level)
        {
            Name = name;
            Level = level;
        }
        public Parameter()
        {
        }
    }
}
