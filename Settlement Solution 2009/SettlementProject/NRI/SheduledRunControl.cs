﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using RPNCalendar.Utilities;

namespace MohanirPouya.NRI
{
    public partial class SheduledRunControl : UserControl
    {

        [Category("NewEvents"), Browsable(true), Description("Occurs when the user click Close button")]
        public event EventHandler CloseButtonClick;

        [Category("NewEvents"), Browsable(true), Description("Occurs when the user click ShowLog linkLabel")]
        public event EventHandler ShowLogClick;

        private int _runNo;

        public int RunNo
        {
            get { return _runNo; }
            set
            {
                _runNo = value;
                lbRunNo.Text = _runNo.ToString();
            }
        }

        public int Order { get; set; }
        private bool _isSelected;
        public bool IsSelected { get { return _isSelected; }
            set
            {
                _isSelected = value;
                MainPanel.Style.BorderColor.Color = _isSelected ? Color.Blue : Color.Black;
                MainPanel.Style.Border = _isSelected ? eBorderType.DoubleLine : eBorderType.SingleLine;

            }
        }
        public int RunId { get; set; }

        public string BillType
        {
            get
            {
                if (cBoxDaily.Checked)
                {
                    return "D";
                }
                if (cBoxMonthly.Checked)
                {
                    return "M";
                }
                return "C"; //cBoxConst.Checked

            }
        }

        public string PType
        {
            get
            {
                switch (cmBoxTitle.SelectedIndex)
                {
                    
                    case 0:
                        return "Sellers";
                    case 1:
                        return "Buyers";
                    case 2:
                        return "Transfer";
                    case 3:
                        return "Fuel";

                }
                return "";
            }
        }

        public string StartDate
        {
            get { return datePickerStart.Text; }
            set { datePickerStart.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(value); }
        }

        public string EndDate
        {
            get { return datePickerEnd.Text; }
            set { datePickerEnd.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(value); }
        }

        public bool Stage1
        {
            get { return chkStep1.Checked; }
        }

        public bool Stage2
        {
            get { return chkStep2.Checked; }
        }

        public bool Stage3
        {
            get { return chkStep3.Checked; }
        }

        public bool Stage4
        {
            get { return chkStep4.Checked; }
        }

        public bool Stage5
        {
            get { return chkStep5.Checked; }
        }

        public bool Stage6
        {
            get { return chkStep6.Checked; }
        }

        public string Status { get; set; } //اجرا نشده، در صف اجرا،در حال اجرا، اجرا شده، با خطا متوقف شده

        public SheduledRunControl()
        {
            InitializeComponent();
            PersianDate occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            datePickerStart.SelectedDateTime = DateTime.Now;
            datePickerStart.Text = occuredDate.ToString();
            cmBoxTitle.SelectedIndex = 0;
            cmBoxTitle.Enabled = false;
            datePickerEnd.Visible = false;
            lblEDate.Visible = false;
            //timer.Tick += new EventHandler(timer_Tick);
            //timer.Interval = 1;
            //timer.Start();
        }

        //void timer_Tick(object sender, EventArgs e)
        //{
        //    if (mouseDown)
        //    {
        //        this.Location = newLocation;
        //    }
        //}

        public SheduledRunControl(string billType, string pType, string startDate, string endDate, bool stage1,
            bool stage2, bool stage3, bool stage4, bool stage5, bool stage6)
        {
            InitializeComponent();

            switch (billType)
            {
                case "D":
                    cBoxDaily.Checked = true;
                    datePickerEnd.Visible = false;
                    lblEDate.Visible = false;
                    cmBoxTitle.Enabled = false;
                    break;
                case "M":
                    cBoxMonthly.Checked = true;
                    datePickerEnd.Visible = true;
                    lblEDate.Visible = true;
                    cmBoxTitle.Enabled = true;
                    break;
                case "C":
                    cBoxConst.Checked = true;
                    datePickerEnd.Visible = true;
                    lblEDate.Visible = true;
                    cmBoxTitle.Enabled = true;
                    break;
            }
            switch (pType)
            {

                case "Sellers":
                    cmBoxTitle.SelectedIndex = 0;
                    break;
                case "Buyers":
                    cmBoxTitle.SelectedIndex = 1;
                    break;
                case "Transfer":
                    cmBoxTitle.SelectedIndex = 2;
                    break;
                case "Fuel":
                    cmBoxTitle.SelectedIndex = 3;
                    break;
            }
            StartDate = startDate;
            EndDate = endDate;
            chkStep1.Checked = stage1;
            chkStep2.Checked = stage2;
            chkStep3.Checked = stage3;
            chkStep4.Checked = stage4;
            chkStep5.Checked = stage5;
            chkStep6.Checked = stage6;

        }

        private void cBoxDaily_CheckedChanged(object sender, EventArgs e)
        {
            if (cBoxDaily.Checked)
            {
                cmBoxTitle.SelectedIndex = 0;
                cmBoxTitle.Enabled = false;
                datePickerEnd.Visible = false;
                lblEDate.Visible = false;
            }
            else
            {
                cmBoxTitle.Enabled = true;
                datePickerEnd.Visible = true;
                lblEDate.Visible = true;
            }
        }

        private void btnPrevMonth_Click(object sender, EventArgs e)
        {
            if (cBoxDaily.Checked)
                GoToPreviousDay();
            else
                GoToPreviousMonth();
        }

        private void GoToPreviousMonth()
        {
            PersianDate occuredDate = PersianDateConverter.ToPersianDate(datePickerStart.SelectedDateTime);

            if (occuredDate.Month == 1)
            {
                occuredDate.Month = 12;
                occuredDate.Year = occuredDate.Year - 1;
            }
            else
            {
                occuredDate.Month--;
            }

            PersianDate beginDate = new PersianDate(occuredDate.Year, occuredDate.Month, 1);
            PersianDate endDate;
            if (occuredDate.Month != 12)
            {
                endDate = new PersianDate(occuredDate.Year, occuredDate.Month, occuredDate.MonthDays);
            }
            else
            {
                try
                {
                    endDate = new PersianDate(occuredDate.Year, occuredDate.Month, occuredDate.MonthDays); //sale kabise
                }
                catch
                {

                    endDate = new PersianDate(occuredDate.Year, occuredDate.Month, 29);
                }
            }

            datePickerStart.SelectedDateTime = (DateTime) beginDate;
            datePickerEnd.SelectedDateTime = (DateTime) endDate;
            datePickerStart.Text = beginDate.ToString();
            datePickerEnd.Text = endDate.ToString();
        }

        private void GoToPreviousDay()
        {
            PersianDate occuredDate = PersianDateConverter.ToPersianDate(datePickerStart.SelectedDateTime);
            PersianDate beginDate;
            if (occuredDate.Day == 1)
            {
                if (occuredDate.Month == 1)
                {
                    occuredDate.Month = 12;
                    occuredDate.Year = occuredDate.Year - 1;
                    try
                    {
                        beginDate = new PersianDate(occuredDate.Year, occuredDate.Month, occuredDate.MonthDays);
                        //sale kabise
                    }
                    catch
                    {

                        beginDate = new PersianDate(occuredDate.Year, occuredDate.Month, 29);
                    }
                }
                else
                {
                    occuredDate.Month--;
                    beginDate = new PersianDate(occuredDate.Year, occuredDate.Month, occuredDate.MonthDays);
                }
            }
            else
            {
                occuredDate.Day--;
                beginDate = new PersianDate(occuredDate.Year, occuredDate.Month, occuredDate.Day);
            }



            datePickerStart.SelectedDateTime = (DateTime) beginDate;
            datePickerStart.Text = beginDate.ToString();

        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.CloseButtonClick != null)
            {
                this.CloseButtonClick(this, e);
            }
        }

        private void linkLblShowLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.ShowLogClick != null)
            {
                this.ShowLogClick(this, e);
            }
        }

        //private bool mouseDown = false;
        //private Point lastMouseLocation;
        //private Point newLocation;
        //private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        //{
        //    mouseDown = true;
        //    lastMouseLocation = new Point(e.X, e.Y); 
        //}

        //private void TopPanel_MouseUp(object sender, MouseEventArgs e)
        //{
        //    mouseDown = false;
 
        //}

        //private void TopPanel_MouseMove(object sender, MouseEventArgs e)
        //{
        //    if (mouseDown)
        //    {
        //        newLocation = new Point(this.Location.X + e.X - lastMouseLocation.X, this.Location.Y + e.Y - lastMouseLocation.Y);
        //    }
        //    lastMouseLocation = new Point(e.X, e.Y);
        //}

        //Timer timer=new Timer();

        private void MainPanel_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                IsSelected = !IsSelected;
                if (IsSelected)
                {
                    foreach (var Control in this.Parent.Controls)
                    {
                        if (Control is SheduledRunControl)
                        {
                            (Control as SheduledRunControl).IsSelected = false;
                        }
                    }
                    IsSelected = true;
                }
               
            }
        }
        


    }

}
