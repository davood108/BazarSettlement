﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace NRI.Classes
{
    public static class SmsUtility
    {

        /// <summary>
        /// ارسال پیامک
        /// </summary>
        /// <param name="startDate">تاریخ روز اول ماه</param>
        /// <param name="billType">نوع صورتحساب : M , C</param>
        public static void SendSms(string startDate, string billType, SmsType smsType)
        {
            string messageText = "";
            int groupId;
            if (smsType == SmsType.SaveInEmis)
            {
                messageText = "اطلاعات صورتحساب ";
                string billTypeTitle = "";
                if (billType == "M")
                    billTypeTitle = "ماهیانه";
                else if (billType == "C")
                    billTypeTitle = "قطعی";
                messageText = messageText + billTypeTitle;
                messageText = messageText + " ماه " + startDate.Substring(0, 7) +
                              " در سیستم اطلاعات بازار برق بارگذاری شد.";

                groupId = 390;
            }
            else //SmsType.Taraz
            {
                messageText = "صورتحساب ";
                string billTypeTitle = "";
                if (billType == "M")
                    billTypeTitle = "ماهیانه";
                else if (billType == "C")
                    billTypeTitle = "قطعی";
                messageText = messageText + billTypeTitle;
                messageText = messageText + " ماه " + startDate.Substring(0, 7) + " با موفقیت اجرا گردید.";

                groupId = 393;
            }

            SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder()
            {
                DataSource = "172.31.1.48",
                InitialCatalog = "Mehrafraz",
                UserID = "RunUsr",
                Password = "Run1394",
                IntegratedSecurity = false
            };
            SqlConnection connection = new SqlConnection(connectionBuilder.ConnectionString);
            string query = "INSERT [mehrafraz].[dbo].[smssend] (smsdes,group_groupid) VALUES (@messageText , @groupId)";
            SqlCommand cmd = new SqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@messageText", messageText);
            cmd.Parameters.AddWithValue("@groupId", groupId);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }

        }
    }

    public enum SmsType
    {
        SaveInEmis,
        Taraz
    }
}
