﻿namespace MohanirPouya.NRI
{
    partial class ShceduledRunControl2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.linkLblShowLog = new System.Windows.Forms.LinkLabel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbRunNo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRemove = new DevComponents.DotNetBar.ButtonX();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.linkLblShowLog);
            this.TopPanel.Controls.Add(this.lblStatus);
            this.TopPanel.Controls.Add(this.label8);
            this.TopPanel.Controls.Add(this.lbRunNo);
            this.TopPanel.Controls.Add(this.label7);
            this.TopPanel.Controls.Add(this.btnRemove);
            this.TopPanel.Cursor = System.Windows.Forms.Cursors.Default;
            this.TopPanel.Location = new System.Drawing.Point(3, 3);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(752, 37);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.Color = System.Drawing.Color.Silver;
            this.TopPanel.Style.BackColor2.Color = System.Drawing.Color.White;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 44;
            // 
            // linkLblShowLog
            // 
            this.linkLblShowLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblShowLog.AutoSize = true;
            this.linkLblShowLog.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.linkLblShowLog.Location = new System.Drawing.Point(117, 14);
            this.linkLblShowLog.Name = "linkLblShowLog";
            this.linkLblShowLog.Size = new System.Drawing.Size(67, 13);
            this.linkLblShowLog.TabIndex = 68;
            this.linkLblShowLog.TabStop = true;
            this.linkLblShowLog.Text = "مشاهده لاگ";
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblStatus.Location = new System.Drawing.Point(237, 12);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(330, 16);
            this.lblStatus.TabIndex = 67;
            this.lblStatus.Text = "اجرا نشده";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.Location = new System.Drawing.Point(572, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 16);
            this.label8.TabIndex = 67;
            this.label8.Text = "وضعیت :";
            // 
            // lbRunNo
            // 
            this.lbRunNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbRunNo.AutoSize = true;
            this.lbRunNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbRunNo.Location = new System.Drawing.Point(646, 12);
            this.lbRunNo.Name = "lbRunNo";
            this.lbRunNo.Size = new System.Drawing.Size(15, 16);
            this.lbRunNo.TabIndex = 66;
            this.lbRunNo.Text = "0";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.Location = new System.Drawing.Point(667, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 65;
            this.label7.Text = "شماره اجرا :";
            // 
            // btnRemove
            // 
            this.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnRemove.Image = global::MohanirPouya.Properties.Resources.icontexto_message_types_error_red1;
            this.btnRemove.Location = new System.Drawing.Point(1, 1);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnRemove.Size = new System.Drawing.Size(36, 34);
            this.btnRemove.TabIndex = 64;
            this.btnRemove.Tooltip = "حذف";
            // 
            // ShceduledRunControl2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TopPanel);
            this.Name = "ShceduledRunControl2";
            this.Size = new System.Drawing.Size(759, 240);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx TopPanel;
        private System.Windows.Forms.LinkLabel linkLblShowLog;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbRunNo;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.ButtonX btnRemove;
    }
}
