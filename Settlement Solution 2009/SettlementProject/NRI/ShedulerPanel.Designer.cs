﻿namespace MohanirPouya.NRI
{
    partial class ShedulerPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAdd = new DevComponents.DotNetBar.ButtonX();
            this.btnGoDown = new DevComponents.DotNetBar.ButtonX();
            this.btnGoUp = new DevComponents.DotNetBar.ButtonX();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.MainPanel.AutoScroll = true;
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.flowLayoutPanel1);
            this.MainPanel.Location = new System.Drawing.Point(3, 45);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(798, 431);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(798, 431);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnAdd.Image = global::MohanirPouya.Properties.Resources.add1;
            this.btnAdd.Location = new System.Drawing.Point(807, 243);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAdd.Size = new System.Drawing.Size(39, 34);
            this.btnAdd.TabIndex = 65;
            this.btnAdd.Tooltip = "Add New";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnGoDown
            // 
            this.btnGoDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGoDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnGoDown.Image = global::MohanirPouya.Properties.Resources.down;
            this.btnGoDown.Location = new System.Drawing.Point(807, 159);
            this.btnGoDown.Name = "btnGoDown";
            this.btnGoDown.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnGoDown.Size = new System.Drawing.Size(39, 34);
            this.btnGoDown.TabIndex = 65;
            this.btnGoDown.Tooltip = "Move Down";
            this.btnGoDown.Click += new System.EventHandler(this.btnGoDown_Click);
            // 
            // btnGoUp
            // 
            this.btnGoUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGoUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnGoUp.Image = global::MohanirPouya.Properties.Resources.up;
            this.btnGoUp.Location = new System.Drawing.Point(807, 119);
            this.btnGoUp.Name = "btnGoUp";
            this.btnGoUp.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnGoUp.Size = new System.Drawing.Size(39, 34);
            this.btnGoUp.TabIndex = 65;
            this.btnGoUp.Tooltip = "Move Up";
            this.btnGoUp.Click += new System.EventHandler(this.btnGoUp_Click);
            // 
            // ShedulerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnGoDown);
            this.Controls.Add(this.btnGoUp);
            this.Controls.Add(this.MainPanel);
            this.Name = "ShedulerPanel";
            this.Size = new System.Drawing.Size(883, 479);
            this.MainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx MainPanel;
        private DevComponents.DotNetBar.ButtonX btnGoUp;
        private DevComponents.DotNetBar.ButtonX btnGoDown;
        private DevComponents.DotNetBar.ButtonX btnAdd;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}
