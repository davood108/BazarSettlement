﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MohanirPouya.NRI;

namespace MohanirPouya.NRI
{
    public partial class ShedulerPanel : UserControl
    {
        private List<SheduledRunControl> itemsList;
        private int locationY = 0;
        public ShedulerPanel()
        {
            InitializeComponent();
            itemsList=new List<SheduledRunControl>();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SheduledRunControl item;
            if (itemsList.Count>0)
            {
                SheduledRunControl lastItem = itemsList[itemsList.Count - 1];
                 item = new SheduledRunControl(lastItem.BillType, lastItem.PType, lastItem.StartDate, lastItem.EndDate, lastItem.Stage1, lastItem.Stage2, lastItem.Stage3, lastItem.Stage4, lastItem.Stage5, lastItem.Stage6);
            }
            else
            {
                item=new SheduledRunControl();
            }
            item.Order = itemsList.Count + 1;
            itemsList.Add(item);
            //item.Dock=DockStyle.Bottom;
            item.CloseButtonClick += new EventHandler(item_CloseButtonClick);
            //item.Location = new Point(0, locationY);
            locationY += item.Size.Height + 5;
            flowLayoutPanel1.Controls.Add(item);
            flowLayoutPanel1.ScrollControlIntoView(item);
        }

        void item_CloseButtonClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to remove the run", "Warning", MessageBoxButtons.YesNo) ==
                DialogResult.Yes)
            {
                (sender as SheduledRunControl).Dispose();
                itemsList.Remove(sender as SheduledRunControl);
                ReArrange();
            }
        }

        private void ReArrange()
        {
           List<SheduledRunControl> sortedList= itemsList.OrderBy(t => t.Order).ToList();
            itemsList.Clear();
            flowLayoutPanel1.Controls.Clear();
            for (int i = 0; i < sortedList.Count; i++)
            {
                sortedList[i].Order = i+1;
                itemsList.Add(sortedList[i]);
                flowLayoutPanel1.Controls.Add(sortedList[i]);
            }
        }

        private void btnGoUp_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < itemsList.Count; i++) //i=1
            {
                if (itemsList[i].IsSelected)
                {
                    itemsList[i].Order--;
                    itemsList[i-1].Order++;
                    ReArrange();
                    break;
                }
            }
        }

        private void btnGoDown_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < itemsList.Count - 1; i++) //itemsList.Count-1
            {
                if (itemsList[i].IsSelected)
                {
                    itemsList[i].Order++;
                    itemsList[i + 1].Order--;
                    ReArrange();
                    break;
                }
            }
        }
    }
}
