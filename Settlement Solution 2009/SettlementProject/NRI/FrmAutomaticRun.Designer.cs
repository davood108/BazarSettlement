﻿namespace MohanirPouya.NRI
{
    partial class FrmAutomaticRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new DevComponents.DotNetBar.ButtonX();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnStop = new DevComponents.DotNetBar.ButtonX();
            this.maskedTxtDaysAgo = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRemainingTime = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkStep1 = new System.Windows.Forms.CheckBox();
            this.chkStep2 = new System.Windows.Forms.CheckBox();
            this.chkStep3 = new System.Windows.Forms.CheckBox();
            this.chkStep4 = new System.Windows.Forms.CheckBox();
            this.chkStep5 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.radioBtnMonthly = new System.Windows.Forms.RadioButton();
            this.radioBtnDaily = new System.Windows.Forms.RadioButton();
            this.maskedTxtDate = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkStep6 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.btnStart.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnStart.Location = new System.Drawing.Point(534, 231);
            this.btnStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnStart.Name = "btnStart";
            this.btnStart.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnStart.Size = new System.Drawing.Size(107, 34);
            this.btnStart.TabIndex = 65;
            this.btnStart.TabStop = false;
            this.btnStart.Text = "شروع";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnStop
            // 
            this.btnStop.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStop.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(323, 231);
            this.btnStop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnStop.Name = "btnStop";
            this.btnStop.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnStop.Size = new System.Drawing.Size(107, 34);
            this.btnStop.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnStop.TabIndex = 65;
            this.btnStop.TabStop = false;
            this.btnStop.Text = "توقف";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // maskedTxtDaysAgo
            // 
            this.maskedTxtDaysAgo.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedTxtDaysAgo.Location = new System.Drawing.Point(471, 20);
            this.maskedTxtDaysAgo.Mask = "0";
            this.maskedTxtDaysAgo.Name = "maskedTxtDaysAgo";
            this.maskedTxtDaysAgo.Size = new System.Drawing.Size(17, 23);
            this.maskedTxtDaysAgo.TabIndex = 66;
            this.maskedTxtDaysAgo.Text = "5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(419, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 16);
            this.label1.TabIndex = 67;
            this.label1.Text = "روز قبل";
            // 
            // lblRemainingTime
            // 
            this.lblRemainingTime.AutoSize = true;
            this.lblRemainingTime.Location = new System.Drawing.Point(331, 277);
            this.lblRemainingTime.Name = "lblRemainingTime";
            this.lblRemainingTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblRemainingTime.Size = new System.Drawing.Size(124, 16);
            this.lblRemainingTime.TabIndex = 67;
            this.lblRemainingTime.Text = "00 ساعت و 00 دقیقه";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "HH:mm";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker1.Location = new System.Drawing.Point(405, 109);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(131, 23);
            this.dateTimePicker1.TabIndex = 68;
            this.dateTimePicker1.Value = new System.DateTime(2016, 8, 2, 5, 0, 0, 0);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.radioBtnMonthly);
            this.panel1.Controls.Add(this.radioBtnDaily);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.maskedTxtDate);
            this.panel1.Controls.Add(this.maskedTxtDaysAgo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(25, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(628, 195);
            this.panel1.TabIndex = 69;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkStep6);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.chkStep1);
            this.groupBox1.Controls.Add(this.chkStep2);
            this.groupBox1.Controls.Add(this.chkStep3);
            this.groupBox1.Controls.Add(this.chkStep4);
            this.groupBox1.Controls.Add(this.chkStep5);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.Red;
            this.groupBox1.Location = new System.Drawing.Point(21, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(339, 168);
            this.groupBox1.TabIndex = 71;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مراحل صدور صورتحساب";
            // 
            // chkStep1
            // 
            this.chkStep1.AutoSize = true;
            this.chkStep1.Checked = true;
            this.chkStep1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep1.Location = new System.Drawing.Point(25, 26);
            this.chkStep1.Name = "chkStep1";
            this.chkStep1.Size = new System.Drawing.Size(15, 14);
            this.chkStep1.TabIndex = 1;
            this.chkStep1.UseVisualStyleBackColor = true;
            // 
            // chkStep2
            // 
            this.chkStep2.AutoSize = true;
            this.chkStep2.Checked = true;
            this.chkStep2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep2.Location = new System.Drawing.Point(25, 48);
            this.chkStep2.Name = "chkStep2";
            this.chkStep2.Size = new System.Drawing.Size(15, 14);
            this.chkStep2.TabIndex = 2;
            this.chkStep2.UseVisualStyleBackColor = true;
            // 
            // chkStep3
            // 
            this.chkStep3.AutoSize = true;
            this.chkStep3.Checked = true;
            this.chkStep3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep3.Location = new System.Drawing.Point(25, 70);
            this.chkStep3.Name = "chkStep3";
            this.chkStep3.Size = new System.Drawing.Size(15, 14);
            this.chkStep3.TabIndex = 3;
            this.chkStep3.UseVisualStyleBackColor = true;
            // 
            // chkStep4
            // 
            this.chkStep4.AutoSize = true;
            this.chkStep4.Checked = true;
            this.chkStep4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep4.Location = new System.Drawing.Point(25, 93);
            this.chkStep4.Name = "chkStep4";
            this.chkStep4.Size = new System.Drawing.Size(15, 14);
            this.chkStep4.TabIndex = 4;
            this.chkStep4.UseVisualStyleBackColor = true;
            // 
            // chkStep5
            // 
            this.chkStep5.AutoSize = true;
            this.chkStep5.Checked = true;
            this.chkStep5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep5.Location = new System.Drawing.Point(25, 114);
            this.chkStep5.Name = "chkStep5";
            this.chkStep5.Size = new System.Drawing.Size(15, 14);
            this.chkStep5.TabIndex = 5;
            this.chkStep5.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(104, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(226, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "5- تولید فایلهای اکسل و شماره زنی صورتحساب";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(223, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "4- ایجاد جدول خروجی";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(231, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "3- محاسبه پارامترها";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(107, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(223, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "2-دریافت اطلاعات از EMIS و پرکردن جدول اصلی";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(175, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(155, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "1- آماده سازی اولیه جدول اصلی";
            // 
            // radioBtnMonthly
            // 
            this.radioBtnMonthly.AutoSize = true;
            this.radioBtnMonthly.Location = new System.Drawing.Point(483, 58);
            this.radioBtnMonthly.Name = "radioBtnMonthly";
            this.radioBtnMonthly.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioBtnMonthly.Size = new System.Drawing.Size(133, 20);
            this.radioBtnMonthly.TabIndex = 70;
            this.radioBtnMonthly.Text = "صورتحساب ماهیانه";
            this.radioBtnMonthly.UseVisualStyleBackColor = true;
            this.radioBtnMonthly.CheckedChanged += new System.EventHandler(this.radioBtnDaily_CheckedChanged);
            // 
            // radioBtnDaily
            // 
            this.radioBtnDaily.AutoSize = true;
            this.radioBtnDaily.Checked = true;
            this.radioBtnDaily.Location = new System.Drawing.Point(494, 21);
            this.radioBtnDaily.Name = "radioBtnDaily";
            this.radioBtnDaily.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioBtnDaily.Size = new System.Drawing.Size(122, 20);
            this.radioBtnDaily.TabIndex = 69;
            this.radioBtnDaily.TabStop = true;
            this.radioBtnDaily.Text = "صورتحساب روزانه";
            this.radioBtnDaily.UseVisualStyleBackColor = true;
            this.radioBtnDaily.CheckedChanged += new System.EventHandler(this.radioBtnDaily_CheckedChanged);
            // 
            // maskedTxtDate
            // 
            this.maskedTxtDate.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedTxtDate.Enabled = false;
            this.maskedTxtDate.Location = new System.Drawing.Point(422, 55);
            this.maskedTxtDate.Mask = "0000\\/00";
            this.maskedTxtDate.Name = "maskedTxtDate";
            this.maskedTxtDate.Size = new System.Drawing.Size(55, 23);
            this.maskedTxtDate.TabIndex = 66;
            this.maskedTxtDate.Text = "139501";
            this.maskedTxtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(553, 114);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(66, 16);
            this.label3.TabIndex = 67;
            this.label3.Text = "زمان اجرا :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(461, 277);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(183, 16);
            this.label4.TabIndex = 67;
            this.label4.Text = "زمان باقی مانده تا اجرای بعدی :";
            // 
            // chkStep6
            // 
            this.chkStep6.AutoSize = true;
            this.chkStep6.Location = new System.Drawing.Point(25, 135);
            this.chkStep6.Name = "chkStep6";
            this.chkStep6.Size = new System.Drawing.Size(15, 14);
            this.chkStep6.TabIndex = 7;
            this.chkStep6.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(245, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "6- ذخیره در EMIS";
            // 
            // FrmAutomaticRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 314);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.lblRemainingTime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnStart);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FrmAutomaticRun";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "اجرای خودکار صورتحساب فروش";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmAutomaticRun_FormClosed);
            this.Load += new System.EventHandler(this.FrmAutomaticRun_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnStart;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.ButtonX btnStop;
        private System.Windows.Forms.MaskedTextBox maskedTxtDaysAgo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRemainingTime;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioBtnMonthly;
        private System.Windows.Forms.RadioButton radioBtnDaily;
        private System.Windows.Forms.MaskedTextBox maskedTxtDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkStep1;
        private System.Windows.Forms.CheckBox chkStep2;
        private System.Windows.Forms.CheckBox chkStep3;
        private System.Windows.Forms.CheckBox chkStep4;
        private System.Windows.Forms.CheckBox chkStep5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkStep6;
        private System.Windows.Forms.Label label9;
    }
}