﻿namespace MohanirPouya.NRI
{
    partial class SheduledRunControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkStep1 = new System.Windows.Forms.CheckBox();
            this.chkStep2 = new System.Windows.Forms.CheckBox();
            this.chkStep3 = new System.Windows.Forms.CheckBox();
            this.chkStep4 = new System.Windows.Forms.CheckBox();
            this.chkStep6 = new System.Windows.Forms.CheckBox();
            this.chkStep5 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrevMonth = new System.Windows.Forms.Button();
            this.cBoxDaily = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cBoxConst = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cBoxMonthly = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.datePickerEnd = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.datePickerStart = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.lblEDate = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.cmBoxTitle = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.linkLblShowLog = new System.Windows.Forms.LinkLabel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbRunNo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRemove = new DevComponents.DotNetBar.ButtonX();
            this.MainPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.groupBox1);
            this.MainPanel.Controls.Add(this.btnPrevMonth);
            this.MainPanel.Controls.Add(this.cBoxDaily);
            this.MainPanel.Controls.Add(this.cBoxConst);
            this.MainPanel.Controls.Add(this.cBoxMonthly);
            this.MainPanel.Controls.Add(this.labelX4);
            this.MainPanel.Controls.Add(this.labelX2);
            this.MainPanel.Controls.Add(this.labelX1);
            this.MainPanel.Controls.Add(this.datePickerEnd);
            this.MainPanel.Controls.Add(this.datePickerStart);
            this.MainPanel.Controls.Add(this.lblEDate);
            this.MainPanel.Controls.Add(this.labelX3);
            this.MainPanel.Controls.Add(this.cmBoxTitle);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(758, 216);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.Color = System.Drawing.Color.GhostWhite;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.Color = System.Drawing.Color.Black;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 0;
            this.MainPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainPanel_MouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.chkStep1);
            this.groupBox1.Controls.Add(this.chkStep2);
            this.groupBox1.Controls.Add(this.chkStep3);
            this.groupBox1.Controls.Add(this.chkStep4);
            this.groupBox1.Controls.Add(this.chkStep6);
            this.groupBox1.Controls.Add(this.chkStep5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.groupBox1.Location = new System.Drawing.Point(17, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 159);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مراحل صدور صورتحساب";
            // 
            // chkStep1
            // 
            this.chkStep1.AutoSize = true;
            this.chkStep1.Checked = true;
            this.chkStep1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep1.Location = new System.Drawing.Point(29, 23);
            this.chkStep1.Name = "chkStep1";
            this.chkStep1.Size = new System.Drawing.Size(15, 14);
            this.chkStep1.TabIndex = 0;
            this.chkStep1.UseVisualStyleBackColor = true;
            // 
            // chkStep2
            // 
            this.chkStep2.AutoSize = true;
            this.chkStep2.Checked = true;
            this.chkStep2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep2.Location = new System.Drawing.Point(29, 47);
            this.chkStep2.Name = "chkStep2";
            this.chkStep2.Size = new System.Drawing.Size(15, 14);
            this.chkStep2.TabIndex = 1;
            this.chkStep2.UseVisualStyleBackColor = true;
            // 
            // chkStep3
            // 
            this.chkStep3.AutoSize = true;
            this.chkStep3.Checked = true;
            this.chkStep3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep3.Location = new System.Drawing.Point(29, 70);
            this.chkStep3.Name = "chkStep3";
            this.chkStep3.Size = new System.Drawing.Size(15, 14);
            this.chkStep3.TabIndex = 2;
            this.chkStep3.UseVisualStyleBackColor = true;
            // 
            // chkStep4
            // 
            this.chkStep4.AutoSize = true;
            this.chkStep4.Checked = true;
            this.chkStep4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep4.Location = new System.Drawing.Point(29, 94);
            this.chkStep4.Name = "chkStep4";
            this.chkStep4.Size = new System.Drawing.Size(15, 14);
            this.chkStep4.TabIndex = 3;
            this.chkStep4.UseVisualStyleBackColor = true;
            // 
            // chkStep6
            // 
            this.chkStep6.AutoSize = true;
            this.chkStep6.Location = new System.Drawing.Point(29, 137);
            this.chkStep6.Name = "chkStep6";
            this.chkStep6.Size = new System.Drawing.Size(15, 14);
            this.chkStep6.TabIndex = 5;
            this.chkStep6.UseVisualStyleBackColor = true;
            // 
            // chkStep5
            // 
            this.chkStep5.AutoSize = true;
            this.chkStep5.Checked = true;
            this.chkStep5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep5.Location = new System.Drawing.Point(29, 116);
            this.chkStep5.Name = "chkStep5";
            this.chkStep5.Size = new System.Drawing.Size(15, 14);
            this.chkStep5.TabIndex = 4;
            this.chkStep5.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(285, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "6- ذخیره در EMIS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(261, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "5- تولید فایلهای اکسل";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(263, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "4- ایجاد جدول خروجی";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(271, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "3- محاسبه پارامترها";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(147, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(223, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "2-دریافت اطلاعات از EMIS و پرکردن جدول اصلی";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(215, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "1- آماده سازی اولیه جدول اصلی";
            // 
            // btnPrevMonth
            // 
            this.btnPrevMonth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevMonth.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnPrevMonth.Location = new System.Drawing.Point(522, 147);
            this.btnPrevMonth.Name = "btnPrevMonth";
            this.btnPrevMonth.Size = new System.Drawing.Size(24, 23);
            this.btnPrevMonth.TabIndex = 5;
            this.btnPrevMonth.Text = "-";
            this.btnPrevMonth.UseVisualStyleBackColor = true;
            this.btnPrevMonth.Click += new System.EventHandler(this.btnPrevMonth_Click);
            // 
            // cBoxDaily
            // 
            this.cBoxDaily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cBoxDaily.BackColor = System.Drawing.Color.Transparent;
            this.cBoxDaily.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cBoxDaily.Checked = true;
            this.cBoxDaily.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBoxDaily.CheckValue = "Y";
            this.cBoxDaily.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cBoxDaily.Location = new System.Drawing.Point(567, 46);
            this.cBoxDaily.Name = "cBoxDaily";
            this.cBoxDaily.Size = new System.Drawing.Size(64, 19);
            this.cBoxDaily.TabIndex = 0;
            this.cBoxDaily.Text = "روزانه";
            this.cBoxDaily.CheckedChanged += new System.EventHandler(this.cBoxDaily_CheckedChanged);
            // 
            // cBoxConst
            // 
            this.cBoxConst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cBoxConst.AutoSize = true;
            this.cBoxConst.BackColor = System.Drawing.Color.Transparent;
            this.cBoxConst.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cBoxConst.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cBoxConst.Location = new System.Drawing.Point(427, 46);
            this.cBoxConst.Name = "cBoxConst";
            this.cBoxConst.Size = new System.Drawing.Size(50, 16);
            this.cBoxConst.TabIndex = 2;
            this.cBoxConst.Text = "قطعی";
            // 
            // cBoxMonthly
            // 
            this.cBoxMonthly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cBoxMonthly.AutoSize = true;
            this.cBoxMonthly.BackColor = System.Drawing.Color.Transparent;
            this.cBoxMonthly.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cBoxMonthly.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cBoxMonthly.Location = new System.Drawing.Point(495, 46);
            this.cBoxMonthly.Name = "cBoxMonthly";
            this.cBoxMonthly.Size = new System.Drawing.Size(54, 16);
            this.cBoxMonthly.TabIndex = 1;
            this.cBoxMonthly.Text = "ماهیانه";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            this.labelX4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX4.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.labelX4.Location = new System.Drawing.Point(607, 117);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(116, 16);
            this.labelX4.TabIndex = 63;
            this.labelX4.Text = "بازه زمانی صورت حساب:";
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            this.labelX2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX2.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.labelX2.Location = new System.Drawing.Point(595, 81);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(128, 16);
            this.labelX2.TabIndex = 63;
            this.labelX2.Text = "انتخاب عنوان صورت حساب:";
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            this.labelX1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.labelX1.Location = new System.Drawing.Point(637, 46);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(85, 16);
            this.labelX1.TabIndex = 63;
            this.labelX1.Text = "نوع صورت حساب:";
            // 
            // datePickerEnd
            // 
            this.datePickerEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.datePickerEnd.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.datePickerEnd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.datePickerEnd.IsNull = false;
            this.datePickerEnd.Location = new System.Drawing.Point(553, 179);
            this.datePickerEnd.Name = "datePickerEnd";
            this.datePickerEnd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.datePickerEnd.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.datePickerEnd.Size = new System.Drawing.Size(109, 20);
            this.datePickerEnd.TabIndex = 6;
            this.datePickerEnd.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // datePickerStart
            // 
            this.datePickerStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.datePickerStart.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.datePickerStart.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.datePickerStart.IsNull = false;
            this.datePickerStart.Location = new System.Drawing.Point(553, 147);
            this.datePickerStart.Name = "datePickerStart";
            this.datePickerStart.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.datePickerStart.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.datePickerStart.Size = new System.Drawing.Size(109, 20);
            this.datePickerStart.TabIndex = 4;
            this.datePickerStart.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // lblEDate
            // 
            this.lblEDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEDate.AutoSize = true;
            this.lblEDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblEDate.Location = new System.Drawing.Point(668, 181);
            this.lblEDate.Name = "lblEDate";
            this.lblEDate.Size = new System.Drawing.Size(54, 16);
            this.lblEDate.TabIndex = 60;
            this.lblEDate.Text = "تاریخ پایان :";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX3.AutoSize = true;
            this.labelX3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX3.Location = new System.Drawing.Point(661, 149);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(61, 16);
            this.labelX3.TabIndex = 59;
            this.labelX3.Text = "تاریخ شروع :";
            // 
            // cmBoxTitle
            // 
            this.cmBoxTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmBoxTitle.DisplayMember = "Text";
            this.cmBoxTitle.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmBoxTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBoxTitle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cmBoxTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.cmBoxTitle.ItemHeight = 15;
            this.cmBoxTitle.Items.AddRange(new object[] {
            this.comboItem2,
            this.comboItem1,
            this.comboItem3,
            this.comboItem4});
            this.cmBoxTitle.Location = new System.Drawing.Point(448, 76);
            this.cmBoxTitle.Name = "cmBoxTitle";
            this.cmBoxTitle.Size = new System.Drawing.Size(142, 21);
            this.cmBoxTitle.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.cmBoxTitle.TabIndex = 3;
            this.cmBoxTitle.WatermarkColor = System.Drawing.SystemColors.GradientInactiveCaption;
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "فروش";
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "خرید";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "خدمات انتقال";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "سوخت";
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.linkLblShowLog);
            this.TopPanel.Controls.Add(this.lblStatus);
            this.TopPanel.Controls.Add(this.label8);
            this.TopPanel.Controls.Add(this.lbRunNo);
            this.TopPanel.Controls.Add(this.label7);
            this.TopPanel.Controls.Add(this.btnRemove);
            this.TopPanel.Cursor = System.Windows.Forms.Cursors.Default;
            this.TopPanel.Location = new System.Drawing.Point(3, 3);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(752, 37);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.Color = System.Drawing.Color.Silver;
            this.TopPanel.Style.BackColor2.Color = System.Drawing.Color.White;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 43;
            // 
            // linkLblShowLog
            // 
            this.linkLblShowLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblShowLog.AutoSize = true;
            this.linkLblShowLog.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.linkLblShowLog.Location = new System.Drawing.Point(117, 14);
            this.linkLblShowLog.Name = "linkLblShowLog";
            this.linkLblShowLog.Size = new System.Drawing.Size(67, 13);
            this.linkLblShowLog.TabIndex = 68;
            this.linkLblShowLog.TabStop = true;
            this.linkLblShowLog.Text = "مشاهده لاگ";
            this.linkLblShowLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLblShowLog_LinkClicked);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblStatus.Location = new System.Drawing.Point(237, 12);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(330, 16);
            this.lblStatus.TabIndex = 67;
            this.lblStatus.Text = "اجرا نشده";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.Location = new System.Drawing.Point(572, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 16);
            this.label8.TabIndex = 67;
            this.label8.Text = "وضعیت :";
            // 
            // lbRunNo
            // 
            this.lbRunNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbRunNo.AutoSize = true;
            this.lbRunNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbRunNo.Location = new System.Drawing.Point(646, 12);
            this.lbRunNo.Name = "lbRunNo";
            this.lbRunNo.Size = new System.Drawing.Size(15, 16);
            this.lbRunNo.TabIndex = 66;
            this.lbRunNo.Text = "0";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.Location = new System.Drawing.Point(667, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 65;
            this.label7.Text = "شماره اجرا :";
            // 
            // btnRemove
            // 
            this.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnRemove.Image = global::MohanirPouya.Properties.Resources.icontexto_message_types_error_red1;
            this.btnRemove.Location = new System.Drawing.Point(1, 1);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnRemove.Size = new System.Drawing.Size(36, 34);
            this.btnRemove.TabIndex = 64;
            this.btnRemove.Tooltip = "حذف";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // SheduledRunControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainPanel);
            this.Name = "SheduledRunControl";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(758, 216);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx MainPanel;
        private System.Windows.Forms.Button btnPrevMonth;
        public DevComponents.DotNetBar.Controls.CheckBoxX cBoxDaily;
        public DevComponents.DotNetBar.Controls.CheckBoxX cBoxConst;
        public DevComponents.DotNetBar.Controls.CheckBoxX cBoxMonthly;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        public RPNCalendar.UI.Controls.PersianDatePicker datePickerEnd;
        public RPNCalendar.UI.Controls.PersianDatePicker datePickerStart;
        private DevComponents.DotNetBar.LabelX lblEDate;
        private DevComponents.DotNetBar.LabelX labelX3;
        public DevComponents.DotNetBar.Controls.ComboBoxEx cmBoxTitle;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkStep1;
        private System.Windows.Forms.CheckBox chkStep2;
        private System.Windows.Forms.CheckBox chkStep3;
        private System.Windows.Forms.CheckBox chkStep4;
        private System.Windows.Forms.CheckBox chkStep6;
        private System.Windows.Forms.CheckBox chkStep5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private DevComponents.DotNetBar.ButtonX btnRemove;
        private System.Windows.Forms.LinkLabel linkLblShowLog;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbRunNo;
        private System.Windows.Forms.Label label7;

    }
}
