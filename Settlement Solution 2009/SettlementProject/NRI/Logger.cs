﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using MohanirPouya.Classes;
using RPNCalendar.Utilities;

namespace NRI.Classes
{
    public  class Logger
    {
        private  Queue<LogItem> outPutLogQueue = new Queue<LogItem>();
        private  Queue<LogItem> dBLogQueue = new Queue<LogItem>();
        private string _startDate;
        private string _endDate;
        private string _billType;
        private string _ptypeE;
        private string _userName;
        private int _runId;
        Timer timer=new Timer();
        private int _progress;
        public  RichTextBox LogRichTextBox { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billType"> D , M , C</param>
        /// <param name="ptypeE"> Seller , Buyer , Transfer , Fuel</param>
        public Logger(RichTextBox logRichTextBox, Form sourceForm, string userName, string startDate,string endDate, string billType, string ptypeE)
        {
            LogRichTextBox = logRichTextBox;
            _userName = userName;
            _startDate = startDate;
            _endDate = endDate;
            _billType = billType;
            _ptypeE = ptypeE;

            LogRichTextBox.Text = " ";

            sourceForm.FormClosing += new FormClosingEventHandler(sourceForm_FormClosing);
            timer.Interval = 10000;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
            _runId = NewRun();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            StoreInDB();
        }

        void sourceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            StoreInDB();
           timer.Dispose();
        }

        private int NewRun()
        {
            int runId = -1;
            SqlConnection conn = new SqlConnection(DbBizClass.DbConnStrPre);
            try
            {
               
                SqlCommand cmd = new SqlCommand("dbo.NewRunId", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.AddWithValue("@UserName", _userName);
                cmd.Parameters.AddWithValue("@StartDate", _startDate);
                cmd.Parameters.AddWithValue("@EndDate", _endDate);
                cmd.Parameters.AddWithValue("@BillType", _billType);
                cmd.Parameters.AddWithValue("@PType", _ptypeE);
                PersianDate occuredDate = PersianDateConverter.ToPersianDate(DateTime.Now.Date);
                cmd.Parameters.AddWithValue("@Time",
                    String.Format("{0}/{1}/{2} {3}", occuredDate.Year, occuredDate.Month, occuredDate.Day,
                        DateTime.Now.ToShortTimeString()));
                SqlParameter paramRunId = new SqlParameter("@RunId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                cmd.Parameters.Add(paramRunId);
                conn.Open();
                cmd.ExecuteNonQuery();

                runId = (int) cmd.Parameters["@RunId"].Value;
            }
            catch (Exception ex)
            {
                AddToLogAndShow("Error in logging :" + ex.Message ,LogStyle.ErrorStyle);
            }
            finally
            {
                conn.Close();
            }
            return runId;
        }

        private void StoreInDB()
        {
            SqlConnection conn = new SqlConnection(DbBizClass.DbConnStrPre);
            string query = "";
            if (_runId != -1)
            {
                while (dBLogQueue.Count > 0)
                {
                   
                    LogItem logItem = dBLogQueue.Dequeue();
                    string q =
                        String.Format(
                            "INSERT INTO [dbo].[RunLogs]([RunId],[LogTime],[LogStyle],[LogText]) VALUES({0},'{1}','{2}','{3}') ",
                            _runId, logItem.OcuuredDate + " " + logItem.OcuuredTime, logItem.Style, logItem.Text.Replace("'","''"));
                    query = query + q;
                }
                if (query != "")
                {
                    try
                    {

                        SqlCommand cmd = new SqlCommand(query, conn);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        AddToLogAndShow("Error in logging :" + ex.Message, LogStyle.ErrorStyle);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }


        }

        /// <summary>
        /// Add an item to log queue 
        /// </summary>
        public void AddToLogQueue(string text, LogStyle style)
        {
            outPutLogQueue.Enqueue(new LogItem(text, style));
            dBLogQueue.Enqueue(new LogItem(text, style));
        }
        /// <summary>
        /// Show log queue items in output 
        /// </summary>
        public void UpdateLogOuput()
        {
            if (LogRichTextBox.InvokeRequired)
            {
                LogRichTextBox.BeginInvoke(new Action(delegate
                {
                    UpdateLogOuput();
                }));
                return;
            }
            while (outPutLogQueue.Count > 0)
            {
               
                LogItem logItem = outPutLogQueue.Dequeue();

                Color textColor = Color.Black;
                switch (logItem.Style)
                {
                    case LogStyle.InfoStyle:
                        textColor = Color.Black;
                        break;
                    case LogStyle.WarningStyle:
                        textColor = Color.BurlyWood;
                        break;
                    case LogStyle.ErrorStyle:
                        textColor = Color.Red;
                        break;
                }

               // AppendAndKeepLastLine(logItem, textColor);

                LogRichTextBox.SelectionStart = LogRichTextBox.TextLength;
                LogRichTextBox.SelectionLength = 0;

                LogRichTextBox.SelectionColor = textColor;
                LogRichTextBox.AppendText(String.Format("{0} ----- {1}\n", logItem.Text, logItem.OcuuredTime));
                LogRichTextBox.SelectionColor = LogRichTextBox.ForeColor;

                ScrollToBottom(LogRichTextBox);
                
                //logRichTextBox.SelectionStart = logRichTextBox.Text.Length;
                // logRichTextBox.ScrollToCaret();//Cause System.AccessViolationException

            }

        }
     

        /// <summary>
        /// Add an item to log queue and show log queue items in output 
        /// </summary>
        public void AddToLogAndShow(string text, LogStyle style)
        {
            outPutLogQueue.Enqueue(new LogItem(text, style));
            dBLogQueue.Enqueue(new LogItem(text, style));
            UpdateLogOuput();
        }

        

        public void AddToLogAndShow(Exception ex)
        {
            string msg = "";
            if (ex is SqlException)
            {
                SqlException sqlEx = ex as SqlException;
                msg = String.Format("Message:{0}, Procedure:{1}, LineNumber:{2}", sqlEx.Message, sqlEx.Procedure,
                    sqlEx.LineNumber);
            }
            else
            {
                msg = ex.Message;
            }
            AddToLogAndShow(msg, LogStyle.ErrorStyle);
        }

        public void ReportProgress(int progress)
        {
            if (LogRichTextBox.InvokeRequired)
            {
                LogRichTextBox.BeginInvoke(new Action(delegate
                {
                    ReportProgress(progress);
                }));
                return;
            }

            LogRichTextBox.SelectionColor = Color.Black;
            LogRichTextBox.AppendText(progress.ToString() + "%\n");
            LogRichTextBox.SelectionColor = LogRichTextBox.ForeColor;
           // LogRichTextBox.Lines[LogRichTextBox.Lines.Length - 1] = progress.ToString() + "%";
        }

        private void AppendAndKeepLastLine(LogItem logItem, Color textColor)
        {
            string lastLine = LogRichTextBox.Lines[LogRichTextBox.Lines.Length - 1];
            LogRichTextBox.Lines[LogRichTextBox.Lines.Length - 1] = "";

            LogRichTextBox.SelectionStart = LogRichTextBox.TextLength;
            LogRichTextBox.SelectionLength = 0;

            LogRichTextBox.SelectionColor = textColor;
            LogRichTextBox.AppendText(String.Format("{0} ----- {1}\n", logItem.Text, logItem.OcuuredTime));
            LogRichTextBox.SelectionColor = LogRichTextBox.ForeColor;

            LogRichTextBox.SelectionColor = Color.Black;
            LogRichTextBox.AppendText(lastLine);
            LogRichTextBox.SelectionColor = LogRichTextBox.ForeColor;

        }


        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);
        private const int WM_VSCROLL = 277;
        private const int SB_PAGEBOTTOM = 7;

        static void ScrollToBottom(RichTextBox myRichTextBox)
        {
            SendMessage(myRichTextBox.Handle, WM_VSCROLL, (IntPtr)SB_PAGEBOTTOM, IntPtr.Zero);
        }
    }

    internal class LogItem
    {
        public string Text { get; set; }
        public LogStyle Style { get; set; }
        public string OcuuredDate { get; set; }
        public string OcuuredTime { get; set; }

        public LogItem(string text, LogStyle style)
        {
            Text = text;
            Style = style;
            PersianDate occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            OcuuredDate = occuredDate.ToString().Substring(0, 10);
            OcuuredTime = DateTime.Now.ToShortTimeString();
        }
    }

    public enum LogStyle
    {
        InfoStyle,
        WarningStyle,
        ErrorStyle
    }

  
}
