﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using MohanirPouya.NRI;
using RPNCalendar.Utilities;
using NRI;
using DevComponents.DotNetBar;
using MohanirPouya.Forms.ManagementBill;
using System.Threading;
using NRI.Classes;
using RPNCalendar.UI.Design;

namespace MohanirPouya.NRI
{
    public partial class FrmAutomaticRun : Office2007Form
    {

        #region Field

        private string _ptypeE;
        private string _typeEN;
        private string _stype;

        private String ptype;
        private string type;
        private Int16 _diff;
        private string _weeKBDate;
        private string _startDate, _endDate;

        private BillSelectD _frmBillD;

        private int _runHour;
        private int _runMinute;
        private int _daysAgoNo;

        #endregion

        public FrmAutomaticRun()
        {
            InitializeComponent();


            ptype = "فروش";
            _ptypeE = "Sellers";
            _stype = "قطعی-صورتحساب";
           

            // timer1.Start();
        }

        //private void btnSelect_Click(object sender, EventArgs e)
        //{
         

        //    //while (true)
        //    //{
        //    //    Thread thread = new Thread(runBill);
        //    //    thread.Start();
        //    //    Thread.Sleep(60000); //sleep one minute
        //    //}


           
        //}

        private void runBill()
         {

            DateTime nowTime = DateTime.Now;
            TimeSpan remainingTime = new DateTime(1000, 1, 1, _runHour,
                  _runMinute, 0).Subtract(new DateTime(1000, 1, 1, nowTime.Hour, nowTime.Minute, 0));

            if (remainingTime.TotalMinutes < 0)
            {
                remainingTime = remainingTime.Add(new TimeSpan(24, 0, 0));
            }

            lblRemainingTime.Text = String.Format("{0} ساعت و {1} دقیقه", remainingTime.Hours, remainingTime.Minutes);


            if (remainingTime.TotalMinutes < 1) //nowTime.Hour == _runHour && nowTime.Minute == _runMinute
            {

                //1396/05/28
                AutomaticSteps.Step1 = chkStep1.Checked;
                AutomaticSteps.Step2 = chkStep2.Checked;
                AutomaticSteps.Step3 = chkStep3.Checked;
                AutomaticSteps.Step4 = chkStep4.Checked;
                AutomaticSteps.Step5 = chkStep5.Checked;
                AutomaticSteps.Step6 = chkStep6.Checked;

                if (radioBtnDaily.Checked)
                {
                    TimeSpan timeSpan = new TimeSpan(_daysAgoNo, 0, 0, 0);
                    PersianDateTime pdt = new PersianDateTime(nowTime.Subtract(timeSpan));


                    PersianDate weekB = PersianDateConverter.ToPersianDate(DateTime.Now.AddDays(-7).Date);
                    String weekBText = weekB.ToString();
                    string weeKBDate = weekBText.Substring(0, 10);

                    if (_frmBillD != null)
                        _frmBillD.Dispose();

                    _frmBillD = new BillSelectD(type, _typeEN, ptype, _ptypeE, pdt.PersianDateString,
                        pdt.PersianDateString, _diff, weeKBDate, _stype, false);
                }
                else
                {
                    _frmBillD = new BillSelectD(type, _typeEN, ptype, _ptypeE, _startDate, _endDate, _diff, _weeKBDate,
                        _stype, false);
                }
                //Application.DoEvents();
                // _frmBillD.ShowDialog();

            }

            //else if (nowTime.Hour == 6 && nowTime.Minute == 0)
            //{
            //    TimeSpan timeSpan5 = new TimeSpan(5, 0, 0, 0);
            //    PersianDateTime pdt = new PersianDateTime(nowTime.Subtract(timeSpan5));


            //    PersianDate weekB = PersianDateConverter.ToPersianDate(DateTime.Now.AddDays(-7).Date);
            //    String weekBText = weekB.ToString();
            //    string weeKBDate = weekBText.Substring(0, 10);

            //    if (_frmBillD != null)
            //        _frmBillD.Dispose();
            //    _frmBillD = new BillSelectD(type, _typeEN, ptype, _ptypeE, pdt.PersianDateString,
            //        pdt.PersianDateString, diff, weeKBDate, _stype);
            //    Application.DoEvents();
            //    _frmBillD.Show();
            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            runBill();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            
            PersianDateTime startDateTime, endDateTime;
            try
            {
                if (radioBtnMonthly.Checked)
                {
                    _startDate = maskedTxtDate.Text + "/01";
                    startDateTime=new PersianDateTime(_startDate);
                    _endDate = maskedTxtDate.Text + "/" + startDateTime.MonthDays;
                    endDateTime=new PersianDateTime(_endDate);
                    _diff = (Int16) (endDateTime.MiladiDateTime).Subtract(startDateTime.MiladiDateTime).Days;
                    _typeEN = "M";
                    type = "ماهيانه";
                     _weeKBDate = startDateTime.AddDays(-7).PersianDateString;

                }
                else
                {
                    _typeEN = "D";
                    type = "روزانه";
                    _diff = 0;
                    _daysAgoNo = int.Parse(maskedTxtDaysAgo.Text);
                }


                _runHour = dateTimePicker1.Value.Hour;
                _runMinute = dateTimePicker1.Value.Minute;
            }
            catch
            {
                MessageBox.Show("خطا");
                return;
            }

            DateTime nowTime = DateTime.Now;
            TimeSpan remainingTime;

            remainingTime = new DateTime(1000, 1, 1, _runHour,
                  _runMinute, 0).Subtract(new DateTime(1000, 1, 1, nowTime.Hour, nowTime.Minute, 0));

            if (remainingTime.TotalMinutes < 0)
            {
                remainingTime = remainingTime.Add(new TimeSpan(24, 0, 0));
            }

            lblRemainingTime.Text=String.Format("{0} ساعت و {1} دقیقه",remainingTime.Hours,remainingTime.Minutes);

            timer1.Start();
            panel1.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = true;

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            timer1.Stop();

            panel1.Enabled = true;
            btnStart.Enabled = true;
            btnStop.Enabled = false;
            lblRemainingTime.Text = "00 ساعت و 00 دقیقه";
        }
        // Prevent computer from entering sleep or hibernate (monitor not affected)
        private void PreventSleep()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS | NativeMethods.ES_SYSTEM_REQUIRED);
        }

        // Prevent the system from turning off monitor
        private void PreventMonitorOff()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS | NativeMethods.ES_DISPLAY_REQUIRED);
        }

        // Prevent the system from entering sleep and turning off monitor
        private void PreventSleepAndMonitorOff()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS | NativeMethods.ES_SYSTEM_REQUIRED | NativeMethods.ES_DISPLAY_REQUIRED);
        }

        // Clear EXECUTION_STATE flags to allow the system to sleep and turn off monitor normally
        private void AllowSleep()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS);
        }

        private void FrmAutomaticRun_Load(object sender, EventArgs e)
        {
           // PreventSleep();
           // PreventSleepAndMonitorOff();
        }

        private void FrmAutomaticRun_FormClosed(object sender, FormClosedEventArgs e)
        {
           // AllowSleep();
        }

        private void radioBtnDaily_CheckedChanged(object sender, EventArgs e)
        {
            if (radioBtnDaily.Checked)
            {
                maskedTxtDate.Enabled = false;
                maskedTxtDaysAgo.Enabled = true;
            }
            else
            {
                maskedTxtDate.Enabled = true;
                maskedTxtDaysAgo.Enabled = false;
            }
        }

    


       

    }

    internal static class NativeMethods
    {
        // Import SetThreadExecutionState Win32 API and necessary flags
        [DllImport("kernel32.dll")]
        public static extern uint SetThreadExecutionState(uint esFlags);
        public const uint ES_CONTINUOUS = 0x80000000;
        public const uint ES_SYSTEM_REQUIRED = 0x00000001;
        public const uint ES_DISPLAY_REQUIRED = 0x00000002;
    }
}
