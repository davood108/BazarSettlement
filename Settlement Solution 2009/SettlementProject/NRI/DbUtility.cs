﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using MohanirPouya.Classes;
using NRI.Classes;


namespace MohanirPouya.NRI
{
    class DbUtility
    {
        public static DataTable GetTable(string selectQuery)
        {
            DataTable dt = new DataTable();
            SqlConnection connection = new SqlConnection(DbBizClass.DbConnStr);
            SqlCommand cmd = new SqlCommand(selectQuery, connection);

            try
            {
                connection.Open();
                dt.Load(cmd.ExecuteReader());

            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return dt;
        }

        public static object GetScalar(string selectQuery)
        {
            object result;
            SqlConnection connection = new SqlConnection(DbBizClass.DbConnStr);
            SqlCommand cmd = new SqlCommand(selectQuery, connection);

            try
            {
                connection.Open();
                result = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public static int RunQuery(string query)
        {
            int rowsNO = 0;
            SqlConnection connection = new SqlConnection(DbBizClass.DbConnStr);
            SqlCommand cmd = new SqlCommand(query, connection);

            try
            {
                connection.Open();
                rowsNO=cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                connection.Close();
            }
            return rowsNO;
        }

        public static int RunQuery(SqlCommand command)
        {
            int rowsNO = 0;
            SqlConnection connection = new SqlConnection(DbBizClass.DbConnStr);
            command.Connection = connection;

            try
            {
                connection.Open();
                rowsNO = command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                connection.Close();
            }
            return rowsNO;
        }
    }
}
