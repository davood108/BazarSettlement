﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.Properties;


namespace MohanirPouya.Forms.Help
{
    public partial class FrmDetailMessageBox : MohanirFormDialog
    {
        private bool _changeSize;
        private readonly string _mainText;
        private readonly string _sysText;
        private readonly string _iconType;
/*
        private string _ButtonDisplayType;
*/

        public FrmDetailMessageBox(string mainText,string sysText,string iconType)
        {
            InitializeComponent();
            _changeSize = false;
            _mainText = mainText;
            _sysText = sysText;
            _iconType = iconType;
         
        }

        private void PbDetailMessageBoxShowClick(object sender, EventArgs e)
        {
            if (_changeSize == false)
            {
                Size = new Size(463,346);
                pbDetailMessageBoxShow.Image = Resources.me5;
                _changeSize = true;
                btnCopyToClipboard.Focus();
            }
            else
            {
                Size = new Size(463,180);
                pbDetailMessageBoxShow.Image = Resources.me5;
                _changeSize = false;
                pbDetailMessageBoxShow.Focus();
            }
        }

        private void FrmDetailMessageBoxLoad(object sender, EventArgs e)
        {
            pbDetailMessageBoxShow.Focus();
            txtDetailMessageBoxMain.Text = _mainText;
            txtDetailMessageBoxSys.Text = _sysText;

            switch (_iconType)
            {
                case "Error":
                    pbDetailMessageBox.Image = Resources.icontexto_message_types_error_red1;
                    btnDetailMessageBoxYes.Visible = false;
                    btnDetailMessageBoxNo.Visible = false;
                    break;

               case "Question":
                    pbDetailMessageBox.Image = Resources.icontexto_message_types_question_red1;
                    btnDetailMessageBoxOk.Visible = false;
                    break;

               case "Exclamation Point":
                    pbDetailMessageBox.Image = Resources.icontexto_message_types_info_red;
                    btnDetailMessageBoxOk.Visible = false;
                    break;
            }
        }

        private void BtnCopyToClipboardClick(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(txtDetailMessageBoxSys.Text, true);
            System.Diagnostics.Process.Start("notepad.exe");
        }

        private void BtnDetailMessageBoxOkClick(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnDetailMessageBoxYesClick(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnDetailMessageBoxNoClick(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmDetailMessageBoxHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "DetailMessageBox.htm");
        }

    }
}
