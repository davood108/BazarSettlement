﻿using System;
using System.Windows.Forms;

namespace MohanirPouya.Forms.Help
{
    partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }

        private void lblProductName_Click(object sender, EventArgs e)
        {

        }

        private void frmAbout_KeyPress(object sender, KeyPressEventArgs e)
        {
           

        }

        private void frmAbout_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "About.htm");
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {

        }

       

    }
}