﻿using System;
using System.Drawing;

namespace MohanirPouya.Forms.Help
{
    partial class FrmDetailMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.pbDetailMessageBox = new System.Windows.Forms.PictureBox();
            this.btnDetailMessageBoxOk = new DevComponents.DotNetBar.ButtonX();
            this.btnDetailMessageBoxNo = new DevComponents.DotNetBar.ButtonX();
            this.btnDetailMessageBoxYes = new DevComponents.DotNetBar.ButtonX();
            this.pbDetailMessageBoxShow = new System.Windows.Forms.PictureBox();
            this.btnCopyToClipboard = new DevComponents.DotNetBar.ButtonX();
            this.plDetailMessageBoxUp = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtDetailMessageBoxSys = new System.Windows.Forms.TextBox();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rsDetaimMessageBoxDown = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.txtDetailMessageBoxMain = new System.Windows.Forms.TextBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rsDetailMessageBoxUp = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.MyMainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDetailMessageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDetailMessageBoxShow)).BeginInit();
            this.plDetailMessageBoxUp.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubtitle.Location = new System.Drawing.Point(107, 211);
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Location = new System.Drawing.Point(108, 257);
            // 
            // lblSep
            // 
            this.lblSep.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSep.Location = new System.Drawing.Point(688, 66);
            this.lblSep.Size = new System.Drawing.Size(136, 114);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.plDetailMessageBoxUp);
            this.MyMainPanel.Size = new System.Drawing.Size(457, 152);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.plDetailMessageBoxUp, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            // 
            // pbDetailMessageBox
            // 
            this.pbDetailMessageBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbDetailMessageBox.ErrorImage = global::MohanirPouya.Properties.Resources.icontexto_message_types_error_red1;
            this.pbDetailMessageBox.Image = global::MohanirPouya.Properties.Resources.icontexto_message_types_alert_red1;
            this.pbDetailMessageBox.InitialImage = global::MohanirPouya.Properties.Resources.icontexto_message_types_question_red1;
            this.pbDetailMessageBox.Location = new System.Drawing.Point(419, 46);
            this.pbDetailMessageBox.Name = "pbDetailMessageBox";
            this.pbDetailMessageBox.Size = new System.Drawing.Size(31, 30);
            this.pbDetailMessageBox.TabIndex = 2;
            this.pbDetailMessageBox.TabStop = false;
            // 
            // btnDetailMessageBoxOk
            // 
            this.btnDetailMessageBoxOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDetailMessageBoxOk.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDetailMessageBoxOk.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnDetailMessageBoxOk.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDetailMessageBoxOk.ForeColor = System.Drawing.Color.Green;
            this.btnDetailMessageBoxOk.Location = new System.Drawing.Point(187, 121);
            this.btnDetailMessageBoxOk.Name = "btnDetailMessageBoxOk";
            this.btnDetailMessageBoxOk.Size = new System.Drawing.Size(60, 23);
            this.btnDetailMessageBoxOk.TabIndex = 3;
            this.btnDetailMessageBoxOk.Text = "تایید";
            this.btnDetailMessageBoxOk.Click += new System.EventHandler(this.BtnDetailMessageBoxOkClick);
            // 
            // btnDetailMessageBoxNo
            // 
            this.btnDetailMessageBoxNo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDetailMessageBoxNo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDetailMessageBoxNo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnDetailMessageBoxNo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetailMessageBoxNo.ForeColor = System.Drawing.Color.Green;
            this.btnDetailMessageBoxNo.Location = new System.Drawing.Point(139, 121);
            this.btnDetailMessageBoxNo.Name = "btnDetailMessageBoxNo";
            this.btnDetailMessageBoxNo.Size = new System.Drawing.Size(60, 23);
            this.btnDetailMessageBoxNo.TabIndex = 5;
            this.btnDetailMessageBoxNo.Text = "خیر";
            this.btnDetailMessageBoxNo.Click += new System.EventHandler(this.BtnDetailMessageBoxNoClick);
            // 
            // btnDetailMessageBoxYes
            // 
            this.btnDetailMessageBoxYes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDetailMessageBoxYes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDetailMessageBoxYes.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnDetailMessageBoxYes.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDetailMessageBoxYes.ForeColor = System.Drawing.Color.Green;
            this.btnDetailMessageBoxYes.Location = new System.Drawing.Point(238, 121);
            this.btnDetailMessageBoxYes.Name = "btnDetailMessageBoxYes";
            this.btnDetailMessageBoxYes.Size = new System.Drawing.Size(60, 23);
            this.btnDetailMessageBoxYes.TabIndex = 4;
            this.btnDetailMessageBoxYes.Text = "بله";
            this.btnDetailMessageBoxYes.Click += new System.EventHandler(this.BtnDetailMessageBoxYesClick);
            // 
            // pbDetailMessageBoxShow
            // 
            this.pbDetailMessageBoxShow.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbDetailMessageBoxShow.ErrorImage = global::MohanirPouya.Properties.Resources.me4;
            this.pbDetailMessageBoxShow.Image = global::MohanirPouya.Properties.Resources.me4;
            this.pbDetailMessageBoxShow.InitialImage = global::MohanirPouya.Properties.Resources.me5;
            this.pbDetailMessageBoxShow.Location = new System.Drawing.Point(431, 121);
            this.pbDetailMessageBoxShow.Name = "pbDetailMessageBoxShow";
            this.pbDetailMessageBoxShow.Size = new System.Drawing.Size(19, 17);
            this.pbDetailMessageBoxShow.TabIndex = 6;
            this.pbDetailMessageBoxShow.TabStop = false;
            this.pbDetailMessageBoxShow.Click += new System.EventHandler(this.PbDetailMessageBoxShowClick);
            // 
            // btnCopyToClipboard
            // 
            this.btnCopyToClipboard.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCopyToClipboard.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCopyToClipboard.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnCopyToClipboard.Font = new System.Drawing.Font("Aban Bold", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCopyToClipboard.ForeColor = System.Drawing.Color.Green;
            this.btnCopyToClipboard.Location = new System.Drawing.Point(145, 40);
            this.btnCopyToClipboard.Name = "btnCopyToClipboard";
            this.btnCopyToClipboard.Size = new System.Drawing.Size(163, 27);
            this.btnCopyToClipboard.TabIndex = 7;
            this.btnCopyToClipboard.Text = "Copy To Clipboard";
            this.btnCopyToClipboard.Click += new System.EventHandler(this.BtnCopyToClipboardClick);
            // 
            // plDetailMessageBoxUp
            // 
            this.plDetailMessageBoxUp.AutoSize = true;
            this.plDetailMessageBoxUp.Controls.Add(this.panel1);
            this.plDetailMessageBoxUp.Controls.Add(this.pbDetailMessageBoxShow);
            this.plDetailMessageBoxUp.Controls.Add(this.btnDetailMessageBoxYes);
            this.plDetailMessageBoxUp.Controls.Add(this.btnDetailMessageBoxNo);
            this.plDetailMessageBoxUp.Controls.Add(this.btnDetailMessageBoxOk);
            this.plDetailMessageBoxUp.Controls.Add(this.pbDetailMessageBox);
            this.plDetailMessageBoxUp.Controls.Add(this.txtDetailMessageBoxMain);
            this.plDetailMessageBoxUp.Controls.Add(this.shapeContainer2);
            this.plDetailMessageBoxUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plDetailMessageBoxUp.Location = new System.Drawing.Point(0, 0);
            this.plDetailMessageBoxUp.Name = "plDetailMessageBoxUp";
            this.plDetailMessageBoxUp.Size = new System.Drawing.Size(457, 152);
            this.plDetailMessageBoxUp.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnCopyToClipboard);
            this.panel1.Controls.Add(this.txtDetailMessageBoxSys);
            this.panel1.Controls.Add(this.shapeContainer3);
            this.panel1.Location = new System.Drawing.Point(3, 165);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(457, 3);
            this.panel1.TabIndex = 9;
            // 
            // txtDetailMessageBoxSys
            // 
            this.txtDetailMessageBoxSys.Location = new System.Drawing.Point(12, 3);
            this.txtDetailMessageBoxSys.Multiline = true;
            this.txtDetailMessageBoxSys.Name = "txtDetailMessageBoxSys";
            this.txtDetailMessageBoxSys.Size = new System.Drawing.Size(430, 108);
            this.txtDetailMessageBoxSys.TabIndex = 3;
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rsDetaimMessageBoxDown});
            this.shapeContainer3.Size = new System.Drawing.Size(457, 3);
            this.shapeContainer3.TabIndex = 0;
            this.shapeContainer3.TabStop = false;
            // 
            // rsDetaimMessageBoxDown
            // 
            this.rsDetaimMessageBoxDown.Location = new System.Drawing.Point(6, 2);
            this.rsDetaimMessageBoxDown.Name = "rsDetaimMessageBoxDown";
            this.rsDetaimMessageBoxDown.Size = new System.Drawing.Size(437, 115);
            // 
            // txtDetailMessageBoxMain
            // 
            this.txtDetailMessageBoxMain.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtDetailMessageBoxMain.Location = new System.Drawing.Point(8, 6);
            this.txtDetailMessageBoxMain.Multiline = true;
            this.txtDetailMessageBoxMain.Name = "txtDetailMessageBoxMain";
            this.txtDetailMessageBoxMain.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDetailMessageBoxMain.Size = new System.Drawing.Size(405, 105);
            this.txtDetailMessageBoxMain.TabIndex = 1;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rsDetailMessageBoxUp});
            this.shapeContainer2.Size = new System.Drawing.Size(457, 152);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // rsDetailMessageBoxUp
            // 
            this.rsDetailMessageBoxUp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rsDetailMessageBoxUp.Location = new System.Drawing.Point(6, 5);
            this.rsDetailMessageBoxUp.Name = "rsDetailMessageBoxUp";
            this.rsDetailMessageBoxUp.Size = new System.Drawing.Size(408, 109);
            // 
            // FrmDetailMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 152);
            this.Name = "FrmDetailMessageBox";
            this.Text = "صفحه پیغام خطا";
            this.Load += new System.EventHandler(this.FrmDetailMessageBoxLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FrmDetailMessageBoxHelpRequested);
            //this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmDetailMessageBox_KeyDown);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDetailMessageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDetailMessageBoxShow)).EndInit();
            this.plDetailMessageBoxUp.ResumeLayout(false);
            this.plDetailMessageBoxUp.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel plDetailMessageBoxUp;
        private DevComponents.DotNetBar.ButtonX btnCopyToClipboard;
        private System.Windows.Forms.PictureBox pbDetailMessageBoxShow;
        private DevComponents.DotNetBar.ButtonX btnDetailMessageBoxYes;
        private DevComponents.DotNetBar.ButtonX btnDetailMessageBoxNo;
        private DevComponents.DotNetBar.ButtonX btnDetailMessageBoxOk;
        private System.Windows.Forms.PictureBox pbDetailMessageBox;
        private System.Windows.Forms.TextBox txtDetailMessageBoxMain;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rsDetailMessageBoxUp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtDetailMessageBoxSys;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rsDetaimMessageBoxDown;

       
    }
}