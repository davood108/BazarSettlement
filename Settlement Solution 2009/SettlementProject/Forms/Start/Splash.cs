﻿#region using
using System;
using System.Windows.Forms;
#endregion

namespace MohanirPouya.Forms.Start
{
    public partial class FrmSplash : Form
    {

        #region Ctor

        public FrmSplash()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void SplashTimerTick(object sender, EventArgs e)
        {
            Hide();
            GC.Collect();
            SplashTimer.Enabled = false;
            SplashTimer.Tick -= SplashTimerTick;
             new DbConnection();
         
        }

        #endregion

     

    }
}