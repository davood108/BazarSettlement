﻿namespace MohanirPouya.Forms.Start.UsersAndGroups
{
    partial class DefineUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblFamily = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPasswprd = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtFamily = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.Apply = new DevComponents.DotNetBar.ButtonX();
            this.cbActiveIsActive = new System.Windows.Forms.CheckBox();
            this.MyMainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(12, 0);
            this.lblSubtitle.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(161, 23);
            this.lblTitle.Size = new System.Drawing.Size(130, 25);
            this.lblTitle.Tag = "";
            this.lblTitle.Text = "تعریف کاربران";
            // 
            // lblSep
            // 
            this.lblSep.Size = new System.Drawing.Size(464, 2);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.cbActiveIsActive);
            this.MyMainPanel.Controls.Add(this.Apply);
            this.MyMainPanel.Controls.Add(this.lblFamily);
            this.MyMainPanel.Controls.Add(this.txtPassword);
            this.MyMainPanel.Controls.Add(this.txtFamily);
            this.MyMainPanel.Controls.Add(this.lblType);
            this.MyMainPanel.Controls.Add(this.lblName);
            this.MyMainPanel.Controls.Add(this.lblUserName);
            this.MyMainPanel.Controls.Add(this.txtName);
            this.MyMainPanel.Controls.Add(this.txtUserName);
            this.MyMainPanel.Controls.Add(this.lblPasswprd);
            this.MyMainPanel.Controls.Add(this.cbType);
            this.MyMainPanel.Size = new System.Drawing.Size(460, 289);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.cbType, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblPasswprd, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtUserName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblUserName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblType, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtFamily, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtPassword, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblFamily, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.Apply, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.cbActiveIsActive, 0);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(365, 82);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(50, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = ": نام کاربر";
            // 
            // lblFamily
            // 
            this.lblFamily.AutoSize = true;
            this.lblFamily.Location = new System.Drawing.Point(365, 114);
            this.lblFamily.Name = "lblFamily";
            this.lblFamily.Size = new System.Drawing.Size(92, 13);
            this.lblFamily.TabIndex = 3;
            this.lblFamily.Text = ":نام خانوادگی کاربر";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(365, 150);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(60, 13);
            this.lblUserName.TabIndex = 4;
            this.lblUserName.Text = ": نام کاربری";
            // 
            // lblPasswprd
            // 
            this.lblPasswprd.AutoSize = true;
            this.lblPasswprd.Location = new System.Drawing.Point(365, 180);
            this.lblPasswprd.Name = "lblPasswprd";
            this.lblPasswprd.Size = new System.Drawing.Size(52, 13);
            this.lblPasswprd.TabIndex = 5;
            this.lblPasswprd.Text = ": رمز عبور";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(229, 79);
            this.txtName.Name = "txtName";
            this.txtName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtName.Size = new System.Drawing.Size(122, 21);
            this.txtName.TabIndex = 6;
            // 
            // txtFamily
            // 
            this.txtFamily.Location = new System.Drawing.Point(229, 114);
            this.txtFamily.Name = "txtFamily";
            this.txtFamily.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtFamily.Size = new System.Drawing.Size(122, 21);
            this.txtFamily.TabIndex = 7;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(229, 147);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtUserName.Size = new System.Drawing.Size(122, 21);
            this.txtUserName.TabIndex = 8;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(229, 180);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPassword.Size = new System.Drawing.Size(122, 21);
            this.txtPassword.TabIndex = 9;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(365, 216);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(52, 13);
            this.lblType.TabIndex = 10;
            this.lblType.Text = ": نوع کاربر";
            // 
            // cbType
            // 
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "لطفا انتخاب نمایید",
            "مدیر",
            "کارشناس ارشد"});
            this.cbType.Location = new System.Drawing.Point(229, 214);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(122, 21);
            this.cbType.TabIndex = 11;
            // 
            // Apply
            // 
            this.Apply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.Apply.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.Apply.Location = new System.Drawing.Point(3, 254);
            this.Apply.Name = "Apply";
            this.Apply.Size = new System.Drawing.Size(89, 23);
            this.Apply.TabIndex = 14;
            this.Apply.Text = "ذخیره";
            this.Apply.Click += new System.EventHandler(this.ApplyClick);
            // 
            // cbActiveIsActive
            // 
            this.cbActiveIsActive.AutoSize = true;
            this.cbActiveIsActive.Location = new System.Drawing.Point(269, 254);
            this.cbActiveIsActive.Name = "cbActiveIsActive";
            this.cbActiveIsActive.Size = new System.Drawing.Size(145, 17);
            this.cbActiveIsActive.TabIndex = 16;
            this.cbActiveIsActive.Text = "فعال یا غیر فعال بودن کاربر";
            this.cbActiveIsActive.UseVisualStyleBackColor = true;
            // 
            // DefineUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 289);
            this.FormTitle = "تعریف کاربران";
            this.Name = "DefineUser";
            this.Text = "فرم تعریف کاربران";
            this.Load += new System.EventHandler(this.DefineUserLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.DefineUserHelpRequested);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFamily;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtFamily;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblPasswprd;
        private System.Windows.Forms.ComboBox cbType;
        private DevComponents.DotNetBar.ButtonX Apply;
        private System.Windows.Forms.CheckBox cbActiveIsActive;
    }
}