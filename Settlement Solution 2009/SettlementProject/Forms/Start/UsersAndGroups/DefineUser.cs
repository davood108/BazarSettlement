﻿using System;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace MohanirPouya.Forms.Start.UsersAndGroups
{
    public partial class DefineUser : MohanirFormDialog
    {
        private readonly PreDbmsDataContext _pdbSM;
        private readonly int _frmMode;

        public DefineUser()
        {
            InitializeComponent();
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
            cbType.SelectedIndex = 0;
             _frmMode = 1;
            ShowDialog();
        }

        public DefineUser(string strName,string strFamily,string strUserName,string strPassword,string strType,bool strActiveIsActive)
        {
            InitializeComponent();
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
            _frmMode = 2;
            txtName.Text = strName;
            txtFamily.Text = strFamily;
            txtUserName.Text = strUserName;
            txtPassword.Text = strPassword;
            cbType.Text = strType;
            cbActiveIsActive.Checked = strActiveIsActive;
            txtUserName.Enabled = false;
            if (strType == "مدير")
            {
                cbType.SelectedIndex = 1;
            }
            if (strType == "کارشناس ارشد")
            {
                cbType.SelectedIndex = 2;
            }
            ShowDialog();
        }

    
        private void DefineUserLoad(object sender, EventArgs e)
        {

        }

        private void ApplyClick(object sender, EventArgs e)
        {
            if (_frmMode == 1)
            {
                if (txtFamily.Text == "" || txtName.Text == "" || txtPassword.Text == "" || txtUserName.Text == "" ||
                    cbType.SelectedIndex == 0)
                {
                    new Help.FrmDetailMessageBox("لطفا اطلاعات را کامل وارد نمایید", "لطفا اطلاعات را کامل وارد نمایید",
                        "Error").ShowDialog();
                    return;
                }

                # region insertUser

                # region cbType1

                if (cbType.SelectedIndex == 1)
                {
                    if (cbActiveIsActive.Checked)
                    {
                        var connection = new SqlConnection(DbBizClass.DbConnStrPre);
                        string strConnection =
                            "insert into [Security].[Tbl_Users] ([IsActive],[Type],[UserName],[Password]," +
                            "[FirstName],[LastName],[Description]) " +
                            " values (1,1,'" + txtUserName.Text + "' ,'" + txtPassword.Text + "','" + txtName.Text +
                            "','" + txtFamily.Text + "','" + cbType.SelectedItem + "') ";
                        connection.Open();
                        var dt = new DataTable();
                        var adapter = new SqlDataAdapter(strConnection, connection);
                        new SqlCommandBuilder(adapter);
                        adapter.Fill(dt);
                        connection.Close();
                    }
                    else
                    {
                        var connection = new SqlConnection(DbBizClass.DbConnStrPre);
                        string strConnection =
                            "insert into [Security].[Tbl_Users] ([IsActive],[Type],[UserName],[Password]," +
                            "[FirstName],[LastName],[Description]) " +
                            " values (0,1,'" + txtUserName.Text + "' ,'" + txtPassword.Text + "','" + txtName.Text +
                            "','" + txtFamily.Text + "','" + cbType.SelectedItem + "') ";
                        connection.Open();
                        var dt = new DataTable();
                        var adapter = new SqlDataAdapter(strConnection, connection);
                        new SqlCommandBuilder(adapter);
                        adapter.Fill(dt);
                        connection.Close();
                    }

                }

                #endregion

                # region cbType2

                if (cbType.SelectedIndex == 2)
                {
                    if (cbActiveIsActive.Checked)
                    {
                        var connection = new SqlConnection(DbBizClass.DbConnStrPre);
                        string strConnection =
                            "insert into [Security].[Tbl_Users] ([IsActive],[Type],[UserName],[Password]," +
                            "[FirstName],[LastName],[Description]) " +
                            " values (1,2,'" + txtUserName.Text + "' ,'" + txtPassword.Text + "','" + txtName.Text +
                            "','" + txtFamily.Text + "', '" + cbType.SelectedItem + "') ";
                        connection.Open();
                        var dt = new DataTable();
                        var adapter = new SqlDataAdapter(strConnection, connection);
                        new SqlCommandBuilder(adapter);
                        adapter.Fill(dt);
                        connection.Close();
                    }
                    else
                    {
                        var connection = new SqlConnection(DbBizClass.DbConnStrPre);
                        string strConnection =
                            "insert into [Security].[Tbl_Users] ([IsActive],[Type],[UserName],[Password]," +
                            "[FirstName],[LastName],[Description]) " +
                            " values (0,2,'" + txtUserName.Text + "' ,'" + txtPassword.Text + "','" + txtName.Text +
                            "','" + txtFamily.Text + "', '" + cbType.SelectedItem + "') ";
                        connection.Open();
                        var dt = new DataTable();
                        var adapter = new SqlDataAdapter(strConnection, connection);
                        new SqlCommandBuilder(adapter);
                        adapter.Fill(dt);
                        connection.Close();
                    }
                }

                #endregion

                txtName.Text = "";
                txtFamily.Text = "";
                txtUserName.Text = "";
                txtPassword.Text = "";
                cbType.SelectedIndex = 0;
                cbActiveIsActive.Checked = false;

                #endregion
            }

   
            if (_frmMode == 2)
            {
                var userD = new Tbl_User
                                {
                                                          IsActive = cbActiveIsActive.Checked,
                                                          Type = (byte)cbType.SelectedIndex,
                                                          UserName = txtUserName.Text,
                                                          Password = txtPassword.Text,
                                                          FirstName = txtName.Text,
                                                          LastName = txtFamily.Text,
                                                          Description = cbType.SelectedItem.ToString()
                                                      };

                var query2 = (from var in _pdbSM.Tbl_Users where var.UserName == txtUserName.Text select var);
                if (query2.Any())
                {
                            foreach (var var in query2)
                            {
                                var.IsActive = userD.IsActive;
                                var.Type = userD.Type;
                                var.UserName = userD.UserName;
                                var.Password = userD.Password;
                                var.FirstName =userD.FirstName;
                                var.LastName = userD.LastName;
                                var.Description = userD.Description;
                            }
                            _pdbSM.SubmitChanges();
                        }

                
                 Close();
               
                }
               
            }

        private void DefineUserHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "DefineUser.htm");
        }
        }


    }


