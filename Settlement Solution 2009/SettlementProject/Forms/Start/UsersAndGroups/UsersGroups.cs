﻿using System;
using System.Data.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using System.Data;
using System.Data.SqlClient;

namespace MohanirPouya.Forms.Start.UsersAndGroups
{
    public partial class UsersGroups : MohanirFormDialog
    {
        private PreDbmsDataContext _pdbSM;
        private Table<Tbl_User> _sourceTbl;
        private Table<Tbl_User> _sourceTbl1;

        //private Table<Tbl_User> _dataSource;

        public UsersGroups()
        {
      
            InitializeComponent();
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
          dgvUser.AutoGenerateColumns = false;


            lblSep.Visible = false;
            lblSubtitle.Visible = false;
            lblTitle.Visible = false;
            label2.Text = @"لیست کاربران";

            ShowDialog();

        }

        private void UsersGroupsLoad(object sender, EventArgs e)
        {
            FilldgvDataSource();
        }

        #region Method

        #region FilldgvDataSource

        private void FilldgvDataSource()
        {

            _sourceTbl = _pdbSM.Tbl_Users;

            dgvUser.DataSource = _sourceTbl;
            dgvUser.Columns[3].Visible = false;//hide password. 1394/11/12
   
        }

        #endregion

        #endregion



        private void BtnDeleteClick(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvUser.SelectedRows)
            {
                string strUserName = row.Cells[2].Value.ToString();

                #region Delete

                switch (
                    MessageBox.Show(@"آیا مطمئن هستید که می خواهید حذف کنید؟", @"پرسش جهت حذف", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Yes:
                        var connection = new SqlConnection(DbBizClass.DbConnStrPre);
                        string strConnection = "Delete [Security].[Tbl_Users] WHERE  UserName='" + strUserName + "' ";
                        connection.Open();
                        var dt = new DataTable();
                        var adapter = new SqlDataAdapter(strConnection, connection);
                        new SqlCommandBuilder(adapter);
                        adapter.Fill(dt);
                        dgvUser.DataSource = dt;
                        connection.Close();
                        break;
                    case DialogResult.No:
                        break;
                }

                #endregion

                #region FillGrid

                var connection1 = new SqlConnection(DbBizClass.DbConnStrPre);
                const string strConnection1 = "Select isactive,type,username,password,firstname,lastname,description from [Security].[Tbl_Users] ";
                connection1.Open();
                var dt1 = new DataTable();
                var adapter1 = new SqlDataAdapter(strConnection1, connection1);
                new SqlCommandBuilder(adapter1);
                adapter1.Fill(dt1);
                dgvUser.DataSource = dt1;
                connection1.Close();

                #endregion
            }
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvUser.SelectedRows)
            {
                string strName = row.Cells[0].Value.ToString();
                string strFamily = row.Cells[1].Value.ToString();
                string strUserName = row.Cells[2].Value.ToString();
                string strPassword = row.Cells[3].Value.ToString();
                string strType = row.Cells[4].Value.ToString();
                bool strActiveIsActive = Convert.ToBoolean(row.Cells[5].Value);

                new DefineUser(strName, strFamily, strUserName, strPassword, strType, strActiveIsActive);





                _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
               _sourceTbl1 = _pdbSM.Tbl_Users;
                dgvUser.DataSource = _sourceTbl1;
            dgvUser.Refresh();


            }
        }

        private void CloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private void UsersGroupsHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "UsersGroups.htm");
            
        }

        private void BtnAddClick(object sender, EventArgs e)
        {
            new DefineUser();
        }
    }
}





