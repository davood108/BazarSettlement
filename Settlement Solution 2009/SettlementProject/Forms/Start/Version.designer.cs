﻿namespace MohanirPouya.Forms.Start
{
    partial class FrmVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.MyMainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(463, 86);
            this.lblSubtitle.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(348, 86);
            this.lblTitle.Visible = false;
            // 
            // lblSep
            // 
            this.lblSep.Size = new System.Drawing.Size(545, 2);
            this.lblSep.Visible = false;
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.btnOk);
            this.MyMainPanel.Controls.Add(this.lblVersion);
            this.MyMainPanel.Size = new System.Drawing.Size(541, 136);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblVersion, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnOk, 0);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblVersion.Location = new System.Drawing.Point(60, 36);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblVersion.Size = new System.Drawing.Size(406, 16);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "ورژن فایلهای template تعییر یافته است.تغییرات لازم اعمال گردید.";
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnOk.Location = new System.Drawing.Point(11, 101);
            this.btnOk.Name = "btnOk";
            this.btnOk.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnOk.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnOk.Size = new System.Drawing.Size(68, 23);
            this.btnOk.TabIndex = 51;
            this.btnOk.Text = "تایید";
            this.btnOk.Tooltip = "بررسي دستورات وارد شده";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // FrmVersion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 136);
            this.DoubleBuffered = true;
            this.Name = "FrmVersion";
            this.Text = "تغییر ورژن برنامه";
            this.Load += new System.EventHandler(this.FrmVersion_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FrmVersion_HelpRequested);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblVersion;
        private DevComponents.DotNetBar.ButtonX btnOk;
    }
}

