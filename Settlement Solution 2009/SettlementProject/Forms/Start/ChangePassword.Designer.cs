﻿namespace MohanirPouya.Forms.Start
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.lblOldPass = new System.Windows.Forms.Label();
            this.lblNewPass = new System.Windows.Forms.Label();
            this.lblCopyNewPass = new System.Windows.Forms.Label();
            this.txtOldPass = new System.Windows.Forms.TextBox();
            this.txtNewPass = new System.Windows.Forms.TextBox();
            this.txtCopyNewPass = new System.Windows.Forms.TextBox();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.txtCopyNewPassword1 = new System.Windows.Forms.TextBox();
            this.txtOldPassword1 = new System.Windows.Forms.TextBox();
            this.txtNewPassword1 = new System.Windows.Forms.TextBox();
            this.btnChangePass1 = new System.Windows.Forms.Button();
            this.MyMainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(3, 158);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(57, 167);
            // 
            // lblSep
            // 
            this.lblSep.Location = new System.Drawing.Point(-2, 226);
            this.lblSep.Size = new System.Drawing.Size(288, 10);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.btnChangePass1);
            this.MyMainPanel.Controls.Add(this.txtNewPassword1);
            this.MyMainPanel.Controls.Add(this.txtOldPassword1);
            this.MyMainPanel.Controls.Add(this.lblOldPass);
            this.MyMainPanel.Controls.Add(this.lblNewPass);
            this.MyMainPanel.Controls.Add(this.lblCopyNewPass);
            this.MyMainPanel.Controls.Add(this.txtCopyNewPassword1);
            this.MyMainPanel.Size = new System.Drawing.Size(284, 136);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.txtCopyNewPassword1, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblCopyNewPass, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblNewPass, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblOldPass, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtOldPassword1, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtNewPassword1, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnChangePass1, 0);
            // 
            // lblOldPass
            // 
            this.lblOldPass.AutoSize = true;
            this.lblOldPass.Location = new System.Drawing.Point(223, 16);
            this.lblOldPass.Name = "lblOldPass";
            this.lblOldPass.Size = new System.Drawing.Size(49, 13);
            this.lblOldPass.TabIndex = 0;
            this.lblOldPass.Text = "رمز قبلی";
            // 
            // lblNewPass
            // 
            this.lblNewPass.AutoSize = true;
            this.lblNewPass.Location = new System.Drawing.Point(223, 44);
            this.lblNewPass.Name = "lblNewPass";
            this.lblNewPass.Size = new System.Drawing.Size(47, 13);
            this.lblNewPass.TabIndex = 1;
            this.lblNewPass.Text = "رمز جدید";
            // 
            // lblCopyNewPass
            // 
            this.lblCopyNewPass.AutoSize = true;
            this.lblCopyNewPass.Location = new System.Drawing.Point(201, 74);
            this.lblCopyNewPass.Name = "lblCopyNewPass";
            this.lblCopyNewPass.Size = new System.Drawing.Size(71, 13);
            this.lblCopyNewPass.TabIndex = 2;
            this.lblCopyNewPass.Text = "تکرار رمز جدید";
            // 
            // txtOldPass
            // 
            this.txtOldPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOldPass.Location = new System.Drawing.Point(72, 8);
            this.txtOldPass.Name = "txtOldPass";
            this.txtOldPass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtOldPass.Size = new System.Drawing.Size(131, 21);
            this.txtOldPass.TabIndex = 3;
            this.txtOldPass.UseSystemPasswordChar = true;
            // 
            // txtNewPass
            // 
            this.txtNewPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewPass.Location = new System.Drawing.Point(72, 42);
            this.txtNewPass.Name = "txtNewPass";
            this.txtNewPass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNewPass.Size = new System.Drawing.Size(131, 21);
            this.txtNewPass.TabIndex = 4;
            this.txtNewPass.UseSystemPasswordChar = true;
            // 
            // txtCopyNewPass
            // 
            this.txtCopyNewPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCopyNewPass.Location = new System.Drawing.Point(72, 74);
            this.txtCopyNewPass.Name = "txtCopyNewPass";
            this.txtCopyNewPass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCopyNewPass.Size = new System.Drawing.Size(91, 21);
            this.txtCopyNewPass.TabIndex = 5;
            this.txtCopyNewPass.UseSystemPasswordChar = true;
            // 
            // btnChangePass
            // 
            this.btnChangePass.Location = new System.Drawing.Point(1, 110);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(96, 23);
            this.btnChangePass.TabIndex = 6;
            this.btnChangePass.Text = "اعمال تغییرات";
            this.btnChangePass.UseVisualStyleBackColor = true;
            // 
            // txtCopyNewPassword1
            // 
            this.txtCopyNewPassword1.Location = new System.Drawing.Point(62, 69);
            this.txtCopyNewPassword1.Name = "txtCopyNewPassword1";
            this.txtCopyNewPassword1.Size = new System.Drawing.Size(133, 21);
            this.txtCopyNewPassword1.TabIndex = 3;
            this.txtCopyNewPassword1.UseSystemPasswordChar = true;
            // 
            // txtOldPassword1
            // 
            this.txtOldPassword1.Location = new System.Drawing.Point(62, 13);
            this.txtOldPassword1.Name = "txtOldPassword1";
            this.txtOldPassword1.Size = new System.Drawing.Size(133, 21);
            this.txtOldPassword1.TabIndex = 1;
            this.txtOldPassword1.UseSystemPasswordChar = true;
            // 
            // txtNewPassword1
            // 
            this.txtNewPassword1.Location = new System.Drawing.Point(62, 41);
            this.txtNewPassword1.Name = "txtNewPassword1";
            this.txtNewPassword1.Size = new System.Drawing.Size(133, 21);
            this.txtNewPassword1.TabIndex = 2;
            this.txtNewPassword1.UseSystemPasswordChar = true;
            // 
            // btnChangePass1
            // 
            this.btnChangePass1.Location = new System.Drawing.Point(8, 104);
            this.btnChangePass1.Name = "btnChangePass1";
            this.btnChangePass1.Size = new System.Drawing.Size(99, 23);
            this.btnChangePass1.TabIndex = 4;
            this.btnChangePass1.Text = "تغییر رمز عبور";
            this.btnChangePass1.UseVisualStyleBackColor = true;
            this.btnChangePass1.Click += new System.EventHandler(this.BtnChangePass1Click);
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 136);
            this.Controls.Add(this.btnChangePass);
            this.Controls.Add(this.txtCopyNewPass);
            this.Controls.Add(this.txtNewPass);
            this.Controls.Add(this.txtOldPass);
            this.DoubleBuffered = true;
            this.Name = "ChangePassword";
            this.Text = "تغییر رمز";
            this.Load += new System.EventHandler(this.ChangePassword_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.ChangePasswordHelpRequested);
            this.Controls.SetChildIndex(this.txtOldPass, 0);
            this.Controls.SetChildIndex(this.txtNewPass, 0);
            this.Controls.SetChildIndex(this.txtCopyNewPass, 0);
            this.Controls.SetChildIndex(this.btnChangePass, 0);
            this.Controls.SetChildIndex(this.MyMainPanel, 0);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOldPass;
        private System.Windows.Forms.Label lblNewPass;
        private System.Windows.Forms.Label lblCopyNewPass;
        private System.Windows.Forms.TextBox txtOldPass;
        private System.Windows.Forms.TextBox txtNewPass;
        private System.Windows.Forms.TextBox txtCopyNewPass;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.TextBox txtNewPassword1;
        private System.Windows.Forms.TextBox txtOldPassword1;
        private System.Windows.Forms.TextBox txtCopyNewPassword1;
        private System.Windows.Forms.Button btnChangePass1;
    }
}