﻿#region using

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Rendering;
using MohanirPouya.Classes;
using MohanirPouya.Forms.Help;
using MohanirPouya.Forms.Management.BillItemsSettings;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirPouya.Forms.Management.ManageParameters.Parameters;
using MohanirPouya.Forms.Management.ManageParameters.ParametersC;
using MohanirPouya.Forms.ManagementBill;
using MohanirPouya.Forms.Start.UsersAndGroups;
using MohanirVBClasses;
using MohanirPouya.NRI;
#endregion

namespace MohanirPouya.Forms.Start
{
    public partial class FrmMainForm : Form
    {

        private FrmAutomaticRun _frmAutomaticRun;

        #region Constructors

        public FrmMainForm()
        {
            InitializeComponent();
            ShowDialog();
        }

        #endregion

        #region Event Handlers


        #region Bill

        #region btnDailyBill_Click


        private void BtnDailyBillClick(object sender, EventArgs e)
        {
            new SelectedTime("روزانه", "روزانه", "D");
        }

        #endregion

        #region btnMonthlyBill_Click

        private void BtnMonthlyBillClick(object sender, EventArgs e)
        {
            new SelectedTime("ماهیانه", "ماهیانه", "M");
        }

        #endregion

        #region btnFinalBill_Click

        private void BtnFinalBillClick(object sender, EventArgs e)
        {
            new SelectedTime("قطعی", "قطعی", "C");
        }

        #endregion


        #endregion

        #region Managment



        #region Sellers

        #region btnSellers_Click

        private void BtnSellersClick(object sender, EventArgs e)
        {
            btnSellers.Expanded = true;
        }

        #endregion

        #region btnSellersParams_Click

        private void BtnSellersParamsClick(object sender, EventArgs e)
        {
            new frmParameters("Sellers", "Pa");
        }

        #endregion

        #region btnSellersParamsC_Click

        private void BtnSellersParamsCClick(object sender, EventArgs e)
        {
            new frmParametersC("Sellers", "Pc");
        }

        #endregion

        #region Param_Structure_Click

        private void ParamStructureSClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new ParamStructure("Sellers");
        }

        #endregion

        #region btnSellersBillItems_Click




        #region btnSellersUnitBillItemSet

        private void BtnSellersUnitBillItemSetClick(object sender, EventArgs e)
        {
            new BillItemSet("Sellers", "U");
        }

        #endregion


        #endregion



        #endregion

        #region Buyers

        #region btnBuyers_Click

        private void BtnBuyersClick(object sender, EventArgs e)
        {
            btnBuyers.Expanded = true;
        }

        #endregion

        #region btnBuyersParams_Click

        private void BtnBuyersParamsClick(object sender, EventArgs e)
        {
            new frmParameters("Buyers", "Pa");
        }

        #endregion

        #region btnBuyersParamsC_Click

        private void BtnBuyersParamsCClick(object sender, EventArgs e)
        {
            new frmParametersC("Buyers", "Pc");
        }

        #endregion



        #region Param_Structure_B_Click

        private void ParamStructureBClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new ParamStructure("Buyers");
        }

        #endregion

        #region btnBuyersBillItems_Click

        private void BtnBuyersBillItemClick(object sender, EventArgs e)
        {
            new BillItemSet("Buyers", "P");
        }

        #endregion



        #endregion

        #region Transfer

        private void BtnTransferClick(object sender, EventArgs e)
        {

            //btnTransfer.Enabled = true;
            //btnTransfer.Expanded = true;
            // btnTransfer.ShowSubItems = true;
            btnTransfer.Expanded = true;
        }

        #region btnTransferParams_Click

        private void BtnTransferParamsClick(object sender, EventArgs e)
        {
            new frmParameters("Transfer", "Pa");
        }

        #endregion

        #region btnTransfeParamsC_Click

        private void BtnTransferParamsCClick(object sender, EventArgs e)
        {
            new frmParametersC("Transfer", "Pc");
        }

        #endregion

        #region btnTransferBillItemD

        private void BtnTransferBillItemDClick(object sender, EventArgs e)
        {
            new BillItemSet("Transfer", "D");

        }

        #endregion

        #region Param_Structure_T_Click

        private void ParamStructureTClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new ParamStructure("Transfer");
        }

        #endregion





        #endregion

        #region Style

        #region buttonStyleCustom_SelectedColorChanged

        private void ButtonStyleCustomSelectedColorChanged(object sender, EventArgs e)
        {
            eOffice2007ColorScheme colorScheme = MainRibbonControl.Office2007ColorTable;
            RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, colorScheme,
                btnStyleCustom.SelectedColor);
            btnStyleCustom.Checked = true;
            btnStyleOffice2007Blue.Checked = false;
            btnStyleOffice2007Black.Checked = false;
            btnStyleOffice2007Silver.Checked = false;
        }

        #endregion

        #region btnStyleCustom_ColorPreview

        private void BtnStyleCustomColorPreview(object sender, ColorPreviewEventArgs e)
        {
            eOffice2007ColorScheme colorScheme = MainRibbonControl.Office2007ColorTable;
            RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, colorScheme,
                e.Color);
            btnStyleCustom.Checked = true;
            btnStyleOffice2007Blue.Checked = false;
            btnStyleOffice2007Black.Checked = false;
            btnStyleOffice2007Silver.Checked = false;
        }

        #endregion

        #region buttonStyleOffice2007Blue_Click

        private void ButtonStyleOffice2007BlueClick(object sender, EventArgs e)
        {
            MainRibbonControl.Office2007ColorTable = eOffice2007ColorScheme.Blue;
            btnStyleOffice2007Blue.Checked = true;
            btnStyleCustom.Checked = false;
        }

        #endregion

        #region buttonStyleOffice2007Black_Click

        private void ButtonStyleOffice2007BlackClick(object sender, EventArgs e)
        {
            MainRibbonControl.Office2007ColorTable = eOffice2007ColorScheme.Black;
            btnStyleOffice2007Black.Checked = true;
            btnStyleCustom.Checked = false;
        }

        #endregion

        #region buttonStyleOffice2007Silver_Click

        private void ButtonStyleOffice2007SilverClick(object sender, EventArgs e)
        {
            MainRibbonControl.Office2007ColorTable = eOffice2007ColorScheme.Silver;
            btnStyleOffice2007Silver.Checked = true;
            btnStyleCustom.Checked = false;
        }

        #endregion

        #endregion

        #endregion

        #region btnExit_Click

        private void BtnExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region Form Closing

        private void FrmMainFormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("مایل به خروج از سیستم هستید؟",
                "تایید خروج", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            AllowSleep();//1395/05/26
            FormClosing -= FrmMainFormClosing;
            Application.Exit();
        }

        #endregion

        #region Form Load

        private void FrmMainFormLoad(object sender, EventArgs e)
        {
           PreventSleepAndMonitorOff();//1395/05/26

            if (DbBizClass.CurrentUserType != 1)
            {
                RTabSetting.Visible = false;
            }


            lblCurentUser.Text = @"کاربر جاری :" + DbBizClass.CurrentUserName;
            lblUserGroup.Text = @"گروه کاربری :" + DbBizClass.CurrentUserDescription;

            if (DbBizClass.CurrentUserName == "اتوماتیک") //1395/05/06
            {
                _frmAutomaticRun = new FrmAutomaticRun();
                _frmAutomaticRun.Show(this);
            }

        }

        #endregion

        #region BtnBillNumSClick

        private void BtnBillNumSClick(object sender, EventArgs e)
        {
            new Management.FrmBillNumber("BillNum_S", "S").ShowDialog();
        }

        #endregion

        #region BtnBillNumBClick

        private void BtnBillNumBClick(object sender, EventArgs e)
        {
            new Management.FrmBillNumber("BillNum_B", "B").ShowDialog();
        }

        #endregion

        #region BtnBillNumTClick

        private void BtnBillNumTClick(object sender, EventArgs e)
        {
            new Management.FrmBillNumber("BillNum_T", "T").ShowDialog();
        }

        #endregion

        #region Help

        private void FrmMainFormHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"help.chm", HelpNavigator.Topic, "Main.htm");
        }

        #region btnAbout_Click

        private void BtnAboutClick(object sender, EventArgs e)
        {
            new frmAbout().ShowDialog();
        }

        #endregion



        #endregion



        private void BtnConnectionSettingClick(object sender, EventArgs e)
        {
            new ConnectionDefine.ConnectionDefine("D");
        }

        private void BtnUserClick(object sender, EventArgs e)
        {
            new UsersGroups();
        }

        private void BtnConnectionSelectClick(object sender, EventArgs e)
        {
            new ConnectionDefine.ConnectionDefine("S");
            btnSellers.Enabled = true;
            btnBuyers.Enabled = true;
            btnTransfer.Enabled = true;
            string[] n1 = DbBizClass.DbConnStr.Split(Convert.ToChar(";"));
            lblDataSource.Text = n1[0] + n1[1];

        }




        // Prevent computer from entering sleep or hibernate (monitor not affected)
        private void PreventSleep()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS | NativeMethods.ES_SYSTEM_REQUIRED);
        }

        // Prevent the system from turning off monitor
        private void PreventMonitorOff()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS | NativeMethods.ES_DISPLAY_REQUIRED);
        }

        // Prevent the system from entering sleep and turning off monitor
        private void PreventSleepAndMonitorOff()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS | NativeMethods.ES_SYSTEM_REQUIRED | NativeMethods.ES_DISPLAY_REQUIRED);
        }

        // Clear EXECUTION_STATE flags to allow the system to sleep and turn off monitor normally
        private void AllowSleep()
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.ES_CONTINUOUS);
        }

    }

    #endregion

    internal static class NativeMethods
    {
        // Import SetThreadExecutionState Win32 API and necessary flags
        [DllImport("kernel32.dll")]
        public static extern uint SetThreadExecutionState(uint esFlags);
        public const uint ES_CONTINUOUS = 0x80000000;
        public const uint ES_SYSTEM_REQUIRED = 0x00000001;
        public const uint ES_DISPLAY_REQUIRED = 0x00000002;
    }
}







