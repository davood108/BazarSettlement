﻿using System;
using System.Linq;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using System.Windows.Forms;
using MohanirVBClasses;
using RPNCalendar.Utilities;

namespace MohanirPouya.Forms.Start.ConnectionDefine
{
    public partial class  ConnectionDefineDisp: MohanirFormDialog
    {
        private readonly PreDbmsDataContext _pdbSM;
        private readonly int _frmMode;
        private IQueryable<Tbl_ConnectionInfo> _query2;
        private string _strFromDate;
        private string _strToDate;
        private string _strBillTypeCode;
        private string _strMonth;
        private string _strServerName;
        private string _strInstanceName;
        private string _strDataBaseName;
        private string _strBillTypeName;
        private int _iD;


        public ConnectionDefineDisp()
        {
            InitializeComponent();
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
          
          _frmMode = 1;
            #region SetTime
            PersianDate occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            StartDate.SelectedDateTime = DateTime.Now;
   
            StartDate.Text = occuredDate.ToString();

            EndDate.SelectedDateTime = DateTime.Now;

            EndDate.Text = occuredDate.ToString();
            #endregion


            ShowDialog();
        }

        public ConnectionDefineDisp(int  _iDp ,string strFromDate, string strTodate, string strBillTypecode,string strBillTypeName, string strMonth, string strServerName, string strInstanceName, string strDataBaseName)
        {
            InitializeComponent();
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
            _iD = _iDp;
            _frmMode = 2;
            _strFromDate = strFromDate;
            _strToDate = strTodate;
            _strBillTypeCode = strBillTypecode;
            _strBillTypeName = strBillTypeName;
            _strMonth = strMonth;
            _strServerName = strServerName;
            _strInstanceName = strInstanceName;
            _strDataBaseName = strDataBaseName;
            ShowDialog();
        }


        private void DefineUserLoad(object sender, EventArgs e)
        {
            if (_frmMode==2)
            {
                if (_strBillTypeCode=="D")
                {
                    cmbBillType.SelectedIndex = 0;
                    cmbBillType.Enabled = false;

                    cmbMonth.SelectedIndex = 0;
                    cmbMonth.Enabled = false;
                    EndDate.Enabled = false;
                    StartDate.Text = _strFromDate;
                    StartDate.Enabled = false;
                    if(_strToDate=="Null")
                    {
                        EndDate.Visible = false;
                        lblEndDate.Visible = false;
                    }
               
                }
                if (_strBillTypeCode == "M")
                {
                    cmbBillType.SelectedIndex = 1;
                    cmbBillType.Enabled = false;
                    cmbMonth.SelectedIndex = 0;
                    cmbMonth.Enabled = false;
                    EndDate.Enabled = false;
                    StartDate.Text = _strFromDate;
                    StartDate.Enabled = false;

                    if (_strToDate == "Null")
                    {
                        EndDate.Visible = false;
                        lblEndDate.Visible = false;
                    }
                }
                if (_strBillTypeCode == "C")
                {
                    cmbBillType.SelectedIndex = 2;
                    cmbBillType.Enabled = false;
                }
                txtServerName.Text= _strServerName;
                txtInstanceName.Text  = _strInstanceName;
                txtDataBaseName.Text = _strDataBaseName;
            }
        }

        private void ApplyClick(object sender, EventArgs e)
        {
      
            if(_frmMode==1)
            {
                #region Check First Day of Month choose
                if ((StartDate.Text.Substring(8, 2)) != "01")
                {
                    PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
                               MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                #endregion

                #region check Server info
                if (txtServerName.Text == string.Empty || txtInstanceName.Text == string.Empty || txtDataBaseName.Text == string.Empty)
                {
                    PersianMessageBox.Show("یکی از گزینه های مشخصات سرور فاقد داده می باشد", "خطا",
                               MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                #endregion

                #region insert
                if (cmbBillType.SelectedIndex == 0)
                {
                    _pdbSM.SP_InsertConnectionInfo(StartDate.Text, EndDate.Text, "D", "روزانه", "همه", txtServerName.Text, txtInstanceName.Text, txtDataBaseName.Text);
                }

                if (cmbBillType.SelectedIndex == 1)
                {
                    _pdbSM.SP_InsertConnectionInfo(StartDate.Text, EndDate.Text, "M", "ماهیانه", "همه", txtServerName.Text, txtInstanceName.Text, txtDataBaseName.Text);
                }
                if (cmbBillType.SelectedIndex == 2)
                {
                    _query2 = (from varx in _pdbSM.Tbl_ConnectionInfos
                               where Equals(varx.BillTypeCode, 'C') && varx.FromDate == StartDate.Text
                               select varx);
                    if (_query2.Count() > 0)
                    {
                        PersianMessageBox.Show("تاریخ آغاز اعتبار تکراری می باشد!", "خطا",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    else
                    {
                        _pdbSM.SP_InsertConnectionInfo(StartDate.Text, EndDate.Text, "C", "قطعی", cmbMonth.SelectedItem.ToString(), txtServerName.Text, txtInstanceName.Text, txtDataBaseName.Text);
                    }


                }
                #endregion
            }
            if (_frmMode==2)
            {
                #region _frmMode
                var connectionDefine = new Tbl_ConnectionInfo
                                           {
                                               FromDate = _strFromDate,
                                               ToDate = _strToDate,
                                               BillTypeCode = _strBillTypeCode,
                                               BillTypeName = _strBillTypeName,
                                               Month = _strMonth,
                                               ServerName = txtServerName.Text,
                                               InstanceName = txtInstanceName.Text,
                                               DataBaseName = txtDataBaseName.Text
                                           };
                var query2 = (from var in _pdbSM.Tbl_ConnectionInfos
                              where var.ID==_iD
                                  select var);
                if (query2.Any())
                {
                    foreach (var var in query2)
                    {
                        var.FromDate = connectionDefine.FromDate;
                        var.ToDate = connectionDefine.ToDate;
                        var.BillTypeCode = connectionDefine.BillTypeCode;
                        var.BillTypeName = connectionDefine.BillTypeName;
                        var.Month = connectionDefine.Month;
                        var.ServerName = connectionDefine.ServerName;
                        var.InstanceName = connectionDefine.InstanceName;
                        var.DataBaseName = connectionDefine.DataBaseName;
                    }
                    _pdbSM.SubmitChanges();
                }
                #endregion
            }
             
            Close();


    }

        #region Help
        private void DefineUserHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"help.chm", HelpNavigator.Topic, "ConnectionDefineDisp.htm");
        }
        #endregion

        private void CmbBillTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbBillType.SelectedIndex!=2)
                    {
                     EndDate.Enabled = false;
                    cmbMonth.Enabled = false;
                    }
            if (cmbBillType.SelectedIndex ==2)
            {
                EndDate.Enabled =true;
                cmbMonth.Enabled = true;
            }
        }
    }
}

