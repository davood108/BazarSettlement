﻿namespace MohanirPouya.Forms.Start.ConnectionDefine
{
    partial class ConnectionDefineDisp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPasswprd = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.Apply = new DevComponents.DotNetBar.ButtonX();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.txtInstanceName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDataBaseName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbBillType = new System.Windows.Forms.ComboBox();
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.StartDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.EndDate = new RPNCalendar.UI.Controls.PersianDatePicker();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.MyMainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(12, 0);
            this.lblSubtitle.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(161, 23);
            this.lblTitle.Size = new System.Drawing.Size(118, 25);
            this.lblTitle.Tag = "";
            this.lblTitle.Text = "تعریف اتصال";
            // 
            // lblSep
            // 
            this.lblSep.Size = new System.Drawing.Size(464, 2);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.EndDate);
            this.MyMainPanel.Controls.Add(this.lblEndDate);
            this.MyMainPanel.Controls.Add(this.StartDate);
            this.MyMainPanel.Controls.Add(this.cmbMonth);
            this.MyMainPanel.Controls.Add(this.cmbBillType);
            this.MyMainPanel.Controls.Add(this.txtDataBaseName);
            this.MyMainPanel.Controls.Add(this.label2);
            this.MyMainPanel.Controls.Add(this.txtInstanceName);
            this.MyMainPanel.Controls.Add(this.label1);
            this.MyMainPanel.Controls.Add(this.txtServerName);
            this.MyMainPanel.Controls.Add(this.Apply);
            this.MyMainPanel.Controls.Add(this.lblType);
            this.MyMainPanel.Controls.Add(this.lblName);
            this.MyMainPanel.Controls.Add(this.lblUserName);
            this.MyMainPanel.Controls.Add(this.lblPasswprd);
            this.MyMainPanel.Size = new System.Drawing.Size(460, 307);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.lblPasswprd, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblUserName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblType, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.Apply, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtServerName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.label1, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtInstanceName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.label2, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtDataBaseName, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.cmbBillType, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.cmbMonth, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.StartDate, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblEndDate, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.EndDate, 0);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(367, 122);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(66, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = ": تاریخ شروع";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(364, 86);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(84, 13);
            this.lblUserName.TabIndex = 4;
            this.lblUserName.Text = ": نوع صورتحساب";
            // 
            // lblPasswprd
            // 
            this.lblPasswprd.AutoSize = true;
            this.lblPasswprd.Location = new System.Drawing.Point(153, 86);
            this.lblPasswprd.Name = "lblPasswprd";
            this.lblPasswprd.Size = new System.Drawing.Size(56, 13);
            this.lblPasswprd.TabIndex = 5;
            this.lblPasswprd.Text = ": ماه اتصال";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(367, 163);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(55, 13);
            this.lblType.TabIndex = 10;
            this.lblType.Text = ": نام سرور";
            // 
            // Apply
            // 
            this.Apply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.Apply.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.Apply.Location = new System.Drawing.Point(11, 275);
            this.Apply.Name = "Apply";
            this.Apply.Size = new System.Drawing.Size(89, 23);
            this.Apply.TabIndex = 14;
            this.Apply.Text = "ذخیره";
            this.Apply.Click += new System.EventHandler(this.ApplyClick);
            // 
            // txtServerName
            // 
            this.txtServerName.Location = new System.Drawing.Point(227, 160);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtServerName.Size = new System.Drawing.Size(122, 21);
            this.txtServerName.TabIndex = 15;
            // 
            // txtInstanceName
            // 
            this.txtInstanceName.Location = new System.Drawing.Point(16, 160);
            this.txtInstanceName.Name = "txtInstanceName";
            this.txtInstanceName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtInstanceName.Size = new System.Drawing.Size(122, 21);
            this.txtInstanceName.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(154, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = ": نام نمونه";
            // 
            // txtDataBaseName
            // 
            this.txtDataBaseName.Location = new System.Drawing.Point(227, 199);
            this.txtDataBaseName.Name = "txtDataBaseName";
            this.txtDataBaseName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDataBaseName.Size = new System.Drawing.Size(122, 21);
            this.txtDataBaseName.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(365, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = ": نام پایگاه داده";
            // 
            // cmbBillType
            // 
            this.cmbBillType.FormattingEnabled = true;
            this.cmbBillType.Items.AddRange(new object[] {
            "روزانه",
            "ماهیانه",
            "قطعی"});
            this.cmbBillType.Location = new System.Drawing.Point(229, 83);
            this.cmbBillType.Name = "cmbBillType";
            this.cmbBillType.Size = new System.Drawing.Size(121, 21);
            this.cmbBillType.TabIndex = 20;
            this.cmbBillType.SelectedIndexChanged += new System.EventHandler(this.CmbBillTypeSelectedIndexChanged);
            // 
            // cmbMonth
            // 
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Items.AddRange(new object[] {
            "همه",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cmbMonth.Location = new System.Drawing.Point(18, 82);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(121, 21);
            this.cmbMonth.TabIndex = 21;
            // 
            // StartDate
            // 
            this.StartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StartDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.StartDate.IsNull = false;
            this.StartDate.Location = new System.Drawing.Point(252, 117);
            this.StartDate.Name = "StartDate";
            this.StartDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.StartDate.Size = new System.Drawing.Size(99, 20);
            this.StartDate.TabIndex = 64;
            this.StartDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // EndDate
            // 
            this.EndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EndDate.ControlCulture = RPNCalendar.UI.CultureName.Persian;
            this.EndDate.IsNull = false;
            this.EndDate.Location = new System.Drawing.Point(39, 117);
            this.EndDate.Name = "EndDate";
            this.EndDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.EndDate.SelectedDateTime = new System.DateTime(2009, 10, 12, 0, 0, 0, 0);
            this.EndDate.Size = new System.Drawing.Size(99, 20);
            this.EndDate.TabIndex = 66;
            this.EndDate.Theme = RPNCalendar.UI.ThemeTypes.Office2003;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(154, 122);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(57, 13);
            this.lblEndDate.TabIndex = 65;
            this.lblEndDate.Text = ": تاریخ پایان";
            // 
            // ConnectionDefineDisp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 307);
            this.FormTitle = "تعریف اتصال";
            this.Name = "ConnectionDefineDisp";
            this.Text = "تعریف اتصال";
            this.Load += new System.EventHandler(this.DefineUserLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.DefineUserHelpRequested);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPasswprd;
        private DevComponents.DotNetBar.ButtonX Apply;
        private System.Windows.Forms.ComboBox cmbMonth;
        private System.Windows.Forms.ComboBox cmbBillType;
        private System.Windows.Forms.TextBox txtDataBaseName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInstanceName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServerName;
        public RPNCalendar.UI.Controls.PersianDatePicker StartDate;
        public RPNCalendar.UI.Controls.PersianDatePicker EndDate;
        private System.Windows.Forms.Label lblEndDate;
    }
}