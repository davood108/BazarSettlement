﻿using System;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;

namespace MohanirPouya.Forms.Start.ConnectionDefine
{
    public partial class ConnectionDefine : MohanirFormDialog
    {


        private  PreDbmsDataContext _pdbSM;
        private IQueryable<Tbl_ConnectionInfo> _sourceTbl;
        private string _type;

        #region DbConnSelected Property



        public static String DbConnSelected { get; set; }

        #endregion

        #region Ctor

        public ConnectionDefine(string typeF)
        {
            InitializeComponent();
            _type = typeF;
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);

            dgvConnection.AutoGenerateColumns = false;

            if(_type=="D")
            {
                btnDelete.Visible =true;
                btnEdit.Visible = true;
                btnAddConnection.Visible =true;
                btnSelection.Visible = false;
            }
            else
            {
                btnDelete.Visible = false;
                btnEdit.Visible = false;
                btnAddConnection.Visible = false;
                btnSelection.Visible = true;
            }
                ShowDialog();
        }

        #endregion

        #region Help

        private void ChangePasswordHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"help.chm", HelpNavigator.Topic, "ConnectionDefine.htm");
        }

        #endregion

        #region Load

        private void ConnectionDefineLoad(object sender, EventArgs e)
        {
            cmbBillType.SelectedIndex = 0;
            FilldgvDataSource();
        }

        #endregion

        #region FilldgvDataSource

        private void FilldgvDataSource()
        {

            _sourceTbl = _pdbSM.Tbl_ConnectionInfos;

            dgvConnection.DataSource = _sourceTbl;

        }

        #endregion

        #region CmbBillTypeSelectedIndexChanged

        private void CmbBillTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbBillType.SelectedIndex == 0)
            {
                _sourceTbl = _pdbSM.Tbl_ConnectionInfos;

                dgvConnection.DataSource = _sourceTbl;
            }
            if (cmbBillType.SelectedIndex == 1)
            {
                _sourceTbl = (from var in _pdbSM.Tbl_ConnectionInfos
                              where var.BillTypeCode == "D"
                              select var);

                dgvConnection.DataSource = _sourceTbl;
            }
            if (cmbBillType.SelectedIndex == 2)
            {
                _sourceTbl = (from var in _pdbSM.Tbl_ConnectionInfos
                              where var.BillTypeCode == "M"
                              select var);

                dgvConnection.DataSource = _sourceTbl;
            }
            if (cmbBillType.SelectedIndex == 3)
            {
                _sourceTbl = (from var in _pdbSM.Tbl_ConnectionInfos
                              where var.BillTypeCode == "C"
                              select var);

                dgvConnection.DataSource = _sourceTbl;
            }
            
            if (cmbBillType.SelectedIndex == 4)
            {
                _sourceTbl = (from var in _pdbSM.Tbl_ConnectionInfos
                              where var.BillTypeCode == "R"
                              select var);

                dgvConnection.DataSource = _sourceTbl;
            }
        }

        #endregion

        #region Close

        private void BtnCloseClick(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region BtnAddConnectionClick
        private void BtnAddConnectionClick(object sender, EventArgs e)
        {
            new ConnectionDefineDisp();
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);

            dgvConnection.DataSource = _pdbSM.Tbl_ConnectionInfos;
            dgvConnection.Refresh();
        }
        #endregion

        #region BtnEditClick
        private void BtnEditClick(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvConnection.SelectedRows)
            {
                int id = Convert.ToInt32(row.Cells[0].Value.ToString());
                string strFromDate = row.Cells[1].Value.ToString();
                string strTodate = row.Cells[2].Value.ToString();
                string strBillTypecode = row.Cells[3].Value.ToString();
                string strBillTypeName = row.Cells[4].Value.ToString();
                string strMonth = row.Cells[5].Value.ToString();
                string strServerName = row.Cells[6].Value.ToString();
                string strInstanceName = row.Cells[7].Value.ToString();
                string strDataBaseName = row.Cells[8].Value.ToString();
                new ConnectionDefineDisp(id,strFromDate, strTodate, strBillTypecode,strBillTypeName, strMonth, strServerName,strInstanceName, strDataBaseName);


                _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);

                dgvConnection.DataSource = _pdbSM.Tbl_ConnectionInfos;
                dgvConnection.Refresh();
            }
        }
        #endregion

        #region BtnDeleteClick
        private void BtnDeleteClick(object sender, EventArgs e)
        {
            switch (
         MessageBox.Show(@"آیا مطمئن هستید که می خواهید حذف کنید؟", @"پرسش جهت حذف", MessageBoxButtons.YesNo,
             MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
            {
                case DialogResult.Yes:

                    foreach (DataGridViewRow row in dgvConnection.SelectedRows)
                    {
                        int id = Convert.ToInt32(row.Cells[0].Value.ToString());

                      var deleteConnInfo =from conn in _pdbSM.Tbl_ConnectionInfos
                        where conn.ID==id
                        select conn;

                      foreach (var x in deleteConnInfo)
                      {
                          _pdbSM.Tbl_ConnectionInfos.DeleteOnSubmit(x);
                      }

                      try
                      {
                          _pdbSM.SubmitChanges();
                      }
                      catch (Exception )
                      {
                     
                      }
               
                    }

                    _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
                       dgvConnection.DataSource = _pdbSM.Tbl_ConnectionInfos;
                dgvConnection.Refresh();



                    break;
                case DialogResult.No:
                    break;
            }

        }
        #endregion

        #region BtnSelectionClick
        private void BtnSelectionClick(object sender, EventArgs e)
        {
            var connectionStringSelected = "Server = " + dgvConnection.SelectedRows[0].Cells[6].Value + @"\" + dgvConnection.SelectedRows[0].Cells[7].Value + "; Database = " + dgvConnection.SelectedRows[0].Cells[8].Value + "; User ID=Bill_Operator ; Password=vas1383#;";

            DbBizClass.DbConnStr = connectionStringSelected;
            Close();

        }
        #endregion

    }
}
            
