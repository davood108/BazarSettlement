﻿namespace MohanirPouya.Forms.Start.ConnectionDefine
{
    partial class ConnectionDefine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtOldPass = new System.Windows.Forms.TextBox();
            this.txtNewPass = new System.Windows.Forms.TextBox();
            this.txtCopyNewPass = new System.Windows.Forms.TextBox();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.cmbBillType = new System.Windows.Forms.ComboBox();
            this.btnEdit = new DevComponents.DotNetBar.ButtonX();
            this.btnDelete = new DevComponents.DotNetBar.ButtonX();
            this.dgvConnection = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.توضیحات = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClose = new DevComponents.DotNetBar.ButtonX();
            this.btnAddConnection = new DevComponents.DotNetBar.ButtonX();
            this.btnSelection = new DevComponents.DotNetBar.ButtonX();
            this.MyMainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConnection)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(516, 500);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(570, 500);
            // 
            // lblSep
            // 
            this.lblSep.Location = new System.Drawing.Point(-50, 500);
            this.lblSep.Size = new System.Drawing.Size(801, 10);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.btnSelection);
            this.MyMainPanel.Controls.Add(this.btnAddConnection);
            this.MyMainPanel.Controls.Add(this.btnEdit);
            this.MyMainPanel.Controls.Add(this.btnDelete);
            this.MyMainPanel.Controls.Add(this.dgvConnection);
            this.MyMainPanel.Controls.Add(this.btnClose);
            this.MyMainPanel.Controls.Add(this.cmbBillType);
            this.MyMainPanel.Size = new System.Drawing.Size(797, 397);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.cmbBillType, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnClose, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.dgvConnection, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnDelete, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnEdit, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnAddConnection, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnSelection, 0);
            // 
            // txtOldPass
            // 
            this.txtOldPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOldPass.Location = new System.Drawing.Point(72, 8);
            this.txtOldPass.Name = "txtOldPass";
            this.txtOldPass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtOldPass.Size = new System.Drawing.Size(131, 21);
            this.txtOldPass.TabIndex = 3;
            this.txtOldPass.UseSystemPasswordChar = true;
            // 
            // txtNewPass
            // 
            this.txtNewPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewPass.Location = new System.Drawing.Point(72, 42);
            this.txtNewPass.Name = "txtNewPass";
            this.txtNewPass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNewPass.Size = new System.Drawing.Size(131, 21);
            this.txtNewPass.TabIndex = 4;
            this.txtNewPass.UseSystemPasswordChar = true;
            // 
            // txtCopyNewPass
            // 
            this.txtCopyNewPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCopyNewPass.Location = new System.Drawing.Point(72, 74);
            this.txtCopyNewPass.Name = "txtCopyNewPass";
            this.txtCopyNewPass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCopyNewPass.Size = new System.Drawing.Size(91, 21);
            this.txtCopyNewPass.TabIndex = 5;
            this.txtCopyNewPass.UseSystemPasswordChar = true;
            // 
            // btnChangePass
            // 
            this.btnChangePass.Location = new System.Drawing.Point(1, 110);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(96, 23);
            this.btnChangePass.TabIndex = 6;
            this.btnChangePass.Text = "اعمال تغییرات";
            this.btnChangePass.UseVisualStyleBackColor = true;
            // 
            // cmbBillType
            // 
            this.cmbBillType.FormattingEnabled = true;
            this.cmbBillType.Items.AddRange(new object[] {
            "همه",
            "روزانه",
            "ماهیانه",
            "قطعی",
            "گزارش سالانه"});
            this.cmbBillType.Location = new System.Drawing.Point(643, 31);
            this.cmbBillType.Name = "cmbBillType";
            this.cmbBillType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbBillType.Size = new System.Drawing.Size(121, 21);
            this.cmbBillType.TabIndex = 2;
            this.cmbBillType.SelectedIndexChanged += new System.EventHandler(this.CmbBillTypeSelectedIndexChanged);
            // 
            // btnEdit
            // 
            this.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnEdit.Location = new System.Drawing.Point(608, 365);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(89, 23);
            this.btnEdit.TabIndex = 18;
            this.btnEdit.Text = "ویرایش";
            this.btnEdit.Click += new System.EventHandler(this.BtnEditClick);
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDelete.Location = new System.Drawing.Point(513, 365);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(89, 23);
            this.btnDelete.TabIndex = 17;
            this.btnDelete.Text = "حذف";
            this.btnDelete.Click += new System.EventHandler(this.BtnDeleteClick);
            // 
            // dgvConnection
            // 
            this.dgvConnection.AllowUserToAddRows = false;
            this.dgvConnection.AllowUserToOrderColumns = true;
            this.dgvConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvConnection.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.dgvConnection.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvConnection.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvConnection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConnection.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column3,
            this.Column4,
            this.توضیحات,
            this.Column2,
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvConnection.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvConnection.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvConnection.Location = new System.Drawing.Point(7, 74);
            this.dgvConnection.MultiSelect = false;
            this.dgvConnection.Name = "dgvConnection";
            this.dgvConnection.ReadOnly = true;
            this.dgvConnection.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvConnection.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvConnection.RowTemplate.Height = 24;
            this.dgvConnection.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConnection.Size = new System.Drawing.Size(783, 281);
            this.dgvConnection.TabIndex = 16;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "ID";
            this.Column8.HeaderText = "ردیف";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "FromDate";
            this.Column5.HeaderText = "تاریخ شروع";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "ToDate";
            this.Column6.HeaderText = "تاریخ پایان";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "BillTypeCode";
            this.Column7.HeaderText = "کد نوع صورتحساب";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "BillTypeName";
            this.Column3.HeaderText = "نوع صورتحساب";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Month";
            this.Column4.HeaderText = "ماه اجرا";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // توضیحات
            // 
            this.توضیحات.DataPropertyName = "ServerName";
            this.توضیحات.HeaderText = "نام سرور";
            this.توضیحات.Name = "توضیحات";
            this.توضیحات.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "InstanceName";
            this.Column2.HeaderText = "نام نمونه";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 110;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "DatabaseName";
            this.Column1.HeaderText = "نام پایگاه داده";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.Width = 125;
            // 
            // btnClose
            // 
            this.btnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnClose.Location = new System.Drawing.Point(8, 365);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(89, 23);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "بستن";
            this.btnClose.Click += new System.EventHandler(this.BtnCloseClick);
            // 
            // btnAddConnection
            // 
            this.btnAddConnection.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddConnection.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddConnection.Location = new System.Drawing.Point(701, 365);
            this.btnAddConnection.Name = "btnAddConnection";
            this.btnAddConnection.Size = new System.Drawing.Size(89, 23);
            this.btnAddConnection.TabIndex = 19;
            this.btnAddConnection.Text = "افزودن";
            this.btnAddConnection.Click += new System.EventHandler(this.BtnAddConnectionClick);
            // 
            // btnSelection
            // 
            this.btnSelection.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSelection.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSelection.Location = new System.Drawing.Point(103, 365);
            this.btnSelection.Name = "btnSelection";
            this.btnSelection.Size = new System.Drawing.Size(89, 23);
            this.btnSelection.TabIndex = 20;
            this.btnSelection.Text = "انتخاب";
            this.btnSelection.Click += new System.EventHandler(this.BtnSelectionClick);
            // 
            // ConnectionDefine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 397);
            this.Controls.Add(this.btnChangePass);
            this.Controls.Add(this.txtCopyNewPass);
            this.Controls.Add(this.txtNewPass);
            this.Controls.Add(this.txtOldPass);
            this.DoubleBuffered = true;
            this.Name = "ConnectionDefine";
            this.Text = "تنظیم اتصالات پایگاههای داده";
            this.Load += new System.EventHandler(this.ConnectionDefineLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.ChangePasswordHelpRequested);
            this.Controls.SetChildIndex(this.txtOldPass, 0);
            this.Controls.SetChildIndex(this.txtNewPass, 0);
            this.Controls.SetChildIndex(this.txtCopyNewPass, 0);
            this.Controls.SetChildIndex(this.btnChangePass, 0);
            this.Controls.SetChildIndex(this.MyMainPanel, 0);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConnection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOldPass;
        private System.Windows.Forms.TextBox txtNewPass;
        private System.Windows.Forms.TextBox txtCopyNewPass;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.ComboBox cmbBillType;
        private DevComponents.DotNetBar.ButtonX btnAddConnection;
        private DevComponents.DotNetBar.ButtonX btnEdit;
        private DevComponents.DotNetBar.ButtonX btnDelete;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvConnection;
        private DevComponents.DotNetBar.ButtonX btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn توضیحات;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private DevComponents.DotNetBar.ButtonX btnSelection;
    }
}