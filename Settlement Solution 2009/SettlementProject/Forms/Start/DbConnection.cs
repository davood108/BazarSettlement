﻿#region using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Windows.Forms;
using MohanirPouya.DbLayer;
using MohanirVBClasses;
using User=MohanirPouya.DbLayer.Tbl_User;
using MohanirPouya.Classes;

#endregion

namespace MohanirPouya.Forms.Start
{
    /// <summary>
    /// كلاس فرم مدیریت ارتباط با بانك اطلاعات
    /// </summary>
    public partial class DbConnection : Form
    {
        private PreDbmsDataContext _pdbSM;
        private string _windowsUserName="";

        #region Ctors

        public DbConnection()
        {
            InitializeComponent();

            ShowDialog();

        }

        #endregion

        #region Event Handlers

        #region Form Load

        private void DbConnectionLoad(object sender, EventArgs e)
        {
            
            if (CheckDataFileExist())
            {
                FormTimer.Enabled = true;

            }
            else
            {
                new Help.FrmDetailMessageBox("فایل DbConnection.DAT در شاخه مورد نظر موجود نمی باشد", "", "Error").
                    ShowDialog();
                Application.Exit();
            }

            DbBizClass.DbConnStrPre = LoadConnectionString();
            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);


            # region UserName

            var windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (windowsIdentity != null)
            {
                string userName = windowsIdentity.Name;
                string strUserName = userName.Substring(userName.LastIndexOf("\\") + 1);
                _windowsUserName = strUserName;//1394/11/12
                txtUsername.Text = strUserName;
            }

            #endregion

            chkMakeNewConnection.Checked = true;
          }

        #endregion

        #region Exit Button Clicked

        private void BtnExitClick(object sender, CancelEventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region Form Closing

        private void DbConnectionSetFormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("مایل به خروج از سیستم هستید؟",
                                       "تایید خروج", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            FormClosing -= DbConnectionSetFormClosing;
            Application.Exit();
        }

        #endregion

        #region Accept Password Button Clicked

        private void BtnAcceptPassword(object sender, CancelEventArgs e)
        {

            if (!CheckUserAndPass())
            {
                e.Cancel = true;
                return;
            }

            #region GetID

            var connection = new SqlConnection(DbBizClass.DbConnStrPre);
            connection.Open();
            var com =
                new SqlCommand("Select type from [Security].[Tbl_Users] where UserName='" + txtUsername.Text + "' ",
                               connection);
            int intType = Convert.ToInt32(com.ExecuteScalar());

            #endregion

            if (intType != 3)
            {
                #region InsertInformation

                var connection1 = new SqlConnection(DbBizClass.DbConnStrPre);
                string strDateTime = DateTime.Now.ToString();
                string strConnection = "INSERT INTO [Security].[Tbl_LoginLog] ([UserName],[WindowsUserName],[DateTime]) VALUES ('" +
                                       txtUsername.Text + "' ,'" +_windowsUserName+"' ,'"+ strDateTime + "') ";//1394/11/12
                connection1.Open();
                var dt = new DataTable();
                var adapter = new SqlDataAdapter(strConnection, connection1);
                new SqlCommandBuilder(adapter);
                adapter.Fill(dt);
                connection1.Close();

                #endregion
            }

            Dispose();
            GC.Collect();
            new FrmMainForm();
        }

        private bool CheckUserAndPass()
        {
            try
            {

                List<User> myData = _pdbSM.Tbl_Users.Where(
                    data => data.UserName == txtUsername.Text.Trim() &&
                            data.Password == txtPassword.Text
                            && data.IsActive).ToList();

                if (myData.Count == 0)
                {
                    new Help.FrmDetailMessageBox("نام کاربری و یا رمز عبور صحیح نمی باشد", "", "Error").ShowDialog();
                    return false;
                }

                DbBizClass.CurrentUserID = myData.First().ID;
                DbBizClass.CurrentUserName = myData.First().LastName;
                DbBizClass.CurrentUserLastName = myData.First().LastName;
                DbBizClass.CurrentUserType = myData.First().Type;
                DbBizClass.CurrentUserDescription = myData.First().Description;
                return true;
            }

            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("یکی از مشخصات پایگاه داده نادرست می باشد", strMessage, "Error").ShowDialog
                    ();
                return false;

            }
        }

        #endregion

        #region Connection Details Button Clicked

        private void BtnConnectionDetailsClick(object sender, EventArgs e)
        {
            try
            {
                var newSqlConnection = new SqlConnection(LoadConnectionString());
                MessageBox.Show(
                    @"Data Source = " + newSqlConnection.DataSource + @"
" +
                    @"Database = " + newSqlConnection.Database,
                    @"متن رشته ارتباط با بانك اطلاعاتی", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("فایل مربوط به جزئیات اتصال به سرور یافت نشد", strMessage, "Error").
                    ShowDialog();
            }
        }

        #endregion

        #region Next Button Clicked

        private void BtnNextClick(object sender, CancelEventArgs e)
        {
            //وضعیت صفحه اول
            if (MyWizard.SelectedPage == PSelectConnection)
            {
                if (chkMakeNewConnection.Checked)
                {
                    return;
                }

                MyWizard.SelectedPage = PCheckPassword;
                txtUsername.Focus();
                return;
            }
            SaveConnectionString("Server = " + txtServerName.Text + @"\" + txtInstance.Text +
                                 "; Database = " + txtDatabase.Text +
                                 "; User ID=Bill_Operator ; Password=vas1383#;");
            txtUsername.Focus();
            return;
        }

        #endregion

        #region Back Button Clicked

        private void BtnBackClick(object sender, CancelEventArgs e)
        {
            // وضعیت صفحه دوم
            if (MyWizard.SelectedPage == PSetConnectionString)
            {
                return;
            }
            // وضعیت صفحه سوم
            if (MyWizard.SelectedPage == PCheckPassword)
            {
                if (chkMakeNewConnection.Checked)
                {
                    return;
                }
                if (chkMakeNewConnection.Checked == false)
                {
                    MyWizard.SelectedPage = PSelectConnection;
                    return;
                }
            }
            return;
        }

        #endregion

        #region Form TimeTicker Tick

        private void FormTimerTick(object sender, EventArgs e)
        {
            FormTimer.Enabled = false;
            FormTimer.Tick -= FormTimerTick;
            MyWizard.SelectedPage = PCheckPassword;
        }

        #endregion

        #endregion

        #region Methods

        #region Save Connection String Method

        /// <summary>
        /// ذخیره رشته ارتباط با بانك اطلاعاتی
        /// </summary>
        /// <param name="connectionString">مقدار رشته ی ارتباط با بانك اطلاعاتی</param>
        public void SaveConnectionString(String connectionString)
        {
            var myDataTable = new DataTable();
            myDataTable.Columns.Add("Field");
            myDataTable.Columns.Add("Value");
            myDataTable.Rows.Add("Connection String", HashMethod.Encrypt(connectionString, "asdfewrewqrss323"));
            var myDataSet = new DataSet("MyDataSet");
            myDataSet.Tables.Add(myDataTable);
            try
            {
                myDataSet.WriteXml("DbConnection.DAT");
            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("یکی از اطلاعات ورودی نادرست می باشد", strMessage, "Error").ShowDialog();

            }
        }

        #endregion

        #region Load Connection String Method

        /// <summary>
        /// خواندن رشته ارتباط با بانك اطلاعاتی=
        /// </summary>
        public String LoadConnectionString()
        {
            var myDataSet = new DataSet("MyDataSet");
            try
            {
                myDataSet.ReadXml("DbConnection.DAT");
                String returnValue = String.Empty;
                // For each row, print the values of each column.
                foreach (DataRow myDataRow in myDataSet.Tables[0].Rows)
                {
                    foreach (DataColumn myDataColumn in myDataSet.Tables[0].Columns)
                    {
                        if (myDataColumn.Caption == "Value")
                            returnValue = (myDataRow[myDataColumn].ToString());
                    }
                }
                returnValue = HashMethod.Decrypt(returnValue, "asdfewrewqrss323");
                return returnValue;
            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("اطلاعات فایل DbConnection.DAT قابل بازیابی نمی باشد", strMessage, "Error").ShowDialog();
                const string returnValue = "";
                return returnValue;
            }

        }
 

    #endregion

        #region Check Data File Exist

        /// <summary>
        /// تابعی برای بررسی آنكه فایل داده ها موجود است یا خیر
        /// </summary>
        /// <returns>
        /// در صورت وجود مقدار درست و در غیر این صورت مقدار نادرست بر میگرداند
        /// </returns>
        private static Boolean CheckDataFileExist()
        {
            if (File.Exists("DbConnection.DAT"))
                return true;
            return false;
        }

        #endregion

        #region ChangePassClick
        private void BtnChangePassClick(object sender, EventArgs e)
        {

            new ChangePassword();
        }
        #endregion


        #endregion

         #region Help
        private void DbConnectionHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "DbConnection.htm");
        }
       #endregion 

  


    

 
    }
}