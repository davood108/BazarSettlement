﻿namespace MohanirPouya.Forms.Start
{
    partial class FrmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainForm));
            this.MainRibbonControl = new DevComponents.DotNetBar.RibbonControl();
            this.RPanelManagement = new DevComponents.DotNetBar.RibbonPanel();
            this.RBarParametersManagement = new DevComponents.DotNetBar.RibbonBar();
            this.btnSellers = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.Param_Structure_S = new DevComponents.DotNetBar.ButtonItem();
            this.btnBillNumS = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersBillItems = new DevComponents.DotNetBar.ButtonItem();
            this.btnSellersUnitBillItemSet = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyers = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.Param_Structure_B = new DevComponents.DotNetBar.ButtonItem();
            this.btnBillNumB = new DevComponents.DotNetBar.ButtonItem();
            this.btnBuyersBillItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransfer = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferParams = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferParamsC = new DevComponents.DotNetBar.ButtonItem();
            this.Param_Structure_T = new DevComponents.DotNetBar.ButtonItem();
            this.btnBillNumT = new DevComponents.DotNetBar.ButtonItem();
            this.btnTransferBillItemD = new DevComponents.DotNetBar.ButtonItem();
            this.btnConnectionSelect = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.btnUser = new DevComponents.DotNetBar.ButtonItem();
            this.btnConnectionSetting = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.btnDailyBill = new DevComponents.DotNetBar.ButtonItem();
            this.btnMonthlyBill = new DevComponents.DotNetBar.ButtonItem();
            this.btnFinalBill = new DevComponents.DotNetBar.ButtonItem();
            this.RTabBill = new DevComponents.DotNetBar.RibbonTabItem();
            this.RTabManagement = new DevComponents.DotNetBar.RibbonTabItem();
            this.RTabSetting = new DevComponents.DotNetBar.RibbonTabItem();
            this.btnChangeStyle = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleOffice2007Blue = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleOffice2007Black = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleOffice2007Silver = new DevComponents.DotNetBar.ButtonItem();
            this.btnStyleCustom = new DevComponents.DotNetBar.ColorPickerDropDown();
            this.StartButton = new DevComponents.DotNetBar.Office2007StartButton();
            this.ItemContainerStart = new DevComponents.DotNetBar.ItemContainer();
            this.ItemContainerStartSmall = new DevComponents.DotNetBar.ItemContainer();
            this.btnStartAbout = new DevComponents.DotNetBar.ButtonItem();
            this.btnStartExit = new DevComponents.DotNetBar.ButtonItem();
            this.MainQatCustomizeItem = new DevComponents.DotNetBar.QatCustomizeItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.MainStatusBar = new DevComponents.DotNetBar.Bar();
            this.lblDataSource = new System.Windows.Forms.Label();
            this.lblCurentUser = new DevComponents.DotNetBar.LabelItem();
            this.lblUserGroup = new DevComponents.DotNetBar.LabelItem();
            this.labelPosition = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer13 = new DevComponents.DotNetBar.ItemContainer();
            this.elementStyle1 = new DevComponents.DotNetBar.ElementStyle();
            this.MainRibbonControl.SuspendLayout();
            this.RPanelManagement.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainStatusBar)).BeginInit();
            this.MainStatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainRibbonControl
            // 
            this.MainRibbonControl.CaptionFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainRibbonControl.CaptionVisible = true;
            this.MainRibbonControl.Controls.Add(this.ribbonPanel1);
            this.MainRibbonControl.Controls.Add(this.RPanelManagement);
            this.MainRibbonControl.Controls.Add(this.ribbonPanel2);
            this.MainRibbonControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainRibbonControl.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RTabBill,
            this.RTabManagement,
            this.RTabSetting,
            this.btnChangeStyle});
            this.MainRibbonControl.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.MainRibbonControl.Location = new System.Drawing.Point(0, 0);
            this.MainRibbonControl.Name = "MainRibbonControl";
            this.MainRibbonControl.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.MainRibbonControl.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.StartButton,
            this.MainQatCustomizeItem});
            this.MainRibbonControl.Size = new System.Drawing.Size(923, 150);
            this.MainRibbonControl.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainRibbonControl.SystemText.MaximizeRibbonText = "بزرگ نمایی";
            this.MainRibbonControl.SystemText.MinimizeRibbonText = "كوچك نمایی";
            this.MainRibbonControl.SystemText.QatAddItemText = "افزودن به نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatCustomizeMenuLabel = "شخصی سازی نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatCustomizeText = "شخصی سازی نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatDialogAddButton = "افزودن";
            this.MainRibbonControl.SystemText.QatDialogCancelButton = "انصراف";
            this.MainRibbonControl.SystemText.QatDialogCaption = "شخصیسازی نوار دسترسی فوری";
            this.MainRibbonControl.SystemText.QatDialogCategoriesLabel = "انتخاب فرمان از";
            this.MainRibbonControl.SystemText.QatDialogOkButton = "تایید";
            this.MainRibbonControl.SystemText.QatDialogPlacementCheckbox = "قرار دادن نوار دسترسی فوری زیر منو";
            this.MainRibbonControl.SystemText.QatDialogRemoveButton = "حذف";
            this.MainRibbonControl.SystemText.QatPlaceAboveRibbonText = "قرار دادن نوار دسترسی فوری بالای منو";
            this.MainRibbonControl.SystemText.QatPlaceBelowRibbonText = "قرار دادن نوار دسترسی فوری زیر منو";
            this.MainRibbonControl.SystemText.QatRemoveItemText = "حذف از نوار دسترسی فوری";
            this.MainRibbonControl.TabGroupHeight = 14;
            this.MainRibbonControl.TabGroupsVisible = true;
            this.MainRibbonControl.TabIndex = 0;
            this.MainRibbonControl.UseExternalCustomization = true;
            // 
            // RPanelManagement
            // 
            this.RPanelManagement.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RPanelManagement.Controls.Add(this.RBarParametersManagement);
            this.RPanelManagement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RPanelManagement.Location = new System.Drawing.Point(0, 56);
            this.RPanelManagement.Name = "RPanelManagement";
            this.RPanelManagement.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.RPanelManagement.Size = new System.Drawing.Size(923, 92);
            this.RPanelManagement.TabIndex = 2;
            this.RPanelManagement.Visible = false;
            // 
            // RBarParametersManagement
            // 
            this.RBarParametersManagement.AutoOverflowEnabled = true;
            this.RBarParametersManagement.Dock = System.Windows.Forms.DockStyle.Left;
            this.RBarParametersManagement.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSellers,
            this.btnBuyers,
            this.btnTransfer,
            this.btnConnectionSelect});
            this.RBarParametersManagement.Location = new System.Drawing.Point(3, 0);
            this.RBarParametersManagement.Name = "RBarParametersManagement";
            this.RBarParametersManagement.Size = new System.Drawing.Size(444, 89);
            this.RBarParametersManagement.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.RBarParametersManagement.TabIndex = 1;
            this.RBarParametersManagement.Text = "مدیریت پارامتر ها";
            // 
            // btnSellers
            // 
            this.btnSellers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSellers.Enabled = false;
            this.btnSellers.Image = global::MohanirPouya.Properties.Resources.payment;
            this.btnSellers.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellers.ImagePaddingHorizontal = 8;
            this.btnSellers.Name = "btnSellers";
            this.btnSellers.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSellersParams,
            this.btnSellersParamsC,
            this.Param_Structure_S,
            this.btnBillNumS,
            this.btnSellersBillItems});
            this.btnSellers.Text = "فروشندگان";
            this.btnSellers.Click += new System.EventHandler(this.BtnSellersClick);
            // 
            // btnSellersParams
            // 
            this.btnSellersParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersParams.ImagePaddingHorizontal = 8;
            this.btnSellersParams.Name = "btnSellersParams";
            this.btnSellersParams.Text = "پارامتر ها";
            this.btnSellersParams.Click += new System.EventHandler(this.BtnSellersParamsClick);
            // 
            // btnSellersParamsC
            // 
            this.btnSellersParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersParamsC.ImagePaddingHorizontal = 8;
            this.btnSellersParamsC.Name = "btnSellersParamsC";
            this.btnSellersParamsC.Text = "پارامتر های شرطی";
            this.btnSellersParamsC.Click += new System.EventHandler(this.BtnSellersParamsCClick);
            // 
            // Param_Structure_S
            // 
            this.Param_Structure_S.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.Param_Structure_S.ImagePaddingHorizontal = 8;
            this.Param_Structure_S.Name = "Param_Structure_S";
            this.Param_Structure_S.Text = "ساختار پارامترها";
            this.Param_Structure_S.Click += new System.EventHandler(this.ParamStructureSClick);
            // 
            // btnBillNumS
            // 
            this.btnBillNumS.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBillNumS.ImagePaddingHorizontal = 8;
            this.btnBillNumS.Name = "btnBillNumS";
            this.btnBillNumS.Text = "مدیریت شماره های صورتحساب";
            this.btnBillNumS.Click += new System.EventHandler(this.BtnBillNumSClick);
            // 
            // btnSellersBillItems
            // 
            this.btnSellersBillItems.BeginGroup = true;
            this.btnSellersBillItems.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnSellersBillItems.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersBillItems.ImagePaddingHorizontal = 8;
            this.btnSellersBillItems.Name = "btnSellersBillItems";
            this.btnSellersBillItems.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSellersUnitBillItemSet});
            this.btnSellersBillItems.Text = "آیتم های صورتحساب";
            // 
            // btnSellersUnitBillItemSet
            // 
            this.btnSellersUnitBillItemSet.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnSellersUnitBillItemSet.ImagePaddingHorizontal = 8;
            this.btnSellersUnitBillItemSet.Name = "btnSellersUnitBillItemSet";
            this.btnSellersUnitBillItemSet.Text = "به تفکیک واحد";
            this.btnSellersUnitBillItemSet.Click += new System.EventHandler(this.BtnSellersUnitBillItemSetClick);
            // 
            // btnBuyers
            // 
            this.btnBuyers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnBuyers.Enabled = false;
            this.btnBuyers.Image = global::MohanirPouya.Properties.Resources.green_dollar;
            this.btnBuyers.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyers.ImagePaddingHorizontal = 8;
            this.btnBuyers.Name = "btnBuyers";
            this.btnBuyers.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnBuyersParams,
            this.btnBuyersParamsC,
            this.Param_Structure_B,
            this.btnBillNumB,
            this.btnBuyersBillItem});
            this.btnBuyers.Text = "خریداران";
            this.btnBuyers.Click += new System.EventHandler(this.BtnBuyersClick);
            // 
            // btnBuyersParams
            // 
            this.btnBuyersParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersParams.ImagePaddingHorizontal = 8;
            this.btnBuyersParams.Name = "btnBuyersParams";
            this.btnBuyersParams.Text = "پارامتر ها";
            this.btnBuyersParams.Click += new System.EventHandler(this.BtnBuyersParamsClick);
            // 
            // btnBuyersParamsC
            // 
            this.btnBuyersParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersParamsC.ImagePaddingHorizontal = 8;
            this.btnBuyersParamsC.Name = "btnBuyersParamsC";
            this.btnBuyersParamsC.Text = "پارامتر های شرطی";
            this.btnBuyersParamsC.Click += new System.EventHandler(this.BtnBuyersParamsCClick);
            // 
            // Param_Structure_B
            // 
            this.Param_Structure_B.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.Param_Structure_B.ImagePaddingHorizontal = 8;
            this.Param_Structure_B.Name = "Param_Structure_B";
            this.Param_Structure_B.Text = "ساختار پارامترها";
            this.Param_Structure_B.Click += new System.EventHandler(this.ParamStructureBClick);
            // 
            // btnBillNumB
            // 
            this.btnBillNumB.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBillNumB.ImagePaddingHorizontal = 8;
            this.btnBillNumB.Name = "btnBillNumB";
            this.btnBillNumB.Text = "مدیریت شماره های صورتحساب";
            this.btnBillNumB.Click += new System.EventHandler(this.BtnBillNumBClick);
            // 
            // btnBuyersBillItem
            // 
            this.btnBuyersBillItem.BeginGroup = true;
            this.btnBuyersBillItem.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnBuyersBillItem.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBuyersBillItem.ImagePaddingHorizontal = 8;
            this.btnBuyersBillItem.Name = "btnBuyersBillItem";
            this.btnBuyersBillItem.Text = "آیتم های صورتحساب";
            this.btnBuyersBillItem.Click += new System.EventHandler(this.BtnBuyersBillItemClick);
            // 
            // btnTransfer
            // 
            this.btnTransfer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnTransfer.Enabled = false;
            this.btnTransfer.Image = global::MohanirPouya.Properties.Resources.web;
            this.btnTransfer.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransfer.ImagePaddingHorizontal = 8;
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.PersonalizedMenus = DevComponents.DotNetBar.ePersonalizedMenus.DisplayOnClick;
            this.btnTransfer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnTransferParams,
            this.btnTransferParamsC,
            this.Param_Structure_T,
            this.btnBillNumT,
            this.btnTransferBillItemD});
            this.btnTransfer.Text = "خدمات انتقال";
            this.btnTransfer.Click += new System.EventHandler(this.BtnTransferClick);
            // 
            // btnTransferParams
            // 
            this.btnTransferParams.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferParams.ImagePaddingHorizontal = 8;
            this.btnTransferParams.Name = "btnTransferParams";
            this.btnTransferParams.Text = "پارامتر ها";
            this.btnTransferParams.Click += new System.EventHandler(this.BtnTransferParamsClick);
            // 
            // btnTransferParamsC
            // 
            this.btnTransferParamsC.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferParamsC.ImagePaddingHorizontal = 8;
            this.btnTransferParamsC.Name = "btnTransferParamsC";
            this.btnTransferParamsC.Text = "پارامتر های شرطی";
            this.btnTransferParamsC.Click += new System.EventHandler(this.BtnTransferParamsCClick);
            // 
            // Param_Structure_T
            // 
            this.Param_Structure_T.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.Param_Structure_T.ImagePaddingHorizontal = 8;
            this.Param_Structure_T.Name = "Param_Structure_T";
            this.Param_Structure_T.Text = "ساختار پارامترها";
            this.Param_Structure_T.Click += new System.EventHandler(this.ParamStructureTClick);
            // 
            // btnBillNumT
            // 
            this.btnBillNumT.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnBillNumT.ImagePaddingHorizontal = 8;
            this.btnBillNumT.Name = "btnBillNumT";
            this.btnBillNumT.Text = "مدیریت شماره های صورتحساب";
            this.btnBillNumT.Click += new System.EventHandler(this.BtnBillNumTClick);
            // 
            // btnTransferBillItemD
            // 
            this.btnTransferBillItemD.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btnTransferBillItemD.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnTransferBillItemD.ImagePaddingHorizontal = 8;
            this.btnTransferBillItemD.Name = "btnTransferBillItemD";
            this.btnTransferBillItemD.Text = "آیتم های صورتحساب (جزئی)";
            this.btnTransferBillItemD.Click += new System.EventHandler(this.BtnTransferBillItemDClick);
            // 
            // btnConnectionSelect
            // 
            this.btnConnectionSelect.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnConnectionSelect.Image = global::MohanirPouya.Properties.Resources.System;
            this.btnConnectionSelect.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnConnectionSelect.ImagePaddingHorizontal = 8;
            this.btnConnectionSelect.Name = "btnConnectionSelect";
            this.btnConnectionSelect.SubItemsExpandWidth = 14;
            this.btnConnectionSelect.Text = "انتخاب اتصال";
            this.btnConnectionSelect.Click += new System.EventHandler(this.BtnConnectionSelectClick);
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel2.Controls.Add(this.ribbonBar2);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(923, 92);
            this.ribbonPanel2.TabIndex = 6;
            this.ribbonPanel2.Visible = false;
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnUser,
            this.btnConnectionSetting});
            this.ribbonBar2.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(230, 89);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar2.TabIndex = 0;
            this.ribbonBar2.Text = "تنظیمات";
            // 
            // btnUser
            // 
            this.btnUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUser.Image = global::MohanirPouya.Properties.Resources.Administrative;
            this.btnUser.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnUser.ImagePaddingHorizontal = 8;
            this.btnUser.Name = "btnUser";
            this.btnUser.SubItemsExpandWidth = 14;
            this.btnUser.Text = "لیست کاربران";
            this.btnUser.Click += new System.EventHandler(this.BtnUserClick);
            // 
            // btnConnectionSetting
            // 
            this.btnConnectionSetting.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnConnectionSetting.Image = global::MohanirPouya.Properties.Resources.DialUp;
            this.btnConnectionSetting.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnConnectionSetting.ImagePaddingHorizontal = 8;
            this.btnConnectionSetting.Name = "btnConnectionSetting";
            this.btnConnectionSetting.SubItemsExpandWidth = 14;
            this.btnConnectionSetting.Text = "تنظیمات اتصالات";
            this.btnConnectionSetting.Click += new System.EventHandler(this.BtnConnectionSettingClick);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel1.Controls.Add(this.ribbonBar1);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(923, 92);
            this.ribbonPanel1.TabIndex = 5;
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnDailyBill,
            this.btnMonthlyBill,
            this.btnFinalBill});
            this.ribbonBar1.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(223, 89);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar1.TabIndex = 0;
            this.ribbonBar1.Text = "صورتحساب";
            // 
            // btnDailyBill
            // 
            this.btnDailyBill.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnDailyBill.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDailyBill.Image = global::MohanirPouya.Properties.Resources.Time___Date;
            this.btnDailyBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnDailyBill.ImagePaddingHorizontal = 8;
            this.btnDailyBill.Name = "btnDailyBill";
            this.btnDailyBill.SubItemsExpandWidth = 14;
            this.btnDailyBill.Text = "روزانه";
            this.btnDailyBill.Click += new System.EventHandler(this.BtnDailyBillClick);
            // 
            // btnMonthlyBill
            // 
            this.btnMonthlyBill.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnMonthlyBill.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMonthlyBill.Image = global::MohanirPouya.Properties.Resources._4585;
            this.btnMonthlyBill.ImageFixedSize = new System.Drawing.Size(32, 32);
            this.btnMonthlyBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnMonthlyBill.ImagePaddingHorizontal = 8;
            this.btnMonthlyBill.Name = "btnMonthlyBill";
            this.btnMonthlyBill.SubItemsExpandWidth = 14;
            this.btnMonthlyBill.Text = "ماهیانه";
            this.btnMonthlyBill.Click += new System.EventHandler(this.BtnMonthlyBillClick);
            // 
            // btnFinalBill
            // 
            this.btnFinalBill.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFinalBill.Image = global::MohanirPouya.Properties.Resources._4165;
            this.btnFinalBill.ImageFixedSize = new System.Drawing.Size(32, 32);
            this.btnFinalBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnFinalBill.ImagePaddingHorizontal = 8;
            this.btnFinalBill.Name = "btnFinalBill";
            this.btnFinalBill.SplitButton = true;
            this.btnFinalBill.Stretch = true;
            this.btnFinalBill.SubItemsExpandWidth = 14;
            this.btnFinalBill.Text = "قطعی";
            this.btnFinalBill.Click += new System.EventHandler(this.BtnFinalBillClick);
            // 
            // RTabBill
            // 
            this.RTabBill.Checked = true;
            this.RTabBill.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabBill.ImagePaddingHorizontal = 8;
            this.RTabBill.Name = "RTabBill";
            this.RTabBill.Panel = this.ribbonPanel1;
            this.RTabBill.Text = "صورتحساب";
            // 
            // RTabManagement
            // 
            this.RTabManagement.ColorTable = DevComponents.DotNetBar.eRibbonTabColor.Green;
            this.RTabManagement.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabManagement.ImagePaddingHorizontal = 8;
            this.RTabManagement.Name = "RTabManagement";
            this.RTabManagement.Panel = this.RPanelManagement;
            this.RTabManagement.Text = "مدیریت";
            // 
            // RTabSetting
            // 
            this.RTabSetting.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.RTabSetting.ImagePaddingHorizontal = 8;
            this.RTabSetting.Name = "RTabSetting";
            this.RTabSetting.Panel = this.ribbonPanel2;
            this.RTabSetting.Text = "تنظیمات";
            // 
            // btnChangeStyle
            // 
            this.btnChangeStyle.AutoExpandOnClick = true;
            this.btnChangeStyle.FontBold = true;
            this.btnChangeStyle.FontItalic = true;
            this.btnChangeStyle.ForeColor = System.Drawing.Color.Red;
            this.btnChangeStyle.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnChangeStyle.ImagePaddingHorizontal = 8;
            this.btnChangeStyle.Name = "btnChangeStyle";
            this.btnChangeStyle.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStyleOffice2007Blue,
            this.btnStyleOffice2007Black,
            this.btnStyleOffice2007Silver,
            this.btnStyleCustom});
            this.btnChangeStyle.Text = "استیل";
            this.btnChangeStyle.Tooltip = "ظاهر فرم جاری برنامه";
            // 
            // btnStyleOffice2007Blue
            // 
            this.btnStyleOffice2007Blue.Checked = true;
            this.btnStyleOffice2007Blue.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleOffice2007Blue.ImagePaddingHorizontal = 8;
            this.btnStyleOffice2007Blue.Name = "btnStyleOffice2007Blue";
            this.btnStyleOffice2007Blue.OptionGroup = "style";
            this.btnStyleOffice2007Blue.Text = "<font color=\"Blue\"><b>آبی</b></font>";
            this.btnStyleOffice2007Blue.Click += new System.EventHandler(this.ButtonStyleOffice2007BlueClick);
            // 
            // btnStyleOffice2007Black
            // 
            this.btnStyleOffice2007Black.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleOffice2007Black.ImagePaddingHorizontal = 8;
            this.btnStyleOffice2007Black.Name = "btnStyleOffice2007Black";
            this.btnStyleOffice2007Black.OptionGroup = "style";
            this.btnStyleOffice2007Black.Text = "<font color=\"black\"><b>مشكی</b></font>";
            this.btnStyleOffice2007Black.Click += new System.EventHandler(this.ButtonStyleOffice2007BlackClick);
            // 
            // btnStyleOffice2007Silver
            // 
            this.btnStyleOffice2007Silver.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleOffice2007Silver.ImagePaddingHorizontal = 8;
            this.btnStyleOffice2007Silver.Name = "btnStyleOffice2007Silver";
            this.btnStyleOffice2007Silver.OptionGroup = "style";
            this.btnStyleOffice2007Silver.Text = "<font color=\"Silver\"><b>نقره ای</b></font>";
            this.btnStyleOffice2007Silver.Click += new System.EventHandler(this.ButtonStyleOffice2007SilverClick);
            // 
            // btnStyleCustom
            // 
            this.btnStyleCustom.BeginGroup = true;
            this.btnStyleCustom.ColorTable = DevComponents.DotNetBar.eButtonColor.Magenta;
            this.btnStyleCustom.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStyleCustom.ImagePaddingHorizontal = 8;
            this.btnStyleCustom.Name = "btnStyleCustom";
            this.btnStyleCustom.Text = "رنگ اختصاصی";
            this.btnStyleCustom.Tooltip = "انتخاب رنگ اختصاصی دلخواه شما";
            this.btnStyleCustom.SelectedColorChanged += new System.EventHandler(this.ButtonStyleCustomSelectedColorChanged);
            this.btnStyleCustom.ColorPreview += new DevComponents.DotNetBar.ColorPreviewEventHandler(this.BtnStyleCustomColorPreview);
            // 
            // StartButton
            // 
            this.StartButton.AutoExpandOnClick = true;
            this.StartButton.CanCustomize = false;
            this.StartButton.Category = "پرونده";
            this.StartButton.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.StartButton.Image = ((System.Drawing.Image)(resources.GetObject("StartButton.Image")));
            this.StartButton.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.StartButton.ImagePaddingHorizontal = 2;
            this.StartButton.ImagePaddingVertical = 2;
            this.StartButton.Name = "StartButton";
            this.StartButton.ShowSubItems = false;
            this.StartButton.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ItemContainerStart});
            this.StartButton.Text = "پرونده";
            this.StartButton.Tooltip = "مدیریت پارامتر ها";
            // 
            // ItemContainerStart
            // 
            // 
            // 
            // 
            this.ItemContainerStart.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.ItemContainerStart.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.ItemContainerStart.Name = "ItemContainerStart";
            this.ItemContainerStart.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ItemContainerStartSmall});
            // 
            // ItemContainerStartSmall
            // 
            // 
            // 
            // 
            this.ItemContainerStartSmall.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.ItemContainerStartSmall.Name = "ItemContainerStartSmall";
            this.ItemContainerStartSmall.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnStartAbout,
            this.btnStartExit});
            // 
            // btnStartAbout
            // 
            this.btnStartAbout.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartAbout.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnStartAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnStartAbout.Image")));
            this.btnStartAbout.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartAbout.ImagePaddingHorizontal = 8;
            this.btnStartAbout.Name = "btnStartAbout";
            this.btnStartAbout.SubItemsExpandWidth = 24;
            this.btnStartAbout.Text = "درباره";
            this.btnStartAbout.Click += new System.EventHandler(this.BtnAboutClick);
            // 
            // btnStartExit
            // 
            this.btnStartExit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStartExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnStartExit.Image = ((System.Drawing.Image)(resources.GetObject("btnStartExit.Image")));
            this.btnStartExit.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.btnStartExit.ImagePaddingHorizontal = 8;
            this.btnStartExit.Name = "btnStartExit";
            this.btnStartExit.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F10);
            this.btnStartExit.SubItemsExpandWidth = 24;
            this.btnStartExit.Text = "خروج از سیستم";
            this.btnStartExit.Click += new System.EventHandler(this.BtnExitClick);
            // 
            // MainQatCustomizeItem
            // 
            this.MainQatCustomizeItem.Name = "MainQatCustomizeItem";
            this.MainQatCustomizeItem.Tooltip = "مدیریت دسترسی فوری به فرامین";
            this.MainQatCustomizeItem.Visible = false;
            // 
            // buttonItem11
            // 
            this.buttonItem11.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.NotSet;
            this.buttonItem11.ImagePaddingHorizontal = 8;
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.SubItemsExpandWidth = 14;
            this.buttonItem11.Text = "buttonItem11";
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 150);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(923, 488);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 1;
            // 
            // MainStatusBar
            // 
            this.MainStatusBar.AccessibleDescription = "DotNetBar Bar (MainStatusBar)";
            this.MainStatusBar.AccessibleName = "DotNetBar Bar";
            this.MainStatusBar.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.MainStatusBar.AntiAlias = true;
            this.MainStatusBar.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.MainStatusBar.Controls.Add(this.lblDataSource);
            this.MainStatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MainStatusBar.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainStatusBar.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.MainStatusBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblCurentUser,
            this.lblUserGroup});
            this.MainStatusBar.ItemSpacing = 2;
            this.MainStatusBar.Location = new System.Drawing.Point(0, 638);
            this.MainStatusBar.Name = "MainStatusBar";
            this.MainStatusBar.Size = new System.Drawing.Size(923, 18);
            this.MainStatusBar.Stretch = true;
            this.MainStatusBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainStatusBar.TabIndex = 8;
            this.MainStatusBar.TabStop = false;
            this.MainStatusBar.Text = "barStatus";
            // 
            // lblDataSource
            // 
            this.lblDataSource.AutoSize = true;
            this.lblDataSource.BackColor = System.Drawing.Color.Transparent;
            this.lblDataSource.ForeColor = System.Drawing.Color.Navy;
            this.lblDataSource.Location = new System.Drawing.Point(14, 3);
            this.lblDataSource.Name = "lblDataSource";
            this.lblDataSource.Size = new System.Drawing.Size(0, 14);
            this.lblDataSource.TabIndex = 0;
            // 
            // lblCurentUser
            // 
            this.lblCurentUser.BeginGroup = true;
            this.lblCurentUser.Name = "lblCurentUser";
            // 
            // lblUserGroup
            // 
            this.lblUserGroup.Name = "lblUserGroup";
            // 
            // labelPosition
            // 
            this.labelPosition.Name = "labelPosition";
            this.labelPosition.PaddingLeft = 2;
            this.labelPosition.PaddingRight = 2;
            this.labelPosition.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.labelPosition.Width = 100;
            // 
            // itemContainer13
            // 
            // 
            // 
            // 
            this.itemContainer13.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer13.Name = "itemContainer13";
            // 
            // elementStyle1
            // 
            this.elementStyle1.Name = "elementStyle1";
            this.elementStyle1.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // FrmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 656);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.MainStatusBar);
            this.Controls.Add(this.MainRibbonControl);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mohanir Pouya Settlement - نرم افزار تولید صورت حساب بازار برق ایران";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMainFormClosing);
            this.Load += new System.EventHandler(this.FrmMainFormLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FrmMainFormHelpRequested);
            this.MainRibbonControl.ResumeLayout(false);
            this.MainRibbonControl.PerformLayout();
            this.RPanelManagement.ResumeLayout(false);
            this.ribbonPanel2.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainStatusBar)).EndInit();
            this.MainStatusBar.ResumeLayout(false);
            this.MainStatusBar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl MainRibbonControl;
        private DevComponents.DotNetBar.RibbonPanel RPanelManagement;
        private DevComponents.DotNetBar.RibbonTabItem RTabManagement;
        private DevComponents.DotNetBar.Office2007StartButton StartButton;
        private DevComponents.DotNetBar.ItemContainer ItemContainerStartSmall;
        private DevComponents.DotNetBar.ButtonItem btnStartAbout;
        private DevComponents.DotNetBar.ButtonItem btnStartExit;
        private DevComponents.DotNetBar.QatCustomizeItem MainQatCustomizeItem;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        private DevComponents.DotNetBar.RibbonBar RBarParametersManagement;
        private DevComponents.DotNetBar.ButtonItem btnSellers;
        private DevComponents.DotNetBar.ButtonItem btnBuyers;
        private DevComponents.DotNetBar.ButtonItem btnTransfer;
        private DevComponents.DotNetBar.ButtonItem btnSellersParams;
        private DevComponents.DotNetBar.ButtonItem btnSellersParamsC;
        private DevComponents.DotNetBar.ButtonItem btnBuyersParams;
        private DevComponents.DotNetBar.ButtonItem btnBuyersParamsC;
        private DevComponents.DotNetBar.ButtonItem btnChangeStyle;
        private DevComponents.DotNetBar.ButtonItem btnStyleOffice2007Blue;
        private DevComponents.DotNetBar.ButtonItem btnStyleOffice2007Black;
        private DevComponents.DotNetBar.ButtonItem btnStyleOffice2007Silver;
        private DevComponents.DotNetBar.ColorPickerDropDown btnStyleCustom;
        private DevComponents.DotNetBar.Bar MainStatusBar;
        private DevComponents.DotNetBar.LabelItem lblCurentUser;
        private DevComponents.DotNetBar.LabelItem lblUserGroup;
        internal DevComponents.DotNetBar.LabelItem labelPosition;
        private DevComponents.DotNetBar.ItemContainer itemContainer13;
        private DevComponents.DotNetBar.ElementStyle elementStyle1;
        private DevComponents.DotNetBar.ButtonItem btnSellersBillItems;
        private DevComponents.DotNetBar.ButtonItem btnBuyersBillItem;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private DevComponents.DotNetBar.RibbonTabItem RTabBill;
        private DevComponents.DotNetBar.ButtonItem btnDailyBill;
        private DevComponents.DotNetBar.ButtonItem btnMonthlyBill;
        private DevComponents.DotNetBar.ButtonItem btnFinalBill;
        private DevComponents.DotNetBar.ButtonItem btnSellersUnitBillItemSet;
        private DevComponents.DotNetBar.ButtonItem btnTransferParams;
        private DevComponents.DotNetBar.ButtonItem btnTransferParamsC;
        private DevComponents.DotNetBar.ButtonItem btnTransferBillItemD;
        private DevComponents.DotNetBar.ButtonItem Param_Structure_S;
        private DevComponents.DotNetBar.ButtonItem Param_Structure_B;
        private DevComponents.DotNetBar.ButtonItem Param_Structure_T;
        private System.Windows.Forms.Label lblDataSource;
        private DevComponents.DotNetBar.ButtonItem btnBillNumS;
        private DevComponents.DotNetBar.ItemContainer ItemContainerStart;
        private DevComponents.DotNetBar.ButtonItem btnBillNumB;
        private DevComponents.DotNetBar.ButtonItem btnBillNumT;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.RibbonBar ribbonBar2;
        private DevComponents.DotNetBar.ButtonItem btnUser;
        private DevComponents.DotNetBar.ButtonItem btnConnectionSetting;
        private DevComponents.DotNetBar.RibbonTabItem RTabSetting;
        private DevComponents.DotNetBar.ButtonItem btnConnectionSelect;
    }
}