﻿namespace MohanirPouya.Forms.Start
{
    partial class DbConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FormTimer = new System.Windows.Forms.Timer(this.components);
            this.PCheckPassword = new DevComponents.DotNetBar.WizardPage();
            this.txtPassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnChangePass = new DevComponents.DotNetBar.ButtonX();
            this.txtUsername = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEnterUNameAndPass = new System.Windows.Forms.Label();
            this.PictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.PSetConnectionString = new DevComponents.DotNetBar.WizardPage();
            this.txtInstance = new System.Windows.Forms.TextBox();
            this.LblInstance = new System.Windows.Forms.Label();
            this.lblServerName = new System.Windows.Forms.Label();
            this.txtServerName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblIPAddress = new System.Windows.Forms.Label();
            this.txtDatabase = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblSelectConnectionType = new System.Windows.Forms.Label();
            this.lblText4 = new System.Windows.Forms.Label();
            this.PSelectConnection = new DevComponents.DotNetBar.WizardPage();
            this.btnConnectionDetails = new DevComponents.DotNetBar.ButtonX();
            this.chkMakeNewConnection = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.lblText3 = new System.Windows.Forms.Label();
            this.lblText2 = new System.Windows.Forms.Label();
            this.lblText1 = new System.Windows.Forms.Label();
            this.MyWizard = new DevComponents.DotNetBar.Wizard();
            this.PCheckPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).BeginInit();
            this.PSetConnectionString.SuspendLayout();
            this.PSelectConnection.SuspendLayout();
            this.MyWizard.SuspendLayout();
            this.SuspendLayout();
            // 
            // FormTimer
            // 
            this.FormTimer.Interval = 500;
            this.FormTimer.Tick += new System.EventHandler(this.FormTimerTick);
            // 
            // PCheckPassword
            // 
            this.PCheckPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PCheckPassword.BackColor = System.Drawing.Color.Transparent;
            this.PCheckPassword.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PCheckPassword.Controls.Add(this.txtPassword);
            this.PCheckPassword.Controls.Add(this.btnChangePass);
            this.PCheckPassword.Controls.Add(this.txtUsername);
            this.PCheckPassword.Controls.Add(this.label2);
            this.PCheckPassword.Controls.Add(this.label1);
            this.PCheckPassword.Controls.Add(this.lblEnterUNameAndPass);
            this.PCheckPassword.Controls.Add(this.PictureBoxLogo);
            this.PCheckPassword.Location = new System.Drawing.Point(7, 57);
            this.PCheckPassword.Name = "PCheckPassword";
            this.PCheckPassword.PageDescription = "ورود نام كاربری و رمز عبور";
            this.PCheckPassword.PageTitle = "ارتباط با بانك اطلاعاتی";
            this.PCheckPassword.Size = new System.Drawing.Size(425, 183);
            this.PCheckPassword.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtPassword.Border.Class = "TextBoxBorder";
            this.txtPassword.Location = new System.Drawing.Point(220, 81);
            this.txtPassword.MaxLength = 100;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPassword.Size = new System.Drawing.Size(196, 21);
            this.txtPassword.TabIndex = 12;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // btnChangePass
            // 
            this.btnChangePass.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnChangePass.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnChangePass.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnChangePass.Location = new System.Drawing.Point(345, 108);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnChangePass.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnChangePass.Size = new System.Drawing.Size(68, 23);
            this.btnChangePass.TabIndex = 48;
            this.btnChangePass.Text = "تغییر رمز";
            this.btnChangePass.Tooltip = "بررسي دستورات وارد شده";
            this.btnChangePass.Click += new System.EventHandler(this.BtnChangePassClick);
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtUsername.Border.Class = "TextBoxBorder";
            this.txtUsername.Location = new System.Drawing.Point(220, 41);
            this.txtUsername.MaxLength = 100;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUsername.Size = new System.Drawing.Size(196, 21);
            this.txtUsername.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(367, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "رمز عبور:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(359, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "نام کاربری:";
            // 
            // lblEnterUNameAndPass
            // 
            this.lblEnterUNameAndPass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnterUNameAndPass.AutoSize = true;
            this.lblEnterUNameAndPass.BackColor = System.Drawing.Color.Transparent;
            this.lblEnterUNameAndPass.Location = new System.Drawing.Point(255, 3);
            this.lblEnterUNameAndPass.Name = "lblEnterUNameAndPass";
            this.lblEnterUNameAndPass.Size = new System.Drawing.Size(165, 13);
            this.lblEnterUNameAndPass.TabIndex = 3;
            this.lblEnterUNameAndPass.Text = "نام كاربری و رمز عبور را وارد نمایید.";
            // 
            // PictureBoxLogo
            // 
            this.PictureBoxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PictureBoxLogo.Image = global::MohanirPouya.Properties.Resources.Button___Logoff;
            this.PictureBoxLogo.Location = new System.Drawing.Point(5, -10);
            this.PictureBoxLogo.Name = "PictureBoxLogo";
            this.PictureBoxLogo.Size = new System.Drawing.Size(162, 159);
            this.PictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxLogo.TabIndex = 12;
            this.PictureBoxLogo.TabStop = false;
            // 
            // PSetConnectionString
            // 
            this.PSetConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PSetConnectionString.BackColor = System.Drawing.Color.Transparent;
            this.PSetConnectionString.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PSetConnectionString.Controls.Add(this.txtInstance);
            this.PSetConnectionString.Controls.Add(this.LblInstance);
            this.PSetConnectionString.Controls.Add(this.lblServerName);
            this.PSetConnectionString.Controls.Add(this.txtServerName);
            this.PSetConnectionString.Controls.Add(this.lblIPAddress);
            this.PSetConnectionString.Controls.Add(this.txtDatabase);
            this.PSetConnectionString.Controls.Add(this.lblSelectConnectionType);
            this.PSetConnectionString.Controls.Add(this.lblText4);
            this.PSetConnectionString.Location = new System.Drawing.Point(7, 57);
            this.PSetConnectionString.Name = "PSetConnectionString";
            this.PSetConnectionString.PageDescription = "انتخاب نوع ارتباط جدید";
            this.PSetConnectionString.PageTitle = "ارتباط با بانك اطلاعاتی";
            this.PSetConnectionString.Size = new System.Drawing.Size(425, 183);
            this.PSetConnectionString.TabIndex = 0;
            // 
            // txtInstance
            // 
            this.txtInstance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.txtInstance.Location = new System.Drawing.Point(183, 88);
            this.txtInstance.Name = "txtInstance";
            this.txtInstance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtInstance.Size = new System.Drawing.Size(141, 22);
            this.txtInstance.TabIndex = 15;
            this.txtInstance.Text = "SQL2014";
            // 
            // LblInstance
            // 
            this.LblInstance.AutoSize = true;
            this.LblInstance.Location = new System.Drawing.Point(323, 91);
            this.LblInstance.Name = "LblInstance";
            this.LblInstance.Size = new System.Drawing.Size(51, 13);
            this.LblInstance.TabIndex = 14;
            this.LblInstance.Text = "نام نمونه:";
            // 
            // lblServerName
            // 
            this.lblServerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServerName.AutoSize = true;
            this.lblServerName.BackColor = System.Drawing.Color.Transparent;
            this.lblServerName.Location = new System.Drawing.Point(323, 55);
            this.lblServerName.Name = "lblServerName";
            this.lblServerName.Size = new System.Drawing.Size(99, 13);
            this.lblServerName.TabIndex = 7;
            this.lblServerName.Tag = "2";
            this.lblServerName.Text = "نام سرور یا IPسرور:";
            // 
            // txtServerName
            // 
            this.txtServerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtServerName.Border.Class = "TextBoxBorder";
            this.txtServerName.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerName.Location = new System.Drawing.Point(222, 52);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtServerName.Size = new System.Drawing.Size(102, 22);
            this.txtServerName.TabIndex = 8;
            this.txtServerName.Tag = "2";
            this.txtServerName.Text = "CB555";
            // 
            // lblIPAddress
            // 
            this.lblIPAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIPAddress.AutoSize = true;
            this.lblIPAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblIPAddress.Location = new System.Drawing.Point(323, 124);
            this.lblIPAddress.Name = "lblIPAddress";
            this.lblIPAddress.Size = new System.Drawing.Size(71, 13);
            this.lblIPAddress.TabIndex = 9;
            this.lblIPAddress.Tag = "2";
            this.lblIPAddress.Text = "نام پایگاه داده:";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtDatabase.Border.Class = "TextBoxBorder";
            this.txtDatabase.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDatabase.Location = new System.Drawing.Point(196, 122);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDatabase.Size = new System.Drawing.Size(128, 22);
            this.txtDatabase.TabIndex = 10;
            this.txtDatabase.Tag = "2";
            // 
            // lblSelectConnectionType
            // 
            this.lblSelectConnectionType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSelectConnectionType.AutoSize = true;
            this.lblSelectConnectionType.BackColor = System.Drawing.Color.Transparent;
            this.lblSelectConnectionType.Location = new System.Drawing.Point(284, 18);
            this.lblSelectConnectionType.Name = "lblSelectConnectionType";
            this.lblSelectConnectionType.Size = new System.Drawing.Size(144, 13);
            this.lblSelectConnectionType.TabIndex = 0;
            this.lblSelectConnectionType.Text = "مشخصات اتصال را وارد نمایید:";
            // 
            // lblText4
            // 
            this.lblText4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText4.AutoSize = true;
            this.lblText4.BackColor = System.Drawing.Color.Transparent;
            this.lblText4.Location = new System.Drawing.Point(196, 170);
            this.lblText4.Name = "lblText4";
            this.lblText4.Size = new System.Drawing.Size(226, 13);
            this.lblText4.TabIndex = 13;
            this.lblText4.Text = "پس از انتخاب ، بر روی گزینه \"بعدی\" كلیك نمایید.";
            // 
            // PSelectConnection
            // 
            this.PSelectConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PSelectConnection.BackColor = System.Drawing.Color.Transparent;
            this.PSelectConnection.CanvasColor = System.Drawing.Color.Transparent;
            this.PSelectConnection.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PSelectConnection.Controls.Add(this.btnConnectionDetails);
            this.PSelectConnection.Controls.Add(this.chkMakeNewConnection);
            this.PSelectConnection.Controls.Add(this.lblText3);
            this.PSelectConnection.Controls.Add(this.lblText2);
            this.PSelectConnection.Controls.Add(this.lblText1);
            this.PSelectConnection.Location = new System.Drawing.Point(7, 57);
            this.PSelectConnection.Name = "PSelectConnection";
            this.PSelectConnection.PageDescription = "انتخاب نوع دسترسی به تنظیمات";
            this.PSelectConnection.PageTitle = "ارتباط با بانك اطلاعاتی";
            this.PSelectConnection.Size = new System.Drawing.Size(425, 183);
            this.PSelectConnection.TabIndex = 8;
            // 
            // btnConnectionDetails
            // 
            this.btnConnectionDetails.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnConnectionDetails.Location = new System.Drawing.Point(251, 102);
            this.btnConnectionDetails.Name = "btnConnectionDetails";
            this.btnConnectionDetails.Size = new System.Drawing.Size(125, 23);
            this.btnConnectionDetails.TabIndex = 1;
            this.btnConnectionDetails.Text = "مشاهده جزئیات اتصال ...";
            this.btnConnectionDetails.Click += new System.EventHandler(this.BtnConnectionDetailsClick);
            // 
            // chkMakeNewConnection
            // 
            this.chkMakeNewConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMakeNewConnection.BackColor = System.Drawing.Color.Transparent;
            this.chkMakeNewConnection.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.chkMakeNewConnection.Location = new System.Drawing.Point(309, 79);
            this.chkMakeNewConnection.Name = "chkMakeNewConnection";
            this.chkMakeNewConnection.Size = new System.Drawing.Size(105, 20);
            this.chkMakeNewConnection.TabIndex = 10;
            this.chkMakeNewConnection.Text = "ایجاد اتصال جدید";
            // 
            // lblText3
            // 
            this.lblText3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText3.AutoSize = true;
            this.lblText3.BackColor = System.Drawing.Color.Transparent;
            this.lblText3.Location = new System.Drawing.Point(188, 167);
            this.lblText3.Name = "lblText3";
            this.lblText3.Size = new System.Drawing.Size(226, 13);
            this.lblText3.TabIndex = 9;
            this.lblText3.Text = "پس از انتخاب ، بر روی گزینه \"بعدی\" كلیك نمایید.";
            // 
            // lblText2
            // 
            this.lblText2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText2.BackColor = System.Drawing.Color.Transparent;
            this.lblText2.Location = new System.Drawing.Point(48, 30);
            this.lblText2.Name = "lblText2";
            this.lblText2.Size = new System.Drawing.Size(369, 46);
            this.lblText2.TabIndex = 7;
            this.lblText2.Text = "در صورتی كه قبلا اتصالی به بانك اطلاعاتی برقرار كرده باشید ، می توانید با استفاده" +
    " از آن به بانك اطلاعاتی متصل شوید. در غیر این صورت می باید یك اتصال جدید ایجاد ن" +
    "مایید.";
            // 
            // lblText1
            // 
            this.lblText1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText1.AutoSize = true;
            this.lblText1.BackColor = System.Drawing.Color.Transparent;
            this.lblText1.Location = new System.Drawing.Point(111, 7);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(303, 13);
            this.lblText1.TabIndex = 7;
            this.lblText1.Text = "این بخش شما را برای ارتباط با بانك اطلاعاتی راهنمایی می نماید.";
            // 
            // MyWizard
            // 
            this.MyWizard.BackButtonText = "< قبلی";
            this.MyWizard.BackButtonWidth = 70;
            this.MyWizard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(229)))), ((int)(((byte)(253)))));
            this.MyWizard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MyWizard.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.MyWizard.CancelButtonText = "خروج";
            this.MyWizard.CancelButtonWidth = 70;
            this.MyWizard.Cursor = System.Windows.Forms.Cursors.Default;
            this.MyWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MyWizard.FinishButtonAutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MyWizard.FinishButtonTabIndex = 3;
            this.MyWizard.FinishButtonText = "ورود";
            this.MyWizard.FinishButtonWidth = 70;
            this.MyWizard.FooterHeight = 35;
            // 
            // 
            // 
            this.MyWizard.FooterStyle.BackColor = System.Drawing.Color.Transparent;
            this.MyWizard.ForeColor = System.Drawing.Color.Navy;
            this.MyWizard.HeaderCaptionFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyWizard.HeaderDescriptionIndent = 45;
            this.MyWizard.HeaderHeight = 45;
            this.MyWizard.HeaderImageAlignment = DevComponents.DotNetBar.eWizardTitleImageAlignment.Left;
            this.MyWizard.HeaderImageSize = new System.Drawing.Size(37, 37);
            // 
            // 
            // 
            this.MyWizard.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
            this.MyWizard.HeaderStyle.BackColorGradientAngle = 90;
            this.MyWizard.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.MyWizard.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.MyWizard.HeaderStyle.BorderBottomWidth = 1;
            this.MyWizard.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.MyWizard.HeaderStyle.BorderLeftWidth = 1;
            this.MyWizard.HeaderStyle.BorderRightWidth = 1;
            this.MyWizard.HeaderStyle.BorderTopWidth = 1;
            this.MyWizard.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.MyWizard.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyWizard.HeaderTitleIndent = 45;
            this.MyWizard.HelpButtonText = "كمك";
            this.MyWizard.HelpButtonVisible = false;
            this.MyWizard.HelpButtonWidth = 70;
            this.MyWizard.Location = new System.Drawing.Point(0, 0);
            this.MyWizard.Name = "MyWizard";
            this.MyWizard.NextButtonText = "بعدی >";
            this.MyWizard.NextButtonWidth = 70;
            this.MyWizard.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MyWizard.Size = new System.Drawing.Size(439, 287);
            this.MyWizard.TabIndex = 0;
            this.MyWizard.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.PSetConnectionString,
            this.PSelectConnection,
            this.PCheckPassword});
            this.MyWizard.BackButtonClick += new System.ComponentModel.CancelEventHandler(this.BtnBackClick);
            this.MyWizard.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.BtnNextClick);
            this.MyWizard.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.BtnAcceptPassword);
            this.MyWizard.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.BtnExitClick);
            // 
            // DbConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(439, 287);
            this.Controls.Add(this.MyWizard);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "DbConnection";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ارتباط با بانك اطلاعاتی";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DbConnectionSetFormClosing);
            this.Load += new System.EventHandler(this.DbConnectionLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.DbConnectionHelpRequested);
            this.PCheckPassword.ResumeLayout(false);
            this.PCheckPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).EndInit();
            this.PSetConnectionString.ResumeLayout(false);
            this.PSetConnectionString.PerformLayout();
            this.PSelectConnection.ResumeLayout(false);
            this.PSelectConnection.PerformLayout();
            this.MyWizard.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer FormTimer;
        private DevComponents.DotNetBar.WizardPage PCheckPassword;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPassword;
        private DevComponents.DotNetBar.Controls.TextBoxX txtUsername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblEnterUNameAndPass;
        internal System.Windows.Forms.PictureBox PictureBoxLogo;
        private DevComponents.DotNetBar.WizardPage PSetConnectionString;
        private System.Windows.Forms.Label lblServerName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtServerName;
        private System.Windows.Forms.Label lblIPAddress;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDatabase;
        private System.Windows.Forms.Label lblSelectConnectionType;
        private System.Windows.Forms.Label lblText4;
        private DevComponents.DotNetBar.WizardPage PSelectConnection;
        private DevComponents.DotNetBar.ButtonX btnConnectionDetails;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkMakeNewConnection;
        private System.Windows.Forms.Label lblText3;
        private System.Windows.Forms.Label lblText2;
        private System.Windows.Forms.Label lblText1;
        internal DevComponents.DotNetBar.Wizard MyWizard;
        private System.Windows.Forms.TextBox txtInstance;
        private System.Windows.Forms.Label LblInstance;
        private DevComponents.DotNetBar.ButtonX btnChangePass;
    }
}

