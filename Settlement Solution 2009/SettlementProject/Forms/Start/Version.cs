﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using MohanirPouya.Classes;

namespace MohanirPouya.Forms.Start
{
    public partial class FrmVersion : MohanirFormDialog
    {
        public FrmVersion()
        {
                InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Directory.Delete(@"D:\Settlement\Temp", true);
                File.Delete(@"D:\Settlement\Version.txt");


                var lastLine = File.ReadAllLines(@"\\cb555\Settlement\Version.txt").Last();
                Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(@"\\cb555\Settlement\Temp", @"D:\Settlement\Temp");
                File.WriteAllText(@"D:\Settlement\Version.txt", lastLine);
                MessageBox.Show(@" فایل ها با موفقیت انتقال یافت",
                                   "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information,
                                       MessageBoxDefaultButton.Button1);


                //Close();
                //Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("اشکال در ایجاد اتصال به سرور ویرایش ها!" +
                                           "", strMessage, "Error").ShowDialog();

            }

            Close();
            Cursor.Current = Cursors.Default;
        }

        private void FrmVersion_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "Version.htm");
        }

        private void FrmVersion_Load(object sender, EventArgs e)
        {

        }

 

       

    }
}
  
