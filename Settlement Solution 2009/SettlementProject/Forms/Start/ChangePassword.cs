﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using MohanirVBClasses;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;


namespace MohanirPouya.Forms.Start
{
    public partial class ChangePassword : MohanirFormDialog
    {
        

        private readonly PreDbmsDataContext _pdbSM;
        public ChangePassword()
        {
            InitializeComponent();

            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);
            ShowDialog();
        }

        private void BtnChangePass1Click(object sender, EventArgs e)
        {
            #region UpdatePass
            string userName;
            userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string strUserName = userName.Substring(userName.LastIndexOf("\\") + 1);

            var query = (from var in _pdbSM.Tbl_Users
                         where var.UserName == strUserName
                         select var.Password);

            try
            {

                if (txtOldPassword1.Text == query.First())
                {
                    if (txtNewPassword1.Text == txtCopyNewPassword1.Text)
                    {
                        var connection = new SqlConnection(DbBizClass.DbConnStrPre);
                        string strConnection = "update [Security].[Tbl_Users] Set Password='" + txtCopyNewPassword1.Text +
                                               "' where UserName='" + strUserName + "' ";

                        connection.Open();
                        var dt = new DataTable();
                        var adapter = new SqlDataAdapter(strConnection, connection);
                        new SqlCommandBuilder(adapter);
                        adapter.Fill(dt);
                        connection.Close();


                        PersianMessageBox.Show(" تغییر رمز با موفقیت انجام شد",
                                               "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        PersianMessageBox.Show(" رمز جدید و تکرار آن یکسان نمی باشد",
                                               "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  
                        txtNewPassword1.Text = "";
                        txtCopyNewPassword1.Text = "";
                    }

                }
                else
                {
                    PersianMessageBox.Show(" رمز قبلی صحیح نمی باشد",
                                           "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtOldPassword1.Text = "";
                    //txtNewPassword1.Text="";
                    //txtCopyNewPassword1.Text = "";

                }
            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("یکی از مشخصات پایگاه داده نادرست می باشد", strMessage, "Error").ShowDialog();
                

            }

            #endregion
        }

        private void ChangePasswordHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "ChangePassword.htm");
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {

        }

   
    }
}
            
