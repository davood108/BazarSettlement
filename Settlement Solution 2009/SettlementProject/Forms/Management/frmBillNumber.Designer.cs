﻿namespace MohanirPouya.Forms.Management
{
    partial class FrmBillNumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdBillNumber = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSaveBillNumber = new System.Windows.Forms.Button();
            this.lblBillType = new System.Windows.Forms.Label();
            this.ssBillNumber = new System.Windows.Forms.StatusStrip();
            this.tsslBillNumber = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtBillType = new System.Windows.Forms.TextBox();
            this.rbReduceBillNum = new System.Windows.Forms.RadioButton();
            this.rbDetail = new System.Windows.Forms.RadioButton();
            this.txtReduceNum = new System.Windows.Forms.TextBox();
            this.MyMainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdBillNumber)).BeginInit();
            this.ssBillNumber.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(368, 129);
            this.lblSubtitle.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(515, 159);
            this.lblTitle.Visible = false;
            // 
            // lblSep
            // 
            this.lblSep.Size = new System.Drawing.Size(818, 2);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.ssBillNumber);
            this.MyMainPanel.Controls.Add(this.lblBillType);
            this.MyMainPanel.Controls.Add(this.rbReduceBillNum);
            this.MyMainPanel.Controls.Add(this.txtBillType);
            this.MyMainPanel.Controls.Add(this.rbDetail);
            this.MyMainPanel.Controls.Add(this.grdBillNumber);
            this.MyMainPanel.Controls.Add(this.txtReduceNum);
            this.MyMainPanel.Controls.Add(this.btnSaveBillNumber);
            this.MyMainPanel.Size = new System.Drawing.Size(814, 544);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.btnSaveBillNumber, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtReduceNum, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.grdBillNumber, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.rbDetail, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtBillType, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.rbReduceBillNum, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblBillType, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.ssBillNumber, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            // 
            // grdBillNumber
            // 
            this.grdBillNumber.AllowUserToAddRows = false;
            this.grdBillNumber.AllowUserToDeleteRows = false;
            this.grdBillNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdBillNumber.BackgroundColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdBillNumber.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdBillNumber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdBillNumber.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Version});
            this.grdBillNumber.Location = new System.Drawing.Point(6, 68);
            this.grdBillNumber.Name = "grdBillNumber";
            this.grdBillNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdBillNumber.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.grdBillNumber.Size = new System.Drawing.Size(808, 396);
            this.grdBillNumber.TabIndex = 2;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "ID";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ToolTipText = "ID";
            this.Column1.Width = 43;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "Subject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column2.HeaderText = "Subject";
            this.Column2.Name = "Column2";
            this.Column2.ToolTipText = "Subject";
            this.Column2.Width = 68;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.DataPropertyName = "CompanyCode";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column3.HeaderText = "CompanyCode";
            this.Column3.Name = "Column3";
            this.Column3.ToolTipText = "CompanyCode";
            this.Column3.Width = 102;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.DataPropertyName = "CompanyName";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column4.HeaderText = "CompanyName";
            this.Column4.Name = "Column4";
            this.Column4.ToolTipText = "CompanyName";
            this.Column4.Width = 104;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.DataPropertyName = "BillType";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column5.HeaderText = "BillType";
            this.Column5.Name = "Column5";
            this.Column5.ToolTipText = "BillType";
            this.Column5.Width = 68;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.DataPropertyName = "CompanyBillCode";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column6.HeaderText = "CompanyBillCode";
            this.Column6.Name = "Column6";
            this.Column6.ToolTipText = "CompanyBillCode";
            this.Column6.Width = 114;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "BillNum";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column7.HeaderText = "BillNum";
            this.Column7.Name = "Column7";
            this.Column7.ToolTipText = "BillNum";
            this.Column7.Width = 65;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "LastState";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column8.HeaderText = "LastState";
            this.Column8.Name = "Column8";
            this.Column8.ToolTipText = "LastState";
            this.Column8.Width = 78;
            // 
            // Version
            // 
            this.Version.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Version.DataPropertyName = "Version";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Version.DefaultCellStyle = dataGridViewCellStyle10;
            this.Version.HeaderText = "Version";
            this.Version.Name = "Version";
            this.Version.ToolTipText = "Version";
            this.Version.Visible = false;
            // 
            // btnSaveBillNumber
            // 
            this.btnSaveBillNumber.Location = new System.Drawing.Point(12, 492);
            this.btnSaveBillNumber.Name = "btnSaveBillNumber";
            this.btnSaveBillNumber.Size = new System.Drawing.Size(104, 23);
            this.btnSaveBillNumber.TabIndex = 10;
            this.btnSaveBillNumber.Text = "ثبت نهایی";
            this.btnSaveBillNumber.UseVisualStyleBackColor = true;
            this.btnSaveBillNumber.Click += new System.EventHandler(this.BtnSaveBillNumberClick);
            // 
            // lblBillType
            // 
            this.lblBillType.AutoSize = true;
            this.lblBillType.Location = new System.Drawing.Point(711, 28);
            this.lblBillType.Name = "lblBillType";
            this.lblBillType.Size = new System.Drawing.Size(93, 13);
            this.lblBillType.TabIndex = 12;
            this.lblBillType.Text = ": نوع صورت حساب";
            // 
            // ssBillNumber
            // 
            this.ssBillNumber.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslBillNumber});
            this.ssBillNumber.Location = new System.Drawing.Point(0, 522);
            this.ssBillNumber.Name = "ssBillNumber";
            this.ssBillNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ssBillNumber.Size = new System.Drawing.Size(814, 22);
            this.ssBillNumber.TabIndex = 14;
            this.ssBillNumber.Text = "statusStrip1";
            // 
            // tsslBillNumber
            // 
            this.tsslBillNumber.Name = "tsslBillNumber";
            this.tsslBillNumber.Size = new System.Drawing.Size(0, 17);
            // 
            // txtBillType
            // 
            this.txtBillType.Enabled = false;
            this.txtBillType.Location = new System.Drawing.Point(578, 25);
            this.txtBillType.Name = "txtBillType";
            this.txtBillType.Size = new System.Drawing.Size(129, 21);
            this.txtBillType.TabIndex = 15;
            // 
            // rbReduceBillNum
            // 
            this.rbReduceBillNum.AutoSize = true;
            this.rbReduceBillNum.Location = new System.Drawing.Point(691, 470);
            this.rbReduceBillNum.Name = "rbReduceBillNum";
            this.rbReduceBillNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rbReduceBillNum.Size = new System.Drawing.Size(118, 17);
            this.rbReduceBillNum.TabIndex = 16;
            this.rbReduceBillNum.TabStop = true;
            this.rbReduceBillNum.Text = "BillNum تغییرات کلی";
            this.rbReduceBillNum.UseVisualStyleBackColor = true;
            this.rbReduceBillNum.CheckedChanged += new System.EventHandler(this.rbReduceBillNum_CheckedChanged);
            // 
            // rbDetail
            // 
            this.rbDetail.AutoSize = true;
            this.rbDetail.Location = new System.Drawing.Point(685, 495);
            this.rbDetail.Name = "rbDetail";
            this.rbDetail.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rbDetail.Size = new System.Drawing.Size(124, 17);
            this.rbDetail.TabIndex = 17;
            this.rbDetail.TabStop = true;
            this.rbDetail.Text = "BillNum تغییرات جزئی";
            this.rbDetail.UseVisualStyleBackColor = true;
            this.rbDetail.CheckedChanged += new System.EventHandler(this.rbDetail_CheckedChanged);
            // 
            // txtReduceNum
            // 
            this.txtReduceNum.Enabled = false;
            this.txtReduceNum.Location = new System.Drawing.Point(629, 470);
            this.txtReduceNum.Name = "txtReduceNum";
            this.txtReduceNum.Size = new System.Drawing.Size(56, 21);
            this.txtReduceNum.TabIndex = 9;
            this.txtReduceNum.Text = "0";
            this.txtReduceNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FrmBillNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 544);
            this.Name = "FrmBillNumber";
            this.Text = "فرم تنظیمات شماره زنی";
            this.Load += new System.EventHandler(this.FrmBillNumberLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FrmBillNumberHelpRequested);
            //this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmBillNumber_KeyDown);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdBillNumber)).EndInit();
            this.ssBillNumber.ResumeLayout(false);
            this.ssBillNumber.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdBillNumber;
        private System.Windows.Forms.Button btnSaveBillNumber;
        private System.Windows.Forms.Label lblBillType;
        private System.Windows.Forms.StatusStrip ssBillNumber;
        private System.Windows.Forms.ToolStripStatusLabel tsslBillNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Version;
        private System.Windows.Forms.TextBox txtBillType;
        private System.Windows.Forms.RadioButton rbDetail;
        private System.Windows.Forms.RadioButton rbReduceBillNum;
        private System.Windows.Forms.TextBox txtReduceNum;
    }
}