﻿namespace MohanirPouya.Forms.Management.BillItemsSettings
{
    partial class BillItemSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblItems = new DevComponents.DotNetBar.LabelX();
            this.lblSetParameter = new DevComponents.DotNetBar.LabelX();
            this.btnClose = new DevComponents.DotNetBar.ButtonX();
            this.btnSetParameter = new DevComponents.DotNetBar.ButtonX();
            this.dgvItems = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.lblParamDesciption = new DevComponents.DotNetBar.LabelX();
            this.txtSetParameter = new System.Windows.Forms.Label();
            this.txtParamDesciption = new System.Windows.Forms.Label();
            this.btnDeleteParameter = new DevComponents.DotNetBar.ButtonX();
            this.btnAddItem = new DevComponents.DotNetBar.ButtonX();
            this.btnApply = new DevComponents.DotNetBar.ButtonX();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MyMainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSubtitle.Location = new System.Drawing.Point(3329, 37);
            this.lblSubtitle.Size = new System.Drawing.Size(108, 24);
            this.lblSubtitle.Text = "تنظیم پارامتر ها";
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(1592, 9);
            this.lblTitle.Size = new System.Drawing.Size(229, 25);
            this.lblTitle.Text = "آیتم های صورتحساب فروش";
            // 
            // lblSep
            // 
            this.lblSep.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSep.Location = new System.Drawing.Point(1182, 57);
            this.lblSep.Size = new System.Drawing.Size(610, 2);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Controls.Add(this.label2);
            this.MyMainPanel.Controls.Add(this.label1);
            this.MyMainPanel.Controls.Add(this.btnApply);
            this.MyMainPanel.Controls.Add(this.btnAddItem);
            this.MyMainPanel.Controls.Add(this.btnDeleteParameter);
            this.MyMainPanel.Controls.Add(this.txtParamDesciption);
            this.MyMainPanel.Controls.Add(this.txtSetParameter);
            this.MyMainPanel.Controls.Add(this.dgvItems);
            this.MyMainPanel.Controls.Add(this.btnSetParameter);
            this.MyMainPanel.Controls.Add(this.btnClose);
            this.MyMainPanel.Controls.Add(this.lblItems);
            this.MyMainPanel.Controls.Add(this.lblParamDesciption);
            this.MyMainPanel.Controls.Add(this.lblSetParameter);
            this.MyMainPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MyMainPanel.Size = new System.Drawing.Size(912, 604);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            this.MyMainPanel.Controls.SetChildIndex(this.lblSep, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblTitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSetParameter, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblParamDesciption, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblItems, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.lblSubtitle, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnClose, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnSetParameter, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.dgvItems, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtSetParameter, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.txtParamDesciption, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnDeleteParameter, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnAddItem, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.btnApply, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.label1, 0);
            this.MyMainPanel.Controls.SetChildIndex(this.label2, 0);
            // 
            // lblItems
            // 
            this.lblItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblItems.AutoSize = true;
            this.lblItems.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblItems.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblItems.Location = new System.Drawing.Point(855, 71);
            this.lblItems.Name = "lblItems";
            this.lblItems.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblItems.Size = new System.Drawing.Size(45, 17);
            this.lblItems.TabIndex = 3;
            this.lblItems.Text = "آیتم ها:";
            // 
            // lblSetParameter
            // 
            this.lblSetParameter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSetParameter.AutoSize = true;
            this.lblSetParameter.Location = new System.Drawing.Point(810, 513);
            this.lblSetParameter.Name = "lblSetParameter";
            this.lblSetParameter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSetParameter.Size = new System.Drawing.Size(88, 16);
            this.lblSetParameter.TabIndex = 3;
            this.lblSetParameter.Text = "پارامتر تنظیم شده:";
            // 
            // btnClose
            // 
            this.btnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(12, 569);
            this.btnClose.Name = "btnClose";
            this.btnClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnClose.Size = new System.Drawing.Size(90, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "بستن <b><font color=\"#B77540\">(Esc)</font></b>";
            // 
            // btnSetParameter
            // 
            this.btnSetParameter.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSetParameter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetParameter.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnSetParameter.Location = new System.Drawing.Point(109, 569);
            this.btnSetParameter.Name = "btnSetParameter";
            this.btnSetParameter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSetParameter.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlN);
            this.btnSetParameter.Size = new System.Drawing.Size(89, 23);
            this.btnSetParameter.TabIndex = 6;
            this.btnSetParameter.TabStop = false;
            this.btnSetParameter.Text = "تنظیم پارامتر...";
            this.btnSetParameter.Click += new System.EventHandler(this.BtnSetParameterClick);
            // 
            // dgvItems
            // 
            this.dgvItems.AllowUserToAddRows = false;
            this.dgvItems.AllowUserToOrderColumns = true;
            this.dgvItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvItems.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItems.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvItems.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvItems.Location = new System.Drawing.Point(12, 93);
            this.dgvItems.MultiSelect = false;
            this.dgvItems.Name = "dgvItems";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItems.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvItems.RowTemplate.Height = 24;
            this.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItems.Size = new System.Drawing.Size(888, 412);
            this.dgvItems.TabIndex = 9;
            this.dgvItems.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvItemsDataError);
            this.dgvItems.SelectionChanged += new System.EventHandler(this.dgvItem_selectionChanged);
            // 
            // lblParamDesciption
            // 
            this.lblParamDesciption.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamDesciption.AutoSize = true;
            this.lblParamDesciption.Location = new System.Drawing.Point(821, 541);
            this.lblParamDesciption.Name = "lblParamDesciption";
            this.lblParamDesciption.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamDesciption.Size = new System.Drawing.Size(77, 16);
            this.lblParamDesciption.TabIndex = 3;
            this.lblParamDesciption.Text = "توضیحات پارامتر:";
            // 
            // txtSetParameter
            // 
            this.txtSetParameter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSetParameter.BackColor = System.Drawing.Color.White;
            this.txtSetParameter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtSetParameter.Location = new System.Drawing.Point(12, 511);
            this.txtSetParameter.Name = "txtSetParameter";
            this.txtSetParameter.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSetParameter.Size = new System.Drawing.Size(792, 21);
            this.txtSetParameter.TabIndex = 10;
            this.txtSetParameter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtParamDesciption
            // 
            this.txtParamDesciption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParamDesciption.BackColor = System.Drawing.Color.White;
            this.txtParamDesciption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtParamDesciption.Location = new System.Drawing.Point(12, 539);
            this.txtParamDesciption.Name = "txtParamDesciption";
            this.txtParamDesciption.Size = new System.Drawing.Size(792, 21);
            this.txtParamDesciption.TabIndex = 10;
            this.txtParamDesciption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDeleteParameter
            // 
            this.btnDeleteParameter.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeleteParameter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteParameter.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnDeleteParameter.Location = new System.Drawing.Point(204, 569);
            this.btnDeleteParameter.Name = "btnDeleteParameter";
            this.btnDeleteParameter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDeleteParameter.Size = new System.Drawing.Size(102, 23);
            this.btnDeleteParameter.TabIndex = 11;
            this.btnDeleteParameter.TabStop = false;
            this.btnDeleteParameter.Text = "پاک کردن پارامتر...";
            this.btnDeleteParameter.Click += new System.EventHandler(this.BtnDeleteParameterClick);
            // 
            // btnAddItem
            // 
            this.btnAddItem.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddItem.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnAddItem.Location = new System.Drawing.Point(420, 569);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAddItem.Size = new System.Drawing.Size(102, 23);
            this.btnAddItem.TabIndex = 12;
            this.btnAddItem.TabStop = false;
            this.btnAddItem.Text = "افزودن آیتم";
            this.btnAddItem.Click += new System.EventHandler(this.BtnAddItemClick);
            // 
            // btnApply
            // 
            this.btnApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnApply.Location = new System.Drawing.Point(312, 569);
            this.btnApply.Name = "btnApply";
            this.btnApply.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnApply.Size = new System.Drawing.Size(102, 23);
            this.btnApply.TabIndex = 13;
            this.btnApply.TabStop = false;
            this.btnApply.Text = "اعمال تغییرات";
            this.btnApply.Click += new System.EventHandler(this.BtnApplyClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(214, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(370, 37);
            this.label1.TabIndex = 14;
            this.label1.Text = "آیتم های صورتحساب فروش به صورت نیروگاهی";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(293, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 37);
            this.label2.TabIndex = 15;
            this.label2.Text = "آیتم های صورتحساب خرید";
            this.label2.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Code";
            this.Column1.HeaderText = "کد";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Name";
            this.Column2.HeaderText = "نام آیتم";
            this.Column2.Name = "Column2";
            this.Column2.Width = 400;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "OrderNo";
            this.Column3.HeaderText = "شماره ردیف";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Subject";
            this.Column4.HeaderText = "نوع آیتم";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 70;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "DailyEnabled";
            this.Column5.HeaderText = "فعال در صورتحساب روزانه";
            this.Column5.Name = "Column5";
            this.Column5.Width = 120;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "MonthlyEnabled";
            this.Column6.HeaderText = "فعال در صورتحساب ماهیانه";
            this.Column6.Name = "Column6";
            this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column6.Width = 120;
            // 
            // BillItemSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(912, 604);
            this.FormSubtitle = "تنظیم پارامتر ها";
            this.FormTitle = "آیتم های صورتحساب فروش";
            this.Name = "BillItemSet";
            this.Text = "تنظیم پارامتر های آیتم های صورتحساب فروش";
            this.Load += new System.EventHandler(this.FrmSetParametersLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.BillItemSetHelpRequested);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblItems;
        private DevComponents.DotNetBar.LabelX lblSetParameter;
        private DevComponents.DotNetBar.ButtonX btnClose;
        private DevComponents.DotNetBar.ButtonX btnSetParameter;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvItems;
        private System.Windows.Forms.Label txtParamDesciption;
        private System.Windows.Forms.Label txtSetParameter;
        private DevComponents.DotNetBar.LabelX lblParamDesciption;
        private DevComponents.DotNetBar.ButtonX btnDeleteParameter;
        private DevComponents.DotNetBar.ButtonX btnAddItem;
        private DevComponents.DotNetBar.ButtonX btnApply;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
    }
}