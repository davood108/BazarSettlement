﻿using System;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;

namespace MohanirPouya.Forms.Management
{
    public partial class FrmBillNumber : MohanirFormDialog
    {
        private readonly DbSMDataContext _dbSM;
        private readonly string _ptype;
        private string _strBillType;

        #region Constructor 1
        public FrmBillNumber()
        {
            InitializeComponent();
            _dbSM = new DbSMDataContext();
           //(DbBizClass.dbConnStr);
           
        }
        #endregion
        
        #region Constructor 2
        public FrmBillNumber(string strBillNum,string strType)
        {
            InitializeComponent();
            _dbSM = new DbSMDataContext
           (DbBizClass.DbConnStr);
            ReadOnlyTrue();
            _ptype = strBillNum;

        }
        #endregion

        #region Constructor3
        public FrmBillNumber(string strBillSelectD)
        {
            InitializeComponent();
            _dbSM = new DbSMDataContext
           (DbBizClass.DbConnStr);


            _ptype = strBillSelectD;
            

            //cbDetail.Visible = false;
            //cbReduceBillNum.Visible = false;
            rbReduceBillNum.Visible = false;
            rbDetail.Visible = false;
            txtReduceNum.Visible = false;
            ReadOnlyTrue();
        }
        #endregion

        #region Events

        #region Form_Load
        private void FrmBillNumberLoad(object sender, EventArgs e)
        {
            #region BillNum
            
            if (_ptype == "BillNum_S")
            {
                txtBillType.Text = "فروشندگان";
                _strBillType = "S";
                var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                where x.Subject == "S" && x.BillType == "N"
                                select x);
                grdBillNumber.DataSource = getdata1;
                tsslBillNumber.Text = @"تعداد فروشندگان:" + getdata1.Count();
            }

            if (_ptype == "BillNum_B")
            {
                txtBillType.Text = "خریداران";
                _strBillType = "B";
                var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                where x.Subject == "B" && x.BillType == "N"
                                select x);
                grdBillNumber.DataSource = getdata1;
                tsslBillNumber.Text = @"تعداد خریداران:" + getdata1.Count();
            }

            if (_ptype == "BillNum_T")
            {
                txtBillType.Text = "خدمات انتقال";
                _strBillType = "T";
                var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                where x.Subject == "T" && x.BillType == "N"
                                select x);
                grdBillNumber.DataSource = getdata1;
                tsslBillNumber.Text = @"تعداد خدمات انتقال:" + getdata1.Count();
            }
            
            #endregion

            #region BillSelectD
            
            if (_ptype == "BillSelectD_S")
            {
                txtBillType.Text = "فروشندگان";
                _strBillType = "S";
                var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                where x.Subject == "S" && x.BillType == "N"
                                select x);
                grdBillNumber.DataSource = getdata1;

                tsslBillNumber.Text = @"تعداد فروشندگان:" + getdata1.Count();

            }

            if (_ptype == "BillSelectD_B")
            {
                txtBillType.Text = "خریداران";
                _strBillType = "B";
                var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                where x.Subject == "B" && x.BillType == "N"
                                select x);


                grdBillNumber.DataSource = getdata1;


                tsslBillNumber.Text = @"تعداد خریداران:" + getdata1.Count();

            }

            if (_ptype == "BillSelectD_T")
            {
                txtBillType.Text = "خدمات انتقال";
                _strBillType = "T";
                var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                where x.Subject == "T" && x.BillType == "N"
                                select x);


                grdBillNumber.DataSource = getdata1;
                tsslBillNumber.Text = @"تعداد خدمات انتقال:" + getdata1.Count();

            }

            #endregion

            ReadOnlyTrue();

                foreach (DataGridViewRow row in grdBillNumber.Rows)
                {
                    string strVersion = row.Cells[8].Value.ToString();

                    if (strVersion == "NEW")
                    {
                        row.DefaultCellStyle.BackColor = System.Drawing.Color.PaleGreen;
                        
                        if (row.DefaultCellStyle.BackColor == System.Drawing.Color.PaleGreen)
                        {
                            ReadOnlyFalse();
                        }
                    }
                }
         }
        #endregion

        #region BtnSaveBillNumberClick
        private void BtnSaveBillNumberClick(object sender, EventArgs e)
        {
            #region BillNum_S
            if (_ptype == "BillNum_S")
            {
                if (rbReduceBillNum.Checked)
                {
                    #region rbReduceBillNum
                    int intBillNum = Convert.ToInt32(txtReduceNum.Text);

                    _dbSM.SP_BillNumberRepair(intBillNum, _strBillType);

                    var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                    where x.Subject == _strBillType && x.BillType == "N"
                                    select x);
                    grdBillNumber.DataSource = getdata1;

                    grdBillNumber.Columns[0].Visible = false;
                    grdBillNumber.Columns[7].Visible = false;
                    #endregion
                }
                else
                {
                    #region rbDetailChecked
                    ReadOnlyFalse();
                    for (int i = 0; i < grdBillNumber.RowCount; i++)
                    {
                        int id = Convert.ToInt32(grdBillNumber.Rows[i].Cells[0].Value);
                        var getdata2 = (_dbSM.Tbl_BillNumbers.Where(x1 => x1.ID == id)).Single();
                        getdata2.CompanyBillCode = (grdBillNumber.Rows[i].Cells[5].Value).ToString();
                        getdata2.BillNum = Convert.ToInt32(grdBillNumber.Rows[i].Cells[6].Value);
                        _dbSM.SubmitChanges();
                    }
                    #endregion
                }

            }
            #endregion

            #region BillNum_B
            if (_ptype == "BillNum_B")
            {
                if (rbReduceBillNum.Checked)
                {
                    #region rbReduceBillNum
                    int intBillNum = Convert.ToInt32(txtReduceNum.Text);

                    _dbSM.SP_BillNumberRepair(intBillNum, _strBillType);

                    var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                    where x.Subject == _strBillType && x.BillType == "N"
                                    select x);
                    grdBillNumber.DataSource = getdata1;

                    grdBillNumber.Columns[0].Visible = false;
                    grdBillNumber.Columns[7].Visible = false;
                    #endregion
                }
                else
                {
                    #region rbDetailChecked
                    ReadOnlyFalse();
                    for (int i = 0; i < grdBillNumber.RowCount; i++)
                    {
                        int id = Convert.ToInt32(grdBillNumber.Rows[i].Cells[0].Value);
                        var getdata2 = (_dbSM.Tbl_BillNumbers.Where(x1 => x1.ID == id)).Single();
                        getdata2.CompanyBillCode = (grdBillNumber.Rows[i].Cells[5].Value).ToString();
                        getdata2.BillNum = Convert.ToInt32(grdBillNumber.Rows[i].Cells[6].Value);
                        _dbSM.SubmitChanges();
                    }
                    #endregion
                }

            }
            #endregion

            #region BillNum_T
            if (_ptype == "BillNum_T")
            {
                if (rbReduceBillNum.Checked)
                {
                    #region rbReduceBillNum
                    int intBillNum = Convert.ToInt32(txtReduceNum.Text);

                    _dbSM.SP_BillNumberRepair(intBillNum, _strBillType);

                    var getdata1 = (from x in _dbSM.Tbl_BillNumbers
                                    where x.Subject == _strBillType && x.BillType == "N"
                                    select x);
                    grdBillNumber.DataSource = getdata1;

                    grdBillNumber.Columns[0].Visible = false;
                    grdBillNumber.Columns[7].Visible = false;
                    #endregion
                }
                else
                {
                    #region rbDetailChecked
                    ReadOnlyFalse();
                    for (int i = 0; i < grdBillNumber.RowCount; i++)
                    {
                        int id = Convert.ToInt32(grdBillNumber.Rows[i].Cells[0].Value);
                        var getdata2 = (_dbSM.Tbl_BillNumbers.Where(x1 => x1.ID == id)).Single();
                        getdata2.CompanyBillCode = (grdBillNumber.Rows[i].Cells[5].Value).ToString();
                        getdata2.BillNum = Convert.ToInt32(grdBillNumber.Rows[i].Cells[6].Value);
                        _dbSM.SubmitChanges();
                    }
                    #endregion
                }

            }
            #endregion
            
            else
            {
                #region BillSelectD
                grdBillNumber.Columns[5].ReadOnly = false;
              
                for (int i = 0; i < grdBillNumber.RowCount; i++)
                {
                    int id = Convert.ToInt32(grdBillNumber.Rows[i].Cells[0].Value);
                   var getdata3 = (_dbSM.Tbl_BillNumbers.Where(x1 => x1.ID == id)).Single();
                    getdata3.CompanyBillCode = (grdBillNumber.Rows[i].Cells[5].Value).ToString();
                    getdata3.BillNum = Convert.ToInt32(grdBillNumber.Rows[i].Cells[6].Value);
                    _dbSM.SubmitChanges();
                }

                var getdata2 = (_dbSM.Tbl_BillNumbers);
                foreach (var x1 in getdata2)
                {
                    x1.Version = "Old";
                    _dbSM.SubmitChanges();
                }
                #endregion

            }
            
            Close();
       
        }
        #endregion

        #region rbReduceBillNumCheckedChanged
        private void rbReduceBillNum_CheckedChanged(object sender, EventArgs e)
        {
            if (rbReduceBillNum.Checked)
            {
                txtReduceNum.Enabled = true;
                rbDetail.Checked = false;
                ReadOnlyTrue();
            }

            else
            {
                txtReduceNum.Enabled = false;
                rbReduceBillNum.Checked = false;
                txtReduceNum.Text = "";
                grdBillNumber.Columns[0].ReadOnly = true;
                grdBillNumber.Columns[1].ReadOnly = true;
                grdBillNumber.Columns[2].ReadOnly = true;
                grdBillNumber.Columns[3].ReadOnly = true;
                grdBillNumber.Columns[4].ReadOnly = true;
                grdBillNumber.Columns[5].ReadOnly = true;
                grdBillNumber.Columns[6].ReadOnly = false;
                grdBillNumber.Columns[7].ReadOnly = true;
                grdBillNumber.Columns[8].ReadOnly = true;
                grdBillNumber.Columns[0].Visible = false;
                grdBillNumber.Columns[7].Visible = false;
            }
        }
        #endregion

        #region rbDetailCheckedChanged
        private void rbDetail_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDetail.Checked)
            {
                txtReduceNum.Enabled = false;
                rbReduceBillNum.Checked = false;
                txtReduceNum.Text = "";
                ReadOnlyTrue();
                //btnSaveBillNumber.Enabled = false;
                grdBillNumber.Columns[0].ReadOnly = true;
                grdBillNumber.Columns[1].ReadOnly = true;
                grdBillNumber.Columns[2].ReadOnly = true;
                grdBillNumber.Columns[3].ReadOnly = true;
                grdBillNumber.Columns[4].ReadOnly = true;
                grdBillNumber.Columns[5].ReadOnly = false;
                grdBillNumber.Columns[6].ReadOnly = false;
                grdBillNumber.Columns[7].ReadOnly = true;
                grdBillNumber.Columns[8].ReadOnly = true;
                grdBillNumber.Columns[0].Visible = false;
                grdBillNumber.Columns[7].Visible = false;
            }

            else
            {
                ReadOnlyTrue();
            }
        }
        #endregion

        #endregion

        #region Method

        #region ReadOnlyTrue
        private void ReadOnlyTrue()
        {
            grdBillNumber.Columns[0].ReadOnly = true;
            grdBillNumber.Columns[1].ReadOnly = true;
            grdBillNumber.Columns[2].ReadOnly = true;
            grdBillNumber.Columns[3].ReadOnly = true;
            grdBillNumber.Columns[4].ReadOnly = true;
            grdBillNumber.Columns[5].ReadOnly = true;
            grdBillNumber.Columns[6].ReadOnly = true;
            grdBillNumber.Columns[7].ReadOnly = true;
            grdBillNumber.Columns[8].ReadOnly = true;
            grdBillNumber.Columns[0].Visible = false;
            grdBillNumber.Columns[7].Visible = false;
        }
        #endregion

        #region ReadOnlyFalse
        private void ReadOnlyFalse()
        {
            grdBillNumber.Columns[0].ReadOnly = false;
            grdBillNumber.Columns[1].ReadOnly = false;
            grdBillNumber.Columns[2].ReadOnly = false;
            grdBillNumber.Columns[3].ReadOnly = false;
            grdBillNumber.Columns[4].ReadOnly = false;
            grdBillNumber.Columns[5].ReadOnly = false;
            grdBillNumber.Columns[6].ReadOnly = false;
            grdBillNumber.Columns[7].ReadOnly = false;
            grdBillNumber.Columns[8].ReadOnly = false;
            grdBillNumber.Columns[0].Visible = false;
            grdBillNumber.Columns[7].Visible = false;
        }
        #endregion

        #endregion

        #region Help
        private void FrmBillNumberHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "BillNumber.htm");
        }
        #endregion

    }
}


