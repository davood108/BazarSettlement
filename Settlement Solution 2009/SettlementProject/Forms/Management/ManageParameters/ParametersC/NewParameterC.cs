﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.ParametersC
{
    /// <summary>
    /// فرم افزودن سرفصل جدید به بانك اطلاعات
    /// </summary>
    public partial class FrmNewParameterC : Form
    {

        #region Fields

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر و سرفصل ها
        /// </summary>
        private DataTable _dataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _arrayListParams;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش مورد جستجو
        /// </summary>
        private readonly String _partName;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر مورد نظر
        /// </summary>
        private readonly String _paramType;

        private readonly string _reuse;
        private readonly string _currentParamName;
        private string _currentParamXml;

        #endregion

        private readonly string _userID;
        private  string _testingQuery;
        private string _cdate;
        private bool _testState;

        #endregion

        #region Constructor
        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="partName">نام بخش مورد نظر</param>
        /// <param name="paramType">نوع پارامتر</param>
        /// <param name="reuse"></param>
        /// <param name="listItem"></param>
        /// <param name="testingQuery"></param>
        public FrmNewParameterC(String partName, String paramType, string reuse, string listItem, string testingQuery)
        {
            InitializeComponent();
            _userID =DbBizClass.CurrentUserLastName;
            _partName = partName;
            _testingQuery = testingQuery;
            TreeViewCode.PartName = partName;
            _paramType = paramType;
            _reuse = reuse;
            _currentParamName = listItem;
            #region SetTime
            PersianDate occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            PCreationDate.Text = occuredDate.ToString();
            #endregion
        }

   

        #endregion

        #region Events Handlers

        #region Form Load

        /// <summary>
        /// روال مدیریت آغاز فراخوانی فرم
        /// </summary>
        private void FormLoad(object sender, EventArgs e)
        {
            #region Set Form Title
            if (_partName == "Sellers") lblTitle.Text = @"فروشندگان";
            else if (_partName == "Buyers") lblTitle.Text = @"خریداران";
            else if (_partName == "Transfer") lblTitle.Text = @"خدمات انتقال";
            if (_paramType == "Pc")
            {
                Text = @"فرم ایجاد پارامتر شرطی جدید";
                lblSubtitle.Text = @"ایجاد پارامتر شرطی جدید";
            }
            else if (_paramType == "Sa")
            {
                Text = @"فرم ایجاد سرفصل جدید";
                lblSubtitle.Text = @"ایجاد سرفصل جدید";
            }
            else if (_paramType == "Sh")
            {
                Text = @"فرم ایجاد شاخص جدید";
                lblSubtitle.Text = @"ایجاد شاخص جدید";
            }
            #endregion
            
            FillParametersTable();
            SetParamsNameAutoComplete();
            if (_reuse == "R")
            {
                FillParametersTable();
                SetParamLastDataByName();
                TreeViewCode.FillTreeView(_currentParamXml);
            }
        }

        #endregion

        // 000000000000000000000000

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void BtnValidationClick(object sender, EventArgs e)
        {
            _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }

            #region Get Line Condition String From TreeView Nodes
            String lineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCode.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(lineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion



 

            String paramName = txtParamName.Text;
            _testingQuery = ManageParametersC.GetParamString(_partName, paramName,lineConditionString, _dataTableParams, _arrayListParams, true, 0);
            
            if (!String.IsNullOrEmpty(_testingQuery)) new frmTestParameters(_testingQuery);
        }
        #endregion

        #region btnSaveAndClose_Click
        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void BtnSaveAndCloseClick(object sender, EventArgs e)
        {

            _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }

            #region Set Current Date
            PersianDate occuredDate1 = PersianDateConverter.ToPersianDate
(DateTime.Now);
            _cdate = occuredDate1.ToString();
            #endregion
            
            #region Get Line Condition String From TreeView Nodes
            String lineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCode.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(lineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion
          
            #region Declare Variables
            String paramName = txtParamName.Text;
            const Int32 newParamRevision = 0;

            #endregion

            #region Testing
            _testingQuery = ManageParametersC.GetParamString(_partName, paramName, lineConditionString, _dataTableParams, _arrayListParams, true, 0);

            if (!String.IsNullOrEmpty(_testingQuery))
                _testState = CheckTestingQuery(_testingQuery);
            if (_testState == false)
            {

                return;
            }
            #endregion

            #region *** Creating Parameter Table By SQL Query ***
            if (ManageParametersC.CreateParameterTable(_dataTableParams,
                                                      _arrayListParams, paramName, txtParamDescribe.Text, newParamRevision, lineConditionString, _userID,_partName, _paramType, TreeViewCode.CodeTreeView, _testingQuery,PCreationDate.Text,_cdate) == false) return;
            #endregion

            Dispose();
        }
        #endregion

        // 000000000000000000000000

        #region btnCancel Click

        /// <summary>
        /// روال انصراف از تعریف پارامتر شرطی
        /// </summary>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            Dispose();
        }

        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()
        /// <summary>
        /// بدست آوردن ليست كلیه پارامترها ، پارامترهای شرطی ، سرفصلها و شاخص ها
        /// </summary>
        private void FillParametersTable()
        {
            _dataTableParams = Classes.Parameters.GetAllParamsByPart(_partName);
            _arrayListParams =
                Classes.Parameters.GetParamsNameByTable(_dataTableParams);
        }

        #endregion

        #region void SetParamsNameAutoComplete()

        /// <summary>
        /// تنظيم عناصر پارامتر ها براي نام پارامتر
        /// </summary>
        private void SetParamsNameAutoComplete()
        {
            var myAutoCompleteCustomSource =
                new AutoCompleteStringCollection();
            foreach (object t in _arrayListParams)
                myAutoCompleteCustomSource.Add(t.ToString());
          //  txtParamName.AutoCompleteCustomSource = myAutoCompleteCustomSource;
        }

        #endregion

        #region void SetParamLastDataByName()
        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void SetParamLastDataByName()
        {


            #region Get Current Param Revision  , SelectString (XML)

            #region Prepare SqlCommand

            String commandText = "SELECT TOP 1 Revision , " +
                                 "SelectString " +
                                 "FROM " + _partName + ".ParametersLog " +
                                 "WHERE EnglishName = @ParamName ORDER BY Revision DESC";
            var mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            var mySqlCommand = new SqlCommand(commandText, mySqlConnection);
            mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

            #endregion

            #region Execute SqlCommand
            try
            {
                mySqlCommand.Connection.Open();
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                mySqlDataReader.Read();
                _currentParamXml = mySqlDataReader[1].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"خطا!");
            }
            finally
            {
                mySqlCommand.Connection.Close();
            }

            #endregion

            #endregion


        }

        #endregion
        
        #region Parameter Saving Validations


        private Boolean ParameterSavingValidations()
        {
            #region Check First Day of Month choose
            if ((PCreationDate.Text.Substring(8, 2)) != "01")
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            #endregion

            #region Param Name Is Empty
            if (txtParamName.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر نامي به انگليسي وارد نماييد!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamName.Focus();
                return false;
            }
            #endregion

            #region Param Name Lenght Less Than 3 Charecter
            if (txtParamName.Text.Length < 3)
            {
                PersianMessageBox.Show("نام پارامتر بايد حداقل داراي 3 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamName.Focus();
                return false;
            }
            #endregion

            #region Boolean CheckDescriptionTextBox

            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }


            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }


            #endregion

            return true;
        }
        #endregion

        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var parameterResult = new DataTable();

            var mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            var mySqlCommand = new SqlCommand(query, mySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                mySqlCommand.Connection.Open();

                // ReSharper disable AssignNullToNotNullAttribute
                parameterResult.Load(mySqlCommand.ExecuteReader());
                // ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("در دستور وارد شده خطایی وجود دارد!" +
                                           "كدهای وارد شده را مجددا بررسی نمایید", strMessage, "Error").ShowDialog();
                mySqlCommand.Connection.Close();
                return false;
            }
            finally
            {
                mySqlCommand.Connection.Close();
           
            }
            #endregion

            return true;

        }
        #endregion

       
        #endregion

        private void FrmNewParameterCHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "NewParameterC.htm");
        }

        private void TxtParamNameKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(32))
            {
                e.Handled = true;
            }
            if (System.Text.Encoding.UTF8.GetByteCount(new char[] { e.KeyChar }) > 1)
            {
                e.Handled = true;
            }
            return;
        }

 

      

   

    }
}