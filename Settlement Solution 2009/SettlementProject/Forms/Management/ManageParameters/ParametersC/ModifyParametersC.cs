﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.ParametersC
{
    /// <summary>
    /// كلاس مدیریت فرم ویرایش پارامتر ها
    /// </summary>
    public partial class FrmModifyParametersC : Form
    {

        #region Fields

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر ها
        /// </summary>
        private DataTable _dataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _arrayListParams;
        #endregion

        #region readonly String _CurrentParamName
        /// <summary>
        /// نام پارامتری كه در حال ویرایش است
        /// </summary>
        private readonly String _currentParamName;
        #endregion

        #region Int32 _CurrentParamRevision
        /// <summary>
        /// شماره آخرین پارامتر ثبت شده
        /// </summary>
        private Int32 _currentParamRevision;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر
        /// </summary>
        private readonly String _paramType;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش تعیین شده
        /// </summary>
        private readonly String _partName;
        #endregion

        #region String _CurrentParamXml

        /// <summary>
        /// ایكس ام ال پارامتر جاری
        /// </summary>
        private String _currentParamXml;

        //    private PersianDatePicker startDateDataGridViewTextBoxColumn1;

        #endregion

        private readonly string _userID;
        private string _cdate;
        private readonly DbSMDataContext _dbSM;
        private DataTable _dataLogtable;
        private string _commandText;
        private SqlConnection _mySqlConnection;
        private SqlCommand _mySqlCommand;
        private int _startDateCount;
        private string _testingQuery;
        private bool _testState;
        private string _activeCount;

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="paramName">نام پارامتر برای ویرایش</param>
        /// <param name="partName">نام بخش شامل فروشندگان ، خریداران و غیره</param>
        /// <param name="paramType">نوع پارامتر از قبیل پارامتر شرطی ، سرفصل و غیره</param>
        public FrmModifyParametersC(String paramName, String partName, String paramType)
        {
            InitializeComponent();
            _dbSM = new DbSMDataContext
(DbBizClass.DbConnStr);

            _userID = DbBizClass.CurrentUserLastName;

            

            _currentParamName = paramName;
            _partName = partName;
            TreeViewCodeM.PartName = partName;
            _paramType = paramType;

            #region Set Form Title
            if (_partName == "Sellers") lblTitle.Text = @"فروشندگان";
            else if (_partName == "Buyers") lblTitle.Text = @"خریداران";
            else if (_partName == "Transfer") lblTitle.Text = @"خدمات انتقال";
            if (_paramType == "Pc")
            {
                // ReSharper disable DoNotCallOverridableMethodsInConstructor
                Text = @"فرم ویرایش پارامتر شرطی";
                // ReSharper restore DoNotCallOverridableMethodsInConstructor
                lblSubtitle.Text = @"ویرایش پارامتر شرطی";
            }
            else if (_paramType == "Sa")
            {
                // ReSharper disable DoNotCallOverridableMethodsInConstructor
                Text = @"فرم ویرایش سرفصل";
                // ReSharper restore DoNotCallOverridableMethodsInConstructor
                lblSubtitle.Text = @"ویرایش سرفصل";
            }
            else if (_paramType == "Sh")
            {
                // ReSharper disable DoNotCallOverridableMethodsInConstructor
                Text = @"فرم ویرایش شاخص";
                // ReSharper restore DoNotCallOverridableMethodsInConstructor
                lblSubtitle.Text = @"ویرایش شاخص";
            }
            #endregion

            #region SetTime
            PersianDate occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            PCreationDate.Text = occuredDate.ToString();
            #endregion

            ShowDialog();
        }
        #endregion

        #region Events Handlers

        #region Form Load
        /// <summary>
        /// روال مدیریت آغاز فراخوانی فرم
        /// </summary>
        private void FormLoad(object sender, EventArgs e)
        {
            
            FillParametersTable();
            SetParamLastDataByName("F",10000);
            TreeViewCodeM.FillTreeView(_currentParamXml);
            FillLogDataGrid();
        }
        #endregion

        #region dgvLog_CellMouseClick

        /// <summary>
        /// روال مدیریت كلیك بر روی جدول جزئیات
        /// </summary>
        private void DgvLogCellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SetParamLastDataByName("E", Convert.ToInt16(dgvLog.Rows[e.RowIndex].Cells[0].Value));
            TreeViewCodeM.FillTreeView(_currentParamXml);
        }
        #endregion

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void BtnValidationClick(object sender, EventArgs e)
        {
            _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }
            #region Get Line Condition String From TreeView Nodes
            String lineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCodeM.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(lineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion

     
          _testingQuery = ManageParametersC.GetParamString(_partName,
                                                                   _currentParamName,lineConditionString, _dataTableParams, _arrayListParams, true,_currentParamRevision);

     

            if (!String.IsNullOrEmpty(_testingQuery)) new frmTestParameters(_testingQuery);
        }
        #endregion

        #region btnSaveAndClose_Click
        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void BtnSaveAndCloseClick(object sender, EventArgs e)
        {

            _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }

            

            #region Get Current Param Revision

            #region Prepare SqlCommand

            _commandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _partName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName";
            _mySqlConnection =
               new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
            _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

            #endregion

            #region Execute SqlCommand

            try
            {
                _mySqlCommand.Connection.Open();
                var mySqlDataReader = _mySqlCommand.ExecuteReader();
                mySqlDataReader.Read();
                _currentParamRevision = Convert.ToInt32(mySqlDataReader[0].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"خطا!");
            }
            finally
            {
                _mySqlCommand.Connection.Close();
            }

            #endregion

            #endregion

            #region Set Current Date
            PersianDate occuredDate1 = PersianDateConverter.ToPersianDate
(DateTime.Now);
            _cdate = occuredDate1.ToString();
            #endregion

            #region Check User Data Entry
            if (CheckDescriptionTextBox() == false) return;
            #endregion

            #region Get Line Condition String From TreeView Nodes
            var lineConditionString =
                ManageParametersC.GetLineConditionByTree(TreeViewCodeM.CodeTreeView.Nodes);
            #endregion

            #region Check Generated Tree String Is Null Or Empty
            if (String.IsNullOrEmpty(lineConditionString))
            {
                PersianMessageBox.Show("هیچ شرط و نتیجه برای ساختار تعیین نشده است!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion

            #region Testing
            _testingQuery = ManageParametersC.GetParamString(_partName,
              _currentParamName, lineConditionString, _dataTableParams, _arrayListParams, true, _currentParamRevision);

            if (!String.IsNullOrEmpty(_testingQuery))
                _testState = CheckTestingQuery(_testingQuery);
            if (_testState == false)
            {

                return;
            }
            #endregion

            
            #region Creating Parameter Table By SQL Query
            if (ManageParametersC.CreateParameterTable(_dataTableParams,
                                                      _arrayListParams, _currentParamName, txtParamDescribe.Text,_currentParamRevision, lineConditionString, _userID,_partName, _paramType, TreeViewCodeM.CodeTreeView, _testingQuery,PCreationDate.Text,_cdate) == false) return;
            #endregion

            FillLogDataGrid();
        }
        #endregion

        #region btn Deactive
        private void BtnDeActiveClick(object sender, EventArgs e)
        {

        
  
                if ((bool) dgvLog.SelectedRows[0].Cells[5].Value)
                {
                    return;
                }


                DialogResult qResult =
                    PersianMessageBox.Show("آيا از اين كار اطمينان داريد؟\n " +
                                           "در صورت تایید ، این ویرایش از متغیر دیگر قابل دسترس نخواهد بود!", "هشدار!",
                                           MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (qResult == DialogResult.No)
                {
                    return;
                }


                _dbSM.SP_ParamDeactivation(Convert.ToInt16(dgvLog.SelectedRows[0].Cells[0].Value), _currentParamName,
                                           _partName);
                FillLogDataGrid();
         
        }
        #endregion

        #region btnCancel Click

        /// <summary>
        /// روال انصراف از تغییر پارامتر
        /// </summary>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            CheckParamBeActive();


            if (Convert.ToInt32(_activeCount) == 0)
            {
                PersianMessageBox.Show("هر پارامتر حداقل باید یک ویرایش فعال داشته باشد\n " +
                                           "لطفا یک ویرایش فعال ایجاد و سپس از این صفحه خارج شوید!", "خطا",
                      MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }
            //MessageBox.Show("close");
            Dispose();
        }
        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()
        /// <summary>
        /// بدست آوردن ليست كلیه پارامترها ، پارامترهای شرطی ، سرفصلها و شاخص ها
        /// </summary>
        private void FillParametersTable()
        {
            _dataTableParams = Classes.Parameters.GetAllParamsByPart(_partName);
            _arrayListParams =
                Classes.Parameters.GetParamsNameByTable(_dataTableParams);
        }
        #endregion

        #region void SetParamLastDataByName()
        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void SetParamLastDataByName(string param, int rev)
        {
            txtCurrentParamName.Text = _currentParamName;

            if (param == "F")
            {
                #region Get SelectString (XML)

                #region Prepare SqlCommand

                 _commandText = "SELECT " +
                                     "SelectString " +
                                     "FROM " + _partName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName and active=0 ORDER BY StartDate  DESC";//remove: and enddate is null. 1395/01/17
               _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
             _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    _mySqlCommand.Connection.Open();
                    var mySqlDataReader = _mySqlCommand.ExecuteReader();
                    mySqlDataReader.Read();
                   

                    _currentParamXml = mySqlDataReader[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }

                #endregion

                #endregion

                #region Get Current Param Revision

                #region Prepare SqlCommand

                _commandText = "SELECT " +
                                      " Count(ID) " +
                                      "FROM " + _partName + ".ParametersLog " +
                                      "WHERE EnglishName = @ParamName";
                _mySqlConnection =
                   new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    _mySqlCommand.Connection.Open();
                    var mySqlDataReader = _mySqlCommand.ExecuteReader();
                    mySqlDataReader.Read();
                    _currentParamRevision = Convert.ToInt32(mySqlDataReader[0].ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            else
            {
                #region Get Current Param Revision , BeginDate , EndDate , SelectString (XML)

                #region Prepare SqlCommand

            _commandText = "SELECT  " +
                                         " SelectString " +
                                         "FROM " + _partName + ".ParametersLog " +
                                         "WHERE EnglishName = @ParamName and Revision=@Rev ORDER BY StartDate  DESC";
                 _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;
                _mySqlCommand.Parameters.Add("@Rev", SqlDbType.Int);
                _mySqlCommand.Parameters["@Rev"].Value = rev;

                #endregion

                #region Execute SqlCommand

                try
                {
                    _mySqlCommand.Connection.Open();
                    var mySqlDataReader = _mySqlCommand.ExecuteReader();
                    mySqlDataReader.Read();
 
                    _currentParamXml = mySqlDataReader[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            txtParamDescribe.Text = Classes.Parameters.GetParamDescription
                (_currentParamName, _dataTableParams);
        }

        #endregion

        #region void FillLogDataGrid()

        /// <summary>
        /// تمكیل آیتم های جدول سابقه تغییرات پارامتر
        /// </summary>
        private void FillLogDataGrid()
        {
            dgvLog.AutoGenerateColumns = false;
            _dataLogtable = new DataTable();

            #region Prepare SqlCommand

            _commandText =
                  "SELECT Revision  , " +
                  "CreationDate,UserID,Startdate,Enddate,'متن پارامتر' as Selectstring,Active   " +
                  "FROM " + _partName + ".ParametersLog " +
                  "WHERE EnglishName = @ParamName ORDER BY Active,StartDate  DESC";
            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
            _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 50);
            _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

            #endregion

            #region Execute SqlCommand

            try
            {
                _mySqlCommand.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _dataLogtable.Load(_mySqlCommand.ExecuteReader());
                dgvLog.DataSource = _dataLogtable;
                // ReSharper restore AssignNullToNotNullAttribute
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"خطا!");
            }
            finally
            {
                _mySqlCommand.Connection.Close();
            }

            #endregion

            #region Change Deactive revision backcolor
            for (int i = 0; i < dgvLog.Rows.Count; i++)
            {

                if ((bool)dgvLog.Rows[i].Cells[5].Value)
                {
                    dgvLog.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                }
            }
            #endregion
        }

        #endregion

        #region Boolean CheckDescriptionTextBox()

        /// <summary>
        /// بررسي جعبه متن توضيحات پارامتر
        /// </summary>
        private Boolean CheckDescriptionTextBox()
        {
            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }
            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #region Parameter Saving Validations


        private Boolean ParameterSavingValidations()
        {
            #region Check First Day of Month choose
            if ((PCreationDate.Text.Substring(8, 2)) != "01")
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            #endregion
            
            #region Boolean CheckDescriptionTextBox

            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }


            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }


            #endregion

            #region Check Same Date in DataBase
            #region Prepare SqlCommand

            _commandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _partName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName and StartDate=@StartDate and Active=0";
            _mySqlConnection =
               new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
            _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;
            _mySqlCommand.Parameters.Add("@StartDate", SqlDbType.NVarChar, 30);
            _mySqlCommand.Parameters["@StartDate"].Value = PCreationDate.Text;
            #endregion

            #region Execute SqlCommand

            try
            {
                _mySqlCommand.Connection.Open();
                SqlDataReader mySqlDataReader = _mySqlCommand.ExecuteReader();
                mySqlDataReader.Read();
                _startDateCount = Convert.ToInt32(mySqlDataReader[0].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"خطا!");
            }
            finally
            {
                _mySqlCommand.Connection.Close();
            }

            #endregion

            if (_startDateCount > 0)
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار تکراری می باشد.باید ابتدا ویرایش موجود غیر فعال شود !", "خطا",
           MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            #endregion

            return true;
        }
        #endregion

        #region void CheckParamBeActive()
        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void CheckParamBeActive()
        {

         
         

                #region Prepare SqlCommand

                _commandText = "SELECT Count(revision) " +
                                "FROM " + _partName + ".ParametersLog " +
                                    "WHERE EnglishName = @ParamName and active=0";
                _mySqlConnection =
                     new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

                #endregion

                #region Execute SqlCommand

          
            try
                {
                    _mySqlCommand.Connection.Open();
                    var mySqlDataReader = _mySqlCommand.ExecuteReader();
                    mySqlDataReader.Read();

                    _activeCount = mySqlDataReader[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }
          

            #endregion





        }

        #endregion
        
        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var parameterResult = new DataTable();

            var mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            var mySqlCommand = new SqlCommand(query, mySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                mySqlCommand.Connection.Open();

                // ReSharper disable AssignNullToNotNullAttribute
                parameterResult.Load(mySqlCommand.ExecuteReader());
                // ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("در دستور وارد شده خطایی وجود دارد!" +
                                           "كدهای وارد شده را مجددا بررسی نمایید", strMessage, "Error").ShowDialog();
                mySqlCommand.Connection.Close();
                return false;
            }
            finally
            {
                mySqlCommand.Connection.Close();
            }
            #endregion
            return true;

        }
        #endregion

        
  #endregion

        private void FrmModifyParametersCHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "ModifyParametersC.htm");
        }

        

    }
}