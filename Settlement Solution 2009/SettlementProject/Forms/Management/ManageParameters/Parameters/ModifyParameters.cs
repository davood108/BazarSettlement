﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    /// <summary>
    /// فرم ویرایش پارامتر های ثبت شده
    /// </summary>
    public partial class FrmModifyParameters : Form
    {

        #region Fields

        #region readonly String _CurrentParamName
        /// <summary>
        /// نام پارامتری كه در حال ویرایش است
        /// </summary>
        private readonly String _currentParamName;
        #endregion

        #region Int32 _CurrentParamRevision
        /// <summary>
        /// شماره آخرین پارامتر ثبت شده
        /// </summary>
        private Int32 _currentParamRevision;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش تعیین شده
        /// </summary>
        private readonly String _partName;
        #endregion

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر ها
        /// </summary>
        private DataTable _dataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _arrayListParams;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر
        /// </summary>
        private readonly String _paramType;

        private int _step;
        private string _testingQuery;

#pragma warning disable 649
        private readonly string _userID;
        private string _cdate;
#pragma warning restore 649

        #endregion

        private DataTable _dataLogtable;
        private readonly DbSMDataContext _dbSM;
        private string _commandText;
        private SqlConnection _mySqlConnection;
        private SqlCommand _mySqlCommand;
        private bool TestState;
        private int _startDateCount;
        private bool _testState;
        private string _h;
        private string _activeCount;

        private const int CpNocloseButton = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                var myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CpNocloseButton;
                return myCp;
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="paramName">نام پارامتری كه در حال ویرایش است</param>
        /// <param name="partName">نام بخش در حال ویرایش</param>
        /// <param name="paramType">نوع پارامتر</param>
        public FrmModifyParameters(String paramName, String partName , String paramType)
        {
            InitializeComponent();
            _dbSM = new DbSMDataContext
                            (DbBizClass.DbConnStr);

            _userID = DbBizClass.CurrentUserLastName;

            #region Set Form Title
            if (partName == "Sellers")
                lblTitle.Text = @"فروشندگان";
            else if (partName == "Buyers")
                lblTitle.Text = @"خریداران";
            else if (partName == "LineTransfer")
                lblTitle.Text = @"خدمات انتقال";
            #endregion

            _currentParamName = paramName;
            _partName = partName;
            _paramType = paramType;

            #region SetTime
            var occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            PCreationDate.Text = occuredDate.ToString();
            #endregion

            btnCancel.DialogResult = DialogResult.None;
            
            ShowDialog();
        }
        #endregion

        #region Events Handlers

        #region Form Load

        private void FormLoad(object sender, EventArgs e)
        {
            #region Set RiboonFunction
            switch (_partName)
            {
                case "Sellers":

                    break;
                case "Buyers":

                    btnSum1.Text = @"مجموع بر اساس ساعت";
                    btnSum2.Text = @"مجموع بر اساس تاریخ";
                    btnSum3.Text = @"مجموع بر اساس نوع شرکت";
                    btnSum4.Visible = false;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    break;
                case "Transfer":
                    btnSum1.Text = @"مجموع بر اساس سطح ولتاژ";
                    btnSum2.Text = @"مجموع بر اساس فوق توزیع و انتقال";
                    btnSum3.Text = @"مجموع بر اساس ماه";
                    btnSum4.Visible = false;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    break;
            }
            #endregion 

            FillParametersTable();
            SetParamLastDataByName("F",10000);

            #region Prepare lstParameters
            lstParameters.DataSource = _arrayListParams;
            lstParameters.SelectedIndex = 0;
            #endregion

            SetRtbCodeMembers();
            FillLogDataGrid();
        }

        #endregion

        #region lstParameters Event Handlers

        #region DoubleClick

        private void lstParameters_DoubleClick(object sender, MouseEventArgs e)
        {
            rtbCode.SelectedText = lstParameters.SelectedItem.ToString();
            rtbCode.Focus();
        }

        #endregion

        #region SelectedIndexChanged

        /// <summary>
        /// اين رويه مقادير توضيحات هر پارامتر را نمايش مي دهد
        /// </summary>
        private void lstParameters_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDescription.Text = Classes.Parameters.GetParamDescription
                (lstParameters.SelectedItem.ToString(), _dataTableParams);
        }

        #endregion

        #endregion
        
        #region Toolbar Buttons

        #region btnNew_Click

        private void btnNew_Click(object sender, EventArgs e)
        {
            rtbCode.Clear();
            rtbCode.Focus();
        }

        #endregion

        #region btnOpenFml_Click

        private void btnOpenFml_Click(object sender, EventArgs e)
        {
            var MyOpenFileDialog = new OpenFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = "txt Files|*.txt|All Files|*.*",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           Multiselect = false,
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           ShowReadOnly = false,
                                           ReadOnlyChecked = false,
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = "خواندن فرمول از فایل"
                                       };

            // Determine whether the user selected a file from the OpenFileDialog.
            if (MyOpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                MyOpenFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.LoadFile(MyOpenFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnSaveFml_Click

        private void btnSaveFml_Click(object sender, EventArgs e)
        {
            var MySaveFileDialog = new SaveFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = "txt Files|*.txt",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = "ذخیره فرمول در فایل"
                                       };
            MySaveFileDialog.ShowDialog();

            // Determine whether the user selected a file from the SaveFileDialog.
            if (MySaveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                MySaveFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.SaveFile(MySaveFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnCutTextFromFml_Click

        private void btnCutTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Cut();
            rtbCode.Focus();
        }

        #endregion

        #region btnCopyTextFromFml_Click

        private void btnCopyTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Copy();
            rtbCode.Focus();
        }

        #endregion

        #region btnPasteTextToFml_Click

        private void btnPasteTextToFml_Click(object sender, EventArgs e)
        {
            rtbCode.Paste();
            rtbCode.Focus();
        }

        #endregion

        

        

        #region btnFkMax_Click

        private void BtnFkMaxClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Max( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnFkMin_Click

        private void BtnFkMinClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Min( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnMin_Click

        private void BtnMinClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Min() ";
            rtbCode.Focus();
        }


        #endregion

        #region btnMax_Click


        private void BtnMaxClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Max() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSum_Click

        private void BtnOpSumClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "+ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSubstract_Click

        private void BtnOpSubstractClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "- ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpMultiplation_Click

        private void BtnOpMultiplationClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "* ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpDevide_Click

        private void BtnOpDevideClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "/ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSqrt_Click

        private void BtnSqrtClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sqrt() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnAbs_Click

        private void BtnAbsClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Abs() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnPower_Click

        private void BtnPowerClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Power( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region Sum Buttons

        #region btnSumByUnitType_Click (Sum1)

        private void BtnSumByUnitTypeClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum1( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByPowerStation_Click (Sum2)

        private void BtnSumByPowerStationClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum2( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompany_Click (Sum3)

        private void BtnSumByCompanyClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum3( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompanyType_Click (Sum4)

        private void BtnSumByCompanyTypeClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum4( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByRegion_Click (Sum5)

        private void BtnSumByRegionClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum5( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByDay_Click (Sum6)

        private void BtnSumByDayClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum6( ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByMonth_Click (Sum7)

        private void BtnSumByMonthClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum7( ) ";
            rtbCode.Focus();
        }

        #endregion

        #endregion

        #endregion

        #region dgvLog_CellMouseClick

        /// <summary>
        /// روال مدیریت كلیك بر روی جدول جزئیات
        /// </summary>
        private void DgvLogCellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {


            SetParamLastDataByName("E", Convert.ToInt16(dgvLog.Rows[e.RowIndex].Cells[0].Value));
          
        //    return true;
        }

        #endregion

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void BtnValidationClick(object sender, EventArgs e)
        {

            _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }

            #region Step Type

            string fg = rtbCode.Text;
            if (fg.Length > 14)
            {
                if (fg.Substring(0, 14) == "Functions.Step")
                {

                    _step = 1;
                }
                else _step = fg.Substring(0, 13) == "Functions.Ava" ? 2 : 0;

            }
            #endregion
      
            #region Power-Step
            if (_step == 1)
            {
               string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                _h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw1),0)* [Sellers].[PwPrP].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    _h = _h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
        


            }
            #endregion

            #region Ava - step 2
            if (_step == 2)
            {

                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));

                string h11 = h1[0].Substring(14);
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                 _h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + lblCurrentParamName.Text + "'";


                {
                    return;
                }
            }
            #endregion

            #region Non-Step
            if (_step != 1 && _step != 2)
            {
                _h = rtbCode.Text;
            }

            #endregion

            _testingQuery = Classes.ManageParameters.
               GetParamString(_partName, _currentParamName, _h, _dataTableParams, _arrayListParams, true, 0, _step, rtbCode.Text);
            if (!String.IsNullOrEmpty(_testingQuery)) new frmTestParameters(_testingQuery);
        }
        #endregion

        #region btnSaveAndClose_Click

        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void BtnSaveAndCloseClick(object sender, EventArgs e)
        {

            _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }

            
            #region Get Current Param Revision

            #region Prepare SqlCommand

            _commandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _partName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName";
            _mySqlConnection =
               new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
            _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

            #endregion

            #region Execute SqlCommand

            try
            {
                _mySqlCommand.Connection.Open();
                SqlDataReader MySqlDataReader = _mySqlCommand.ExecuteReader();
                if (MySqlDataReader != null)
                {
                    MySqlDataReader.Read();
                    _currentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());



                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                _mySqlCommand.Connection.Close();
            }

            #endregion

            #endregion

            #region Set Current Date
            var occuredDate1 = PersianDateConverter.ToPersianDate
(DateTime.Now);
            _cdate = occuredDate1.ToString();
            #endregion
            
            #region Step Type

            string fg = rtbCode.Text;
            if (fg.Length > 14)
            {
                if (fg.Substring(0, 14) == "Functions.Step")
                {

                    _step = 1;
                }
                else if (fg.Substring(0, 13) == "Functions.Ava")
                {

                    _step = 2;
                }
                else if (fg.Substring(0, 13) == "Functions.EPG")
                {

                    _step = 3;
                }
                else
                {

                    _step = 0;
                }

            }
            #endregion

            #region Power-Step

            #region Setrtb Text
            if (_step == 1)
            {
                string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                 _h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw0),0)* [Sellers].[PwPrP].Pr0";

                for (int y = 1; y <= 11; y++)
                {
                    _h = _h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
            #endregion
     
            }
            #endregion

            #region Ava-Step2

            if (_step == 2)
            {
                #region rtb Text
                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));

                string h11 = h1[0].Substring(14);
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                 _h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + lblCurrentParamName.Text + "'";
                #endregion
                
            }
            #endregion

            #region Power-Step EPG
            if (_step == 3)
            {
                #region Step3

                string h1 = rtbCode.Text.Substring(14, rtbCode.TextLength - 15);
                 _h = "Functions.Max(Functions.Min(" + h1 + "-((RequiredValue-ULmw+OCmw)*(1-LossCo)),([Sellers].[FL_EPG].Pw0)-((RequiredValue-ULmw+OCmw)*(1-LossCo))),0)* [Sellers].[FL_EPG].Pr0";

                for (int y = 1; y <= 11; y++)
                {
                    _h = _h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[FL_EPG].Pw" + (y - 1) + ",[Sellers].[FL_EPG].Pw" + y + "-[Sellers].[FL_EPG].Pw" + (y - 1) + "),0)* [Sellers].[FL_EPG].Pr" + y + "";
                }

                #endregion

                
                
            }
            #endregion

            #region Non-Step
            if (_step ==0)
            {
                #region Step 0

                _h = rtbCode.Text;
                #endregion

                
            }
            #endregion

            #region Testing
            _testingQuery = Classes.ManageParameters.
                         GetParamString(_partName, _currentParamName, _h, _dataTableParams, _arrayListParams, true, 0, _step, rtbCode.Text);

            if (!String.IsNullOrEmpty(_testingQuery))
                TestState = CheckTestingQuery(_testingQuery);
            if (TestState == false)
            {

                return;
            }
            #endregion

            #region Create Table

            Classes.ManageParameters.CreateParameterTable(_dataTableParams, _arrayListParams, lblCurrentParamName.Text, txtParamDescribe.Text, _currentParamRevision, _h, _userID, _partName, _paramType, _step, rtbCode.Text, PCreationDate.Text, _cdate); 
            #endregion
    
            FillLogDataGrid();
        }

        #endregion

        #region btn Deactive
        private void BtnDeActiveClick(object sender, EventArgs e)
        {
            if((bool) dgvLog.SelectedRows[0].Cells[5].Value)
            {
               return; 
            }


            var qResult =
       PersianMessageBox.Show("آيا از اين كار اطمينان داريد؟\n " +
                              "در صورت تایید ، این ویرایش از متغیر دیگر قابل دسترس نخواهد بود!", "هشدار!",
                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (qResult == DialogResult.No)
            {
                return;
            }


            _dbSM.SP_ParamDeactivation(Convert.ToInt16(dgvLog.SelectedRows[0].Cells[0].Value), _currentParamName, _partName);
         FillLogDataGrid();
        }
        #endregion

        #region btnCancel Click

        /// <summary>
        /// دستگیره مدیریت انصراف از تغییرات
        /// </summary>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            CheckParamBeActive();


            if (Convert.ToInt32(_activeCount) == 0)
            {
                PersianMessageBox.Show("هر پارامتر حداقل باید یک ویرایش فعال داشته باشد\n " +
                                           "لطفا یک ویرایش فعال ایجاد و سپس از این صفحه خارج شوید!", "خطا",
                      MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }
      
            Dispose();
        }

        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()

        /// <summary>
        /// بدست آوردن جدول و ليست پارامتر ها از بانك اطلاعات
        /// </summary>
        private void FillParametersTable()
        {
            _dataTableParams = Classes.Parameters.GetAllParamsByPart(_partName);
            _arrayListParams =
                Classes.Parameters.GetParamsNameByTable(_dataTableParams);
            _arrayListParams.Remove(_currentParamName);
        }

        #endregion

        #region void SetParamLastDataByName()

        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void SetParamLastDataByName(string param, int Rev)
        {
            lblCurrentParamName.Text = _currentParamName;
            if (param == "F")
            {

                #region Select String

                #region Prepare SqlCommand

                 _commandText = "SELECT " +
                                     " SelectString " +
                                     "FROM " + _partName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName and active=0  ORDER BY StartDate  DESC"; //remove:and enddate is null. 1395/01/17
                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                 _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    _mySqlCommand.Connection.Open();
                    var mySqlDataReader = _mySqlCommand.ExecuteReader();
                    if (mySqlDataReader != null)
                    {
                        mySqlDataReader.Read();
                        //_CurrentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());


                        rtbCode.Text = mySqlDataReader[0].ToString();
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }

                #endregion

                #endregion

                #region Get Current Param Revision 

                #region Prepare SqlCommand

               _commandText = "SELECT " +
                                     " Count(ID) " +
                                     "FROM " + _partName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName";
                 _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
               _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    _mySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = _mySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();
                       _currentParamRevision = Convert.ToInt32(MySqlDataReader[0].ToString());


                 
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            else
            {
                #region Get Current Param Revision , BeginDate , EndDate , Select String

                #region Prepare SqlCommand

              _commandText = "SELECT  " +
                                     " SelectString " +
                                     "FROM " + _partName + ".ParametersLog " +
                                     "WHERE EnglishName = @ParamName and Revision=@Rev ORDER BY StartDate  DESC";
             _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;
                _mySqlCommand.Parameters.Add("@Rev", SqlDbType.Int);
                _mySqlCommand.Parameters["@Rev"].Value = Rev;

                #endregion

                #region Execute SqlCommand

                try
                {
                    _mySqlCommand.Connection.Open();
                    SqlDataReader MySqlDataReader = _mySqlCommand.ExecuteReader();
                    if (MySqlDataReader != null)
                    {
                        MySqlDataReader.Read();



                        rtbCode.Text = MySqlDataReader[0].ToString();
                    }
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }

                #endregion

                #endregion
            }
            txtParamDescribe.Text =
                Classes.Parameters.GetParamDescription(_currentParamName, _dataTableParams);
        }

        #endregion

        #region void SetRtbCodeMembers()

        /// <summary>
        /// تنظيم عناصر قابل كامپايل در جعبه متن
        /// </summary>
        private void SetRtbCodeMembers()
        {

            #region Set Keywords
            rtbCode.CompilerSettings.Keywords.Add("Max");
            rtbCode.CompilerSettings.Keywords.Add("Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Max");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Step");
            rtbCode.CompilerSettings.Keywords.Add("Functions.EPG");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Ava");
            rtbCode.CompilerSettings.Keywords.Add("Sqrt");
            rtbCode.CompilerSettings.Keywords.Add("Abs");
            rtbCode.CompilerSettings.Keywords.Add("Power");
            rtbCode.CompilerSettings.Keywords.Add("Sum1");
            rtbCode.CompilerSettings.Keywords.Add("Sum2");
            rtbCode.CompilerSettings.Keywords.Add("Sum3");
            rtbCode.CompilerSettings.Keywords.Add("Sum4");
            rtbCode.CompilerSettings.Keywords.Add("Sum5");
            rtbCode.CompilerSettings.Keywords.Add("Sum6");
            rtbCode.CompilerSettings.Keywords.Add("Sum7");
            #endregion

            #region Set Parameters Keyword
            foreach (String Str in _arrayListParams)
                rtbCode.CompilerSettings.Keywords.Add(Str);
            #endregion

            #region Set Compiler Settings
            // تنظيم رنگ پارامتر ها:
            rtbCode.CompilerSettings.KeywordColor = Color.Blue ;
            rtbCode.CompilerSettings.IntegerColor = Color.Red;
            // تنظيم خواص كلاس:
            rtbCode.CompilerSettings.EnableStrings = true;
            rtbCode.CompilerSettings.EnableIntegers = true;
            // كامپايل كلمات كليدي:
            rtbCode.CompileKeywords();
            // بررسي خطوط برنامه
            rtbCode.ProcessAllLines();
            #endregion

        }

        #endregion

        #region void FillLogDataGrid()

        /// <summary>
        /// تمكیل آیتم های جدول سابقه تغییرات پارامتر
        /// </summary>
        private void FillLogDataGrid()
        {
            dgvLog.AutoGenerateColumns = false;
            _dataLogtable=new DataTable();
            try
            {

                #region Prepare SqlCommand

                _commandText =
                    "SELECT Revision  , " +
                    "CreationDate,UserID,Startdate,Enddate,'متن پارامتر' as Selectstring,Active   " +
                    "FROM " + _partName + ".ParametersLog " +
                    "WHERE EnglishName = @ParamName ORDER BY Active,StartDate  DESC";
                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
                _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 50);
                _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

                #endregion

                #region Execute SqlCommand

                try
                {
                    _mySqlCommand.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _dataLogtable.Load(_mySqlCommand.ExecuteReader());
                    dgvLog.DataSource = _dataLogtable;
                    // ReSharper restore AssignNullToNotNullAttribute
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"خطا!");
                }
                finally
                {
                    _mySqlCommand.Connection.Close();
                }

                #endregion

                #region Change Deactive revision backcolor

                for (int i = 0; i < dgvLog.Rows.Count; i++)
                {

                    if ((bool) dgvLog.Rows[i].Cells[5].Value)
                    {
                        dgvLog.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                    }
                }

                #endregion
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion
        
        #region Parameter Saving Validations


        private Boolean ParameterSavingValidations()


        {


            #region Check First Day of Month choose
            if ((PCreationDate.Text.Substring(8, 2)) != "01")
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            #endregion

            #region Check Same Date in DataBase
            #region Prepare SqlCommand

            _commandText = "SELECT " +
                                  " Count(ID) " +
                                  "FROM " + _partName + ".ParametersLog " +
                                  "WHERE EnglishName = @ParamName and StartDate=@StartDate and Active=0";
            _mySqlConnection =
               new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
            _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;
            _mySqlCommand.Parameters.Add("@StartDate", SqlDbType.NVarChar, 30);
            _mySqlCommand.Parameters["@StartDate"].Value = PCreationDate.Text;
            #endregion

            #region Execute SqlCommand

            try
            {
                _mySqlCommand.Connection.Open();
                var mySqlDataReader = _mySqlCommand.ExecuteReader();
                mySqlDataReader.Read();
                _startDateCount = Convert.ToInt32(mySqlDataReader[0].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"خطا!");
            }
            finally
            {
                _mySqlCommand.Connection.Close();
            }

            #endregion

            if (_startDateCount > 0)
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار تکراری می باشد.باید ابتدا ویرایش موجود غیر فعال شود !", "خطا",
           MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            #endregion
            
            #region Param Name Is Empty
            if (lblCurrentParamName.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر نامي به انگليسي وارد نماييد!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lblCurrentParamName.Focus();
                return false;
            }
            #endregion

            #region Param Name Lenght Less Than 3 Charecter
            if (lblCurrentParamName.Text.Length < 3)
            {
                PersianMessageBox.Show("نام پارامتر بايد حداقل داراي 3 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                lblCurrentParamName.Focus();
                return false;
            }
            #endregion

            #region Boolean CheckDescriptionTextBox

            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }


            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }


            #endregion

            return true;
        }
        #endregion

        #region void CheckParamBeActive()
        /// <summary>
        /// بدست آوردن اطلاعات آخرین نسخه پارامتر
        /// </summary>
        private void CheckParamBeActive()
        {




            #region Prepare SqlCommand

            _commandText = "SELECT Count(revision) " +
                            "FROM " + _partName + ".ParametersLog " +
                                "WHERE EnglishName = @ParamName and active=0";
            _mySqlConnection =
                 new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand = new SqlCommand(_commandText, _mySqlConnection);
            _mySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            _mySqlCommand.Parameters["@ParamName"].Value = _currentParamName;

            #endregion

            #region Execute SqlCommand


            try
            {
                _mySqlCommand.Connection.Open();
                var mySqlDataReader = _mySqlCommand.ExecuteReader();
                mySqlDataReader.Read();

                _activeCount = mySqlDataReader[0].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"خطا!");
            }
            finally
            {
                _mySqlCommand.Connection.Close();
            }


            #endregion





        }

        #endregion

        
        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var parameterResult = new DataTable();

            var mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            var mySqlCommand = new SqlCommand(query, mySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                mySqlCommand.Connection.Open();

                // ReSharper disable AssignNullToNotNullAttribute
                parameterResult.Load(mySqlCommand.ExecuteReader());
                // ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("در دستور وارد شده خطایی وجود دارد!" +
                                           "كدهای وارد شده را مجددا بررسی نمایید", strMessage, "Error").ShowDialog();
                mySqlCommand.Connection.Close();
                return false;
              
            }
            finally
            {
                mySqlCommand.Connection.Close();
            }
            #endregion
            return true;

        }
        #endregion


        #endregion

        private void FrmModifyParametersHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "ModifyParameters.htm");
        }

    }
}