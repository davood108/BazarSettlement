﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.Forms.Management.ManageParameters.Classes;
using MohanirVBClasses;
using RPNCalendar.Utilities;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Parameters
{
    /// <summary>
    /// فرم تعریف پارامتر جدید
    /// </summary>
    public partial class FrmNewParameter : Form
    {

        #region Fields

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول پارامتر ها
        /// </summary>
        private DataTable _dataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// نام پارامتر ها
        /// </summary>
        private ArrayList _arrayListParams;
        #endregion

        #region readonly String _PartName
        /// <summary>
        /// نام بخش مورد جستجو
        /// </summary>
        private readonly String _partName;
        #endregion

        #region readonly String _ParamType
        /// <summary>
        /// نوع پارامتر مورد نظر
        /// </summary>
        private readonly String _paramType;

        private int _step;
        private string _testingQuery;

        private readonly string _userID;
        private string _cdate;
        private bool _testState;
        private string _fg;
        private string _h;

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        /// <param name="partName">نام بخش از قبیل فروشندگان ، خریداران و غیره</param>
        /// <param name="paramType">نوع پارامتر از قبیل عادی و غیره</param>
        public FrmNewParameter(String partName, String paramType)
        {
             InitializeComponent();
            _userID = DbBizClass.CurrentUserLastName;

            #region Set Form Title
            switch (partName)
            {
                case "Sellers":
                    lblTitle.Text = @"فروشندگان";
                    break;
                case "Buyers":
                    lblTitle.Text = @"خریداران";
                    break;
                case "Transfer":
                    lblTitle.Text = @"خدمات انتقال";
                    break;
            }
            #endregion

            #region SetTime
            PersianDate occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);


            PCreationDate.Text = occuredDate.ToString();


            #endregion

            _partName = partName;
            _paramType = paramType;
        }
        #endregion

        #region Events Handlers

        #region Form Load
        private void FormLoad(object sender, EventArgs e)
        {
            #region Set RiboonFunction
            switch (_partName)
            {
                case "Sellers":
                   
                    break;
                case "Buyers":

                    btnSum1.Text = @"مجموع بر اساس ساعت";
                    btnSum2.Text = @"مجموع بر اساس تاریخ";
                    btnSum3.Text = @"مجموع بر اساس نوع شرکت";
                    btnSum4.Visible = false ;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    break;
                case "Transfer":
                    btnSum1.Text = @"مجموع بر اساس سطح ولتاژ";
                    btnSum2.Text = @"مجموع بر اساس فوق توزیع و انتقال";
                    btnSum3.Text = @"مجموع بر اساس ماه";
                    btnSum4.Visible = false;
                    btnSum5.Visible = false;
                    btnSum6.Visible = false;
                    btnSum7.Visible = false;
                    
                    break;
            }
            #endregion 
   
            FillParametersTable();

            #region Prepare lstParams
            lstParameters.DataSource = _arrayListParams;
            lstParameters.SelectedIndex = 0;
            #endregion
            
            SetRtbCodeMembers();
            SetParamsNameAutoComplete();
        }
        #endregion

        #region lstParameters Event Handlers

        #region lstParameters_DoubleClick

        private void LstParametersDoubleClick(object sender, MouseEventArgs e)
        {
            rtbCode.SelectedText = lstParameters.SelectedItem.ToString();
            rtbCode.Focus();
        }

        #endregion

        #region lstParameters_SelectedIndexChanged

        /// <summary>
        /// اين رويه مقادير توضيحات هر پارامتر را نمايش مي دهد
        /// </summary>
        private void LstParametersSelectedIndexChanged(object sender, EventArgs e)
        {
            lblDescription.Text = Classes.Parameters.GetParamDescription
                (lstParameters.SelectedItem.ToString(), _dataTableParams);
        }

        #endregion

        #endregion

        #region Toolbar Buttons

        #region btnNew_Click

        private void BtnNewClick(object sender, EventArgs e)
        {
            rtbCode.Clear();
            rtbCode.Focus();
        }

        #endregion

        #region btnOpenFml_Click

        private void BtnOpenFmlClick(object sender, EventArgs e)
        {
            var myOpenFileDialog = new OpenFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = @"txt Files|*.txt|All Files|*.*",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           Multiselect = false,
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           ShowReadOnly = false,
                                           ReadOnlyChecked = false,
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = @"خواندن فرمول از فایل"
                                       };

            // Determine whether the user selected a file from the OpenFileDialog.
            if (myOpenFileDialog.ShowDialog() == DialogResult.OK &&
                myOpenFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.LoadFile(myOpenFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnSaveFml_Click

        private void BtnSaveFmlClick(object sender, EventArgs e)
        {
            var mySaveFileDialog = new SaveFileDialog
                                       {
                                           CheckFileExists = true,
                                           CheckPathExists = true,
                                           Filter = @"txt Files|*.txt",
                                           FilterIndex = 1,
                                           DefaultExt = "txt Files",
                                           DereferenceLinks = true,
                                           InitialDirectory = @"C:\",
                                           RestoreDirectory = true,
                                           ShowHelp = false,
                                           ValidateNames = true,
                                           SupportMultiDottedExtensions = true,
                                           Title = @"ذخیره فرمول در فایل"
                                       };
            mySaveFileDialog.ShowDialog();

            // Determine whether the user selected a file from the SaveFileDialog.
            if (mySaveFileDialog.ShowDialog() == DialogResult.OK &&
                mySaveFileDialog.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                rtbCode.SaveFile(mySaveFileDialog.FileName,
                                 RichTextBoxStreamType.PlainText);
            }
            rtbCode.Focus();
        }

        #endregion

        #region btnCutTextFromFml_Click

        private void BtnCutTextFromFmlClick(object sender, EventArgs e)
        {
            rtbCode.Cut();
            rtbCode.Focus();
        }

        #endregion

        #region btnCopyTextFromFml_Click

        private void BtnCopyTextFromFmlClick(object sender, EventArgs e)
        {
            rtbCode.Copy();
            rtbCode.Focus();
        }

        #endregion

        #region btnPasteTextToFml_Click

        private void BtnPasteTextToFmlClick(object sender, EventArgs e)
        {
            rtbCode.Paste();
            rtbCode.Focus();
        }

        #endregion
        
        #region btnFkMax_Click

        private void BtnFkMaxClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Max( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnMin_Click

        private void BtnMinClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Min() ";
            rtbCode.Focus();
        }
        

        #endregion

        #region btnMax_Click


        private void BtnMaxClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Max() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnFkMin_Click

        private void BtnFkMinClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Min( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSum_Click

        private void BtnOpSumClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "+ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpSubstract_Click

        private void BtnOpSubstractClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "- ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpMultiplation_Click

        private void BtnOpMultiplationClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "* ";
            rtbCode.Focus();
        }

        #endregion

        #region btnOpDevide_Click

        private void BtnOpDevideClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "/ ";
            rtbCode.Focus();
        }

        #endregion

        #region btnSqrt_Click

        private void BtnSqrtClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sqrt() ";
            rtbCode.Focus();
        }

        #endregion

        #region btnAbs_Click

        private void BtnAbsClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Abs()";
            rtbCode.Focus();
        }

        #endregion

        #region btnPower_Click

        private void BtnPowerClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Power( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #region btnStep Click
        private void BtnStepClick(object sender, EventArgs e)
        {
            _step = 1;
            rtbCode.SelectedText = "Functions.Step()";
            rtbCode.Focus();
        }
        #endregion 

        #region btnStepEPG_Click
        private void BtnStepEpgClick(object sender, EventArgs e)
        {
            _step = 3;
            rtbCode.SelectedText = "Functions.EPG()";
            rtbCode.Focus();
        }
        #endregion

        #region btnAva Click
        private void BtnAvaClick(object sender, EventArgs e)
        {
            _step = 2;
            rtbCode.SelectedText = "Functions.Ava(,)";
            rtbCode.Focus();
        }
        #endregion 

        #region BtnAmaLClick
        private void BtnAmaLClick(object sender, EventArgs e)
        {
            _step = 4;
            rtbCode.SelectedText = "Functions.AmaL( , )";
            rtbCode.Focus();
        }
        #endregion

        #region Sum Buttons

        #region btnSumByUnitType_Click (Sum1)

        private void BtnSumByUnitTypeClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum1()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByPowerStation_Click (Sum2)

        private void BtnSumByPowerStationClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum2()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompany_Click (Sum3)

        private void BtnSumByCompanyClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum3()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByCompanyType_Click (Sum4)

        private void BtnSumByCompanyTypeClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum4()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByRegion_Click (Sum5)

        private void BtnSumByRegionClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum9()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByDay_Click (Sum6)

        private void BtnSumByDayClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum6()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByMonth_Click (Sum7)

        private void BtnSumByMonthClick(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum7()";
            rtbCode.Focus();
        }

        #endregion

        #region btnSumByJustDate(Sum8)
        private void BtnSum8Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum8()";
            rtbCode.Focus();
        }
        #endregion

        #region btnSumByJustDateHour(Sum9)
        private void BtnSum9Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sum9()";
            rtbCode.Focus();
        }
        #endregion

        #endregion

        #endregion

        #region txtParamName KeyPress

        /// <summary>
        /// روالي براي بررسي عدم ورود كليد فاصله در نام پارامتر
        /// </summary>
        private void TxtParamNameKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(32))
            {
                e.Handled = true;
           }
            if (System.Text.Encoding.UTF8.GetByteCount(new char[] { e.KeyChar }) > 1)
            {
                e.Handled = true;
            }
            return;
        }

        #endregion

        #region btnValidation_Click
        /// <summary>
        /// دكمه بررسي صحت كد ها
        /// </summary>
        private void BtnValidationClick(object sender, EventArgs e)
        {
            _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }

            #region Step Type

           _fg = rtbCode.Text;
            if (_fg.Length > 14)
            {
                if (_fg.Substring(0, 14) == "Functions.Step")
                {

                    _step = 1;
                }
                else if (_fg.Substring(0, 13) == "Functions.Ava")
                {

                    _step = 2;
                }
                else if (_fg.Substring(0, 13) == "Functions.EPG")
                {

                    _step = 3;
                }
                else if (_fg.Substring(0, 14) == "Functions.AmaL")
                {

                    _step = 4;
                }
                else
                {

                    _step = 0;
                }
            }
            #endregion

            #region Power-Step
            if (_step == 1)
            {
               string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                _h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw1),0)* [Sellers].[PwPrP].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    _h = _h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
              


            }
            #endregion
            
            #region Ava - step 2
            if (_step== 2)
            {

                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));

                string h11 = h1[0].Substring(14);
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                _h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + txtParamName.Text + "'";


            }
            #endregion

            #region Power-StepEPG
            if (_step == 3)
            {
                string h1 = rtbCode.Text.Substring(14, rtbCode.TextLength - 15);
                _h = "Functions.Max(Functions.Min(" + h1 +
                           "-RequiredValue,([Sellers].[FL_EPG].Pw1)-RequiredValue),0)* [Sellers].[FL_EPG].Pr1";

                for (int y = 2; y <= 11; y++)
                {
                    _h = _h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[FL_EPG].Pw" + (y - 1) + ",[Sellers].[FL_EPG].Pw" + y + "-[Sellers].[FL_EPG].Pw" +
                        (y - 1) + "),0)* [Sellers].[FL_EPG].Pr" + y + "";
                }

            }

            #endregion

            #region AmaL - step 4
            if (_step == 4)
            {

                string[] N1 = rtbCode.Text.Split(Convert.ToChar(","));

                string N11 = N1[0].Substring(15);
                string N12 = N1[1];
                string N13 = N1[2];
                string N14 = N1[3].Substring(0,N1[3].Length-1);
                _h = "exec Sellers.SP_AmaL " + "'SellersParam." + N11 + "','SellersParam." + N12 + "'" + ",'SellersParam." + txtParamName.Text + "'";


            }
            #endregion

            #region Step 0
            if (_step == 0)
            {


                _h = rtbCode.Text;

            }
            #endregion

            _testingQuery = Classes.ManageParameters.GetParamString(_partName, txtParamName.Text, _h, _dataTableParams, _arrayListParams, true, 0, _step, rtbCode.Text);
            if (!String.IsNullOrEmpty(_testingQuery)) new frmTestParameters(_testingQuery);
            
   

        }
        #endregion

        #region btnSaveAndClose_Click
        /// <summary>
        /// دكمه ي بررسي و ذخيره سازي پارامتر در بانك اطلاعات
        /// </summary>
        private void BtnSaveAndCloseClick(object sender, EventArgs e)
        {

           _testState = ParameterSavingValidations();
            if (_testState == false)
            {

                return;
            }

            #region Set Current Date
            PersianDate occuredDate1 = PersianDateConverter.ToPersianDate
            (DateTime.Now);
            _cdate = occuredDate1.ToString();
            #endregion

            #region Step Type

            string fg = rtbCode.Text;
            if (fg.Length > 14)
            {
                if (fg.Substring(0, 14) == "Functions.Step")
                {

                    _step = 1;
                }
                else if (fg.Substring(0, 13) == "Functions.Ava")
                {

                    _step = 2;
                }
                else if (fg.Substring(0, 13) == "Functions.EPG")
                {

                    _step = 3;
                }
                else if (fg.Substring(0, 14) == "Functions.AmaL")
                {

                    _step = 4;
                }
                else
                {

                    _step = 0;
                }
            }
            #endregion

            #region Power-Step
            if (_step == 1)
            {

                #region Set rtbText

                string h1 = rtbCode.Text.Substring(15, rtbCode.TextLength - 16);
                _h = "Functions.Max(Functions.Min(" + h1 + ",[Sellers].[PwPrP].Pw0),0)* [Sellers].[PwPrP].Pr0";

                for (int y = 1; y <= 11; y++)
                {
                    _h = _h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[PwPrP].Pw" + (y - 1) + ",[Sellers].[PwPrP].Pw" + y + "-[Sellers].[PwPrP].Pw" + (y - 1) + "),0)* [Sellers].[PwPrP].Pr" + y + "";
                }
                #endregion

            
            }
            #endregion

            #region Ava-Step2
            if (_step==2)
            {
                #region Set rtbText
                string[] h1 = rtbCode.Text.Split(Convert.ToChar(","));
           
                string h11 = h1[0].Substring(14);  
                string h12 = h1[1].Substring(0, h1[1].Length - 1);

                 _h = "exec Sellers.SP_LAva " + "'SellersParam." + h11 + "','SellersParam." + h12 + "'" + ",'SellersParam." + txtParamName.Text + "'";

                #endregion

              
            }
            #endregion

            #region Power-Step EPG
            if (_step == 3)
            {
                #region Step-EPG

                string h1 = rtbCode.Text.Substring(14, rtbCode.TextLength - 15);
                _h = "Functions.Max(Functions.Min(" + h1 + "-((RequiredValue-ULmw+OCmw)*(1-LossCo)),([Sellers].[FL_EPG].Pw0)-((RequiredValue-ULmw+OCmw)*(1-LossCo))),0)* [Sellers].[FL_EPG].Pr0";

                for (int y = 1; y <= 11; y++)
                {
                    _h = _h + "+Functions.Max(Functions.Min(" + h1 +
                        "-[Sellers].[FL_EPG].Pw" + (y - 1) + ",[Sellers].[FL_EPG].Pw" + y + "-[Sellers].[FL_EPG].Pw" + (y - 1) + "),0)* [Sellers].[FL_EPG].Pr" + y + "";
                }



                #endregion

                }
            #endregion

            #region AmaL - step 4
            if (_step == 4)
            {

                string[] N1 = rtbCode.Text.Split(Convert.ToChar(","));

                string N11 = N1[0].Substring(15);
                string N12 = N1[1].Substring(0, N1[1].Length - 1);
                //string N13 = N1[2];
                //string N14 = N1[3].Substring(0, N1[3].Length - 1);
                _h = "exec Sellers.SP_AmaL " + "'SellersParam." + N11 + "','SellersParam." + N12 + "'" + ",'SellersParam." + txtParamName.Text + "'";


            }
            #endregion
            
            #region Step 0
            if (_step == 0)
            {


                _h = rtbCode.Text;

        


            }
            #endregion
     
            #region testing
                _testingQuery = Classes.ManageParameters.GetParamString(_partName, txtParamName.Text, _h, _dataTableParams, _arrayListParams, true, 0, _step, rtbCode.Text);

                if (!String.IsNullOrEmpty(_testingQuery))
                    _testState = CheckTestingQuery(_testingQuery);
                if (_testState == false)
                {

                    return;
                }
#endregion

            #region Create Table

            Classes.ManageParameters.CreateParameterTable(
                _dataTableParams, _arrayListParams, txtParamName.Text, txtParamDescribe.Text, 0, _h, _userID,
                _partName, _paramType, _step, rtbCode.Text, PCreationDate.Text, _cdate);

            Dispose();

            #endregion


        }

        

        #endregion

        #region btnCancel Click

        /// <summary>
        /// روال انصراف از تغییر پارامتر
        /// </summary>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            Dispose();
        }

        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()
        /// <summary>
        /// بدست آوردن جدول و ليست پارامتر ها از بانك اطلاعات
        /// </summary>
        private void FillParametersTable()
        {
            _dataTableParams =
                Classes.Parameters.GetAllParamsByPart(_partName);
            _arrayListParams =
                Classes.Parameters.GetParamsNameByTable(_dataTableParams);
        }
        #endregion

        #region void SetRtbCodeMembers()
        /// <summary>
        /// تنظيم عناصر قابل كامپايل در جعبه متن
        /// </summary>
        private void SetRtbCodeMembers()
        {
            #region Set Keywords
            rtbCode.CompilerSettings.Keywords.Add("Max");
            rtbCode.CompilerSettings.Keywords.Add("Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Max");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Min");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Step");
            rtbCode.CompilerSettings.Keywords.Add("Functions.EPG");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Ava");
            rtbCode.CompilerSettings.Keywords.Add("Functions.AmaL");
            rtbCode.CompilerSettings.Keywords.Add("Sqrt");
            rtbCode.CompilerSettings.Keywords.Add("Abs");
            rtbCode.CompilerSettings.Keywords.Add("Power");
            rtbCode.CompilerSettings.Keywords.Add("Sum1");
            rtbCode.CompilerSettings.Keywords.Add("Sum2");
            rtbCode.CompilerSettings.Keywords.Add("Sum3");
            rtbCode.CompilerSettings.Keywords.Add("Sum4");
            rtbCode.CompilerSettings.Keywords.Add("Sum5");
            rtbCode.CompilerSettings.Keywords.Add("Sum6");
            rtbCode.CompilerSettings.Keywords.Add("Sum7");
            rtbCode.CompilerSettings.Keywords.Add("Sum8");
            #endregion

            #region Set Parameters Keyword
            foreach (String str in _arrayListParams)
                rtbCode.CompilerSettings.Keywords.Add(str);
            #endregion

            #region Set Compiler Settings
            // تنظيم رنگ پارامتر ها:
            rtbCode.CompilerSettings.KeywordColor = Color.Blue;
            rtbCode.CompilerSettings.IntegerColor = Color.Red;
            // تنظيم خواص كلاس:
            rtbCode.CompilerSettings.EnableStrings = true;
            rtbCode.CompilerSettings.EnableIntegers = true;
            // كامپايل كلمات كليدي:
            rtbCode.CompileKeywords();
            // بررسي خطوط برنامه
            rtbCode.ProcessAllLines();
            #endregion
        }
        #endregion

        #region void SetParamsNameAutoComplete()
        /// <summary>
        /// تنظيم عناصر پارامتر ها براي نام پارامتر
        /// </summary>
        private void SetParamsNameAutoComplete()
        {
            var myAutoCompleteCustomSource =
                new AutoCompleteStringCollection();
            foreach (object t in _arrayListParams)
                myAutoCompleteCustomSource.Add(t.ToString());
           // txtParamName.AutoCompleteCustomSource = myAutoCompleteCustomSource;
        }
        #endregion

        #region Parameter Saving Validations


        private Boolean  ParameterSavingValidations()
        {
            #region Check First Day of Month choose
            if ((PCreationDate.Text.Substring(8, 2)) != "01")
            {
                PersianMessageBox.Show("تاریخ آغاز اعتبار می بایست روز اول ماه انتخاب شود!", "خطا",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            #endregion

            #region Param Name Is Empty
            if (txtParamName.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر نامي به انگليسي وارد نماييد!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamName.Focus();
                return false;
            }
            #endregion

            #region Param Name Lenght Less Than 3 Charecter
            if (txtParamName.Text.Length < 3)
            {
                PersianMessageBox.Show("نام پارامتر بايد حداقل داراي 3 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamName.Focus();
                return false;
            }
            #endregion

            #region Boolean CheckDescriptionTextBox

            if (txtParamDescribe.Text == String.Empty)
            {
                PersianMessageBox.Show("براي پارامتر حتما توضيحاتي كافي ثبت نماييد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }
    

            if (txtParamDescribe.Text.Length < 5)
            {
                PersianMessageBox.Show("توضيحات بايد حداقل داراي 5 حرف باشد!", "خطا",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtParamDescribe.Focus();
                return false;
            }
   
      
        #endregion

            return true;
        }
        #endregion

        #region CheckTestingQuery
        private static Boolean CheckTestingQuery(string query)
        {

            #region Prepare SqlCommand
            var parameterResult = new DataTable();

            var mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            var mySqlCommand = new SqlCommand(query, mySqlConnection) { CommandTimeout = 0 };

            #endregion

            #region Execute SqlCommand
            try
            {
                mySqlCommand.Connection.Open();

// ReSharper disable AssignNullToNotNullAttribute
                parameterResult.Load(mySqlCommand.ExecuteReader());
// ReSharper restore AssignNullToNotNullAttribute

            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("در دستور وارد شده خطایی وجود دارد!" +
                                           "كدهای وارد شده را مجددا بررسی نمایید", strMessage, "Error").ShowDialog();
                mySqlCommand.Connection.Close();
                return false;
            }
            finally
            {
                mySqlCommand.Connection.Close();
            }
            #endregion

            return true;

        }
        #endregion


        #endregion

        #region Help
        private void FrmNewParameterHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "NewParameter.htm");
        }
        #endregion
























    }
}