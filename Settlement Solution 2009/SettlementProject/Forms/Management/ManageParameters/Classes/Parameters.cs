﻿
#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MohanirPouya.DbLayer;
using MohanirVBClasses;
using MohanirPouya.Classes;
using PureComponents.TreeView;
using TreeView=PureComponents.TreeView.TreeView;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{
    /// <summary>
    /// كلاس مدیریت توابع مشترك فرم پارامتر ها
    /// </summary>
    static class Parameters
    {
        private static string DropBillItemParam;
        private static SqlCommand f;
        private static string CommandText2;
        private static SqlCommand SqlCommand2;
        private static object ParamID;
        private static string CommandText3;
        private static SqlCommand SqlCommand3;
        //private static object _ParamSumType;
        //public static int ParamLevel ;
        private static DbSMDataContext _DbSM;

        #region public Methods

        #region ArrayList GetSavedParamsByPart(String PartName, String ParamType)

        /// <summary>
        /// تابعی برای بدست آوردن لیست پارامتر های ذخیره شده در یك گروه
        /// </summary>
        /// <param name="PartName">نوع بخش مورد نظر</param>
        /// <param name="ParamType">پارامتر ، پارامتر شرطی ، سرفصل یا شاخص</param>
        /// <returns>یك ArrayList از نام پارامتر ها</returns>
        public static DataTable GetSavedParamsByPart(String PartName, String ParamType)
        {
            #region Prepare SqlConnection & SqlCommand

            String CommandText = "SELECT ID , EnglishName , Category , Description FROM " +
                                 PartName + ".Parameters WHERE Category = '" + ParamType + "'";

            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            var MySqlCommand =
                new SqlCommand(CommandText, MySqlConnection);

            #endregion
            var ParamsTable = new DataTable();

            #region Execute SqlCommand

            #region try

            try
            {
                MySqlCommand.Connection.Open();
// ReSharper disable AssignNullToNotNullAttribute
                ParamsTable.Load(MySqlCommand.ExecuteReader());
// ReSharper restore AssignNullToNotNullAttribute
            }
                #endregion

                #region catch
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
                #endregion

                #region finally
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion

            #endregion

            // Returning Return-Value:
            return ParamsTable;
        }

        #endregion

        #region String GetParamDescription(String ParamName)
        /// <summary>
        /// تابعی برای بدست آوردن توضیحات یك پارامتر
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="DataTableParams">جدول پارامتر ها</param>
        /// <returns>توضیحات پارامتر</returns>
        public static String GetParamDescription(String ParamName, DataTable DataTableParams)
        {
            String ReturnValue = String.Empty;
            DataTableReader MyDataTableReader = DataTableParams.CreateDataReader();
            while (MyDataTableReader.Read())
                if (ParamName == MyDataTableReader["EnglishName"].ToString())
                    ReturnValue = MyDataTableReader["Description"].ToString();
            MyDataTableReader.Close();
            return ReturnValue;
        }
        #endregion

        #region void DeleteParam(String ParamName , String PartName)

        /// <summary>
        /// تابع حذف یك پارامتر به همراه كلیه اجزاء آن
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="PartName">بخش پارامتر</param>
        public static void DeleteParam(String ParamName, String PartName)
        {
            #region Declare DataTableParamLog
            DataTable DataTableParamLog = new DataTable();
            DataTableParamLog.Columns.Add("EnglishName", typeof(String));
            DataTableParamLog.Columns.Add("ObjectNameP", typeof(String));
            DataTableParamLog.Columns.Add("ParamID", typeof(Int32));
            #endregion

            #region Fill DataTable
            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            String CommandText = "SELECT EnglishName, ObjectNameP,ParamID " +
                                 "FROM " + PartName + ".ParametersLog " +
                                 "WHERE EnglishName = @ParamName ";
            var MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = ParamName;
            try
            {
                MySqlCommand.Connection.Open();
                DataTableParamLog.Load(MySqlCommand.ExecuteReader());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion

            #region DROP Created Tables
            DataRow[] FoundRows = DataTableParamLog.Select();
            for (int i = 0; i <= FoundRows.GetUpperBound(0); i++)
            {
                #region DropViewIfExist

                String DropViewString = "IF  EXISTS (SELECT * FROM sys.Tables " +
                                        "WHERE object_id = " + "OBJECT_ID(N'" +
                                        FoundRows[i]["ObjectNameP"] + "')) " +
                                        "DROP Table " + FoundRows[i]["ObjectNameP"];
                var ParamCreateView = new SqlCommand(DropViewString,
                                                            new SqlConnection(DbBizClass.DbConnStr));
                try
                {
                    ParamCreateView.Connection.Open();
                    ParamCreateView.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                    return;
                }
                finally
                {
                    ParamCreateView.Connection.Close();
                }

                #endregion
            }
            #endregion

            #region Delete From Parameters

            String DeleteItemString2 = "DELETE FROM " + PartName +
                                       ".Parameters " + "WHERE EnglishName = @ParamName";

            SqlCommand SqlCommand2 = new SqlCommand(DeleteItemString2,
                                                    new SqlConnection(DbBizClass.DbConnStr));
            SqlCommand2.Parameters.Add("@ParamName", SqlDbType.NVarChar, 50);
            SqlCommand2.Parameters["@ParamName"].Value = ParamName;
            try
            {
                SqlCommand2.Connection.Open();
                SqlCommand2.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return;
            }
            finally
            {
                SqlCommand2.Connection.Close();
            }

            #endregion

            #region Delete from ParamIn
            var MySqlConnection5 = new SqlConnection(DbBizClass.DbConnStr);
            String DeleteItemString23 = "DELETE FROM " + PartName +
                                        ".ParamIn " + "WHERE EnglishName = @ParamName";

            SqlCommand SqlCommand23 = new SqlCommand(DeleteItemString23, MySqlConnection5);
            SqlCommand23.Parameters.Add("@ParamName", SqlDbType.NVarChar, 50);
            SqlCommand23.Parameters["@ParamName"].Value = ParamName;
            try
            {
                SqlCommand23.Connection.Open();
                SqlCommand23.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return;
            }
            finally
            {
                SqlCommand23.Connection.Close();
            }
            #endregion

            #region DROP Bill Item Param


            #region DropViewIfExist


            switch (PartName)
            {
                case "Sellers":
                    DropBillItemParam = "DELETE FROM [General].[Tbl_BillItemParameters]" +
                                        " WHERE Subject='S' And ParameterIX=" + FoundRows[0][2];
                    break;
                case "Buyers":
                    DropBillItemParam = "DELETE FROM [General].[Tbl_BillItemParameters]" +
                                        " WHERE Subject='B' And ParameterIX=" + FoundRows[0][2];
                    break;
                case "Transfer":
                    DropBillItemParam = "DELETE FROM [General].[Tbl_BillItemParameters]" +
                                        " WHERE Subject='T' And ParameterIX=" + FoundRows[0][2];
                    break;

            }
            f = new SqlCommand(DropBillItemParam,
                               new SqlConnection(DbBizClass.DbConnStr));
            try
            {
                f.Connection.Open();
                f.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return;
            }
            finally
            {
                f.Connection.Close();
            }

            #endregion

            #endregion

        }

        #endregion

        #region Boolean ExecuteSelectString(String Query)

        /// <summary>
        /// تابعي براي اجراي فرمان اس كیو ال بر اساس رشته پرس و جو
        /// </summary>
        /// <param name="Query">رشته فرمان اجرايي</param>
        /// <returns>تعیین صحیح اجرا شدن فرمان</returns>
        public static Boolean ExecuteSelectString(String Query)
        {
            #region Execute SqlCommand
            SqlCommand MySqlCommand = new SqlCommand(Query,
                                                     new SqlConnection(DbBizClass.DbConnStr));
            MySqlCommand.CommandTimeout = 0;
            try
            {
                MySqlCommand.Connection.Open();
                MySqlCommand.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return false;
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion
            return true;
        }
        #endregion

        #region Boolean InsertParamData(...)

        /// <summary>
        /// تابعی برای افزودن اطلاعات پارامتر جدید ایجاد شده به بانك اطلاعات
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Description">توضیحات</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="SelectString">رشته فرمان</param>
        /// <param name="UserID">كد كاربر</param>
        /// <param name="ParamType">نوع پارامتر انتخاب شده</param>
        /// <param name="PartName">بخش انتخاب شده</param>
        /// <returns>تایید اجرای صحیح عملیات</returns>
        public static bool InsertParamData(string ParamName, string Description, int Revision, string SelectString, string UserID, string PartName, string ParamType, string GenerateQuery,string PCreationDate,string CDate,string @SumType,int @PLevel)
        {

            #region Add Or Modify Data In Parameters Table
            _DbSM = new DbSMDataContext
           (DbBizClass.DbConnStr);

            if (Revision == 0)
            {
                #region ADD Data IN Tbl.Parameters

                #region Prepare SqlCommand
                String CommandText1 = "INSERT INTO " + PartName + ".Parameters " +
                                      "(EnglishName, Category , [Description]) " +
                                      "VALUES (@ParamName, '" + ParamType + "' , @Description)";
                SqlCommand SqlCommand1 = new SqlCommand(CommandText1,
                                                        new SqlConnection(DbBizClass.DbConnStr));
                SqlCommand1.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                SqlCommand1.Parameters["@ParamName"].Value = ParamName;
                SqlCommand1.Parameters.Add("@Description", SqlDbType.NVarChar, 100);
                SqlCommand1.Parameters["@Description"].Value = Description;
                #endregion

                #region Execute SqlCommand
                try
                {
                    SqlCommand1.Connection.Open();
                    SqlCommand1.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                    return false;
                }
                finally
                {
                    SqlCommand1.Connection.Close();
                }
                #endregion

                #endregion

                #region Find Added Parameter ID In Parameter Table

                #region Prepare SqlCommand
                CommandText2 = "SELECT TOP 1 ID " +
                               "FROM " + PartName + ".Parameters " +
                               "WHERE EnglishName = @EnglishName";
                SqlCommand2 = new SqlCommand(CommandText2,
                                             new SqlConnection(DbBizClass.DbConnStr));
                SqlCommand2.Parameters.Add("@EnglishName", SqlDbType.NVarChar, 30);
                SqlCommand2.Parameters["@EnglishName"].Value = ParamName;
                #endregion

                #region Execute SqlCommand
                try
                {
                    SqlCommand2.Connection.Open();
                    ParamID = Convert.ToInt32(SqlCommand2.ExecuteScalar());
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                    return false;
                }
                finally
                {
                    SqlCommand2.Connection.Close();
                }
                #endregion

                #endregion

                #region Add Data To ParametersLog Table

                #region Prepare SqlCommand
                CommandText3 = "INSERT INTO " + PartName +
                               ".ParametersLog (EnglishName, ParamID , Revision , " +
                               "ObjectName ,ObjectNameP, SelectString , SelectTable,SumType ,PLevel, UserID,CreationDate,StartDate,Active) " + "VALUES ('" + ParamName + "' , " + ParamID + " , " + 0 + " , " + "@ObjectName , @ObjectNameP,@SelectString ,@SelectTable, @SumType,@PLevel , '" + UserID + "',@PCreationDate,@StartDate,@Active)";

                SqlCommand3 = new SqlCommand(CommandText3, new SqlConnection(DbBizClass.DbConnStr));
                SqlCommand3.Parameters.Add("@ObjectName", SqlDbType.NVarChar, 50);
                SqlCommand3.Parameters["@ObjectName"].Value =
                PartName + "Views." + ParamName;
                SqlCommand3.Parameters.Add("@ObjectNameP", SqlDbType.NVarChar, 50);
                SqlCommand3.Parameters["@ObjectNameP"].Value =
                PartName + "Param." + ParamName;
                SqlCommand3.Parameters.Add("@SelectString", SqlDbType.Text);
                SqlCommand3.Parameters["@SelectString"].Value = SelectString;
                SqlCommand3.Parameters.Add("@SelectTable", SqlDbType.Text);
                SqlCommand3.Parameters["@SelectTable"].Value = GenerateQuery;
                SqlCommand3.Parameters.Add("@SumType", SqlDbType.TinyInt);
                SqlCommand3.Parameters["@SumType"].Value = Convert.ToInt32(@SumType);
                SqlCommand3.Parameters.Add("@PLevel", SqlDbType.TinyInt);
                SqlCommand3.Parameters["@PLevel"].Value = @PLevel + 1;
                SqlCommand3.Parameters.Add("@PCreationDate", SqlDbType.NVarChar, 20);
                SqlCommand3.Parameters["@PCreationDate"].Value = CDate;
                SqlCommand3.Parameters.Add("@StartDate", SqlDbType.NVarChar, 10);
                SqlCommand3.Parameters["@StartDate"].Value = PCreationDate;
                SqlCommand3.Parameters.Add("@Active", SqlDbType.Bit);
                SqlCommand3.Parameters["@Active"].Value = false;
                #endregion

                #region Execute SqlCommand
                try
                {
                    SqlCommand3.Connection.Open();
                    SqlCommand3.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                    return false;
                }
                finally
                {
                    SqlCommand3.Connection.Close();
                }
                #endregion

                #endregion

            }

            else
            {
                #region Find Added Parameter ID In Parameter Table

                #region Prepare SqlCommand

                CommandText2 = "SELECT TOP 1 ID " + "FROM " + PartName + ".Parameters " + "WHERE EnglishName = @EnglishName";
                SqlCommand2 = new SqlCommand(CommandText2,
                                             new SqlConnection(DbBizClass.DbConnStr));
                SqlCommand2.Parameters.Add("@EnglishName", SqlDbType.NVarChar, 30);
                SqlCommand2.Parameters["@EnglishName"].Value = ParamName;
                #endregion



                #region Execute SqlCommand
                try
                {
                    SqlCommand2.Connection.Open();
                    ParamID = Convert.ToInt32(SqlCommand2.ExecuteScalar());
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                    return false;
                }
                finally
                {
                    SqlCommand2.Connection.Close();
                }
                #endregion

                #endregion

                #region Update Tbl.Parameter

                #region Prepare SqlCommand
                String CommandText1 = "UPDATE " + PartName + ".Parameters " +
                                      "SET [Description] = @Description " +
                                      "WHERE EnglishName = @ParamName;";
                var SqlCommand1 = new SqlCommand(CommandText1,
                                                        new SqlConnection(DbBizClass.DbConnStr));
                SqlCommand1.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
                SqlCommand1.Parameters["@ParamName"].Value = ParamName;
                SqlCommand1.Parameters.Add("@Description", SqlDbType.NVarChar, 100);
                SqlCommand1.Parameters["@Description"].Value = Description;
                #endregion

                #region Execute SqlCommand
                try
                {
                    SqlCommand1.Connection.Open();
                    SqlCommand1.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
                    return false;
                }
                finally
                {
                    SqlCommand1.Connection.Close();
                }
                #endregion

                #endregion

                #region lastplevel
                int LastPlevel = GetParamLevel(ParamName, PartName);
                #endregion

                #region InsertNewRevision
                _DbSM.SP_ParamInsertNewRevision(PartName,ParamName, (int?)ParamID, (short?)Revision, PartName + "Views." + ParamName, PartName + "Param." + ParamName, SelectString, GenerateQuery, @SumType, (byte?)(@PLevel + 1), UserID, CDate, PCreationDate);
                #endregion

                #region UpdatePlevel
                if (LastPlevel <(@PLevel + 1))
                {
                    new ParamStructure(PartName, ParamName, @PLevel - LastPlevel + 1);
                }
                #endregion
            }

            #endregion
            
            return true;
        }

        #endregion

        #region String GetParamSumType(String ParamName , String PartName)

        /// <summary>
        /// متدی برای دریافت نوع پارامتر مجموع
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="PartName">نام بخش مورد نظر</param>
        /// <returns>نوع پارامتر مجموع</returns>
        public  static String GetParamSumType(String ParamName, String PartName)
        {
            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);

            #region Get Param Last ID

            #region Prepare SqlCommand
            String CommandText = "SELECT ID FROM " + PartName + ".Parameters " +
                                 "WHERE EnglishName = @EnglishName";
            var SqlCommand1 =
                new SqlCommand(CommandText, MySqlConnection);
            SqlCommand1.Parameters.Add("@EnglishName", SqlDbType.NVarChar, 50);
            SqlCommand1.Parameters["@EnglishName"].Value = ParamName;
            Int32 ParameterID = 0;
            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand1.Connection.Open();
                // بدست آوردن كد پارامتر
                ParameterID = Convert.ToInt32(SqlCommand1.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                SqlCommand1.Connection.Close();
            }
            #endregion

            #endregion

            #region Get Param Sum Type

            #region Prepare SqlCommand
            CommandText2 = "SELECT TOP 1 SumType " +
                                  "FROM " + PartName + ".ParametersLog " +
                                  "WHERE ParamID = @ParamID ORDER BY Revision DESC";
            SqlCommand2 =
                new SqlCommand(CommandText2, MySqlConnection);
            SqlCommand2.Parameters.Add("@ParamID", SqlDbType.Int);
            SqlCommand2.Parameters["@ParamID"].Value = ParameterID;
            String ParamSumType = String.Empty;
            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand2.Connection.Open();
                // بدست آوردن نوع پارامتر مجموع
                ParamSumType = SqlCommand2.ExecuteScalar().ToString();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                SqlCommand2.Connection.Close();
            }
            #endregion

            #endregion

            return ParamSumType;
        }

        #endregion
        
        #endregion

        #region String GetParamLastObjectP(String ParamName , String PartName)

        /// <summary>
        /// تابعي براي بدست آوردن نام آخرين شيء يك پارامتر
        /// </summary>
        /// <param name="ParamName">نام پارامتر را دريافت مي كند</param>
        /// <param name="PartName">نام بخش مورد نظراز قبیل فروشندگان و غیره</param>
        /// <returns>آخرين شيء مربوط به پارامتر را باز مي گرداند</returns>
        public static String GetParamLastObjectP(String ParamName, String PartName)
        {
            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);

            #region Find Param ID

            #region Prepare SqlCommand
            String CommandText1 = "SELECT ID FROM " + PartName + ".Parameters " +
                                  "WHERE EnglishName = @EnglishName";
            var SqlCommand1 = new SqlCommand(CommandText1, MySqlConnection);
            SqlCommand1.Parameters.Add("@EnglishName", SqlDbType.NVarChar, 50);
            SqlCommand1.Parameters["@EnglishName"].Value = ParamName;
            Int32 ParameterID = 0;
            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand1.Connection.Open();
                // بدست آوردن كد پارامتر
                ParameterID = Convert.ToInt32(SqlCommand1.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                SqlCommand1.Connection.Close();
            }
            #endregion

            #endregion

            #region Find Param Last Object Name

            #region Prepare SqlCommand
            CommandText2 = "SELECT  ObjectNameP " +
                                  "FROM " + PartName + ".ParametersLog " +
                                  "WHERE ParamID = @ParamID AND Revision=0";
            SqlCommand2 = new SqlCommand(CommandText2, MySqlConnection);
            SqlCommand2.Parameters.Add("@ParamID", SqlDbType.Int);
            SqlCommand2.Parameters["@ParamID"].Value = ParameterID;
            String ParamObjectName = String.Empty;
            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand2.Connection.Open();
                // بدست آوردن كد پارامتر
                ParamObjectName = SqlCommand2.ExecuteScalar().ToString();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                SqlCommand2.Connection.Close();
            }
            #endregion

            #endregion

            return ParamObjectName;
        }

        #endregion
        
        #region  SetParamIDIN(String ParamName , String PartName)

        /// <summary>
        /// تابعي براي بدست آوردن نام آخرين شيء يك پارامتر
        /// </summary>
        /// <param name="ParamName">نام پارامتر را دريافت مي كند</param>
        /// <param name="ParameterIN"></param>
        /// <param name="PartName">نام بخش مورد نظراز قبیل فروشندگان و غیره</param>
        /// <returns>آخرين شيء مربوط به پارامتر را باز مي گرداند</returns>
        public static void SetParamIDIN(string ParamName, string ParameterIN, string @PartName)
        {


            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);

           #region Find Param ID

            //#region Prepare SqlCommand
            //String CommandText1 = "SELECT ID FROM " + @PartName + ".Parameters " +
            //                      "WHERE EnglishName = @EnglishName";
            //var SqlCommand1 = new SqlCommand(CommandText1, MySqlConnection);
            //SqlCommand1.Parameters.Add("@EnglishName", SqlDbType.NVarChar, 50);
            //SqlCommand1.Parameters["@EnglishName"].Value = ParameterIN;
            //Int32 ParameterID = 0;
            //#endregion

            //#region Execute SqlCommand
            //try
            //{
            //    SqlCommand1.Connection.Open();
            //    // بدست آوردن كد پارامتر
            //    ParameterID = Convert.ToInt32(SqlCommand1.ExecuteScalar());
            //}
            //catch (Exception Ex)
            //{
            //    MessageBox.Show(Ex.Message, "خطا!");
            //}
            //finally
            //{
            //    SqlCommand1.Connection.Close();
            //}
            //#endregion

          #endregion

            #region Insert PramIn

            #region Prepare SqlCommand

            //string CommandText3 = "INSERT INTO " + @PartName +
            //                      ".ParamIn ([EnglishName],[ParamInID])" +
            //                      "VALUES ('" + ParamName + "' ,'" + ParameterID + "')";

            //SqlCommand SqlCommand3 = new SqlCommand(CommandText3, MySqlConnection);
            string CommandText3 = "INSERT INTO " + @PartName +
                        ".ParamIn ([EnglishName],[EnglishNameIN])" +
                        "VALUES ('" + ParamName + "' ,'" + ParameterIN + "')";

            var SqlCommand3 = new SqlCommand(CommandText3, MySqlConnection);
            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand3.Connection.Open();
                SqlCommand3.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");

            }
            finally
            {
                SqlCommand3.Connection.Close();
            }
            #endregion

            #endregion


        }

        #endregion

        #region  DelParamIDIN(String ParamName , String PartName)

        /// <summary>
        /// تابعي براي بدست آوردن نام آخرين شيء يك پارامتر
        /// </summary>
        /// <param name="ParamName">نام پارامتر را دريافت مي كند</param>
        /// <param name="PartName">نام بخش مورد نظراز قبیل فروشندگان و غیره</param>
        /// <returns>آخرين شيء مربوط به پارامتر را باز مي گرداند</returns>
       public static void DelParamIDIN(string ParamName, string @PartName)
        {
            #region Delete From Parameters
            var MySqlConnection21 = new SqlConnection(DbBizClass.DbConnStr);
            String DeleteItemString21 = "DELETE FROM " + @PartName +
                                        ".ParamIn " + "WHERE EnglishName = @ParamName";

            var SqlCommand21 = new SqlCommand(DeleteItemString21, MySqlConnection21);
            SqlCommand21.Parameters.Add("@ParamName", SqlDbType.NVarChar, 50);
            SqlCommand21.Parameters["@ParamName"].Value = ParamName;
            try
            {
                SqlCommand21.Connection.Open();
                SqlCommand21.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return;
            }
            finally
            {
                SqlCommand21.Connection.Close();
            }

            #endregion

        }

        #endregion

        #region Int GetParamLevel(String ParamName , String PartName)

        /// <summary>
        /// تابعي براي بدست آوردن نام آخرين شيء يك پارامتر
        /// </summary>
        /// <param name="ParamName">نام پارامتر را دريافت مي كند</param>
        /// <param name="PartName">نام بخش مورد نظراز قبیل فروشندگان و غیره</param>
        /// <returns>آخرين شيء مربوط به پارامتر را باز مي گرداند</returns>
       public static int GetParamLevel(String ParamName, String PartName)
        {
            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);

            #region Find Param ID

            #region Prepare SqlCommand
            var CommandText1 = "SELECT ID FROM " + PartName + ".Parameters " +
                                  "WHERE EnglishName = @EnglishName";
            var SqlCommand1 = new SqlCommand(CommandText1, MySqlConnection);
            SqlCommand1.Parameters.Add("@EnglishName", SqlDbType.NVarChar, 50);
            SqlCommand1.Parameters["@EnglishName"].Value = ParamName;
            Int32 ParameterID = 0;
            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand1.Connection.Open();
                // بدست آوردن كد پارامتر
                ParameterID = Convert.ToInt32(SqlCommand1.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                SqlCommand1.Connection.Close();
            }
            #endregion

            #endregion

            #region Find Param Last Object Name

            #region Prepare SqlCommand
            String CommandText2p = "SELECT  Max(PLevel) " +
                                   "FROM " + PartName + ".ParametersLog " +
                                   "WHERE ParamID = @ParamID ";
            var SqlCommand2p = new SqlCommand(CommandText2p, MySqlConnection);
            SqlCommand2p.Parameters.Add("@ParamID", SqlDbType.Int);
            SqlCommand2p.Parameters["@ParamID"].Value = ParameterID;
            int Paramlevel = 0;
            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand2p.Connection.Open();
                // بدست آوردن كد پارامتر
                Paramlevel = Convert.ToInt16(SqlCommand2p.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                SqlCommand2p.Connection.Close();
            }
            #endregion

            #endregion

            return Paramlevel;
        }

        #endregion

        #region DataTable GetAllParamsByPart(String PartName)
       /// <summary>
       /// دریافت جدول اطلاعات كلیه پارامتر های یك بخش
       /// </summary>
       /// <param name="PartName">نام بخش</param>
       /// <returns>جدول پارامتر ها</returns>
       public static DataTable GetAllParamsByPart(String PartName)
       {
           #region Make SELECT Command
           String CommandText = "SELECT ID , EnglishName , Category , [Description] " +
                                "FROM " + PartName + ".Parameters " +
                                "ORDER BY EnglishName ASC";
           #endregion

           #region Prepare SqlConnection & SqlCommand
           var MySqlConnection =
               new SqlConnection(DbBizClass.DbConnStr);
           var MySqlCommand =
               new SqlCommand(CommandText, MySqlConnection);
           #endregion
           var ParametersList = new DataTable();
           #region Execute SqlCommand

           #region try
           try
           {
               MySqlCommand.Connection.Open();
               ParametersList.Load(MySqlCommand.ExecuteReader());
           }
           #endregion

           #region catch
           catch (Exception Ex)
           {
               MessageBox.Show(Ex.Message);
           }
           #endregion

           #region finally
           finally
           {
               MySqlCommand.Connection.Close();
           }
           #endregion

           #endregion
           return ParametersList;
       }
       #endregion

        #region ArrayList GetParamsNameByTable(DataTable DataTableParams)

       /// <summary>
       /// تابع لیست كردن نام انگلیسی پارامتر ها از جدول پارامتر ها
       /// </summary>
       /// <param name="DataTableParams">جدول پارامتر ها</param>
       /// <returns>لیست نام پارامتر ها</returns>
       public static ArrayList GetParamsNameByTable(DataTable DataTableParams)
       {
           ArrayList ArrayListParamsEngName = new ArrayList();
           DataTableReader MyDataTableReader = DataTableParams.CreateDataReader();
           while (MyDataTableReader.Read())
               ArrayListParamsEngName.Add(MyDataTableReader["EnglishName"].ToString());
           MyDataTableReader.Close();
           return ArrayListParamsEngName;
       }

       #endregion



    }

    /// <summary>
    /// كلاس مدیریت توابع مشترك فرم های مدیریت پارامتر
    /// </summary>
    static class ManageParameters
    {

        #region Fields

  
        public static String _ParamSumType;


        public static int PLevel;
        public static int ParamLevel;
        private static string ObjectFullName;
        private static string SelectColumns;

        #endregion

        #region public static Methods

        
        #region String GetParamString(...)

        /// <summary>
        /// تابع بازگرداندن رشته فرمان اس كیو ال برای تولید توابع
        /// </summary>
        /// <param name="PartName">نام بخش از قبیل خریداران ، فروشندگان و غیره</param>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="ParamCode">متن دستور پارامتر</param>
        /// <param name="DataTableParams">لیست پارامتر ها</param>
        /// <param name="ArrayListParams">آرایه پارامتر ها</param>
        /// <param name="IsTesting">تستی بودن یا قطعی بودن فرمان</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <returns>رشته فرمان اس كیو ال</returns>
        public static String GetParamString(String PartName, String ParamName,
                                            String ParamCode, DataTable DataTableParams,
                                            ArrayList ArrayListParams, Boolean IsTesting, Int32 Revision,int step, string @param)
        {
            String ReturnValue = String.Empty;

            #region Sellers
            if (PartName == "Sellers")
            {
                if (step != 2 && step!=4)
                {
                    ReturnValue =
                        GetSellersParamString
                            (ParamName, ParamCode, DataTableParams, ArrayListParams, IsTesting, Revision, step);
                }

                #region Step2
                if (step==2 || step==4)
                {
                    #region DropTableIfExist

                    String DropTableString = "IF  EXISTS (SELECT * FROM sys.Tables " +
                                            "WHERE object_id = " + "OBJECT_ID(N'SellersParam." +ParamName +
                                              "')) " +"DROP Table " +"SellersParam."+ParamName ;
                    SqlCommand ParamCreateView = new SqlCommand(DropTableString,
                                                                new SqlConnection(DbBizClass.DbConnStr));
                    try
                    {
                        ParamCreateView.Connection.Open();
                        ParamCreateView.ExecuteNonQuery();
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message, "خطا!");
                    }
                    finally
                    {
                        ParamCreateView.Connection.Close();
                    }

                    #endregion

                    #region Execute Query (CREATE Table)
                    if (Parameters.ExecuteSelectString(ParamCode) == false)
                    {
                    }

                    #endregion

                    ReturnValue = "Select * From SellersParam." + ParamName;

                }
                #endregion
            }
                #endregion

            #region Buyers
            else if (PartName == "Buyers")
                ReturnValue =
                    GetBuyersParamString(ParamName, ParamCode, DataTableParams, ArrayListParams, IsTesting, Revision);
                #endregion

            #region Transfer
            else if (PartName == "Transfer")
                ReturnValue =
                    GetTransferParamString(ParamName, ParamCode, DataTableParams, ArrayListParams, IsTesting, Revision);
            #endregion

            return ReturnValue;
        }
        #endregion

        #region Boolean CheckParamExist(String ParamName , String PartName)

        /// <summary>
        /// روالی برای بررسی وجود پارامتری با نام پارامتر جدید
        /// </summary>
        /// <param name="ParamName">نام پارامتر وارد شده</param>
        /// <param name="PartName">نام بخش مورد نظر</param>
        /// <returns>اعلام وجود یا عدم وجود پارامتر</returns>
        public static Boolean CheckParamExist(String ParamName, String PartName)
        {
            #region Prepare SqlCommand
            String FindQuery = "IF EXISTS (" +
                               "SELECT TOP 1 EnglishName FROM " + PartName + ".Parameters " +
                               "WHERE EnglishName = @ParamName) SELECT 1 ELSE SELECT 0";
            var MyCommand = new SqlCommand(FindQuery,
                                                  new SqlConnection(DbBizClass.DbConnStr));
            MyCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MyCommand.Parameters["@ParamName"].Value = ParamName;
            Int32 Result;
            #endregion

            #region Execute SqlCommand
            try
            {
                MyCommand.Connection.Open();
                Result = Convert.ToInt32(MyCommand.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return true;
            }
            finally
            {
                MyCommand.Connection.Close();
            }
            #endregion

            // اگر پارامتری وجود داشته باشید:
            if (Result == 1)
                return true;
            // عدم وجود پارامتر:
            return false;
        }

        #endregion

        #region Boolean InsertParamNewRevisionData(...)

        /// <summary>
        /// تابعی برای افزودن اطلاعات دوره جدید پارامتر ایجاد شده به بانك اطلاعات
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Description">توضیحات</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="SelectString">رشته فرمان</param>
        /// <param name="UserID">كد كاربر</param>
        /// <param name="ValidationBeginDate">تاریخ آغاز اعتبار</param>
        /// <param name="ValidationEndDate">تاریخ پایان اعتبار</param>
        /// <param name="PartName">بخش انتخاب شده</param>
        /// <returns>تایید اجرای صحیح عملیات</returns>
        public static Boolean InsertParamNewRevisionData(String ParamName, String Description,
                                                         Int32 Revision, String SelectString, Int32 UserID,
                                                         DateTime ValidationBeginDate, DateTime ValidationEndDate, String PartName)
        {

            #region Edit Description In Parameter Table

            String CommandText1 =
                "UPDATE " + PartName + ".Parameters " +
                "SET Description = @Description " +
                "WHERE EnglishName = @ParamName;";
            var SqlCommand1 = new SqlCommand(CommandText1,
                                                    new SqlConnection(DbBizClass.DbConnStr));
            SqlCommand1.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            SqlCommand1.Parameters["@ParamName"].Value = ParamName;
            SqlCommand1.Parameters.Add("@Description", SqlDbType.NVarChar, 100);
            SqlCommand1.Parameters["@Description"].Value = Description;
            try
            {
                SqlCommand1.Connection.Open();
                SqlCommand1.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return false;
            }
            finally
            {
                SqlCommand1.Connection.Close();
            }

            #endregion

            #region Find Added Parameter ID In Parameter Table

            Int32 ParamID;

            String CommandText2 =
                "SELECT ID FROM " + PartName + ".Parameters " +
                "WHERE EnglishName = @EnglishName";
            var SqlCommand2 = new SqlCommand(CommandText2,
                                                    new SqlConnection(DbBizClass.DbConnStr));
            SqlCommand2.Parameters.Add("@EnglishName", SqlDbType.NVarChar, 30);
            SqlCommand2.Parameters["@EnglishName"].Value = ParamName;
            try
            {
                SqlCommand2.Connection.Open();
                ParamID = Convert.ToInt32(SqlCommand2.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return false;
            }
            finally
            {
                SqlCommand2.Connection.Close();
            }

            #endregion

            #region Add Data To ParametersLog Table

            #region Prepare SqlCommand
            String CommandText3 = "INSERT INTO " + PartName +
                                  ".ParametersLog (EnglishName, ParamID , Revision , " +
                                  "ObjectName , SelectString , SumType , UserID , StartDate , ExpirationDate) " +
                                  "VALUES ('" + ParamName + "' , " + ParamID + " , " + Revision + " , " +
                                  "@ObjectName , @SelectString , @SumType , " +
                                  UserID + " , @StartDate , @ExpirationDate)";

            var SqlCommand3 = new SqlCommand(CommandText3,
                                                    new SqlConnection(DbBizClass.DbConnStr));
            SqlCommand3.Parameters.Add("@ObjectName", SqlDbType.NVarChar, 50);
            SqlCommand3.Parameters["@ObjectName"].Value =
                PartName + "Views." + ParamName + Revision;
            SqlCommand3.Parameters.Add("@SelectString", SqlDbType.NVarChar, 200);
            SqlCommand3.Parameters["@SelectString"].Value = SelectString;
            SqlCommand3.Parameters.Add("@StartDate", SqlDbType.DateTime);
            SqlCommand3.Parameters["@StartDate"].Value = ValidationBeginDate;
            SqlCommand3.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
            SqlCommand3.Parameters["@ExpirationDate"].Value = ValidationEndDate;

            #region Set SumType Value

            SqlCommand3.Parameters.Add("@SumType", SqlDbType.TinyInt);

            #region Sellers SumType

            if (PartName == "Sellers")
            {
                if (_ParamSumType == "0") // Not Sum:
                    SqlCommand3.Parameters["@SumType"].Value = 0;
                else if (_ParamSumType == "1")
                    SqlCommand3.Parameters["@SumType"].Value = 1;
                else if (_ParamSumType == "2")
                    SqlCommand3.Parameters["@SumType"].Value = 2;
                else if (_ParamSumType == "3")
                    SqlCommand3.Parameters["@SumType"].Value = 3;
                else if (_ParamSumType == "4")
                    SqlCommand3.Parameters["@SumType"].Value = 4;
                else if (_ParamSumType == "5")
                    SqlCommand3.Parameters["@SumType"].Value = 5;
                else if (_ParamSumType == "60")
                    SqlCommand3.Parameters["@SumType"].Value = 60;
                else if (_ParamSumType == "61")
                    SqlCommand3.Parameters["@SumType"].Value = 61;
                else if (_ParamSumType == "62")
                    SqlCommand3.Parameters["@SumType"].Value = 62;
                else if (_ParamSumType == "63")
                    SqlCommand3.Parameters["@SumType"].Value = 63;
                else if (_ParamSumType == "64")
                    SqlCommand3.Parameters["@SumType"].Value = 64;
                else if (_ParamSumType == "65")
                    SqlCommand3.Parameters["@SumType"].Value = 65;
                else if (_ParamSumType == "7")
                    SqlCommand3.Parameters["@SumType"].Value = 7;
            }

                #endregion

                #region Buyers SumType

            else if (PartName == "Buyers")
            {
                if (_ParamSumType == "0") // Not Sum:
                    SqlCommand3.Parameters["@SumType"].Value = 0;
            }

            #endregion

            #endregion

            #endregion

            #region Execute SqlCommand
            try
            {
                SqlCommand3.Connection.Open();
                SqlCommand3.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
                return false;
            }
            finally
            {
                SqlCommand3.Connection.Close();
            }
            #endregion

            #endregion

            return true;
        }

        #endregion

        #region Boolean *** CreateParameterTable(...) ***
        /// <summary>
        /// روالي براي ايجاد نما براي پارامتر
        /// </summary>
        /// <param name="DataTableParams">جدول پارامتر ها</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Description">توضیحات پارامتر</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="SelectString">رشته فرمان</param>
        /// <param name="UserID">كد كاربر</param>
        /// <param name="PartName">نام بخش</param>
        /// <param name="ParamType">نوع پارامتر</param>
        /// <param name="step"></param>
        /// <param name="param"></param>
        /// <param name="PCreationDate"></param>
        /// <param name="Cdate"></param>
        /// <returns></returns>
        public static bool CreateParameterTable(DataTable DataTableParams, ArrayList ArrayListParams, string ParamName, string Description, int Revision, string SelectString, string UserID, string PartName, string ParamType, int step, string param, string PCreationDate, string Cdate)
        {
            #region Check Existance Of New Parameter

            #region Rev 0
            if (Revision == 0)
            {
                if (CheckParamExist(ParamName, PartName))
                {
                    PersianMessageBox.Show(
                        "پارامتري با اين نام موجود مي باشد ، لطفاً نام ديگري انتخاب نماييد!",
                        "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            #endregion 

            #endregion

            #region Check User Permission

            DialogResult QResult =
                PersianMessageBox.Show("آيا از اين كار اطمينان داريد؟\n " +
                                       "قبل از ذخيره سازي پارامتر از صحت عملكرد آن اطمينان يابيد!" +
                                       " براي اين منظور بر روي دكمه ي \"بررسي دستورات\" كليك نماييد", "هشدار!",
                                       MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (QResult == DialogResult.No)
                return false;

            #endregion
            
            #region  Generate SQL Query String For Create Table
            var GenerateQuery = String.Empty;

            #region Sellers
       
                if (PartName == "Sellers")
                    if (step != 2 && step!=4)
                    {
                    GenerateQuery = GetSellersParamString(ParamName,
                                                          SelectString, DataTableParams, ArrayListParams, false,
                                                          Revision, step);
            }
            else
            {
                GenerateQuery = GetSellersParamString(ParamName,
                              SelectString, DataTableParams, ArrayListParams, false,
                              Revision, step);
                GenerateQuery = SelectString;
            
            }
                #endregion

            #region Buyers
            else if (PartName == "Buyers")
                GenerateQuery = GetBuyersParamString(ParamName,
                                                     SelectString, DataTableParams, ArrayListParams, false, Revision);
            #endregion

            #region Transfer
            else if (PartName == "Transfer")
                GenerateQuery = GetTransferParamString(ParamName,
                                                       SelectString, DataTableParams, ArrayListParams, false, Revision);
            #endregion

            #endregion

            #region Insert Param Data To Database
            if (Parameters.InsertParamData(ParamName, Description, Revision, @param,UserID, PartName, ParamType, GenerateQuery,PCreationDate,Cdate,_ParamSumType,ParamLevel) == false)
                return false;
            #endregion

            #region Del Created Table

            #region Declare DataTableParamLog
            DataTable DataTableParamLog = new DataTable();
            DataTableParamLog.Columns.Add("EnglishName", typeof(String));
            DataTableParamLog.Columns.Add("ObjectNameP", typeof(String));
            DataTableParamLog.Columns.Add("ParamID", typeof(Int32));
            #endregion

            #region Fill DataTable
            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            String CommandText = "SELECT EnglishName, ObjectNameP,ParamID " +
                                 "FROM " + PartName + ".ParametersLog " +
                                 "WHERE EnglishName = @ParamName ";
            var MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = ParamName;
            try
            {
                MySqlCommand.Connection.Open();
                DataTableParamLog.Load(MySqlCommand.ExecuteReader());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion

            #region DROP Created Tables
            DataRow[] FoundRows = DataTableParamLog.Select();
            for (int i = 0; i <= FoundRows.GetUpperBound(0); i++)
            {
                #region DropViewIfExist

                String DropViewString = "IF  EXISTS (SELECT * FROM sys.Tables " +
                                        "WHERE object_id = " + "OBJECT_ID(N'" +
                                        FoundRows[i]["ObjectNameP"] + "')) " +
                                        "DROP Table " + FoundRows[i]["ObjectNameP"];
                SqlCommand ParamCreateView = new SqlCommand(DropViewString,
                                                            new SqlConnection(DbBizClass.DbConnStr));
                try
                {
                    ParamCreateView.Connection.Open();
                    ParamCreateView.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");
               
                }
                finally
                {
                    ParamCreateView.Connection.Close();
                }

                #endregion
            }
            #endregion

            #endregion

            #region Exec SelectString

            Parameters.ExecuteSelectString(GenerateQuery);
            #endregion

            return true;
        }
        #endregion
        

        #endregion

        #region private Methods

        #region @@@ String GetSellersParamString(...) @@@
        /// <summary>
        /// توليد رشته فرمان اس كيو ال براي ايجاد نما
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="SelectString">رشته وارد شده</param>
        /// <param name="DataTableParams">جدول پارامتر های ذخیره شده</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="IsTesting">تعیین تستی بودن یا نبودن فرمان</param>
        /// <param name="Rivision">شماره دوره پارامتر</param>
        /// <returns>رشته ي فرمان اس كيو ال را بر مي گرداند</returns>
        private static String GetSellersParamString(String ParamName, String SelectString,
                                                    DataTable DataTableParams, ArrayList ArrayListParams,
                                                    Boolean IsTesting, Int32 Rivision ,int step)
        {

            #region Prepare Execution

            #region Check String Null Value

            if (String.IsNullOrEmpty(SelectString))
            {
                PersianMessageBox.Show("هیچ دستوری نوشته نشده ، بررسی ممكن نیست!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return String.Empty;
            }
            #endregion

            #region Variables Declaration
            Boolean ContainSumType0 = false;
            Boolean ContainSumType1 = false;
            Boolean ContainSumType2 = false;
            Boolean ContainSumType3 = false;
            Boolean ContainSumType4 = false;
            Boolean ContainSumType5 = false;
            Boolean ContainSumType60 = false;
            Boolean ContainSumType61 = false;
            Boolean ContainSumType62 = false;
            Boolean ContainSumType63 = false;
            Boolean ContainSumType64 = false;
            Boolean ContainSumType65 = false;
            Boolean ContainSumType7 = false;
            Boolean ContainSumType8 = false;
            Boolean ContainSumType9 = false;
            Boolean ContainParam = false;
            #endregion

            #region Fill ArrayList Used Parameters

            var ArrayListUsedParams = new ArrayList();
            foreach (String Str in ArrayListParams)
            {
                String SearchWord = @"\b" + Str + @"\b";
                if (Regex.IsMatch(SelectString, SearchWord))
                    ArrayListUsedParams.Add(Str);
            }

            #endregion

            #region Replace Complete object
            if (IsTesting)
            {


                #region Replace Complete Objects Name

                foreach (String Str in ArrayListUsedParams)
                {
                    var SchemaName = Parameters.GetParamLastObjectP(Str, "Sellers");
                    var TextToFind = new Regex(Str + @"\b");
                   ObjectFullName = SchemaName + "." + Str;
                    SelectString = TextToFind.Replace(SelectString, ObjectFullName);
                }

                #endregion

             
            }
            else
            {
                #region Delete from ParamIn

                Parameters.DelParamIDIN(ParamName, "Sellers");

                #endregion

                if (step != 2)
                {


                    #region Replace Complete Objects Name

                    ParamLevel = 0;
                    foreach (String Str in ArrayListUsedParams)
                    {
                        String SchemaName = Parameters.GetParamLastObjectP(Str, "Sellers");
                        Parameters.SetParamIDIN(ParamName, Str, "Sellers");
                        PLevel = Parameters.GetParamLevel(Str, "Sellers");
                        if (PLevel > ParamLevel)
                        {
                            ParamLevel = PLevel;
                        }
                        Regex TextToFind = new Regex(Str + @"\b");


                        ObjectFullName = SchemaName + "." + Str;
                        SelectString = TextToFind.Replace(SelectString, ObjectFullName);



                    }

                    #endregion
                    
                }
                else
                {
                    #region return step 2

                    #region Replace Complete Objects Name

                    ParamLevel = 0;
                    foreach (String Str in ArrayListUsedParams)
                    {
                        Parameters.SetParamIDIN(ParamName, Str, "Sellers");
                        PLevel = Parameters.GetParamLevel(Str, "Sellers");
                        if (PLevel > ParamLevel)
                        {
                            ParamLevel = PLevel;
                        }
                      }


                    #endregion

                    _ParamSumType = "0";
                    return SelectString;

                    #endregion
                }

                
            }
            #endregion
            
            #region Check Used Variables For Sum Types

            if (ArrayListUsedParams.Count > 0)
            {
                #region Find Containing of Sum Params
                foreach (String Str in ArrayListUsedParams)
                {
                    String CurentParamSum = Parameters.GetParamSumType(Str, "Sellers");
                    if (CurentParamSum == "0")
                        ContainSumType0 = true;
                    else if (CurentParamSum == "1")
                        ContainSumType1 = true;
                    else if (CurentParamSum == "2")
                        ContainSumType2 = true;
                    else if (CurentParamSum == "3")
                        ContainSumType3 = true;
                    else if (CurentParamSum == "4")
                        ContainSumType4 = true;
                    else if (CurentParamSum == "5")
                        ContainSumType5 = true;
                    else if (CurentParamSum == "60")
                        ContainSumType60 = true;
                    else if (CurentParamSum == "61")
                        ContainSumType61 = true;
                    else if (CurentParamSum == "62")
                        ContainSumType62 = true;
                    else if (CurentParamSum == "63")
                        ContainSumType63 = true;
                    else if (CurentParamSum == "64")
                        ContainSumType64 = true;
                    else if (CurentParamSum == "65")
                        ContainSumType65 = true;
                    else if (CurentParamSum == "7")
                        ContainSumType7 = true;
                    else if (CurentParamSum == "8")
                        ContainSumType8 = true;
                    else if (CurentParamSum == "9")
                        ContainSumType9 = true;
                }
                #endregion

                #region Set Current Param SumType
                if (ContainSumType0)
                    _ParamSumType = "0";
                else if (ContainSumType1)
                    _ParamSumType = "1";
                else if (ContainSumType2)
                    _ParamSumType = "2";
                else if (ContainSumType3)
                    _ParamSumType = "3";
                else if (ContainSumType4)
                    _ParamSumType = "4";
                else if (ContainSumType5)
                    _ParamSumType = "5";
                else if (ContainSumType60)
                    _ParamSumType = "60";
                else if (ContainSumType61)
                    _ParamSumType = "61";
                else if (ContainSumType62)
                    _ParamSumType = "62";
                else if (ContainSumType63)
                    _ParamSumType = "63";
                else if (ContainSumType64)
                    _ParamSumType = "64";
                else if (ContainSumType65)
                    _ParamSumType = "65";
                else if (ContainSumType7)
                    _ParamSumType = "7";
                else if (ContainSumType8)
                    _ParamSumType = "8";
                else if (ContainSumType9)
                    _ParamSumType = "9";
                #endregion

                ContainParam = true;
            }

            #endregion

            #endregion

            #region SUM SELECT

            #region For Sum1 (SumByUnitType)

            if (Regex.IsMatch(SelectString, @"\bSum1\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "1";
                var SumTextToFind = new Regex(@"\bSum1\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString = SelectString.Insert(0, "SELECT RegionName, " +
                                                      "CompanyName , " +
                                                      "PowerStationCode , " + "PowerStationName , " + 
                                                    " BillUnitID, "+ "Date , " + "Hour , ");
                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName 
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                const String GroupByString = " GROUP BY " +
                                             "RegionName , " + "CompanyName , " +
                                             "PowerStationCode , " + "PowerStationName , " +
                                              " BillUnitID, " + "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

            #region For Sum2 (SumByPowerStationName)

            else if (Regex.IsMatch(SelectString, @"\bSum2\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "2";
                var SumTextToFind = new Regex(@"\bSum2\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " +
                                           "CompanyName , " +
                                           "PowerStationCode , " + "PowerStationName , " + "Date , " + "Hour , ");

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , "  +
                                             "CompanyName , " + "PowerStationCode , " +
                                             "PowerStationName , " +
                                             "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

            #region For Sum3 (SumByCompanyName)

            else if (Regex.IsMatch(SelectString, @"\bSum3\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "3";
                var SumTextToFind = new Regex(@"\bSum3\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " +
                                           "CompanyTypeName , " + "CompanyName , " +
                                           "Date , " + "Hour , ");

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName+ "' FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                else
                {

                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"+ " into SellersParam." + ParamName +" FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , " + "CompanyTypeName , " +
                                             "CompanyName , " + "Date , " + "Hour";

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

            #region For Sum4 (SumByCompanyTypeName)

            else if (Regex.IsMatch(SelectString, @"\bSum4\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "4";
                var SumTextToFind = new Regex(@"\bSum4\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " +
                                           "CompanyTypeName , " + "Date , " + "Hour , ");

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName+ "' FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"+ " into SellersParam." + ParamName +" FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , " + "CompanyTypeName , " +
                                             "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

            #region For Sum5 (SumByRegionName)

            else if (Regex.IsMatch(SelectString, @"\bSum5\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "5";
                var SumTextToFind = new Regex(@"\bSum5\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " + "Date , " + "Hour , ");

                #region Istesting-From

                if (!IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                                                                            + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }

                #endregion

                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , " + "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

            #region For Sum6 (SumByDay)

            else if (Regex.IsMatch(SelectString, @"\bSum6\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "6";
                var SumTextToFind = new Regex(@"\bSum6\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString,                   SumTextToReplace);

                #endregion

                #region Set Param Sum Type

                if (ContainSumType0)
                    _ParamSumType = "60";
                else if (ContainSumType1)
                    _ParamSumType = "61";
                else if (ContainSumType2)
                    _ParamSumType = "62";
                else if (ContainSumType3)
                    _ParamSumType = "63";
                else if (ContainSumType4)
                    _ParamSumType = "64";
                else if (ContainSumType5)
                    _ParamSumType = "65";

                #endregion

                #region Make SELECT

                #region Contain Sum Type 0 (Fixed & Mixed)

                if (ContainSumType0)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyName , " +
                                               "PowerStationCode , " + "PowerStationName , " + "UnitType , "
                                               + "UnitID ,UnitCode, " + "Date , ");

                    #endregion
            
                #region Contain Sum Type 2(SumByPowerStationName)

                else if (ContainSumType2)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                                "CompanyName , " + "PowerStationCode , " +
                                               "PowerStationName , " + "Date , ");

                    #endregion

                #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " + "Date , ");

                    #endregion

                #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "Date , ");

                    #endregion

                #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " + "Date , ");

                #endregion

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName+ "' FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"+ " into SellersParam." + ParamName +" FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                String GroupByString = String.Empty;

                    #region Contain Sum Type 0

                if (ContainSumType0)
                    GroupByString = " GROUP BY " +
                                    "RegionName , " + 
                                    "CompanyName , " + "PowerStationCode , " + "PowerStationName , " +
                                    "UnitType , " + "UnitID ,UnitCode, " + "Date";

                    #endregion

                    #region Contain Sum Type 1 (SumByUnitType)

                else if (ContainSumType1)
                    GroupByString = " GROUP BY " +
                                    "RegionName , " +
                                    "CompanyName , " + "PowerStationCode , " + "PowerStationName , " +
                                    "UnitType , " +"BillUnitID , " +"BillUnitT , " + "Date";

                    #endregion

                    #region Contain Sum Type 2 (SumByPowerStationName)

                else if (ContainSumType2)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                  "CompanyName , " 
                                   +"PowerStationCode , " + "PowerStationName , " + "Date";

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName , " + "Date";

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "Date";

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    GroupByString = " GROUP BY " + "RegionName , " + "Date";

                #endregion

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

            #region For Sum7 (SumByMonth)

            else if (Regex.IsMatch(SelectString, @"\bSum7\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "7";
                var SumTextToFind = new Regex(@"\bSum7\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

               #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2                                    (PowerStation)
                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " +                        "PowerStationName , ");

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , ");

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " + "CompanyTypeName , ");

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, ");

                    #endregion

                    #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, ");

                #endregion


                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                String GroupByString = String.Empty;

                #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2 (PowerStation)

                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName , " + "PowerStationName";

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName";

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName";

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    GroupByString = " GROUP BY " + "RegionName";

                    #endregion

                    #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    GroupByString = " GROUP BY " + "RegionName";

                #endregion

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

            #region For Sum8 (SumByJustDate)

            else if (Regex.IsMatch(SelectString, @"\bSum8\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "8";
                var SumTextToFind = new Regex(@"\bSum8\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2                                    (PowerStation)
                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date, " );

                #endregion

                #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date, " );

                #endregion

                #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date, ");

                #endregion

                #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date, ");

                #endregion

                #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date, ");

                #endregion


                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                String GroupByString = String.Empty;

                #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2 (PowerStation)

                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    GroupByString = " GROUP BY " + "Date" ;

                #endregion

                #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    GroupByString = " GROUP BY " + "Date";


                #endregion

                #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    GroupByString = " GROUP BY " + "Date";


                #endregion

                #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    GroupByString = " GROUP BY " + "Date";


                #endregion

                #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    GroupByString = " GROUP BY " + "Date";


                #endregion

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

            #endregion

            #region For Sum9 (SumByJustDateHour)

            else if (Regex.IsMatch(SelectString, @"\bSum9\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "9";
                var SumTextToFind = new Regex(@"\bSum9\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2                                    (PowerStation)
                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date,Hour, ");

                #endregion

                #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date,Hour, ");

                #endregion

                #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date,Hour, ");

                #endregion

                #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date,Hour, ");

                #endregion

                #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date,Hour ");

                #endregion


                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                String GroupByString = String.Empty;

                #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2 (PowerStation)

                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    GroupByString = " GROUP BY " + "Date,Hour";

                #endregion

                #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    GroupByString = " GROUP BY " + "Date,Hour";


                #endregion

                #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    GroupByString = " GROUP BY " + "Date,Hour";


                #endregion

                #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    GroupByString = " GROUP BY " + "Date,Hour";


                #endregion

                #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    GroupByString = " GROUP BY " + "Date,Hour";


                #endregion

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

            #endregion


            #endregion

            
            #region Not SUM SELECT For Parameters
            else if (ContainParam)
            {

                if ((Regex.IsMatch(SelectString, @"\bMin\b", RegexOptions.IgnoreCase)
               || Regex.IsMatch(SelectString, @"\bMax\b", RegexOptions.IgnoreCase)) && !(Regex.IsMatch(SelectString, @"\bFunctions\b", RegexOptions.IgnoreCase)))

                {
                    #region Select
                    SelectColumns = String.Empty;
                        _ParamSumType = "8";
                        SelectColumns = "SELECT Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    step = 6;
                    #endregion
                }
             
                
                else
                {
                    #region SELECT

                     SelectColumns = String.Empty;

                    #region Make SELECT

                    #region Contain Sum Type 0 (Fixed Or Mixed)

                    if (ContainSumType0)
                    {
                        _ParamSumType = "0";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.UnitCode," +
                                        "Sellers.FixedParameters.UnitID ," +
                                        "Sellers.FixedParameters.BillUnitID ," +
                                        "Sellers.FixedParameters.BillUnitT ," +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                        #endregion

                    #region Contain Sum Type 1 (SumByUnitType)

                    else if (ContainSumType1)
                    {
                        _ParamSumType = "1";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.BillUnitID," +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }
                        #endregion

                    #region Contain Sum Type 2 (SumByPowerStationName)

                    else if (ContainSumType2)
                    {
                        _ParamSumType = "2";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }
                        #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                    else if (ContainSumType3)
                    {
                        _ParamSumType = "3";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                        #endregion

                   #region Contain Sum Type 4 (SumByCompanyTypeName)

                    else if (ContainSumType4)
                    {
                        _ParamSumType = "4";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                        #endregion

                   #region Contain Sum Type 5 (SumByRegionName)

                    else if (ContainSumType5)
                    {
                        _ParamSumType = "5";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                        #endregion

                   #region Contain Sum Type 6 (SumByDay)

                        #region Contain Sum Type 60 (Fixed & Mixed)

                    else if (ContainSumType60)
                    {
                        _ParamSumType = "60";
                        SelectColumns = "SELECT " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.BillUnitID , " +
                                        "Sellers.FixedParameters.UnitID , " +
                                        "Sellers.FixedParameters.UnitCode , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region Contain Sum Type 61 (SumByUnitType)

                    else if (ContainSumType61)
                    {
                        _ParamSumType = "61";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +

                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.BillUnitID , " +
                                        "Sellers.FixedParameters.BillUnitT , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region Contain Sum Type 62 (SumByPowerStationName)

                    else if (ContainSumType62)
                    {
                        _ParamSumType = "62";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                   
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region Contain Sum Type 63 (SumByCompanyName)

                    else if (ContainSumType63)
                    {
                        _ParamSumType = "63";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region Contain Sum Type 64 (SumByCompanyTypeName)

                    else if (ContainSumType64)
                    {
                        _ParamSumType = "64";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region Contain Sum Type 65 (SumByRegionName)

                    else if (ContainSumType65)
                    {
                        _ParamSumType = "65";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #endregion

                   #region Contain Sum Type 7 (SumByMonth)

                    else if (ContainSumType7)
                    {
                        _ParamSumType = "7";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , ";
                    }

                    #endregion

                    #region Contain Sum Type 8 (SumByJustDate)

                    else if (ContainSumType8)
                    {
                        _ParamSumType = "8";
                        SelectColumns = "SELECT Sellers.FixedParameters.Date , ";
                    }

                    #endregion                       

                    #region Contain Sum Type 9 (SumByJustDate)

                    else if (ContainSumType9)
                    {
                        _ParamSumType = "9";
                        SelectColumns = "SELECT Sellers.FixedParameters.Date ,Sellers.FixedParameters.Hour , ";
                    }

                    #endregion                       


                    #endregion

                    #endregion
                }

                    #region Istesting-From

                    if (IsTesting)
                    {
                        SelectString = SelectString.Insert(0, SelectColumns);
                        SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                                                                                + "' FROM Sellers.FixedParameters");
                    }
                    else
                    {

                        SelectString = SelectString.Insert(0, SelectColumns);
                        SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                                + " into SellersParam." + ParamName +
                                                                                " FROM Sellers.FixedParameters");
                    }

                    #endregion
                
                    #region INNER JOINs

                    foreach (String Str in ArrayListUsedParams)
                    {
                        DataRow[] FoundRows =
                            DataTableParams.Select("EnglishName = '" + Str + "'");

                            #region Check Non-Fixed Paameters (Mixed & Sum)

                        if (FoundRows[0]["Category"].ToString() != "Fx")
                        {
                            String CurentParamSum = Parameters.GetParamSumType(Str, "Sellers");
                            String ObjectName = Parameters.GetParamLastObjectP(Str, "Sellers");

                                #region Sum Type 0 (Mixed Parameters)

                            if (CurentParamSum == "0")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.UnitID = " +
                                                         ObjectName + ".[UnitID] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] " +
                                                         " AND Sellers.FixedParameters.Hour = " +
                                                         ObjectName + ".[Hour] ";
                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }

                                #endregion

                                #region Sum Type 1 (SumByUnitType)

                            else if (CurentParamSum == "1")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.PowerStationCode = " +
                                                         ObjectName + ".PowerStationCode" +
                                                         " AND Sellers.FixedParameters.BillUnitID = " +
                                                         ObjectName + ".[BillUnitID] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] " +
                                                         " AND Sellers.FixedParameters.Hour = " +
                                                         ObjectName + ".[Hour] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }

                                #endregion

                                #region Sum Type 2 (SumByPowerStation)

                            else if (CurentParamSum == "2")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.PowerStationName = " +
                                                         ObjectName + ".[PowerStationName]" +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date]" +
                                                         " AND Sellers.FixedParameters.Hour = " +
                                                         ObjectName + ".[Hour] ";
                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                            }

                                #endregion

                                #region Sum Type 3 (SumByCompany)

                            else if (CurentParamSum == "3")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.CompanyName = " +
                                                         ObjectName + ".[CompanyName]" +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date]" +
                                                         " AND Sellers.FixedParameters.Hour = " +
                                                         ObjectName + ".[Hour] ";
                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                            }

                                #endregion

                                #region Sum Type 4 (SumByCompanyType)

                            else if (CurentParamSum == "4")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.CompanyTypeName = " +
                                                         ObjectName + ".[CompanyTypeName]" +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date]" +
                                                         " AND Sellers.FixedParameters.Hour = " +
                                                         ObjectName + ".[Hour] ";
                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                            }

                                #endregion

                                #region Sum Type 5 (SumByRegion)

                            else if (CurentParamSum == "5")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.RegionName = " +
                                                         ObjectName + ".[RegionName]" +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date]" +
                                                         " AND Sellers.FixedParameters.Hour = " +
                                                         ObjectName + ".[Hour] ";
                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                            }

                                #endregion

                                #region Sum Type 6 (SumByDay)

                                #region ContainSumType60

                            else if (CurentParamSum == "6" || CurentParamSum == "60")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.UnitID = " +
                                                         ObjectName + ".[UnitID] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                                #endregion

                                #region ContainSumType61

                            else if (CurentParamSum == "61")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.PowerStationCode = " +
                                                         ObjectName + ".[PowerStationCode] " +
                                                         " AND Sellers.FixedParameters.BillUnitID = " +
                                                         ObjectName + ".[BillUnitID] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                                #endregion

                                #region ContainSumType62

                            else if (CurentParamSum == "62")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.PowerStationCode = " +
                                                         ObjectName + ".[PowerStationCode] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                                #endregion

                                #region ContainSumType63

                            else if (CurentParamSum == "63")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.CompanyName = " +
                                                         ObjectName + ".[CompanyName] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                                #endregion

                                #region ContainSumType64

                            else if (CurentParamSum == "64")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.CompanyTypeName = " +
                                                         ObjectName + ".[CompanyTypeName] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                                #endregion

                                #region ContainSumType65

                            else if (CurentParamSum == "65")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.RegionName = " +
                                                         ObjectName + ".[RegionName] " +
                                                         " AND Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                                #endregion

                                #endregion

                                #region Sum Type 7 (SumByMonth)

                            else if (CurentParamSum == "7")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.RegionName  = " +
                                                         ObjectName + ".RegionName ";
                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }

                            #endregion

                                #region ContainSumType8

                            else if (CurentParamSum == "8")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] " ;

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                            #endregion

                               #region ContainSumType9

                            else if (CurentParamSum == "9")
                            {
                                String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                         " ON Sellers.FixedParameters.Date = " +
                                                         ObjectName + ".[Date] "+
                                                            " AND Sellers.FixedParameters.Hour = " +
                                                         ObjectName + ".[Hour] ";

                                SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                            }
                            #endregion


                        }

                            #endregion
                         

                        else
                        {
                            #region Check Fixed Parameters
                            var TextToFind = new Regex(@"\b" + Str + @"\b");
                            SelectString = TextToFind.Replace(SelectString, "[" + Str + "]");
                            #endregion
                        }

                    }

                    #region step 1
                    if (step == 1)
                    {
                        const string InsertInnerJoinS =
                            " INNER JOIN   Sellers.PwPrP ON Sellers.FixedParameters.PowerStationCode =" +
                            "Sellers.PwPrP.PowerStationCode AND Sellers.FixedParameters.UnitID = Sellers.PwPrP.UnitID AND" +
                            " Sellers.FixedParameters.Date = Sellers.PwPrP.Date AND Sellers.FixedParameters.Hour = Sellers.PwPrP.Hour";

                        SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoinS);
                    }
                    #endregion

                    #region step3
                    if (step == 3)
                    {
                        const string InsertInnerJoinS =
                            " INNER JOIN   Sellers.FL_EPG ON Sellers.FixedParameters.UnitID = Sellers.FL_EPG.UnitID AND" +
                            " Sellers.FixedParameters.Date = Sellers.FL_EPG.Date AND Sellers.FixedParameters.Hour = Sellers.FL_EPG.Hour";

                        SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoinS);
                    }
                    #endregion

                    #endregion

                    #region GROUP BY

                    String InsertGROUPBY = String.Empty;

                    #region Fixed Parameters GROUP BY

                        #region Contain Sum Type 0 In Statement

                    if (ContainSumType0)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyName, " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.UnitID ," +
                                        "Sellers.FixedParameters.UnitCode ," +
                                        "Sellers.FixedParameters.BillUnitID, " +
                                        "Sellers.FixedParameters.BillUnitT, " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                        #endregion

                        #region Contain Sum Type 1 In Statement(SumByUnitType)

                    else if (ContainSumType1)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyName, " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.BillUnitID," +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                        #endregion

                        #region Contain Sum Type 2 In Statement (SumByPowerStation)

                    else if (ContainSumType2)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName, " +
                                        "Sellers.FixedParameters.CompanyName, " +
                                        "Sellers.FixedParameters.PowerStationName, " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour, ";
                    }

                        #endregion

                        #region Contain Sum Type 3 In Statement (SumByCompany)

                    else if (ContainSumType3)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName, " +
                                        "Sellers.FixedParameters.CompanyName, " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour, ";
                    }

                        #endregion

                        #region Contain Sum Type 4 In Statement (SumByCompanyType)

                    else if (ContainSumType4)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName, " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour, ";
                    }

                        #endregion

                        #region Contain Sum Type 5 In Statement (SumByRegion)

                    else if (ContainSumType5)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour, ";
                    }

                        #endregion

                        #region Contain Sum Type 6 In Statement (SumByDay)

                        #region ContainSumType60

                    else if (ContainSumType60)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.BillUnitID , " +
                                        "Sellers.FixedParameters.UnitID , " +
                                        "Sellers.FixedParameters.UnitCode , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region ContainSumType61

                    else if (ContainSumType61)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName , " +

                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.BillUnitID , " +
                                        "Sellers.FixedParameters.BillUnitT , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region ContainSumType62

                    else if (ContainSumType62)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName , " +
                                     
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region ContainSumType63

                    else if (ContainSumType63)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }

                        #endregion

                        #region ContainSumType64

                    else if (ContainSumType64)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #region ContainSumType65

                    else if (ContainSumType65)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                        #endregion

                        #endregion

                        #region Contain Sum Type 7 In Statement (SumByMonth)

                    else if (ContainSumType7)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.RegionName, ";
                    }

                    #endregion

                       #region Contain Sum Type 8 In Statement (SumByJustByDate)

                    else if (ContainSumType8)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.Date, ";
                    }

                    #endregion

                    #region Contain Sum Type 9 In Statement (SumByJustByDateHour)

                    else if (ContainSumType9)
                    {
                        InsertGROUPBY = " GROUP BY " +
                                        "Sellers.FixedParameters.Date,Sellers.FixedParameters.Hour, ";
                    }

                    #endregion

                    #endregion

                    #region Non-Fixed Columns GROUP BY

                    for (int i = 0; i < ArrayListUsedParams.Count; i++)
                    {
                        String Str = ArrayListUsedParams[i].ToString();
                        String ObjectName = Parameters.GetParamLastObjectP(Str, "Sellers");
                        if (i < ArrayListUsedParams.Count - 1)
                            InsertGROUPBY += ObjectName + "." + Str + ", ";
                        else
                            InsertGROUPBY += ObjectName + "." + Str;
                    }

                    #endregion

                    #region Insert GROUP BY Text

                    if (ContainSumType1 || ContainSumType2 || ContainSumType3 ||
                        ContainSumType4 || ContainSumType5 || ContainSumType60 ||
                        ContainSumType61 || ContainSumType62 || ContainSumType63 ||
                        ContainSumType64 || ContainSumType65 || ContainSumType7||ContainSumType8||ContainSumType9)
                    {
                        SelectString = SelectString.Insert(SelectString.Length, InsertGROUPBY);
                    }


                   if (step == 0)
                    {
                    }
                    if (step == 1)
                    {

                    }
                    if (step == 6)  // min() and max()

                {
                    InsertGROUPBY = " GROUP BY Sellers.FixedParameters.Date, " +
                                 "Sellers.FixedParameters.Hour  ";
                    SelectString = SelectString.Insert(SelectString.Length, InsertGROUPBY);
                }
                    #endregion

                    #endregion
                

            }
                #endregion

            #region SELECT For Numbers Without Param

            else
            {
                _ParamSumType = "0";
                const String SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                             "Sellers.FixedParameters.CompanyName , " +
                                             "Sellers.FixedParameters.PowerStationCode , " +
                                             "Sellers.FixedParameters.PowerStationName , " +
                                             "Sellers.FixedParameters.UnitType , " +
                                             "Sellers.FixedParameters.UnitID , " +
                                             "Sellers.FixedParameters.Date, " +
                                             "Sellers.FixedParameters.Hour , ";

                #region Istesting-From
                if (IsTesting)
                {                SelectString = SelectString.Insert(0, SelectColumns);
                SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                    + "' FROM Sellers.FixedParameters");
                }
                else
                {
                                  
                                    SelectString = SelectString.Insert(0, SelectColumns);
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM Sellers.FixedParameters");
                }
                #endregion

            }
            #endregion

            #region Return Results
            
            return SelectString;

            #endregion

        }

        #endregion

        #region @@@ String GetBuyersParamString(...) @@@
        /// <summary>
        /// توليد رشته فرمان اس كيو ال براي ايجاد نما
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="SelectString">رشته وارد شده</param>
        /// <param name="DataTableParams">جدول پارامتر های ذخیره شده</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="IsTesting">تعیین تستی بودن یا نبودن فرمان</param>
        /// <param name="Rivision">شماره دوره پارامتر</param>
        /// <returns>رشته ي فرمان اس كيو ال را بر مي گرداند</returns>
        private static String GetBuyersParamString(String ParamName, String SelectString,
                                                   DataTable DataTableParams, ArrayList ArrayListParams,
                                                   Boolean IsTesting, Int32 Rivision)
        {

            #region Prepare Execution

            #region Check String Null Value

            if (String.IsNullOrEmpty(SelectString))
            {
                PersianMessageBox.Show("هیچ دستوری نوشته نشده ، بررسی ممكن نیست!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return String.Empty;
            }
            #endregion

            #region Variables Declaration
            Boolean ContainSumType0 = false;
            Boolean ContainSumType1 = false;
            Boolean ContainSumType2 = false;
            Boolean ContainSumType3 = false;
            Boolean ContainSumType30 = false;
            Boolean ContainSumType31 = false;
            Boolean ContainSumType32 = false;
 
            Boolean ContainParam = false;
            #endregion

            #region Fill ArrayList Used Parameters

            var ArrayListUsedParams = new ArrayList();
            foreach (String Str in ArrayListParams)
            {
                String SearchWord = @"\b" + Str + @"\b";
                if (Regex.IsMatch(SelectString, SearchWord))
                    ArrayListUsedParams.Add(Str);
            }

            #endregion

            if (IsTesting)
            {
                #region Replace Complete Objects Name

                ParamLevel = 0;
                foreach (String Str in ArrayListUsedParams)
                {
                    String SchemaName = Parameters.GetParamLastObjectP(Str, "Buyers");
                    var TextToFind = new Regex(Str + @"\b");

                    ObjectFullName = SchemaName + "." + Str;
                    SelectString = TextToFind.Replace(SelectString, ObjectFullName);



                }

                #endregion

            }
            else
            {
                #region Delete from ParamIn
                Parameters.DelParamIDIN(ParamName, "Buyers");
                #endregion

                #region Replace Complete Objects Name

                ParamLevel = 0;
                foreach (String Str in ArrayListUsedParams)
                {
                    String SchemaName = Parameters.GetParamLastObjectP(Str, "Buyers");
                    Parameters.SetParamIDIN(ParamName, Str, "Buyers");
                    PLevel = Parameters.GetParamLevel(Str, "Buyers");
                    if (PLevel > ParamLevel)
                    {
                        ParamLevel = PLevel;
                    }
                    var TextToFind = new Regex(Str + @"\b");

                    ObjectFullName = SchemaName + "." + Str;
                    SelectString = TextToFind.Replace(SelectString, ObjectFullName);



                }

                #endregion

            }


            #region Check Used Variables For Sum Types
            if (ArrayListUsedParams.Count > 0)
            {
                #region Find Containing of Sum Params
                foreach (String Str in ArrayListUsedParams)
                {
                    String CurentParamSum = Parameters.GetParamSumType(Str, "Buyers");
                    if (CurentParamSum == "0") ContainSumType0 = true;
                    else if (CurentParamSum == "1") ContainSumType1 = true;
                    else if (CurentParamSum == "2") ContainSumType2 = true;
                    else if (CurentParamSum == "3") ContainSumType3 = true;
                    else if (CurentParamSum == "30") ContainSumType30 = true;
                    else if (CurentParamSum == "31") ContainSumType31 = true;
                    else if (CurentParamSum == "32") ContainSumType32 = true;
        
                }
                #endregion

                #region Set Current Param SumType
                if (ContainSumType0) _ParamSumType = "0";
                else if (ContainSumType1) _ParamSumType = "1";
                else if (ContainSumType2) _ParamSumType = "2";
                else if (ContainSumType3) _ParamSumType = "3";
                else if (ContainSumType30)_ParamSumType = "30";
                else if (ContainSumType31)_ParamSumType = "31";
                else if (ContainSumType32)_ParamSumType = "32";
                #endregion

                ContainParam = true;
            }

            #endregion

            #endregion

            #region SUM SELECT

            #region Sum1 (SumByHour)
            if (Regex.IsMatch(SelectString, @"\bSum1\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "1";
                var SumTextToFind = new Regex(@"\bSum1\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT
                SelectString = SelectString.Insert(0, "SELECT " + "CompanyTypeName, " +
                                                      "CompanyName , " + "Date , ");

                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName  + "' FROM " + Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Buyers"));
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'" + " into BuyersParam." + ParamName + " FROM " + Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Buyers"));
                }

         
                #endregion

                #region Make GROUP BY
                const String GroupByString = " GROUP BY " + "CompanyTypeName, " +
                                             "CompanyName , " + "Date ";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);
                #endregion

            }
                #endregion

                #region Sum2 (SumByDate)
            else if (Regex.IsMatch(SelectString, @"\bSum2\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "2";
                var SumTextToFind = new Regex(@"\bSum2\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT
                SelectString = SelectString.Insert(0, "SELECT " + "CompanyTypeName, " +
                                                      "CompanyName , ");

                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "' FROM " + Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Buyers"));
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'" + " into BuyersParam." + ParamName + " FROM " + Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Buyers"));
                }
                #endregion

                #region Make GROUP BY
                const String GroupByString = " GROUP BY " + "CompanyTypeName, " +
                                             "CompanyName ";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);
                #endregion

            }
                #endregion

                #region Sum3 (SumByCompany)
            else if (Regex.IsMatch(SelectString, @"\bSum3\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "3";
                var SumTextToFind = new Regex(@"\bSum3\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Set Param Sum Type

                if (ContainSumType0)
                    _ParamSumType = "30";
                else if (ContainSumType1)
                    _ParamSumType = "31";
                else if (ContainSumType2)
                    _ParamSumType = "32";
             

                #endregion

                #region Make SELECT

                #region Contain Sum Type 0 (Fixed & Mixed)

                if (ContainSumType0)
                    SelectString =
                        SelectString.Insert(0, "SELECT Date , "+"Hour ,");

                    #endregion

                    #region Contain Sum Type 1 (SumByUnitType)

                else if (ContainSumType1)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " +
                                               "PowerStationName , " + "UnitType , " + "Date , ");

                    #endregion

                    #region Contain Sum Type 2 (SumByPowerStationName)

                else if (ContainSumType2)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " +
                                               "PowerStationName , " + "Date , ");

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " + "Date , ");

                #endregion

                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "' FROM " + Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Buyers"));
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'" + " into BuyersParam." + ParamName + " FROM " + Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Buyers"));
                }



                #endregion

                #region Make GROUP BY

                String GroupByString = String.Empty;

                #region Contain Sum Type 0

                if (ContainSumType0)
                    GroupByString = " GROUP BY " +
                                    "Date , " + "Hour";
                    #endregion

                    #region Contain Sum Type 1 (SumByUnitType)

                else if (ContainSumType1)
                    GroupByString = " GROUP BY " +
                                    "RegionName , " + "CompanyTypeName , " +
                                    "CompanyName , " + "PowerStationName , " +
                                    "UnitType , " + "Date";

                    #endregion

                    #region Contain Sum Type 2 (SumByPowerStationName)

                else if (ContainSumType2)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName , " +
                                    "PowerStationName , " + "Date";

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName , " + "Date";

                #endregion

                
                

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }
                #endregion


                #endregion

            #region Not SUM (Fixed , Mixed Or Sum)
            else if (ContainParam)
            {
            

                #region SELECT
                String SelectColumns = String.Empty;

                #region Make SELECT

                #region Contain Sum Type 0 (Fixed Or Mixed)
                if (ContainSumType0)
                {
                    _ParamSumType = "0";
                    SelectColumns = "SELECT Buyers.FixedParameters.CompanyTypeName , " +
                                    "Buyers.FixedParameters.CompanyName , " +
                                    "Buyers.FixedParameters.Date, " +
                                    "Buyers.FixedParameters.Hour , ";
                }
                    #endregion

                    #region Contain Sum Type 1 (SumByHour)
                else if (ContainSumType1)
                {
                    _ParamSumType = "1";
                    SelectColumns = "SELECT Buyers.FixedParameters.CompanyTypeName , " +
                                    "Buyers.FixedParameters.CompanyName , " +
                                    "Buyers.FixedParameters.Date , ";
                }
                    #endregion

                    #region Contain Sum Type 2 (SumByDate)
                else if (ContainSumType2)
                {
                    _ParamSumType = "2";
                    SelectColumns = "SELECT Buyers.FixedParameters.CompanyTypeName , " +
                                    "Buyers.FixedParameters.CompanyName , "+
                                    "Buyers.FixedParameters.Hour , ";
                }
                    #endregion

                    #region Contain Sum Type 3 (SumByCompany)

                    #region Contain Sum Type 30 (Fixed & Mixed)

                else if (ContainSumType30)
                {
                    _ParamSumType = "30";
                    SelectColumns = "SELECT " +
                 
                           
                                    "Buyers.FixedParameters.Date , " +
                                    "Buyers.FixedParameters.Hour , ";
                }
                    #endregion

                    #region Contain Sum Type 31 (SumByCompany and 1)

                else if (ContainSumType31)
                {
                    _ParamSumType = "31";
                    SelectColumns = "SELECT Buyers.FixedParameters.Date, ";
                    
                }
                    #endregion

                    #region Contain Sum Type 32 (SumBycompany and 2)

                else if (ContainSumType32)
                {
                    _ParamSumType = "32";
                    SelectColumns = "SELECT Buyers.FixedParameters.Hour , ";
                }
                #endregion

                
                #endregion


                #endregion

                SelectString = SelectString.Insert(0, SelectColumns);

                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "' FROM Buyers.FixedParameters");
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'" + " into BuyersParam." + ParamName + " FROM Buyers.FixedParameters");
                }

                #endregion

                #region INNER JOINs
                foreach (String Str in ArrayListUsedParams)
                {
                    DataRow[] FoundRows =
                        DataTableParams.Select("EnglishName = '" + Str + "'");

                    #region Check Non-Fixed Paameters (Mixed & Sum)
                    if (FoundRows[0]["Category"].ToString() != "Fx")
                    {
                        String CurentParamSum = Parameters.GetParamSumType(Str, "Buyers");
                        String ObjectName = Parameters.GetParamLastObjectP(Str, "Buyers");

                        #region Sum Type 0 (Mixed Parameters)
                        if (CurentParamSum == "0")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Buyers.FixedParameters.CompanyName = " +
                                                     ObjectName + ".[CompanyName] " +
                                                     " AND Buyers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] " +
                                                     " AND Buyers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region Sum Type 1 (SumByHour)
                        else if (CurentParamSum == "1")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Buyers.FixedParameters.CompanyName = " +
                                                     ObjectName + ".[CompanyName]" +
                                                     " AND Buyers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region Sum Type 2 (SumByDate)
                        else if (CurentParamSum == "2")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Buyers.FixedParameters.CompanyName = " +
                                                     ObjectName + ".[CompanyName]";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region Sum Type 3 (SumByCompany)

                            #region ContainSumType30
                        else if (CurentParamSum == "30")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Buyers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date]" +
                                                     " AND Buyers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                        #endregion
                  
                        #endregion


                    }
                        #endregion

                        #region Check Fixed Parameters
                    else
                    {
                        var TextToFind = new Regex(@"\b" + Str + @"\b");
                        SelectString = TextToFind.Replace(SelectString, "[" + Str + "]");
                    }
                    #endregion

                } // End of "foreach"
                #endregion

                #region GROUP BY

                String InsertGROUPBY = String.Empty;

                #region Fixed Parameters GROUP BY

                #region Contain Sum Type 0 In Statement
                if (ContainSumType0)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Buyers.FixedParameters.CompanyTypeName, " +
                                    "Buyers.FixedParameters.CompanyName, " +
                                    "Buyers.FixedParameters.Date, " +
                                    "Buyers.FixedParameters.Hour, ";
                }
                    #endregion

                    #region Contain Sum Type 1 In Statement(SumByHour)
                else if (ContainSumType1)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Buyers.FixedParameters.CompanyTypeName, " +
                                    "Buyers.FixedParameters.CompanyName, " +
                                    "Buyers.FixedParameters.Date, ";
                }
                    #endregion

                    #region Contain Sum Type 2 In Statement (SumByDate)
                else if (ContainSumType2)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Buyers.FixedParameters.CompanyTypeName, " +
                                    "Buyers.FixedParameters.CompanyName, ";
                }
                    #endregion

                    #region Contain Sum Type 3 In Statement (SumByCompany)

                    #region ContainSumType30
                else if (ContainSumType30)
                {
                    InsertGROUPBY = " GROUP BY " +
                  
                     
                     
                                    "Buyers.FixedParameters.Date , " +
                                    "Buyers.FixedParameters.Hour , ";
                }
                #endregion
                #endregion


                #endregion

                #region Non-Fixed Columns GROUP BY

                for (int i = 0; i < ArrayListUsedParams.Count; i++)
                {
                    String Str = ArrayListUsedParams[i].ToString();
                    String ObjectName = Parameters.GetParamLastObjectP(Str, "Buyers");
                    if (i < ArrayListUsedParams.Count - 1)
                        InsertGROUPBY += ObjectName + "." + Str + ", ";
                    else
                        InsertGROUPBY += ObjectName + "." + Str;
                }

                #endregion

                #region Insert GROUP BY Text

                if (ContainSumType1 || ContainSumType2 || ContainSumType30 ||
                    ContainSumType31 || ContainSumType32)
                    SelectString = SelectString.Insert(SelectString.Length, InsertGROUPBY);
                #endregion

                #endregion

            }
                #endregion

            #region SELECT For Numbers Without Param
            else
            {
                _ParamSumType = "0";
                const String SelectColumns = "SELECT " +
                                             "Buyers.FixedParameters.CompanyTypeName , " +
                                             "Buyers.FixedParameters.CompanyName , " +
                                             "Buyers.FixedParameters.Date, " +
                                             "Buyers.FixedParameters.Hour , ";
                SelectString = SelectString.Insert(0, SelectColumns);

  

                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName  + "'  FROM Buyers.FixedParameters");
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "' " + " into BuyersParam." + ParamName + " FROM Buyers.FixedParameters");
                }
            }

            #endregion

            #region Return Results



            return SelectString;

            #endregion

        }
        #endregion

        #region @@@ String GetTransferParamString(...) @@@

        /// <summary>
        /// توليد رشته فرمان اس كيو ال براي ايجاد نما
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="SelectString">رشته وارد شده</param>
        /// <param name="DataTableParams">جدول پارامتر های ذخیره شده</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="IsTesting">تعیین تستی بودن یا نبودن فرمان</param>
        /// <param name="Rivision">شماره دوره پارامتر</param>
        /// <returns>رشته ي فرمان اس كيو ال را بر مي گرداند</returns>
        private static String GetTransferParamString(String ParamName, String SelectString,
                                                     DataTable DataTableParams, ArrayList ArrayListParams,
                                                     Boolean IsTesting, Int32 Rivision)
        {

            #region Prepare Execution

            #region Check String Null Value

            if (String.IsNullOrEmpty(SelectString))
            {
                PersianMessageBox.Show("هیچ دستوری نوشته نشده ، بررسی ممكن نیست!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return String.Empty;
            }
            #endregion

            #region Variables Declaration
            Boolean ContainSumType0 = false;
            Boolean ContainSumType1 = false;
            Boolean ContainSumType2 = false;
            Boolean ContainSumType3 = false;
            Boolean ContainParam = false;
            #endregion

            #region Fill ArrayList Used Parameters

            ArrayList ArrayListUsedParams = new ArrayList();
            foreach (String Str in ArrayListParams)
            {
                String SearchWord = @"\b" + Str + @"\b";
                if (Regex.IsMatch(SelectString, SearchWord))
                    ArrayListUsedParams.Add(Str);
            }

            #endregion

            #region Delete from ParamIn
            Parameters.DelParamIDIN(ParamName, "Transfer");
            #endregion

            #region Replace Complete Objects Name

            ParamLevel = 0;

            foreach (String Str in ArrayListUsedParams)
            {
                String SchemaName = Parameters.GetParamLastObjectP(Str, "Transfer");
                Parameters.SetParamIDIN(ParamName, Str, "Transfer");
                PLevel = Parameters.GetParamLevel(Str, "Transfer");
                if (PLevel > ParamLevel)
                {
                    ParamLevel = PLevel;
                }
                Regex TextToFind = new Regex(Str + @"\b");
                String ObjectFullName = SchemaName + "." + Str;
                SelectString = TextToFind.Replace(SelectString, ObjectFullName);
            }

            #endregion

            #region Check Used Variables For Sum Types

            if (ArrayListUsedParams.Count > 0)
            {
                #region Find Containing of Sum Params
                foreach (String Str in ArrayListUsedParams)
                {
                    String CurentParamSum = Parameters.GetParamSumType(Str, "Transfer");
                    if (CurentParamSum == "0") ContainSumType0 = true;
                    else if (CurentParamSum == "1") ContainSumType1 = true;
                    else if (CurentParamSum == "2") ContainSumType2 = true;
                    else if (CurentParamSum == "3") ContainSumType3 = true;
                }
                #endregion

                #region Set Current Param SumType
                if (ContainSumType0) _ParamSumType = "0";
                else if (ContainSumType1) _ParamSumType = "1";
                else if (ContainSumType2) _ParamSumType = "2";
                else if (ContainSumType3) _ParamSumType = "3";
                #endregion

                ContainParam = true;
            }

            #endregion

            #endregion

           #region SUM SELECT

            #region For Sum1 (SumByVoltage Level)
            if (Regex.IsMatch(SelectString, @"\bSum1\b", RegexOptions.IgnoreCase))
            {
                #region Replace SUM Expression sum1

                _ParamSumType = "1";
                Regex SumTextToFind = new Regex(@"\bSum1\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT
                SelectString = SelectString.Insert(0, "SELECT " +

                                                      "CompanyCode , " +
                                                      "Date , " +
                                                      "Hour ,"+
                                                      "EquipmentType , " +
                                                      "EquipmentPosition , " +
                                                      "VoltageLevel,");
     

                SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                       + " into TransferParam." + ParamName + " FROM " +
                                                                       Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Transfer"));
                #endregion

                #region Make GROUP BY
                const String GroupByString = " GROUP BY " +
                                             //"Transfer.FixedParameters.CompanyName , " +
                                             //   "Transfer.FixedParameters.Date , " +
                                             //   "Transfer.FixedParameters.EquipmentType , " +
                                             //   "Transfer.FixedParameters.EquipmentPosition , " +
                                             //   "Transfer.FixedParameters.VoltageLevel";
                                             "CompanyCode , " +
                                             "Date , " +
                                             "Hour ," +
                                             "EquipmentType , " +
                                             "EquipmentPosition , " +
                                             "VoltageLevel";

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);
                #endregion
            }
                #endregion

            #region For Sum2 (SumByEquipmentPosition)
            else if (Regex.IsMatch(SelectString, @"\bSum2\b", RegexOptions.IgnoreCase))
            {
                #region Replace SUM Expression sum2

                _ParamSumType = "2";
                var SumTextToFind = new Regex(@"\bSum2\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT
                SelectString = SelectString.Insert(0, "SELECT " +
                                                      //"Transfer.FixedParameters.CompanyName , " +
                                                      //"Transfer.FixedParameters.Date , " +
                                                      //"Transfer.FixedParameters.EquipmentType , " +
                                                      //"Transfer.FixedParameters.EquipmentPosition , ");
                                                      "CompanyCode , " +
                                                      "Date , " +
                                                      "Hour ," +
                                                      "EquipmentType , " +
                                                      "EquipmentPosition , ");

                SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                       + " into TransferParam." + ParamName + " FROM " +
                                                                       Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Transfer"));
                #endregion

                #region Make GROUP BY
                const String GroupByString = " GROUP BY " +
                                             //"Transfer.FixedParameters.CompanyName , " +
                                             //"Transfer.FixedParameters.Date , " +
                                             //"Transfer.FixedParameters.EquipmentType , " +
                                             //"Transfer.FixedParameters.EquipmentPosition";
                                             "CompanyCode , " +
                                             "Date , " +
                                             "Hour ," +
                                             "EquipmentType , " +
                                             "EquipmentPosition";

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);
                #endregion
            }
                #endregion

            #region For Sum3 (SumByMonth)
            else if (Regex.IsMatch(SelectString, @"\bSum3\b", RegexOptions.IgnoreCase))
            {
                #region Replace SUM Expression sum3

                _ParamSumType = "3";
                var SumTextToFind = new Regex(@"\bSum3\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT
                SelectString = SelectString.Insert(0, "SELECT " +
                                                      //"Transfer.FixedParameters.CompanyName ," +
                                                      //"Transfer.FixedParameters.Date ," );
                                                      "CompanyCode ," +
                                                      "Date ,");

                SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                       + " into TransferParam." + ParamName + " FROM " +
                                                                       Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(), "Transfer"));
                #endregion

                #region Make GROUP BY
                const String GroupByString = " GROUP BY " +
                                             //      "Transfer.FixedParameters.CompanyName," + "Transfer.FixedParameters.Date";
                                             "CompanyName," + "Date";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);
                #endregion
            }
                #endregion

                #endregion

           #region Not SUM SELECT For Parameters
            else if (ContainParam)
            {

                #region SELECT

                String SelectColumns = String.Empty;

                #region Make SELECT

                #region Contain Sum Type 0 (Fixed Or Mixed)
                if (ContainSumType0)
                {
                    _ParamSumType = "0";
                    SelectColumns = "SELECT " +
                                    "Transfer.FixedParameters.CompanyCode , " +
                                    "Transfer.FixedParameters.Date , " +
                                    "Transfer.FixedParameters.Hour , " +
                                    "Transfer.FixedParameters.EquipmentType , " +
                                    "Transfer.FixedParameters.EquipmentCode , " +
                                    "Transfer.FixedParameters.EquipmentPosition , " +
                                    "Transfer.FixedParameters.VoltageLevel, ";
                }
                    #endregion

                #region Contain Sum Type 1 (SumByLineType)

                else if (ContainSumType1)
                {
                    _ParamSumType = "1";
                    SelectColumns = "SELECT " +
                                    "Transfer.FixedParameters.CompanyName , " +
                                    "Transfer.FixedParameters.Date , " +
                                    "Transfer.FixedParameters.Hour , " +
                                    "Transfer.FixedParameters.EquipmentType , " +
                                    "Transfer.FixedParameters.EquipmentPosition , " +
                                    "Transfer.FixedParameters.VoltageLevel,";
                }
                    #endregion

                #region Contain Sum Type 2 (SumByRegion)

                else if (ContainSumType2)
                {
                    _ParamSumType = "2";
                    SelectColumns = "SELECT " +
                                    "Transfer.FixedParameters.CompanyName , " +
                                    "Transfer.FixedParameters.Date , " +
                                    "Transfer.FixedParameters.EquipmentType ," +
                                    "Transfer.FixedParameters.EquipmentPosition,";
                }
                    #endregion

                #region Contain Sum Type 3 (SumByMonth)

                else if (ContainSumType3)
                {
                    _ParamSumType = "3";
                    SelectColumns = "SELECT " +
                                    "Transfer.FixedParameters.CompanyName ," +
                                    "Transfer.FixedParameters.Date ,";
                }
                #endregion

                #endregion
                if (IsTesting)
                {
 
                    SelectString = SelectString.Insert(0, SelectColumns);
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                                                                            + "' FROM Transfer.FixedParameters");
                }
                else
                {

                    SelectString = SelectString.Insert(0, SelectColumns);
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into TransferParam." + ParamName + " FROM Transfer.FixedParameters");
                }
               

                #endregion

                #region INNER JOINs
                foreach (String Str in ArrayListUsedParams)
                {
                    DataRow[] Row = DataTableParams.Select("EnglishName = '" + Str + "'");

                    #region Check Non-Fixed Parameters (Mixed & Sum)

                    if (Row[0]["Category"].ToString() != "Fx")
                    {
                        String CurentParamSum = Parameters.GetParamSumType(Str, "Transfer");
                        String ObjectName = Parameters.GetParamLastObjectP(Str, "Transfer");

                        #region Sum Type 0 (Mixed Parameters)
                        if (CurentParamSum == "0")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Transfer.FixedParameters.EquipmentCode = " +
                                                     ObjectName + ".EquipmentCode" +
                                                     " AND Transfer.FixedParameters.Date = " +
                                                     ObjectName + ".Date "+
                                                       " AND Transfer.FixedParameters.Hour = " +
                                             ObjectName + ".[Hour] "+
                                                       " AND Transfer.FixedParameters.CompanyCode= " +
                                             ObjectName + ".CompanyCode ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                        #region Sum Type 1
                        else if (CurentParamSum == "1")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Transfer.FixedParameters.[Voltagelevel]= " +
                                                     ObjectName + ".[VoltageLevel] " +
                                                     "AND  Transfer.FixedParameters.[EquipmentType]=" +
                                                     ObjectName + ".[EquipmentType] " +
                                                     " AND Transfer.FixedParameters.[Date] = " +
                                                     ObjectName + ".[Date] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                        #region Sum Type 2 (SumByRegionName)
                        else if (CurentParamSum == "2")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Transfer.FixedParameters.[EquipmentPosition]= " +
                                                     ObjectName + ".[EquipmentPosition] " +
                                                     "AND  Transfer.FixedParameters.[EquipmentType]=" +
                                                     ObjectName + ".[EquipmentType] " +
                                                     " AND Transfer.FixedParameters.[Date] = " +
                                                     ObjectName + ".[Date] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                        #region Sum Type 3 (SumByMonth)
                        else if (CurentParamSum == "3")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Transfer.FixedParameters.CompanyName  = " +
                                                     ObjectName + ".[CompanyName] " +
                                                     "AND  Transfer.FixedParameters.[Date]=" +
                                                     ObjectName + ".[Date] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                        #endregion

                    }

                        #endregion

                        #region Check Fixed Parameters
                    else
                    {
                        var TextToFind = new Regex(@"\b" + Str + @"\b");
                        SelectString = TextToFind.Replace(SelectString, "[" + Str + "]");
                    }
                    #endregion

                } // End of "foreach"
                #endregion

                #region GROUP BY

                String InsertGROUPBY = String.Empty;

                #region Fixed Parameters GROUP BY

                #region Contain Sum Type 0
                if (ContainSumType0)
                    InsertGROUPBY = " GROUP BY " +
                                    "CompanyName , " +
                                    "Date , " +
                                    "EquipmentType , " +
                                    "EquipmentCode , " +
                                    "VoltageLevel, ";
                    #endregion

                    #region Contain Sum Type 1 (SumByLineType)
                else if (ContainSumType1)
                    InsertGROUPBY = " GROUP BY " +
                                    "Transfer.FixedParameters.CompanyName , " +
                                    "Transfer.FixedParameters.Date , " +
                                    "Transfer.FixedParameters.EquipmentType , " +
                                    "Transfer.FixedParameters.EquipmentPosition , " +
                                    "Transfer.FixedParameters.VoltageLevel,";
                    #endregion

                    #region Contain Sum Type 2 (SumByRegionName)
                else if (ContainSumType2)
                    InsertGROUPBY = " GROUP BY " +
                                    "Transfer.FixedParameters.CompanyName , " +
                                    "Transfer.FixedParameters.Date , " +
                                    "Transfer.FixedParameters.EquipmentType , " +
                                    "Transfer.FixedParameters.EquipmentPosition,";


                    #endregion

                    #region Contain Sum Type 3 (SumByMonth)
                else if (ContainSumType3)
                    InsertGROUPBY = " GROUP BY " +
                                    "Transfer.FixedParameters.CompanyName, " +
                                    "Transfer.FixedParameters.Date , ";
                #endregion

                #endregion

                #region Non-Fixed Columns GROUP BY
                for (int i = 0; i < ArrayListUsedParams.Count; i++)
                {
                    String Str = ArrayListUsedParams[i].ToString();
                    String ObjectName = Parameters.GetParamLastObjectP(Str, "Transfer");
                    if (i < ArrayListUsedParams.Count - 1)
                        InsertGROUPBY += ObjectName + "." + Str + ", ";
                    else InsertGROUPBY += ObjectName + "." + Str;
                } // End Of "for" Loop
                #endregion

                #region Insert GROUP BY Text
                if (ContainSumType1 || ContainSumType2 || ContainSumType3)
                    SelectString = SelectString.Insert(SelectString.Length, InsertGROUPBY);
                #endregion

                #endregion

            }
                #endregion

           #region SELECT For Numbers Without Param

            else
            {
                _ParamSumType = "0";
                const String SelectColumns = "SELECT " +
                                             "CompanyCode , " +
                                             "Date , " +
                                             "EquipmentType , " +
                                             "EquipmentCode , " +
                                             "EquipmentPosition , " +
                                             "VoltageLevel, ";

                if (IsTesting)
                {

                    SelectString = SelectString.Insert(0, SelectColumns);
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                                                                            + "' FROM Transfer.FixedParameters");
                }
                else
                {

                    SelectString = SelectString.Insert(0, SelectColumns);
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into TransferParam." + ParamName + " FROM Transfer.FixedParameters");
                }
            }
            #endregion

           #region Return Results

     
            return SelectString;

            #endregion

        }

        #endregion


        #endregion

    }

    /// <summary>
    /// كلاس مدیریت توابع مشترك فرم تعریف پارامتر شرطی جدید
    /// </summary>
    /// 
    static class ManageParametersC
    {

        #region Fields

        /// <summary>
        /// تعیین نوع پارامتر از قبیل مجموع بودن یا نبودن
        /// </summary>
        public static String _ParamSumType;

        public  static int  PLevel;
 
        public  static int ParamLevel;
        private static string SelectColumns;

        #endregion

        #region public Methods

        #region String GetParamString(...)

        /// <summary>
        /// تابع بازگرداندن رشته فرمان اس كیو ال برای تولید توابع
        /// </summary>
        /// <param name="PartName">نام بخش از قبیل خریداران ، فروشندگان و غیره</param>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="ParamCode">متن دستور پارامتر</param>
        /// <param name="DataTableParams">لیست پارامتر ها</param>
        /// <param name="ArrayListParams">آرایه پارامتر ها</param>
        /// <param name="IsTesting">تستی بودن یا قطعی بودن فرمان</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <returns>رشته فرمان اس كیو ال</returns>
        public static String GetParamString(String PartName, String ParamName,
                                            String ParamCode, DataTable DataTableParams,
                                            ArrayList ArrayListParams, Boolean IsTesting, Int32 Revision)
        {
            String ReturnValue = String.Empty;

            #region Sellers
            if (PartName == "Sellers")
                ReturnValue = GetSellersParamString(ParamName, ParamCode, DataTableParams,ArrayListParams, IsTesting, Revision);
                #endregion

            #region Buyers
            else if (PartName == "Buyers")
                ReturnValue = GetBuyersParamString(ParamName, ParamCode, DataTableParams,
                                                   ArrayListParams, IsTesting, Revision);
                #endregion

            #region Transfer
            else if (PartName == "Transfer")
                ReturnValue = GetTransferParamString(ParamName, ParamCode, DataTableParams,
                                                               ArrayListParams, IsTesting, Revision);
            #endregion

            return ReturnValue;
        }
        #endregion

        #region String GetLineConditionByTree(NodeCollection NodesCollection)

        /// <summary>
        /// ایجاد شرط خطی از ساختار شرط درختی
        /// </summary>
        /// <param name="NodesCollection">مجموعه گره های مورد نظر</param>
        /// <returns>رشته تولید شده از درخت به صورت خطی</returns>
        public static String GetLineConditionByTree(NodeCollection NodesCollection)
        {
            String ReturnValue = String.Empty;
            #region Search In Nodes
            foreach (PureComponents.TreeView.Node Node in NodesCollection)
            {
                // بررسی حالتی که اولین گزینه از نوع نتیجه است:
                if (Node.Tag.ToString() == "R")
                {
                    ReturnValue += Node.Text;
                    return ReturnValue;
                }
                // بررسی اجزاء شرط:
                ReturnValue += RecursiveSearchInTree(Node);
            }
            #endregion
            return ReturnValue;
        }

        #endregion

        #region Boolean CreateParameterTable(...)
        /// <summary>
        /// روالي براي ايجاد نما براي پارامتر
        /// </summary>
        /// <param name="DataTableParams">جدول پارامتر ها</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Description">توضیحات پارامتر</param>
        /// <param name="Revision">دوره پارامتر</param>
        /// <param name="LineConditionString">رشته فرمان</param>
        /// <param name="UserID">كد كاربر</param>
        /// <param name="PartName">نام بخش</param>
        /// <param name="ParamType">نام بخش</param>
        /// <param name="Tree">درخت شروط</param>
        /// <param name="TestingQuery"></param>
        /// <param name="PCreationDate"></param>
        /// <param name="Cdate"></param>
        /// <returns></returns>
        public static bool CreateParameterTable(DataTable DataTableParams, ArrayList ArrayListParams, string ParamName, string Description, int Revision, string LineConditionString, string UserID, string PartName, string ParamType, TreeView Tree, string TestingQuery,string PCreationDate,string Cdate)
        {
            #region Check Existance Of New Parameter

            if (Revision == 0)

                #region Rev0
                if (ManageParameters.CheckParamExist(ParamName, PartName))
                {
                    PersianMessageBox.Show(
                        "پارامتري با اين نام موجود مي باشد ، لطفاً نام ديگري انتخاب نماييد!",
                        "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
                    return false;
                }
            #endregion

            #endregion

            #region Check User Permission
            DialogResult QResult =
                PersianMessageBox.Show("آيا از اين كار اطمينان داريد؟\n " +
                                       "قبل از ذخيره سازي پارامتر از صحت عملكرد آن اطمينان يابيد!" +
                                       " براي اين منظور بر روي دكمه ي \"بررسي دستورات\" كليك نماييد", "هشدار!",
                                       MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                                       MessageBoxDefaultButton.Button1);
            if (QResult == DialogResult.No)
                return false;
            #endregion


            #region *** Generate SQL Query String For CREATE VIEW ***
            String GenerateQuery = String.Empty;

                #region Sellers
            if (PartName == "Sellers")
                GenerateQuery = GetSellersParamString(ParamName,
                                                      LineConditionString, DataTableParams, ArrayListParams, false, Revision);
                #endregion

                #region Buyers
            else if (PartName == "Buyers")
                GenerateQuery = GetBuyersParamString(ParamName,
                                                     LineConditionString, DataTableParams, ArrayListParams, false, Revision);
                #endregion

                #region Transfer
            else if (PartName == "Transfer")
                GenerateQuery = GetTransferParamString(ParamName,
                                                       LineConditionString, DataTableParams, ArrayListParams, false, Revision);
            #endregion

            #endregion


            #region Insert Param Data To Database


      

            if (Parameters.InsertParamData(ParamName, Description, Revision,
                                GetTreeXml(Tree), UserID, PartName, ParamType, GenerateQuery,PCreationDate,Cdate,_ParamSumType,ParamLevel) == false)
                return false;

            #endregion

            #region Del Created Table

            #region Declare DataTableParamLog
            DataTable DataTableParamLog = new DataTable();
            DataTableParamLog.Columns.Add("EnglishName", typeof(String));
            DataTableParamLog.Columns.Add("ObjectNameP", typeof(String));
            DataTableParamLog.Columns.Add("ParamID", typeof(Int32));
            #endregion

            #region Fill DataTable
            var MySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            String CommandText = "SELECT EnglishName, ObjectNameP,ParamID " +
                                 "FROM " + PartName + ".ParametersLog " +
                                 "WHERE EnglishName = @ParamName ";
            var MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
            MySqlCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar, 30);
            MySqlCommand.Parameters["@ParamName"].Value = ParamName;
            try
            {
                MySqlCommand.Connection.Open();
                DataTableParamLog.Load(MySqlCommand.ExecuteReader());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "خطا!");
            }
            finally
            {
                MySqlCommand.Connection.Close();
            }
            #endregion

            #region DROP Created Tables
            DataRow[] FoundRows = DataTableParamLog.Select();
            for (int i = 0; i <= FoundRows.GetUpperBound(0); i++)
            {
                #region DropViewIfExist

                String DropViewString = "IF  EXISTS (SELECT * FROM sys.Tables " +
                                        "WHERE object_id = " + "OBJECT_ID(N'" +
                                        FoundRows[i]["ObjectNameP"] + "')) " +
                                        "DROP Table " + FoundRows[i]["ObjectNameP"];
                SqlCommand ParamCreateView = new SqlCommand(DropViewString,
                                                            new SqlConnection(DbBizClass.DbConnStr));
                try
                {
                    ParamCreateView.Connection.Open();
                    ParamCreateView.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message, "خطا!");

                }
                finally
                {
                    ParamCreateView.Connection.Close();
                }

                #endregion
            }
            #endregion

            #endregion

            #region Exec SelectString

            Parameters.ExecuteSelectString(GenerateQuery);
            #endregion

            
            return true;
        }
        #endregion
        
        #endregion

        #region private Methods

        #region @@@ String GetSellersParamString(...) @@@

        /// <summary>
        /// توليد رشته فرمان اس كيو ال براي ايجاد نما
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="SelectString"></param>
        /// <param name="DataTableParams">جدول پارامتر های ذخیره شده</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="IsTesting">تعیین تستی بودن یا نبودن فرمان</param>
        /// <param name="Rivision">شماره دوره پارامتر</param>
        /// <returns>رشته ي فرمان اس كيو ال را بر مي گرداند</returns>
        private static String GetSellersParamString(String ParamName, String SelectString,DataTable DataTableParams, ArrayList ArrayListParams,Boolean IsTesting, Int32 Rivision)
        {

            #region Prepare Execution

            #region Check String Null Value

            if (String.IsNullOrEmpty(SelectString))
            {
                PersianMessageBox.Show("هیچ دستوری نوشته نشده ، بررسی ممكن نیست!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return String.Empty;
            }
            #endregion

            #region Variables Declaration
            Boolean ContainSumType0 = false;
            Boolean ContainSumType1 = false;
            Boolean ContainSumType2 = false;
            Boolean ContainSumType3 = false;
            Boolean ContainSumType4 = false;
            Boolean ContainSumType5 = false;
            Boolean ContainSumType60 = false;
            Boolean ContainSumType61 = false;
            Boolean ContainSumType62 = false;
            Boolean ContainSumType63 = false;
            Boolean ContainSumType64 = false;
            Boolean ContainSumType65 = false;
            Boolean ContainSumType7 = false;
            Boolean ContainSumType8 = false;
            Boolean ContainSumType9 = false;
            Boolean ContainParam = false;
            #endregion

            #region Fill ArrayList Used Parameters

            var ArrayListUsedParams = new ArrayList();
            foreach (String Str in ArrayListParams)
            {
                String SearchWord = @"\b" + Str + @"\b";
                if (Regex.IsMatch(SelectString, SearchWord))
                    ArrayListUsedParams.Add(Str);
            }

            #endregion

            #region Replace Complete Object
            if(IsTesting)
            {
                #region Replace Complete Objects Name


                foreach (String Str in ArrayListUsedParams)
                {
                    var SchemaName = Parameters.GetParamLastObjectP(Str, "Sellers");
              
                    PLevel = Parameters.GetParamLevel(Str, "Sellers");
                    if (PLevel > ParamLevel)
                    {
                        ParamLevel = PLevel;
                    }
                    var TextToFind = new Regex(Str + @"\b");
                    var ObjectFullName = SchemaName + "." + Str;
                    SelectString = TextToFind.Replace(SelectString, ObjectFullName);
                }

                #endregion
            }
            else
            {
                #region Delete from ParamIn
                Parameters.DelParamIDIN(ParamName, "Sellers");
                #endregion  

                #region Replace Complete Objects Name

                ParamLevel = 0;

                foreach (String Str in ArrayListUsedParams)
                {
                    var SchemaName = Parameters.GetParamLastObjectP(Str, "Sellers");
                    Parameters.SetParamIDIN(ParamName, Str, "Sellers");
                    PLevel = Parameters.GetParamLevel(Str, "Sellers");
                    if (PLevel > ParamLevel)
                    {
                        ParamLevel = PLevel;
                    }
                    var TextToFind = new Regex(Str + @"\b");
                    var ObjectFullName = SchemaName + "." + Str;
                    SelectString = TextToFind.Replace(SelectString, ObjectFullName);
                }

                #endregion
            }
            #endregion

            #region Check Used Variables For Sum Types

            if (ArrayListUsedParams.Count > 0)
            {
                #region Find Containing of Sum Params
                foreach (String Str in ArrayListUsedParams)
                {
                    String CurentParamSum = Parameters.GetParamSumType(Str, "Sellers");
                    if (CurentParamSum == "0")
                        ContainSumType0 = true;
                    else if (CurentParamSum == "1")
                        ContainSumType1 = true;
                    else if (CurentParamSum == "2")
                        ContainSumType2 = true;
                    else if (CurentParamSum == "3")
                        ContainSumType3 = true;
                    else if (CurentParamSum == "4")
                        ContainSumType4 = true;
                    else if (CurentParamSum == "5")
                        ContainSumType5 = true;
                    else if (CurentParamSum == "60")
                        ContainSumType60 = true;
                    else if (CurentParamSum == "61")
                        ContainSumType61 = true;
                    else if (CurentParamSum == "62")
                        ContainSumType62 = true;
                    else if (CurentParamSum == "63")
                        ContainSumType63 = true;
                    else if (CurentParamSum == "64")
                        ContainSumType64 = true;
                    else if (CurentParamSum == "65")
                        ContainSumType65 = true;
                    else if (CurentParamSum == "7")
                        ContainSumType7 = true;
                    else if (CurentParamSum == "8")
                        ContainSumType8 = true;
                    else if (CurentParamSum == "9")
                        ContainSumType9 = true;
                }
                #endregion

                #region Set Current Param SumType
                if (ContainSumType0)
                    _ParamSumType = "0";
                else if (ContainSumType1)
                    _ParamSumType = "1";
                else if (ContainSumType2)
                    _ParamSumType = "2";
                else if (ContainSumType3)
                    _ParamSumType = "3";
                else if (ContainSumType4)
                    _ParamSumType = "4";
                else if (ContainSumType5)
                    _ParamSumType = "5";
                else if (ContainSumType60)
                    _ParamSumType = "60";
                else if (ContainSumType61)
                    _ParamSumType = "61";
                else if (ContainSumType62)
                    _ParamSumType = "62";
                else if (ContainSumType63)
                    _ParamSumType = "63";
                else if (ContainSumType64)
                    _ParamSumType = "64";
                else if (ContainSumType65)
                    _ParamSumType = "65";
                else if (ContainSumType7)
                    _ParamSumType = "7";
                else if (ContainSumType8)
                    _ParamSumType = "8";
                else if (ContainSumType9)
                    _ParamSumType = "9";
                #endregion

                ContainParam = true;
            }

            #endregion

            #endregion

            #region SUM SELECT

                 #region For Sum1 (SumByUnitType)

            if (Regex.IsMatch(SelectString, @"\bSum1\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "1";
                var SumTextToFind = new Regex(@"\bSum1\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString = SelectString.Insert(0, "SELECT RegionName, " +
                                                      "CompanyName , " +
                                                      "PowerStationCode , " + "PowerStationName , " + 
                                                     " BillUnitID, " + "Date , " + "Hour , ");

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName+ "' FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"+ " into SellersParam." + ParamName +" FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                #endregion


                #endregion

                #region Make GROUP BY

                const String GroupByString = " GROUP BY " +
                                             "RegionName , " + "CompanyName , " +
                                             "PowerStationCode , " + "PowerStationName , " +
                                             " BillUnitID, "  + "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion
            }

                #endregion

                #region For Sum2 (SumByPowerStationName)

            else if (Regex.IsMatch(SelectString, @"\bSum2\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "2";
                var SumTextToFind = new Regex(@"\bSum2\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " +
                                           "CompanyTypeName , " + "CompanyName , " +
                                           "PowerStationName , " + "Date , " + "Hour , ");


                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName+ "' FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                else
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"+ " into SellersParam." + ParamName +" FROM " +Parameters.GetParamLastObjectP(ArrayListUsedParams[0].ToString(),"Sellers"));
                }
                #endregion




                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , " + "CompanyTypeName , " +
                                             "CompanyName , " + "PowerStationName , " +
                                             "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

                #region For Sum3 (SumByCompanyName)

            else if (Regex.IsMatch(SelectString, @"\bSum3\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "3";
                Regex SumTextToFind = new Regex(@"\bSum3\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " +
                                           "CompanyTypeName , " + "CompanyName , " +
                                           "Date , " + "Hour , ");

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , " + "CompanyTypeName , " +
                                             "CompanyName , " + "Date , " + "Hour";

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

                #region For Sum4 (SumByCompanyTypeName)

            else if (Regex.IsMatch(SelectString, @"\bSum4\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "4";
                var SumTextToFind = new Regex(@"\bSum4\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " +
                                           "CompanyTypeName , " + "Date , " + "Hour , ");

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , " + "CompanyTypeName , " +
                                             "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

                #region For Sum5 (SumByRegionName)

            else if (Regex.IsMatch(SelectString, @"\bSum5\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "5";
                var SumTextToFind = new Regex(@"\bSum5\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                SelectString =
                    SelectString.Insert(0, "SELECT RegionName, " + "Date , " + "Hour , ");

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                const string GroupByString = " GROUP BY " +
                                             "RegionName , " + "Date , " + "Hour";
                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

                #region For Sum6 (SumByDay)

            else if (Regex.IsMatch(SelectString, @"\bSum6\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "6";
                Regex SumTextToFind = new Regex(@"\bSum6\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Set Param Sum Type

                if (ContainSumType0)
                    _ParamSumType = "60";
                else if (ContainSumType1)
                    _ParamSumType = "61";
                else if (ContainSumType2)
                    _ParamSumType = "62";
                else if (ContainSumType3)
                    _ParamSumType = "63";
                else if (ContainSumType4)
                    _ParamSumType = "64";
                else if (ContainSumType5)
                    _ParamSumType = "65";

                #endregion

                #region Make SELECT

                   #region Contain Sum Type 0 (Fixed & Mixed)

                if (ContainSumType0)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " +
                                               "PowerStationName , " + "PowerStationCode, " + "UnitType , " +"BillUnitID ,"+ "UnitID , " + "Date , ");

                    #endregion

                    #region Contain Sum Type 1 (SumByUnitType)

                else if (ContainSumType1)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " +
                                               "PowerStationName , " + "UnitType , " + "Date , ");

                    #endregion

                    #region Contain Sum Type 2 (SumByPowerStationName)

                else if (ContainSumType2)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                                "CompanyName , " +
                                               "PowerStationName , " + "Date , ");

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " + "Date , ");

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "Date , ");

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " + "Date , ");

                #endregion

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                String GroupByString = String.Empty;

                #region Contain Sum Type 0

                if (ContainSumType0)
                    GroupByString = " GROUP BY " +
                                    "RegionName , " + "CompanyTypeName , " +
                                    "CompanyName , " + "PowerStationName , " + "PowerStationCode, " + "UnitType , " +"BillUnitID ,"+ "UnitID , " + "Date";

                    #endregion

                    #region Contain Sum Type 1 (SumByUnitType)

                else if (ContainSumType1)
                    GroupByString = " GROUP BY " +
                                    "RegionName , " + "CompanyTypeName , " +
                                    "CompanyName , " + "PowerStationName , " +
                                    "UnitType , " + "Date";

                    #endregion

                    #region Contain Sum Type 2 (SumByPowerStationName)

                else if (ContainSumType2)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyName , " +
                                    "PowerStationName , " + "Date";

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName , " + "Date";

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "Date";

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    GroupByString = " GROUP BY " + "RegionName , " + "Date";

                #endregion

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion

                #region For Sum7 (SumByMonth)

            else if (Regex.IsMatch(SelectString, @"\bSum7\b", RegexOptions.IgnoreCase))
            {

                #region Replace SUM Expression

                _ParamSumType = "7";
                var SumTextToFind = new Regex(@"\bSum7\b");
                const string SumTextToReplace = "SUM";
                SelectString = SumTextToFind.Replace(SelectString, SumTextToReplace);

                #endregion

                #region Make SELECT

                #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2 (PowerStation)

                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , " + "PowerStationName , ");

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " +
                                               "CompanyTypeName , " + "CompanyName , ");

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, " + "CompanyTypeName , ");

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, ");

                    #endregion

                    #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    SelectString =
                        SelectString.Insert(0, "SELECT RegionName, ");

                #endregion

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName
                        + "' FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                else
                {


                    SelectString = SelectString.Insert(SelectString.Length, " AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName +
                                                                            " FROM " +
                                                                            Parameters.GetParamLastObjectP(
                                                                                ArrayListUsedParams[0].ToString(),
                                                                                "Sellers"));
                }
                #endregion

                #endregion

                #region Make GROUP BY

                String GroupByString = String.Empty;

                #region Contain Sum Type 0 (Fixed & Mixed) & 1 (UnitType) & 2 (PowerStation)

                if (ContainSumType0 || ContainSumType1 || ContainSumType2)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName , " + "PowerStationName";

                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                else if (ContainSumType3)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName , " + "CompanyName";

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                else if (ContainSumType4)
                    GroupByString = " GROUP BY " + "RegionName , " +
                                    "CompanyTypeName";

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                else if (ContainSumType5)
                    GroupByString = " GROUP BY " + "RegionName";

                    #endregion

                    #region Contain Sum Type 6 (SumByDay)

                else if (ContainSumType60 || ContainSumType61 || ContainSumType62 ||
                         ContainSumType63 || ContainSumType64 || ContainSumType65)
                    GroupByString = " GROUP BY " + "RegionName";

                #endregion

                SelectString = SelectString.Insert(SelectString.Length, GroupByString);

                #endregion

            }

                #endregion



                #endregion

            #region Not SUM SELECT For Parameters
            else if (ContainParam)
            {
                if ((Regex.IsMatch(SelectString, @"\bMin\b", RegexOptions.IgnoreCase)
               || Regex.IsMatch(SelectString, @"\bMax\b", RegexOptions.IgnoreCase)) && !(Regex.IsMatch(SelectString, @"\bFunctions\b", RegexOptions.IgnoreCase)))
                {
                    #region Select
                    SelectColumns = String.Empty;
                    _ParamSumType = "8";
                    SelectColumns = "SELECT Sellers.FixedParameters.Date, " +
                                    "Sellers.FixedParameters.Hour , ";
                 //   step = 6;
                    #endregion
                }
                else
                {
                    SelectColumns = String.Empty;

                    #region Make SELECT



                    #region Contain Sum Type 0 (Fixed Or Mixed)

                    if (ContainSumType0)
                    {
                        _ParamSumType = "0";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                         "Sellers.FixedParameters.UnitCode," +
                                        "Sellers.FixedParameters.UnitID ," +
                                        "Sellers.FixedParameters.BillUnitID ," +
                                        "Sellers.FixedParameters.BillUnitT ," +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                    #endregion

                    #region Contain Sum Type 1 (SumByUnitType)

                    else if (ContainSumType1)
                    {
                        _ParamSumType = "1";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.BillUnitID," +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }
                    #endregion

                    #region Contain Sum Type 2 (SumByPowerStationName)

                    else if (ContainSumType2)
                    {
                        _ParamSumType = "2";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }
                    #endregion

                    #region Contain Sum Type 3 (SumByCompanyName)

                    else if (ContainSumType3)
                    {
                        _ParamSumType = "3";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                    #endregion

                    #region Contain Sum Type 4 (SumByCompanyTypeName)

                    else if (ContainSumType4)
                    {
                        _ParamSumType = "4";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                    #endregion

                    #region Contain Sum Type 5 (SumByRegionName)

                    else if (ContainSumType5)
                    {
                        _ParamSumType = "5";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                        "Sellers.FixedParameters.Date, " +
                                        "Sellers.FixedParameters.Hour , ";
                    }

                    #endregion

                    #region Contain Sum Type 6 (SumByDay)

                    #region Contain Sum Type 60 (Fixed & Mixed)

                    else if (ContainSumType60)
                    {
                        _ParamSumType = "60";
                        SelectColumns = "SELECT " +
                                        "Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.BillUnitID , " +
                                        "Sellers.FixedParameters.UnitID , " +
                                        "Sellers.FixedParameters.UnitCode , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                    #endregion

                    #region Contain Sum Type 61 (SumByUnitType)

                    else if (ContainSumType61)
                    {
                        _ParamSumType = "61";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationCode , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.UnitType , " +
                                        "Sellers.FixedParameters.BillUnitID , " +
                                        "Sellers.FixedParameters.BillUnitT , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                    #endregion

                    #region Contain Sum Type 62 (SumByPowerStationName)

                    else if (ContainSumType62)
                    {
                        _ParamSumType = "62";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.PowerStationName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                    #endregion

                    #region Contain Sum Type 63 (SumByCompanyName)

                    else if (ContainSumType63)
                    {
                        _ParamSumType = "63";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.CompanyName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                    #endregion

                    #region Contain Sum Type 64 (SumByCompanyTypeName)

                    else if (ContainSumType64)
                    {
                        _ParamSumType = "64";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.CompanyTypeName , " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                    #endregion

                    #region Contain Sum Type 65 (SumByRegionName)

                    else if (ContainSumType65)
                    {
                        _ParamSumType = "65";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName, " +
                                        "Sellers.FixedParameters.Date , ";
                    }
                    #endregion

                    #endregion

                    #region Contain Sum Type 7 (SumByMonth)

                    else if (ContainSumType7)
                    {
                        _ParamSumType = "7";
                        SelectColumns = "SELECT Sellers.FixedParameters.RegionName , ";
                    }

                    #region Contain Sum Type 8 (SumByJustDate)

                    else if (ContainSumType8)
                    {
                        _ParamSumType = "8";
                        SelectColumns = "SELECT Sellers.FixedParameters.Date , ";
                    }

                    #endregion

                    #region Contain Sum Type 9 (SumByJustDate)

                    else if (ContainSumType9)
                    {
                        _ParamSumType = "9";
                        SelectColumns = "SELECT Sellers.FixedParameters.Date ,Sellers.FixedParameters.Hour , ";
                    }

                    #endregion                       

                    #endregion


                    #endregion 
                }
         

          


                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(0, SelectColumns + " (CASE ");
                    SelectString = SelectString.Insert(SelectString.Length, " END) AS '" + ParamName +
                                                                            "' FROM Sellers.FixedParameters");
                }
                else
                {
                    SelectString = SelectString.Insert(0, SelectColumns + " (CASE ");
                    SelectString = SelectString.Insert(SelectString.Length, " END) AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName + " FROM Sellers.FixedParameters");
                }
                #endregion

                #region INNER JOINs

                foreach (String Str in ArrayListUsedParams)
                {
                    DataRow[] FoundRows =
                        DataTableParams.Select("EnglishName = '" + Str + "'");

                    #region Check Non-Fixed Paameters (Mixed & Sum)

                    if (FoundRows[0]["Category"].ToString() != "Fx")
                    {
                        String CurentParamSum = Parameters.GetParamSumType(Str, "Sellers");
                        String ObjectName = Parameters.GetParamLastObjectP(Str, "Sellers");


                        #region Sum Type 0 (Mixed Parameters)

                        if (CurentParamSum == "0")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.UnitID = " +
                                                     ObjectName + ".[UnitID] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] " +
                                                     " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }

                            #endregion

                            #region Sum Type 1 (SumByUnitType)

                        else if (CurentParamSum == "1")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.PowerStationCode = " +
                                                     ObjectName + ".PowerStationCode" +
                                                     " AND Sellers.FixedParameters.BillUnitID = " +
                                                     ObjectName + ".[BillUnitID] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] " +
                                                     " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }

                            #endregion

                            #region Sum Type 2 (SumByPowerStation)

                        else if (CurentParamSum == "2")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.PowerStationName = " +
                                                     ObjectName + ".[PowerStationName]" +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date]" +
                                                     " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                        }

                            #endregion

                            #region Sum Type 3 (SumByCompany)

                        else if (CurentParamSum == "3")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.CompanyName = " +
                                                     ObjectName + ".[CompanyName]" +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date]" +
                                                     " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                        }

                            #endregion

                            #region Sum Type 4 (SumByCompanyType)

                        else if (CurentParamSum == "4")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.CompanyTypeName = " +
                                                     ObjectName + ".[CompanyTypeName]" +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date]" +
                                                     " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                        }

                            #endregion

                            #region Sum Type 5 (SumByRegion)

                        else if (CurentParamSum == "5")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.RegionName = " +
                                                     ObjectName + ".[RegionName]" +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date]" +
                                                     " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);

                        }

                            #endregion

                            #region Sum Type 6 (SumByDay)

                            #region ContainSumType60
                        else if (CurentParamSum == "6" || CurentParamSum=="60")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.UnitID = " +
                                                     ObjectName + ".[UnitID] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region ContainSumType61
                        else if (CurentParamSum == "61")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.PowerStationCode = " +
                                                     ObjectName + ".[PowerStationCode] " +
                                                     " AND Sellers.FixedParameters.BillUnitID = " +
                                                     ObjectName + ".[BillUnitID] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region ContainSumType62
                        else if (CurentParamSum == "62")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.PowerStationName = " +
                                                     ObjectName + ".[PowerStationName] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region ContainSumType63
                        else if (CurentParamSum == "63")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.CompanyName = " +
                                                     ObjectName + ".[CompanyName] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region ContainSumType64
                        else if (CurentParamSum == "64")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.CompanyTypeName = " +
                                                     ObjectName + ".[CompanyTypeName] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #region ContainSumType65
                        else if (CurentParamSum == "65")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.RegionName = " +
                                                     ObjectName + ".[RegionName] " +
                                                     " AND Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                            #endregion

                            #endregion

                            #region Sum Type 7 (SumByMonth)

                        else if (CurentParamSum == "7")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.RegionName  = " +
                                                     ObjectName + ".RegionName ";
                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }

                        #endregion

                            #region ContainSumType8

                        else if (CurentParamSum == "8")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] " +
                                                     " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[hour] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                        #endregion
                        #region ContainSumType8

                        else if (CurentParamSum == "8")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                        #endregion

                        #region ContainSumType9

                        else if (CurentParamSum == "9")
                        {
                            String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                     " ON Sellers.FixedParameters.Date = " +
                                                     ObjectName + ".[Date] " +
                                                        " AND Sellers.FixedParameters.Hour = " +
                                                     ObjectName + ".[Hour] ";

                            SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                        }
                        #endregion

                    }

                        #endregion

                        #region Check Fixed Parameters

                    else
                    {
                        var TextToFind = new Regex(@"\b" + Str + @"\b");
                        SelectString = TextToFind.Replace(SelectString, "[" + Str + "]");
                    }

                    #endregion

                } // End of "foreach"

                #endregion

                #region GROUP BY

                String InsertGROUPBY = String.Empty;

                #region Fixed Parameters GROUP BY


                #region Contain Sum Type 0 In Statement

                if (ContainSumType0)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName, " +
                                    "Sellers.FixedParameters.CompanyName, " +
                                    "Sellers.FixedParameters.PowerStationCode , " +
                                    "Sellers.FixedParameters.PowerStationName , " +
                                    "Sellers.FixedParameters.UnitType , " +
                                    "Sellers.FixedParameters.UnitID ," +
                                       "Sellers.FixedParameters.UnitCode ," +
                                    "Sellers.FixedParameters.BillUnitID, " +
                                    "Sellers.FixedParameters.BillUnitT, " +
                                    "Sellers.FixedParameters.Date, " +
                                    "Sellers.FixedParameters.Hour , ";
                }

                    #endregion

                    #region Contain Sum Type 1 In Statement(SumByUnitType)

                else if (ContainSumType1)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName, " +
                                    "Sellers.FixedParameters.CompanyName, " +
                                    "Sellers.FixedParameters.PowerStationCode , " +
                                    "Sellers.FixedParameters.PowerStationName , " +
                           
                                    "Sellers.FixedParameters.BillUnitID," +
                     
                                    "Sellers.FixedParameters.Date, " +
                                    "Sellers.FixedParameters.Hour , ";
                }

                    #endregion

                    #region Contain Sum Type 2 In Statement (SumByPowerStation)

                else if (ContainSumType2)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName, " +
                                    "Sellers.FixedParameters.CompanyTypeName, " +
                                    "Sellers.FixedParameters.CompanyName, " +
                                    "Sellers.FixedParameters.PowerStationName, " +
                                    "Sellers.FixedParameters.Date, " +
                                    "Sellers.FixedParameters.Hour, ";
                }

                    #endregion

                    #region Contain Sum Type 3 In Statement (SumByCompany)

                else if (ContainSumType3)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName, " +
                                    "Sellers.FixedParameters.CompanyTypeName, " +
                                    "Sellers.FixedParameters.CompanyName, " +
                                    "Sellers.FixedParameters.Date, " +
                                    "Sellers.FixedParameters.Hour, ";
                }

                    #endregion

                    #region Contain Sum Type 4 In Statement (SumByCompanyType)

                else if (ContainSumType4)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName, " +
                                    "Sellers.FixedParameters.CompanyTypeName, " +
                                    "Sellers.FixedParameters.Date, " +
                                    "Sellers.FixedParameters.Hour, ";
                }

                    #endregion

                    #region Contain Sum Type 5 In Statement (SumByRegion)

                else if (ContainSumType5)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName, " +
                                    "Sellers.FixedParameters.Date, " +
                                    "Sellers.FixedParameters.Hour, ";
                }

                    #endregion

                    #region Contain Sum Type 6 In Statement (SumByDay)

                    #region ContainSumType60
                else if (ContainSumType60)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName , " +
                                    "Sellers.FixedParameters.CompanyTypeName , " +
                                    "Sellers.FixedParameters.CompanyName , " +
                                    "Sellers.FixedParameters.PowerStationName , " +
                                    "Sellers.FixedParameters.PowerStationCode , " +
                                    "Sellers.FixedParameters.UnitType , " +
                                    "Sellers.FixedParameters.BillUnitID , " +
                                    "Sellers.FixedParameters.UnitID , " +
                                    "Sellers.FixedParameters.UnitCode ," +

                                    "Sellers.FixedParameters.Date , ";
                }
                    #endregion

                    #region ContainSumType61
                else if (ContainSumType61)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName , " +
                                    "Sellers.FixedParameters.CompanyName , " +
                                    "Sellers.FixedParameters.PowerStationCode , " +
                                    "Sellers.FixedParameters.PowerStationName , " +
                                    "Sellers.FixedParameters.UnitType , " +
                                    "Sellers.FixedParameters.BillUnitID , " +
                                    "Sellers.FixedParameters.BillUnitT , " +
                                    "Sellers.FixedParameters.Date , ";
                }
                    #endregion

                    #region ContainSumType62
                else if (ContainSumType62)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName , " +
                                    "Sellers.FixedParameters.CompanyTypeName , " +
                                    "Sellers.FixedParameters.CompanyName , " +
                                    "Sellers.FixedParameters.PowerStationName , " +
                                    "Sellers.FixedParameters.Date , ";
                }
                    #endregion

                    #region ContainSumType63
                else if (ContainSumType63)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName , " +
                                    "Sellers.FixedParameters.CompanyTypeName , " +
                                    "Sellers.FixedParameters.CompanyName , " +
                                    "Sellers.FixedParameters.Date , ";
                }

                    #endregion

                    #region ContainSumType64
                else if (ContainSumType64)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName , " +
                                    "Sellers.FixedParameters.CompanyTypeName , " +
                                    "Sellers.FixedParameters.Date , ";
                }
                    #endregion

                    #region ContainSumType65
                else if (ContainSumType65)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName , " +
                                    "Sellers.FixedParameters.Date , ";
                }
                    #endregion

                    #endregion

                    #region Contain Sum Type 7 In Statement (SumByMonth)

                else if (ContainSumType7)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.RegionName, ";
                }

                #endregion
                #region Contain Sum Type 8 In Statement (SumByJustByDate)

                else if (ContainSumType8)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.Date, ";
                }

                #endregion

                #region Contain Sum Type 9 In Statement (SumByJustByDateHour)

                else if (ContainSumType9)
                {
                    InsertGROUPBY = " GROUP BY " +
                                    "Sellers.FixedParameters.Date,Sellers.FixedParameters.Hour, ";
                }

                #endregion

                #endregion

                #region Non-Fixed Columns GROUP BY

                for (int i = 0; i < ArrayListUsedParams.Count; i++)
                {
                    String Str = ArrayListUsedParams[i].ToString();
                    String ObjectName = Parameters.GetParamLastObjectP(Str, "Sellers");
                    if (i < ArrayListUsedParams.Count - 1)
                        InsertGROUPBY += ObjectName + "." + Str + ", ";
                    else
                        InsertGROUPBY += ObjectName + "." + Str;
                }

                #endregion

                #region Insert GROUP BY Text

                if (ContainSumType1 || ContainSumType2 || ContainSumType3 ||
                    ContainSumType4 || ContainSumType5 || ContainSumType60 ||
                    ContainSumType61 || ContainSumType62 || ContainSumType63 ||
                    ContainSumType64 || ContainSumType65 || ContainSumType7 || ContainSumType8 || ContainSumType9)
                    SelectString = SelectString.Insert(SelectString.Length, InsertGROUPBY);

                #endregion

                #endregion

            }
                #endregion

            #region SELECT For Numbers Without Param

            else
            {
                _ParamSumType = "0";
                const String SelectColumns = "SELECT Sellers.FixedParameters.RegionName , " +
                                             "Sellers.FixedParameters.CompanyName , " +
                                             "Sellers.FixedParameters.PowerStationCode , " +
                                             "Sellers.FixedParameters.PowerStationName , " +
                                             "Sellers.FixedParameters.UnitType , " +
                                             "Sellers.FixedParameters.UnitID , " +
                                             "Sellers.FixedParameters.Date, " +
                                             "Sellers.FixedParameters.Hour , ";

                #region Istesting-From
                if (IsTesting)
                {
                    SelectString = SelectString.Insert(0, SelectColumns + " (CASE ");
                    SelectString = SelectString.Insert(SelectString.Length, " END) AS '" + ParamName +
                                                                            "' FROM Sellers.FixedParameters");
                }
                else
                {
                    SelectString = SelectString.Insert(0, SelectColumns + " (CASE ");
                    SelectString = SelectString.Insert(SelectString.Length, " END) AS '" + ParamName + "'"
                                                                            + " into SellersParam." + ParamName + " FROM Sellers.FixedParameters");
                }
                #endregion
            }
            #endregion

            #region Return Results

 

            return SelectString;

            #endregion
        }
        #endregion
        
        #region @@@ String GetBuyersParamString(...) @@@
        /// <summary>
        /// توليد رشته فرمان اس كيو ال براي ايجاد نما
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="Select">رشته وارد شده</param>
        /// <param name="DataTableParams">جدول پارامتر های ذخیره شده</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="IsTesting">تعیین تستی بودن یا نبودن فرمان</param>
        /// <param name="Rivision">شماره دوره پارامتر</param>
        /// <returns>رشته ي فرمان اس كيو ال را بر مي گرداند</returns>
        private static String GetBuyersParamString(String ParamName, String Select,
                                                   DataTable DataTableParams, ArrayList ArrayListParams,
                                                   Boolean IsTesting, Int32 Rivision)
        {

            #region Prepare Execution

            #region Check String Null Value

            if (String.IsNullOrEmpty(Select))
            {
                PersianMessageBox.Show("هیچ دستوری نوشته نشده ، بررسی ممكن نیست!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return String.Empty;
            }
            #endregion

            #region Variables Declaration
            Boolean ContainSumType0 = false;
            Boolean ContainSumType1 = false;
            Boolean ContainSumType2 = false;
            Boolean ContainSumType3 = false;
            Boolean ContainSumType30 = false;
            Boolean ContainSumType31 = false;
            Boolean ContainSumType32 = false;
    
            #endregion

            #region Fill ArrayList Used Parameters

            var ArrayListUsedParams = new ArrayList();
            foreach (String Str in ArrayListParams)
            {
                String SearchWord = @"\b" + Str + @"\b";
                if (Regex.IsMatch(Select, SearchWord))
                    ArrayListUsedParams.Add(Str);
            }

            #endregion

            

            #region Delete from ParamIn
            Parameters.DelParamIDIN(ParamName, "Buyers");
            #endregion

            #region Replace Complete Objects Name

            ParamLevel = 0;

            foreach (String Str in ArrayListUsedParams)
            {
                String SchemaName = Parameters.GetParamLastObjectP(Str, "Buyers");
                Parameters.SetParamIDIN(ParamName, Str, "Buyers");
                PLevel = Parameters.GetParamLevel(Str, "Buyers");
                if (PLevel > ParamLevel)
                {
                    ParamLevel = PLevel;
                }
                Regex TextToFind = new Regex(Str + @"\b");
                String ObjectFullName = SchemaName + "." + Str;
                Select = TextToFind.Replace(Select, ObjectFullName);
            }

            #endregion

            #region Check Used Variables For Sum Types

            #region Find Containing of Sum Params
            foreach (String Str in ArrayListUsedParams)
            {
                String CurentParamSum = Parameters.GetParamSumType(Str, "Buyers");
                if (CurentParamSum == "0") ContainSumType0 = true;
                else if (CurentParamSum == "1") ContainSumType1 = true;
                else if (CurentParamSum == "2") ContainSumType2 = true;
                else if (CurentParamSum == "30") ContainSumType30 = true;
                else if (CurentParamSum == "31") ContainSumType31 = true;
                else if (CurentParamSum == "32") ContainSumType32 = true;

            }
            #endregion

            #region Set Current Param SumType
            if (ContainSumType0) _ParamSumType = "0";
            else if (ContainSumType1) _ParamSumType = "1";
            else if (ContainSumType2) _ParamSumType = "2";
            else if (ContainSumType30) _ParamSumType = "30";
            else if (ContainSumType31) _ParamSumType = "31";
            else if (ContainSumType32) _ParamSumType = "32";
      
            #endregion

            #endregion

            #endregion

            #region SELECT
            String SelectColumns = String.Empty;

            #region Make SELECT

            #region Contain Sum Type 0 (Fixed Or Mixed)
            if (ContainSumType0)
            {
                _ParamSumType = "0";
                SelectColumns = "SELECT Buyers.FixedParameters.CompanyTypeName , " +
                                "Buyers.FixedParameters.CompanyName , " +
                                "Buyers.FixedParameters.Date, " +
                                "Buyers.FixedParameters.Hour , ";
            }
                #endregion

                #region Contain Sum Type 1 (SumByHour)
            else if (ContainSumType1)
            {
                _ParamSumType = "1";
                SelectColumns = "SELECT Buyers.FixedParameters.CompanyTypeName , " +
                                "Buyers.FixedParameters.CompanyName , " +
                                "Buyers.FixedParameters.Date , ";
            }
                #endregion

                #region Contain Sum Type 2 (SumByDate)
            else if (ContainSumType2)
            {
                _ParamSumType = "2";
                SelectColumns = "SELECT Buyers.FixedParameters.CompanyTypeName , " +
                                "Buyers.FixedParameters.CompanyName , ";
            }
                #endregion

                #region Contain Sum Type 3 (SumByCompany)
            else if (ContainSumType30)
            {
                _ParamSumType = "30";
                SelectColumns = "SELECT Buyers.FixedParameters.Date , " +
                                "Buyers.FixedParameters.Hour , ";
            }
            #endregion

            

            #endregion




            if (IsTesting)
            {

                Select = Select.Insert(0, SelectColumns + " (CASE ");
                Select = Select.Insert(Select.Length, " END) AS '" + ParamName +
                                                                        "' FROM Buyers.FixedParameters");
            }
            else
            {

                Select = Select.Insert(0, SelectColumns + " (CASE ");
                Select = Select.Insert(Select.Length, " END) AS '" + ParamName + "'" + " into BuyersParam." + ParamName + " FROM Buyers.FixedParameters");
            }

            #endregion

            #region INNER JOINs
            foreach (String Str in ArrayListUsedParams)
            {
                DataRow[] FoundRows =
                    DataTableParams.Select("EnglishName = '" + Str + "'");

                #region Check Non-Fixed Paameters (Mixed & Sum)
                if (FoundRows[0]["Category"].ToString() != "Fx")
                {
                    String CurentParamSum = Parameters.GetParamSumType(Str, "Buyers");
                    String ObjectName = Parameters.GetParamLastObjectP(Str, "Buyers");

                    #region Sum Type 0 (Mixed Parameters)
                    if (CurentParamSum == "0")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Buyers.FixedParameters.CompanyName = " +
                                                 ObjectName + ".[CompanyName] " +
                                                 " AND Buyers.FixedParameters.Date = " +
                                                 ObjectName + ".[Date] " +
                                                 " AND Buyers.FixedParameters.Hour = " +
                                                 ObjectName + ".[Hour] ";
                        Select = Select.Insert(Select.Length, InsertInnerJoin);
                    }
                        #endregion

                        #region Sum Type 1 (SumByDay)
                    else if (CurentParamSum == "1")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Buyers.FixedParameters.CompanyName = " +
                                                 ObjectName + ".[CompanyName]" +
                                                 " AND Buyers.FixedParameters.Date = " +
                                                 ObjectName + ".[Date] ";
                        Select = Select.Insert(Select.Length, InsertInnerJoin);
                    }
                        #endregion

                        #region Sum Type 2 (SumByMonth)
                    else if (CurentParamSum == "2")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Buyers.FixedParameters.CompanyName = " +
                                                 ObjectName + ".[CompanyName]";
                        Select = Select.Insert(Select.Length, InsertInnerJoin);
                    }
                        #endregion

                        #region Sum Type 3 (SumByCompany)
                    else if (CurentParamSum == "30")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Buyers.FixedParameters.Date = " +
                                                 ObjectName + ".[Date]" +
                                                 " AND Buyers.FixedParameters.Hour = " +
                                                 ObjectName + ".[Hour] ";
                        Select = Select.Insert(Select.Length, InsertInnerJoin);
                    }
                    #endregion


                }
                    #endregion

                    #region Check Fixed Parameters
                else
                {
                    var TextToFind = new Regex(@"\b" + Str + @"\b");
                    Select = TextToFind.Replace(Select, "[" + Str + "]");
                }
                #endregion

            } // End of "foreach"
            #endregion

            #region GROUP BY
            String InsertGROUPBY = String.Empty;

            #region Fixed Parameters GROUP BY

            #region Contain Sum Type 0 In Statement
            if (ContainSumType0)
            {
                InsertGROUPBY = " GROUP BY " +
                                "Buyers.FixedParameters.CompanyTypeName, " +
                                "Buyers.FixedParameters.CompanyName, " +
                                "Buyers.FixedParameters.Date, " +
                                "Buyers.FixedParameters.Hour, ";
            }
                #endregion

                #region Contain Sum Type 1 In Statement(SumByDay)
            else if (ContainSumType1)
            {
                InsertGROUPBY = " GROUP BY " +
                                "Buyers.FixedParameters.CompanyTypeName, " +
                                "Buyers.FixedParameters.CompanyName, " +
                                "Buyers.FixedParameters.Date, ";
            }
                #endregion

                #region Contain Sum Type 2 In Statement (SumByMonth)
            else if (ContainSumType2)
            {
                InsertGROUPBY = " GROUP BY " +
                                "Buyers.FixedParameters.CompanyTypeName, " +
                                "Buyers.FixedParameters.CompanyName, ";
            }
                #endregion

                #region Contain Sum Type 3 In Statement (SumByFullHour)
            else if (ContainSumType3)
            {
                InsertGROUPBY = " GROUP BY " +
                                "Buyers.FixedParameters.CompanyTypeName, " +
                                "Buyers.FixedParameters.CompanyName, " +
                                "Buyers.FixedParameters.Date, ";
            }
            #endregion



            

            #endregion

            #region Non-Fixed Columns GROUP BY
            for (int i = 0; i < ArrayListUsedParams.Count; i++)
            {
                String Str = ArrayListUsedParams[i].ToString();
                String ObjectName = Parameters.GetParamLastObjectP(Str, "Buyers");
                if (i < ArrayListUsedParams.Count - 1)
                    InsertGROUPBY += ObjectName + "." + Str + ", ";
                else
                    InsertGROUPBY += ObjectName + "." + Str;
            }
            #endregion

            #region Insert GROUP BY Text
            if (ContainSumType1 || ContainSumType2 || ContainSumType3 
                )
                Select = Select.Insert(Select.Length, InsertGROUPBY);
            #endregion

            #endregion

            #region Return Results
   
            return Select;

            #endregion

        }
        #endregion

        #region @@@ String GetTransferParamString(...) @@@

        /// <summary>
        /// توليد رشته فرمان اس كيو ال براي ايجاد نما
        /// </summary>
        /// <param name="ParamName">نام پارامتر</param>
        /// <param name="SelectString">رشته وارد شده</param>
        /// <param name="DataTableParams">جدول پارامتر های ذخیره شده</param>
        /// <param name="ArrayListParams">لیست نام پارامتر ها</param>
        /// <param name="IsTesting">تعیین تستی بودن یا نبودن فرمان</param>
        /// <param name="Rivision">شماره دوره پارامتر</param>
        /// <returns>رشته ي فرمان اس كيو ال را بر مي گرداند</returns>
        private static String GetTransferParamString(String ParamName, String SelectString,
                                                     DataTable DataTableParams, ArrayList ArrayListParams,
                                                     Boolean IsTesting, Int32 Rivision)
        {
            #region Prepare Execution

            #region Check String Null Value
            if (String.IsNullOrEmpty(SelectString))
            {
                PersianMessageBox.Show("هیچ دستوری نوشته نشده ، بررسی ممكن نیست!",
                                       "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                       MessageBoxDefaultButton.Button1);
                return String.Empty;
            }
            #endregion

            #region Variables Declaration

            Boolean ContainSumType0 = false;
            Boolean ContainSumType1 = false;
            Boolean ContainSumType2 = false;
            Boolean ContainSumType3 = false;

            #endregion

            #region Fill ArrayList Used Parameters
            var ArrayListUsedParams = new ArrayList();
            foreach (String Str in ArrayListParams)
            {
                String SearchWord = @"\b" + Str + @"\b";
                if (Regex.IsMatch(SelectString, SearchWord))
                    ArrayListUsedParams.Add(Str);
            }
            #endregion

            #region Delete from ParamIn
            Parameters.DelParamIDIN(ParamName, "Transfer");
            #endregion
            
            #region Replace Complete Objects Name

            ParamLevel = 0;

            foreach (String Str in ArrayListUsedParams)
            {
                String SchemaName = Parameters.GetParamLastObjectP(Str, "Transfer");
                Parameters.SetParamIDIN(ParamName, Str, "Transfer");
                PLevel = Parameters.GetParamLevel(Str, "Transfer");
                if (PLevel > ParamLevel)
                {
                    ParamLevel = PLevel;
                }
                var TextToFind = new Regex(Str + @"\b");
                String ObjectFullName = SchemaName + "." + Str;
                SelectString = TextToFind.Replace(SelectString, ObjectFullName);
            }

            #endregion
            
            #region Check Used Variables For Sum Types

            #region Find Containing of Sum Params
            foreach (String Str in ArrayListUsedParams)
            {
                String CurentParamSum = Parameters.GetParamSumType(Str, "Transfer");

                if (CurentParamSum == "0") ContainSumType0 = true;
                else if (CurentParamSum == "1") ContainSumType1 = true;
                else if (CurentParamSum == "2") ContainSumType2 = true;
                else if (CurentParamSum == "3") ContainSumType3 = true;
            }
            #endregion

            #region Set Current Param SumType
            if (ContainSumType0) _ParamSumType = "0";
            else if (ContainSumType1) _ParamSumType = "1";
            else if (ContainSumType2) _ParamSumType = "2";
            else if (ContainSumType3) _ParamSumType = "3";
            #endregion

            #endregion

            #endregion

            #region SELECT
            String SelectColumns = String.Empty;

            #region Make SELECT

            #region Contain Sum Type 0 (Fixed Or Mixed)
            if (ContainSumType0)
            {
                _ParamSumType = "0";
                SelectColumns = "SELECT " +
                                "Transfer.FixedParameters.CompanyCode , " +
                                "Transfer.FixedParameters.Date , " +
                                "Transfer.FixedParameters.Hour , " +
                                "Transfer.FixedParameters.EquipmentType , " +
                                "Transfer.FixedParameters.EquipmentCode , " +
                                "Transfer.FixedParameters.EquipmentPosition , " +
                                "Transfer.FixedParameters.VoltageLevel, ";
            }
                #endregion

                #region Contain Sum Type 1 (SumByLineType)

            else if (ContainSumType1)
            {
                _ParamSumType = "1";
                SelectColumns = "SELECT " +
                                "Transfer.FixedParameters.CompanyCode , " +
                                "Transfer.FixedParameters.Date , " +
                                "Transfer.FixedParameters.Hour , " +
                                "Transfer.FixedParameters.EquipmentType , " +
                                "Transfer.FixedParameters.EquipmentPosition , " +
                                "Transfer.FixedParameters.VoltageLevel,";
            }
                #endregion

                #region Contain Sum Type 2 (SumByRegion)

            else if (ContainSumType2)
            {
                _ParamSumType = "2";
                SelectColumns = "SELECT " +
                                "Transfer.FixedParameters.CompanyCode , " +
                                "Transfer.FixedParameters.Date , " +
                                "Transfer.FixedParameters.Hour , " +
                                "Transfer.FixedParameters.EquipmentType ," +
                                "Transfer.FixedParameters.EquipmentPosition,";
            }
                #endregion

                #region Contain Sum Type 3 (SumByMonth)

            else if (ContainSumType3)
            {
                _ParamSumType = "3";
                SelectColumns = "SELECT " +
                                "Transfer.FixedParameters.CompanyCode ," +
                                "Transfer.FixedParameters.Date ,";
            }
            #endregion

            #endregion


            if (IsTesting)
            {
       

                SelectString = SelectString.Insert(0, SelectColumns + " (CASE ");
                SelectString = SelectString.Insert(SelectString.Length, " END) AS '" + ParamName +
                                                                        "' FROM Transfer.FixedParameters");
            }
            else
            {

                SelectString = SelectString.Insert(0, SelectColumns + " (CASE ");
                SelectString = SelectString.Insert(SelectString.Length, " END) AS '" + ParamName + "'"
                                                                        + " into TransferParam." + ParamName + " FROM Transfer.FixedParameters");
            }
            #endregion

            #region INNER JOINs
            foreach (String Str in ArrayListUsedParams)
            {
                DataRow[] Row = DataTableParams.Select("EnglishName = '" + Str + "'");

                #region Check Non-Fixed Paameters (Mixed & Sum)

                if (Row[0]["Category"].ToString() != "Fx")
                {
                    String CurentParamSum = Parameters.GetParamSumType(Str, "Transfer");
                    String ObjectName = Parameters.GetParamLastObjectP(Str, "Transfer");

                    #region Sum Type 0 (Mixed Parameters)
                    if (CurentParamSum == "0")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Transfer.FixedParameters.EquipmentCode = " +
                                                 ObjectName + ".[EquipmentCode]" +
                                                 " AND Transfer.FixedParameters.Date = " +
                                                 ObjectName + ".[Date] "+
                        " AND Transfer.FixedParameters.Hour = " +
                                             ObjectName + ".[Hour] "+
                                                      " AND Transfer.FixedParameters.CompanyCode= " +
                                             ObjectName + ".CompanyCode ";
                        SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                    }
                        #endregion

                    #region Sum Type 1 (SumByLineType)
                    else if (CurentParamSum == "1")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Transfer.FixedParameters.[Voltagelevel]= " +
                                                 ObjectName + ".VoltageLevel " +
                                                 "AND  Transfer.FixedParameters.[EquipmentType]=" +
                                                 ObjectName + ".[EquipmentType] " +
                                                 " AND Transfer.FixedParameters.[Date] = " +
                                                 ObjectName + ".[Date] ";
                        SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                    }
                        #endregion

                    #region Sum Type 2 (SumByRegionName)
                    else if (CurentParamSum == "2")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Transfer.FixedParameters.[EquipmentPosition]= " +
                                                 ObjectName + ".[EquipmentPosition] " +
                                                 "AND  Transfer.FixedParameters.[EquipmentType]=" +
                                                 ObjectName + ".[EquipmentType] " +
                                                 " AND Transfer.FixedParameters.[Date] = " +
                                                 ObjectName + ".[Date] ";
                        SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                    }
                        #endregion

                    #region Sum Type 3 (SumByMonth)
                    else if (CurentParamSum == "3")
                    {
                        String InsertInnerJoin = " INNER JOIN " + ObjectName +
                                                 " ON Transfer.FixedParameters.CompanyName  = " +
                                                 ObjectName + ".[CompanyName] " +
                                                 "AND  Transfer.FixedParameters.[Date]=" +
                                                 ObjectName + ".[Date] ";
                        SelectString = SelectString.Insert(SelectString.Length, InsertInnerJoin);
                    }
                    #endregion

                }

                    #endregion

                    #region Check Fixed Parameters
                else
                {
                    var TextToFind = new Regex(@"\b" + Str + @"\b");
                    SelectString = TextToFind.Replace(SelectString, "[" + Str + "]");
                }
                #endregion

            } // End of "foreach"
            #endregion

            #region GROUP BY

            String InsertGROUPBY = String.Empty;

            #region Fixed Parameters GROUP BY

            #region Contain Sum Type 0
            if (ContainSumType0)
                InsertGROUPBY = " GROUP BY " +
                                "Transfer.FixedParameters.CompanyCode , " +
                                "Transfer.FixedParameters.Date , " +
                                "Transfer.FixedParameters.Hour , " +
                                "Transfer.FixedParameters.EquipmentType , " +
                                "Transfer.FixedParameters.EquipmentCode , " +
                                "Transfer.FixedParameters.EquipmentPosition , " +
                                "Transfer.FixedParameters.VoltageLevel, ";
                #endregion

                #region Contain Sum Type 1 (SumByLineType)
            else if (ContainSumType1)
                InsertGROUPBY = " GROUP BY " +
                                "Transfer.FixedParameters.CompanyCode , " +
                                "Transfer.FixedParameters.Date , " +
                                "Transfer.FixedParameters.Hour , " +
                                "Transfer.FixedParameters.EquipmentType , " +
                                "Transfer.FixedParameters.EquipmentPosition , " +
                                "Transfer.FixedParameters.VoltageLevel,";
                #endregion

                #region Contain Sum Type 2 (SumByRegionName)
            else if (ContainSumType2)
                InsertGROUPBY = " GROUP BY " +
                                "Transfer.FixedParameters.CompanyCode , " +
                                "Transfer.FixedParameters.Date , " +
                                "Transfer.FixedParameters.Hour , " +
                                "Transfer.FixedParameters.EquipmentType , " +
                                "Transfer.FixedParameters.EquipmentPosition,";


                #endregion

                #region Contain Sum Type 3 (SumByMonth)
            else if (ContainSumType3)
                InsertGROUPBY = " GROUP BY " +
                                "Transfer.FixedParameters.CompanyCode, " +
                                "Transfer.FixedParameters.Date , ";
            #endregion

            #endregion

            #region Non-Fixed Columns GROUP BY
            for (int i = 0; i < ArrayListUsedParams.Count; i++)
            {
                String Str = ArrayListUsedParams[i].ToString();
                String ObjectName = Parameters.GetParamLastObjectP(Str, "Transfer");
                if (i < ArrayListUsedParams.Count - 1)
                    InsertGROUPBY += ObjectName + "." + Str + ", ";
                else InsertGROUPBY += ObjectName + "." + Str;
            } // End Of "for" Loop
            #endregion

            #region Insert GROUP BY Text
            if (ContainSumType1 || ContainSumType2 || ContainSumType3)
                SelectString = SelectString.Insert(SelectString.Length, InsertGROUPBY);
            #endregion

            #endregion

            #region Return Results
            return SelectString;
            #endregion
        }
        #endregion
        
        #region String RecursiveSearchInTree(TreeNode TreeNode)

        /// <summary>
        /// تابع بازگشتی جستجو در گره های یك درخت
        /// </summary>
        /// <param name="TreeNode">گره دریافتی برای جستجوی فرزندان آن</param>
        /// <returns>رشته تولید شده از گره</returns>
        private static String RecursiveSearchInTree(Node TreeNode)
        {
            String ReturnValue = String.Empty;
            foreach (Node node in TreeNode.Nodes)
            {
                Int32 NodeLevel = Regex.Matches(node.FullPath, @"\\").Count - 1;
                #region Result Node (R)
                if (node.Tag.ToString() == "R")
                {
                    String TheQuery = "WHEN ";
                    Node MyNode = node;
                    for (int i = 0; i < NodeLevel; i++)
                    {
                        MyNode = MyNode.Parent;
                        String NodeCondition =
                            MyNode.Text.Substring(3, MyNode.Text.Length - 8);
                        if (i == NodeLevel - 1)
                            TheQuery += NodeCondition;
                        else
                            TheQuery += NodeCondition + " AND ";
                    }
                    TheQuery += " THEN " + node.Text;
                    // افزودن مقدار به رشته حاصل:
                    ReturnValue += TheQuery + " ";
                    return ReturnValue;
                }
                #endregion
                ReturnValue += RecursiveSearchInTree(node);
            }
            return ReturnValue;
        }

        #endregion

        #region String GetTreeXml(AdvTree TreeToSerialize)

        /// <summary>
        /// متدی برای دریافت فایل ایكس ام ال تولید شده از ساختار درخت
        /// </summary>
        /// <returns>رشته ایكس ام ال تولید شده</returns>
        private static String GetTreeXml(TreeView TreeToSerialize)
        {
            String XmlString = String.Empty;
            try
            {
                TreeToSerialize.SaveXml("TempXml.Xml");
                XmlString = File.ReadAllText("TempXml.Xml");
                File.Delete("TempXml.Xml");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            return XmlString;
        }

        #endregion

 

        #endregion

    }
}