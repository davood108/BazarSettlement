﻿using System;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.Forms.Management.ManageParameters.Parameters;
using MohanirPouya.Forms.Management.ManageParameters.ParametersC;
using MohanirVBClasses;

namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{
    public partial class ParamStructure : Form
    {
        #region Field
        private readonly DbSMDataContext _dbSm;
        private readonly string _partName;
        private IOrderedQueryable<ParametersLogS> _queryS;
        private IOrderedQueryable<ParametersLogB> _queryB;
        private IOrderedQueryable<ParametersLogT> _queryT;
        private IQueryable<View_ParamInParamB> _query2B;
        private IQueryable<View_ParamInParamT> _query2T;
        private IQueryable<ParameterS> _querySc;
        private IQueryable<ParameterB> _queryBc;
        private IQueryable<ParameterT> _queryTc;
        private string _categori;
        private readonly string _paramName;
        private readonly int _typ;
        private IQueryable<ParametersLogS> _queryS1;
        private IQueryable<ParametersLogB> _queryB1;
        private IQueryable<ParametersLogT> _queryT1;
        private string[] _pname;
        private int _icount;
        private readonly int _plevel;
        private IQueryable<View_ParamInParamS> _query2S;
       #endregion

        #region Ctor

        public ParamStructure(String partName)
        {
            InitializeComponent();
            _dbSm = new DbSMDataContext
                (DbBizClass.DbConnStr);
  
            _partName = partName;
            _typ = 0;
            rbt_DepOn.Visible = false;
            rbt_OnDep.Visible = false;

            #region Set Form Title
            if (_partName == "Sellers")
                lblTitle.Text = @"ساختار پارامترهای بخش فروش";
            else if (_partName == "Buyers")
                lblTitle.Text = @"ساختار پارامترهای بخش خرید";
            else if (_partName == "Transfer")
                lblTitle.Text = @"ساختار پارامترهای بخش خدمات انتقال";
            #endregion

            ShowDialog();
        }
        public ParamStructure(String partName,String paramName)
        {
            InitializeComponent();
            _dbSm = new DbSMDataContext
                (DbBizClass.DbConnStr);

            _partName = partName;
            _paramName = paramName;
            _typ = 1;

            #region Set Form Title
            if (_partName == "Sellers")
                lblTitle.Text = @"ساختار پارامترهای بخش فروش";
            else if (_partName == "Buyers")
                lblTitle.Text = @"ساختار پارامترهای بخش خرید";
            else if (_partName == "Transfer")
                lblTitle.Text = @"ساختار پارامترهای بخش خدمات انتقال";
            #endregion

            ShowDialog();
        }
        public ParamStructure(String partName, String paramName,int plevel)
        {
            InitializeComponent();
            _dbSm = new DbSMDataContext
                (DbBizClass.DbConnStr);

            _partName = partName;
            _paramName = paramName;
            _typ = 2;
            _plevel = plevel;

            #region Set Form Title
            if (_partName == "Sellers")
                lblTitle.Text = @"ساختار پارامترهای بخش فروش";
            else if (_partName == "Buyers")
                lblTitle.Text = @"ساختار پارامترهای بخش خرید";
            else if (_partName == "Transfer")
                lblTitle.Text = @"ساختار پارامترهای بخش خدمات انتقال";
            #endregion

            FillTree("OnDeP");
        }


        #endregion

        #region Event

        #region Load
        private void ParamStructureLoad(object sender, EventArgs e)
        {
            FillTree("OnDeP");
        }
        #endregion
        
        #region treeView1_NodeMouseDoubleClick
        private void TreeView1NodeMouseDoubleClick(object sender,                     TreeNodeMouseClickEventArgs e)
        {
            switch (_partName)
            {
                case "Sellers":
                    {
                        _querySc = _dbSm.ParameterS.
                            Where(data => data.EnglishName == e.Node.Text);
                        foreach (var t in _querySc)
                        {
                            _categori = t.Category;
                        }
                    }
                    break;
                case "Buyers":
                    {
                        _queryBc = _dbSm.ParameterBs.
            Where(data => data.EnglishName == e.Node.Text);
                        foreach (var t in _queryBc)
                        {
                            _categori = t.Category;
                        }

                    }
                    break;
                case "Transfer":
                    {
                        _queryTc = _dbSm.ParameterTs.
          Where(data => data.EnglishName == e.Node.Text);
                        foreach (var t in _queryTc)
                        {
                            _categori = t.Category;
                        }

                    }
                    break;
            }

            switch (_categori)
            {
                case "Fx"  :
                    {
                        PersianMessageBox.Show("پارامتر انتخابی از نوع پارامترهای اصلی می باشد!", "خطا",
                                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    }
                    break;
                case "Pa":
                    {
                        new FrmModifyParameters(e.Node.Text,
                                 _partName, _categori); 
                    }
                    break;
                case "Pc":
                    {
                        new FrmModifyParametersC(e.Node.Text,
                                 _partName, _categori); 
                    }
                    break;

            }
        
        }
        #endregion

        #endregion

        #region Method

        #region Fill Tree

        private void FillTree(string paramDep)
        {
            #region ONDep
            if (paramDep == "OnDeP")
            {
                treeView1.BeginUpdate();
                treeView1.Nodes.Clear();
                if (_typ == 0)
                {
                    #region Typ 0

                    switch (_partName)
                    {
                        case "Sellers":
                            {
                                _queryS = _dbSm.ParametersLogS.
                                    Where(data => data.PLevel == 0).
                                    OrderBy(data => data.EnglishName);
                                foreach (var v in _queryS)
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                        case "Buyers":
                            {
                                _queryB = _dbSm.ParametersLogBs.
                                    Where(data => data.PLevel == 0).
                                    OrderBy(data => data.EnglishName);
                                foreach (var v in _queryB)
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                        case "Transfer":
                            {
                                _queryT = _dbSm.ParametersLogTs.
                                    Where(data => data.PLevel == 0).
                                    OrderBy(data => data.EnglishName);
                                foreach (var v in _queryT)
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                    }

                    #endregion
                }
                else
                {
                    #region Typ 1,2

                    switch (_partName)
                    {
                        case "Sellers":
                            {
                                _queryS1 = _dbSm.ParametersLogS.
                                    Where(data => data.EnglishName == _paramName && data.Revision == 0);

                                foreach (var v in _queryS1.Distinct())
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                        case "Buyers":
                            {
                                _queryB1 = _dbSm.ParametersLogBs.
                                    Where(data => data.EnglishName == _paramName);
                                foreach (var v in _queryB1)
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                        case "Transfer":
                            {
                                _queryT1 = _dbSm.ParametersLogTs.
                                    Where(data => data.EnglishName == _paramName);
                                foreach (var v in _queryT1)
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                    }

                    #endregion
                }


                _pname = new string[50000];//
                _icount = 1;


                foreach (TreeNode t1 in treeView1.Nodes)
                {
                    RecursiveSearchInTree(t1,"OnDeP");
                }

                treeView1.EndUpdate();

                if (_typ == 2)
                {
                    UpdatePlevel();
                }
            }
            #endregion

            #region DepON
            if (paramDep == "DepOn")
            {
                treeView1.BeginUpdate();
                treeView1.Nodes.Clear();
          
                    #region Typ 1,2

                    switch (_partName)
                    {
                        case "Sellers":
                            {
                                _queryS1 = _dbSm.ParametersLogS.
                                    Where(data => data.EnglishName == _paramName && data.Revision == 0);

                                foreach (var v in _queryS1.Distinct())
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                        case "Buyers":
                            {
                                _queryB1 = _dbSm.ParametersLogBs.
                                    Where(data => data.EnglishName == _paramName);
                                foreach (var v in _queryB1)
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                        case "Transfer":
                            {
                                _queryT1 = _dbSm.ParametersLogTs.
                                    Where(data => data.EnglishName == _paramName);
                                foreach (var v in _queryT1)
                                {
                                    treeView1.Nodes.Add(v.EnglishName, v.EnglishName);
                                }

                            }
                            break;
                    }

                    #endregion
              //  }


                _pname = new string[40000];
                _icount = 1;


                foreach (TreeNode t1 in treeView1.Nodes)
                {
                    RecursiveSearchInTree(t1,"DepOn");
                }

                treeView1.EndUpdate();

                if (_typ == 2)
                {
                    UpdatePlevel();
                }
            }
            #endregion


        }


        #endregion

        #region String RecursiveSearchInTree(TreeNode TreeNode)


        private void RecursiveSearchInTree(TreeNode treeNode1,String paramDep)
        {
            
            #region OnDeP
            if (paramDep == "OnDeP")
            {
                switch (_partName)
                {
                    case "Sellers":
                        {

                            _query2S = _dbSm.View_ParamInParamS.Where(d => d.Expr1  == treeNode1.Text);
                            foreach (var v1 in _query2S)
                            {
                                TreeNode t2 = treeNode1.Nodes.Add(v1.EnglishName);

                                _pname[_icount] = v1.EnglishName;
                                _icount = _icount + 1;

                                RecursiveSearchInTree(t2, "OnDeP");
                            }
                        }
                        break;
                    case "Buyers":
                        {
                            _query2B = _dbSm.View_ParamInParamBs.Where(d => d.Expr1 == treeNode1.Text);
                            foreach (var v1 in _query2B)
                            {
                                TreeNode t2 = treeNode1.Nodes.Add(v1.EnglishName);
                                _pname[_icount] = v1.EnglishName;
                                _icount = _icount + 1;

                                RecursiveSearchInTree(t2, "OnDeP");
                            }

                        }
                        break;
                    case "Transfer":
                        {

                            _query2T = _dbSm.View_ParamInParamTs.Where(d => d.Expr1 == treeNode1.Text);
                            foreach (var v1 in _query2T)
                            {
                                TreeNode t2 = treeNode1.Nodes.Add(v1.EnglishName);
                                _pname[_icount] = v1.EnglishName;
                                _icount = _icount + 1;

                                RecursiveSearchInTree(t2, "OnDeP");
                            }
                        }
                        break;
                }
            }

            #endregion

            #region DepOn
            if (paramDep == "DepOn")
            {
                switch (_partName)
                {
                    case "Sellers":
                        {

                            _query2S = _dbSm.View_ParamInParamS.Where(d => d.EnglishName== treeNode1.Text);
                            foreach (var v1 in _query2S)
                            {
                               var  t2 = treeNode1.Nodes.Add(v1.Expr1);

                                _pname[_icount] = v1.Expr1;
                                _icount = _icount + 1;

                                RecursiveSearchInTree(t2, "DepOn");
                            }
                        }
                        break;
                    case "Buyers":
                        {
                            _query2B = _dbSm.View_ParamInParamBs.Where(d => d.EnglishName == treeNode1.Text);
                            foreach (var v1 in _query2B)
                            {
                                var t2 = treeNode1.Nodes.Add(v1.Expr1);
                                _pname[_icount] = v1.Expr1;
                                _icount = _icount + 1;

                                RecursiveSearchInTree(t2, "DepOn");
                            }

                        }
                        break;
                    case "Transfer":
                        {

                            _query2T = _dbSm.View_ParamInParamTs.Where(d => d.EnglishName == treeNode1.Text);
                            foreach (var v1 in _query2T)
                            {
                                var t2 = treeNode1.Nodes.Add(v1.Expr1);
                                _pname[_icount] = v1.Expr1;
                                _icount = _icount + 1;

                                RecursiveSearchInTree(t2, "DepOn");
                            }
                        }
                        break;
                }
            }

            #endregion

        }

        #endregion

        #region UpdatePlevel
        private void UpdatePlevel()
        {
            foreach (var variable in _pname.Distinct())
            {
          _dbSm.SP_UpdatePlevel(variable, _plevel);
            }
        }
        #endregion

        #region rbt_Param_OnDep
                 private void rbt_OnDep_Click(object sender, EventArgs e)
                  {
                       treeView1.BeginUpdate();
                       treeView1.Nodes.Clear();
                       treeView1.EndUpdate();
                       treeView1.Refresh();
                      FillTree("OnDeP");
                   }
        #endregion

        #region rbt_Param_DepOn
               private void RbtDepOnClick(object sender, EventArgs e)
                   {
                        treeView1.BeginUpdate();
                        treeView1.Nodes.Clear();
                        treeView1.EndUpdate();
                        treeView1.Refresh();
                        FillTree("DepOn");
                     }
       #endregion

              
        #endregion

               #region help
               private void ParamStructureHelpRequested(object sender, HelpEventArgs hlpevent)
               {
                   System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "ParamStructure.htm");
               }
               #endregion

    }
}