﻿namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{
    partial class ConditionTreeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PureComponents.TreeView.ContextMenuStrings contextMenuStrings1 = new PureComponents.TreeView.ContextMenuStrings();
            PureComponents.TreeView.TreeViewStyle treeViewStyle1 = new PureComponents.TreeView.TreeViewStyle();
            PureComponents.TreeView.NodeStyle nodeStyle1 = new PureComponents.TreeView.NodeStyle();
            PureComponents.TreeView.CheckBoxStyle checkBoxStyle1 = new PureComponents.TreeView.CheckBoxStyle();
            PureComponents.TreeView.ExpandBoxStyle expandBoxStyle1 = new PureComponents.TreeView.ExpandBoxStyle();
            PureComponents.TreeView.NodeTooltipStyle nodeTooltipStyle1 = new PureComponents.TreeView.NodeTooltipStyle();
            PureComponents.TreeView.TreeViewPathSelectorStyle treeViewPathSelectorStyle1 = new PureComponents.TreeView.TreeViewPathSelectorStyle();
            this.CodeTreeView = new PureComponents.TreeView.TreeView();
            this.ParameterPanel = new DevComponents.DotNetBar.PanelEx();
            this.TreeViewNavigator = new PureComponents.TreeView.TreeViewPathSelector();
            this.btnClearTreeView = new DevComponents.DotNetBar.ButtonX();
            this.btnEditNode = new DevComponents.DotNetBar.ButtonX();
            this.btnAddResult = new DevComponents.DotNetBar.ButtonX();
            this.btnAddChildCondition = new DevComponents.DotNetBar.ButtonX();
            this.btnAddRootCondition = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTreeView)).BeginInit();
            this.ParameterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // CodeTreeView
            // 
            this.CodeTreeView.AllowAdding = false;
            this.CodeTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CodeTreeView.ContextMenuArranging = true;
            this.CodeTreeView.ContextMenuEditing = true;
            contextMenuStrings1.AddNode = "افزودن";
            contextMenuStrings1.Collapse = "بستن";
            contextMenuStrings1.Copy = "كپی";
            contextMenuStrings1.DeleteNode = "حذف";
            contextMenuStrings1.EditNode = "ویرایش";
            contextMenuStrings1.Expand = "باز كردن";
            contextMenuStrings1.MoveBottom = "ارسال به پایین";
            contextMenuStrings1.MoveDown = "انتقال به پایین";
            contextMenuStrings1.MoveLeft = "جابجایی به چپ";
            contextMenuStrings1.MoveRight = "جابجایی به راست";
            contextMenuStrings1.MoveTop = "ارسال به بالا";
            contextMenuStrings1.MoveUp = "انتقال به بالا";
            contextMenuStrings1.Paste = "چسباندن";
            this.CodeTreeView.ContextMenuStrings = contextMenuStrings1;
            this.CodeTreeView.LabelEdit = false;
            this.CodeTreeView.Location = new System.Drawing.Point(133, 41);
            this.CodeTreeView.Multiline = false;
            this.CodeTreeView.Name = "CodeTreeView";
            this.CodeTreeView.Size = new System.Drawing.Size(366, 348);
            treeViewStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
            treeViewStyle1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(51)))), ((int)(((byte)(161)))));
            treeViewStyle1.FadeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(193)))), ((int)(((byte)(255)))));
            treeViewStyle1.FillStyle = PureComponents.TreeView.FillStyle.VerticalCentreFading;
            treeViewStyle1.FlashColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(108)))));
            treeViewStyle1.FullRowSelect = false;
            treeViewStyle1.HighlightSelectedPath = false;
            treeViewStyle1.LineStyle = PureComponents.TreeView.LineStyle.Dot;
            checkBoxStyle1.BorderColor = System.Drawing.Color.CornflowerBlue;
            checkBoxStyle1.BorderStyle = PureComponents.TreeView.CheckBoxBorderStyle.Solid;
            checkBoxStyle1.CheckColor = System.Drawing.Color.RoyalBlue;
            checkBoxStyle1.HoverBackColor = System.Drawing.Color.LightSteelBlue;
            checkBoxStyle1.HoverBorderColor = System.Drawing.Color.RoyalBlue;
            checkBoxStyle1.HoverCheckColor = System.Drawing.Color.Black;
            nodeStyle1.CheckBoxStyle = checkBoxStyle1;
            expandBoxStyle1.BackColor = System.Drawing.Color.LightBlue;
            expandBoxStyle1.ForeColor = System.Drawing.Color.Crimson;
            nodeStyle1.ExpandBoxStyle = expandBoxStyle1;
            nodeStyle1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nodeStyle1.HoverBackColor = System.Drawing.Color.LightCoral;
            nodeStyle1.SelectedBackColor = System.Drawing.Color.LightSalmon;
            nodeStyle1.SelectedBorderColor = System.Drawing.Color.CornflowerBlue;
            nodeStyle1.SelectedFillStyle = PureComponents.TreeView.FillStyle.VistaFading;
            nodeStyle1.SelectedForeColor = System.Drawing.Color.RoyalBlue;
            nodeTooltipStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
            nodeTooltipStyle1.BorderColor = System.Drawing.Color.RoyalBlue;
            nodeTooltipStyle1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nodeStyle1.TooltipStyle = nodeTooltipStyle1;
            nodeStyle1.UnderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            treeViewStyle1.NodeStyle = nodeStyle1;
            treeViewStyle1.ShowSubitemsIndicator = true;
            this.CodeTreeView.Style = treeViewStyle1;
            this.CodeTreeView.TabIndex = 0;
            // 
            // ParameterPanel
            // 
            this.ParameterPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.ParameterPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ParameterPanel.Controls.Add(this.TreeViewNavigator);
            this.ParameterPanel.Controls.Add(this.CodeTreeView);
            this.ParameterPanel.Controls.Add(this.btnClearTreeView);
            this.ParameterPanel.Controls.Add(this.btnEditNode);
            this.ParameterPanel.Controls.Add(this.btnAddResult);
            this.ParameterPanel.Controls.Add(this.btnAddChildCondition);
            this.ParameterPanel.Controls.Add(this.btnAddRootCondition);
            this.ParameterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParameterPanel.Location = new System.Drawing.Point(0, 0);
            this.ParameterPanel.Name = "ParameterPanel";
            this.ParameterPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ParameterPanel.RightToLeftLayout = true;
            this.ParameterPanel.Size = new System.Drawing.Size(502, 392);
            this.ParameterPanel.Style.Alignment = System.Drawing.StringAlignment.Far;
            this.ParameterPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.ParameterPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.ParameterPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ParameterPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ParameterPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ParameterPanel.Style.GradientAngle = 90;
            this.ParameterPanel.Style.LineAlignment = System.Drawing.StringAlignment.Near;
            this.ParameterPanel.TabIndex = 0;
            // 
            // TreeViewNavigator
            // 
            this.TreeViewNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeViewNavigator.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeViewNavigator.Location = new System.Drawing.Point(133, 3);
            this.TreeViewNavigator.Name = "TreeViewNavigator";
            this.TreeViewNavigator.Size = new System.Drawing.Size(366, 32);
            treeViewPathSelectorStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
            treeViewPathSelectorStyle1.FadeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(193)))), ((int)(((byte)(255)))));
            this.TreeViewNavigator.Style = treeViewPathSelectorStyle1;
            this.TreeViewNavigator.TabIndex = 1;
            this.TreeViewNavigator.TreeView = this.CodeTreeView;
            // 
            // btnClearTreeView
            // 
            this.btnClearTreeView.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnClearTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearTreeView.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btnClearTreeView.Location = new System.Drawing.Point(7, 364);
            this.btnClearTreeView.Name = "btnClearTreeView";
            this.btnClearTreeView.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.btnClearTreeView.Size = new System.Drawing.Size(120, 23);
            this.btnClearTreeView.TabIndex = 6;
            this.btnClearTreeView.Text = "حذف تمام عناصر";
            this.btnClearTreeView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnClearTreeView_MouseClick);
            // 
            // btnEditNode
            // 
            this.btnEditNode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEditNode.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnEditNode.Location = new System.Drawing.Point(7, 97);
            this.btnEditNode.Name = "btnEditNode";
            this.btnEditNode.Size = new System.Drawing.Size(120, 23);
            this.btnEditNode.TabIndex = 5;
            this.btnEditNode.Text = "ویرایش عنصر ";
            this.btnEditNode.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnEditNode_MouseClick);
            // 
            // btnAddResult
            // 
            this.btnAddResult.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddResult.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnAddResult.Location = new System.Drawing.Point(7, 68);
            this.btnAddResult.Name = "btnAddResult";
            this.btnAddResult.Size = new System.Drawing.Size(120, 23);
            this.btnAddResult.TabIndex = 4;
            this.btnAddResult.Text = "افزودن نتیجه";
            this.btnAddResult.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnAddResult_MouseClick);
            // 
            // btnAddChildCondition
            // 
            this.btnAddChildCondition.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddChildCondition.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddChildCondition.Location = new System.Drawing.Point(7, 39);
            this.btnAddChildCondition.Name = "btnAddChildCondition";
            this.btnAddChildCondition.Size = new System.Drawing.Size(120, 23);
            this.btnAddChildCondition.SplitButton = true;
            this.btnAddChildCondition.TabIndex = 3;
            this.btnAddChildCondition.Text = "افزودن شرط فرزند";
            this.btnAddChildCondition.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnAddChildCondition_MouseClick);
            // 
            // btnAddRootCondition
            // 
            this.btnAddRootCondition.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddRootCondition.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddRootCondition.Location = new System.Drawing.Point(7, 10);
            this.btnAddRootCondition.Name = "btnAddRootCondition";
            this.btnAddRootCondition.Size = new System.Drawing.Size(120, 23);
            this.btnAddRootCondition.SplitButton = true;
            this.btnAddRootCondition.TabIndex = 2;
            this.btnAddRootCondition.Text = "افزودن شرط ریشه";
            this.btnAddRootCondition.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnAddRootCondition_MouseClick);
            // 
            // ConditionTreeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ParameterPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Name = "ConditionTreeControl";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(502, 392);
            ((System.ComponentModel.ISupportInitialize)(this.CodeTreeView)).EndInit();
            this.ParameterPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx ParameterPanel;
        private DevComponents.DotNetBar.ButtonX btnAddResult;
        private DevComponents.DotNetBar.ButtonX btnAddRootCondition;
        internal PureComponents.TreeView.TreeViewPathSelector TreeViewNavigator;
        public PureComponents.TreeView.TreeView CodeTreeView;
        private DevComponents.DotNetBar.ButtonX btnEditNode;
        private DevComponents.DotNetBar.ButtonX btnAddChildCondition;
        private DevComponents.DotNetBar.ButtonX btnClearTreeView;
    }
}