﻿#region using

using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;

#endregion

namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{

    #region public class MohanirCompilerRichTextBox
    /// <summary>
    /// كلاسي براي كامپايل و تغيير رنگ كلمات كليدي
    /// </summary>
    public class MohanirCompilerRichTextBox : System.Windows.Forms.RichTextBox
    {

        #region Fields

        private readonly SyntaxSettings _syntaxSettings;
        private static bool _canPaint;
        private string _lineString;
        private int _lineLength;
        private int _lineStart;
        private int _lineEnd;
        private string _keywordsString;

        #endregion

        #region Constructors

        /// <summary>
        /// كلاسي براي كامپايل و تغيير رنگ كلمات كليدي يك جعبه متن
        /// </summary>
        public MohanirCompilerRichTextBox()
        {
            _syntaxSettings = new SyntaxSettings();
            _canPaint = true;
            _lineString = String.Empty;
            _lineLength = 0;
            _lineStart = 0;
            _lineEnd = 0;
            _keywordsString = String.Empty;
        }
        #endregion

        #region Properties

        #region public SyntaxSettings CompilerSettings

        /// <summary>
        /// تنظيمات مربوط به كامپايلر
        /// </summary>
        [Browsable(false)]
        public SyntaxSettings CompilerSettings
        {
            get { return _syntaxSettings; }
        }

        #endregion

        #endregion

        #region Events

        #region protected void OnTextChanged Event

        /// <summary>
        /// رخداد باز نویسی شده ی زمان تغییر متن
        /// </summary>
        /// <param name="e"></param>
        protected  override void OnTextChanged(EventArgs e)
        {
            // Calculate shit here:
            int currentSelectionStart = SelectionStart;

            _canPaint = false;

            // Find the start of the current line:
            _lineStart = currentSelectionStart;
            while ((_lineStart > 0) && (Text[_lineStart - 1] != '\n'))
                _lineStart--;

            // Find the end of the current line:
            _lineEnd = currentSelectionStart;
            while ((_lineEnd < Text.Length) && (Text[_lineEnd] != '\n'))
                _lineEnd++;

            // Calculate the length of the line:
            _lineLength = _lineEnd - _lineStart;

            // Get the current line.
            _lineString = Text.Substring(_lineStart, _lineLength);

            // Process this line:
            ProcessLine();

            _canPaint = true;
        }

        #endregion

        #region protected override void WndProc

        /// <summary>
        /// WndProc
        /// </summary>
        /// <param name="theMessage"></param>
        protected override void WndProc(ref System.Windows.Forms.Message theMessage)
        {
            if (theMessage.Msg == 0x00f)
            {
                if (_canPaint)
                    base.WndProc(ref theMessage);
                else
                    theMessage.Result = IntPtr.Zero;
            }
            else
                base.WndProc(ref theMessage);
        }

        #endregion

        #endregion

        #region Methods

        #region Private

        #region private void ProcessLine

        /// <summary>
        /// روال بررسی یك خط
        /// </summary>
        private void ProcessLine()
        {
            // Save the position and make the whole line black
            int nPosition = SelectionStart;
            SelectionStart = _lineStart;
            SelectionLength = _lineLength;
            SelectionColor = Color.Black;

            // Process the keywords
            ProcessRegex(_keywordsString, CompilerSettings.KeywordColor);
            // Process numbers
            if (CompilerSettings.EnableIntegers)
                ProcessRegex("\\b(?:[0-9]*\\.)?[0-9]+\\b", CompilerSettings.IntegerColor);
            // Process strings
            if (CompilerSettings.EnableStrings)
                ProcessRegex("\"[^\"\\\\\\r\\n]*(?:\\\\.[^\"\\\\\\r\\n]*)*\"", CompilerSettings.StringColor);
            // Process comments
            if (CompilerSettings.EnableComments && !string.IsNullOrEmpty(CompilerSettings.Comment))
                ProcessRegex(CompilerSettings.Comment + ".*$", CompilerSettings.CommentColor);

            SelectionStart = nPosition;
            SelectionLength = 0;
            SelectionColor = Color.Black;

        }

        #endregion

        #region private void ProcessRegex

        /// <summary>
        /// روال بررسی كلمات و حروف
        /// </summary>
        /// <param name="regexString">The regular Expression.</param>
        /// <param name="color">The color.</param>
        private void ProcessRegex(String regexString, Color color)
        {
            var regKeywords =
                new Regex(regexString, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            Match matchRegex;

            for (matchRegex = regKeywords.Match(_lineString);
                 matchRegex.Success;
                 matchRegex = matchRegex.NextMatch())
            {
                // Process the words
                int nStart = _lineStart + matchRegex.Index;
                int nLenght = matchRegex.Length;
                SelectionStart = nStart;
                SelectionLength = nLenght;
                SelectionColor = color;
            }
        }

        #endregion

        #endregion

        #region Public

        #region public void CompileKeywords

        /// <summary>
        /// كامپايل كلمات كليدي به صورت لغت به لغت
        /// </summary>
        public void CompileKeywords()
        {
            for (int i = 0; i < CompilerSettings.Keywords.Count; i++)
            {
                string strKeyword = CompilerSettings.Keywords[i];

                if (i == CompilerSettings.Keywords.Count - 1)
                    _keywordsString += "\\b" + strKeyword + "\\b";
                else
                    _keywordsString += "\\b" + strKeyword + "\\b|";
            }
        }

        #endregion

        #region public void ProcessAllLines

        /// <summary>
        /// بررسي خطوط موجود در جعبه متن
        /// </summary>
        public void ProcessAllLines()
        {
            _canPaint = false;

            int nStartPos = 0;
            int i = 0;
            while (i < Lines.Length)
            {
                _lineString = Lines[i];
                _lineStart = nStartPos;
                _lineEnd = _lineStart + _lineString.Length;

                ProcessLine();
                i++;

                nStartPos += _lineString.Length + 1;
            }

            _canPaint = true;
        }

        #endregion

        #endregion

        #endregion

    }
    #endregion
    
    #region public class SyntaxList
    /// <summary>
    /// كلاسي براي ذخيره سازي ليست كلمات كليدي
    /// </summary>
    public class SyntaxList
    {
        private List<String> _regexList = new List<String>();

        public List<string> RegexList
        {
            get { return _regexList; }
            set { _regexList = value; }
        }

        public Color TheColor {get; set;}
    }
    #endregion

    #region public class SyntaxSettings
    /// <summary>
    /// كلاس تنظيمات كامپايلر
    /// </summary>
    public class SyntaxSettings
    {
        #region Fields

        private readonly SyntaxList _syntaxList;

        #endregion

        #region Constructors
        public SyntaxSettings()
        {
            _syntaxList = new SyntaxList();
            Comment = String.Empty;
            CommentColor = Color.Green;
            StringColor = Color.Gray;
            IntegerColor = Color.Red;
            EnableComments = true;
            EnableIntegers = true;
            EnableStrings = true;
        }
        #endregion

        #region Properties

        #region public List<string> Keywords

        /// <summary>
        /// لیست كلمات كلیدی اضافه شده
        /// </summary>
        public List<String> Keywords
        {
            get { return _syntaxList.RegexList; }
        }

        #endregion

        #region public Color KeywordColor

        /// <summary>
        /// رنگ كلمات كلیدی
        /// </summary>
        public Color KeywordColor
        {
            get { return _syntaxList.TheColor; }
            set { _syntaxList.TheColor = value; }
        }

        #endregion

        #region public String Comment

        /// <summary>
        /// تعیین كاراكتر توضیحات برای كامپایلر
        /// </summary>
        public String Comment { get; set; }

        #endregion

        #region public Color CommentColor

        /// <summary>
        /// رنگ كاراكتر های توضیحات
        /// </summary>
        public Color CommentColor { get; set; }

        #endregion

        #region public bool EnableComments

        /// <summary>
        /// تعیین فعال یا غیر فعال بودن توضیحات
        /// </summary>
        public bool EnableComments { get; set; }

        #endregion

        #region public bool EnableIntegers

        /// <summary>
        /// فعال بودن كامپایل اعداد
        /// </summary>
        public bool EnableIntegers { get; set; }

        #endregion

        #region public bool EnableStrings

        /// <summary>
        /// فعال بودن كامپایل رشته ها
        /// </summary>
        public bool EnableStrings { get; set; }

        #endregion

        #region public Color StringColor

        /// <summary>
        /// رنگ رشته ها
        /// </summary>
        public Color StringColor { get; set; }

        #endregion

        #region public Color IntegerColor

        /// <summary>
        /// رنگ اعداد
        /// </summary>
        public Color IntegerColor { get; set; }

        #endregion

        #endregion
    }
    #endregion
    
}