﻿namespace MohanirPouya.Forms.Management.ManageParameters.Classes
{
    partial class ParamStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.lblTitle = new System.Windows.Forms.Label();
            this.rbt_DepOn = new System.Windows.Forms.RadioButton();
            this.rbt_OnDep = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(39, 106);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(555, 609);
            this.treeView1.TabIndex = 1;
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeView1NodeMouseDoubleClick);
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("B Titr", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblTitle.Location = new System.Drawing.Point(64, 25);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(502, 37);
            this.lblTitle.TabIndex = 29;
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rbt_DepOn
            // 
            this.rbt_DepOn.AutoSize = true;
            this.rbt_DepOn.Location = new System.Drawing.Point(80, 76);
            this.rbt_DepOn.Name = "rbt_DepOn";
            this.rbt_DepOn.Size = new System.Drawing.Size(225, 17);
            this.rbt_DepOn.TabIndex = 33;
            this.rbt_DepOn.Text = "پارامترهای که این متغیر به آنها وابسته است";
            this.rbt_DepOn.UseVisualStyleBackColor = true;
            this.rbt_DepOn.Click += new System.EventHandler(this.RbtDepOnClick);
            // 
            // rbt_OnDep
            // 
            this.rbt_OnDep.AutoSize = true;
            this.rbt_OnDep.Checked = true;
            this.rbt_OnDep.Location = new System.Drawing.Point(355, 76);
            this.rbt_OnDep.Name = "rbt_OnDep";
            this.rbt_OnDep.Size = new System.Drawing.Size(196, 17);
            this.rbt_OnDep.TabIndex = 32;
            this.rbt_OnDep.TabStop = true;
            this.rbt_OnDep.Text = "پارامترهای که به این متغیر وابسته اند";
            this.rbt_OnDep.UseVisualStyleBackColor = true;
            this.rbt_OnDep.Click += new System.EventHandler(this.rbt_OnDep_Click);
            // 
            // ParamStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 727);
            this.Controls.Add(this.rbt_DepOn);
            this.Controls.Add(this.rbt_OnDep);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.treeView1);
            this.Name = "ParamStructure";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ساختار پارامترها";
            this.Load += new System.EventHandler(this.ParamStructureLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.ParamStructureHelpRequested);
            //this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ParamStructure_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.RadioButton rbt_DepOn;
        private System.Windows.Forms.RadioButton rbt_OnDep;
    }
}