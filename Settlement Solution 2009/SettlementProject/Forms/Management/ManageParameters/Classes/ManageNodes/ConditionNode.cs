﻿#region using
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
#endregion

namespace MohanirPouya.Forms.Options.ManageParameters.Classes.ManageNodes
{
    /// <summary>
    /// كلاس فرم مدیریت گره های شرط
    /// </summary>
    public partial class frmConditionNode : Form
    {

        #region Fields

        #region readonly String _PartName
        /// <summary>
        /// نام بخش مورد نظر
        /// </summary>
        private readonly String _PartName;
        #endregion

        #region String _ConditionExpression
        /// <summary>
        /// فیلد عبارت ساخته شده
        /// </summary>
        private String _ConditionExpression;
        #endregion

        #region DataTable _DataTableParams
        /// <summary>
        /// جدول اطلاعات پارامتر ها و سرفصل ها
        /// </summary>
        private DataTable _DataTableParams;
        #endregion

        #region ArrayList _ArrayListParams
        /// <summary>
        /// لیست پارامتر ها و سرفصل های قابل استفاده
        /// </summary>
        private ArrayList _ArrayListParams;
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// سازنده پیش فرض كلاس
        /// </summary>
        /// <param name="PartName">نام بخش از قبیل فروشندگان ، خریداران و غیره</param>
        public frmConditionNode(String PartName)
        {
            InitializeComponent();
            IsOk = false;
            _PartName = PartName;
        }

        #endregion

        #region Properties

        #region String ResultExpression

        /// <summary>
        /// مقدار رشته ای عبارت ایجاد شده
        /// </summary>
        public String ConditionExpression
        {
            get { return _ConditionExpression; }
            set
            {
                _ConditionExpression = value;
                rtbCode.Text = _ConditionExpression;
            }
        }

        #endregion

        #region Boolean IsOk

        /// <summary>
        /// تعیین فشرده شدن كلید تایید
        /// </summary>
        public Boolean IsOk { get; set; }

        #endregion

        #region String CurentParamName

        /// <summary>
        /// نام پارامتری كه در حال ویرایش شدن است
        /// </summary>
        public String CurentParamName { get; set; }

        #endregion

        #endregion

        #region Event Handlers

        #region Form Load

        /// <summary>
        /// رویه مدیریت اجرای فرم
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            FillParametersTable();
            SetRtbCodeMembers();
        }

        #endregion

        #region lstParams Event Handlers

        #region lstParams_SelectedIndexChanged
        /// <summary>
        /// اين رويه مقادير توضيحات هر پارامتر را نمايش مي دهد
        /// </summary>
        private void lstParams_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTableReader MyDataTableReader = _DataTableParams.CreateDataReader();
            while (MyDataTableReader.Read())
                if (lstParamsList.SelectedItem.ToString() ==
                    MyDataTableReader["EnglishName"].ToString())
                    lblDescription.Text = MyDataTableReader["Description"].ToString();
            MyDataTableReader.Close();
        }
        #endregion

        #region lstParams_MouseDoubleClick
        private void lstParams_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            rtbCode.SelectedText = lstParamsList.SelectedItem.ToString();
            rtbCode.Focus();
        }
        #endregion

        #endregion

        #region Toolbar Buttons

        #region Edit Texts

        private void btnNew_Click(object sender, EventArgs e)
        {
            rtbCode.Clear();
            rtbCode.Focus();
        }

        private void btnCutTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Cut();
            rtbCode.Focus();
        }

        private void btnCopyTextFromFml_Click(object sender, EventArgs e)
        {
            rtbCode.Copy();
            rtbCode.Focus();
        }

        private void btnPasteTextToFml_Click(object sender, EventArgs e)
        {
            rtbCode.Paste();
            rtbCode.Focus();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            rtbCode.Undo();
        }

        private void btnRedo_Click(object sender, EventArgs e)
        {
            rtbCode.Redo();
        }

        #endregion

        #region Conditional Operators

        private void btnLessThan_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = " < ";
            rtbCode.Focus();
        }

        private void btnLessThanEqual_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = " <= ";
            rtbCode.Focus();
        }

        private void btnMoreThan_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = " > ";
            rtbCode.Focus();
        }

        private void btnMoreThanEqual_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = " >= ";
            rtbCode.Focus();
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = " = ";
            rtbCode.Focus();
        }

        private void btnNotEqual_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = " <> ";
            rtbCode.Focus();
        }

        #endregion

        #region Operators

        private void btnFkMax_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Max( , ) ";
            rtbCode.Focus();
        }

        private void btnFkMin_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Functions.Min( , ) ";
            rtbCode.Focus();
        }

        private void btnOpSum_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "+ ";
            rtbCode.Focus();
        }

        private void btnOpSubstract_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "- ";
            rtbCode.Focus();
        }

        private void btnOpMultiplation_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "* ";
            rtbCode.Focus();
        }

        private void btnOpDevide_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "/ ";
            rtbCode.Focus();
        }

        #endregion

        #region Functions Buttons

        private void btnSqrt_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Sqrt() ";
            rtbCode.Focus();
        }

        private void btnAbs_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Abs() ";
            rtbCode.Focus();
        }

        private void btnPower_Click(object sender, EventArgs e)
        {
            rtbCode.SelectedText = "Power( , ) ";
            rtbCode.Focus();
        }

        #endregion

        #endregion

        #region btnAccept_Click

        private void btnAccept_Click(object sender, EventArgs e)
        {
            IsOk = true;
            _ConditionExpression = rtbCode.Text;
            Close();
        }

        #endregion

        #region btnCancel_Click

        private void btnCancel_Click(object sender, EventArgs e)
        {
            IsOk = false;
            Close();
        }

        #endregion

        #endregion

        #region Methods

        #region void FillParametersTable()
        /// <summary>
        /// خواندن پارامتر ها و سرفصل ها از بانك اطلاعات
        /// </summary>
        private void FillParametersTable()
        {
            #region For Add New Condition
            if (CurentParamName == null)
                _DataTableParams = Management.ManageParameters.Classes.Parameters.GetAllParamsByPart(_PartName);
            #endregion

            #region Modify Saved Condition
            else
            {
                #region Prepare SqlCommand
                String CommandText = "SELECT ID , EnglishName , Category , [Description] " +
                    "FROM " + _PartName + ".Parameters " +
                    "WHERE EnglishName <> @CurentParam " +
                    "ORDER BY EnglishName ASC";
                SqlConnection MySqlConnection =
                    new SqlConnection(MohanirPouya.Classes.DbBizClass.DbConnStr);
                SqlCommand MySqlCommand = new SqlCommand(CommandText, MySqlConnection);
                MySqlCommand.Connection = MySqlConnection;
                MySqlCommand.Parameters.Add("@CurentParam", SqlDbType.NVarChar, 50);
                MySqlCommand.Parameters["@CurentParam"].Value = CurentParamName;
                _DataTableParams = new DataTable();
                #endregion

                #region Execute SqlCommand
                try
                {
                    MySqlCommand.Connection.Open();
                    _DataTableParams.Load(MySqlCommand.ExecuteReader());
                }
                catch (Exception EX)
                {
                    MessageBox.Show(EX.Message, "خطا!");
                }
                finally
                {
                    MySqlCommand.Connection.Close();
                }
                #endregion
            }
            #endregion

            _ArrayListParams = Management.ManageParameters.Classes.Parameters.GetParamsNameByTable(_DataTableParams);
            lstParamsList.DataSource = _ArrayListParams;
        }

        #endregion

        #region Set rtbCode Members
        /// <summary>
        /// تنظيم عناصر قابل كامپايل در جعبه متن
        /// </summary>
        private void SetRtbCodeMembers()
        {

            #region Set Keywords
            rtbCode.CompilerSettings.Keywords.Add("Functions.Max");
            rtbCode.CompilerSettings.Keywords.Add("Functions.Min");
            rtbCode.CompilerSettings.Keywords.Add("Sqrt");
            rtbCode.CompilerSettings.Keywords.Add("Abs");
            rtbCode.CompilerSettings.Keywords.Add("Power");
            #endregion

            #region Set Parameters Keyword
            foreach (String Str in _ArrayListParams)
                rtbCode.CompilerSettings.Keywords.Add(Str);
            #endregion

            #region Set Compiler Settings
            // تنظيم رنگ پارامتر ها:
            rtbCode.CompilerSettings.KeywordColor = Color.Blue;
            rtbCode.CompilerSettings.IntegerColor = Color.Red;
            // تنظيم خواص كلاس:
            rtbCode.CompilerSettings.EnableStrings = true;
            rtbCode.CompilerSettings.EnableIntegers = true;
            // كامپايل كلمات كليدي:
            rtbCode.CompileKeywords();
            // بررسي خطوط برنامه
            rtbCode.ProcessAllLines();
            #endregion

        }
        #endregion

        #endregion

        private void frmConditionNode_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "ConditionNode.htm");
        }

    }
}