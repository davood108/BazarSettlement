﻿namespace MohanirPouya.Forms.ManagementBill
{
    partial class BillExecStepT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        private new void InitializeComponent()
        {
            this.btnEXEC = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.FormPanel = new DevComponents.DotNetBar.PanelEx();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbt2 = new System.Windows.Forms.RadioButton();
            this.rbt1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.MyMainPanel.SuspendLayout();
            this.FormPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(423, 36);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(424, 0);
            // 
            // lblSep
            // 
            this.lblSep.Size = new System.Drawing.Size(508, 2);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Size = new System.Drawing.Size(504, 204);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            // 
            // btnEXEC
            // 
            this.btnEXEC.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEXEC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEXEC.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnEXEC.Location = new System.Drawing.Point(260, 160);
            this.btnEXEC.Name = "btnEXEC";
            this.btnEXEC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnEXEC.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnEXEC.Size = new System.Drawing.Size(98, 28);
            this.btnEXEC.TabIndex = 48;
            this.btnEXEC.Text = "اجرا ";
            this.btnEXEC.Tooltip = "اجرای مراحل صورتحساب";
            this.btnEXEC.Click += new System.EventHandler(this.BtnExecClick);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(146, 160);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(98, 28);
            this.btnCancel.TabIndex = 49;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // FormPanel
            // 
            this.FormPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.FormPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.FormPanel.Controls.Add(this.groupBox1);
            this.FormPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormPanel.Location = new System.Drawing.Point(0, 0);
            this.FormPanel.Name = "FormPanel";
            this.FormPanel.Size = new System.Drawing.Size(504, 204);
            this.FormPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.FormPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.FormPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.FormPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.FormPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.FormPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.FormPanel.Style.GradientAngle = 90;
            this.FormPanel.TabIndex = 50;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbt2);
            this.groupBox1.Controls.Add(this.rbt1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.Red;
            this.groupBox1.Location = new System.Drawing.Point(16, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 119);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مراحل صدور صورتحساب خدمات انتقال";
            // 
            // rbt2
            // 
            this.rbt2.AutoSize = true;
            this.rbt2.Location = new System.Drawing.Point(35, 75);
            this.rbt2.Name = "rbt2";
            this.rbt2.Size = new System.Drawing.Size(14, 13);
            this.rbt2.TabIndex = 3;
            this.rbt2.UseVisualStyleBackColor = true;
            // 
            // rbt1
            // 
            this.rbt1.AutoSize = true;
            this.rbt1.Checked = true;
            this.rbt1.Location = new System.Drawing.Point(35, 38);
            this.rbt1.Name = "rbt1";
            this.rbt1.Size = new System.Drawing.Size(14, 13);
            this.rbt1.TabIndex = 2;
            this.rbt1.TabStop = true;
            this.rbt1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(73, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(370, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "2-صدور صورتحساب برای شرکت های انتخاب شده در لیست";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(173, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "1- صدور صورتحساب برای تمامی شرکت ها";
            // 
            // BillExecStepT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 204);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEXEC);
            this.Controls.Add(this.FormPanel);
            this.DoubleBuffered = true;
            this.Name = "BillExecStepT";
            this.Text = "مراحل اجرای صورتحساب";
            this.Load += new System.EventHandler(this.BillExecStepT_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.BillExecStepT_HelpRequested);
            this.Controls.SetChildIndex(this.MyMainPanel, 0);
            this.Controls.SetChildIndex(this.FormPanel, 0);
            this.Controls.SetChildIndex(this.btnEXEC, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            this.FormPanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private DevComponents.DotNetBar.ButtonX btnEXEC;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.PanelEx FormPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbt2;
        private System.Windows.Forms.RadioButton rbt1;
    }
}