﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using System.Threading;


namespace MohanirPouya.Forms.Help
{
    public partial class Form2 : Form
    {
        private int _Hour;
        private int _Minutes;
        private int _Second;
        
        public Form2()
        {
            InitializeComponent();
            m_oWorker = new BackgroundWorker();
            m_oWorker.DoWork += new DoWorkEventHandler(m_oWorker_DoWork);
            m_oWorker.ProgressChanged += new ProgressChangedEventHandler
                    (m_oWorker_ProgressChanged);
            m_oWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler
                    (m_oWorker_RunWorkerCompleted);
            m_oWorker.WorkerReportsProgress = true;
            m_oWorker.WorkerSupportsCancellation = true;

        }

        private void Form2_Load(object sender, EventArgs e)
        {
        }

        private void m_oWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            lblStatus.Text = "Processing......" + progressBar1.Value.ToString() + "%";

        }

        private void m_oWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                lblStatus.Text = "Task Cancelled.";
            }

            else if (e.Error != null)
            {
                lblStatus.Text = "Error while performing background operation.";
            }
            else
            {
                lblStatus.Text = "Task Completed...";
            }
            timer.Stop();
            
            //lblHour.Text = "00:";
            //lblMinutes.Text = "00:";
            //lblSecond.Text = "00";
            _Hour = 0;
            _Minutes = 0;
            _Second = 0;
            btnStartAsyncOperation.Enabled = true;
            btnCancel.Enabled = false;

        }

        private void m_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(100);
        
                m_oWorker.ReportProgress(i);
               
                if (m_oWorker.CancellationPending)
                {
                    e.Cancel = true;
                    m_oWorker.ReportProgress(0);
                    return;
                }
            }

            m_oWorker.ReportProgress(100);

        }

        private void btnStartAsyncOperation_Click(object sender, EventArgs e)
        {
            btnStartAsyncOperation.Enabled = false;
            btnCancel.Enabled = true;
            m_oWorker.RunWorkerAsync();
            _Hour = 0;
            _Minutes = 0;
            _Second = 0;
            timer.Start();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (m_oWorker.IsBusy)
            {
                m_oWorker.CancelAsync();
            }
            //lblHour.Text = "00:";
            //lblMinutes.Text = "00:";
            //lblSecond.Text="00";
            _Hour = 0;
            _Minutes = 0;
            _Second = 0;
            timer.Stop();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            //timer.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //timer.Stop();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (_Second <= 9)
            {
                lblSecond.Text = "0" + _Second.ToString();
                _Second++;
            }
            else
            {
                lblSecond.Text = _Second.ToString();
                _Second++;
            }

            if (_Second > 59)
            {
                _Minutes++;

                if (_Minutes <= 9)
                {
                    lblMinutes.Text = "0" + _Minutes.ToString() + ":";
                }
                else
                {
                    lblMinutes.Text = _Minutes.ToString() + ":";
                }
                _Second = 0;
            }

            if (_Minutes > 59)
            {
                _Hour++;

                if (_Hour <= 9)
                {
                    lblHour.Text = "0" + _Hour.ToString() + ":";
                }
                else
                {
                    lblHour.Text = _Hour.ToString() + ":";
                }
                _Second = 0;
                _Minutes = 0;
            }
           
        }
    }
}
