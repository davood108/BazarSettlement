﻿#region using
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using DataTable = System.Data.DataTable;
using Excel = Microsoft.Office.Interop.Excel;

using NRI.Classes;

#endregion

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class BillSelectD
    {
        #region Make_Excel-Main

        private void MakeExcel()
        {
            btnStopStage.BeginInvoke((MethodInvoker)delegate { btnStopStage.Enabled = true; });
            richTextBoxLog.BeginInvoke((MethodInvoker)delegate() { richTextBoxLog.Visible = true; });
            panelLog.BeginInvoke((MethodInvoker)delegate() { panelLog.Visible = true; });
            try
            {
                _logger.AddToLogAndShow("Creating Excel Files :", LogStyle.InfoStyle);
                _revision = getLastBillRevision(_sDate, _eDate, _subjectChar, _typeId) + 1;

                switch (_ptypeE)
                {

                    case "Sellers":

                        #region Sellers

                        _dateCounter = _sDate;

                        if (_billTypeCode == "D" && String.Compare(_sDate, _eDate, StringComparison.Ordinal) < 0)
                        //roozaneh be soorate baze'i
                        {
                            while (String.Compare(_dateCounter, _eDate, StringComparison.Ordinal) <= 0)
                            {
                                if (_bw.CancellationPending)
                                {
                                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                    return;
                                }

                                _logger.AddToLogAndShow("date: " + _dateCounter, LogStyle.InfoStyle);

                                string query = "DROP TABLE [Sellers].[BillDataU]; "
                                               +
                                               "SELECT * INTO [Sellers].[BillDataU] FROM [Sellers].[BillDataUD] WHERE Date ='" +
                                               _dateCounter + "'";
                                _globalSqlCmd = new SqlCommand(query, _cnn) { CommandTimeout = 1000 };
                                ExecuteGlobalCmd();
                                if (_globalSqlCmdError)
                                {
                                    _bw.CancelAsync();
                                    return;
                                }

                                _revision = getLastBillRevision(_dateCounter, _dateCounter, _subjectChar, _typeId) + 1;
                                MakeSellerExcell();

                                _logger.AddToLogAndShow(
                                    "____________________________________________________________________",
                                    LogStyle.InfoStyle);
                                _dateCounter = NextDay(_dateCounter);
                            }

                            return;
                        }

                        
                        MakeSellerExcell();

                        #endregion


                        break;

                    case "Buyers":

                        #region Make Excel Buyers

                        _logger.AddToLogAndShow("Buyers Bill", LogStyle.InfoStyle);
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        SubBuyersBill();
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        _logger.AddToLogAndShow("Taraz", LogStyle.InfoStyle);
                        SubTaraz();
                        //فراخوانی متد MaliTarazSell&Buy
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        _logger.AddToLogAndShow("MaliTarazSellers", LogStyle.InfoStyle);
                        SubMaliTarazSellers();
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        _logger.AddToLogAndShow("MaliTarazBuyers", LogStyle.InfoStyle);
                        SubMaliTarazBuyers();
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        _logger.AddToLogAndShow("MonthlyReport", LogStyle.InfoStyle);
                        SubMonthlyReport();
                      
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        _logger.AddToLogAndShow("ERate", LogStyle.InfoStyle);
                        SubERate();

                        #endregion
                        break;


                    case "Transfer":

                        _logger.AddToLogAndShow("Bill Company", LogStyle.InfoStyle);
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        SubTransferCompany();
                        _logger.AddToLogAndShow("Bill Total", LogStyle.InfoStyle);
                        if (_bw.CancellationPending)
                        {
                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                            break;
                        }
                        SubTransferTotal();
                        break;

                    case "Fuel":
                        btnStopStage.BeginInvoke((MethodInvoker)delegate { btnStopStage.Enabled = false; });
                        SubFuel();

                        break;

                }
                _logger.AddToLogAndShow("____________________________________________________________________",
                    LogStyle.InfoStyle);
                btnStopStage.BeginInvoke((MethodInvoker)delegate { btnStopStage.Enabled = false; });
            }
            catch (Exception ex)
            {
                _bw.ReportProgress(0);
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("خطا در اجرای مرحله پنجم - تولید فایل های اکسل", strMessage, "Error")
                    .ShowDialog();
            }
        }

        #endregion

        #region Sub-Ex-RepU

        private void SubRepU()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }
            _ds1Rep = new DataSet();
            _table1Rep = new DataTable();

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = ("SELECT * " +
                               " FROM [Sellers].[View_BillDataRepU]" +
                               " ORDER BY PSC,PSN,UnitCode  ")
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1Rep.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region Bill-Excel

            _table1Rep.TableName = "table1";
            _ds1Rep.Tables.Add(_table1Rep);
            _exToexcel = new ExportToExcel();




            if (_btype == "روزانه")
            {

                _exToexcel.toexcel("",
                    " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds1Rep,
                    @"D:\Settlement\Temp\temp-Rep_BuyU.xls",
                    @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                    _dateCounter.Substring(8, 2) + @"\",
                    @"Rep_Buy_U_D_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                    _dateCounter.Substring(8, 2) + ".xls", false, _logger);


            }
            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel("",
                    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                    @"D:\Settlement\Temp\temp-Rep_BuyU.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Rep_Buy_U_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }
            if (_btype == "قطعي")
            {
                _exToexcel.toexcel("",
                    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                    @"D:\Settlement\Temp\temp-Rep_BuyU.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Rep_Buy_U_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }

            #endregion

        }


        #endregion

        #region Sub-Ex-Unit

        private void SubUnit()
        {
            #region PowerPlant-Unit

            _tableCn = new DataTable();

            #region bill-PowerStationCode

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    ("SELECT  Distinct(PowerStationCode),PowerStationName  FROM [Sellers].[BillDataU] --where MarketStateCode like 'N%' ")
            };

            #endregion

            #region Execute Command

            try
            {

                _mySqlCommand1.Connection.Open();

                _tableCn.Load(_mySqlCommand1.ExecuteReader());


                if (_tableCn.Rows.Count > 0)
                {

                }
                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {

                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    return;
                }
                _xcn = _tableCn.Rows[j][0].ToString();
                _powerStationName = _tableCn.Rows[j][1].ToString();
                if (lstPowerStations.List.CheckedItems.Count > 0)
                {
                    #region Intial

                    _ds = new DataSet();
                    _table1 = new DataTable();
                    _table2 = new DataTable();
                    _table3 = new DataTable();
                    _table4 = new DataTable();
                    _table5 = new DataTable();
                    _table6 = new DataTable();

                    #endregion

                    #region bill-Table1

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT * " +
                                       " FROM [Sellers].[BillDataU] " +
                                       " where PowerStationCode='" +
                                       _xcn + "' Order By Date,UnitCode,Hour")//Order By Date,UnitCode,Hour 1395/11/23
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table1.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-table6-PS

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT DISTINCT(PowerStationCode),PowerStationName,UnitType,BillUnitT,UnitCode,[PowerStationCode] as PsCT" +
                             " FROM [Sellers].[BillDataU] " +
                             " where PowerStationCode='" +
                             _xcn + "'" + " order by PowerStationCode,UnitCode")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table6.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Date

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table2.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Sum

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *     FROM [Sellers].[View_BillIDataSumU]" +
                             " where PowerStationCode='" +
                             _xcn + "'" + " order by unitcode,Date")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table3.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-TotalP

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                       " FROM [Sellers].[View_BillIDataTotalPU]" +
                                       " where PowerStationCode='" +
                                       _xcn + "'" +
                                       "order by [PowerStationCode],unitCode")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table4.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-CompanyCodeName

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  Distinct(CompanyCode),CompanyName FROM [Sellers].[BillDataU] " +
                             " where PowerStationCode='" +
                             _xcn + "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        SqlDataReader mySqlDataReader =
                            _mySqlCommand1.ExecuteReader();
                        if (mySqlDataReader.HasRows)
                            while (mySqlDataReader.Read())
                                _companyCode = mySqlDataReader[0].ToString();

                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Fuel
                    //1395/11/20

                    DataTable tableFuel = new DataTable();
                    _mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Fuel].[View_BillIDataEms_OutFuelBill]" +
                             " WHERE  PowerPlantCode='" + _xcn + "'")
                    };


                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        tableFuel.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }
                    #endregion

                    #endregion


                    #region bill-TotalPP

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *    FROM [Sellers].[View_BillIDataTotalPPU]" +
                             " where PowerStationCode='" +
                             _xcn + "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table5.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Bill-Excel

                    _table1.TableName = "table1";
                    _table2.TableName = "Date";
                    _table3.TableName = "Sum";
                    _table4.TableName = "TotalP";
                    _table5.TableName = "Total";
                    _table6.TableName = "PS";
                    tableFuel.TableName = "Fuel";

                    _ds.Tables.Add(_table1);
                    _ds.Tables.Add(_table2);
                    _ds.Tables.Add(_table3);
                    _ds.Tables.Add(_table4);
                    _ds.Tables.Add(_table5);
                    _ds.Tables.Add(_table6);
                    _ds.Tables.Add(tableFuel);
                    _exToexcel = new ExportToExcel();




                    if (_btype == "روزانه")
                    {

                        _exToexcel.toexcel(
                            _powerStationName, " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                            @"D:\Settlement\Temp\temp-BillSellU.xls",
                            @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                            _dateCounter.Substring(8, 2) + @"\Unit_Bill\Reghabati\",
                            @"BillS_U_D_" + " (" + _xcn + ")_" + _dateCounter.Substring(2, 2) +
                            _dateCounter.Substring(5, 2) +
                            _dateCounter.Substring(8, 2) + ".xls", false, _logger);

                    }

                    if (_btype == "ماهيانه")
                    {
                        _exToexcel.toexcel(
                            _powerStationName,
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSellU.xls",
                            @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\Unit_Bill\Reghabati\",
                            @"BillS_U_" + " (" + _xcn + ")_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls",
                            false, _logger);
                    }
                    if (_btype == "قطعي")
                    {
                        _exToexcel.toexcel(
                            _powerStationName,
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSellU.xls",
                            @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\Unit_Bill\Reghabati\",
                            @"BillS_U_C_" + " (" + _xcn + ")_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            ".xls", false, _logger);
                    }

                    #endregion
                }

            }

            #endregion

        }

        #endregion

        #region Sub-Ex-Buyersbill

        private void SubBuyersBill()
        {
            #region bill

            #region Company

            _tableCn = new DataTable();
            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT  DISTINCT CompanyCode,CompanyName FROM  [Buyers].[BillData]"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _tableCn.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
                //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion

            _logger.AddToLogAndShow(string.Format("Number of companies: {0}", _tableCn.Rows.Count), LogStyle.InfoStyle);

            #endregion

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {
                _xcn = _tableCn.Rows[j][0].ToString();
                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    return;
                }

                if (lstSellerList.List.CheckedItems.Count > 0)
                {
                    _ds = new DataSet();
                    _table1 = new DataTable();
                    _table2 = new DataTable();
                    _table3 = new DataTable();
                    _table4 = new DataTable();


                    #region bill-Table1

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            "SELECT  *     FROM  [Buyers].[BillData]" +
                            " where CompanyCode='" + _xcn +"'"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table1.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Date

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            "SELECT   DISTINCT (Date)  Date   FROM  [Buyers].[BillData] order by Date"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table2.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Sum

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = "SELECT  *   FROM  [Buyers].[View_BillDataSum]" +
                                      " where CompanyCode='" + _xcn +"'"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table3.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Total

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = "SELECT  *   FROM  [Buyers].[View_BillDataTotal]" +
                                      " where CompanyCode='" + _xcn +"'"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table4.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion


                    #region Fuel
                    //1395/11/20

                    DataTable tableFuel = new DataTable();
                    _mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Fuel].[Tbl_Taraz_Fuel]" +
                             " WHERE  CompanyCode='" + _xcn + "'")
                    };


                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        tableFuel.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }
                    #endregion

                    #endregion

                    _table1.TableName = "table1";
                    _table2.TableName = "Date";
                    _table3.TableName = "Sum";
                    _table4.TableName = "Total";
                    tableFuel.TableName = "Fuel";
                    _ds.Tables.Add(_table1);
                    _ds.Tables.Add(_table2);
                    _ds.Tables.Add(_table3);
                    _ds.Tables.Add(_table4);
                    _ds.Tables.Add(tableFuel);
                    _exToexcel = new ExportToExcel();



                    //--------------------------
                    if (_btype == "ماهيانه")
                    {
                        string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}.xls", _subjectChar, _billTypeCode,
                             _year + _month, _revision, _xcn);
                        _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                        _exToexcel.toexcel(
                            _tableCn.Rows[j][0].ToString(), " از تاریخ   " + _sDate + "  تا  " + _eDate,
                            _ds, @"D:\Settlement\Temp\temp-BillBuy1.xls",
                            @"D:\Settlement\Out\Monthly\" + _year + _month + @"\Buyer\",
                            fileName, false, _logger);


                    }

                    if (_btype == "قطعي")
                    {
                        string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}.xls", _subjectChar, _billTypeCode,
                                _year + _month, _revision, _xcn);
                        _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                        _exToexcel.toexcel(
                            _tableCn.Rows[j][0].ToString(), " از تاریخ   " + _sDate + "  تا  " + _eDate,
                            _ds, @"D:\Settlement\Temp\temp-BillBuy1.xls",
                            @"D:\Settlement\Out\Definitive\" + _year + _month + @"\Buyer\", fileName, false, _logger);

                    }

                    //  --------------

                    //i = i + 1;
                }

            }
            

            #endregion
            _logger.AddToLogAndShow("__________________________________", LogStyle.InfoStyle);
        }

        #endregion




        private void MakeSellerExcell()
        {

           
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }
            _logger.AddToLogAndShow("Rep Unit PowerPlant", LogStyle.InfoStyle);
            SubRepUP();

            //transfer from buyer 1396/01/19
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }
            _logger.AddToLogAndShow("MaliReport", LogStyle.InfoStyle);
            SubMaliReport();
            //
            
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }
            _logger.AddToLogAndShow("SubCompany", LogStyle.InfoStyle);
            SubComPany();

            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }
            _logger.AddToLogAndShow("SubUnitPowerPlant", LogStyle.InfoStyle);
            SubUnitPowerPlant();

        }

        private void PrintYearlySellerExcels()
        {
            _excelPath = @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                         _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                         _eDate.Substring(8, 2) + @"\Seller\Company";
            var filesList = Directory.GetFiles(_excelPath).ToList();

            //_excelPath = @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
            //            _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
            //            _eDate.Substring(8, 2) + @"\ComPany\Tazmini";
            //filesList.AddRange(Directory.GetFiles(_excelPath).ToList());


            foreach (String fileN in filesList)
            {
                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    break;
                }

                _logger.AddToLogAndShow("Printing file =" + fileN, LogStyle.InfoStyle);

                var excelApp = new Excel.Application { Visible = false };
                var wk = excelApp.Workbooks.Open(fileN, 0, Type.Missing, 5, Type.Missing, Type.Missing, true,
                    Excel.XlPlatform.xlWindows, "\\t", false, false, 0, true, true, Type.Missing);

                int sheetIndex = 0;

                foreach (Excel.Worksheet sheet in wk.Sheets)
                {

                    sheetIndex = sheetIndex + 1;
                    if (sheet.Name == "صورتحساب")
                    {
                        break;
                    }

                }
                var wt = (Excel.Worksheet)wk.Worksheets[sheetIndex];

                wt.PrintOut(Type.Missing, Type.Missing, _printingForm.NumberOfCopy, Type.Missing,
                    _printingForm.PrinterName, Type.Missing, Type.Missing, Type.Missing);

                wk.Close(false, null, null);
                excelApp.Quit();

                releaseObject(wk);
                releaseObject(wt);
                releaseObject(excelApp);
            }
        }

        private void PrintYearlyBuyerExcels()
        {
            _excelPath = @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                         _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                         _eDate.Substring(8, 2) + @"\Buyer";
            var filesList = Directory.GetFiles(_excelPath).ToList();

            foreach (String fileN in filesList)
            {
                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    break;
                }

                _logger.AddToLogAndShow("Printing file =" + fileN, LogStyle.InfoStyle);

                var excelApp = new Excel.Application { Visible = false };
                var wk = excelApp.Workbooks.Open(fileN, 0, Type.Missing, 5, Type.Missing, Type.Missing, true,
                    Excel.XlPlatform.xlWindows, "\\t", false, false, 0, true, true, Type.Missing);

                int sheetIndex = 0;

                foreach (Excel.Worksheet sheet in wk.Sheets)
                {

                    sheetIndex = sheetIndex + 1;
                    if (sheet.Name == "صورتحساب")
                    {
                        break;
                    }

                }
                var wt = (Excel.Worksheet)wk.Worksheets[sheetIndex];

                wt.PrintOut(Type.Missing, Type.Missing, _printingForm.NumberOfCopy, Type.Missing,
                    _printingForm.PrinterName, Type.Missing, Type.Missing, Type.Missing);

                wk.Close(false, null, null);
                excelApp.Quit();

                releaseObject(wk);
                releaseObject(wt);
                releaseObject(excelApp);
            }
        }

        private void PrintYearlyTransferExcels()
        {
            _excelPath = @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                         _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                         _eDate.Substring(8, 2) + @"\Transfer";
            var filesList = Directory.GetFiles(_excelPath).ToList();

            foreach (String fileN in filesList)
            {
                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    break;
                }

                _logger.AddToLogAndShow("Printing file =" + fileN, LogStyle.InfoStyle);

                var excelApp = new Excel.Application { Visible = false };
                var wk = excelApp.Workbooks.Open(fileN, 0, Type.Missing, 5, Type.Missing, Type.Missing, true,
                    Excel.XlPlatform.xlWindows, "\\t", false, false, 0, true, true, Type.Missing);

                int sheetIndex = 0;

                foreach (Excel.Worksheet sheet in wk.Sheets)
                {

                    sheetIndex = sheetIndex + 1;
                    if (sheet.Name == "صورتحساب")
                    {
                        break;
                    }

                }
                var wt = (Excel.Worksheet)wk.Worksheets[sheetIndex];

                wt.PrintOut(Type.Missing, Type.Missing, _printingForm.NumberOfCopy, Type.Missing,
                    _printingForm.PrinterName, Type.Missing, Type.Missing, Type.Missing);

                wk.Close(false, null, null);
                excelApp.Quit();

                releaseObject(wk);
                releaseObject(wt);
                releaseObject(excelApp);
            }
        }

        #region Sub-Transfer-Company

        private void SubTransferCompany()
        {
            #region Make Excel Transfer New

            _tableCn = new DataTable();

            #region bill-CompanyName

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT Distinct(CompanyCode),CompanyName from Transfer.BillData order by CompanyCode"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();

                _tableCn.Load(_mySqlCommand1.ExecuteReader());

                if (_tableCn.Rows.Count == 0)
                    return;
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
                //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion

            #endregion
            _logger.AddToLogAndShow(string.Format("Number of companies: {0}", _tableCn.Rows.Count), LogStyle.InfoStyle);

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {
                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    break;
                }
                _xcn = _tableCn.Rows[j][0].ToString();
                _companyName1 = _tableCn.Rows[j][1].ToString();

                #region company

                #region Intial

                _ds = new DataSet();
                _table1 = new DataTable();
                _table2 = new DataTable();
                _table3 = new DataTable();
                _table4 = new DataTable();
                _table5 = new DataTable();
                _table6 = new DataTable();
                _table7 = new DataTable();
                _table8 = new DataTable();

                #endregion

                #region bill-Table1

                #region Prepare Connection & Command

                _mySqlConnection = new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText = ("SELECT *  FROM [Transfer].[BillData] " +
                                   " where CompanyCode='" +
                                   _xcn + "'")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table1.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion

                #region bill-table6-PS

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT distinct EquipmentType,VoltageLevel" +
                         " FROM [Transfer].[BillData] " +
                         " where CompanyCode='" +
                         _xcn + "'")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table6.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion

                #region bill-Sum

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *  FROM [Transfer].[View_BillIDataSum]" +
                         " where CompanyCode='" +
                         _xcn + "'" +
                         " Order by EquipmentType,VoltageLevel,EquipmentCode,Date")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table3.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion

                #region bill-TotalM

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *  FROM [Transfer].[View_BillIDataTotal_M]" +
                         " where CompanyCode='" +
                         _xcn + "'" +
                         " Order by EquipmentType,VoltageLevel")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table5.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion

                #region bill-Total

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT * FROM [Transfer].[View_BillIDataTotal]" +
                         " where CompanyCode='" +
                         _xcn + "'")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table7.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion

                #region bill-TotalBC

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *  FROM [Transfer].[View_BillIDataTotal_BC]" +
                         " where CompanyCode='" +
                         _xcn + "'")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table8.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion

                #region Bill-Excel

                _table1.TableName = "table1";

                _table3.TableName = "Sum";

                _table5.TableName = "TotalM";
                _table6.TableName = "PS";
                _table7.TableName = "Total";
                _table8.TableName = "TotalBC";

                _ds.Tables.Add(_table1);

                _ds.Tables.Add(_table3);

                _ds.Tables.Add(_table5);
                _ds.Tables.Add(_table6);
                _ds.Tables.Add(_table7);
                _ds.Tables.Add(_table8);

                #region Try

                try
                {
                    if (_btype == "ماهيانه")
                    {

                        string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}.xls", _subjectChar, _billTypeCode,
                           _year + _month, _revision, _xcn);
                        _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                        _exToexcel = new ExportToExcel();
                        _exToexcel.toexcel(
                            _companyName1 + "( " + " از تاریخ   " + _sDate + "  تا  " + _eDate +
                            " )", "", _ds, @"D:\Settlement\Temp\temp-Billtransfer.xls",
                            @"D:\Settlement\Out\Monthly\" + _year + _month + @"\Transfer\", fileName, false, _logger);
                    }


                    if (_btype == "قطعي")
                    {
                        string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}.xls", _subjectChar, _billTypeCode,
                            _year + _month, _revision, _xcn);
                        _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                        _exToexcel = new ExportToExcel();
                        _exToexcel.toexcel(
                            _companyName1 + "( " + " از تاریخ   " + _sDate + "  تا  " + _eDate +
                            " )", "", _ds, @"D:\Settlement\Temp\temp-Billtransfer.xls",
                            @"D:\Settlement\Out\Definitive\" + _year + _month + @"\Transfer\", fileName, false, _logger);
                    }
                }
                #endregion

                #region Catch

                catch (Exception ee)
                {
                    _logger.AddToLogAndShow(ee);
                    const String errorMessage =
                        "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                        "موارد زیر را بررسی نمایید:\n" +
                        "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                    MessageBox.Show(errorMessage, @"خطا!");
                    MessageBox.Show(ee.Message);
                    return;
                }

                #endregion

                #endregion

                #endregion
            }

            #endregion
            _logger.AddToLogAndShow("__________________________________", LogStyle.InfoStyle);
        }

        #endregion

        #region Sub-Transfer-Total

        private void SubTransferTotal()
        {
            #region Make Excel Transfer TotalReport

            _logger.AddToLogAndShow("Transfer Total Report", LogStyle.InfoStyle);

            #region company

            #region Intial

            _ds = new DataSet();
            _table1 = new DataTable();
            _table2 = new DataTable();

            #endregion

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = ("SELECT * FROM [Transfer].[View_BillIDataTotal_BC] order by CompanyCode"
                    )

            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _table1.Load(_mySqlCommand1.ExecuteReader());
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
                //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion

            #endregion

            #region bill-Table2

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT * FROM [Transfer].[View_BillIData_Total_Sum] "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _table2.Load(_mySqlCommand1.ExecuteReader());
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
                //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region Bill-Excel

            _table1.TableName = "tablep";
            _table2.TableName = "tablep2";
            _ds.Tables.Add(_table1);
            _ds.Tables.Add(_table2);

            #region Try

            try
            {
                if (_btype == "ماهيانه")
                {
                    _exToexcel = new ExportToExcel();
                    _exToexcel.toexcel(
                        " " + " از تاریخ   " + _sDate + "  تا  " + _eDate + " ",
                        "", _ds,
                        @"D:\Settlement\Temp\temp-Transfer-Report.xls",
                        @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                        @"\Transfer\",

                        @"BillT_M_SumReport" + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                        ".xls", false, _logger);
                }

                if (_btype == "قطعي")
                {
                    _exToexcel = new ExportToExcel();
                    _exToexcel.toexcel(
                        " " + " از تاریخ   " + _sDate + "  تا  " + _eDate + " ",
                        "", _ds,
                        @"D:\Settlement\Temp\temp-Transfer-Report.xls",
                        @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) +
                        _sDate.Substring(5, 2) +
                        @"\Transfer\",

                        @"BillT_C_SumReport" + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                        ".xls", false, _logger);
                }
            }

            #endregion

            #region Catch

            catch (Exception ee)
            {
                _logger.AddToLogAndShow(ee);
                const String errorMessage =
                    "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                    "موارد زیر را بررسی نمایید:\n" +
                    "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                MessageBox.Show(errorMessage, @"خطا!");
                MessageBox.Show(ee.Message);
                return;
            }

            #endregion

            #endregion

            #endregion

            #endregion
        }

        #endregion

        #region Sub-Ex-CompanyTAZ

        private void SubCompanyTaz()
        {
            if (lstPowerStations.List.CheckedItems.Count == 0)
            {

                #region CompanyTAZ

                _tableCn = new DataTable();

                #region bill-CompanyCode

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(CompanyCode),CompanyName,AccountingCode FROM [Sellers].[BillDataU] where MarketStateCode like 'Z%' order by AccountingCode,CompanyCode ")
                };

                #endregion

                #region Execute Command

                try
                {

                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());


                    if (_tableCn.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        return;
                    }

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }

                    _xcn = _tableCn.Rows[j][0].ToString();
                    _companyName1 = _tableCn.Rows[j][1].ToString();
                    if (lstSellerList.List.CheckedItems.Count > 0)
                    {

                        #region Intial

                        _ds = new DataSet();
                        _table1 = new DataTable();
                        _table2 = new DataTable();
                        _table3 = new DataTable();
                        _table4 = new DataTable();
                        _table5 = new DataTable();
                        _table6 = new DataTable();
                        _table7 = new DataTable();
                        _table8 = new DataTable();
                        new DataTable();

                        #endregion

                        #region bill-Table1

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText = ("SELECT * " +
                                           " FROM [Sellers].[View_BillIDataU_PS] " +
                                           " where MarketStateCode like 'Z%' and CompanyCode='" +
                                           _xcn + "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table1.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-table6-PS

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT DISTINCT(PowerStationCode),PowerStationName,'-' as UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                                 " FROM [Sellers].[View_BillIDataU_PS] " +
                                 " where MarketStateCode like 'Z%' and CompanyCode='" + _xcn +
                                 "'" +
                                 "Order by PowerStationCode,PowerStationName")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table6.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Date

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table2.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Sum

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *  FROM [Sellers].[View_BillIDataSum]" +
                                 "where MarketStateCode like 'Z%' and CompanyCode='" + _xcn +
                                 "'" +
                                 "Order by PowerStationCode,PowerStationName,Date")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table3.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Total

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *    FROM [Sellers].[View_BillIDataTotal]" +
                                 " where MarketStateCode like 'Z%' and CompanyCode='" + _xcn +
                                 "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table5.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion


                        #region Bill-Excel

                        _table1.TableName = "table1";
                        _table2.TableName = "Date";
                        _table3.TableName = "Sum";

                        _table5.TableName = "Total";
                        _table6.TableName = "PS";




                        _ds.Tables.Add(_table1);
                        _ds.Tables.Add(_table2);
                        _ds.Tables.Add(_table3);

                        _ds.Tables.Add(_table5);
                        _ds.Tables.Add(_table6);




                        _exToexcel = new ExportToExcel();


                        if (_btype == "روزانه")
                        {

                            _exToexcel.toexcel(
                                _companyName1,
                                " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) +
                                _dateCounter.Substring(5, 2) +
                                _dateCounter.Substring(8, 2) + @"\PowerPlant_Bill\ComPany\Tazmini\",
                                @"BillS_D_" + _xcn + "_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                                "_Z.xls",
                                false, _logger);

                        }
                        if (_btype == "ماهيانه")
                        {
                            _exToexcel.toexcel(
                                _companyName1,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\ComPany\Tazmini\",
                                @"BillS_M_" + _xcn + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + "_Z.xls",
                                false, _logger);
                        }
                        if (_btype == "قطعي")
                        {
                            _exToexcel.toexcel(
                                _companyName1,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\ComPany\Tazmini\",
                                @"BillS_C_" + _xcn + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls",
                                false, _logger);
                        }

                        #endregion


                    }

                }

                #endregion
            }

            else
            {


                #region CompanyTAZ

                _tableCn = new DataTable();

                #region bill-CompanyCode

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(CompanyCode),CompanyName,AccountingCode FROM [Sellers].[BillDataU] where MarketStateCode like 'Z%' order by AccountingCode,CompanyCode ")
                };

                #endregion

                #region Execute Command

                try
                {

                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());


                    if (_tableCn.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        return;
                    }

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }

                    _xcn = _tableCn.Rows[j][0].ToString();
                    _companyName1 = _tableCn.Rows[j][1].ToString();
                    if (lstSellerList.List.CheckedItems.Count > 0)
                    {

                        #region Intial

                        _ds = new DataSet();
                        _table1 = new DataTable();
                        _table2 = new DataTable();
                        _table3 = new DataTable();
                        _table4 = new DataTable();
                        _table5 = new DataTable();
                        _table6 = new DataTable();
                        _table7 = new DataTable();
                        _table8 = new DataTable();
                        new DataTable();

                        #endregion

                        #region bill-Table1

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText = ("SELECT * " +
                                           " FROM [Sellers].[View_BillIDataU_PS] " +
                                           " where MarketStateCode like 'Z%' and CompanyCode='" +
                                           _xcn + "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table1.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-table6-PS

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT DISTINCT(PowerStationCode),PowerStationName,'-' as UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                                 " FROM [Sellers].[View_BillIDataU_PS] " +
                                 " where MarketStateCode like 'Z%' and CompanyCode='" + _xcn +
                                 "'" +
                                 "Order by PowerStationCode,PowerStationName")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table6.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Date

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table2.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Sum

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *  FROM [Sellers].[View_BillIDataSum]" +
                                 "where MarketStateCode like 'Z%' and CompanyCode='" + _xcn +
                                 "'" +
                                 "Order by PowerStationCode,PowerStationName,Date")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table3.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Total

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *    FROM [Sellers].[View_BillIDataTotal]" +
                                 " where MarketStateCode like 'Z%' and CompanyCode='" + _xcn +
                                 "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table5.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region Bill-Excel

                        _table1.TableName = "table1";
                        _table2.TableName = "Date";
                        _table3.TableName = "Sum";

                        _table5.TableName = "Total";
                        _table6.TableName = "PS";




                        _ds.Tables.Add(_table1);
                        _ds.Tables.Add(_table2);
                        _ds.Tables.Add(_table3);

                        _ds.Tables.Add(_table5);
                        _ds.Tables.Add(_table6);




                        _exToexcel = new ExportToExcel();


                        if (_btype == "روزانه")
                        {

                            _exToexcel.toexcel(
                                _companyName1,
                                " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) +
                                _dateCounter.Substring(5, 2) +
                                _dateCounter.Substring(8, 2) + @"\PowerPlant_Bill\ComPany\Tazmini\",
                                @"BillS_D_" + _xcn + "_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                                "_Z.xls",
                                false, _logger);

                        }
                        if (_btype == "ماهيانه")
                        {
                            _exToexcel.toexcel(
                                _companyName1,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\ComPany\Tazmini\",
                                @"BillS_M_" + _xcn + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + "_Z.xls",
                                false, _logger);
                        }
                        if (_btype == "قطعي")
                        {
                            _exToexcel.toexcel(
                                _companyName1,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\ComPany\Tazmini\",
                                @"BillS_C_" + _xcn + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls",
                                false, _logger);
                        }

                        #endregion

                    }

                }

                #endregion


                #region PowerPlant

                _tableCn = new DataTable();

                #region bill-PowerStationCode

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(PowerStationCode),PowerStationName FROM  [Sellers].[BillDataU] where MarketStateCode like 'Z%'  ")
                };

                #endregion

                #region Execute Command

                try
                {

                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());


                    if (_tableCn.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        return;
                    }

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }
                    _xcn = _tableCn.Rows[j][0].ToString();
                    _powerStationName = _tableCn.Rows[j][1].ToString();

                    if (lstSellerList.List.CheckedItems.Count > 0)
                    {

                        #region Intial

                        _ds = new DataSet();
                        _table1 = new DataTable();
                        _table2 = new DataTable();
                        _table3 = new DataTable();
                        _table4 = new DataTable();
                        _table5 = new DataTable();
                        _table6 = new DataTable();
                        _table7 = new DataTable();
                        _table8 = new DataTable();
                        new DataTable();

                        #endregion

                        #region bill-Table1

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText = ("SELECT * " +
                                           " FROM [Sellers].[View_BillIDataU_PS] " +
                                           " where PowerStationCode='" +
                                           _xcn + "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table1.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-table6-PS

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT DISTINCT(PowerStationCode),PowerStationName,'-' as UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                                 " FROM [Sellers].[BillDataU] " +
                                 " where PowerStationCode='" + _xcn +
                                 "'" +
                                 "Order by PowerStationCode,PowerStationName")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table6.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Date

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table2.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Sum

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *  FROM [Sellers].[View_BillIDataSum]" +
                                 "where PowerStationCode='" + _xcn +
                                 "'" +
                                 "Order by PowerStationCode,PowerStationName,Date")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table3.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Total

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *    FROM [Sellers].[View_BillIDataTotalP]" +
                                 " where PowerStationCode='" + _xcn +
                                 "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table5.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region Bill-Excel


                        _table1.TableName = "table1";
                        _table2.TableName = "Date";
                        _table3.TableName = "Sum";

                        _table5.TableName = "Total";
                        _table6.TableName = "PS";




                        _ds.Tables.Add(_table1);
                        _ds.Tables.Add(_table2);
                        _ds.Tables.Add(_table3);

                        _ds.Tables.Add(_table5);
                        _ds.Tables.Add(_table6);


                        _exToexcel = new ExportToExcel();



                        if (_btype == "روزانه")
                        {

                            _exToexcel.toexcel(
                                _companyName1,
                                " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) +
                                _dateCounter.Substring(5, 2) +
                                _dateCounter.Substring(8, 2) + @"\PowerPlant_Bill\ComPany\Tazmini\",
                                @"BillS_D_" + _xcn + "_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                                "_Z.xls",
                                false, _logger);


                        }
                        if (_btype == "ماهيانه")
                        {
                            _exToexcel.toexcel(_powerStationName,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\PowerPlant\Tazmini\",
                                @"BillS_M_" + " (" + _xcn + ")_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                ".xls", false, _logger);
                        }
                        if (_btype == "قطعي")
                        {
                            _exToexcel.toexcel(_powerStationName,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-EnsuredBill_N.xls",
                                @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\PowerPlant\Tazmini\",
                                @"BillS_C_" + " (" + _xcn + ")_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                ".xls", false, _logger);
                        }
                    }



                }

            }

                        #endregion



                #endregion
        }

        #endregion

        #region Sub-Ex-ComPanyPowerPlant

        private void SubComPanyPowerPlant()
        {
            if (lstPowerStations.List.CheckedItems.Count == 0)
            {
                #region Company

                _tableCn = new DataTable();

                #region bill-CompanyCode

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(CompanyCode),CompanyName,companyCodeE,CompanyTypeName,AccountingCode FROM [Sellers].[BillDataU] " +
                         " order by AccountingCode,CompanyTypeName,CompanyCode") //where  MN ='N' 1395/11/24
                };

                #endregion

                #region Execute Command

                try
                {

                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());


                    if (_tableCn.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        return;
                    }

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }

                    _xcn = _tableCn.Rows[j][0].ToString();
                    _companyName1 = _tableCn.Rows[j][1].ToString();
                    _xcnE1 = _tableCn.Rows[j][2].ToString();

                    if (_tableCn.Rows[j][0].ToString() != _tableCn.Rows[j][2].ToString())
                    {
                        _xcnE = _tableCn.Rows[j][0] + "_" + (string)_tableCn.Rows[j][2];
                    }
                    else
                    {
                        _xcnE = _xcn;
                    }

                    #region company

                    #region Intial

                    _ds = new DataSet();

                    _table1 = new DataTable();
                    _table2 = new DataTable();
                    _table3 = new DataTable();
                    _table4 = new DataTable();
                    _table5 = new DataTable();
                    _table6 = new DataTable();

                    #endregion

                    #region bill-Table1

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT * " +
                                       " FROM [Sellers].[View_BillIDataU_PS] " +
                                       " where MarketStateCode ='N' and companyCodeE='" + _xcnE1 + "' and CompanyCode='" +
                                       _xcn + "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table1.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-table6-PS

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT DISTINCT(PowerStationCode),PowerStationName,'-' as UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                             " FROM [Sellers].[View_BillIDataU_PS] " +
                             " where MarketStateCode ='N' and companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn +
                             "'" +
                             "Order by PowerStationCode,PowerStationName,BillUnitT")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table6.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Date

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table2.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Sum

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *  FROM [Sellers].[View_BillIDataSum]" +
                             "where MarketStateCode ='N' and companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn +
                             "'" +
                             "Order by PowerStationCode,PowerStationName,BillUnitT,Date")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table3.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-TotalP

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                       " FROM [Sellers].[View_BillIDataTotalP]" +
                                       " where MarketStateCode ='N' and companyCodeE='" + _xcnE1 +
                                       "' and  CompanyCode='" +
                                       _xcn + "'" +
                                       "order by [PowerStationCode]")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table4.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Total

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *    FROM [Sellers].[View_BillIDataTotal]" +
                             " where MarketStateCode ='N' and companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn +
                             "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table5.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Fuel 
                    //1395/11/20

                    DataTable tableFuel = new DataTable();
                    _mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Fuel].[View_BillIDataCompany]" +
                             " WHERE CompanyCode='" + _xcn + "'")
                    };


                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        tableFuel.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }
                    #endregion

                    #endregion

                    #region Bill-Excel

                    _table1.TableName = "table1";
                    _table2.TableName = "Date";
                    _table3.TableName = "Sum";
                    _table4.TableName = "TotalP";
                    _table5.TableName = "Total";
                    _table6.TableName = "PS";
                    tableFuel.TableName = "Fuel";//1395/11/20

                    _ds.Tables.Add(_table1);
                    _ds.Tables.Add(_table2);
                    _ds.Tables.Add(_table3);
                    _ds.Tables.Add(_table4);
                    _ds.Tables.Add(_table5);
                    _ds.Tables.Add(tableFuel);
                   // _ds.Tables.Add(_table6);
                    _exToexcel = new ExportToExcel();



                    if (_btype == "روزانه")
                    {

                        _exToexcel.toexcel(
                            _companyName1,
                            " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                            @"D:\Settlement\Temp\temp-BillSell.xls",
                            @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                            _dateCounter.Substring(8, 2) + @"\ComPany\Reghabati\",
                            @"BillS_D_" + _xcnE + "_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                            _dateCounter.Substring(8, 2) + ".xls", false, _logger);

                    }

                    if (_btype == "ماهيانه")
                    {



                        _exToexcel.toexcel(_companyName1, "",
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSell.xls",
                            @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\PowerPlant_Bill\ComPany\Reghabati\",
                            @"BillS_M_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false,
                            _logger);
                    }
                    if (_btype == "قطعي")
                    {
                        _exToexcel.toexcel(_companyName1, "",
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSell.xls",
                            @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\PowerPlant_Bill\ComPany\Reghabati\",
                            @"BillS_C_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false,
                            _logger);
                    }

                    #endregion

                    #endregion

                }

                #endregion
            }
            else
            {
                

                #region Company

                _tableCn = new DataTable();

                #region bill-CompanyCode

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(CompanyCode),CompanyName,companyCodeE,CompanyTypeName,AccountingCode FROM [Sellers].[BillDataU] " +
                         " order by AccountingCode,CompanyTypeName,CompanyCode") //where  MN ='N' 1395/11/24
                };

                #endregion

                #region Execute Command

                try
                {

                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());


                    if (_tableCn.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        return;
                    }

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {

                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }

                    _xcn = _tableCn.Rows[j][0].ToString();
                    _companyName1 = _tableCn.Rows[j][1].ToString();
                    _xcnE1 = _tableCn.Rows[j][2].ToString();

                    if (_tableCn.Rows[j][0].ToString() != _tableCn.Rows[j][2].ToString())
                    {
                        _xcnE = _tableCn.Rows[j][0] + "_" + (string)_tableCn.Rows[j][2];
                    }
                    else
                    {
                        _xcnE = _xcn;
                    }

                    #region company

                    #region Intial

                    _ds = new DataSet();

                    _table1 = new DataTable();
                    _table2 = new DataTable();
                    _table3 = new DataTable();
                    _table4 = new DataTable();
                    _table5 = new DataTable();
                    _table6 = new DataTable();

                    #endregion

                    #region bill-Table1

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT * " +
                                       " FROM [Sellers].[View_BillIDataU_PS] " +
                                       " where companyCodeE='" + _xcnE1 + "' and CompanyCode='" +
                                       _xcn + "'")//delete:  MarketStateCode ='N' 1395/11/24
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table1.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-table6-PS

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT DISTINCT(PowerStationCode),PowerStationName,'-' as UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                             " FROM [Sellers].[View_BillIDataU_PS] " +
                             " where companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn +"'" +
                             " Order by PowerStationCode,PowerStationName,BillUnitT")//delete:  MarketStateCode ='N' 1395/11/24
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table6.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Date

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table2.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Sum

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *  FROM [Sellers].[View_BillIDataSum]" +
                             " where companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn +"'" +
                             " Order by PowerStationCode,PowerStationName,BillUnitT,Date")//delete:  MarketStateCode ='N' 1395/11/24
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table3.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-TotalP

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                       " FROM [Sellers].[View_BillIDataTotalP]" +
                                       " where companyCodeE='" + _xcnE1 +
                                       "' and  CompanyCode='" +_xcn + "'" +
                                       " order by [PowerStationCode]") //delete:  MarketStateCode ='N' 1395/11/24
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table4.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-Total

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Sellers].[View_BillIDataTotal]" +
                             " where companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn +
                             "'")//delete:  MarketStateCode ='N' 1395/11/24
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        _table5.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion


                    #region Fuel
                    //1395/11/20

                    DataTable tableFuel = new DataTable();
                    _mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Fuel].[View_BillIDataCompany]" +
                             " WHERE CompanyCode='" + _xcn + "'")
                    };


                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        tableFuel.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }
                    #endregion

                    #endregion


                    #region Bill-Excel

                    _table1.TableName = "table1";
                    _table2.TableName = "Date";
                    _table3.TableName = "Sum";
                    _table4.TableName = "TotalP";
                    _table5.TableName = "Total";
                    _table6.TableName = "PS";
                    tableFuel.TableName = "Fuel";
                    _ds.Tables.Add(_table1);
                    _ds.Tables.Add(_table2);
                    _ds.Tables.Add(_table3);
                    _ds.Tables.Add(_table4);
                    _ds.Tables.Add(_table5);
                    _ds.Tables.Add(_table6);
                    _ds.Tables.Add(tableFuel);
                    _exToexcel = new ExportToExcel();



                    if (_btype == "روزانه")
                    {

                        _exToexcel.toexcel(
                            _companyName1,
                            " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                            @"D:\Settlement\Temp\temp-BillSell.xls",
                            @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                            _dateCounter.Substring(8, 2) + @"\ComPany\Reghabati\",
                            @"BillS_D_" + _xcnE + "_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                            _dateCounter.Substring(8, 2) + ".xls", false, _logger);

                    }

                    if (_btype == "ماهيانه")
                    {



                        _exToexcel.toexcel(_companyName1, "",
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSell.xls",
                            @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\PowerPlant_Bill\ComPany\Reghabati\",
                            @"BillS_M_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false,
                            _logger);
                    }
                    if (_btype == "قطعي")
                    {
                        _exToexcel.toexcel(_companyName1, "",
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSell.xls",
                            @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\PowerPlant_Bill\ComPany\Reghabati\",
                            @"BillS_C_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false,
                            _logger);
                    }

                    #endregion

                    #endregion


                }

                #endregion

                #region PowerPlant

                _tableCn = new DataTable();

                #region bill-PowerStationCode

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(PowerStationCode),PowerStationName  FROM [Sellers].[BillDataU] "
                         )//delete " where MarketStateCode  like 'N%' " 1395/11/25
                };

                #endregion

                #region Execute Command

                try
                {

                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());


                    if (_tableCn.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        return;
                    }

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }
                    _xcn = _tableCn.Rows[j][0].ToString();
                    _powerStationName = _tableCn.Rows[j][1].ToString();
                    if (lstPowerStations.List.CheckedItems.Count > 0)
                    {
                        #region Intial

                        _ds = new DataSet();
                        _table1 = new DataTable();
                        _table2 = new DataTable();
                        _table3 = new DataTable();
                        _table4 = new DataTable();
                        _table5 = new DataTable();
                        _table6 = new DataTable();

                        #endregion

                        #region bill-Table1

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText = ("SELECT * " +
                                           " FROM [Sellers].[View_BillIDataU_PS] " +
                                           " where PowerStationCode='" +
                                           _xcn + "' Order By Date,Hour")//Add: Order By Date,Hour 1395/11/25
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table1.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-table6-PS

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT DISTINCT(PowerStationCode),PowerStationName,'-' as UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                                 " FROM [Sellers].[View_BillIDataU_PS] " +
                                 " where PowerStationCode='" +
                                 _xcn + "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table6.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Date

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table2.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-Sum

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *     FROM [Sellers].[View_BillIDataSum]" +
                                 " where PowerStationCode='" +
                                 _xcn + "'" + " order by  PowerStationCode,PowerStationName,BillUnitT,Date")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table3.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-TotalP

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                           " FROM [Sellers].[View_BillIDataTotalP]" +
                                           " where PowerStationCode='" +
                                           _xcn + "'" +
                                           "order by [PowerStationCode]")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table4.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-CompanyCodeName

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  Distinct(CompanyCode),CompanyName FROM [Sellers].[BillDataU] " +
                                 " where PowerStationCode='" +
                                 _xcn + "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            SqlDataReader mySqlDataReader =
                                _mySqlCommand1.ExecuteReader();
                            if (mySqlDataReader.HasRows)
                                while (mySqlDataReader.Read())
                                    _companyCode = mySqlDataReader[0].ToString();

                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region bill-TotalPP

                        #region Prepare Connection & Command

                        _mySqlConnection =
                            new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT  *    FROM [Sellers].[View_BillIDataTotalPP]" +
                                 " where PowerStationCode='" +
                                 _xcn + "'")
                        };

                        #endregion

                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            _table5.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }

                        #endregion


                        #endregion

                        #region Fuel
                        //1395/11/20

                        DataTable tableFuel = new DataTable();
                        _mySqlConnection =
                           new SqlConnection(DbBizClass.DbConnStr);
                        _mySqlCommand1 = new SqlCommand
                        {
                            Connection = _mySqlConnection,
                            CommandText =
                                ("SELECT * FROM [Fuel].[View_BillIDataEms_OutFuelBill]" +
                                 " WHERE  PowerPlantCode='" + _xcn + "'")
                        };


                        #region Execute Command

                        try
                        {
                            _mySqlCommand1.Connection.Open();
                            // ReSharper disable AssignNullToNotNullAttribute
                            tableFuel.Load(_mySqlCommand1.ExecuteReader());
                             
                        }
                        catch (Exception ex)
                        {
                            _logger.AddToLogAndShow(ex);
                            //MessageBox.Show(ex.Message);;
                        }
                        finally
                        {
                            _mySqlCommand1.Connection.Close();
                        }
                        #endregion

                        #endregion



                        #region Bill-Excel

                        _table1.TableName = "table1";
                        _table2.TableName = "Date";
                        _table3.TableName = "Sum";
                        _table4.TableName = "TotalP";
                        _table5.TableName = "Total";
                        _table6.TableName = "PS";
                        tableFuel.TableName = "Fuel";

                        _ds.Tables.Add(_table1);
                        _ds.Tables.Add(_table2);
                        _ds.Tables.Add(_table3);
                        _ds.Tables.Add(_table4);
                        _ds.Tables.Add(_table5);
                        _ds.Tables.Add(_table6);
                        _ds.Tables.Add(tableFuel);
                        _exToexcel = new ExportToExcel();




                        if (_btype == "روزانه")
                        {

                            _exToexcel.toexcel(
                                _powerStationName, " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                                @"D:\Settlement\Temp\temp-BillSell.xls",
                                @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) +
                                _dateCounter.Substring(5, 2) +
                                _dateCounter.Substring(8, 2) + @"\PowerPlant\Reghabati\",
                                @"BillS_D_" + _powerStationName + " (" + _xcn + ")" + "_" + _dateCounter.Substring(2, 2) +
                                _dateCounter.Substring(5, 2) + _dateCounter.Substring(8, 2) + ".xls", false, _logger);


                        }
                        if (_btype == "ماهيانه")
                        {
                            _exToexcel.toexcel(
                                _powerStationName,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-BillSell.xls",
                                @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\PowerPlant\Reghabati\",
                                @"BillS_M_" + " (" + _xcn + ")_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                ".xls", false, _logger);
                        }
                        if (_btype == "قطعي")
                        {
                            _exToexcel.toexcel(
                                _powerStationName,
                                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                                @"D:\Settlement\Temp\temp-BillSell.xls",
                                @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                @"\PowerPlant_Bill\PowerPlant\Reghabati\",
                                @"BillS_C_" + " (" + _xcn + ")_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                ".xls", false, _logger);
                        }


                        #endregion


                    }

                }

                #endregion


               
            }
        }

        #endregion

        #region Sub-ComPany
        //1395/11/26
        private void SubComPany()
        {
            #region Company

            _tableCn = new DataTable();

            #region bill-CompanyCode

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    ("SELECT  DISTINCT RTRIM(CompanyCode),RTRIM(CompanyName),RTRIM(companyCodeE),CompanyTypeName,AccountingCode FROM [Sellers].[BillDataU] " +
                     " order by AccountingCode,CompanyTypeName,RTRIM(CompanyCode)")
                //where  MN ='N' 1395/11/24 //Add RTRIM 1395/12/15
            };

            #endregion

            #region Execute Command

            try
            {

                _mySqlCommand1.Connection.Open();

                _tableCn.Load(_mySqlCommand1.ExecuteReader());


                if (_tableCn.Rows.Count > 0)
                {

                }
                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
                //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            _logger.AddToLogAndShow(string.Format("Number of companies: {0}", _tableCn.Rows.Count), LogStyle.InfoStyle);

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {

                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    return;
                }

                _xcn = _tableCn.Rows[j][0].ToString();
                _companyName1 = _tableCn.Rows[j][1].ToString();
                _xcnE1 = _tableCn.Rows[j][2].ToString();

                if (_tableCn.Rows[j][0].ToString() != _tableCn.Rows[j][2].ToString())
                {
                    _xcnE = _tableCn.Rows[j][0] + "_" + (string) _tableCn.Rows[j][2];
                }
                else
                {
                    _xcnE = _xcn;
                }

                #region company

                #region Intial

                _ds = new DataSet();

                _table1 = new DataTable();
                _table2 = new DataTable();
                _table3 = new DataTable();
                _table4 = new DataTable();
                _table5 = new DataTable();
                _table6 = new DataTable();

                #endregion

                #region bill-Table1

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText = ("SELECT * " +
                                   " FROM [Sellers].[View_BillIDataU_PS] " +
                                   " where companyCodeE='" + _xcnE1 + "' and CompanyCode='" +
                                   _xcn + "'") //delete:  MarketStateCode ='N' 1395/11/24
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table1.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-table6-PS

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT DISTINCT(PowerStationCode),PowerStationName,'-' as UnitType,BillUnitT,[PowerStationCode] as PsCT" +
                         " FROM [Sellers].[View_BillIDataU_PS] " +
                         " where companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn + "'" +
                         " Order by PowerStationCode,PowerStationName,BillUnitT")
                    //delete:  MarketStateCode ='N' 1395/11/24
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table6.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-Date

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        "SELECT   DISTINCT (Date)  Date FROM [Sellers].[BillDataU] order by Date"
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table2.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-Sum

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *  FROM [Sellers].[View_BillIDataSum]" +
                         " where companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn + "'" +
                         " Order by PowerStationCode,PowerStationName,BillUnitT,Date")
                    //delete:  MarketStateCode ='N' 1395/11/24
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table3.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-TotalP

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                   " FROM [Sellers].[View_BillIDataTotalP]" +
                                   " where companyCodeE='" + _xcnE1 +
                                   "' and  CompanyCode='" + _xcn + "'" +
                                   " order by [PowerStationCode]") //delete:  MarketStateCode ='N' 1395/11/24
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table4.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-Total

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT * FROM [Sellers].[View_BillIDataTotal]" +
                         " where companyCodeE='" + _xcnE1 + "' and  CompanyCode='" + _xcn +
                         "'") //delete:  MarketStateCode ='N' 1395/11/24
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table5.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion


                #region Fuel

                //1395/11/20

                DataTable tableFuel = new DataTable();
                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT * FROM [Fuel].[View_BillIDataCompany]" +
                         " WHERE CompanyCode='" + _xcn + "'")
                };


                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    tableFuel.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion


                #region Bill-Excel

                _table1.TableName = "table1";
                _table2.TableName = "Date";
                _table3.TableName = "Sum";
                _table4.TableName = "TotalP";
                _table5.TableName = "Total";
                _table6.TableName = "PS";
                tableFuel.TableName = "Fuel";
                _ds.Tables.Add(_table1);
                _ds.Tables.Add(_table2);
                _ds.Tables.Add(_table3);
                _ds.Tables.Add(_table4);
                _ds.Tables.Add(_table5);
                _ds.Tables.Add(_table6);
                _ds.Tables.Add(tableFuel);
                _exToexcel = new ExportToExcel();



                if (_btype == "روزانه")
                {

                    string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}.xls", _subjectChar, _billTypeCode,
                        _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) + _dateCounter.Substring(8, 2),
                        _revision, _xcnE);
                    _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);
          
                    _exToexcel.toexcel(
                        _companyName1,
                        " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                        @"D:\Settlement\Temp\temp-BillSell.xls",
                        @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                        _dateCounter.Substring(8, 2) + @"\Seller\Company\", fileName, false, _logger);

                }

                if (_btype == "ماهيانه")
                {
                    string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}.xls", _subjectChar, _billTypeCode,
                        _year + _month, _revision, _xcnE);
                    _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                    _exToexcel.toexcel(_companyName1, "",
                        " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-BillSell.xls",
                        @"D:\Settlement\Out\Monthly\" + _year + _month +
                        @"\Seller\Company\", fileName, false, _logger);
                }
                if (_btype == "قطعي")
                {
                    string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}.xls", _subjectChar, _billTypeCode,
                        _year + _month, _revision, _xcnE);
                    _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                    _exToexcel.toexcel(_companyName1, "",
                        " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-BillSell.xls",
                        @"D:\Settlement\Out\Definitive\" + _year + _month +
                        @"\Seller\Company\", fileName, false,
                        _logger);
                }

                #endregion

                #endregion


            }

            #endregion

            _logger.AddToLogAndShow("__________________________________", LogStyle.InfoStyle);
        }

        #endregion

        #region SubUnitPowerPlant

        //1395/11/26
        private void SubUnitPowerPlant()
        {
            #region PowerPlant

            _tableCn = new DataTable();

            #region bill-PowerStationCode

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    ("SELECT  DISTINCT RTRIM(PowerStationCode),RTRIM(PowerStationName),RTRIM(CompanyCode),RTRIM(CompanyCodeE),MN  FROM [Sellers].[BillDataU] "
                     )//delete " where MarketStateCode  like 'N%' " 1395/11/25 //Add RTRIM 1395/12/15
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _tableCn.Load(_mySqlCommand1.ExecuteReader());
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
                //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            _logger.AddToLogAndShow(string.Format("Number of powerplants: {0}", _tableCn.Rows.Count), LogStyle.InfoStyle);

            #endregion

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {
                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    return;
                }
                _xcn = _tableCn.Rows[j][0].ToString();
                _powerStationName = _tableCn.Rows[j][1].ToString();
                string companyCode = _tableCn.Rows[j][3].ToString();//CompanyCodeE

                //string subjectChar = _tableCn.Rows[j][4].ToString().TrimEnd() == "N" ? "S" : "Z";

                if (lstPowerStations.List.CheckedItems.Count > 0)
                {
                    #region Intial

                    _ds = new DataSet();
                    _table1 = new DataTable();
                    _table2 = new DataTable();
                    _table3 = new DataTable();
                    _table4 = new DataTable();
                    _table5 = new DataTable();
                    _table6 = new DataTable();
                    _table7 = new DataTable();

                    #endregion

                    #region billP

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT * " +
                                       " FROM [Sellers].[View_BillIDataU_PS] " +
                                       " where PowerStationCode='" +
                                       _xcn + "' Order By Date,Hour")//Add: Order By Date,Hour 1395/11/25
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table4.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion
               

                    #region bill-Sum

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *     FROM [Sellers].[View_BillIDataSum]" +
                             " where PowerStationCode='" +
                             _xcn + "'" + " order by  PowerStationCode,PowerStationName,BillUnitT,Date")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table5.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-TotalP

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                       " FROM [Sellers].[View_BillIDataTotalP]" +
                                       " where PowerStationCode='" +
                                       _xcn + "'" +
                                       "order by [PowerStationCode]")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table6.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-CompanyCodeName

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  Distinct(CompanyCode),CompanyName FROM [Sellers].[BillDataU] " +
                             " where PowerStationCode='" +
                             _xcn + "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        SqlDataReader mySqlDataReader =
                            _mySqlCommand1.ExecuteReader();
                        if (mySqlDataReader.HasRows)
                            while (mySqlDataReader.Read())
                                _companyCode = mySqlDataReader[0].ToString();

                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-TotalPP

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *    FROM [Sellers].[View_BillIDataTotalPP]" +
                             " where PowerStationCode='" +
                             _xcn + "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table7.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Fuel
                    //1395/11/20

                    DataTable tableFuel = new DataTable();
                    _mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Fuel].[View_BillIDataEms_OutFuelBill]" +
                             " WHERE  PowerPlantCode='" + _xcn + "'")
                    };


                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        tableFuel.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }
                    #endregion

                    #endregion

                    /////////////////////////////////////////////////////////////
                    #region Unit  
                    
                    #region BillU

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT * " +
                                       " FROM [Sellers].[BillDataU] " +
                                       " where PowerStationCode='" +
                                       _xcn + "' Order By Date,UnitCode,Hour")//Order By Date,UnitCode,Hour 1395/11/23
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table1.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion
          

                    #region bill-Sum

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  * FROM [Sellers].[View_BillIDataSumU]" +
                             " where PowerStationCode='" +
                             _xcn + "'" + " order by unitcode,Date")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table2.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-TotalU

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = ("SELECT *,[PowerStationCode] as PsCT" +
                                       " FROM [Sellers].[View_BillIDataTotalPU]" +
                                       " where PowerStationCode='" +
                                       _xcn + "'" +
                                       "order by [PowerStationCode],unitCode")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table3.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region bill-CompanyCodeName

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  Distinct(CompanyCode),CompanyName FROM [Sellers].[BillDataU] " +
                             " where PowerStationCode='" +
                             _xcn + "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        SqlDataReader mySqlDataReader =
                            _mySqlCommand1.ExecuteReader();
                        if (mySqlDataReader.HasRows)
                            while (mySqlDataReader.Read())
                                _companyCode = mySqlDataReader[0].ToString();

                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion
              
                    #endregion



                    #region Bill-Excel
                    _table1.TableName = "Bill";
                    _table2.TableName = "Sum";
                    _table3.TableName = "TotalU";
                    _table4.TableName = "BillP";
                    _table5.TableName = "SumP";
                    _table6.TableName = "TotalP";
                    _table7.TableName = "Total";
                    tableFuel.TableName = "Fuel";

                    _ds.Tables.Add(_table1);
                    _ds.Tables.Add(_table2);
                    _ds.Tables.Add(_table3);
                    _ds.Tables.Add(_table4);
                    _ds.Tables.Add(_table5);
                    _ds.Tables.Add(_table6);
                    _ds.Tables.Add(_table7);
                    _ds.Tables.Add(tableFuel);
                    _exToexcel = new ExportToExcel();




                    if (_btype == "روزانه")
                    {
                        string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}_{5}.xls", _subjectChar, _billTypeCode,
                            _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) + _dateCounter.Substring(8, 2),
                            _revision, companyCode, _xcn);
                        _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                        _exToexcel.toexcel(
                            _powerStationName, " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                            @"D:\Settlement\Temp\temp-BillSellUP.xls",
                            @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) +
                            _dateCounter.Substring(5, 2) +
                            _dateCounter.Substring(8, 2) + @"\Seller\PowerPlant\", fileName, false, _logger);//add \ before Seller 1396/01/27


                    }
                    if (_btype == "ماهيانه")
                    {
                        string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}_{5}.xls", _subjectChar, _billTypeCode,
                            _year + _month, _revision, companyCode, _xcn);
                        _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                        _exToexcel.toexcel(
                            _powerStationName,
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSellUP.xls",
                            @"D:\Settlement\Out\Monthly\" + _year + _month +
                            @"\Seller\PowerPlant\", fileName, false, _logger);
                    }
                    if (_btype == "قطعي")
                    {
                        string fileName = String.Format("Bill_{0}_{1}_{2}_{3}_{4}_{5}.xls", _subjectChar, _billTypeCode,
                               _year + _month, _revision, companyCode, _xcn);
                        _logger.AddToLogAndShow(string.Format("{0}: {1}", j + 1, fileName), LogStyle.InfoStyle);

                        _exToexcel.toexcel(
                            _powerStationName,
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSellUP.xls",
                            @"D:\Settlement\Out\Definitive\" + _year + _month +
                            @"\Seller\PowerPlant\", fileName, false, _logger);
                    }


                    #endregion


                }

            }
            
            #endregion
            _logger.AddToLogAndShow("__________________________________", LogStyle.InfoStyle);
        }

        #endregion

        #region Sub-Ex-Rep

        private void SubRep()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }
            _ds1Rep = new DataSet();
            _table1Rep = new DataTable();



            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = ("SELECT * " +
                               " FROM [Sellers].[View_BillDataRep]" +
                               " ORDER BY PSC,PSN")
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1Rep.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region Bill-Excel

            _table1Rep.TableName = "table1";
            _ds1Rep.Tables.Add(_table1Rep);
            _exToexcel = new ExportToExcel();




            if (_btype == "روزانه")
            {

                _exToexcel.toexcel("",
                    " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds1Rep, //1394/12/16
                    @"D:\Settlement\Temp\temp-Rep_Buy.xls",
                    @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                    _dateCounter.Substring(8, 2) + @"\",
                    @"Rep_Buy_D_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                    _dateCounter.Substring(8, 2) + ".xls",
                    false, _logger);


            }
            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel("",
                    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                    @"D:\Settlement\Temp\temp-Rep_Buy.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Rep_Buy_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }
            if (_btype == "قطعي")
            {
                _exToexcel.toexcel("",
                    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                    @"D:\Settlement\Temp\temp-Rep_Buy.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Rep_Buy_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }
        }

       #endregion
        #endregion

        //Abdollahzadeh 1395/12/01
        private void SubRepUP()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }
            _ds1Rep = new DataSet();
            _table1= new DataTable();
            _table2 = new DataTable();
            _table3 = new DataTable();


            #region RepU

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = ("SELECT * " +
                               " FROM [Sellers].[View_BillDataRepU]" +
                               " ORDER BY PSC,PSN,UnitCode  "),
                CommandTimeout = 100
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion


            #endregion

            #region RepP

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = ("SELECT * " +
                               " FROM [Sellers].[View_BillDataRep]" +
                               " ORDER BY PSC,PSN"),
                CommandTimeout = 100
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table2.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region RepF

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = ("SELECT * FROM [Fuel].[View_BillIDataEms_OutFuelBill] " +
                               " ORDER BY [PowerPlantCode],[PowerPlantName]")
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _table3.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region Bill-Excel

            _table1.TableName = "RepU";
            _table2.TableName = "RepP";
            _table3.TableName = "RepF";
            _ds1Rep.Tables.Add(_table1);
            _ds1Rep.Tables.Add(_table2);
            _ds1Rep.Tables.Add(_table3);

            _exToexcel = new ExportToExcel();




            if (_btype == "روزانه")
            {

                _exToexcel.toexcel("",
                    " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds1Rep, //1394/12/16
                    @"D:\Settlement\Temp\temp-Rep.xls",
                    @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                    _dateCounter.Substring(8, 2) + @"\",
                    @"Rep_D_" + _dateCounter.Substring(2, 2) + _dateCounter.Substring(5, 2) +
                    _dateCounter.Substring(8, 2) + ".xls",
                    false, _logger);


            }
            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel("",
                    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                    @"D:\Settlement\Temp\temp-Rep.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Rep_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }
            if (_btype == "قطعي")
            {
                _exToexcel.toexcel("",
                    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                    @"D:\Settlement\Temp\temp-Rep.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Rep_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }
        }



        #endregion



        #region Sub-Ex-Taraz

        private void SubTaraz()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region Taraz

            _ds = new DataSet();
            _table1 = new DataTable();
            _table3 = new DataTable();

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT *" +
                    "FROM  [Buyers].[View_TarazExcel] order by  AccountingCode,BillState,CompanyTypeName,CompanyCode"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region bill-Sum

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT  *  From  [Buyers].[View_TarazSUM] "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table3.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            _table1.TableName = "Taraz";
            _table3.TableName = "Sum";
            _ds.Tables.Add(_table1);
            _ds.Tables.Add(_table3);
            _exToexcel = new ExportToExcel();



            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel("", "ماهیانه    " + _sDate + "   تا   " + _eDate, _ds,
                    @"D:\Settlement\Temp\temp-Taraz_final.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Taraz_Final_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }

            if (_btype == "قطعي")
            {
                _exToexcel.toexcel("", "قطعی      " + _sDate + "   تا   " + _eDate, _ds,
                    @"D:\Settlement\Temp\temp-Taraz_final.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Taraz_Final_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
            }

            #endregion

        }

        #endregion

        //تغییر سورس براساس درخواست خانم خدایی برای template جدید تراز مالی

        #region Sub-Ex-MaliTarazSellers

        private void SubMaliTarazSellers()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region MaliTarazSellers

            _ds = new DataSet();
            _table1 = new DataTable();
            _table3 = new DataTable();

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT *" +
                    "FROM  [Buyers].[View_MaliTarazExcel] order by  AccountingCode,BillState,CompanyTypeName,CompanyCode"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region bill-Sum

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT  *  From  [Buyers].[View_MaliTarazSUM] "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table3.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            _table1.TableName = "MaliTaraz";
            _table3.TableName = "MaliSum";
            _ds.Tables.Add(_table1);
            _ds.Tables.Add(_table3);
            _exToexcel = new ExportToExcel();



            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel(_sDate + "   تا   " + _eDate, "ماهیانه    " + _sDate + "   تا   " + _eDate, _ds,
                    @"D:\Settlement\Temp\temp-Taraz_MaliSellers.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Taraz_MaliSellers_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }

            if (_btype == "قطعي")
            {
                _exToexcel.toexcel(_sDate + "   تا   " + _eDate, "قطعی      " + _sDate + "   تا   " + _eDate, _ds,
                    @"D:\Settlement\Temp\temp-Taraz_MaliSellers.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Taraz_MaliSellers_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
            }

            #endregion

        }

        #endregion

        #region Sub-Ex-MaliTarazBuyers

        private void SubMaliTarazBuyers()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region MaliTarazBuyers

            _ds = new DataSet();
            _table1 = new DataTable();
            _table3 = new DataTable();

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT *" +
                    "FROM  [Buyers].[View_MaliTarazExcelBuyer] order by  AccountingCode,BillState,CompanyTypeName,CompanyCode"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region bill-Sum

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT  *  From  [Buyers].[View_MaliTarazSUMBuyer] "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table3.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            _table1.TableName = "MaliTarazBuy";
            _table3.TableName = "MaliSumBuy";
            _ds.Tables.Add(_table1);
            _ds.Tables.Add(_table3);
            _exToexcel = new ExportToExcel();



            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel(_sDate + "   تا   " + _eDate, "ماهیانه    " + _sDate + "   تا   " + _eDate, _ds,
                    @"D:\Settlement\Temp\temp-Taraz_MaliBuyers.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Taraz_MaliBuyers_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }

            if (_btype == "قطعي")
            {
                _exToexcel.toexcel(_sDate + "   تا   " + _eDate, "قطعی      " + _sDate + "   تا   " + _eDate, _ds,
                    @"D:\Settlement\Temp\temp-Taraz_MaliBuyers.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"Taraz_MaliBuyers_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
            }

            #endregion

        }

        #endregion

        #region Sub-Ex-ERate

        private void SubERate()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region Rate

            _ds = new DataSet();
            _table1 = new DataTable();


            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT *" +
                    "FROM  [Buyers].[View_ERateReport] order by  Date,Hour"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion



            _table1.TableName = "Rate";

            _ds.Tables.Add(_table1);

            _exToexcel = new ExportToExcel();



            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel("", _sDate + "   تا   " + _eDate, _ds, @"D:\Settlement\Temp\temp-ERate.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"ERate_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }

            if (_btype == "قطعي")
            {
                _exToexcel.toexcel("", _sDate + "   تا   " + _eDate, _ds, @"D:\Settlement\Temp\temp-ERate.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"ERate_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
            }

            #endregion

        }

        #endregion

        #region Sub-Ex-Fuel

        private void SubFuel()
        {
            try
            {
                _logger.AddToLogAndShow("SP_FuelBill_Calculation", LogStyle.InfoStyle);
                if (_btype == "ماهيانه")
                {
                    _dbSm.CommandTimeout = 10000;
                    _dbSm.SP_FuelBill_Calculation(_sDate, _eDate, "M");

                }

                if (_btype == "قطعي")
                {
                    _dbSm.CommandTimeout = 10000;
                    _dbSm.SP_FuelBill_Calculation(_sDate, _eDate, "C");
                }



                _logger.AddToLogAndShow("Excel FuelBill", LogStyle.InfoStyle);

                #region FuelBill

                _ds = new DataSet();
                _table1 = new DataTable();


                #region bill-Table1

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        "SELECT *" +
                        "FROM [Fuel].[Tbl_FuelBillInfo_S]  order by  PowerStationCode"
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table1.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion



                _table1.TableName = "Fuel";

                _ds.Tables.Add(_table1);

                _exToexcel = new ExportToExcel();



                if (_btype == "ماهيانه")
                {
                    _exToexcel.toexcel("ماهیانه از " + _sDate + "   تا   " + _eDate, _sDate + "   تا   " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-FuelBill.xls",
                        @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                        @"FuelBill_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

                }

                if (_btype == "قطعي")
                {
                    _exToexcel.toexcel(" قطعی از" + _sDate + "   تا   " + _eDate, _sDate + "   تا   " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-FuelBill.xls",
                        @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                        @"FuelBill_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
                }

                #endregion


                _logger.AddToLogAndShow("SP_FuelTaraz", LogStyle.InfoStyle);

                if (_btype == "ماهيانه")
                {
                    _dbSm.SP_FuelTaraz(_sDate, _eDate, "M");

                }

                if (_btype == "قطعي")
                {
                    _dbSm.SP_FuelTaraz(_sDate, _eDate, "C");
                }



                _logger.AddToLogAndShow("Excel Taraz_Fuel", LogStyle.InfoStyle);

                #region FuelTaraz

                _ds = new DataSet();
                _table1 = new DataTable();
                _table3 = new DataTable();


                #region FuelTaraz

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        "SELECT *" +
                        "FROM [Fuel].[Tbl_Taraz_Fuel]  order by  AccountingCode,CompanyCode"


                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table1.Load(_mySqlCommand1.ExecuteReader());
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region FuelTarazSum

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        "SELECT *" +
                        "FROM [Fuel].[View_Taraz_Fuel_Sum]  "
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table3.Load(_mySqlCommand1.ExecuteReader());
                     
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                _table1.TableName = "Taraz";
                _table3.TableName = "SUM";

                _ds.Tables.Add(_table1);
                _ds.Tables.Add(_table3);

                _exToexcel = new ExportToExcel();



                if (_btype == "ماهيانه")
                {
                    _exToexcel.toexcel(_sDate + "   تا   " + _eDate, "ماهیانه " + _sDate + "   تا   " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-Taraz_Fuel.xls",
                        @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                        @"Taraz_Fuel_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

                }

                if (_btype == "قطعي")
                {
                    _exToexcel.toexcel(_sDate + "   تا   " + _eDate, "قطعی " + _sDate + "   تا   " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-Taraz_Fuel.xls",
                        @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                        @"Taraz_Fuel_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
                }

                #endregion
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }

        }

        #endregion

        #region Sub-Ex-MonthlyReport

        private void SubMonthlyReport()
        {

            #region MonthlyReport

            _dbSm.CommandTimeout = 60;
            _dbSm.SP_MonthlyReport();

            _ds = new DataSet();
            _table1 = new DataTable();
            _table2 = new DataTable();
            _table3 = new DataTable();
            _table4 = new DataTable();
            _table5 = new DataTable();

            #region Table1-Seller

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT  *  FROM  [Report].[Tbl_MonthlyReport] order by date,Hour "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region Table2-Buyer

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT  * FROM  [Report].[Tbl_MonthlyReport] order by date,Hour"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table2.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region Table3-Transfer

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT  * FROM  [Report].[Tbl_MonthlyReport] order by date,hour"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table3.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region Table4-Amadegi

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT  * from Report.View_MonthlyReportAmadegiDP2 order by Date "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table4.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region Table5-SellRate

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT  * from Report.View_MonthlyReportBuyRateDP2 order by Date "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table5.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }


            //-----------------------------------------------




            _table1.TableName = "Seller";
            _table2.TableName = "Buyer";
            _table3.TableName = "Transfer";
            _table4.TableName = "Amadegi";
            _table5.TableName = "Energy";

            _ds.Tables.Add(_table1);
            _ds.Tables.Add(_table2);
            _ds.Tables.Add(_table3);
            _ds.Tables.Add(_table4);
            _ds.Tables.Add(_table5);


            _exToexcel = new ExportToExcel();




            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel(
                    " ماهیانه از تاریخ      " + _sDate + "  تا  " + _eDate, "", _ds,
                    @"D:\Settlement\Temp\temp-Monthly-report.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"MonthlyReport_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }

            if (_btype == "قطعي")
            {
                _exToexcel.toexcel(
                    " قطعی از تاریخ    " + _sDate + "  تا  " + _eDate, "", _ds,
                    @"D:\Settlement\Temp\temp-Monthly-report.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"DefinitiveReport_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
            }


            #endregion


        }

        #endregion

        #region Sub-Ex-MaliReport

        private void SubMaliReport()
        {
            if (_bw.CancellationPending)
            {
                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                return;
            }

            #region MonthlyReport

            _ds = new DataSet();
            _table1 = new DataTable();


            #region Table1-Seller

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT  *     FROM  [Report].[View_MaliReport] order by AccountingCode,CompanyTypeName,CompanyCode"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            /////-------------------------------------------




            _table1.TableName = "Mali";

            _ds.Tables.Add(_table1);
            _exToexcel = new ExportToExcel();

            if (_btype == "ماهيانه")
            {
                _exToexcel.toexcel("",
                    " ماهیانه از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-MaliReport.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"MaliReport_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

                //1396/01/21 New template
                _exToexcel.toexcel("",
                    " ماهیانه از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-MaliReport2.xls",
                    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"MaliReport2_M_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

            }

            if (_btype == "قطعي")
            {
                _exToexcel.toexcel("",
                    " قطعی از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-MaliReport.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"MaliReport_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);

                //1396/01/21 New template
                _exToexcel.toexcel("",
                    " قطعی از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-MaliReport2.xls",
                    @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
                    @"MaliReport2_C_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false, _logger);
            }


            #endregion


        }

        #endregion

        #region Sub-Ex-RepU_YearlyReport

        private void SubRepUYearlyReport()
        {

            _ds1Rep = new DataSet();
            _table1Rep = new DataTable();

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,

                CommandText = ("SELECT * " +
                               " FROM [dbo].[Tbl_YearlyRep_Unit_PowerPlant_Company]")

            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1Rep.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region Bill-Excel

            _table1Rep.TableName = "table1";
            _ds1Rep.Tables.Add(_table1Rep);
            _exToexcel = new ExportToExcel();

            _exToexcel.toexcel("",
                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                @"D:\Settlement\Temp\temp-Rep_BuyU.xls",
                @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                _eDate.Substring(8, 2) + @"\",
                @"Rep_U_C_YearlyReport" + ".xls", false, _logger);

            #endregion

        }

        #endregion

        #region Sub-Ex-RepP_YearlyReport

        private void SubRepPYearlyReport()
        {

            _ds1Rep = new DataSet();
            _table1Rep = new DataTable();

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,

                CommandText = ("SELECT * " +
                               " FROM [dbo].[Tbl_YearlyRep_PowerPlant_Company]")

            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1Rep.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region Bill-Excel

            _table1Rep.TableName = "table1";
            _ds1Rep.Tables.Add(_table1Rep);
            _exToexcel = new ExportToExcel();

            _exToexcel.toexcel("",
                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                @"D:\Settlement\Temp\temp-Rep_Buy.xls",
                @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                _eDate.Substring(8, 2) + @"\",
                @"Rep_C_YearlyReport" + ".xls", false, _logger);

            #endregion
        }

        #endregion


        #region Sub-Ex-Rep_YearlyReport

        private void SubRepYearlyReport()
        {

            _ds1Rep = new DataSet();
            _table1 = new DataTable();
            _table2 = new DataTable();
            _table3 = new DataTable();//Fuel

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,

                CommandText = ("SELECT * " +
                               " FROM [dbo].[Tbl_YearlyRep_Unit_PowerPlant_Company]")

            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _table1.Load(_mySqlCommand1.ExecuteReader());

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region bill-Table2

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,

                CommandText = ("SELECT * " +
                               " FROM [dbo].[Tbl_YearlyRep_PowerPlant_Company]")

            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _table2.Load(_mySqlCommand1.ExecuteReader());

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region bill-Table3

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,

                CommandText = ("SELECT * " +
                               " FROM [dbo].[Tbl_YearlyRep_PowerPlant_Fuel]")

            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _table3.Load(_mySqlCommand1.ExecuteReader());

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region Bill-Excel

            _table1.TableName = "RepU";
            _table2.TableName = "RepP";
            _table3.TableName = "RepF";
            _ds1Rep.Tables.Add(_table1);
            _ds1Rep.Tables.Add(_table2);
            _ds1Rep.Tables.Add(_table3);

            _exToexcel = new ExportToExcel();

            _exToexcel.toexcel("",
                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds1Rep,
                @"D:\Settlement\Temp\temp-Rep.xls",
                @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                _eDate.Substring(8, 2) + @"\",
                @"Rep_C_YearlyReport" + ".xls", false, _logger);

            #endregion
        }

        #endregion


        #region Sub-Ex-Bill_YearlyReport

        private void SubBillYearlyReport()
        {
            #region Company
            
            _tableCn = new DataTable();

            #region bill-CompanyName

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    ("SELECT  Distinct CompanyCode,CompanyName,CompanyTypeName,AccountingCode,CompanyID  FROM " +
                     "[dbo].[Tbl_YearlyRep_Company]")//remove: where IsEnsured=0 add:AccountingCode,CompanyID
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _tableCn.Load(_mySqlCommand1.ExecuteReader());
                if (_tableCn.Rows.Count == 0)
                    return;
               

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {
                _xcn = _tableCn.Rows[j][0].ToString();
                _companyName1 = _tableCn.Rows[j][1].ToString();
                _AccountingCode = _tableCn.Rows[j][3].ToString();
                string companyID = _tableCn.Rows[j][4].ToString();

                #region company

                #region Intial

                _ds = new DataSet();
                _table5 = new DataTable();
                _table6 = new DataTable();//fuel

                #endregion

                #region bill-Total

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *   FROM [dbo].[Tbl_YearlyRep_Company]" +
                         " where CompanyCode='" + _xcn +"' AND CompanyID=" + companyID )
                    //remove:  IsEnsured=0 and . add CompanyID=" + companyID
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table5.Load(_mySqlCommand1.ExecuteReader());
                     
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-Fuel

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  * FROM  [dbo].[Tbl_YearlyRep_Company_Fuel] " +
                         " where CompanyCode='" + _xcn + "' AND CompanyID=" + companyID) //add: CompanyID=" + companyID
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table6.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-BillNumber

                //#region Prepare Connection & Command

                //_mySqlConnection =
                //    new SqlConnection(DbBizClass.DbConnStr);
                //_mySqlCommand1 = new SqlCommand
                //{
                //    Connection = _mySqlConnection,
                //    CommandText =
                //        ("SELECT   convert(nvarchar(5),[BillNum])+'-'+[CompanyBillCode] as BillNumber FROM [General].[Tbl_BillNumber] " +
                //         " where BillType='N' and subject='S' and  CompanyCode='" + _xcn +
                //         "'")
                //};

                //#endregion

                //#region Execute Command

                //try
                //{
                //    _createXls = 1;
                //    _mySqlCommand1.Connection.Open();
                //    // ReSharper disable AssignNullToNotNullAttribute
                //    SqlDataReader mySqlDataReader =
                //        _mySqlCommand1.ExecuteReader();
                //    if (mySqlDataReader.HasRows)
                //    {
                //        while (mySqlDataReader.Read())
                //            _billNumber = mySqlDataReader[0].ToString();
                //    }
                //    else
                //    {
                //        _createXls = 1;
                //    }
                     
                //}
                //catch (Exception ex)
                //{
                //    _logger.AddToLogAndShow(ex);
                //    //MessageBox.Show(ex.Message);;
                //}
                //finally
                //{
                //    _mySqlCommand1.Connection.Close();
                //}

               // #endregion

                #endregion

                _billNumber = _xcn + "_" + _sDate.Substring(0, 4);//1396/02/07

                //if (_createXls == 1)
                //{
                    #region Bill-Excel


                    _table5.TableName = "Total";
                    _table6.TableName = "Fuel";

                    _ds.Tables.Add(_table5);
                    _ds.Tables.Add(_table6);

                    _exToexcel = new ExportToExcel();

                _exToexcel.toexcel(_companyName1, _billNumber,
                    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                    @"D:\Settlement\Temp\temp-BillSell_BN_BillPrint.xls",
                    @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                    _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                    _eDate.Substring(8, 2) + @"\Seller\Company\",
                    @"BillS_C_" + _AccountingCode + "_" + _xcn + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                    ".xls",
                    false, _logger);



                #endregion

                //}


                #endregion

            }

            #endregion
            //remove : region CompanyTAZ
           
        }


        #endregion

        #region Sub-Ex-Taraz_YearlyReport

        private void SubTarazYearlyReport()
        {

            #region Taraz

            _ds = new DataSet();
            _table1 = new DataTable();
            _table3 = new DataTable();

            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT * FROM  [dbo].[Tbl_YearlyTaraz_Excel] order by AccountingCode,BillType,CompanyTypeName,CompanyCode "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region bill-Sum

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = "SELECT  *  From  [dbo].[Tbl_YearlyTaraz_SUM] "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table3.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            _table1.TableName = "Taraz";
            _table3.TableName = "Sum";
            _ds.Tables.Add(_table1);
            _ds.Tables.Add(_table3);
            _exToexcel = new ExportToExcel();

            _exToexcel.toexcel("",
                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                @"D:\Settlement\Temp\temp-Taraz_final.xls",
                @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                _eDate.Substring(8, 2) + @"\",
                @"Taraz_Final_YearlyReport" + ".xls", false, _logger);


            //_exToexcel.toexcel("", "از تاریخ " + _sDate + "   تا   " + _eDate, _ds, @"D:\Settlement\Temp\temp-Taraz_final.xls",
            //             @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + @"\",
            //             @"Taraz_Final_Y_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls", false,logger);

            #endregion

        }

        #endregion

        #region Sub-Ex-YearlyMaliReport

        private void SubYearlyMaliReport()
        {

            #region MonthlyReport

            _ds = new DataSet();
            _table1 = new DataTable();


            #region Table1-Seller

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT  *     FROM  [Report].[View_YearlyMaliReport] order by AccountingCode,CompanyTypeName,CompanyCode"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            /////-------------------------------------------




            _table1.TableName = "Mali";

            _ds.Tables.Add(_table1);


            _exToexcel = new ExportToExcel();



            _exToexcel.toexcel("",
                " قطعی از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-MaliReport.xls",
                @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                _eDate.Substring(8, 2) + @"\",
                @"MaliReport_YearlyReport" + ".xls", false, _logger);

            //1396/02/04
            _exToexcel.toexcel("",
              " قطعی از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-MaliReport2.xls",
              @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
              _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
              _eDate.Substring(8, 2) + @"\",
              @"MaliReport_YearlyReport2" + ".xls", false, _logger);

            //   _exToexcel.toexcel("",
            //" از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
            //@"D:\Settlement\Temp\temp-Taraz_final.xls",
            //@"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
            //_sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
            //_eDate.Substring(8, 2) + @"\",
            //@"Taraz_Final_YearlyReport" + ".xls", false,logger);



            #endregion


        }

        #endregion

        #region Sub-Ex-Buyer_YearlyReport

        private void SubBuyerYearlyReport()
        {
            #region Company

            _tableCn = new DataTable();

            #region bill-CompanyName

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    ("SELECT  CompanyCode,CompanyName,CompanyTypeName  FROM " +
                     " [dbo].[Tbl_YearlyReport_CompanyB]")
            };

            #endregion

            #region Execute Command

            try
            {

                _mySqlCommand1.Connection.Open();

                _tableCn.Load(_mySqlCommand1.ExecuteReader());


                if (_tableCn.Rows.Count > 0)
                {

                }
                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {
                _xcn = _tableCn.Rows[j][0].ToString();
                _companyName1 = _tableCn.Rows[j][1].ToString();


                #region company

                #region Intial

                _ds = new DataSet();
                _table5 = new DataTable();
                _table6 = new DataTable();//Fuel


                #endregion

                #region bill-Total

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *   FROM [dbo].[Tbl_YearlyReport_CompanyB]" +
                         " where CompanyCode='" + _xcn +
                         "'")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table5.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-Fuel

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *   FROM [dbo].[Tbl_YearlyReport_CompanyB_Fuel]" +
                         " where CompanyCode='" + _xcn +
                         "'")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    _table6.Load(_mySqlCommand1.ExecuteReader());

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion


                #region bill-BillNumber

                //#region Prepare Connection & Command

                //_mySqlConnection =
                //    new SqlConnection(DbBizClass.DbConnStr);
                //_mySqlCommand1 = new SqlCommand
                //{
                //    Connection = _mySqlConnection,
                //    CommandText =
                //        ("SELECT   convert(nvarchar(5),[BillNum])+'-'+[CompanyBillCode] as BillNumber FROM [General].[Tbl_BillNumber] " +
                //         " where BillType='N' and subject='B' and  CompanyCode='" + _xcn +
                //         "'")
                //};

                //#endregion

                //#region Execute Command

                //try
                //{
                //    _createXls = 1;
                //    _mySqlCommand1.Connection.Open();
                //    // ReSharper disable AssignNullToNotNullAttribute
                //    SqlDataReader mySqlDataReader =
                //        _mySqlCommand1.ExecuteReader();
                //    if (mySqlDataReader.HasRows)
                //    {
                //        while (mySqlDataReader.Read())
                //            _billNumber = mySqlDataReader[0].ToString();
                //    }
                //    else
                //    {
                //        _createXls = 1;
                //    }
                     
                //}
                //catch (Exception ex)
                //{
                //    _logger.AddToLogAndShow(ex);
                //    //MessageBox.Show(ex.Message);;
                //}
                //finally
                //{
                //    _mySqlCommand1.Connection.Close();
                //}

                //#endregion


                #endregion

                _billNumber = _xcn + "_" + _sDate.Substring(0, 4);//1396/02/07

                //if (_createXls == 1)
                //{
                    #region Bill-Excel

                    _table5.TableName = "Total";
                    _table6.TableName = "Fuel";
                    _ds.Tables.Add(_table5);
                    _ds.Tables.Add(_table6);

                    _exToexcel = new ExportToExcel();

                    _exToexcel.toexcel(_companyName1, _billNumber,
                        " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-BillBuy1_BN_Final.xls",
                        @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                        _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                        _eDate.Substring(8, 2) + @"\Buyer\",
                        @"BillB_C_" + _companyName1 + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls",
                        false, _logger);



                    #endregion
                //}


                #endregion

            }

            #endregion
        }

        #endregion

        #region Sub-Ex-Transfer_YearlyReport

        private void SubTransferYearlyReport()
        {
            #region Company

            _tableCn = new DataTable();

            #region bill-CompanyName

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    ("SELECT  CompanyCode,CompanyName FROM " +
                     " [dbo].[Tbl_YearlyReport_CompanyT]")
            };

            #endregion

            #region Execute Command

            try
            {

                _mySqlCommand1.Connection.Open();

                _tableCn.Load(_mySqlCommand1.ExecuteReader());

                if (_tableCn.Rows.Count == 0)
                {
                    return;
                }
               

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            for (int j = 0; j < _tableCn.Rows.Count; j++)
            {
                _xcn = _tableCn.Rows[j][0].ToString();
                _companyName1 = _tableCn.Rows[j][1].ToString();


                #region company

                #region Intial

                _ds = new DataSet();
                _table5 = new DataTable();


                #endregion

                #region bill-Total

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  *   FROM [dbo].[Tbl_YearlyReport_CompanyT]" +
                         " where CompanyCode='" + _xcn +
                         "'")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();
                    // ReSharper disable AssignNullToNotNullAttribute
                    _table5.Load(_mySqlCommand1.ExecuteReader());
                     
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                #region bill-BillNumber

                //#region Prepare Connection & Command

                //_mySqlConnection =
                //    new SqlConnection(DbBizClass.DbConnStr);
                //_mySqlCommand1 = new SqlCommand
                //{
                //    Connection = _mySqlConnection,
                //    CommandText =
                //        ("SELECT   convert(nvarchar(5),[BillNum])+'-'+[CompanyBillCode] as BillNumber FROM [General].[Tbl_BillNumber] " +
                //         " where BillType='N' and subject='T' and  CompanyCode='" + _xcn +
                //         "'")
                //};

                //#endregion

                //#region Execute Command

                //try
                //{
                //    _createXls = 1;
                //    _mySqlCommand1.Connection.Open();
                //    // ReSharper disable AssignNullToNotNullAttribute
                //    SqlDataReader mySqlDataReader =
                //        _mySqlCommand1.ExecuteReader();
                //    if (mySqlDataReader.HasRows)
                //    {
                //        while (mySqlDataReader.Read())
                //            _billNumber = mySqlDataReader[0].ToString();
                //    }
                //    else
                //    {
                //        _createXls = 1;
                //    }
                     
                //}
                //catch (Exception ex)
                //{
                //    _logger.AddToLogAndShow(ex);
                //    //MessageBox.Show(ex.Message);;
                //}
                //finally
                //{
                //    _mySqlCommand1.Connection.Close();
                //}

                //#endregion


                #endregion

                _billNumber = _xcn + "_" + _sDate.Substring(0, 4);//1396/02/07

                //if (_createXls == 1)
                //{
                    #region Bill-Excel


                    _table5.TableName = "TotalBC";
                    _ds.Tables.Add(_table5);

                    _exToexcel = new ExportToExcel();

                    _exToexcel.toexcel(_companyName1 + " ( از تاریخ   " + _sDate + "  تا  " + _eDate + ")", _billNumber,
                        " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                        @"D:\Settlement\Temp\temp-BillTransfer_BN_Final.xls",
                        @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                        _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                        _eDate.Substring(8, 2) + @"\Transfer\",
                        @"BillB_C_" + _companyName1 + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls",
                        false, _logger);



                    #endregion
                //}


                #endregion

            }

            #endregion
        }

        #endregion

        #region Sub-Ex-Fuel_YearlyReport

        private void SubFuelYearlyReport()
        {


            _dbSm.CommandTimeout = 10000;
            _dbSm.SP_YearlyReport_CompanyF(_sDate, _eDate);

            #region FuelBill

            _ds = new DataSet();
            _table1 = new DataTable();


            #region bill-Table1

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT *" +
                    "FROM [dbo].[Tbl_YearlyReport_FuelBill]  order by  PowerStationCode"
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion



            _table1.TableName = "Fuel";

            _ds.Tables.Add(_table1);

            _exToexcel = new ExportToExcel();






            _exToexcel.toexcel("",
                " قطعی از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-FuelBill.xls",
                @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                _eDate.Substring(8, 2) + @"\",
                @"FuelBill_YearlyReport" + ".xls", false, _logger);

            #endregion

            #region FuelTaraz

            _ds = new DataSet();
            _table1 = new DataTable();
            _table3 = new DataTable();


            #region FuelTaraz

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT *" +
                    "FROM [dbo].[Tbl_YearlyReport_FuelTaraz]  order by  AccountingCode,CompanyCode"


            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table1.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            #region FuelTarazSum

            #region Prepare Connection & Command

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText =
                    "SELECT *" +
                    "FROM [Fuel].[View_yearlyReport_Taraz_Fuel_Sum]  "
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                // ReSharper disable AssignNullToNotNullAttribute
                _table3.Load(_mySqlCommand1.ExecuteReader());
                 
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex); //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion


            #endregion

            _table1.TableName = "Taraz";
            _table3.TableName = "SUM";

            _ds.Tables.Add(_table1);
            _ds.Tables.Add(_table3);

            _exToexcel = new ExportToExcel();







            _exToexcel.toexcel("",
                " قطعی از تاریخ   " + _sDate + "  تا  " + _eDate, _ds, @"D:\Settlement\Temp\temp-Taraz_Fuel.xls",
                @"D:\Settlement\Out\YearlyReport\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                _sDate.Substring(8, 2) + "-" + _eDate.Substring(2, 2) + _eDate.Substring(5, 2) +
                _eDate.Substring(8, 2) + @"\",
                @"FuelTaraz_YearlyReport" + ".xls", false, _logger);

            #endregion

        }

        #endregion


        #region Create Fuel BillNumber

        private void FuelBillNumber()
        {
            if (_stype != "قطعی-صورتحساب")
            {
                SubFuelYearlyReport();
            }






        }

        #endregion

        #region Create Transfer BillNumber

        private void TransferBillNumber()
        {
            if (_stype == "قطعی-صورتحساب")
            {
                _tableCn = new DataTable();

                #region bill-CompanyName

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(CompanyCode),CompanyName FROM [Transfer].[BillData] order by CompanyCode")
                };

                #endregion

                #region Execute Command

                try
                {

                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());

                    if (_tableCn.Rows.Count == 0)
                        return;
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }

                    //_xcn = _tableCn.Rows[j][1].ToString();
                    _xcn = _tableCn.Rows[j][0].ToString();
                    _companyName1 = _tableCn.Rows[j][1].ToString();

                    _billNumber = _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + "_" + _tableCn.Rows[j]["CompanyCode"]; //1395/02/11

                    #region company

                    #region Intial

                    _ds = new DataSet();
                    _table1 = new DataTable();

                    #endregion

                    #region bill-TotalBC

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *    FROM  [Transfer].[View_BillIDataTotal_BC]" +
                             " where CompanyCode='" + _xcn +
                             "'")
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table1.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Bill-Excel


                    _table1.TableName = "TotalBC";
                    _ds.Tables.Add(_table1);
                    _exToexcel = new ExportToExcel();

                    _logger.AddToLogAndShow("BillS_M_" + _xcn + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2), LogStyle.InfoStyle);

                    _exToexcel.toexcel(
                        _companyName1 + "( " + " از تاریخ   " + _sDate + "  تا  " + _eDate + " )", _billNumber
                        , _ds,
                        @"D:\Settlement\Temp\temp-Billtransfer_BN.xls",
                        @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                        @"\Transfer\BillPrint\",
                        @"BillS_M_" + _xcn + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls",
                        false, _logger);


                    #endregion




                    #endregion
                }
            }

            else
            {
                _dbSm.CommandTimeout = 10000;
                _dbSm.SP_YearlyReport_CompanyT(_sDate, _eDate);

                try
                {
                    SubTransferYearlyReport();
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    _bw.ReportProgress(0);
                    string strMessage = ex.Message;
                    new Help.FrmDetailMessageBox("خطا در اجرای مرحله پنجم - تولید فایل های اکسل", strMessage,
                        "Error")
                        .ShowDialog();
                }

            }




        }

        #endregion

        #region Create Buyer BillNumber

        private void BuyerBillNumber()
        {

            if (_stype == "قطعی-صورتحساب" || _stype == "قطعی-صورتحساب معوق")
            {
                _tableCn = new DataTable();

                #region bill-CompanyName

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(CompanyCode),CompanyName,AccountingCode  FROM [Buyers].[BillData]  order by AccountingCode")
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());

                    if (_tableCn.Rows.Count == 0)
                        return;

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion

                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }

                    _xcn = _tableCn.Rows[j][0].ToString();
                    _companyName1 = _tableCn.Rows[j][1].ToString();

                    _billNumber = _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + "_" + _tableCn.Rows[j]["CompanyCode"]; //1395/02/11


                    #region bill

                    _ds = new DataSet();
                    _table4 = new DataTable();

                    #region bill-Total

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = "SELECT  *   FROM  [Buyers].[View_BillDataTotal]" +
                                      " where CompanyCode='" + _xcn +
                                      "'"
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table4.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion


                    #endregion

                    #region Fuel
                    //1395/11/20

                    DataTable tableFuel = new DataTable();
                    _mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Fuel].[Tbl_Taraz_Fuel]" +
                             " WHERE  CompanyCode='" + _xcn + "'")
                    };


                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        tableFuel.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }
                    #endregion

                    #endregion


                    _table4.TableName = "Total";
                    tableFuel.TableName = "Fuel";

                    _ds.Tables.Add(_table4);
                    _ds.Tables.Add(tableFuel);
                    _exToexcel = new ExportToExcel();

                    //--------------------------
                    if (_btype == "ماهيانه")
                    {

                        _logger.AddToLogAndShow("BillB_M_" + _tableCn.Rows[j][2] + "_" + _tableCn.Rows[j][0], LogStyle.InfoStyle);//1395/02/11

                        _exToexcel.toexcel(_companyName1
                            , " از تاریخ   " + _sDate + "  تا  " + _eDate, _billNumber, _ds,
                            @"D:\Settlement\Temp\temp-BillBuy1_BN.xls",
                            @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\Buyer\Billprint\",
                            @"BillB_M_" + _tableCn.Rows[j][2] + "_" + _tableCn.Rows[j][0] + ".xls", false, _logger);
                    }

                    if (_btype == "قطعي")
                    {

                        _logger.AddToLogAndShow("BillB_C_" + j, LogStyle.InfoStyle);//1395/02/11

                        _exToexcel.toexcel(
                            _xcn, " از تاریخ   " + _sDate + "  تا  " + _eDate, _billNumber, _ds,
                            @"D:\Settlement\Temp\temp-BillBuy1p.xls",
                            @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\Buyer\Billprint\",
                            @"BillB_C_" + j + ".xls", false, _logger);
                    }

                    //  --------------




                    #endregion
                }
            }

            else
            {
                _dbSm.CommandTimeout = 10000;
                _logger.AddToLogAndShow("SP_YearlyReport_CompanyB", LogStyle.InfoStyle);
                _dbSm.SP_YearlyReport_CompanyB(_sDate, _eDate);
                _logger.AddToLogAndShow("SP_YearlyTaraz_Excel", LogStyle.InfoStyle);
                _dbSm.SP_YearlyTaraz_Excel(_sDate, _eDate);
                _logger.AddToLogAndShow("SP_YearlyTaraz_SUM", LogStyle.InfoStyle);
                _dbSm.SP_YearlyTaraz_SUM(_sDate, _eDate);
                try
                {
                    _logger.AddToLogAndShow("SubTarazYearlyReport", LogStyle.InfoStyle);
                    SubTarazYearlyReport();
                    _logger.AddToLogAndShow("SubBuyerYearlyReport", LogStyle.InfoStyle);
                    SubBuyerYearlyReport();

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    _bw.ReportProgress(0);
                    string strMessage = ex.Message;
                    new Help.FrmDetailMessageBox("خطا در اجرای مرحله پنجم - تولید فایل های اکسل", strMessage,
                        "Error")
                        .ShowDialog();
                }





            }


            //    SubBoronMarzy();


        }

        #endregion

        #region Create Seller BillNumber

        private void SellerBillNumber()
        {
            #region Bill

            if (_stype == "قطعی-صورتحساب")
            {
                #region Company

                _tableCn = new DataTable();

                #region bill-CompanyName

                #region Prepare Connection & Command

                _mySqlConnection =
                    new SqlConnection(DbBizClass.DbConnStr);
                _mySqlCommand1 = new SqlCommand
                {
                    Connection = _mySqlConnection,
                    CommandText =
                        ("SELECT  Distinct(CompanyCode),CompanyName,companycodeE,CompanyTypeName,AccountingCode FROM [Sellers].[View_BillIDataTotal]  order by AccountingCode,CompanyTypeName,CompanyCode") //[General].[View_BillNumberCNonZero_N] 1395/12/02
                };

                #endregion

                #region Execute Command

                try
                {
                    _mySqlCommand1.Connection.Open();

                    _tableCn.Load(_mySqlCommand1.ExecuteReader());

                    if (_tableCn.Rows.Count == 0)
                        return;

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    //MessageBox.Show(ex.Message);;
                }
                finally
                {
                    _mySqlCommand1.Connection.Close();
                }

                #endregion


                #endregion

                for (int j = 0; j < _tableCn.Rows.Count; j++)
                {
                    if (_bw.CancellationPending)
                    {
                        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        return;
                    }

                    _xcn = _tableCn.Rows[j][0].ToString();
                    _companyName1 = _tableCn.Rows[j][1].ToString();
                    _xcnE1 = _tableCn.Rows[j][2].ToString();
                    _AccountingCode = _tableCn.Rows[j][4].ToString();

                    _billNumber = _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + "_" + _tableCn.Rows[j]["CompanyCode"]; //1395/02/11

                    if (_tableCn.Rows[j][0].ToString() != _tableCn.Rows[j][2].ToString())
                    {
                        _xcnE = _tableCn.Rows[j][0] + "_" + (string)_tableCn.Rows[j][2];
                    }
                    else
                    {
                        _xcnE = _xcn;
                    }

                    #region company

                    #region Intial

                    _ds = new DataSet();
                    _table5 = new DataTable();

                    #endregion

                    #region bill-Total

                    #region Prepare Connection & Command

                    _mySqlConnection =
                        new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT  *  FROM [Sellers].[View_BillIDataTotal]" +
                             " where  companyCodeE='" + _xcnE1 + "' and CompanyCode='" + _xcn +
                             "'")//delete : MarketStateCode ='N' 1395/11/24
                    };

                    #endregion

                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        _table5.Load(_mySqlCommand1.ExecuteReader());
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }

                    #endregion

                    #endregion

                    #region Fuel
                    //1395/11/20

                    DataTable tableFuel = new DataTable();
                    _mySqlConnection =
                       new SqlConnection(DbBizClass.DbConnStr);
                    _mySqlCommand1 = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText =
                            ("SELECT * FROM [Fuel].[View_BillIDataCompany]" +
                             " WHERE CompanyCode='" + _xcn + "'")
                    };


                    #region Execute Command

                    try
                    {
                        _mySqlCommand1.Connection.Open();
                        // ReSharper disable AssignNullToNotNullAttribute
                        tableFuel.Load(_mySqlCommand1.ExecuteReader());
                         
                    }
                    catch (Exception ex)
                    {
                        _logger.AddToLogAndShow(ex);
                        //MessageBox.Show(ex.Message);;
                    }
                    finally
                    {
                        _mySqlCommand1.Connection.Close();
                    }
                    #endregion

                    #endregion

                    
                    #region bill-BillNumber

                    #endregion

                    #region Bill-Excel


                    _table5.TableName = "Total";
                    tableFuel.TableName = "Fuel";
                    _ds.Tables.Add(tableFuel);
                    _ds.Tables.Add(_table5);

                    _exToexcel = new ExportToExcel();



                    if (_btype == "روزانه")
                    {

                        //ExToexcel.toexcel(
                        //                                 xcn,
                        //                                  " از تاریخ   " + _SDate + "  تا  " + _EDate, ds,
                        //                                  @"D:\Settlement\Temp\temp-BillSell.xls",
                        //                                  @"D:\Settlement\Out\Daily\" + _SDate.Substring(2, 2) + _SDate.Substring(5, 2) + _SDate.Substring(8, 2) + @"\ComPany\Reghabati\",
                        //                                  @"BillS_D_" + CompanyCode + "_" + _SDate.Substring(2, 2) + _SDate.Substring(5, 2) + _SDate.Substring(8, 2) + ".xls", false,logger);

                    }

                    if (_btype == "ماهيانه")
                    {
                        _logger.AddToLogAndShow(
                            "BillS_M_" + _AccountingCode + "_" + _xcnE + "_" + _sDate.Substring(2, 2) +
                            _sDate.Substring(5, 2), LogStyle.InfoStyle);


                        _exToexcel.toexcel(
                            _companyName1, _billNumber,
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSell_BN_BillPrint.xls",
                            @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\Seller\BillPrint\",
                            @"BillS_M_" + _AccountingCode + "_" + _xcnE + "_" + _sDate.Substring(2, 2) +
                            _sDate.Substring(5, 2) + ".xls", false, _logger);

                        //1395/12/04
                        //_logger.AddToLogAndShow(
                        //    "BillS_M_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2)
                        //    , LogStyle.InfoStyle);

                        //_exToexcel.toexcel(
                        //    _companyName1, _billNumber,
                        //    " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                        //    @"D:\Settlement\Temp\temp-BillSell_BN_BillPrint.xls",
                        //    @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                        //    @"\PowerPlant_Bill\ComPany\Reghabati\",
                        //    @"BillS_M_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) + ".xls",
                        //    false, _logger);
                    }
                    if (_btype == "قطعي")
                    {
                        _logger.AddToLogAndShow(
                            "BillS_C_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2),
                            LogStyle.InfoStyle);

                        _exToexcel.toexcel(
                            _xcn,
                            " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                            @"D:\Settlement\Temp\temp-BillSell_BN_BillPrint.xls",
                            @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            @"\Seller\BillPrint\",
                            @"BillS_C_" + _xcnE + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                            ".xls",
                            false, _logger);
                    }

                    #endregion


                    #endregion


                }

                #endregion

                #region CompanyTAZ
                ////1395/12/02

                //_tableCn = new DataTable();

                //#region bill-CompanyName

                //#region Prepare Connection & Command

                //_mySqlConnection =
                //    new SqlConnection(DbBizClass.DbConnStr);
                //_mySqlCommand1 = new SqlCommand
                //{
                //    Connection = _mySqlConnection,
                //    CommandText =
                //        ("SELECT  Distinct(CompanyCode),CompanyName,AccountingCode FROM [General].[View_BillNumberCNonZero_Z]  order by AccountingCode,CompanyCode")
                //};

                //#endregion

                //#region Execute Command

                //try
                //{

                //    _mySqlCommand1.Connection.Open();
                //    _tableCn.Load(_mySqlCommand1.ExecuteReader());

                //    if (_tableCn.Rows.Count == 0)
                //        return;

                //}
                //catch (Exception ex)
                //{
                //    _logger.AddToLogAndShow(ex);
                //    //MessageBox.Show(ex.Message);;
                //}
                //finally
                //{
                //    _mySqlCommand1.Connection.Close();
                //}

                //#endregion


                //#endregion

                //for (int j = 0; j < _tableCn.Rows.Count; j++)
                //{
                //    if (_bw.CancellationPending)
                //    {
                //        _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                //        return;
                //    }

                //    _xcn = _tableCn.Rows[j][1].ToString();


                //    if (lstSellerList.List.CheckedItems.Count > 0)
                //    {

                //        #region Intial

                //        _ds = new DataSet();
                //        _table5 = new DataTable();


                //        #endregion

                //        #region bill-Total

                //        #region Prepare Connection & Command

                //        _mySqlConnection =
                //            new SqlConnection(DbBizClass.DbConnStr);
                //        _mySqlCommand1 = new SqlCommand
                //        {
                //            Connection = _mySqlConnection,
                //            CommandText =
                //                ("SELECT  *    FROM [Sellers].[View_BillIDataTotal]" +
                //                 " where MarketStateCode like 'Z%' and CompanyName='" + _xcn +
                //                 "'")
                //        };

                //        #endregion

                //        #region Execute Command

                //        try
                //        {
                //            _mySqlCommand1.Connection.Open();
                //            _table5.Load(_mySqlCommand1.ExecuteReader());
                //        }
                //        catch (Exception ex)
                //        {
                //            _logger.AddToLogAndShow(ex);
                //            //MessageBox.Show(ex.Message);;
                //        }
                //        finally
                //        {
                //            _mySqlCommand1.Connection.Close();
                //        }

                //        #endregion


                //        #endregion

                //        #region bill-CompanyCode

                //        #region Prepare Connection & Command

                //        _mySqlConnection =
                //            new SqlConnection(DbBizClass.DbConnStr);
                //        _mySqlCommand1 = new SqlCommand
                //        {
                //            Connection = _mySqlConnection,
                //            CommandText =
                //                ("SELECT  Distinct(CompanyCode),CompanyName FROM [Sellers].[BillDataU] " +
                //                 " where CompanyName='" + _xcn +
                //                 "'")
                //        };

                //        #endregion

                //        #region Execute Command

                //        try
                //        {
                //            _mySqlCommand1.Connection.Open();
                //            SqlDataReader mySqlDataReader =
                //                _mySqlCommand1.ExecuteReader();
                //            if (mySqlDataReader.HasRows)
                //            {
                //                while (mySqlDataReader.Read())
                //                    _companyCode = mySqlDataReader[0].ToString();
                //            }

                //        }
                //        catch (Exception ex)
                //        {
                //            _logger.AddToLogAndShow(ex);
                //            //MessageBox.Show(ex.Message);;
                //        }
                //        finally
                //        {
                //            _mySqlCommand1.Connection.Close();
                //        }

                //        #endregion


                //        #endregion

                //        #region Bill-Excel

                //        _table5.TableName = "Total";

                //        _ds.Tables.Add(_table5);

                //        _exToexcel = new ExportToExcel();


                //        if (_btype == "روزانه")
                //        {
                //            _logger.AddToLogAndShow(
                //                "BillS_D_" + _companyCode + "_" + _dateCounter.Substring(2, 2) +
                //                _dateCounter.Substring(5, 2) +
                //                _dateCounter.Substring(8, 2), LogStyle.InfoStyle);

                //            _exToexcel.toexcel(_xcn, " از تاریخ   " + _dateCounter + "  تا  " + _dateCounter, _ds,
                //                @"D:\Settlement\Temp\temp-EnsuredBill_BillPrint.xls",
                //                @"D:\Settlement\Out\Daily\" + _dateCounter.Substring(2, 2) +
                //                _dateCounter.Substring(5, 2) +
                //                _dateCounter.Substring(8, 2) + @"\ComPany\Tazmini\",
                //                @"BillS_D_" + _companyCode + "_" + _dateCounter.Substring(2, 2) +
                //                _dateCounter.Substring(5, 2) +
                //                _dateCounter.Substring(8, 2) + ".xls", false, _logger);

                //        }
                //        if (_btype == "ماهيانه")
                //        {
                //            _logger.AddToLogAndShow(
                //               "BillS_M_" + _tableCn.Rows[j][2] + "_" + _companyCode + "_" +
                //                _sDate.Substring(2, 2) +
                //                _sDate.Substring(5, 2) + "_Z", LogStyle.InfoStyle);

                //            _exToexcel.toexcel(
                //                _xcn,
                //                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                //                @"D:\Settlement\Temp\temp-EnsuredBill_BillPrint.xls",
                //                @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                //                @"\PowerPlant_Bill\ComPany\BillPrint\",
                //                @"BillS_M_" + _tableCn.Rows[j][2] + "_" + _companyCode + "_" +
                //                _sDate.Substring(2, 2) +
                //                _sDate.Substring(5, 2) + "_Z.xls", false, _logger);
                //        }
                //        if (_btype == "قطعي")
                //        {
                //            _logger.AddToLogAndShow(
                //                "BillS_C_" + _companyCode + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2),
                //                LogStyle.InfoStyle);

                //            _exToexcel.toexcel(
                //                _xcn,
                //                " از تاریخ   " + _sDate + "  تا  " + _eDate, _ds,
                //                @"D:\Settlement\Temp\temp-EnsuredBill_BillPrint.xls",
                //                @"D:\Settlement\Out\Definitive\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                //                @"\PowerPlant_Bill\ComPany\Tazmini\",
                //                @"BillS_C_" + _companyCode + "_" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                //                ".xls", false, _logger);
                //        }

                //        #endregion


                //    }

                //}

                #endregion
            }
            #endregion

            #region YearlyReport

            else
            {
                _dbSm.CommandTimeout = 10000;

                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    return;
                }
                _logger.AddToLogAndShow("SP_YearlyRep_Unit_PowerPlant_Company", LogStyle.InfoStyle);
                _dbSm.SP_YearlyRep_Unit_PowerPlant_Company(_sDate, _eDate);
                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    return;
                }
                _logger.AddToLogAndShow("SP_YearlyRep_PowerPlant_Company", LogStyle.InfoStyle);
                _dbSm.SP_YearlyRep_PowerPlant_Company(_sDate, _eDate);

                if (_bw.CancellationPending)
                {
                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                    return;
                }
                _logger.AddToLogAndShow("SP_YearlyRep_Company", LogStyle.InfoStyle);
                _dbSm.SP_YearlyRep_Company(_sDate, _eDate);

                //if (_bw.CancellationPending)
                //{
                //    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                //    return;
                //}
                //_logger.AddToLogAndShow("SP_YearlyRep_OutSellerBillTotal", LogStyle.InfoStyle);
                //_dbSm.SP_YearlyRep_OutSellerBillTotal(_sDate, _eDate);
                //if (_bw.CancellationPending)
                //{
                //    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                //    return;
                //}
                //_logger.AddToLogAndShow("SP_YearlyRep_UpdateOutSellerBillTotal", LogStyle.InfoStyle);
                //_dbSm.SP_YearlyRep_UpdateOutSellerBillTotal(_sDate, _eDate);
            

                try
                {
                    //_logger.AddToLogAndShow("SubRepUYearlyReport", LogStyle.InfoStyle);
                    //SubRepUYearlyReport();
                    //_logger.AddToLogAndShow("SubRepPYearlyReport", LogStyle.InfoStyle);
                    //SubRepPYearlyReport();
                    _logger.AddToLogAndShow("SubRepYearlyReport", LogStyle.InfoStyle);
                    SubRepYearlyReport();
                    _logger.AddToLogAndShow("SubBillYearlyReport", LogStyle.InfoStyle);
                    SubBillYearlyReport();
                    _logger.AddToLogAndShow("SubYearlyMaliReport", LogStyle.InfoStyle);
                    SubYearlyMaliReport();

                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow(ex);
                    _bw.ReportProgress(0);
                    string strMessage = ex.Message;
                    new Help.FrmDetailMessageBox("خطا در اجرای مرحله پنجم - تولید فایل های اکسل", strMessage,
                        "Error")
                        .ShowDialog();
                }

            }

            #endregion
        }


        #endregion

        #region SetBillNumber

        private void SetBillNumber()
        {
            //if (_btype == "قطعي")
            //{
            //    return;//1395/04/27 
            //}

            btnStopStage.BeginInvoke((MethodInvoker)delegate() { btnStopStage.Enabled = true; });
            richTextBoxLog.BeginInvoke((MethodInvoker)delegate() { richTextBoxLog.Visible = true; });
            panelLog.BeginInvoke((MethodInvoker)delegate() { panelLog.Visible = true; });

            _logger.AddToLogAndShow("\n********************** STAGE 7 **********************", LogStyle.InfoStyle);
            _logger.AddToLogAndShow("Setting Bill Number", LogStyle.InfoStyle);

            try
            {
                switch (_ptypeE)
                {
                    case "Sellers":
                        //1395/02/11
                        //_logger.AddToLogAndShow("SP_BillNumberUpdate", LogStyle.InfoStyle);
                        //_dbSm.SP_BillNumberUpdate(_sDate, _eDate, "S");
                        SellerBillNumber();
                        break;

                    case "Buyers":
                        //1395/02/11
                        //_logger.AddToLogAndShow("SP_BillNumberUpdate", LogStyle.InfoStyle);
                        //_dbSm.SP_BillNumberUpdate(_sDate, _eDate, "B");
                        BuyerBillNumber();

                        break;
                    case "Transfer":
                        //1395/02/11
                        //_logger.AddToLogAndShow("SP_BillNumberUpdate", LogStyle.InfoStyle);
                        //_dbSm.SP_BillNumberUpdate(_sDate, _eDate, "T");
                        TransferBillNumber();
                        break;

                    case "Fuel":
                        FuelBillNumber();
                        break;
                }
            }
            catch (Exception e)
            {
                _logger.AddToLogAndShow(e);
            }
            btnStopStage.BeginInvoke((MethodInvoker)delegate() { btnStopStage.Enabled = false; });
            _logger.AddToLogAndShow("____________________________________________________________________",
                LogStyle.InfoStyle);
        }

        #endregion



        //1395/12/04 Abdollahzadeh
        private int getLastBillRevision(string startDate,string endDate,string subjectChar,int typeId)
        {
            _tableCn = new DataTable();

            #region Prepare Connection & Command

            string query =
                String.Format(
                    "SELECT MAX(Revision)FROM EMISMAIN.EMISB.dbo.Bill WHERE Subject='{0}' AND FromDate='{1}' AND ToDate='{2}' AND TypeID={3}",
                    subjectChar, startDate, endDate, typeId);

            _mySqlConnection =
                new SqlConnection(DbBizClass.DbConnStr);
            _mySqlCommand1 = new SqlCommand
            {
                Connection = _mySqlConnection,
                CommandText = query
            };

            #endregion

            #region Execute Command

            try
            {
                _mySqlCommand1.Connection.Open();
                _tableCn.Load(_mySqlCommand1.ExecuteReader());
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
                //MessageBox.Show(ex.Message);;
            }
            finally
            {
                _mySqlCommand1.Connection.Close();
            }

            #endregion

            int r = 0;
            try
            {
                 r = int.Parse(_tableCn.Rows[0][0].ToString());
            }
            catch {}


            return r;
        }

    }
}
