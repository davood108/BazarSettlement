﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class BillExecStepT :  MohanirFormDialog
    {
        #region Field

        #region int  ExecState
        public int ExecState { get; set; }
        #endregion

        #region Step
        public bool _Step1 { get; set; }
        public bool _Step2 { get; set; }
        #endregion
        #endregion

        #region Ctor
        public BillExecStepT()
        {
            InitializeComponent();
            StepState.ExecStateT = false;

            ShowDialog();
        }
        #endregion

        #region  BtnCancelClick
        private void BtnCancelClick(object sender, EventArgs e)
        {
            StepState.ExecStateT = false;
            Close();
        }
        #endregion

        #region BtnExecClick
        private void BtnExecClick(object sender, EventArgs e)
        {
            StepState.ExecStateT = true;
            if(rbt1.Checked)
            {
                _Step1 = true;
            }
            else
            {
                _Step2 = true;
            }
            Close();
        }
#endregion

        private void BillExecStepT_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"help.chm", HelpNavigator.Topic, "BillExecStepT.htm");
        }

        private void BillExecStepT_Load(object sender, EventArgs e)
        {

        }

        
    }
}
