﻿using System;
using System.Drawing.Printing;
using System.Windows.Forms;
using MohanirPouya.Classes;

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class Printing :  MohanirFormDialog

    {
        #region Properties

        #region string  PrinterName
        public string PrinterName { get; set; }
        #endregion

        #region int  NumberOfCopy


        public int NumberOfCopy { get; set; }


        #endregion


        #endregion

        #region Ctor
        public Printing()
        {
            InitializeComponent();
            ShowDialog();
        }
        #endregion

        #region Load
        private void PrintingLoad(object sender, EventArgs e)
        {
          
{
    for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
    {
        var pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
        comboBox1.Items.Add(pkInstalledPrinters);
        comboBox1.Text = comboBox1.Items[0].ToString();
    }
}

        }
        #endregion

        private void BtnPrintingFClick(object sender, EventArgs e)
        {
            PrinterName = comboBox1.Text;
            NumberOfCopy = Convert.ToInt32(textBox1.Text);
            DialogResult = DialogResult.OK;
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            Close();
        }

        private void PrintingHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "Printing.htm");
        }

    
    }
}
