﻿#region using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;
using MohanirPouya.NRI;
using MohanirPouya.Properties;
using MohanirVBClasses;
using RPNCalendar.Utilities;
using DataTable = System.Data.DataTable;
using Parameters = MohanirPouya.Forms.Management.ManageParameters.Classes.Parameters;
using Excel = Microsoft.Office.Interop.Excel;

using System.Threading;
using System.Globalization;

using  NRI.Classes;

#endregion

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class BillSelectD : Office2007Form
    {

        #region Field

        private readonly DbSMDataContext _dbSm;
        private readonly string _ptypeE;
        private readonly string _sDate;
        private readonly string _eDate;
        private readonly BackgroundWorker _bw;
        private string _selectString;
        private readonly string _btype;
        private int _itemsCount;
        private string _btnselect;
        private DataTable _table1;
        private DataTable _table1Rep;
        private DataTable _table2;
        private DataTable _table3;
        private DataTable _table4;
        private readonly String _connectionString;
        private SqlConnection _sqlConnection;
        private SqlCommand _sqlCommand;
        private DataTable _table5;
        private string _companyCode;
        private string _billNumber;
        private DataTable _table6;
        private DataSet _ds;
        private SqlConnection _mySqlConnection;
        private SqlCommand _mySqlCommand1;
        private ExportToExcel _exToexcel;
        private string _companyName1;
        private DataSet _ds1Rep;
        private int _createXls;
        private short _diff;
        private DataTable _table7;
        private readonly string _weekBDate;
        private IQueryable<Tbl_billItem> _query;
        private DataTable _table8;
        private DataTable _tableCn;
        private String _xcn;
        private String _xcnE;
        private string _powerStationName;
        private string _xccL;
        private IQueryable<Tbl_billItemsTD> _query1;
        private Printing _printingForm;
        private string _excelPath;
        private string[] _fileInFolderName1;
        private int _hour;
        private int _minutes;
        private int _second;
        private BillExecStepSb _billExecS;
        private int _errorState;
        private BillExecStepT _billExecT;
        private string _ConnectionStringM;
        private PreDbmsDataContext _pdbSM;
        private string _billTypeCode;
        private string _ServerName;
        private string _InstanceName;
        private string _DataBaseName;
        private string _stype;
        private string _xcnE1;
        private string _AccountingCode;

        private SqlCommand _globalSqlCmd;
        private bool _globalSqlCmdError;
        private SqlConnection _cnn;
        private string _tempLogText = "";
        private Logger _logger;
        private string _dateCounter;

        //1395/12/04
        private int _revision;
        private string _subjectChar;
        private string _year;
        private string _month;
        private string _day;
        private int _typeId;
        //

        #endregion

        #region Properties

        #region Sellers String

        public String[] SellersString
        {
            get
            {
                var returnVal = new String[lstSellerType.List.CheckedItems.Count];
                for (int i = 0; i < lstSellerType.List.CheckedItems.Count; i++)
                {
                    returnVal[i] = lstSellerType.List.CheckedItems[i].ToString();
                }
                return returnVal;
            }
        }

        #endregion

        #region SellersList String

        public String[] SellersListString
        {
            get
            {
                var returnVal = new String[lstSellerList.List.CheckedItems.Count];
                for (int i = 0; i < lstSellerList.List.CheckedItems.Count; i++)
                {
                    returnVal[i] = lstSellerList.List.CheckedItems[i].ToString();
                }
                return returnVal;
            }
        }

        #endregion

        #region Power Stations String

        public String[] PowerStationsString
        {
            get
            {
                var returnVal = new String[lstPowerStations.List.CheckedItems.Count];
                for (int i = 0; i < lstPowerStations.List.CheckedItems.Count; i++)
                {
                    returnVal[i] = lstPowerStations.List.CheckedItems[i].ToString();
                }
                return returnVal;
            }
        }

        #endregion

        #endregion

        #region  Constructor

        /// <summary>
        /// رويه سازنده فرم
        /// </summary>
        public BillSelectD(string type1, string typeEn, string ptype, string ptypeE, string sDate, string eDate,
            short diff, string weeKBDate, String stype, bool showDialog)
        {
            InitializeComponent();

            #region initial Field

            _btype = type1;
            _ptypeE = ptypeE;
            _sDate = sDate;
            _eDate = eDate;
            _diff = diff;
            _weekBDate = weeKBDate;
            _errorState = 0;
            _stype = stype;

            if (typeEn == "C" && stype == "قطعی-صورتحساب")
            {
                _billTypeCode = "C";
                _typeId = 3;
            }

            else if (typeEn == "C" && stype == "قطعی-گزارش سالانه")
            {
                _billTypeCode = "R";
            }

            else if (typeEn == "D" && stype == "قطعی-صورتحساب")
            {
                _billTypeCode = "D";
                _typeId = 1;
            }


            else if (typeEn == "M" && stype == "قطعی-صورتحساب")
            {
                _billTypeCode = "M";
                _typeId = 2;
            }


            else if (typeEn == "C" && stype == "قطعی-صورتحساب معوق")
            {
                _billTypeCode = "DF";
                _typeId = 4;
            }


            _subjectChar = _ptypeE.Substring(0, 1);
            _year = _sDate.Substring(2, 2);
            _month = _sDate.Substring(5, 2);
            _day=_sDate.Substring(8, 2);

       

            #endregion

            #region Initial DataBase Connection

            _pdbSM = new PreDbmsDataContext(DbBizClass.DbConnStrPre);


            var query1 = (from var in _pdbSM.SP_ConnectionInfo(_sDate, _eDate, _billTypeCode)
                select var);
            foreach (var x in query1)
            {
                _ServerName = x.ServerName;
                _InstanceName = x.InstanceName;
                _DataBaseName = x.DataBasename;
            }
            try
            {
                _ConnectionStringM = "Server = " + _ServerName + @"\" + _InstanceName +
                                     "; Database = " + _DataBaseName.Trim() +
                                     "; User ID=Bill_Operator ; Password=vas1383#;";


                lblConnection.Text = @"Server = " + _ServerName + @"\" + _InstanceName + @" , " + @"Data Base = " +
                                     _DataBaseName.Trim();

                DbBizClass.DbConnStr = _ConnectionStringM;
                _connectionString = _ConnectionStringM;
                _dbSm = new DbSMDataContext
                    (DbBizClass.DbConnStr);
            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("خطا در ارتباط با پایگاه داده مورد نظر" + "\n" + _ConnectionStringM,
                    strMessage, "Error").ShowDialog();
                return;
            }

            #endregion

            #region initial BGWorker

            _bw = new BackgroundWorker {WorkerSupportsCancellation = true, WorkerReportsProgress = true};
            _bw.DoWork += BwDoWork;
            _bw.RunWorkerCompleted += BwRunWorkerCompleted;
            _bw.ProgressChanged += BwProgressChanged;


            #endregion

            #region Set Form Title

            //lblBillTitle.Text = ptype;
            //lblSDate.Text = sDate;
            //lblEDate.Text = eDate;
            //lblBillType.Text = type1;
            if (type1 == "روزانه" && stype == "قطعی-صورتحساب")
            {
                lblBillTitle.Text = ptype;
                lblSDate.Text = sDate;
                lblEDate.Text = eDate;
                lblBillType.Text = "روزانه";
            }

            if (type1 == "ماهيانه" && stype == "قطعی-صورتحساب")
            {
                lblBillTitle.Text = ptype;
                lblSDate.Text = sDate;
                lblEDate.Text = eDate;
                lblBillType.Text = "ماهيانه";
            }


            if (type1 == "قطعي" && stype == "قطعی-صورتحساب")
            {
                lblBillTitle.Text = ptype;
                lblSDate.Text = sDate;
                lblEDate.Text = eDate;
                lblBillType.Text = "قطعی-صورتحساب";
            }


            if (type1 == "قطعي" && stype == "قطعی-گزارش سالانه")
            {
                lblBillTitle.Text = ptype;
                lblSDate.Text = sDate;
                lblEDate.Text = eDate;
                lblBillType.Text = "قطعی-گزارش سالانه";
            }
            if (type1 == "قطعي" && stype == "قطعی-صورتحساب معوق")
            {
                lblBillTitle.Text = ptype;
                lblSDate.Text = sDate;
                lblEDate.Text = eDate;
                lblBillType.Text = "قطعی-صورتحساب معوق";
            }

            if (lblBillType.Text == @"گزارش سالانه")
            {
                btnSaveToEMS.Visible = false;
                btnBillNumber.Visible = false;
                btnPrinting.Visible = false;

            }

            #endregion

            #region InitialList

            switch (_ptypeE)
            {
                case "Sellers":

                    #region seller

                    #endregion

                    break;

                case "Buyers":

                    #region Buyer

                    lstPowerStations.Visible = false;
                    lstSellerType.Title = "خریداران";
                    lstSellerList.Title = "لیست خریداران";

                    #endregion

                    break;
                case "Transfer":

                    #region Transfer

                    lstPowerStations.Visible = false;
                    lstSellerType.Title = "خدمات انتقال";
                    lstSellerList.Title = "لیست خدمات انتقال";

                    #endregion

                    break;
            }

            #endregion


       

            if (showDialog)//1395/05/11
                ShowDialog(); 
            else
                Show();
        }



        #endregion

        #region Events Handlers

        #region Form_Load

        private void FormLoad(object sender, EventArgs e)
        {


            _logger = new Logger(richTextBoxLog, this, DbBizClass.CurrentUserName, _sDate, _eDate, _billTypeCode, _ptypeE);
            _cnn = new SqlConnection(_ConnectionStringM);
            _cnn.FireInfoMessageEventOnUserErrors = true;
            _cnn.InfoMessage += new SqlInfoMessageEventHandler(cnn_InfoMessage);
            if (_stype == "قطعی-گزارش سالانه")
            {
                btnExcelOut.Enabled = false;
            }
            else
            {

                _sqlConnection = new SqlConnection(_connectionString);
                _sqlCommand = new SqlCommand();
                _btnselect = "";

                #region Fill list

                switch (_ptypeE)
                {
                    case "Sellers":

                        #region  Sellers


                        if (_btype == "روزانه")
                        {
                            _dbSm.CommandTimeout = 200;
                            _dbSm.SP_InitialUnitBilling(_sDate, _eDate, "S"); //
                        }
                        if (_btype == "ماهيانه")
                        {
                            _dbSm.CommandTimeout = 200;
                            _dbSm.SP_InitialUnitBilling(_sDate, _eDate, "S");

                        }
                        if (_btype == "قطعي")
                        {
                            _dbSm.CommandTimeout = 200;
                            _dbSm.SP_InitialUnitBilling(_sDate, _eDate, "S");
                        }

                        FillSellers();
                        FillSellersListAll();
                        FillPowerStationsAll();

                         lstSellerType.List.SelectedIndexChanged += FillSellersList;
                        lstSellerList.List.SelectedIndexChanged += FillPowerStations;

                        #endregion

                        break;

                    case "Buyers":

                        #region Buyers

                        _dbSm.SP_InitialUnitBilling(_sDate, _eDate, "B");
                        FillSellers();
                        FillSellersListAll();
                        lstSellerType.List.SelectedIndexChanged += FillSellersList;
                        #endregion

                        break;

                    case "Transfer":

                        #region  Transfer

                        _dbSm.SP_InitialUnitBilling(_sDate, _eDate, "T");
                        FillSellers();
                        FillSellersListAll();
                        lstSellerType.List.SelectedIndexChanged += FillSellersList;
                        #endregion

                        break;
                }

                #endregion

                StepState.ExecStateSb = false;
                StepState.ExecStateT = false;
            }

            //1395/04/30
            lstSellerType.PerformClickSellectAll();
            lstSellerList.PerformClickSellectAll();
            lstPowerStations.PerformClickSellectAll();

            if (DbBizClass.CurrentUserName == "اتوماتیک") //1395/05/06
            {
                _billExecS = new BillExecStepSb(true);

                if (_billTypeCode == "M" || _billTypeCode == "C" || _billTypeCode == "DF") //1396/01/31
                {   
                    //1396/05/28
                    _billExecS._Step1 = AutomaticSteps.Step1;
                    _billExecS._Step2 = AutomaticSteps.Step2;
                    _billExecS._Step3 = AutomaticSteps.Step3;
                    _billExecS._Step4 = AutomaticSteps.Step4;
                    _billExecS._Step5 = AutomaticSteps.Step5;
                    _billExecS._stepSaveinEmis = AutomaticSteps.Step6;
                    //1396/05/28
                    ////_billExecS._Step1 = false;
                    ////_billExecS._Step2 = false;
                    ////_billExecS._Step3 = false;
                    ////_billExecS._Step4 = false;
                    //_billExecS._Step5 = false;//excell

                    //_billExecS._stepSaveinEmis = false;
                }

                else if (_billTypeCode == "R")
                {
                    return;
                }

                _errorState = 0;
                _second = 0;
                _minutes = 0;
                _hour = 0;
                lblSecond.Text = @"00";
                lblMinutes.Text = @"00";
                lblHour.Text = @"00";
                timer.Start();
                ProgressBarSearch.Visible = true;
                ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                ProgressBarSearch.Text = StepState.ExecStateSb
                    ? @"مرحله اول - آماده سازی جدول اصلی"
                    : @"در حال اجرای مراحل صورتحساب";
                _btnselect = "E";
                _bw.RunWorkerAsync("E");
            }
        }

        #endregion

        #region btnBillNumber_Click

        private void BtnBillNumberClick(object sender, EventArgs e)
        {
            #region Fill list

            switch (_ptypeE)
            {
                case "Sellers":

                    #region  Sellers

                    //1395/02/11

                    //var connectionS = new SqlConnection(DbBizClass.DbConnStr);
                    //var cmdS = new SqlCommand
                    //{
                    //    CommandText = "SELECT COUNT(*) FROM [Sellers].[View_CountBillNumber]",
                    //    CommandType = CommandType.Text,
                    //    Connection = connectionS
                    //};
                    //connectionS.Open();
                    //object returnValueS = cmdS.ExecuteScalar();
                    //connectionS.Close();
                    //int intReturnValueS = Convert.ToInt32(returnValueS);

                    //if (intReturnValueS > 0)
                    //{
                    //    PersianMessageBox.Show(returnValueS + " نیروگاه به لیست شرکت های موجود در سیستم اضافه شد ",
                    //        "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    _dbSm.SP_AddBillNumber("S");
                    //    new Management.FrmBillNumber("BillSelectD_S").ShowDialog();
                    //}
                    //else
                    //{
                        _second = 0;
                        _minutes = 0;
                        _hour = 0;
                        lblSecond.Text = @"00";
                        lblMinutes.Text = @"00";
                        lblHour.Text = @"00";
                        timer.Start();
                        ProgressBarSearch.Visible = true;
                        ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                        ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                        ProgressBarSearch.Text = @"در حال شماره زنی صورتحسابها ";
                        _btnselect = "B";
                        _bw.RunWorkerAsync("B");
                    //}

                    #endregion

                    break;

                case "Buyers":

                    #region Buyers

                    //var connectionB = new SqlConnection(DbBizClass.DbConnStr);
                    //var cmdB = new SqlCommand
                    //{
                    //    CommandText = "SELECT COUNT(*) FROM [Buyers].[View_CountBillNumber]",
                    //    CommandType = CommandType.Text,
                    //    Connection = connectionB
                    //};
                    //connectionB.Open();
                    //object returnValueB = cmdB.ExecuteScalar();
                    //connectionB.Close();
                    //int intReturnValueB = Convert.ToInt32(returnValueB);

                    //if (intReturnValueB > 0)
                    //{
                    //    PersianMessageBox.Show(returnValueB + " نیروگاه به لیست شماره زنی اضافه شد",
                    //        "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    _dbSm.SP_AddBillNumber("B");
                    //    new Management.FrmBillNumber("BillSelectD_B").ShowDialog();
                    //}
                    //else
                    //{
                        _second = 0;
                        _minutes = 0;
                        _hour = 0;
                        lblSecond.Text = @"00";
                        lblMinutes.Text = @"00";
                        lblHour.Text = @"00";
                        timer.Start();
                        ProgressBarSearch.Visible = true;
                        ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                        ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                        ProgressBarSearch.Text = @"در حال شماره زنی صورتحسابها ";
                        //_Second = 0;
                        //_Minutes = 0;
                        //_Hour = 0;
                        //timer.Start();
                        _btnselect = "B";
                        _bw.RunWorkerAsync("B");
                    //}

                    #endregion

                    break;

                case "Transfer":

                    #region  Transfer

                    //var connectionT = new SqlConnection(DbBizClass.DbConnStr);
                    //var cmdT = new SqlCommand
                    //{
                    //    CommandText = "SELECT COUNT(*) FROM [Transfer].[View_CountBillNumber]",
                    //    CommandType = CommandType.Text,
                    //    Connection = connectionT
                    //};
                    //connectionT.Open();
                    //object returnValueT = cmdT.ExecuteScalar();
                    //connectionT.Close();
                    //int intReturnValueT = Convert.ToInt32(returnValueT);

                    //if (intReturnValueT > 0)
                    //{
                    //    PersianMessageBox.Show(returnValueT + " نیروگاه به لیست شماره زنی اضافه شد",
                    //        "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    _dbSm.SP_AddBillNumber("T");
                    //    new Management.FrmBillNumber("BillSelectD_T").ShowDialog();

                    //}
                    //else
                    //{
                        _second = 0;
                        _minutes = 0;
                        _hour = 0;
                        lblSecond.Text = @"00";
                        lblMinutes.Text = @"00";
                        lblHour.Text = @"00";
                        timer.Start();
                        ProgressBarSearch.Visible = true;
                        ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                        ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                        ProgressBarSearch.Text = @"در حال شماره زنی صورتحسابها ";
                        //_Second = 0;
                        //_Minutes = 0;
                        //_Hour = 0;
                        //timer.Start();
                        _btnselect = "B";
                        _bw.RunWorkerAsync("B");
                    //}

                    #endregion

                    break;

                case "Fuel":

                    #region  Fuel



                    _second = 0;
                    _minutes = 0;
                    _hour = 0;
                    lblSecond.Text = @"00";
                    lblMinutes.Text = @"00";
                    lblHour.Text = @"00";
                    timer.Start();
                    ProgressBarSearch.Visible = true;
                    ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                    ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                    ProgressBarSearch.Text = @"در حال شماره زنی صورتحسابها ";
                    //_Second = 0;
                    //_Minutes = 0;
                    //_Hour = 0;
                    //timer.Start();
                    _btnselect = "B";
                    _bw.RunWorkerAsync("B");


                    #endregion

                    break;
            }

            #endregion
        }

        #endregion
      

        #region btnPrinting_Click

        private void BtnPrintingClick(object sender, EventArgs e)
        {
            _printingForm = new Printing();

            if (_printingForm.DialogResult == DialogResult.OK)
            {
                _second = 0;
                _minutes = 0;
                _hour = 0;
                lblSecond.Text = @"00";
                lblMinutes.Text = @"00";
                lblHour.Text = @"00";
                timer.Start();
                ProgressBarSearch.Visible = true;
                ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                ProgressBarSearch.Text = @"در حال پرینت صورتحسابها";

                _btnselect = "P";
                _bw.RunWorkerAsync("P");
            }
        }

        #endregion

        #region btnExcelOut_Click

        private void BtnExcelOutClick(object sender, EventArgs e)
        {
            if (_ptypeE != @"Fuel")
            {
                #region not fuel

                if (lstSellerList.List.CheckedItems.Count > 0)
                {
                    #region _PtypeE

                    switch (_ptypeE)
                    {
                        case "Sellers":
                            _billExecS = new BillExecStepSb();

                            break;

                        case "Buyers":

                            _billExecS = new BillExecStepSb();

                            break;
                        case "Transfer":

                            _billExecS = new BillExecStepSb();
                            // _billExecT = new BillExecStepT();
                            break;
                    }

                    #endregion

                    #region ExcelOut

                    if (StepState.ExecStateSb || StepState.ExecStateT)
                    {
                        _errorState = 0;
                        _second = 0;
                        _minutes = 0;
                        _hour = 0;
                        lblSecond.Text = @"00";
                        lblMinutes.Text = @"00";
                        lblHour.Text = @"00";
                        timer.Start();
                        ProgressBarSearch.Visible = true;
                        ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                        ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                        ProgressBarSearch.Text = StepState.ExecStateSb
                            ? @"مرحله اول - آماده سازی جدول اصلی"
                            : @"در حال اجرای مراحل صورتحساب";
                        _btnselect = "E";
                        _bw.RunWorkerAsync("E");
                    }

                    #endregion
                }

                else
                {
                    new Help.FrmDetailMessageBox("لطفاآیتمی از لیست فروشندگان انتخاب نمایید",
                        "لطفاآیتمی از لیست فروشندگان انتخاب نمایید  ", "Exclamation Point").
                        ShowDialog();


                }

                #endregion
            }
            else
            {
                _errorState = 0;
                _second = 0;
                _minutes = 0;
                _hour = 0;
                lblSecond.Text = @"00";
                lblMinutes.Text = @"00";
                lblHour.Text = @"00";
                timer.Start();
                ProgressBarSearch.Visible = true;
                ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                ProgressBarSearch.Text = StepState.ExecStateSb
                    ? @"مرحله اول - آماده سازی جدول اصلی"
                    : @"در حال اجرای مراحل صورتحساب";
                _btnselect = "E";
                _bw.RunWorkerAsync("E");
            }
        }

        #endregion

        #region btnSaveToEMS_Click

        private void BtnSaveToEmsClick(object sender, EventArgs e)
        {
            _second = 0;
            _second = 0;
            _minutes = 0;
            _hour = 0;
            lblSecond.Text = @"00";
            lblMinutes.Text = @"00";
            lblHour.Text = @"00";
            timer.Start();
            ProgressBarSearch.Visible = true;
            ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
            ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
            ProgressBarSearch.Text = @"EMS در حال ذخیره سازی در ";
            _btnselect = "S";
            _bw.RunWorkerAsync("S");

        }

        #endregion

        #region btnCpfUpdate_Click

        private void btnCpfUpdate_Click(object sender, EventArgs e)
        {
            _second = 0;
            _minutes = 0;
            _hour = 0;
            lblSecond.Text = @"00";
            lblMinutes.Text = @"00";
            lblHour.Text = @"00";
            timer.Start();
            ProgressBarSearch.Visible = true;
            ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
            ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
            ProgressBarSearch.Text = @"CPF در حال بروزرسانی ضرایب ";
            _btnselect = "C";
            _bw.RunWorkerAsync("C");
        }


        #endregion

        #region BtnCancelClick

        private void BtnCancelClick(object sender, EventArgs eventArgs)
        {
            Close();
        }



        #endregion

        #region TimerTick

        private void TimerTick(object sender, EventArgs e)
        {

            _second++;
            if (_second == 60)
            {
                _second = 0;
                _minutes++;
                if (_minutes == 60)
                {
                    _minutes = 0;
                    _hour++;
                    lblHour.Text = _hour.ToString("D2") + @":";
                }
                lblMinutes.Text = _minutes.ToString("D2") + @":";
            }
            lblSecond.Text = _second.ToString("D2");

            ///////////////////////////////////////////////////
            //logger.UpdateLogOuput(richTextBoxLog);
            // Log();

        }

        #endregion

        #endregion

        private void Log()
        {
            if (_tempLogText != "")
            {
                richTextBoxLog.AppendText(_tempLogText);
                richTextBoxLog.SelectionStart = richTextBoxLog.Text.Length;
                richTextBoxLog.ScrollToCaret();
                richTextBoxLog.Refresh();
                _tempLogText = "";
            }
        }

        private void StopTimer()
        {
            _logger.UpdateLogOuput();
            //Log();
            timer.Stop();
        }

        #region Method

        #region BW_DoWork

        private void BwDoWork(object sender, DoWorkEventArgs e)
        {
            lstPowerStations.BeginInvoke((MethodInvoker) delegate() { lstPowerStations.Enabled = false; });
            lstSellerList.BeginInvoke((MethodInvoker) delegate() { lstSellerList.Enabled = false; });
            lstSellerType.BeginInvoke((MethodInvoker) delegate() { lstSellerType.Enabled = false; });

            if (_bw.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            if ((string) e.Argument == "P")
            {
                PrintingProcess();
            }
            if ((string) e.Argument == "B")
            {
                SetBillNumber();
            }
            if ((string) e.Argument == "S")
            {
                FillEmsSettlement();
            }

            if ((string) e.Argument == "C")
            {
                FillEmsSettlement();
            }

            if ((string) e.Argument == "E")
            {

                if (_ptypeE == @"Fuel")
                {
                    MakeExcel();
                    SetBillNumber(); //1395/04/26
                }
                else
                {
                    if (StepState.ExecStateSb)
                    {
                        #region Step 1

                        if (_billExecS._Step1)
                        {
                            _logger.AddToLogAndShow("\n********************** STAGE 1 **********************",
                                LogStyle.InfoStyle);
                            InitialFixedParameter();
                            if (_errorState == 0)
                            {
                                _bw.ReportProgress(11);
                            }
                        }

                        #endregion

                        #region Step2

                        if (_bw.CancellationPending)
                        {
                            e.Cancel = true;
                            EnableLists();
                            return;
                        }
                        if (_billExecS._Step2)
                        {
                            _logger.AddToLogAndShow("\n********************** STAGE 2 **********************",
                                LogStyle.InfoStyle);
                            _bw.ReportProgress(2);
                            FillFixedParameter();
                            if (_errorState == 0)
                            {
                                _bw.ReportProgress(21);
                            }
                        }

                        #endregion

                        #region step 3

                        if (_billExecS._Step3)
                        {
                            if (_bw.CancellationPending)
                            {
                                e.Cancel = true;
                                EnableLists();
                                return;
                            }
                            _logger.AddToLogAndShow("\n********************** STAGE 3 **********************",
                                LogStyle.InfoStyle);
                            _bw.ReportProgress(3);
                            CreateTable();
                            if (_errorState == 0)
                            {
                                _bw.ReportProgress(31);
                            }
                        }

                        #endregion

                        #region Step 4

                        if (_billExecS._Step4)
                        {
                            if (_bw.CancellationPending)
                            {
                                e.Cancel = true;
                                EnableLists();
                                return;
                            }
                            _logger.AddToLogAndShow("\n********************** STAGE 4 **********************",
                                LogStyle.InfoStyle);
                            _bw.ReportProgress(4);
                            MakeTable2();
                            if (_errorState == 0)
                            {
                                _bw.ReportProgress(41);
                            }
                        }

                        #endregion

                        #region Step 5

                        if (_billExecS._Step5)
                        {
                            if (_bw.CancellationPending)
                            {
                                e.Cancel = true;
                                EnableLists();
                                return;
                            }
                            _logger.AddToLogAndShow("\n********************** STAGE 5 **********************",
                                LogStyle.InfoStyle);
                            _bw.ReportProgress(5);
                            MakeExcel();
                            SetBillNumber(); //1395/04/26
                            if (_errorState == 0)
                            {
                                _bw.ReportProgress(51);
                            }
                        }

                        #endregion

                        #region Step SaveInEmis

                        if (_billExecS._stepSaveinEmis)
                        {
                            FillEmsSettlement();
                        }

                        #endregion
                    }
                    if (StepState.ExecStateT)
                    {
                        #region Step 

                        InitialFixedParameter();
                        if (_errorState == 0)
                        {
                            _bw.ReportProgress(61);
                        }

                        #endregion

                    }
                }
            }

            lstPowerStations.BeginInvoke((MethodInvoker) delegate() { lstPowerStations.Enabled = true; });
            lstSellerList.BeginInvoke((MethodInvoker) delegate() { lstSellerList.Enabled = true; });
            lstSellerType.BeginInvoke((MethodInvoker) delegate() { lstSellerType.Enabled = true; });

        }

        private void EnableLists()
        {
            lstPowerStations.BeginInvoke((MethodInvoker) delegate() { lstPowerStations.Enabled = true; });
            lstSellerList.BeginInvoke((MethodInvoker) delegate() { lstSellerList.Enabled = true; });
            lstSellerType.BeginInvoke((MethodInvoker) delegate() { lstSellerType.Enabled = true; });
        }

        #endregion

        #region BW_ProgressChanged

        private void BwProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            if (e.ProgressPercentage == 0)
            {

                _errorState = 1;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Error;
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.Value = 2;
                //timer.Stop();
                StopTimer();
            }
            if (e.ProgressPercentage == 2)
            {
                ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                ProgressBarSearch.Text = @"و پرکردن جداول اصلی EMIS مرحله دوم - دریافت اطلاعات از ";
            }
            if (e.ProgressPercentage == 3)
            {
                ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                ProgressBarSearch.Text = @"مرحله سوم - محاسبه پارامترها";
            }
            if (e.ProgressPercentage == 4)
            {
                ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                ProgressBarSearch.Text = @"مرحله چهارم - ایجاد جدول خروجی";
            }
            if (e.ProgressPercentage == 5)
            {
                ProgressBarSearch.ProgressType = eProgressItemType.Marquee;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Normal;
                ProgressBarSearch.Text = @"مرحله پنجم - تولید فایلهای اکسل";
            }
           

            if (e.ProgressPercentage == 11)
            {
                ProgressBarSearch.Text = @"اتمام مرحله اول ";
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                ProgressBarSearch.Value = 2;
            }
            if (e.ProgressPercentage == 21)
            {
                ProgressBarSearch.Text = @"اتمام مرحله دوم ";
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                ProgressBarSearch.Value = 2;
            }
            if (e.ProgressPercentage == 31)
            {
                ProgressBarSearch.Text = @"اتمام مرحله سوم ";
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                ProgressBarSearch.Value = 2;
            }
            if (e.ProgressPercentage == 41)
            {
                ProgressBarSearch.Text = @"اتمام مرحله چهارم ";
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                ProgressBarSearch.Value = 2;
            }
            if (e.ProgressPercentage == 51)
            {
                ProgressBarSearch.Text = @"اتمام مرحله پنجم 6";
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                ProgressBarSearch.Value = 2;
            }
            if (e.ProgressPercentage == 61)
            {
                ProgressBarSearch.Text = @"اتمام صدور صورتحساب ";
                ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                ProgressBarSearch.Value = 2;
            }
        }

        #endregion

        #region BW_RunWorkerCompleted

        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (e.Cancelled)
            {
                ProgressBarSearch.Value = 0;
                ProgressBarSearch.ColorTable = eProgressBarItemColor.Error;
                ProgressBarSearch.Text = @"خطا  ";
            }
            else
            {

                //timer.Stop();
                StopTimer();
                if (_btnselect == "B")
                {
                    ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                    ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                    ProgressBarSearch.Value = 2;
                    ProgressBarSearch.Text = @"اتمام شماره زنی";
                }

                if (_btnselect == "S")
                {
                    ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                    ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                    ProgressBarSearch.Value = 2;
                    ProgressBarSearch.Text = @"EMS اتمام ذخیره سازی در";
                }

                if (_btnselect == "E" && _ptypeE == @"Fuel")
                {
                    ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                    ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                    ProgressBarSearch.Value = 2;
                    ProgressBarSearch.Text = @"اتمام صدور صورتحساب";
                }

                if (_btnselect == "P")
                {
                    ProgressBarSearch.ProgressType = eProgressItemType.Standard;
                    ProgressBarSearch.ColorTable = eProgressBarItemColor.Paused;
                    ProgressBarSearch.Value = 2;
                    ProgressBarSearch.Text = @"اتمام پرینت صورتحسابها";
                }
            }
        }

        #endregion

        #region Printing

        private void PrintingProcess()
        {
            btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = true; });
            richTextBoxLog.BeginInvoke((MethodInvoker) delegate() { richTextBoxLog.Visible = true; });
            panelLog.BeginInvoke((MethodInvoker) delegate() { panelLog.Visible = true; });

            _logger.AddToLogAndShow("\n********************** STAGE 8 **********************", LogStyle.InfoStyle);
            _logger.AddToLogAndShow("Printing excel files", LogStyle.InfoStyle);


            CultureInfo tempCultureInfo = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            //Fix error "old format or invalid type library excel"

            try
            {
                if (_stype == "قطعی-گزارش سالانه") //1395/01/15 By Abdollahzadeh
                {
                    switch (_ptypeE)
                    {

                        case "Sellers":
                            PrintYearlySellerExcels();
                            break;
                        case "Buyers":
                            PrintYearlyBuyerExcels();
                            break;

                        case "Transfer":
                            PrintYearlyTransferExcels();
                            break;


                    }
                }
                else
                {
                    switch (_ptypeE)
                    {

                        case "Sellers":

                            #region BillPrint

                            _excelPath = @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) +
                                         _sDate.Substring(5, 2) + @"\Seller\BillPrint";
                            _fileInFolderName1 = Directory.GetFiles(_excelPath);
                            foreach (String fileN in _fileInFolderName1)
                            {
                                if (_bw.CancellationPending)
                                {
                                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                    break;
                                }

                                #region print

                                _logger.AddToLogAndShow("Printing file =" + fileN, LogStyle.InfoStyle);

                                var excelApp = new Excel.Application {Visible = false};
                                var wk = excelApp.Workbooks.Open(fileN, 0, Type.Missing, 5, Type.Missing, Type.Missing,
                                    true,
                                    Excel.XlPlatform.xlWindows, "\\t", false, false, 0, true, true, Type.Missing);

                                int sheetIndex = 0;

                                foreach (Excel.Worksheet sheet in wk.Sheets)
                                {

                                    sheetIndex = sheetIndex + 1;
                                    if (sheet.Name == "صورتحساب")
                                    {
                                        break;
                                    }

                                }
                                var wt = (Excel.Worksheet) wk.Worksheets[sheetIndex];

                                wt.PrintOut(Type.Missing, Type.Missing, _printingForm.NumberOfCopy, Type.Missing,
                                    _printingForm.PrinterName, Type.Missing, Type.Missing, Type.Missing);

                                wk.Close(false, null, null);
                                excelApp.Quit();

                                releaseObject(wk);
                                releaseObject(wt);
                                releaseObject(excelApp);

                                #endregion


                            }

                            #endregion

                            break;

                        case "Buyers":

                            #region Buyers

                            _excelPath = @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                         @"\Buyer\Billprint";
                            _fileInFolderName1 = Directory.GetFiles(_excelPath);

                            foreach (String fileN in _fileInFolderName1)
                            {
                                if (_bw.CancellationPending)
                                {
                                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                    break;
                                }

                                #region print

                                _logger.AddToLogAndShow("Printing file =" + fileN, LogStyle.InfoStyle);

                                var excelApp = new Excel.Application {Visible = false};
                                var wk = excelApp.Workbooks.Open(fileN, 0, Type.Missing, 5, Type.Missing, Type.Missing,
                                    true,
                                    Excel.XlPlatform.xlWindows, "\\t", false, false, 0, true, true, Type.Missing);

                                int sheetIndex = 0;

                                foreach (Excel.Worksheet sheet in wk.Sheets)
                                {

                                    sheetIndex = sheetIndex + 1;
                                    if (sheet.Name == "صورتحساب")
                                    {
                                        break;
                                    }

                                }

                                var wt = (Excel.Worksheet) wk.Worksheets[sheetIndex];

                                wt.PrintOut(Type.Missing, Type.Missing, _printingForm.NumberOfCopy, Type.Missing,
                                    _printingForm.PrinterName, Type.Missing, Type.Missing, Type.Missing);

                                wk.Close(false, null, null);
                                excelApp.Quit();

                                releaseObject(wk);
                                releaseObject(wt);
                                releaseObject(excelApp);

                                #endregion
                            }

                            #endregion


                            break;

                        case "Transfer":

                            #region Transfer

                            _excelPath = @"D:\Settlement\Out\Monthly\" + _sDate.Substring(2, 2) + _sDate.Substring(5, 2) +
                                         @"\Transfer\BillPrint";
                            _fileInFolderName1 = Directory.GetFiles(_excelPath);

                            foreach (String fileN in _fileInFolderName1)
                            {
                                if (_bw.CancellationPending)
                                {
                                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                    break;
                                }

                                #region print

                                _logger.AddToLogAndShow("Printing file =" + fileN, LogStyle.InfoStyle);

                                var excelApp = new Excel.Application {Visible = false};
                                var wk = excelApp.Workbooks.Open(fileN, 0, Type.Missing, 5, Type.Missing, Type.Missing,
                                    true,
                                    Excel.XlPlatform.xlWindows, "\\t", false, false, 0, true, true, Type.Missing);

                                int sheetIndex = 0;

                                foreach (Excel.Worksheet sheet in wk.Sheets)
                                {

                                    sheetIndex = sheetIndex + 1;
                                    if (sheet.Name == "صورتحساب")
                                    {
                                        break;
                                    }

                                }

                                var wt = (Excel.Worksheet) wk.Worksheets[sheetIndex];

                                wt.PrintOut(Type.Missing, Type.Missing, _printingForm.NumberOfCopy, Type.Missing,
                                    _printingForm.PrinterName, Type.Missing, Type.Missing, Type.Missing);



                                wk.Close(false, null, null);
                                excelApp.Quit();

                                releaseObject(wk);
                                releaseObject(wt);
                                releaseObject(excelApp);

                                #endregion

                            }

                            #endregion

                            break;

                    }
                }

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }

            Thread.CurrentThread.CurrentCulture = tempCultureInfo;
            _logger.AddToLogAndShow("____________________________________________________________________",
                LogStyle.InfoStyle);

            btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = false; });
        }

        #endregion

        #region int GetBillItemsCount()

        private int GetBillItemsCount(string ptypeE, string taz, string unitPower1)
        {
            Int32 returnValue = 0;
            switch (ptypeE)
            {
                case "Sellers":
                    if (taz == "S")
                    {
                        _query = (from var in _dbSm.Tbl_billItems
                            where var.Subject == 'S' && var.Unit_Power == Convert.ToChar(unitPower1)
                            select var);
                        returnValue = _query.Count();
                    }
                    if (taz == "Z")
                    {
                        _query = (from var in _dbSm.Tbl_billItems
                            where var.Subject == 'Z'
                            select var);
                        returnValue = _query.Count();
                    }
                    break;
                case "Buyers":
                    _query = (from var in _dbSm.Tbl_billItems
                        where var.Subject == 'B'
                        select var);
                    returnValue = _query.Count();
                    break;
                case "Transfer":
                    _query1 = (from var in _dbSm.Tbl_billItemsTDs
                        where var.Subject == 'D'
                        select var);
                    returnValue = _query1.Count();
                    break;

                case "PowerPlant":
                    _query = (from var in _dbSm.Tbl_billItems
                        where var.Subject == 'P'
                        select var);
                    returnValue = _query.Count();
                    break;
            }
            return returnValue;
        }

        #endregion

        #region InitialFixedParam

        private void InitialFixedParameter()
        {
            var i = 0;
            _selectString = "";
            _dbSm.SP_TruncateDetails(_ptypeE);

            try
            {
                btnStopStage.BeginInvoke((MethodInvoker) delegate() { btnStopStage.Enabled = true; });
                richTextBoxLog.BeginInvoke((MethodInvoker) delegate() { richTextBoxLog.Visible = true; });
                panelLog.BeginInvoke((MethodInvoker) delegate() { panelLog.Visible = true; });
                _logger.AddToLogAndShow("Initialing Fixed Parameter : ", LogStyle.InfoStyle);
                switch (_ptypeE)
                {
                    case "Sellers":

                        #region Run Sellers SpInitParam

                        if (_btype == "روزانه")
                        {
                            #region Company

                            if (lstPowerStations.List.CheckedItems.Count == 0)
                            {
                                _logger.AddToLogAndShow("Companies : ", LogStyle.InfoStyle);
                                foreach (var x in lstSellerList.List.CheckedItems)
                                {

                                    if (lstSellerList.List.CheckedItems.Count > 0)
                                    {
                                        ///////////////////////////////////////////
                                        if (_bw.CancellationPending)
                                        {
                                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                            break;
                                        }
                                        _logger.AddToLogAndShow(lstSellerList.List.CheckedItems[i].ToString(),
                                            LogStyle.InfoStyle);
                                        _dbSm.SP_InitialFixedParam(_sDate, _eDate, //roozaneh
                                            lstSellerList.List.CheckedItems[i].ToString(), "",
                                            "C", "S");

                                        i = i + 1;
                                    }

                                }
                            }
                                #endregion

                                #region PowerPlant

                            else
                            {
                                _logger.AddToLogAndShow("Power Stations : ", LogStyle.InfoStyle);
                                foreach (var x in lstPowerStations.List.CheckedItems)

                                {
                                    if (lstPowerStations.List.CheckedItems.Count > 0)
                                    {
                                        ///////////////////////////////////////////
                                        if (_bw.CancellationPending)
                                        {
                                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                            break;
                                        }
                                        _logger.AddToLogAndShow(lstPowerStations.List.CheckedItems[i].ToString(),
                                            LogStyle.InfoStyle);

                                        _dbSm.SP_InitialFixedParam(_sDate, _eDate, "", //roozaneh
                                            lstPowerStations.List.CheckedItems[i].ToString(), "P",
                                            "S");
                                        i = i + 1;
                                    }

                                }
                            }

                            #endregion

                        }
                        if (_btype == "ماهيانه")
                        {
                            #region Company

                            if (lstPowerStations.List.CheckedItems.Count == 0)
                            {
                                _logger.AddToLogAndShow("Companies : ", LogStyle.InfoStyle);
                                foreach (var x1 in lstSellerList.List.CheckedItems)
                                {
                                    if (lstSellerList.List.CheckedItems.Count > 0)
                                    {
                                        ///////////////////////////////////////////
                                        if (_bw.CancellationPending)
                                        {
                                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                            break;
                                        }
                                        _logger.AddToLogAndShow(lstSellerList.List.CheckedItems[i].ToString(),
                                            LogStyle.InfoStyle);

                                        _dbSm.SP_InitialFixedParam(_sDate, _eDate,
                                            lstSellerList.List.CheckedItems[i].ToString().
                                                TrimStart().TrimEnd(), "", "C", "S");

                                        i = i + 1;
                                    }

                                }
                            }
                                #endregion

                                #region PowerPlant

                            else
                            {
                                _logger.AddToLogAndShow("Power Stations : ", LogStyle.InfoStyle);
                                foreach (var x in lstPowerStations.List.CheckedItems)

                                {
                                    if (lstPowerStations.List.CheckedItems.Count > 0)
                                    {
                                        if (lstPowerStations.List.CheckedItems.Count > 0)
                                        {
                                            ///////////////////////////////////////////
                                            if (_bw.CancellationPending)
                                            {
                                                _logger.AddToLogAndShow("Operation canceled by user",
                                                    LogStyle.ErrorStyle);
                                                break;
                                            }
                                            _logger.AddToLogAndShow(lstPowerStations.List.CheckedItems[i].ToString(),
                                                LogStyle.InfoStyle);

                                            _dbSm.SP_InitialFixedParam(_sDate, _eDate, "",
                                                lstPowerStations.List.CheckedItems[i].ToString(), "P",
                                                "S");
                                            i = i + 1;
                                        }

                                    }
                                }
                            }

                            #endregion

                        }
                        if (_btype == "قطعي")
                        {
                            #region Company

                            if (lstPowerStations.List.CheckedItems.Count == 0)
                            {
                                _logger.AddToLogAndShow("Companies : ", LogStyle.InfoStyle);
                                foreach (var x1 in lstSellerList.List.CheckedItems)
                                {
                                    if (lstSellerList.List.CheckedItems.Count > 0)
                                    {
                                        ///////////////////////////////////////////
                                        if (_bw.CancellationPending)
                                        {
                                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                            break;
                                        }
                                        _logger.AddToLogAndShow(lstSellerList.List.CheckedItems[i].ToString(),
                                            LogStyle.InfoStyle);


                                        _dbSm.SP_InitialFixedParam(_sDate, _eDate,
                                            lstSellerList.List.CheckedItems[i].ToString().
                                                TrimStart().TrimEnd(), "", "C", "S");

                                        i = i + 1;
                                    }

                                }
                            }
                                #endregion
                   
                                #region PowerPlant

                            else
                            {
                                _logger.AddToLogAndShow("Power Stations : ", LogStyle.InfoStyle);
                                foreach (var x in lstPowerStations.List.CheckedItems)

                                {
                                    if (lstPowerStations.List.CheckedItems.Count > 0)
                                    {
                                        ///////////////////////////////////////////
                                        if (_bw.CancellationPending)
                                        {
                                            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                            break;
                                        }
                                        _logger.AddToLogAndShow(lstPowerStations.List.CheckedItems[i].ToString(),
                                            LogStyle.InfoStyle);
                                        _dbSm.SP_InitialFixedParam(_sDate, _eDate, "",
                                            lstPowerStations.List.CheckedItems[i].ToString(), "P",
                                            "S");
                                        i = i + 1;
                                    }

                                }
                            }

                            #endregion
                        }

                        #endregion

                        break;

                    case "Buyers":

                        #region Run Buyers SpInitParam

                        _logger.AddToLogAndShow("Companies : ", LogStyle.InfoStyle);
                        foreach (var x in lstSellerList.List.CheckedItems)
                        {
                            if (lstSellerList.List.CheckedItems.Count > 0)
                            {
                                ///////////////////////////////////////////
                                if (_bw.CancellationPending)
                                {
                                    _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                    break;
                                }
                                _logger.AddToLogAndShow(lstSellerList.List.CheckedItems[i].ToString(),
                                    LogStyle.InfoStyle);
                                _dbSm.SP_InitialFixedParamB(_sDate, _eDate,
                                    lstSellerList.List.CheckedItems[i].ToString());
                                i = i + 1;
                            }

                        }

                        #endregion

                        break;
                    case "Transfer":

                        #region Run Transfer SpInitParam

                          _globalSqlCmd = new SqlCommand("Transfer.SP_InitialFixedParamT ", _cnn)
                        {
                            CommandTimeout = 2000,
                            CommandType = CommandType.StoredProcedure
                        };
                        _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);
                        _globalSqlCmd.Parameters.AddWithValue("@EndDate", _eDate);

                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }


                        //_logger.AddToLogAndShow("Companies : ", LogStyle.InfoStyle);

                        //PersianDate od = PersianDateConverter.ToPersianDate(DateTime.Now);
                        //string cdatet = od.ToString();
                        //var iii = cdatet.Substring(0, 10) + "-" + PersianDateConverter.ToPersianDate
                        //    (DateTime.Now).Time + ".00";

                      


                        ////if (_billExecT._Step1)
                        ////{
                        //_dbSm.CommandTimeout = 10000;
                        //_dbSm.SP_InitialParamTF(_sDate, _eDate);
                        ////}

                        //foreach (var x in lstSellerList.List.CheckedItems)
                        //{
                        //    if (lstSellerList.List.CheckedItems.Count > 0)
                        //    {
                        //        _dbSm.CommandTimeout = 10000;
                        //        //    _dbSm.SP_InitialParamTF2();

                        //        #region intial Company

                        //        ///////////////////////////////////////////
                        //        if (_bw.CancellationPending)
                        //        {
                        //            _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                        //            break;
                        //        }
                        //        _logger.AddToLogAndShow(lstSellerList.List.CheckedItems[i].ToString(),
                        //            LogStyle.InfoStyle);
                        //        try
                        //        {
                        //            _dbSm.CommandTimeout = 2000;
                        //            _dbSm.SP_InitialParamT(_sDate, _eDate,
                        //                lstSellerList.List.CheckedItems[i].ToString());

                        //        }
                        //            #region Catch

                        //        catch (Exception et)
                        //        {
                        //            _logger.AddToLogAndShow(et.Message, LogStyle.ErrorStyle);
                        //            const String errorMessage =
                        //                "امكان خواندن اطلاعات صورت حساب از بانك وجود ندارد.\n" +
                        //                "موارد زیر را بررسی نمایید:\n" +
                        //                "1. آیا ارتباط شما با بانك اطلاعات برقرار است و شبكه متصل می باشد؟";
                        //            MessageBox.Show(errorMessage, @"خطا!");
                        //            MessageBox.Show(et.Message);
                        //            return;
                        //        }

                        //        #endregion

                        //        #endregion
                        //    }

                        //    i = i + 1;

                        //}

                        #endregion

                        break;
                }
                btnStopStage.BeginInvoke((MethodInvoker) delegate() { btnStopStage.Enabled = false; });
                _logger.AddToLogAndShow("____________________________________________________________________",
                    LogStyle.InfoStyle);
            }
            catch (Exception ex)
            {
                _cnn.Close();
                _logger.AddToLogAndShow(ex);
                _bw.ReportProgress(0);
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("خطا در اجرای مرحله اول - آماده سازی جدول اصلی", strMessage, "Error")
                    .ShowDialog();

            }
        }


        #endregion

        #region FillFixedParam

        private void FillFixedParameter()
        {
            try
            {
                btnStopStage.BeginInvoke((MethodInvoker) delegate() { btnStopStage.Enabled = true; });
                richTextBoxLog.BeginInvoke((MethodInvoker) delegate() { richTextBoxLog.Visible = true; });
                panelLog.BeginInvoke((MethodInvoker) delegate() { panelLog.Visible = true; });

                switch (_ptypeE)
                {
                    case "Sellers":

                        #region Run Sellers SpFixParam


                        if (_btype == "روزانه")
                        {

                            //_dbSm.CommandTimeout = 10000;
                            //_dbSm.SP_FillFixedParamD(_sDate, _sDate);
                            //_dbSm.SP_CreateBillText();
                            //_dbSm.SP_PriceStep(_sDate);


                            _globalSqlCmd = new SqlCommand("Sellers.SP_FillFixedParamD ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };
                            _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);
                            _globalSqlCmd.Parameters.AddWithValue("@EndDate", _eDate); //roozaneh


                            //_cnn.Open();
                            //_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();
                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }
                            //////////////////////////////
                            _globalSqlCmd = new SqlCommand("Sellers.SP_CreateBillText ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };


                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }
                            //_cnn.Open();
                            //_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();
                            /////////////////////////////////
                            _globalSqlCmd = new SqlCommand("Sellers.SP_PriceStep ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };
                            _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);

                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }
                            //_cnn.Open();
                            //_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();


                        }
                        if (_btype == "ماهيانه")
                        {

                            //_dbSm.CommandTimeout = 10000;
                            //_dbSm.SP_FillFixedParam(_sDate, _eDate);
                            //_dbSm.SP_CreateBillText();
                            //_dbSm.SP_PriceStep(_sDate);

                            _globalSqlCmd = new SqlCommand("Sellers.SP_FillFixedParam ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };

                            _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);
                            _globalSqlCmd.Parameters.AddWithValue("@EndDate", _eDate);

                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }
                            //_cnn.Open();
                            //_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();
                            //////////////////////////////
                            _globalSqlCmd = new SqlCommand("Sellers.SP_CreateBillText ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };

                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }

                            //_cnn.Open();
                            //_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();
                            /////////////////////////////////
                            _globalSqlCmd = new SqlCommand("Sellers.SP_PriceStep ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };
                            _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);

                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }

                            //_cnn.Open();
                            //_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();


                        }
                        if (_btype == "قطعي")
                        {

                            //_dbSm.CommandTimeout = 10000;
                            //_dbSm.SP_FillFixedParam(_sDate, _eDate);
                            //_dbSm.SP_CreateBillText();
                            //_dbSm.SP_PriceStep(_sDate);
                            _globalSqlCmd = new SqlCommand("Sellers.SP_FillFixedParam ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };

                            _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);
                            _globalSqlCmd.Parameters.AddWithValue("@EndDate", _eDate);


                            // _cnn.Open();
                            //commandResult = _globalSqlCmd.ExecuteNonQuery();
                            // _cnn.Close();
                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }
                            //////////////////////////////
                            _globalSqlCmd = new SqlCommand("Sellers.SP_CreateBillText ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };



                            //_cnn.Open();
                            //commandResult =_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();
                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }
                            /////////////////////////////////
                            _globalSqlCmd = new SqlCommand("Sellers.SP_PriceStep ", _cnn)
                            {
                                CommandTimeout = 10000,
                                CommandType = CommandType.StoredProcedure
                            };
                            _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);


                            //_cnn.Open();
                            //commandResult =_globalSqlCmd.ExecuteNonQuery();
                            //_cnn.Close();
                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }
                        }

                        #endregion

                        break;

                    case "Buyers":

                        #region Run Buyers SpFixParam

                        //_dbSm.CommandTimeout = 2000;
                        //_dbSm.SP_FillFixedParamB(_sDate, _eDate, _weekBDate);

                        _globalSqlCmd = new SqlCommand("Buyers.SP_FillFixedParamB ", _cnn)
                        {
                            CommandTimeout = 2000,
                            CommandType = CommandType.StoredProcedure
                        };
                        _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);
                        _globalSqlCmd.Parameters.AddWithValue("@EndDate", _eDate);
                        _globalSqlCmd.Parameters.AddWithValue("@WeekB", _weekBDate);

                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }

                        #endregion

                        break;
                    case "Transfer":

                        #region Run Transfer SpFixParam

                        //_globalSqlCmd = new SqlCommand("Transfer.SP_FillFixedParamT ", _cnn)
                        //{
                        //    CommandTimeout = 2000,
                        //    CommandType = CommandType.StoredProcedure
                        //}; //1395/05/11
                        _globalSqlCmd = new SqlCommand("Transfer.SP_FillFixedParamT_New ", _cnn)
                        {
                            CommandTimeout = 2000,
                            CommandType = CommandType.StoredProcedure
                        };
                        _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);
                        _globalSqlCmd.Parameters.AddWithValue("@EndDate", _eDate);

                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }
                        break;

                        #endregion
                }
                btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = false; });
            }
            catch (Exception ex)
            {
                _cnn.Close();
                _logger.AddToLogAndShow(ex);
                _bw.ReportProgress(0);
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox(
                    "و پرکردن جداول اصلی" + " EMIS " + "خطا در اجرای مرحله دوم - دریافت اطلاعات از ", strMessage,
                    "Error").ShowDialog();
            }
        }

        #endregion

        #region CreateTable

        private void CreateTable()
        {
            try
            {
                btnStopStage.BeginInvoke((MethodInvoker) delegate() { btnStopStage.Enabled = true; });
                richTextBoxLog.BeginInvoke((MethodInvoker) delegate() { richTextBoxLog.Visible = true; });
                panelLog.BeginInvoke((MethodInvoker) delegate() { panelLog.Visible = true; });
                switch (_ptypeE)
                {
                    case "Sellers":

                        #region Run Sellers SpFixParam

                        _globalSqlCmd = new SqlCommand("Sellers.SP_CreateTableS ", _cnn)
                        {
                            CommandTimeout = 10000,
                            CommandType = CommandType.StoredProcedure
                        };

                        _globalSqlCmd.Parameters.AddWithValue("@Cdate", _sDate);
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();

                        //if (_btype == "روزانه")
                        //{

                        //    _dbSm.CommandTimeout = 10000;

                        //    _dbSm.SP_CreateTableS(_sDate);
                        //}
                        //if (_btype == "ماهيانه")
                        //{

                        //    _dbSm.CommandTimeout = 10000;

                        //    _dbSm.SP_CreateTableS(_sDate);

                        //}
                        //if (_btype == "قطعي")
                        //{

                        //    _dbSm.CommandTimeout = 10000;

                        //    _dbSm.SP_CreateTableS(_sDate);

                        //}

                        #endregion

                        break;

                    case "Buyers":

                        #region Run Buyers SpFixParam

                        _globalSqlCmd = new SqlCommand("Buyers.SP_CreateTableB ", _cnn)
                        {
                            CommandTimeout = 2000,
                            CommandType = CommandType.StoredProcedure
                        };
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        //_dbSm.CommandTimeout = 2000;
                        //_dbSm.SP_CreateTableB();

                        #endregion

                        break;
                    case "Transfer":

                        #region Run Transfer SpFixParam

                        _globalSqlCmd = new SqlCommand("Transfer.SP_CreateTableT ", _cnn)
                        {
                            CommandTimeout = 1000,
                            CommandType = CommandType.StoredProcedure
                        };
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }
                        break;

                        #endregion
                }
                btnStopStage.BeginInvoke((MethodInvoker) delegate() { btnStopStage.Enabled = false; });
            }
            catch (Exception ex)
            {
                _cnn.Close();
                _logger.AddToLogAndShow(ex);
                _bw.ReportProgress(0);
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("خطا در اجرای مرحله سوم - محاسبه پارامترها", strMessage, "Error")
                    .ShowDialog();
            }

        }

        #endregion

        #region   MakeTable2()

        private void MakeTable2()
        {
            try
            {
                btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = true; });
                panelLog.BeginInvoke((MethodInvoker) delegate() { panelLog.Visible = true; });
                richTextBoxLog.BeginInvoke((MethodInvoker) delegate() { richTextBoxLog.Visible = true; });

                _logger.AddToLogAndShow("Saving BillData", LogStyle.InfoStyle);

                switch (_ptypeE)
                {
                    case "Sellers":

                        #region Sellers


                        #region Unit

                        #region Normal

                        #region Make Table Fixed Items


                        _selectString = "IF OBJECT_ID('Sellers.BillDataU', 'U') IS NOT NULL " +
                                        "DROP TABLE Sellers.BillDataU; " +
                                        "CREATE TABLE Sellers.[BillDataU] ( " +
                                        "[RegionID] [int] ," +
                                        "[RegionName] [nvarchar](50) , " +
                                        "[CompanyTypeName] [nvarchar](50) , " +
                                        "[CompanyID] [int] , " +
                                        "[CompanyCode] [nvarchar](5) ," +
                                        "[CompanyName] [nvarchar](50) , " +
                                        "[CompanyCodeE] [nvarchar](5) ," +
                                        "[CompanyNameE] [nvarchar](50) , " +
                                        "[PowerStationID] [int] ," +
                                        "[PowerStationCode] [varchar](10) , " +
                                        "[PowerStationName] [nvarchar](50) , " +
                                        "[PowerStationLatinName] [nvarchar](50) ," +
                                        "[MarketStateCode]  [nvarchar](5) ," +
                                        "[UnitType] [nvarchar](50) , " +
                                        "[UnitID] [int] ," +
                                        "[UnitCode][nvarchar](5)," +
                                        "[BlockID] [int] ," + // 94/08/26
                                        "[BlockCode] [nchar](10) ," + // 94/08/26
                                        "[BillUnitT] [varchar](MAX) ," +
                                        "[BillUnitID] [int] ," +
                                        "[Date] [varchar](50) , " +
                                        "[Hour] [tinyint] ,";

                        #endregion

                        #region Make BillItems Columns

                        _itemsCount = GetBillItemsCount(_ptypeE, "S", "U");

                        for (int i = 0; i < _itemsCount; i++)
                            if (i == _itemsCount - 1)
                                _selectString += "Field" + (i + 1) + " numeric(28, 16) DEFAULT 0,";
                            else
                                _selectString += "Field" + (i + 1) + " numeric(28, 16) DEFAULT 0, ";

                        #endregion


                        _selectString += "[AccountingCode] [Varchar](10),MN [Varchar](1))";

                        #region ExecuteSelectString

                        //try
                        //{
                        //    Parameters
                        //        .ExecuteSelectString(_selectString);
                        //}
                        //catch (Exception ex)
                        //{
                        //    MessageBox.Show(ex.Message);
                        //}
                        _globalSqlCmd = new SqlCommand(_selectString, _cnn) {CommandTimeout = 1000};
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }

                        #endregion

                        #region Insert Fixed Data Items

                        _selectString = " INSERT INTO Sellers.[BillDataU]  " +
                                        "([RegionID],[RegionName]," +
                                        "[CompanyTypeName],[CompanyID],[CompanyCode],[CompanyName],[CompanyCodeE],[CompanyNameE]," +
                                        "[PowerStationID],[PowerStationCode],[PowerStationName],[PowerStationLatinName],[MarketStateCode]," +
                                        "[UnitType],[UnitID],[UnitCode],[BlockID],[BlockCode],[BillUnitT],[BillUnitID],[Date],[Hour]) " +
                                        "(SELECT [RegionID],[RegionName]," +
                                        "[CompanyTypeName],[CompanyID],[CompanyCode],[CompanyName],[CompanyCode],[CompanyName], " +
                                        "[PowerStationID],[PowerStationCode],[PowerStationName],[PowerStationLatinName],[MarketStateCode]," +
                                        "[UnitType],[UnitID],[UnitCode],[BlockID],[BlockCode],[BillUnitT],[BillUnitID],[Date],[Hour] " +
                                        "FROM [Sellers].[FixedParameters] " +
                                        " )";


                        #endregion

                        #region Execute


                        if (_btype == "روزانه")
                        {

                            _selectString += " EXEC Sellers.SP_SaveBillDataU 'D' ;";
                        }
                        if (_btype == "ماهيانه")
                        {

                            _selectString += " EXEC Sellers.SP_SaveBillDataU 'M' ;";
                        }
                        if (_btype == "قطعي")
                        {

                            _selectString += " EXEC Sellers.SP_SaveBillDataU 'M' ;";
                        }

                        #endregion

                        #region ExecuteSelectString

                        //try
                        //{
                        //    Parameters
                        //        .ExecuteSelectString(_selectString);
                        //}
                        //catch (Exception ex)
                        //{
                        //    MessageBox.Show(ex.Message);
                        //}
                        _globalSqlCmd = new SqlCommand(_selectString, _cnn) {CommandTimeout = 1500};
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }

                        if (_billTypeCode == "D" && String.Compare(_sDate, _eDate, StringComparison.Ordinal) < 0)
                            //roozaneh be soorate baze'i
                        {
                            string query = " IF OBJECT_ID('[Sellers].[BillDataUD]') IS NOT NULL "
                                           + "BEGIN DROP TABLE [Sellers].[BillDataUD]; END "
                                           + "SELECT * INTO [Sellers].[BillDataUD] FROM [Sellers].[BillDataU]";
                            _globalSqlCmd = new SqlCommand(query, _cnn) {CommandTimeout = 1000};
                            ExecuteGlobalCmd();
                        }

                        #endregion

                        #endregion

                        #endregion

                        #endregion

                        break;

                    case "Buyers":

                        #region Buyers

                        #region Make Table Fixed Items

                        _selectString = "IF OBJECT_ID('Buyers.BillData', 'U') IS NOT NULL " +
                                        "DROP TABLE Buyers.BillData; " +
                                        "CREATE TABLE Buyers.[BillData] ( " +
                                        "[AccountingCode] [nvarchar](10)," +
                                        "[CompanyTypeName] [nvarchar](50)," +
                                        "[CompanyID] [int] ," +
                                        "[CompanyCode] [nvarchar](5)," +
                                        "[CompanyName] [nvarchar](50) , " +
                                        "[Date] [varchar](50) NULL, " +
                                        "[Hour] [tinyint] NULL ,";

                        #endregion

                        #region Make BillItems Columns

                        _itemsCount = GetBillItemsCount(_ptypeE, "B", "");

                        for (int i = 0; i < _itemsCount; i++)
                            if (i == _itemsCount - 1)
                                _selectString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0);";
                            else
                                _selectString += "Field" + (i + 1) + " numeric(18, 4) DEFAULT 0, ";

                        #endregion

                        #region ExecuteSelectString

                        //try
                        //{
                        //    Parameters
                        //        .ExecuteSelectString(_selectString);
                        //}
                        //catch (Exception ex)
                        //{
                        //    MessageBox.Show(ex.Message);
                        //}
                        _globalSqlCmd = new SqlCommand(_selectString, _cnn) {CommandTimeout = 1000};
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }

                        #endregion

                        #region Insert Fixed Data Items

                        _selectString = " INSERT INTO Buyers.[BillData] " +
                                        "([AccountingCode],[CompanyTypeName],CompanyID,[CompanyCode],[CompanyName] " +
                                        ",[Date] , [Hour]) " +
                                        "(SELECT " +
                                        "Null,CompanyTypeName,CompanyID,CompanyCode,CompanyName," +
                                        "Date, Hour " +
                                        "FROM [Buyers].[FixedParameters])";

                        #endregion

                        #region Execute

                        _selectString += "EXEC Buyers.SP_SaveBillData;";

                        #endregion

                        #region ExecuteSelectString

                        //try
                        //{
                        //    Parameters
                        //        .ExecuteSelectString(_selectString);
                        //}
                        //catch (Exception ex)
                        //{
                        //    MessageBox.Show(ex.Message);
                        //}
                        _globalSqlCmd = new SqlCommand(_selectString, _cnn) {CommandTimeout = 1000};
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }
                        long tarazValue =
                            Convert.ToInt64(DbUtility.GetScalar("SELECT  expr31  From  [Buyers].[View_TarazSUM]"));

                        if (Math.Abs(tarazValue) <= 100)
                        {
                            _logger.AddToLogAndShow("مبلغ تراز (ریال) : " + tarazValue.ToString(), LogStyle.WarningStyle);
                            try
                            {
                                SmsUtility.SendSms(_sDate, _billTypeCode, SmsType.Taraz);
                            }
                            catch (Exception ex)
                            {
                                _logger.AddToLogAndShow("خطا در ارسال پیامک:", LogStyle.ErrorStyle);
                                _logger.AddToLogAndShow(ex.Message, LogStyle.ErrorStyle);
                                return;
                            }
                        }
                        else
                        {
                            _logger.AddToLogAndShow("مبلغ تراز (ریال) : " + tarazValue.ToString(), LogStyle.ErrorStyle);
                        }


                        #endregion

                        #endregion

                        break;

                    case "Transfer":

                        #region Transfer

                        #region Make Table Fixed Items

                        _selectString = "";
                        _selectString += "IF OBJECT_ID('Transfer.BillData', 'U') IS NOT NULL " +
                                         "DROP TABLE Transfer.BillData; " +
                                         "CREATE TABLE Transfer.[BillData] ( " +
                                         "[CompanyID] [int] ," +
                                         "[CompanyCode] [nvarchar](5) ," +
                                         "[CompanyName] [nvarchar](50) , " +
                                         "[Date] [varchar](12) , " +
                                         "[Hour] [tinyint] ," +
                                         "[EquipmentName] [nvarchar](101) ," +
                                         // new column EquipmentName 1394/09/09
                                         "[EquipmentCode] [nvarchar](50) ," +
                                         "[EquipmentType] [nchar](10) ," +
                                         "[EquipmentPosition] [nchar](10) ," +
                                         "[VoltageLevel] [numeric](4, 1) ," +
                                         "[PGDS_Id] [int] ,"; //PGDS_Id 1396/07/01

                        #endregion

                        #region Make BillItems Columns

                        _itemsCount = GetBillItemsCount(_ptypeE, "T", "");

                        for (int it = 0; it < _itemsCount; it++)
                        {
                            if (it == _itemsCount - 1)
                            {
                                _selectString += "Field" + (it + 1) + " numeric(18, 4) DEFAULT 0);";
                            }
                            else
                            {
                                if (it == 3)
                                {
                                    _selectString += "Field" + (it + 1) + " nvarchar(50) ,";
                                }
                                else
                                {
                                    _selectString += "Field" + (it + 1) + " numeric(18, 4) DEFAULT 0, ";
                                }

                            }
                        }

                        #endregion

                        #region ExecuteSelectString

                        //try
                        //{
                        //    Parameters
                        //        .ExecuteSelectString(_selectString);
                        //}
                        //catch (Exception ex)
                        //{
                        //    logger.AddToLogAndShow(ex.Message, LogStyle.ErrorStyle, richTextBoxLog);
                        //    //MessageBox.Show(ex.Message);;
                        //}
                        _globalSqlCmd = new SqlCommand(_selectString, _cnn) {CommandTimeout = 1000};
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }

                        #endregion

                        #region Insert Fixed Data Items

                        //PGDS_Id 1396/07/01
                        _selectString = " INSERT INTO [Transfer].[BillData] " +
                                        " ([CompanyID],[CompanyCode],[CompanyName],[Date]," +
                                        " [Hour],[EquipmentName],[EquipmentCode],[EquipmentType]," +
                                        // new column EquipmentName 1394/09/09
                                        " [EquipmentPosition],[VoltageLevel],[PGDS_Id])" +
                                        " SELECT [CompanyID],[CompanyCode],[CompanyName]," +
                                        " [Date],[Hour],[EquipmentName],[EquipmentCode],[EquipmentType]," + //
                                        " [EquipmentPosition],[VoltageLevel],[PGDS_Id] " +
                                        " FROM [Transfer].[FixedParameters] ";

                        #endregion

                        #region Execute

                        _selectString += "EXEC Transfer.SP_SaveBillData ;";

                        #endregion

                        #region ExecuteSelectString

                        //try
                        //{
                        //    Parameters
                        //        .ExecuteSelectString(_selectString);
                        //}
                        //catch (Exception ex)
                        //{
                        //    logger.AddToLogAndShow(ex.Message, LogStyle.ErrorStyle, richTextBoxLog);
                        //    //MessageBox.Show(ex.Message);;
                        //}
                        _globalSqlCmd = new SqlCommand(_selectString, _cnn) {CommandTimeout = 1000};
                        //_cnn.Open();
                        //_globalSqlCmd.ExecuteNonQuery();
                        //_cnn.Close();
                        ExecuteGlobalCmd();
                        if (_globalSqlCmdError)
                        {
                            _bw.CancelAsync();
                            return;
                        }

                        #endregion

                        #endregion

                        break;

                }

                _logger.AddToLogAndShow("____________________________________________________________________",
                    LogStyle.InfoStyle);
                btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = false; });
            }
            catch (Exception ex)
            {
                _cnn.Close();
                _logger.AddToLogAndShow(ex);
                _bw.ReportProgress(0);
                string strMessage = ex.Message;
                new Help.FrmDetailMessageBox("خطا در اجرای مرحله چهارم - ایجاد جدول خروجی", strMessage, "Error")
                    .ShowDialog();
            }
        }

        #endregion

       

       

       
        #region Fill Sellers type

        /// <summary>
        /// تكمیل لیست باكس فروشندگان
        /// </summary>
        private void FillSellers()
        {
            // رشته فرمان شیء را خالی می كند

            _sqlCommand.CommandText = string.Empty;
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstSellerType.List.Items.Clear();
            lstSellerList.List.Items.Clear();
            _sqlCommand.Connection = _sqlConnection;

            //------------------

            #region Fill list

            switch (_ptypeE)
            {
                case "Sellers":

                    #region  Sellers

                    lstPowerStations.List.Items.Clear();
                    _sqlCommand.CommandText = "SELECT distinct ([CompanyTypeName]) [CompanyTypeName] FROM " +
                                              " [Sellers].[Tbl_Company_PowerStation] ";

                    #endregion

                    break;

                case "Buyers":

                    #region Buyers

                    _sqlCommand.CommandText = "SELECT distinct ([CompanyTypeName]) [CompanyTypeName] FROM " +
                                              " [Buyers].[Tbl_Company] ";

                    #endregion

                    break;

                case "Transfer":

                    #region  Transfer

                    _sqlCommand.CommandText = "SELECT distinct ([CompanyTypeName]) [CompanyTypeName] FROM " +
                                              " [Transfer].[Tbl_Company] ";

                    #endregion

                    break;
            }

            #endregion

            //------------------------

            lstSellerType.TheSqlConnection = _sqlConnection;
            lstSellerType.TheSqlCommand = _sqlCommand;
            lstSellerType.FillListBox();
        }

        #endregion

        #region Fill Sellers List

        /// <summary>
        /// تكمیل كننده ی لیست فروشندگان
        /// </summary>
        private void FillSellersList(object o, EventArgs e)
        {
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstSellerList.List.Items.Clear();
            lstPowerStations.List.Items.Clear();

            // متن فرمان اجرایی را پاك می كند
            _sqlCommand.CommandText = String.Empty;

            // حذف عناصر در صورتی كه گزینه ای انتخاب نشده باشد

            #region First List Box Checked Items = 0

            if (lstSellerType.List.CheckedItems.Count == 0)
            {
                lstSellerList.List.Items.Clear();
                // از تابع خارج می گردد
                return;
            }

            #endregion

            // بررسی موقعیتی كه تنها یك گزینه انتخاب شده

            #region First List Box Checked Items = 1

            if (lstSellerType.List.CheckedItems.Count == 1)
            {
                // افزودن پارامتر به دستور
                _sqlCommand.Parameters.Add("@Param1", SqlDbType.NVarChar, 50);
                // تخصیص مقدار رشته ای انتخاب شده به پارامتر
                _sqlCommand.Parameters[0].Value =
                    lstSellerType.List.CheckedItems[0].ToString();
                // تولید فرمان اجرایی اس كیو ال
                //************

                #region Fill list

                switch (_ptypeE)
                {
                    case "Sellers":

                        #region  Sellers

                        _sqlCommand.CommandText =
                            " SELECT  distinct ([CompanyName]) [CompanyName] " +
                            " FROM [Sellers].[Tbl_Company_PowerStation] " +
                            " Where CompanyTypeName = @Param1  ";

                        #endregion

                        break;

                    case "Buyers":

                        #region Buyers

                        _sqlCommand.CommandText =
                            " SELECT  distinct ([CompanyName]) [CompanyName]  " +
                            " FROM [Buyers].[Tbl_Company] " +
                            "Where CompanyTypeName = @Param1 ";

                        #endregion

                        break;

                    case "Transfer":

                        #region  Transfer

                        _sqlCommand.CommandText =
                            " SELECT  distinct ([CompanyName]) [CompanyName] " +
                            " FROM [Transfer].[Tbl_Company] " +
                            " Where CompanyTypeName = @Param1 ";

                        #endregion

                        break;
                }

                #endregion

                //**************

            }

            #endregion

            // بررسی موقعیتی كه بیش از  یك گزینه انتخاب شده

            #region First List Box Checked Items > 1

            if (lstSellerType.List.CheckedItems.Count > 1)
            {
                // حلقه ای برای پیمایش عناصر به اندازه آیتم های چك شده
                for (int i = 0; i < lstSellerType.List.CheckedItems.Count; i++)
                {
                    // ایجاد شماره پارامتر - یكی بیشتر از شمارنده
                    string paraName = "@Param" + i + 1;
                    // افزودن پارامتر به فرمان اجرایی بر اساس نام آن
                    _sqlCommand.Parameters.Add(paraName, SqlDbType.NVarChar, 50);
                    // تخصیص مقدار آیتم انتخاب شده با اندیس شمارنده به پارامتر
                    _sqlCommand.Parameters[i].Value =
                        lstSellerType.List.CheckedItems[i].ToString();
                    // ------------------------------------------------------
                    // بررسی اینكه شمارنده اولین عنصر را بررسی می نماید یا خیر
                    if (i == 0)
                    {
                        #region Fill list

                        switch (_ptypeE)
                        {
                            case "Sellers":

                                #region  Sellers

                                _sqlCommand.CommandText =
                                    " SELECT distinct ([CompanyName]) [CompanyName] " +
                                    " FROM [Sellers].[Tbl_Company_PowerStation] " +
                                    " Where CompanyTypeName = " + paraName;

                                #endregion

                                break;

                            case "Buyers":

                                #region Buyers

                                _sqlCommand.CommandText =
                                    " SELECT distinct ([CompanyName]) [CompanyName] " +
                                    " FROM [Buyers].[Tbl_Company] " +
                                    " Where CompanyTypeName = " + paraName;

                                #endregion

                                break;

                            case "Transfer":

                                #region  Transfer

                                _sqlCommand.CommandText =
                                    " SELECT distinct ([CompanyName]) [CompanyName] " +
                                    " FROM [Transfer].[Tbl_Company] " +
                                    " Where CompanyTypeName = " + paraName;

                                #endregion

                                break;
                        }

                        #endregion

                    }
                    else
                    {
                        #region Fill list

                        switch (_ptypeE)
                        {
                            case "Sellers":

                                #region  Sellers

                                _sqlCommand.CommandText +=
                                    " Union SELECT distinct ([CompanyName]) [CompanyName] " +
                                    " FROM [Sellers].[Tbl_Company_PowerStation] " +
                                    " Where CompanyTypeName = " + paraName;

                                #endregion

                                break;

                            case "Buyers":

                                #region Buyers

                                _sqlCommand.CommandText +=
                                    " Union  SELECT distinct ([CompanyName]) [CompanyName] " +
                                    " FROM [Buyers].[Tbl_Company] " +
                                    " Where CompanyTypeName = " + paraName;

                                #endregion

                                break;

                            case "Transfer":

                                #region  Transfer

                                _sqlCommand.CommandText +=
                                    " Union SELECT distinct ([CompanyName]) [CompanyName] " +
                                    " FROM [Transfer].[Tbl_Company] " +
                                    " Where CompanyTypeName = " + paraName;

                                #endregion

                                break;
                        }

                        #endregion

                    }
                }
            }

            #endregion

            // هنگامی رخ می دهد كه حداقل یك عنصر انتخاب شده باشد

            #region First ListBox CheckItems Count > 0

            // رشته اتصال اصلی را به شیء اتصال نصبت می دهد
            _sqlConnection.ConnectionString = _connectionString;
            // شیء اتصال را به شیء اتصال لیست باكس نصبت می دهد
            lstSellerList.TheSqlConnection = _sqlConnection;
            // شیء فرمان اس كیو ال را به شیء فرمان لیست باكس نصب می دهد
            lstSellerList.TheSqlCommand = _sqlCommand;
            // لیست باكس را تكمیل می كند
            lstSellerList.FillListBox();
            // كلیه پارامتر های ایجاد شده را حذف می نماید
            lstSellerList.TheSqlCommand.Parameters.Clear();
            _sqlCommand.Parameters.Clear();

            #endregion

        }

        private void FillSellersListAll()
        {
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstSellerList.List.Items.Clear();
            lstPowerStations.List.Items.Clear();

            // متن فرمان اجرایی را پاك می كند
            _sqlCommand.CommandText = String.Empty;




            #region Fill list

            switch (_ptypeE)
            {
                case "Sellers":

                    #region  Sellers

                    _sqlCommand.CommandText =
                        " SELECT distinct ([CompanyName]) [CompanyName] " +
                        " FROM [Sellers].[Tbl_Company_PowerStation] ";

                    #endregion

                    break;

                case "Buyers":

                    #region Buyers

                    _sqlCommand.CommandText =
                        " SELECT distinct ([CompanyName]) [CompanyName] " +
                        " FROM [Buyers].[Tbl_Company] ";


                    #endregion

                    break;

                case "Transfer":

                    #region  Transfer

                    _sqlCommand.CommandText =
                        " SELECT distinct ([CompanyName]) [CompanyName] " +
                        " FROM [Transfer].[Tbl_Company] ";

                    #endregion

                    break;
            }

            #endregion





            #endregion

            // هنگامی رخ می دهد كه حداقل یك عنصر انتخاب شده باشد

            #region First ListBox CheckItems Count > 0

            // رشته اتصال اصلی را به شیء اتصال نصبت می دهد
            _sqlConnection.ConnectionString = _connectionString;
            // شیء اتصال را به شیء اتصال لیست باكس نصبت می دهد
            lstSellerList.TheSqlConnection = _sqlConnection;
            // شیء فرمان اس كیو ال را به شیء فرمان لیست باكس نصب می دهد
            lstSellerList.TheSqlCommand = _sqlCommand;
            // لیست باكس را تكمیل می كند
            lstSellerList.FillListBox();
            // كلیه پارامتر های ایجاد شده را حذف می نماید
            lstSellerList.TheSqlCommand.Parameters.Clear();
            _sqlCommand.Parameters.Clear();

            #endregion

        }

        #endregion

        #region Fill PowerStations

        private void FillPowerStations(object o, EventArgs e)
        {
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstPowerStations.List.Items.Clear();

            // متن فرمان اجرایی را پاك می كند
            _sqlCommand.CommandText = String.Empty;

            // حذف عناصر در صورتی كه گزینه ای انتخاب نشده باشد

            #region First List Box Checked Items = 0

            if (lstSellerList.List.CheckedItems.Count == 0)
            {
                lstPowerStations.List.Items.Clear();
                // از تابع خارج می گردد
                return;
            }

            #endregion

            // بررسی موقعیتی كه تنها یك گزینه انتخاب شده

            #region First List Box Checked Items = 1

            if (lstSellerList.List.CheckedItems.Count == 1)
            {
                // افزودن پارامتر به دستور
                _sqlCommand.Parameters.Add("@Param1", SqlDbType.NVarChar, 50);
                // تخصیص مقدار رشته ای انتخاب شده به پارامتر
                _sqlCommand.Parameters[0].Value =
                    lstSellerList.List.CheckedItems[0].ToString();
                // تولید فرمان اجرایی اس كیو ال
                _sqlCommand.CommandText =
                    "SELECT Distinct([PowerStationName]) FROM [Sellers].[Tbl_Company_PowerStation]" +
                    " Where CompanyName = @Param1 ";


            }

            #endregion

            // بررسی موقعیتی كه بیش از  یك گزینه انتخاب شده

            #region First List Box Checked Items > 1

            if (lstSellerList.List.CheckedItems.Count > 1)
            {
                // حلقه ای برای پیمایش عناصر به اندازه آیتم های چك شده
                for (int i = 0; i < lstSellerList.List.CheckedItems.Count; i++)
                {
                    // ایجاد شماره پارامتر - یكی بیشتر از شمارنده
                    string paraName = "@Param" + i + 1;
                    // افزودن پارامتر به فرمان اجرایی بر اساس نام آن
                    _sqlCommand.Parameters.Add(paraName, SqlDbType.NVarChar, 50);
                    // تخصیص مقدار آیتم انتخاب شده با اندیس شمارنده به پارامتر
                    _sqlCommand.Parameters[i].Value =
                        lstSellerList.List.CheckedItems[i].ToString();
                    // ------------------------------------------------------
                    // بررسی اینكه شمارنده اولین عنصر را بررسی می نماید یا خیر
                    if (i == 0)
                    {
                        _sqlCommand.CommandText =
                            " SELECT Distinct([PowerStationName]) FROM [Sellers].[Tbl_Company_PowerStation]" +
                            " Where CompanyName = " + paraName;

                    }
                    else
                    {
                        _sqlCommand.CommandText +=
                            " Union SELECT [PowerStationName] FROM [Sellers].[Tbl_Company_PowerStation]" +
                            " Where CompanyName = " + paraName;

                    }
                }
            }

            #endregion

            // هنگامی رخ می دهد كه حداقل یك عنصر انتخاب شده باشد

            #region First ListBox CheckItems Count > 0

            // رشته اتصال اصلی را به شیء اتصال نصبت می دهد
            _sqlConnection.ConnectionString = _connectionString;
            // شیء اتصال را به شیء اتصال لیست باكس نصبت می دهد
            lstPowerStations.TheSqlConnection = _sqlConnection;
            // شیء فرمان اس كیو ال را به شیء فرمان لیست باكس نصب می دهد
            lstPowerStations.TheSqlCommand = _sqlCommand;
            // لیست باكس را تكمیل می كند
            lstPowerStations.FillListBox();
            // كلیه پارامتر های ایجاد شده را حذف می نماید
            lstPowerStations.TheSqlCommand.Parameters.Clear();
            _sqlCommand.Parameters.Clear();

            #endregion

        }

        private void FillPowerStationsAll()
        {
            // عناصر لیست باكس های بعدی را پاك می نماید
            lstPowerStations.List.Items.Clear();

            // متن فرمان اجرایی را پاك می كند
            _sqlCommand.CommandText = String.Empty;

            // حذف عناصر در صورتی كه گزینه ای انتخاب نشده باشد

            _sqlCommand.CommandText =
                " SELECT Distinct([PowerStationName]) FROM [Sellers].[Tbl_Company_PowerStation]";


            // هنگامی رخ می دهد كه حداقل یك عنصر انتخاب شده باشد

            #region First ListBox CheckItems Count > 0

            // رشته اتصال اصلی را به شیء اتصال نصبت می دهد
            _sqlConnection.ConnectionString = _connectionString;
            // شیء اتصال را به شیء اتصال لیست باكس نصبت می دهد
            lstPowerStations.TheSqlConnection = _sqlConnection;
            // شیء فرمان اس كیو ال را به شیء فرمان لیست باكس نصب می دهد
            lstPowerStations.TheSqlCommand = _sqlCommand;
            // لیست باكس را تكمیل می كند
            lstPowerStations.FillListBox();
            // كلیه پارامتر های ایجاد شده را حذف می نماید
            lstPowerStations.TheSqlCommand.Parameters.Clear();
            _sqlCommand.Parameters.Clear();

            #endregion

        }

        #endregion


        #region Fill_Ems_Settlement

        private void FillEmsSettlement()
        {
            btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = true; });
            richTextBoxLog.BeginInvoke((MethodInvoker) delegate() { richTextBoxLog.Visible = true; });
            panelLog.BeginInvoke((MethodInvoker) delegate() { panelLog.Visible = true; });

            _logger.AddToLogAndShow("\n********************** STAGE 6 **********************", LogStyle.InfoStyle);
            _logger.AddToLogAndShow("Saving BillData in EMIS", LogStyle.InfoStyle);


            var od = PersianDateConverter.ToPersianDate
                (DateTime.Now);
            string cdate = od.ToString();
            var currentDateTime = cdate.Substring(0, 10) + "-" + PersianDateConverter.ToPersianDate
                (DateTime.Now).Time + ".00";

            switch (_ptypeE)
            {
                case "Sellers":
                    if (_billTypeCode == "D" && String.Compare(_sDate, _eDate, StringComparison.Ordinal) < 0)
                        //roozaneh be soorate baze'i
                    {
                        _dateCounter = _sDate;
                        while (String.Compare(_dateCounter, _eDate, StringComparison.Ordinal) <= 0)
                        {
                            if (_bw.CancellationPending)
                            {
                                _logger.AddToLogAndShow("Operation canceled by user", LogStyle.ErrorStyle);
                                return;
                            }

                            _logger.AddToLogAndShow("date: " + _dateCounter, LogStyle.InfoStyle);

                            string query = "DROP TABLE [Sellers].[BillDataU]; "
                                           +
                                           "SELECT * INTO [Sellers].[BillDataU] FROM [Sellers].[BillDataUD] WHERE Date ='" +
                                           _dateCounter + "'";
                            _globalSqlCmd = new SqlCommand(query, _cnn) {CommandTimeout = 1000};
                            ExecuteGlobalCmd();
                            if (_globalSqlCmdError)
                            {
                                _bw.CancelAsync();
                                return;
                            }

                            FillEmsSettlementSeller(currentDateTime);

                            _logger.AddToLogAndShow(
                                "____________________________________________________________________",
                                LogStyle.InfoStyle);
                            _dateCounter = NextDay(_dateCounter);
                        }
                    }
                    else
                    {
                        FillEmsSettlementSeller(currentDateTime);
                    }

                    break;
                case "Buyers":
                    FillEmsSettlementBuyer(currentDateTime);
                    break;
                case "Transfer":
                    FillEmsSettlementTransfer(currentDateTime);
                    break;
                case "Fuel":
                    FillEmsSettlementFuel(currentDateTime);
                    break;
            }

            _logger.AddToLogAndShow("____________________________________________________________________",
                LogStyle.InfoStyle);

            btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = false; });
        }

        private void FillEmsSettlementSeller(string currentDateTime)
        {
            try
            {
                _globalSqlCmd = new SqlCommand("Sellers.SP_Fill_EmsSettlement ", _cnn)
                {
                    CommandTimeout = 10000,
                    CommandType = CommandType.StoredProcedure
                };
                _globalSqlCmd.Parameters.AddWithValue("@DateTime", currentDateTime);

                switch (_btype)
                {
                    case "روزانه":
                        _globalSqlCmd.Parameters.AddWithValue("@Type", "D");
                        break;
                    case "ماهيانه":
                        _globalSqlCmd.Parameters.AddWithValue("@Type", "M");
                        break;
                    case "قطعي":
                        if (_stype == "قطعی-صورتحساب")
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "C");
                        }
                        else if (_stype == "قطعی-صورتحساب معوق")
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "DF");
                        }
                        break;
                }
                ExecuteGlobalCmd();
                if (_globalSqlCmdError)
                {
                    _bw.CancelAsync();
                    return;
                }
                //_cnn.Open();
                //_globalSqlCmd.ExecuteNonQuery();
                //_cnn.Close();
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
        }

        private void FillEmsSettlementBuyer(string currentDateTime)
        {
            try
            {
                _globalSqlCmd = new SqlCommand("Buyers.SP_Fill_EmsSettlementB ", _cnn)
                {
                    CommandTimeout = 10000,
                    CommandType = CommandType.StoredProcedure
                };
                _globalSqlCmd.Parameters.AddWithValue("@DateTime", currentDateTime);


                switch (_btype)
                {

                    case "ماهيانه":
                        _globalSqlCmd.Parameters.AddWithValue("@Type", "M");
                        break;
                    case "قطعي":
                          if (_stype == "قطعی-صورتحساب")
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "C");
                        }
                        else if (_stype == "قطعی-صورتحساب معوق") //added 1395/06/27
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "DF");
                        }
                        break;
                }
                ExecuteGlobalCmd();
                if (_globalSqlCmdError)
                {
                    _bw.CancelAsync();
                    return;
                }
                //_cnn.Open();
                //_globalSqlCmd.ExecuteNonQuery();
                //_cnn.Close();

                try
                {
                    SmsUtility.SendSms(_sDate, _billTypeCode, SmsType.SaveInEmis);
                }
                catch (Exception ex)
                {
                    _logger.AddToLogAndShow("خطا در ارسال پیامک:", LogStyle.ErrorStyle);
                    _logger.AddToLogAndShow(ex);
                }

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
        }

        private void FillEmsSettlementTransfer(string currentDateTime)
        {
            try
            {
                _globalSqlCmd = new SqlCommand("Transfer.SP_Fill_EmsSettlementT ", _cnn)
                {
                    CommandTimeout = 10000,
                    CommandType = CommandType.StoredProcedure
                };

                _globalSqlCmd.Parameters.AddWithValue("@subject", "T");
                _globalSqlCmd.Parameters.AddWithValue("@DateTime", currentDateTime);

                _globalSqlCmd.Parameters.AddWithValue("@FromDate", _sDate);
                _globalSqlCmd.Parameters.AddWithValue("@ToDate", _eDate);
                switch (_btype)
                {

                    case "ماهيانه":
                        _globalSqlCmd.Parameters.AddWithValue("@Type", "M");
                        break;
                    case "قطعي":
                        if (_stype == "قطعی-صورتحساب")
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "C");
                        }
                        else if (_stype == "قطعی-صورتحساب معوق")//added 1395/06/27
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "DF");
                        }
                        break;
                }
                ExecuteGlobalCmd();
                if (_globalSqlCmdError)
                {
                    _bw.CancelAsync();
                    return;
                }
                //_cnn.Open();
                //_globalSqlCmd.ExecuteNonQuery();
                //_cnn.Close();

            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
        }

        private void FillEmsSettlementFuel(string currentDateTime)
        {
            try
            {
                _globalSqlCmd = new SqlCommand("Fuel.SP_Fill_EmsSettlement_F ", _cnn)
                {
                    CommandTimeout = 10000,
                    CommandType = CommandType.StoredProcedure
                };

                _globalSqlCmd.Parameters.AddWithValue("@subject", "F");
                _globalSqlCmd.Parameters.AddWithValue("@DateTime", currentDateTime);

                _globalSqlCmd.Parameters.AddWithValue("@BeginDate", _sDate);
                _globalSqlCmd.Parameters.AddWithValue("@EndDate", _eDate);
                switch (_btype)
                {

                    case "ماهيانه":
                        _globalSqlCmd.Parameters.AddWithValue("@Type", "M");
                        break;
                    case "قطعي":
                        if (_stype == "قطعی-صورتحساب")
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "C");
                        }
                        else if (_stype == "قطعی-صورتحساب معوق")//added 1395/06/27
                        {
                            _globalSqlCmd.Parameters.AddWithValue("@Type", "DF");
                        }
                        break;
                }

                //_cnn.Open();
                //_globalSqlCmd.ExecuteNonQuery();
                //_cnn.Close();
                ExecuteGlobalCmd();
                if (_globalSqlCmdError)
                {
                    _bw.CancelAsync();
                    return;
                }
            }
            catch (Exception ex)
            {
                _logger.AddToLogAndShow(ex);
            }
        }

        #endregion

      

        #region releaseObject

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Unable to release the Object " + ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        #endregion

        #region Help

        private void BillSelectDHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"help.chm", HelpNavigator.Topic, "BillSelectD.htm");
        }

        #endregion

        private void btnHideShowLog_Click(object sender, EventArgs e)
        {
            if (btnHideShowLog.Text == "-")
            {
                panelLog.Height = 30;
                btnHideShowLog.Text = "+";
                btnHideShowLog.Image = Resources.me5;
            }
            else
            {
                panelLog.Height = 352;
                btnHideShowLog.Text = "-";
                btnHideShowLog.Image = Resources.me4;
            }
        }

        private void BillSelectD_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (
                MessageBox.Show("Are you sure you want to close the form?", "Confirm",
                    MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (btnStopStage.Enabled)
                {
                    btnStopStage.PerformClick();
                }

            }
            else
            {
                e.Cancel = true;
            }

        }

        ////////////////////////////////////////////////////////////////////
        private void cnn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            foreach (SqlError error in e.Errors)
            {
                if (error.Class <= 10)
                {
                    _logger.AddToLogQueue(error.Message,
                        error.Message.StartsWith("Warning") ? LogStyle.WarningStyle : LogStyle.InfoStyle);
                }
                else
                {
                    _logger.AddToLogQueue(
                        String.Format("Message:{0},SP:{1},Line:{2}", error.Message, error.Procedure, error.LineNumber),
                        LogStyle.ErrorStyle);
                    _globalSqlCmdError = true;
                   
                    _bw.CancelAsync();
                    _globalSqlCmd.Cancel();
                    _bw.ReportProgress(0);
                   
                }
                _logger.UpdateLogOuput();
                //_tempLogText = _tempLogText + error.Message + "\n";


                // richTextBoxLog.BeginInvoke((MethodInvoker)delegate()
                // {
                //     richTextBoxLog.AppendText(string.Format(error.Message) + "\n");
                //     richTextBoxLog.SelectionStart = richTextBoxLog.Text.Length;
                //     richTextBoxLog.ScrollToCaret();
                //     richTextBoxLog.Refresh();
                // }); //Cause System.AccessViolationException
            }
        }

        private void btnStopStage_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to cancel?", "Cancel", MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {
                try
                {
                    _globalSqlCmd.Cancel();
                }
                catch
                {
                }
                _bw.CancelAsync();
                btnStopStage.BeginInvoke((MethodInvoker) delegate { btnStopStage.Enabled = false; });
            }
        }

        private void ExecuteGlobalCmd()
        {
            _cnn.Open();
            _globalSqlCmdError = false;
            _globalSqlCmd.ExecuteNonQuery();
            _cnn.Close();
            if (_globalSqlCmdError)
            {
                throw new Exception("Error in executing  sql query :" + _globalSqlCmd.CommandText);
            }

        }

        private string NextDay(string date)
        {

            PersianDate Date = new PersianDate(int.Parse(date.Substring(0, 4)), int.Parse(date.Substring(5, 2)),
                int.Parse(date.Substring(8, 2)));
            PersianDate NextDayDate;
            try
            {
                NextDayDate = new PersianDate(Date.Year, Date.Month, Date.Day + 1);
            }
            catch (Exception)
            {

                if (Date.Month == 12)
                {
                    NextDayDate = new PersianDate(Date.Year + 1, 1, 1);
                }
                else
                {
                    NextDayDate = new PersianDate(Date.Year, Date.Month + 1, 1);
                }
            }
            return NextDayDate.ToString().Substring(0, 10);
        }

        
    }

}

//#endregion