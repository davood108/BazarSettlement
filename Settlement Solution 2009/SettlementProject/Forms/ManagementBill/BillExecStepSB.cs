﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using MohanirPouya.Classes;
using MohanirPouya.DbLayer;

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class BillExecStepSb :  MohanirFormDialog
    {



        public bool _Step1 { get; set; }
        public bool _Step2 { get; set; }
        public bool _Step3 { get; set; }
        public bool _Step4 { get; set; }
        public bool _Step5 { get; set; }
        public bool _stepSaveinEmis { get; set; }

        #region Ctor
        public BillExecStepSb()
        {
            InitializeComponent();
            StepState.ExecStateSb= false;
            ShowDialog();
        }

        public BillExecStepSb(bool fullTick) //1395/05/06
        {
            InitializeComponent();
            if (fullTick)
            {
                _Step1 = true;
                _Step2 = true;
                _Step3 = true;
                _Step4 = true;
                _Step5 = true;
                _stepSaveinEmis = true;
                StepState.ExecStateSb = true;
            }
            else
            {
                StepState.ExecStateSb = false;
                ShowDialog();
            }
        }

        #endregion

        

  

        private void BtnCancelClick(object sender, EventArgs e)
        {
            StepState.ExecStateSb = false;
            Close();
        }

        private void BtnExecClick(object sender, EventArgs e)
        {
            StepState.ExecStateSb = true;
            _Step1 = chkStep1.Checked;
            _Step2 = chkStep2.Checked;
            _Step3 = chkStep3.Checked;
            _Step4 = chkStep4.Checked;
            _Step5 = chkStep5.Checked;
            Close();
        }

        private void BillExecStepSb_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"help.chm", HelpNavigator.Topic, "BillExecStepSB.htm");
        }

        private void BillExecStepSb_Load(object sender, EventArgs e)
        {

        }


        
    }
}
