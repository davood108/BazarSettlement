#region Using
using System;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using MohanirPouya.NRI;
using RPNCalendar.Utilities;
using NRI;
#endregion

namespace MohanirPouya.Forms.ManagementBill
{
    public partial class SelectedTime : Office2007Form
    {
       
       
        #region Field

        private string _ptypeE;
   //     private string UnitCN;
        //private readonly int _TypeID;
        private readonly string _type;
        private readonly string _title;

        private BillSelectD _frmBillD;
        private string _typeEN;
        private string _stype;
     

        #endregion

        #region Ctor
        /// <summary>
        /// سازنده فرم
        /// </summary>
        
        public SelectedTime(string type,string title,string TypeEN)
        {
            InitializeComponent();

            btnPrevMonth.Visible = false;

            #region SetTime
            PersianDate occuredDate = PersianDateConverter.ToPersianDate
                (DateTime.Now.Date);
            StartDate.SelectedDateTime = DateTime.Now;
            EndDate.SelectedDateTime = DateTime.Now;
            StartDate.Text = occuredDate.ToString();
            EndDate.Text = occuredDate.ToString();


            cBoxMultipleDaily.Visible = false;
         
            #endregion
            
             _type = type;
             _title = title;
            _typeEN = TypeEN;
           
          
            #region Type
            #region Daily
             if (type == "روزانه" && title == "روزانه")
            {
                cBoxDaily.Checked = true;
                lblEDate.Visible = false;
                EndDate.Visible = false;
                gbSelect.Visible = false;
                cBoxMultipleDaily.Visible = true;
                //lblTitle.Text = "صورت حساب روزانه";
            }
                #endregion
             else if (type == "ماهیانه" && title == "ماهیانه")
             {
                 cBoxMounthly.Checked = true;
                 gbSelect.Visible = false;
                 //lblTitle.Text = "صورت حساب ماهیانه";
             }
             else if (type == "قطعی" && title == "قطعی")
             {
                 cBoxConst.Checked = true;
                 //lblTitle.Text = "صورت حساب قطعی";
             }
             //else if (Type == "گزارش سالانه" && Title == "خرید")
             //{
             //    cBoxYearlyReport.Checked = true;
             //    lblTitle.Text = "خرید";
             //    Text = "فرم گزارش سالانه";
              
             //}
             //else if (Type == "گزارش سالانه" && Title == "فروش")
             //{
             //    cBoxYearlyReport.Checked = true;
             //    lblTitle.Text = " فروش";
             //    Text = "فرم گزارش سالانه";
             //}
             //else if (Type == "گزارش سالانه" && Title == "خدمات انتقال")
             //{
             //    cBoxYearlyReport.Checked = true;
             //    lblTitle.Text = "خدمات انتقال";
             //    Text = "فرم گزارش سالانه";
             //}
             //else if (Type == "گزارش سالانه" && Title == "گزارش ماهیانه واحدی")
             //{
             //    cBoxYearlyReport.Checked = true;
             //    cboTitle.Visible = false;
             //    labelX4.Visible = false;
             //    lblTitle.Text = "گزارش ماهیانه واحدی";
             //    Text = "فرم گزارش سالانه";
             //}

             //else if (Type == "گزارش سالانه" && Title == "گزارش ماهیانه نیروگاهی")
             //{
             //    cBoxYearlyReport.Checked = true;
             //    cboTitle.Visible = false;
             //    labelX4.Visible = false;
             //    lblTitle.Text = "گزارش ماهیانه نیروگاهی";
             //    Text = "فرم گزارش سالانه";
             //}
             //else if (Type == "گزارش سالانه" && Title == "تراز")
             //{
             //    cBoxYearlyReport.Checked = true;
             //    cboTitle.Visible = false;
             //    labelX4.Visible = false;
             //    lblTitle.Text = "تراز";
             //    Text = "فرم گزارش سالانه";
             //}
           
            #endregion
            ShowDialog();
        }
        #endregion


        #region Event

        #region Form_Load
        private void FormLoad(object sender, EventArgs e)
        {
            if (BillSetting.IsSet && BillSetting.BillType == _typeEN)
            {
                StartDate.SelectedDateTime = BillSetting.StartDateTime;
                EndDate.SelectedDateTime = BillSetting.EnDateTime;
                StartDate.Text = BillSetting.StartDateText;
                EndDate.Text = BillSetting.EndDateText;

            }
            else
                BillSetting.IsSet = false;



            if ((cBoxMounthly.Checked || cBoxConst.Checked))
            {
                btnPrevMonth.Visible = true;

                if (!BillSetting.IsSet)
                    GoToPreviousMonth();
            }

            #region FillCboTitle
            if (_type == "روزانه" && _title == "روزانه")
            {
                var title = new String[1];
                title[0] = "فروش";
                cboTitle.DataSource = title;
               
            }
            if (_type == "ماهیانه" && _title == "ماهیانه")
            {
                var title = new String[4];
                title[0] = "فروش";
                title[1] = "خرید";
                title[2] = "خدمات انتقال";
                title[3] = "سوخت";

                cboTitle.DataSource = title;

            }
            if (_type == "قطعی" && _title == "قطعی")
            {
                var title = new String[4];
                title[0] = "فروش";
                title[1] = "خرید";
                title[2] = "خدمات انتقال";
                title[3] = "سوخت";
                cboTitle.DataSource = title;

            }
            if (_type == "گزارش سالانه" &&  _title == "فروش")
            {
                var title = new String[1];
                title[0] = "فروش";
                cboTitle.DataSource = title;

            }

            if (_type == "گزارش سالانه" && _title == "خرید")
            {
                var title = new String[1];
                title[0] = "خرید";
                cboTitle.DataSource = title;

            }

            if (_type == "گزارش سالانه" &&  _title == "خدمات انتقال")
            {
                var title = new String[1];
                title[0] = "خدمات انتقال";
                cboTitle.DataSource = title;

            }

            #endregion
        }
        #endregion

        #region btnSelect_Click

        private void BtnSelectClick(object sender, EventArgs e)
        {
            #region Type

            String type = String.Empty;


            String ptype = cboTitle.Text;

            switch (ptype)
            {
                case "فروش":
                    _ptypeE = "Sellers";
                    break;
                case "خرید":
                    _ptypeE = "Buyers";
                    break;
                case "خدمات انتقال":
                    _ptypeE = "Transfer";
                    break;

                case "سوخت":
                    _ptypeE = "Fuel";
                    break;


            }
            if (cBoxConst.Checked) type = "قطعي";
            else if (cBoxDaily.Checked)
            {
                type = "روزانه";
                if (!cBoxMultipleDaily.Checked)
                {
                    EndDate.Text = StartDate.Text;
                }
            }
            else if (cBoxMounthly.Checked) type = "ماهيانه";
            //else if (cBoxYearlyReport.Checked) Type = "گزارش سالانه";

            if (lblTitle.Text == @"گزارش ماهیانه واحدی")
            {
                ptype = "گزارش ماهیانه واحدی";
                _ptypeE = "گزارش ماهیانه واحدی";

            }

            if (lblTitle.Text == @"گزارش ماهیانه نیروگاهی")
            {
                ptype = "گزارش ماهیانه نیروگاهی";
                _ptypeE = "گزارش ماهیانه نیروگاهی";

            }

            if (lblTitle.Text == @"تراز")
            {
                ptype = "تراز";
                _ptypeE = "تراز";

            }

            #endregion

            #region Set  Initial

            Int16 diff;
            if (cBoxDaily.Checked) diff = 0;
            else
                diff = (Int16) (EndDate.SelectedDateTime).Subtract
                    (StartDate.SelectedDateTime).Days;
            PersianDate weekB = PersianDateConverter.ToPersianDate(StartDate.SelectedDateTime.AddDays
                (-7).Date);
            String weekBText = weekB.ToString();
            string weeKBDate = weekBText.Substring(0, 10);

            if (rbBill.Checked)
            {
                _stype = "قطعی-صورتحساب";
            }

            if (rbBill_Movagh.Checked)
            {
                _stype = "قطعی-صورتحساب معوق";
            }
            if (rbYearlyReport.Checked)
            {
                _stype = "قطعی-گزارش سالانه";
            }




            #endregion


            //////////////////
            BillSetting.StartDateTime = StartDate.SelectedDateTime;
            BillSetting.EnDateTime = EndDate.SelectedDateTime;
            BillSetting.StartDateText = StartDate.Text;
            BillSetting.EndDateText = EndDate.Text;
            BillSetting.BillType = _typeEN;
            BillSetting.IsSet = true;


            ///////////
            _frmBillD = new BillSelectD(type, _typeEN, ptype, _ptypeE, StartDate.Text, EndDate.Text, diff, weeKBDate,
                _stype,true);
          //  _frmBillD.ShowDialog();//1395/05/11

            // BillSelectD2 _frmBillD2 = new BillSelectD2(type, _typeEN, ptype, _ptypeE, StartDate.Text, EndDate.Text, diff, weeKBDate, _stype);
            //_frmBillD2.Dispose();

            _frmBillD.Dispose();



        }

        #endregion

        #region Close
        private void BtnCancelClick(object sender, EventArgs e)
        {
            Close();
        }
        #endregion



        #endregion

        #region Help
        private void SelectedTimeHelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this,@"help.chm", HelpNavigator.Topic, "SelectedTime.htm");
        }
        #endregion



        private void StartDate_SelectedDateTimeChanged(object sender, EventArgs e)
        {

            if (!cBoxDaily.Checked)
            {
                PersianDate beginDate = PersianDateConverter.ToPersianDate(StartDate.SelectedDateTime);
                PersianDate endDate;
                if (beginDate.Month != 12)
                {
                    endDate = new PersianDate(beginDate.Year, beginDate.Month, beginDate.MonthDays);
                }
                else
                {
                    try
                    {
                        endDate = new PersianDate(beginDate.Year, beginDate.Month, beginDate.MonthDays); //sale kabise
                    }
                    catch
                    {

                        endDate = new PersianDate(beginDate.Year, beginDate.Month, 29);
                    }
                }
                EndDate.SelectedDateTime = (DateTime) endDate;
                EndDate.Text = endDate.ToString();
            }
            else
            {
                EndDate.SelectedDateTime = StartDate.SelectedDateTime;
                EndDate.Text = StartDate.Text;
            }
        }


        private void GoToPreviousMonth()
        {
            PersianDate occuredDate = PersianDateConverter.ToPersianDate(StartDate.SelectedDateTime);

            if (occuredDate.Month == 1)
            {
                occuredDate.Month = 12;
                occuredDate.Year = occuredDate.Year - 1;
            }
            else
            {
                occuredDate.Month--;
            }

            PersianDate beginDate = new PersianDate(occuredDate.Year, occuredDate.Month, 1);
            PersianDate endDate;
            if (occuredDate.Month != 12)
            {
                endDate = new PersianDate(occuredDate.Year, occuredDate.Month, occuredDate.MonthDays);
            }
            else
            {
                try
                {
                    endDate = new PersianDate(occuredDate.Year, occuredDate.Month, occuredDate.MonthDays);//sale kabise
                }
                catch
                {

                    endDate = new PersianDate(occuredDate.Year, occuredDate.Month, 29);
                }
            }

            StartDate.SelectedDateTime = (DateTime)beginDate;
            EndDate.SelectedDateTime = (DateTime)endDate;
            StartDate.Text = beginDate.ToString();
            EndDate.Text = endDate.ToString();
        }

        private void btnPrevMonth_Click(object sender, EventArgs e)
        {
            GoToPreviousMonth();
        }

        private void cBoxMultipleDaily_CheckedChanged(object sender, EventArgs e)
        {
            lblEDate.Visible = cBoxMultipleDaily.Checked;
            EndDate.Visible = cBoxMultipleDaily.Checked;
        }


    }
}