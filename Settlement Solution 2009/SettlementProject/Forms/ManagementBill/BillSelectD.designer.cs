namespace MohanirPouya.Forms.ManagementBill
{
    partial class BillSelectD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnStopStage = new System.Windows.Forms.Button();
            this.DataSetLog = new System.Data.DataSet();
            this.ParametersLog = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.CCreationDate = new System.Data.DataColumn();
            this.CStartDate = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.lblParamDescribe = new System.Windows.Forms.Label();
            this.lblBillType = new System.Windows.Forms.Label();
            this.lblBeginDate = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.btnBillNumber = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.lblParamName = new System.Windows.Forms.Label();
            this.TopPanel = new DevComponents.DotNetBar.PanelEx();
            this.lblTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrinting = new DevComponents.DotNetBar.ButtonX();
            this.lblSDate = new System.Windows.Forms.Label();
            this.lblEDate = new System.Windows.Forms.Label();
            this.lblBillTitle = new System.Windows.Forms.Label();
            this.MainPanel = new DevComponents.DotNetBar.PanelEx();
            this.panelLog = new System.Windows.Forms.Panel();
            this.lblLogViewer = new System.Windows.Forms.Label();
            this.btnHideShowLog = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.btnCpfUpdate = new DevComponents.DotNetBar.ButtonX();
            this.MainStatusBar = new DevComponents.DotNetBar.Bar();
            this.lblConnection = new System.Windows.Forms.Label();
            this.lblCurentUser = new DevComponents.DotNetBar.LabelItem();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblHour = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblSecond = new System.Windows.Forms.Label();
            this.lblMinutes = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lstPowerStations = new SettlementControlLibraries.Controls.MohanirCheckList();
            this.lstSellerList = new SettlementControlLibraries.Controls.MohanirCheckList();
            this.lstSellerType = new SettlementControlLibraries.Controls.MohanirCheckList();
            this.btnExcelOut = new DevComponents.DotNetBar.ButtonX();
            this.btnSaveToEMS = new DevComponents.DotNetBar.ButtonX();
            this.ProgressBarSearch = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).BeginInit();
            this.TopPanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.panelLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainStatusBar)).BeginInit();
            this.MainStatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // MyToolTip
            // 
            this.MyToolTip.AutoPopDelay = 10000;
            this.MyToolTip.InitialDelay = 500;
            this.MyToolTip.IsBalloon = true;
            this.MyToolTip.ReshowDelay = 100;
            this.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MyToolTip.ToolTipTitle = "راهنمايي";
            // 
            // btnStopStage
            // 
            this.btnStopStage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStopStage.Enabled = false;
            this.btnStopStage.Image = global::MohanirPouya.Properties.Resources.symbol_stop1;
            this.btnStopStage.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStopStage.Location = new System.Drawing.Point(426, 0);
            this.btnStopStage.Name = "btnStopStage";
            this.btnStopStage.Size = new System.Drawing.Size(64, 26);
            this.btnStopStage.TabIndex = 73;
            this.btnStopStage.Text = "Stop";
            this.btnStopStage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MyToolTip.SetToolTip(this.btnStopStage, "Stop Process");
            this.btnStopStage.UseVisualStyleBackColor = true;
            this.btnStopStage.Click += new System.EventHandler(this.btnStopStage_Click);
            // 
            // DataSetLog
            // 
            this.DataSetLog.DataSetName = "ParametersLog";
            this.DataSetLog.Tables.AddRange(new System.Data.DataTable[] {
            this.ParametersLog});
            // 
            // ParametersLog
            // 
            this.ParametersLog.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.CCreationDate,
            this.CStartDate,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.ParametersLog.TableName = "ParametersLog";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "رديف";
            this.dataColumn1.ColumnName = "Revision";
            this.dataColumn1.DataType = typeof(int);
            // 
            // CCreationDate
            // 
            this.CCreationDate.Caption = "تاريخ ايجاد";
            this.CCreationDate.ColumnName = "CreationDate";
            this.CCreationDate.DataType = typeof(System.DateTime);
            this.CCreationDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // CStartDate
            // 
            this.CStartDate.Caption = "تاريخ آغاز اعتبار";
            this.CStartDate.ColumnName = "StartDate";
            this.CStartDate.DataType = typeof(System.DateTime);
            this.CStartDate.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "تاريخ اتمام اعتبار";
            this.dataColumn4.ColumnName = "ExpirationDate";
            this.dataColumn4.DataType = typeof(System.DateTime);
            this.dataColumn4.DateTimeMode = System.Data.DataSetDateTime.Local;
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "متن كد پارامتر";
            this.dataColumn5.ColumnName = "SelectString";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "نام كاربر";
            this.dataColumn6.ColumnName = "UserID";
            // 
            // lblParamDescribe
            // 
            this.lblParamDescribe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamDescribe.AutoSize = true;
            this.lblParamDescribe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblParamDescribe.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblParamDescribe.Location = new System.Drawing.Point(413, 64);
            this.lblParamDescribe.Name = "lblParamDescribe";
            this.lblParamDescribe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamDescribe.Size = new System.Drawing.Size(112, 13);
            this.lblParamDescribe.TabIndex = 17;
            this.lblParamDescribe.Text = "عنوان صورت حساب:";
            // 
            // lblBillType
            // 
            this.lblBillType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBillType.BackColor = System.Drawing.Color.White;
            this.lblBillType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBillType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblBillType.ForeColor = System.Drawing.Color.Green;
            this.lblBillType.Location = new System.Drawing.Point(531, 60);
            this.lblBillType.Name = "lblBillType";
            this.lblBillType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBillType.Size = new System.Drawing.Size(144, 21);
            this.lblBillType.TabIndex = 11;
            this.lblBillType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBeginDate.AutoSize = true;
            this.lblBeginDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblBeginDate.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblBeginDate.Location = new System.Drawing.Point(677, 98);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBeginDate.Size = new System.Drawing.Size(74, 13);
            this.lblBeginDate.TabIndex = 15;
            this.lblBeginDate.Text = "تاريخ شروع :";
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblEndDate.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblEndDate.Location = new System.Drawing.Point(413, 98);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblEndDate.Size = new System.Drawing.Size(64, 13);
            this.lblEndDate.TabIndex = 14;
            this.lblEndDate.Text = "تاريخ پايان:";
            // 
            // btnBillNumber
            // 
            this.btnBillNumber.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnBillNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnBillNumber.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnBillNumber.Location = new System.Drawing.Point(644, 486);
            this.btnBillNumber.Name = "btnBillNumber";
            this.btnBillNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnBillNumber.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F2);
            this.btnBillNumber.Size = new System.Drawing.Size(140, 28);
            this.btnBillNumber.TabIndex = 26;
            this.btnBillNumber.Text = "شماره زنی صورتحساب (F2)";
            this.btnBillNumber.Tooltip = "تولید اکسل فایل های خروجی شماره زده شده";
            this.btnBillNumber.Click += new System.EventHandler(this.BtnBillNumberClick);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(24, 486);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(98, 28);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Tooltip = "انصرف از اجرایی صورتحساب و خروج از فرم";
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // lblParamName
            // 
            this.lblParamName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblParamName.AutoSize = true;
            this.lblParamName.Location = new System.Drawing.Point(315, 537);
            this.lblParamName.Name = "lblParamName";
            this.lblParamName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblParamName.Size = new System.Drawing.Size(0, 13);
            this.lblParamName.TabIndex = 11;
            // 
            // TopPanel
            // 
            this.TopPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.TopPanel.Controls.Add(this.lblTitle);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(888, 40);
            this.TopPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.TopPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.TopPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.TopPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TopPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.TopPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.TopPanel.Style.GradientAngle = 90;
            this.TopPanel.TabIndex = 43;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.LightPink;
            this.lblTitle.Location = new System.Drawing.Point(245, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTitle.Size = new System.Drawing.Size(398, 37);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "تنظيمات  نمايش صورتحساب";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label1.Location = new System.Drawing.Point(677, 64);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "نوع صورت حساب:";
            // 
            // btnPrinting
            // 
            this.btnPrinting.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrinting.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnPrinting.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnPrinting.Location = new System.Drawing.Point(352, 486);
            this.btnPrinting.Name = "btnPrinting";
            this.btnPrinting.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnPrinting.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnPrinting.Size = new System.Drawing.Size(140, 28);
            this.btnPrinting.TabIndex = 47;
            this.btnPrinting.Text = "ارسال به چاپگر (F3)";
            this.btnPrinting.Tooltip = "ارسال فایل های شماره زده شده به چاپگر";
            this.btnPrinting.Click += new System.EventHandler(this.BtnPrintingClick);
            // 
            // lblSDate
            // 
            this.lblSDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSDate.BackColor = System.Drawing.Color.White;
            this.lblSDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblSDate.ForeColor = System.Drawing.Color.Green;
            this.lblSDate.Location = new System.Drawing.Point(531, 94);
            this.lblSDate.Name = "lblSDate";
            this.lblSDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSDate.Size = new System.Drawing.Size(144, 21);
            this.lblSDate.TabIndex = 51;
            this.lblSDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEDate
            // 
            this.lblEDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEDate.BackColor = System.Drawing.Color.White;
            this.lblEDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblEDate.ForeColor = System.Drawing.Color.Green;
            this.lblEDate.Location = new System.Drawing.Point(260, 94);
            this.lblEDate.Name = "lblEDate";
            this.lblEDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblEDate.Size = new System.Drawing.Size(150, 21);
            this.lblEDate.TabIndex = 52;
            this.lblEDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBillTitle
            // 
            this.lblBillTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBillTitle.BackColor = System.Drawing.Color.White;
            this.lblBillTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBillTitle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblBillTitle.ForeColor = System.Drawing.Color.Green;
            this.lblBillTitle.Location = new System.Drawing.Point(260, 60);
            this.lblBillTitle.Name = "lblBillTitle";
            this.lblBillTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBillTitle.Size = new System.Drawing.Size(150, 21);
            this.lblBillTitle.TabIndex = 53;
            this.lblBillTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainPanel
            // 
            this.MainPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.MainPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainPanel.Controls.Add(this.panelLog);
            this.MainPanel.Controls.Add(this.btnCpfUpdate);
            this.MainPanel.Controls.Add(this.MainStatusBar);
            this.MainPanel.Controls.Add(this.lblTime);
            this.MainPanel.Controls.Add(this.lblHour);
            this.MainPanel.Controls.Add(this.label5);
            this.MainPanel.Controls.Add(this.lblSecond);
            this.MainPanel.Controls.Add(this.lblMinutes);
            this.MainPanel.Controls.Add(this.label2);
            this.MainPanel.Controls.Add(this.lstPowerStations);
            this.MainPanel.Controls.Add(this.lstSellerList);
            this.MainPanel.Controls.Add(this.lstSellerType);
            this.MainPanel.Controls.Add(this.btnExcelOut);
            this.MainPanel.Controls.Add(this.btnSaveToEMS);
            this.MainPanel.Controls.Add(this.ProgressBarSearch);
            this.MainPanel.Controls.Add(this.lblBillTitle);
            this.MainPanel.Controls.Add(this.lblEDate);
            this.MainPanel.Controls.Add(this.lblSDate);
            this.MainPanel.Controls.Add(this.btnPrinting);
            this.MainPanel.Controls.Add(this.label1);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Controls.Add(this.lblParamName);
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.btnBillNumber);
            this.MainPanel.Controls.Add(this.lblEndDate);
            this.MainPanel.Controls.Add(this.lblBeginDate);
            this.MainPanel.Controls.Add(this.lblBillType);
            this.MainPanel.Controls.Add(this.lblParamDescribe);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(888, 625);
            this.MainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MainPanel.Style.GradientAngle = 90;
            this.MainPanel.TabIndex = 28;
            // 
            // panelLog
            // 
            this.panelLog.BackColor = System.Drawing.Color.LightYellow;
            this.panelLog.Controls.Add(this.lblLogViewer);
            this.panelLog.Controls.Add(this.btnHideShowLog);
            this.panelLog.Controls.Add(this.btnStopStage);
            this.panelLog.Controls.Add(this.richTextBoxLog);
            this.panelLog.Location = new System.Drawing.Point(162, 128);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(523, 352);
            this.panelLog.TabIndex = 75;
            this.panelLog.Visible = false;
            // 
            // lblLogViewer
            // 
            this.lblLogViewer.AutoSize = true;
            this.lblLogViewer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblLogViewer.Location = new System.Drawing.Point(4, 6);
            this.lblLogViewer.Name = "lblLogViewer";
            this.lblLogViewer.Size = new System.Drawing.Size(80, 16);
            this.lblLogViewer.TabIndex = 75;
            this.lblLogViewer.Text = "Log Viewer";
            // 
            // btnHideShowLog
            // 
            this.btnHideShowLog.Image = global::MohanirPouya.Properties.Resources.me4;
            this.btnHideShowLog.Location = new System.Drawing.Point(496, 0);
            this.btnHideShowLog.Name = "btnHideShowLog";
            this.btnHideShowLog.Size = new System.Drawing.Size(24, 26);
            this.btnHideShowLog.TabIndex = 74;
            this.btnHideShowLog.Text = "-";
            this.btnHideShowLog.UseVisualStyleBackColor = true;
            this.btnHideShowLog.Click += new System.EventHandler(this.btnHideShowLog_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Location = new System.Drawing.Point(0, 26);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.richTextBoxLog.Size = new System.Drawing.Size(523, 326);
            this.richTextBoxLog.TabIndex = 72;
            this.richTextBoxLog.Text = "";
            this.richTextBoxLog.Visible = false;
            // 
            // btnCpfUpdate
            // 
            this.btnCpfUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCpfUpdate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnCpfUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnCpfUpdate.Enabled = false;
            this.btnCpfUpdate.Location = new System.Drawing.Point(571, 565);
            this.btnCpfUpdate.Name = "btnCpfUpdate";
            this.btnCpfUpdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCpfUpdate.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.btnCpfUpdate.Size = new System.Drawing.Size(140, 28);
            this.btnCpfUpdate.TabIndex = 71;
            this.btnCpfUpdate.Text = "بروزرسانی F5) CPF)";
            this.btnCpfUpdate.Tooltip = "بروز رسانی ضرایب بهای آمادگی";
            this.btnCpfUpdate.Visible = false;
            this.btnCpfUpdate.Click += new System.EventHandler(this.btnCpfUpdate_Click);
            // 
            // MainStatusBar
            // 
            this.MainStatusBar.AccessibleDescription = "DotNetBar Bar (MainStatusBar)";
            this.MainStatusBar.AccessibleName = "DotNetBar Bar";
            this.MainStatusBar.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.MainStatusBar.AntiAlias = true;
            this.MainStatusBar.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.MainStatusBar.Controls.Add(this.lblConnection);
            this.MainStatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MainStatusBar.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.MainStatusBar.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainStatusBar.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.MainStatusBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblCurentUser});
            this.MainStatusBar.ItemSpacing = 2;
            this.MainStatusBar.Location = new System.Drawing.Point(0, 607);
            this.MainStatusBar.Name = "MainStatusBar";
            this.MainStatusBar.Size = new System.Drawing.Size(888, 18);
            this.MainStatusBar.Stretch = true;
            this.MainStatusBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MainStatusBar.TabIndex = 70;
            this.MainStatusBar.TabStop = false;
            this.MainStatusBar.Text = "barStatus";
            // 
            // lblConnection
            // 
            this.lblConnection.AutoSize = true;
            this.lblConnection.BackColor = System.Drawing.Color.Transparent;
            this.lblConnection.ForeColor = System.Drawing.Color.Blue;
            this.lblConnection.Location = new System.Drawing.Point(20, 4);
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(0, 14);
            this.lblConnection.TabIndex = 0;
            // 
            // lblCurentUser
            // 
            this.lblCurentUser.BeginGroup = true;
            this.lblCurentUser.Name = "lblCurentUser";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblTime.Location = new System.Drawing.Point(378, 567);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(89, 13);
            this.lblTime.TabIndex = 69;
            this.lblTime.Text = "مدت زمان اجرا :";
            // 
            // lblHour
            // 
            this.lblHour.AutoSize = true;
            this.lblHour.Location = new System.Drawing.Point(292, 567);
            this.lblHour.Name = "lblHour";
            this.lblHour.Size = new System.Drawing.Size(19, 13);
            this.lblHour.TabIndex = 68;
            this.lblHour.Text = "00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(311, 565);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 67;
            this.label5.Text = ":";
            // 
            // lblSecond
            // 
            this.lblSecond.AutoSize = true;
            this.lblSecond.Location = new System.Drawing.Point(352, 567);
            this.lblSecond.Name = "lblSecond";
            this.lblSecond.Size = new System.Drawing.Size(19, 13);
            this.lblSecond.TabIndex = 66;
            this.lblSecond.Text = "00";
            // 
            // lblMinutes
            // 
            this.lblMinutes.AutoSize = true;
            this.lblMinutes.Location = new System.Drawing.Point(322, 566);
            this.lblMinutes.Name = "lblMinutes";
            this.lblMinutes.Size = new System.Drawing.Size(19, 13);
            this.lblMinutes.TabIndex = 65;
            this.lblMinutes.Text = "00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(341, 565);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 64;
            this.label2.Text = ":";
            // 
            // lstPowerStations
            // 
            this.lstPowerStations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstPowerStations.BackColor = System.Drawing.Color.Transparent;
            this.lstPowerStations.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstPowerStations.Location = new System.Drawing.Point(21, 154);
            this.lstPowerStations.Name = "lstPowerStations";
            this.lstPowerStations.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lstPowerStations.Size = new System.Drawing.Size(266, 314);
            this.lstPowerStations.TabIndex = 63;
            this.lstPowerStations.Title = "لیست نیروگاه ها";
            // 
            // lstSellerList
            // 
            this.lstSellerList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstSellerList.BackColor = System.Drawing.Color.Transparent;
            this.lstSellerList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstSellerList.Location = new System.Drawing.Point(293, 154);
            this.lstSellerList.Name = "lstSellerList";
            this.lstSellerList.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lstSellerList.Size = new System.Drawing.Size(309, 314);
            this.lstSellerList.TabIndex = 62;
            this.lstSellerList.Title = "لیست فروشندگان";
            // 
            // lstSellerType
            // 
            this.lstSellerType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstSellerType.BackColor = System.Drawing.Color.Transparent;
            this.lstSellerType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lstSellerType.Location = new System.Drawing.Point(614, 154);
            this.lstSellerType.Name = "lstSellerType";
            this.lstSellerType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lstSellerType.Size = new System.Drawing.Size(243, 314);
            this.lstSellerType.TabIndex = 61;
            this.lstSellerType.Title = "فروشندگان";
            // 
            // btnExcelOut
            // 
            this.btnExcelOut.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExcelOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnExcelOut.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnExcelOut.Location = new System.Drawing.Point(202, 486);
            this.btnExcelOut.Name = "btnExcelOut";
            this.btnExcelOut.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnExcelOut.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5);
            this.btnExcelOut.Size = new System.Drawing.Size(146, 28);
            this.btnExcelOut.TabIndex = 60;
            this.btnExcelOut.Text = "اجرای مراحل صورتحساب (F5)";
            this.btnExcelOut.Tooltip = "شروع اجرای برنامه و طی شدن مراحل دریافت اطلاعات ، محاسبات و تولید خروجی اکسل";
            this.btnExcelOut.Click += new System.EventHandler(this.BtnExcelOutClick);
            // 
            // btnSaveToEMS
            // 
            this.btnSaveToEMS.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveToEMS.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSaveToEMS.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnSaveToEMS.Location = new System.Drawing.Point(498, 486);
            this.btnSaveToEMS.Name = "btnSaveToEMS";
            this.btnSaveToEMS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSaveToEMS.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.btnSaveToEMS.Size = new System.Drawing.Size(140, 28);
            this.btnSaveToEMS.TabIndex = 59;
            this.btnSaveToEMS.Text = "ذخیره در F4) EMS)";
            this.btnSaveToEMS.Tooltip = "ذخیره صورتحسابهای محاسبه شده در EMIS";
            this.btnSaveToEMS.Click += new System.EventHandler(this.BtnSaveToEmsClick);
            // 
            // ProgressBarSearch
            // 
            this.ProgressBarSearch.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ProgressBarSearch.ForeColor = System.Drawing.Color.Red;
            this.ProgressBarSearch.Location = new System.Drawing.Point(20, 529);
            this.ProgressBarSearch.Maximum = 2;
            this.ProgressBarSearch.Name = "ProgressBarSearch";
            this.ProgressBarSearch.ProgressType = DevComponents.DotNetBar.eProgressItemType.Marquee;
            this.ProgressBarSearch.Size = new System.Drawing.Size(839, 23);
            this.ProgressBarSearch.TabIndex = 58;
            this.ProgressBarSearch.TabStop = false;
            this.ProgressBarSearch.Text = "در حال محاسبه";
            this.ProgressBarSearch.TextVisible = true;
            this.ProgressBarSearch.Visible = false;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // BillSelectD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(888, 625);
            this.Controls.Add(this.MainPanel);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BillSelectD";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم تنظيمات نمايش صورتحساب";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BillSelectD_FormClosing);
            this.Load += new System.EventHandler(this.FormLoad);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.BillSelectDHelpRequested);
            ((System.ComponentModel.ISupportInitialize)(this.DataSetLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParametersLog)).EndInit();
            this.TopPanel.ResumeLayout(false);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.panelLog.ResumeLayout(false);
            this.panelLog.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainStatusBar)).EndInit();
            this.MainStatusBar.ResumeLayout(false);
            this.MainStatusBar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip MyToolTip;
        private System.Data.DataSet DataSetLog;
        private System.Data.DataTable ParametersLog;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn CCreationDate;
        private System.Data.DataColumn CStartDate;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Windows.Forms.Label lblParamDescribe;
        private System.Windows.Forms.Label lblBillType;
        private System.Windows.Forms.Label lblBeginDate;
        private System.Windows.Forms.Label lblEndDate;
        private DevComponents.DotNetBar.ButtonX btnBillNumber;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private System.Windows.Forms.Label lblParamName;
        private DevComponents.DotNetBar.PanelEx TopPanel;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ButtonX btnPrinting;
        private System.Windows.Forms.Label lblBillTitle;
        private DevComponents.DotNetBar.PanelEx MainPanel;
        public System.Windows.Forms.Label lblSDate;
        public System.Windows.Forms.Label lblEDate;
        internal DevComponents.DotNetBar.Controls.ProgressBarX ProgressBarSearch;
        private DevComponents.DotNetBar.ButtonX btnExcelOut;
        private DevComponents.DotNetBar.ButtonX btnSaveToEMS;
        private SettlementControlLibraries.Controls.MohanirCheckList lstPowerStations;
        private SettlementControlLibraries.Controls.MohanirCheckList lstSellerList;
        private SettlementControlLibraries.Controls.MohanirCheckList lstSellerType;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblHour;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblSecond;
        private System.Windows.Forms.Label lblMinutes;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Bar MainStatusBar;
        private System.Windows.Forms.Label lblConnection;
        private DevComponents.DotNetBar.LabelItem lblCurentUser;
        private DevComponents.DotNetBar.ButtonX btnCpfUpdate;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.Button btnStopStage;
        private System.Windows.Forms.Panel panelLog;
        private System.Windows.Forms.Button btnHideShowLog;
        private System.Windows.Forms.Label lblLogViewer;
    }
}