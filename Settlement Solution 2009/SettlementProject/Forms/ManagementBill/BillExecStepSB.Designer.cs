﻿namespace MohanirPouya.Forms.ManagementBill
{
    partial class BillExecStepSb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        private new void InitializeComponent()
        {
            this.btnEXEC = new DevComponents.DotNetBar.ButtonX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.FormPanel = new DevComponents.DotNetBar.PanelEx();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkStep1 = new System.Windows.Forms.CheckBox();
            this.chkStep2 = new System.Windows.Forms.CheckBox();
            this.chkStep3 = new System.Windows.Forms.CheckBox();
            this.chkStep4 = new System.Windows.Forms.CheckBox();
            this.chkStep5 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.MyMainPanel.SuspendLayout();
            this.FormPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Location = new System.Drawing.Point(258, 36);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(259, 0);
            // 
            // lblSep
            // 
            this.lblSep.Size = new System.Drawing.Size(343, 2);
            // 
            // MyMainPanel
            // 
            this.MyMainPanel.Size = new System.Drawing.Size(437, 300);
            this.MyMainPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.MyMainPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.MyMainPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.MyMainPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.MyMainPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.MyMainPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.MyMainPanel.Style.GradientAngle = 90;
            // 
            // btnEXEC
            // 
            this.btnEXEC.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEXEC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEXEC.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnEXEC.Location = new System.Drawing.Point(226, 256);
            this.btnEXEC.Name = "btnEXEC";
            this.btnEXEC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnEXEC.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnEXEC.Size = new System.Drawing.Size(98, 28);
            this.btnEXEC.TabIndex = 48;
            this.btnEXEC.Text = "اجرا ";
            this.btnEXEC.Tooltip = "اجرای مراحل صورتحساب";
            this.btnEXEC.Click += new System.EventHandler(this.BtnExecClick);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(112, 256);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCancel.Size = new System.Drawing.Size(98, 28);
            this.btnCancel.TabIndex = 49;
            this.btnCancel.Text = "انصراف <b><font color=\"#FFC20E\">(Esc)</font></b>";
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // FormPanel
            // 
            this.FormPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.FormPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.FormPanel.Controls.Add(this.groupBox1);
            this.FormPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormPanel.Location = new System.Drawing.Point(0, 0);
            this.FormPanel.Name = "FormPanel";
            this.FormPanel.Size = new System.Drawing.Size(437, 300);
            this.FormPanel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.FormPanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.FormPanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.FormPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.FormPanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.FormPanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.FormPanel.Style.GradientAngle = 90;
            this.FormPanel.TabIndex = 50;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkStep1);
            this.groupBox1.Controls.Add(this.chkStep2);
            this.groupBox1.Controls.Add(this.chkStep3);
            this.groupBox1.Controls.Add(this.chkStep4);
            this.groupBox1.Controls.Add(this.chkStep5);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.Red;
            this.groupBox1.Location = new System.Drawing.Point(16, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 209);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مراحل صدور صورتحساب";
            // 
            // chkStep1
            // 
            this.chkStep1.AutoSize = true;
            this.chkStep1.Checked = true;
            this.chkStep1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep1.Location = new System.Drawing.Point(29, 43);
            this.chkStep1.Name = "chkStep1";
            this.chkStep1.Size = new System.Drawing.Size(15, 14);
            this.chkStep1.TabIndex = 1;
            this.chkStep1.UseVisualStyleBackColor = true;
            // 
            // chkStep2
            // 
            this.chkStep2.AutoSize = true;
            this.chkStep2.Checked = true;
            this.chkStep2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep2.Location = new System.Drawing.Point(29, 77);
            this.chkStep2.Name = "chkStep2";
            this.chkStep2.Size = new System.Drawing.Size(15, 14);
            this.chkStep2.TabIndex = 2;
            this.chkStep2.UseVisualStyleBackColor = true;
            // 
            // chkStep3
            // 
            this.chkStep3.AutoSize = true;
            this.chkStep3.Checked = true;
            this.chkStep3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep3.Location = new System.Drawing.Point(29, 109);
            this.chkStep3.Name = "chkStep3";
            this.chkStep3.Size = new System.Drawing.Size(15, 14);
            this.chkStep3.TabIndex = 3;
            this.chkStep3.UseVisualStyleBackColor = true;
            // 
            // chkStep4
            // 
            this.chkStep4.AutoSize = true;
            this.chkStep4.Checked = true;
            this.chkStep4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep4.Location = new System.Drawing.Point(29, 141);
            this.chkStep4.Name = "chkStep4";
            this.chkStep4.Size = new System.Drawing.Size(15, 14);
            this.chkStep4.TabIndex = 4;
            this.chkStep4.UseVisualStyleBackColor = true;
            // 
            // chkStep5
            // 
            this.chkStep5.AutoSize = true;
            this.chkStep5.Checked = true;
            this.chkStep5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStep5.Location = new System.Drawing.Point(29, 173);
            this.chkStep5.Name = "chkStep5";
            this.chkStep5.Size = new System.Drawing.Size(15, 14);
            this.chkStep5.TabIndex = 5;
            this.chkStep5.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(71, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(301, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "5- تولید فایلهای اکسل و شماره زنی صورتحساب";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(227, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "4- ایجاد جدول خروجی";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(240, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "3- محاسبه پارامترها";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(73, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(299, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "2-دریافت اطلاعات از EMIS و پرکردن جدول اصلی";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(164, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "1- آماده سازی اولیه جدول اصلی";
            // 
            // BillExecStepSb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 300);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEXEC);
            this.Controls.Add(this.FormPanel);
            this.DoubleBuffered = true;
            this.Name = "BillExecStepSb";
            this.Text = "مراحل اجرای صورتحساب";
            this.Load += new System.EventHandler(this.BillExecStepSb_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.BillExecStepSb_HelpRequested);
            this.Controls.SetChildIndex(this.MyMainPanel, 0);
            this.Controls.SetChildIndex(this.FormPanel, 0);
            this.Controls.SetChildIndex(this.btnEXEC, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.MyMainPanel.ResumeLayout(false);
            this.MyMainPanel.PerformLayout();
            this.FormPanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private DevComponents.DotNetBar.ButtonX btnEXEC;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.PanelEx FormPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkStep1;
        private System.Windows.Forms.CheckBox chkStep2;
        private System.Windows.Forms.CheckBox chkStep3;
        private System.Windows.Forms.CheckBox chkStep4;
        private System.Windows.Forms.CheckBox chkStep5;
    }
}