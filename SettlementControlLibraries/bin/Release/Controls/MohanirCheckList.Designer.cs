﻿namespace SettlementControlLibraries.Controls
{
    partial class MohanirCheckList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Group = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.List = new System.Windows.Forms.CheckedListBox();
            this.MyBar = new DevComponents.DotNetBar.Bar();
            this.btnSelectAll = new DevComponents.DotNetBar.ButtonItem();
            this.btnSelectNone = new DevComponents.DotNetBar.ButtonItem();
            this.Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MyBar)).BeginInit();
            this.SuspendLayout();
            // 
            // Group
            // 
            this.Group.BackColor = System.Drawing.Color.Transparent;
            this.Group.CanvasColor = System.Drawing.SystemColors.Control;
            this.Group.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.Group.Controls.Add(this.List);
            this.Group.Controls.Add(this.MyBar);
            this.Group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Group.Location = new System.Drawing.Point(0, 0);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(170, 246);
            // 
            // 
            // 
            this.Group.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.Group.Style.BackColorGradientAngle = 90;
            this.Group.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.DockSiteBackColor;
            this.Group.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.Group.Style.BorderBottomWidth = 1;
            this.Group.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.Group.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.Group.Style.BorderLeftWidth = 1;
            this.Group.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.Group.Style.BorderRightWidth = 1;
            this.Group.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.Group.Style.BorderTopWidth = 1;
            this.Group.Style.CornerDiameter = 4;
            this.Group.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.Group.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.Group.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarCaptionInactiveText;
            this.Group.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.Group.TabIndex = 28;
            this.Group.Text = "تیتر";
            // 
            // List
            // 
            this.List.CheckOnClick = true;
            this.List.Dock = System.Windows.Forms.DockStyle.Fill;
            this.List.FormattingEnabled = true;
            this.List.IntegralHeight = false;
            this.List.Location = new System.Drawing.Point(0, 24);
            this.List.Name = "List";
            this.List.Size = new System.Drawing.Size(164, 200);
            this.List.TabIndex = 26;
            // 
            // MyBar
            // 
            this.MyBar.AccessibleDescription = "DotNetBar Bar (MyBar)";
            this.MyBar.AccessibleName = "DotNetBar Bar";
            this.MyBar.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.MyBar.AutoHide = true;
            this.MyBar.CanHide = true;
            this.MyBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.MyBar.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.MyBar.FadeEffect = true;
            this.MyBar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSelectAll,
            this.btnSelectNone});
            this.MyBar.Location = new System.Drawing.Point(0, 0);
            this.MyBar.MenuBar = true;
            this.MyBar.Name = "MyBar";
            this.MyBar.Size = new System.Drawing.Size(164, 24);
            this.MyBar.Stretch = true;
            this.MyBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.MyBar.TabIndex = 27;
            this.MyBar.TabStop = false;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.TextOnlyAlways;
            this.btnSelectAll.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.PopupType = DevComponents.DotNetBar.ePopupType.ToolBar;
            this.btnSelectAll.Text = "انتخاب همه";
            this.btnSelectAll.Tooltip = "انتخاب تمام گزینه های موجود";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnSelectNone
            // 
            this.btnSelectNone.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.TextOnlyAlways;
            this.btnSelectNone.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSelectNone.Name = "btnSelectNone";
            this.btnSelectNone.PopupType = DevComponents.DotNetBar.ePopupType.ToolBar;
            this.btnSelectNone.Text = "انتخاب هیچ";
            this.btnSelectNone.Tooltip = "حذف انتخاب كلیه گزینه ها";
            this.btnSelectNone.Click += new System.EventHandler(this.btnSelectNone_Click);
            // 
            // MohanirCheckList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.Group);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Name = "MohanirCheckList";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(170, 246);
            this.Group.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MyBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel Group;
        public System.Windows.Forms.CheckedListBox List;
        private DevComponents.DotNetBar.Bar MyBar;
        private DevComponents.DotNetBar.ButtonItem btnSelectAll;
        private DevComponents.DotNetBar.ButtonItem btnSelectNone;
    }
}
