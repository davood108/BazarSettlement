﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SettlementControlLibraries.Controls
{
    /// <summary>
    /// كنترل لیست باكس مهانیر
    /// </summary>
    [ToolboxBitmap(typeof(ListBox))]
    [Description("كنترل لیست باكس مهانیر")]
    public partial class MohanirCheckList : UserControl
    {
        #region Fields

        /// <summary>
        /// شیء اتصال به بانك اطلاعاتی
        /// </summary>
        private SqlConnection _SqlConnection = new SqlConnection();
        /// <summary>
        /// شیء اجرای دستورات در بانك اطلاعاتی
        /// </summary>
        private SqlCommand _SqlCommand = new SqlCommand();

        #endregion

        #region Properties

        #region Title Property
        /// <summary>
        /// تعیین تیتر كنترل
        /// </summary>
        [Description("تعیین تیتر كنترل")]
        [Category("مهانیر لیست باكس")]
        [Browsable(true)]
        public String Title
        {
            get { return Group.Text; }
            set { Group.Text = value; }
        }
        #endregion

        #region TheSqlConnection Property
        /// <summary>
        /// مشخصات شیء ارتباطی با بانك اطلاعات
        /// </summary>
        [Description("مشخصات شیء ارتباطی با بانك اطلاعات")]
        [Category("مهانیر لیست باكس")]
        [ParenthesizePropertyName(true)]
        [Browsable(true)]
        public SqlConnection TheSqlConnection
        {
            get { return _SqlConnection; }
            set { _SqlConnection = value; }
        }
        #endregion

        #region TheSqlCommand Property
        /// <summary>
        /// مشخصات شیء فرمان اجرایی اس كیو ال
        /// </summary>
        [Description("مشخصات شیء فرمان اجرایی اس كیو ال")]
        [Category("مهانیر لیست باكس")]
        [ParenthesizePropertyName(true)]
        [Browsable(true)]
        public SqlCommand TheSqlCommand
        {
            get { return _SqlCommand; }
            set { _SqlCommand = value; }
        }
        #endregion

        #endregion

        #region Constructors & Destructor

        public MohanirCheckList()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        #region Fill ListBox Method
        /// <summary>
        /// تكمیل لیست باكس با استفاده از رشته اتصال
        /// </summary>
        /// <returns></returns>
        public void FillListBox()
        {
            List.Items.Clear();
            if (CheckFieldsReady() == false)
                return;
            using (_SqlConnection)
            {
                try
                {
                    _SqlCommand.Connection = _SqlConnection;
                    _SqlConnection.Open();

                    SqlDataReader MySqlDataReader =
                        _SqlCommand.ExecuteReader();
                    List.BeginUpdate();
                    if (MySqlDataReader.HasRows)
                        while (MySqlDataReader.Read())
                            List.Items.Add(MySqlDataReader[0].ToString());

                    MySqlDataReader.Close();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("بروز خطایی با متن زیر" + "\n" + Ex.Message , 
                    "خطا!", MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
                }
                finally
                {
                    _SqlConnection.Close();
                    List.EndUpdate();
                }
            }
        }
        #endregion

        #region Check Fields Method
        /// <summary>
        /// بررسی نمونه های ساخته شده از اشیاء
        /// </summary>
        private Boolean CheckFieldsReady()
        {
            if (_SqlConnection == null || _SqlCommand == null)
                return false;
            return true;
        }
        #endregion

        #endregion

        #region Events Handler
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < List.Items.Count; i++)
            {
                List.BeginUpdate();
                List.SetItemChecked(i, true);
                List.SetSelected(0, true);
                List.EndUpdate();
            }
        }

        private void btnSelectNone_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < List.Items.Count; i++)
            {
                List.BeginUpdate();
                List.SetItemChecked(i, false);
                List.SetSelected(0, true);
                List.EndUpdate();
            }
        }
        #endregion

    }
}